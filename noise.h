/****************************************************************************
* Machinations copyright (C) 2001, 2002 by Jon Sargeant and Jindra Kolman   *
****************************************************************************/
#ifndef noiseH
#define noiseH
/****************************************************************************/
#include "vec.h"
#include "mtrand.h"

extern void generateNoise(float *_data, int _sizeX, int _sizeY, float _variation, float _power,
                MTRand_int32& randomNumber);
extern void generateRandomValues(float *data, int sizeX, int sizeY, MTRand_int32& randomNumber);
extern void smoothNoise(float *in,  float *out, int sizeX, int sizeY, float amount);
extern float interpolateSplinePoint(float *data, int sizeX, int sizeY, float x, float y);
extern tVector3D interpolateSplineNormal(float *data, int sizeX, int sizeY, float x, float y);
extern void interpolateSplineSurface(float *in, float *out, int inX, int inY, int outX, int outY);
extern void generateRandomSplineSurface(float *out, int inX, int inY, int outX, int outY,
                MTRand_int32& randomNumber);
extern void copyNoise(float *in, float *out, int sizeX, int sizeY);
/****************************************************************************/
#endif
