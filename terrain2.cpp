/****************************************************************************
* Machinations copyright (C) 2001-2003 by John Carter and Jon Sargeant      *
*                                                                           *
* TERRAIN.CPP:     Generates and draws the terrain.                         *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
#include <GL/glfw.h> /////////////////reversed these lines/////////////////
#include <IL/ilut.h> /////////////////reversed these lines/////////////////
#ifndef CACHE_HEADERS
    #ifdef BORLAND
        #include <stack.h> //for stack
    #else
        #include <stack> //for stack
    #endif
#endif
/****************************************************************************/
#include "terrain2.h"
#include "noise.h"
#include "memwatch.h"
#include "oglwrap.h"
#include "client02.h"
#ifndef BORLAND
    #include "snprintf.h"
#endif

#pragma package(smart_init)
/****************************************************************************/
const long machTerrainCode='T'+('E'<<8)+('R'<<16)+('1'<<24);

void tTerrain::loadRandomControlPoints()
{
    int x,y;
    bool found;

    const int intervalCount=100;
    int i, j, index, intervals[intervalCount]={0};
    int total=0; //target=(int)(percentWater*controlSizeX*controlSizeY);
    float min=MAX_FLOAT, max=-MAX_FLOAT;
    float bias, scale;
    float val, addend;
    float *userBuffers[8]={NULL};
    float *output=NULL;

    //Seed the random number generator with the specified value.  We seed the
    //random number generator before each component so that the components are
    //independent of one another.
    randomNumber.seed(seed);

    displayProgressBar("Generating control points...  Please Wait.");
    invalidateScreen();

    for(i=0;i<splineSurfaces.size();i++)
    {
        tSplineSurface& ss=splineSurfaces[i];

        if(ss.target==SPLINE_SURFACE_BUFFER)
        {
            if(ss.targetIndex<0 || ss.targetIndex>=MAX_BUFFERS)
            {
                bug("tTerrain::loadRandomControlPoints: invalid target index #1");
                break;
            }
            if(userBuffers[ss.targetIndex]==NULL)
            {
                userBuffers[ss.targetIndex]=mwNew float[controlSizeX*controlSizeY];
                memset((void*)userBuffers[ss.targetIndex],0,sizeof(float)*controlSizeX*controlSizeY);
            }
            output=userBuffers[ss.targetIndex];
        }
        else if(ss.target==SPLINE_SURFACE_DECORATION_DENSITY)
        {
            if(ss.targetIndex<0 || ss.targetIndex>=decorationTypes.size())
            {
                bug("tTerrain::loadRandomControlPoints: invalid target index #2");
                break;
            }
            if(decorationTypes[ss.targetIndex].densityData==NULL)
            {
                decorationTypes[ss.targetIndex].densityData=mwNew float[controlSizeX*controlSizeY];
                memset((void*)decorationTypes[ss.targetIndex].densityData,0,sizeof(float)*controlSizeX*controlSizeY);
            }
            output=decorationTypes[ss.targetIndex].densityData;
        }

        for(j=0;j<ss.waveforms.size();j++)
        {
            tWaveform& w=ss.waveforms[j];

            if(w.source==WAVEFORM_RANDOM)
            {
                w.data=mwNew float[controlSizeX*controlSizeY];
                generateRandomSplineSurface(w.data,(int)((float)controlSizeX/w.wavelength),
                    (int)((float)controlSizeY/w.wavelength), controlSizeX,controlSizeY, randomNumber);
            }
            else if(w.source==WAVEFORM_DECORATION_DENSITY)
            {
                if(w.sourceIndex<0 || w.sourceIndex>=decorationTypes.size())
                {
                    bug("tTerrain::loadRandomControlPoints: invalid source index #1");
                    goto break_2;
                }
                w.data=decorationTypes[w.sourceIndex].densityData;
            }
            else if(w.source==WAVEFORM_BUFFER)
            {
                if(w.sourceIndex<0 || w.sourceIndex>=MAX_BUFFERS)
                {
                    bug("tTerrain::loadRandomControlPoints: invalid source index #2");
                    goto break_2;
                }
                w.data=userBuffers[w.sourceIndex];
            }
        }

        for(x=0;x<controlSizeX;x++)
            for(y=0;y<controlSizeY;y++)
            {
                tControlPoint& node=getControlPoint(x,y);

                addend=1;

                for(j=0;j<ss.waveforms.size();j++)
                {
                    tWaveform& w=ss.waveforms[j];

                    switch(w.source)
                    {
                        case WAVEFORM_RANDOM:
                            val=(w.data[y*controlSizeX+x]-0.5)*2;
                            break;
                        case WAVEFORM_ELEVATION:
                            val=node.height;
                            break;
                        case WAVEFORM_STRATUM_TYPE:
                            val=node.stratumIndex;
                            break;
                        case WAVEFORM_BUFFER:
                        case WAVEFORM_DECORATION_DENSITY:
                            if(w.data)
                                val=w.data[y*controlSizeX+x];
                            else
                                val=0;
                            break;
                        case WAVEFORM_NONE:
                            val=0;
                            break;
                    }

                    addend*=transformValue(w.amplitude*val+w.bias,w.mappings);
                }

                switch(ss.target)
                {
                    case SPLINE_SURFACE_ELEVATION:
                        node.height+=addend;
                        if(ss.influenceFlightPath)
                            node.smoothHeight+=addend;
                        break;
                    case SPLINE_SURFACE_STRATUM_TYPE:
                        node.stratumIndex+=addend;
                        clampRange(node.stratumIndex,1.0f,(float)strata.size());
                        break;
                    case SPLINE_SURFACE_BUFFER:
                    case SPLINE_SURFACE_DECORATION_DENSITY:
                        output[y*controlSizeX+x]+=addend;
                        break;
                    case SPLINE_SURFACE_NONE:
                        break;
                }
            }

        for(j=0;j<ss.waveforms.size();j++)
        {
            tWaveform& w=ss.waveforms[j];
            if(w.source==WAVEFORM_RANDOM && w.data)
                mwDelete[] w.data;
            w.data=NULL;
        }

        updateProgressBar((float)i/(splineSurfaces.size()+1));
        invalidateScreen();
    }
break_2: ;

    for(i=0;i<8;i++)
        if(userBuffers[i])
        {
            mwDelete[] userBuffers[i];
            userBuffers[i]=NULL;
        }

    //Raise or lower the terrain to fulfill the percent water requirement to within 1%:
    //Step 1: Count the number of tiles within each of 100 intervals.
    //Step 1: Find the proper division between intervals and add the appropriate value to the elevations.
    /*for(x=0;x<controlSizeX;x++)
        for(y=0;y<controlSizeY;y++)
        {
            tControlPoint& node=getControlPoint(x,y);
            if(node.height<min)
                min=node.height;
            if(node.height>max)
                max=node.height;
        }

    scale=(max-min)/(float)intervalCount;
    if(scale>0)
    {
        for(x=0;x<controlSizeX;x++)
            for(y=0;y<controlSizeY;y++)
            {
                tControlPoint& node=getControlPoint(x,y);

                index=(int)((node.height-min)/scale);
                clampRange(index,0,intervalCount-1);
                intervals[index]++;
            }

        for(i=0;i<intervalCount;i++)
            if(intervals[i]!=0)
                break;
        for(;i<intervalCount;i++)
        {
            if(total>=target)
                break;
            total+=intervals[i];
        }

        bias=-((float)i*scale+min);
    }
    else
        bias=-min;

    for(x=0;x<controlSizeX;x++)
        for(y=0;y<controlSizeY;y++)
        {
            getControlPoint(x,y).height+=bias;
            getControlPoint(x,y).smoothHeight+=bias;
        }*/

    for(x=0;x<controlSizeX;x++)
        for(y=0;y<controlSizeY;y++)
        {
            tControlPoint& node=getControlPoint(x,y);
            node.underwater=(node.height<-minPoolDepth);
        }

    //Fill pools with water:
    //Step 1: Identify tiles whose elevation is less than waterDepth.
    //Step 2: Flood fill the area bounded by tiles whose elevation exceeds waterLevel.
    do
    {
        found=false;

        for(x=0;x<controlSizeX;x++)
            for(y=0;y<controlSizeY;y++)
            {
                tControlPoint& node=getControlPoint(x,y);
                if(node.height<=maxSeaElevation && !node.underwater)
                    if(getControlPoint(x+1,y).underwater ||
                        getControlPoint(x-1,y).underwater ||
                        getControlPoint(x,y+1).underwater ||
                        getControlPoint(x,y-1).underwater)
                    {
                        node.underwater=true;
                        found=true;
                    }
            }
    }
    while(found);

    updateProgressBar(1);
    invalidateScreen();
    closeProgressBar();
}
void tTerrain::loadRandomDecorations()
{
    tDecoration d;
    tVector2D v2,above,below,center;
    tTileInfo ti;
    int i,j,x,y,xi,yi,di,decorationCount;
    float df;
    FILE *fp=NULL;
    long l;
    int index,count;
    tShorePoint sp;

    //Seed the random number generator with the specified value.  We seed the
    //random number generator before each component so that the components are
    //independent of one another.
    randomNumber.seed(seed);

    tCache decorationCache;
    decorationCache.addStringToHeader(" Machinations Decoration Cache ");
    addControlPointVariablesToCacheHeader(decorationCache);

    fp=decorationCache.readFromCache(CACHE_PATH "decors.dat");
    if(fp)
    {
        while(true)
        {
            fread(&l,sizeof(l),1,fp);
            index=(int)l;
            if(index<0 || index>=decorationTypes.size())
                break;

            tDecorationType& dt=decorationTypes[index];
            fread(&l,sizeof(l),1,fp);
            decorationCount=(int)l;

            dt.decorations.assign(decorationCount,d);
            fread(&dt.decorations[0],sizeof(tDecoration),decorationCount,fp);
        }

        fclose(fp);
        fp=NULL;
    }
    else
    {
        displayProgressBar("Generating terrain decorations...  Please wait.");

        for(i=0;i<decorationTypes.size();i++)
        {
            tDecorationType& dt=decorationTypes[i];

            if(dt.modelType && dt.densityData)
            {
                for(yi=0;yi<controlSizeY;yi++)
                    for(xi=0;xi<controlSizeX;xi++)
                    {
                        df=dt.densityData[yi*controlSizeX+xi]*controlScaleX*controlScaleY;
                        di=(int)df;
                        df-=(float)di;

                        decorationCount=di;
                        if((float)randomNumber()/MTRAND_MAX<df)
                            decorationCount++;

                        for(j=0;j<decorationCount;j++)
                        {
                            d.position.x=((float)xi+(float)randomNumber()/MTRAND_MAX)*controlScaleX;
                            d.position.y=((float)yi+(float)randomNumber()/MTRAND_MAX)*controlScaleY;
                            ti=getTileInfo(to2D(d.position),DOMAIN_NONE,SEA_FLOOR_DEPTH);
                            d.position.z=ti.height;
                            d.forward.x=(randomNumber()%2049)/1024.0-1;
                            d.forward.y=(randomNumber()%2049)/1024.0-1;
                            d.forward.z=0;
                            d.forward=d.forward.normalize();
                            d.up=ti.normal;
                            d.up.z*=2;
                            d.up=d.up.normalize();
                            d.side=d.up.cross(d.forward);
                            d.forward=d.side.cross(d.up);

                            dt.decorations.push_back(d);
                        }
                    }
            }
            //We no longer need the density data so we can deallocate it:
            if(dt.densityData)
            {
                mwDelete[] dt.densityData;
                dt.densityData=NULL;
            }

            updateProgressBar( (float)i/decorationTypes.size() );
            invalidateScreen();
        }
        updateProgressBar(1);
        invalidateScreen();
        closeProgressBar();

        fp=decorationCache.writeToCache(CACHE_PATH "decors.dat");
        if(fp)
        {
            for(i=0;i<decorationTypes.size();i++)
            {
                tDecorationType& dt=decorationTypes[i];

                if(dt.decorations.size()>0)
                {
                    l=(long)i;
                    fwrite(&l,sizeof(l),1,fp);
                    l=(long)dt.decorations.size();
                    fwrite(&l,sizeof(l),1,fp);
                    fwrite(&dt.decorations[0],sizeof(tDecoration),dt.decorations.size(),fp);
                }
            }
            l=-1;
            fwrite(&l,sizeof(l),1,fp);

            fclose(fp);
            fp=NULL;
        }
    }

    //Calculate shore points
    if(waveTexture>0)
    {
        tCache shorePointCache;
        shorePointCache.addStringToHeader(" Machinations Shore Point Cache ");
        addControlPointVariablesToCacheHeader(shorePointCache);

        fp=shorePointCache.readFromCache(CACHE_PATH "shorepts.dat");
        if(fp)
        {
            fread(&l,sizeof(l),1,fp);
            count=(int)l;

            shorePoints.assign(count,sp);
            fread(&shorePoints[0],sizeof(tShorePoint),count,fp);

            fclose(fp);
            fp=NULL;
        }
        else
        {
            displayProgressBar("Generating shore points...  Please wait.");
            //Vertical edges:
            for(x=0;x<controlSizeX-1;x++)
            {
                for(y=0;y<controlSizeY;y++)
                {
                    above.x=-1;
                    below.x=-1;
                    v2.x=(float)x;      v2.y=(float)y;
                    ti=getTileInfo(v2,DOMAIN_NONE,SEA_FLOOR_DEPTH);
                    if(ti.underwater)
                        below=v2;
                    else
                        above=v2;
                    v2.x=(float)(x+1);  v2.y=(float)y;
                    ti=getTileInfo(v2,DOMAIN_NONE,SEA_FLOOR_DEPTH);
                    if(ti.underwater)
                        below=v2;
                    else
                        above=v2;

                    if(above.x>=0 && below.x>=0)
                    {
                        for(i=0;i<10;i++) //arbitrary value
                        {
                            center=(above+below)/2;
                            ti=getTileInfo(center,DOMAIN_NONE,SEA_FLOOR_DEPTH);
                            if(ti.underwater)
                                below=center;
                            else
                                above=center;
                        }

                        addShorePoint(center);
                    }
                }
                if(x%16==0)
                {
                    updateProgressBar(((float)x/controlSizeX) / 2);
                    invalidateScreen();
                }
            }
            updateProgressBar(1.0/2);
            invalidateScreen();

            //Horizontal edges:
            for(x=0;x<controlSizeX;x++)
            {
                for(y=0;y<controlSizeY-1;y++)
                {
                    above.x=-1;
                    below.x=-1;
                    v2.x=(float)x;      v2.y=(float)y;
                    ti=getTileInfo(v2,DOMAIN_NONE,SEA_FLOOR_DEPTH);
                    if(ti.underwater)
                        below=v2;
                    else
                        above=v2;
                    v2.x=(float)x;      v2.y=(float)(y+1);
                    ti=getTileInfo(v2,DOMAIN_NONE,SEA_FLOOR_DEPTH);
                    if(ti.underwater)
                        below=v2;
                    else
                        above=v2;

                    if(above.x>=0 && below.x>=0)
                    {
                        for(i=0;i<10;i++) //arbitrary value
                        {
                            center=(above+below)/2;
                            ti=getTileInfo(center,DOMAIN_NONE,SEA_FLOOR_DEPTH);
                            if(ti.underwater)
                                below=center;
                            else
                                above=center;
                        }

                        addShorePoint(center);
                    }
                }
                if(x%16==0)
                {
                    updateProgressBar(1.0/2 + ((float)x/controlSizeX) / 2);
                    invalidateScreen();
                }
            }
            updateProgressBar(1);
            invalidateScreen();
            closeProgressBar();
        }

        fp=shorePointCache.writeToCache(CACHE_PATH "shorepts.dat");
        l=(long)shorePoints.size();
        fwrite(&l,sizeof(l),1,fp);
        fwrite(&shorePoints[0],sizeof(tShorePoint),shorePoints.size(),fp);

        fclose(fp);
        fp=NULL;
    }
}

void tTerrain::addShorePoint(const tVector2D& v)
{
    float i,a,b,h,zu=0,zv=0;
    int x,y,q,r;
    float *tableA,*tableB,*tableC,*tableD;
    tVector2D v2;
    tTileInfo ti;

    //Determine which tile the position falls in:
    a=v.x/controlScaleX;
    b=v.y/controlScaleY;

    x=(int)a;
    y=(int)b;

    //Clamp x and y:
    clampRange(x,0,controlSizeX-2);
    clampRange(y,0,controlSizeY-2);

    //Find fractional offset within tile:
    a-=(float)x;
    b-=(float)y;
    clampRange(a,0.0f,1.0f);
    clampRange(b,0.0f,1.0f);

    tableA=lookupSplineArray(a);
    tableB=lookupSplineArray(b);
    tableC=lookupSplineDerivArray(a);
    tableD=lookupSplineDerivArray(b);

    for(q=0;q<4;q++)
        for(r=0;r<4;r++)
        {
            h=getControlPoint(x+q-1,y+r-1).height;
            zu+=h*tableC[q]*tableB[r];
            zv+=h*tableA[q]*tableD[r];
        }

    tVector2D gradient={zu,zv};
    gradient=gradient.normalize();
    tVector2D gradientCW=gradient.perpendicularCW();
    tVector2D gradientCCW=gradient.perpendicularCCW();

    for(i=1;i<=10;i++)
    {
        v2=v+gradient*i;
        ti=getTileInfo(v2,DOMAIN_NONE,SEA_FLOOR_DEPTH);
        if(i<=3 && ti.underwater)
            return;
    }
    for(i=1;i<=20;i++)
    {
        v2=v-gradient*i;
        ti=getTileInfo(v2,DOMAIN_NONE,SEA_FLOOR_DEPTH);
        if(!ti.underwater)
            return;
    }
    for(i=1;i<=10;i++)
    {
        v2=v-gradient*i+gradientCW*3;
        ti=getTileInfo(v2,DOMAIN_NONE,SEA_FLOOR_DEPTH);
        if(!ti.underwater)
            return;
    }
    for(i=1;i<=10;i++)
    {
        v2=v-gradient*i+gradientCCW*3;
        ti=getTileInfo(v2,DOMAIN_NONE,SEA_FLOOR_DEPTH);
        if(!ti.underwater)
            return;
    }

    tShorePoint sp(v,gradient);
    shorePoints.push_back(sp);
}

tTerrain::tTerrain(float width, float height, tStream& s) throw(tString) :
    controlPoints(NULL), waterTexture(0), interval(1), mapWidth(width), mapHeight(height),
    waterScaleX(4), waterScaleY(4), minimapTexture(0), seaLevel(0),
    minElevation(MAX_FLOAT), maxElevation(-MAX_FLOAT), averageElevation(0),
    shadeTexture(0), controlScaleX(1), controlScaleY(1), waveTexture(0), waveScaleX(4),
    waveScaleY(2), waveLength(5), waveDuration(6), waveFrequency(0.1), waterMapTexture(0),
    waveRadius(0), randomNumber(0), seed(0), intervalFixed(false),
    internalShadeSize(0), internalAlphaSize(0),
    internalWaterSize(0), /*percentWater(0.5)*/ minPoolDepth(0), stratumTexture3D(0),
    minSeaElevation(0), maxSeaElevation(0), shadeMapScale(1), shadeMapBias(0),
    shadeMapMin(0), shadeMapMax(1), seaAlphaScale(-0.1), seaAlphaBias(0.5),
    seaAlphaMin(0), seaAlphaMax(1),
    shadowTextureMinX(0),shadowTextureMaxX(0),shadowTextureMinY(0),shadowTextureMaxY(0),
    nextMeshTriangleID(0), meshTriangleBlock(NULL)
{
    int i,x,y,count;
    FILE *fp=NULL;
    int sx,sy;
    float totalHeight=0;
    tString name;
    char buf[100];
    tCache cache1,cache2;
    FILE *cachefp1=NULL,*cachefp2=NULL;
    tMeshVertex mv;
    long l;

    if(width<=0 || height<=0)
    {
        bug("tTerrain::tTerrain: width and/or height invalid");
        throw tString("Unexpected error occurred");
    }

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"spline_surface"))
				parseSplineSurfaceBlock(s);
			else if(strPrefix(str,"stratum"))
			{
				if(strata.size()==MAX_STRATA)
				{
					snprintf(buf,sizeof(buf),"You can only define %d strata",MAX_STRATA);
					throw tString(buf);
				}
				parseStratumBlock(s);
			}
			else if(strPrefix(str,"decoration_type"))
				parseDecorationTypeBlock(s);
			else if(strPrefix(str,"water"))
			{
				name=parseString("water filename",s);
				waterFilename=tString(TERRAIN_PATH)+name;
				if(s.hasOptionalParameter())
				{
					waterScaleX=parseFloatMin("water scale x",0.01,s);
					waterScaleY=parseFloatMin("water scale y",0.01,s);
				}
			}
			else if(strPrefix(str,"wave"))
			{
				name=parseString("wave filename",s);
				waveFilename=tString(TERRAIN_PATH)+name;
				if(s.hasOptionalParameter())
				{
					waveScaleX=parseFloatMin("wave scale x",0.01,s);
					waveScaleY=parseFloatMin("wave scale y",0.01,s);
				}
			}
			else if(strPrefix(str, "wave_length"))
				waveLength=parseFloatMin("wave length",0.01,s);
			else if(strPrefix(str, "wave_duration"))
				waveDuration=parseFloatMin("wave duration",0.01,s);
			else if(strPrefix(str, "wave_frequency"))
				waveFrequency=parseFloatMin("wave frequency",0,s);
			else if(strPrefix(str, "seed"))
				seed=parseInt("seed number",s);
			/*else if(strPrefix(str, "percent_water"))
				percentWater=parseFloatRange("percent water",0,100,s)/100.0;*/
			else if(strPrefix(str, "minimum_pool_depth")||strPrefix(str, "min_pool_depth"))
				minPoolDepth=parseFloatMin("minimum pool depth",0,s);
			else if(strPrefix(str, "max_sea_elevation")||strPrefix(str, "maximum_sea_elevation"))
				maxSeaElevation=parseFloatMin("max sea elevation",0,s);
			else if(strPrefix(str, "min_sea_elevation")||strPrefix(str, "minimum_sea_elevation"))
				minSeaElevation=parseFloatMax("min sea elevation",0,s);
			else if(strPrefix(str, "shade_map_scale"))
				shadeMapScale=parseFloat("shade map scale",s);
			else if(strPrefix(str, "shade_map_bias"))
				shadeMapBias=parseFloat("shade map bias",s);
			else if(strPrefix(str, "shade_map_minimum"))
				shadeMapMin=parseFloat("shade map minimum",s);
			else if(strPrefix(str, "shade_map_maximum"))
				shadeMapMax=parseFloat("shade map maximum",s);
			else if(strPrefix(str, "sea_alpha_scale"))
				seaAlphaScale=parseFloat("sea alpha scale",s);
			else if(strPrefix(str, "sea_alpha_bias"))
				seaAlphaBias=parseFloat("sea alpha bias",s);
			else if(strPrefix(str, "sea_alpha_min"))
				seaAlphaMin=parseFloat("sea alpha min",s);
			else if(strPrefix(str, "sea_alpha_max"))
				seaAlphaMax=parseFloat("sea alpha max",s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

    try
    {
        controlSizeX=(int)ceil(mapWidth/CONTROL_SPACING);
        controlSizeY=(int)ceil(mapHeight/CONTROL_SPACING);
        controlScaleX=(float)mapWidth/controlSizeX;
        controlScaleY=(float)mapHeight/controlSizeY;

        //Allocate two extra rows and two extra columns for spline margins.
        //We will store cell (0,0) at nodes[1][1].
        controlPoints = mwNew tControlPoint[(controlSizeX+2)*(controlSizeY+2)];

        cache1.addStringToHeader(" Machinations Control Point Cache ");
        addControlPointVariablesToCacheHeader(cache1);

        cache2.addStringToHeader(" Machinations Mesh Cache ");
        addControlPointVariablesToCacheHeader(cache2);
        cache2.addFloatToHeader(minSeaElevation);
        cache2.addFloatToHeader(maxSeaElevation);

        cachefp1=cache1.readFromCache(CACHE_PATH "ctrlpts.dat");
        if(cachefp1)
        {
            fread(controlPoints,sizeof(tControlPoint),(controlSizeX+2)*(controlSizeY+2),cachefp1);
            fread(&minElevation,sizeof(minElevation),1,cachefp1);
            fread(&maxElevation,sizeof(maxElevation),1,cachefp1);
            fread(&averageElevation,sizeof(averageElevation),1,cachefp1);
            fclose(cachefp1);
            cachefp1=NULL;
        }
        else
        {
            memset((void*)controlPoints,0,sizeof(tControlPoint)*(controlSizeX+2)*(controlSizeY+2));
            loadRandomControlPoints();

            sx=controlSizeX;
            sy=controlSizeY;

            //Calculate spline margins:
            for(x=0;x<sx;x++)
            {
                getControlPoint(x,-1).height=getControlPoint(x,   0).height*2-getControlPoint(x,   1).height;
                getControlPoint(x,sy).height=getControlPoint(x,sy-1).height*2-getControlPoint(x,sy-2).height;

                getControlPoint(x,-1).stratumIndex=
                    getControlPoint(x,   0).stratumIndex*2-getControlPoint(x,   1).stratumIndex;
                getControlPoint(x,sy).stratumIndex=
                    getControlPoint(x,sy-1).stratumIndex*2-getControlPoint(x,sy-2).stratumIndex;
            }
            for(y=0;y<sy;y++)
            {
                getControlPoint(-1,y).height=getControlPoint(   0,y).height*2-getControlPoint(   1,y).height;
                getControlPoint(sx,y).height=getControlPoint(sx-1,y).height*2-getControlPoint(sx-2,y).height;

                getControlPoint(-1,y).stratumIndex=
                    getControlPoint(   0,y).stratumIndex*2-getControlPoint(   1,y).stratumIndex;
                getControlPoint(sx,y).stratumIndex=
                    getControlPoint(sx-1,y).stratumIndex*2-getControlPoint(sx-2,y).stratumIndex;
            }
            getControlPoint(-1,-1).height=getControlPoint(   0,   0).height*4-getControlPoint(   1,   0).height*2-getControlPoint(   0,   1).height*2+getControlPoint(   1,   1).height;
            getControlPoint(sx,-1).height=getControlPoint(sx-1,   0).height*4-getControlPoint(sx-2,   0).height*2-getControlPoint(sx-1,   1).height*2+getControlPoint(sx-2,   1).height;
            getControlPoint(-1,sy).height=getControlPoint(   0,sy-1).height*4-getControlPoint(   1,sy-1).height*2-getControlPoint(   0,sy-2).height*2+getControlPoint(   1,sy-2).height;
            getControlPoint(sx,sy).height=getControlPoint(sx-1,sy-1).height*4-getControlPoint(sx-2,sy-1).height*2-getControlPoint(sx-1,sy-2).height*2+getControlPoint(sx-2,sy-2).height;

            getControlPoint(-1,-1).stratumIndex=
                getControlPoint(   0,   0).stratumIndex*4-getControlPoint(   1,   0).stratumIndex*2-
                getControlPoint(   0,   1).stratumIndex*2+getControlPoint(   1,   1).stratumIndex;
            getControlPoint(sx,-1).stratumIndex=
                getControlPoint(sx-1,   0).stratumIndex*4-getControlPoint(sx-2,   0).stratumIndex*2-
                getControlPoint(sx-1,   1).stratumIndex*2+getControlPoint(sx-2,   1).stratumIndex;
            getControlPoint(-1,sy).stratumIndex=
                getControlPoint(   0,sy-1).stratumIndex*4-getControlPoint(   1,sy-1).stratumIndex*2-
                getControlPoint(   0,sy-2).stratumIndex*2+getControlPoint(   1,sy-2).stratumIndex;
            getControlPoint(sx,sy).stratumIndex=
                getControlPoint(sx-1,sy-1).stratumIndex*4-getControlPoint(sx-2,sy-1).stratumIndex*2-
                getControlPoint(sx-1,sy-2).stratumIndex*2+getControlPoint(sx-2,sy-2).stratumIndex;

            //It's alright to access values at sx and sy since controlPoints contains a border.
            for(x=0;x<sx;x++)
                for(y=0;y<sy;y++)
                    getControlPoint(x,y).tileUnderwater=
                        getControlPoint(x,y).underwater||
                        getControlPoint(x+1,y).underwater||
                        getControlPoint(x,y+1).underwater||
                        getControlPoint(x+1,y+1).underwater;

            for(x=0;x<=controlSizeX;x++)
                for(y=0;y<=controlSizeY;y++)
                {
                    tControlPoint& node=getControlPoint(x,y);
                    tVector2D v={x*controlScaleX, y*controlScaleY};
                    tTileInfo ti=getTileInfo(v,DOMAIN_NONE,SEA_FLOOR_DEPTH);

                    if(node.height<minElevation)
                        minElevation=node.height;
                    if(node.height>maxElevation)
                        maxElevation=node.height;
                    totalHeight+=node.height;
                }
            averageElevation=totalHeight/controlSizeX/controlSizeY;

            cachefp1=cache1.writeToCache(CACHE_PATH "ctrlpts.dat");
            if(cachefp1)
            {
                fwrite(controlPoints,sizeof(tControlPoint),(controlSizeX+2)*(controlSizeY+2),cachefp1);
                fwrite(&minElevation,sizeof(minElevation),1,cachefp1);
                fwrite(&maxElevation,sizeof(maxElevation),1,cachefp1);
                fwrite(&averageElevation,sizeof(averageElevation),1,cachefp1);
                fclose(cachefp1);
                cachefp1=NULL;
            }
        }

        cachefp2=cache2.readFromCache(CACHE_PATH "mesh.dat");
        if(cachefp2)
        {
            fread(&l,sizeof(l),1,cachefp2);
            count=(int)l;
            meshVertices.assign(count,mv);
            fread(&meshVertices[0],sizeof(mv),count,cachefp2);
            readMeshFromFile(cachefp2);
            fclose(cachefp2);
            cachefp2=NULL;
        }
        else
        {
            buildMesh();

            cachefp2=cache2.writeToCache(CACHE_PATH "mesh.dat");
            if(cachefp2)
            {
                l=(long)meshVertices.size();
                fwrite(&l,sizeof(l),1,cachefp2);
                fwrite(&meshVertices[0],sizeof(tMeshVertex),meshVertices.size(),cachefp2);
                writeMeshToFile(cachefp2);
                fclose(cachefp2);
                cachefp2=NULL;
            }
        }

        waveRadius=sqrt(waveScaleX*waveScaleX/4+waveScaleY*waveScaleY);
    }
    catch(const tString& s)
    {
        if(fp)
        {
            fclose(fp);
            fp=NULL;
        }
        destroy();
        throw s;
    }

    if(STRLEN(waterFilename)>0)
        glGenTextures(1, &waterTexture);
    if(STRLEN(waveFilename)>0)
        glGenTextures(1, &waveTexture);
    glGenTextures(1, &minimapTexture);
    glGenTextures(1, &shadeTexture);
    glGenTextures(1, &waterMapTexture);
    if(texture3DSupported)
        glGenTextures(1, &stratumTexture3D);
    else
        for(i=0;i<strata.size();i++)
            glGenTextures(1, &strata[i].texture);

    for(i=0;i<strata.size();i++)
        glGenTextures(1, &strata[i].alphaMap);

    refresh();

    loadRandomDecorations();
}
void tTerrain::parseStratumBlock(tStream& s) throw(tString)
{
    tString filename;
    tStratum d;

    d.scaleX=mapWidth;
    d.scaleY=mapHeight;
    d.blend=true;
    
	if(s.doesBlockHaveName())
	{
		d.name=parseIdentifier("name",s);
		if(stratumLookup(d.name,/*exact=*/true)>=0)
			throw tString("A stratum by the name of '")+d.name+"' already exists";
	}

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"texture"))
			{
				filename=parseString("texture filename",s);
				d.filename=tString(TERRAIN_PATH)+filename;
			}
			else if(strPrefix(str,"scale_x"))
				d.scaleX=parseFloatMin("texture scale x",0.01,s);
			else if(strPrefix(str,"scale_y"))
				d.scaleY=parseFloatMin("texture scale y",0.01,s);
			else if(strPrefix(str,"blend"))
				d.blend=parseBool("blend",s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

    strata.push_back(d);
}
void tTerrain::parseSplineSurfaceBlock(tStream& s) throw(tString)
{
    tSplineSurface ss;
    ss.target=SPLINE_SURFACE_NONE;
    ss.targetIndex=0;
    ss.influenceFlightPath=false;

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str, "waveform"))
				ss.waveforms.push_back(parseWaveformBlock(s));
			else if(strPrefix(str, "influence_flight_path"))
				ss.influenceFlightPath=parseBool("influence flight path",s);
			else if(strPrefix(str, "target"))
			{
				ss.target=parseSplineSurfaceTarget("spline surface target",s);
				if(ss.target==SPLINE_SURFACE_BUFFER)
					ss.targetIndex=parseIntRange("buffer index",0,MAX_BUFFERS-1,s);
				else if(ss.target==SPLINE_SURFACE_DECORATION_DENSITY)
					ss.targetIndex=parseDecorationType("decoration type",s);
			}
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

    splineSurfaces.push_back(ss);
}
void tTerrain::parseDecorationTypeBlock(tStream& s) throw(tString)
{
    tDecorationType dt;
    dt.modelType=NULL;
    dt.densityData=NULL;

	if(s.doesBlockHaveName())
	{
		dt.name=parseIdentifier("name",s);
		if(decorationTypeLookup(dt.name,/*exact=*/true)>=0)
			throw tString("A decoration type by the name of '")+dt.name+"' already exists";
	}
    
    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str, "model_type"))
				dt.modelType=parseModelType("decoration model type",s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

    decorationTypes.push_back(dt);
}

tWaveform tTerrain::parseWaveformBlock(tStream& s) throw(tString)
{
    tWaveform w;
    w.source=WAVEFORM_NONE;
    w.sourceIndex=0;
    w.wavelength=1;
    w.amplitude=1;
    w.bias=0;
    w.data=NULL;

    if(s.beginBlock())
    {                 
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str, "source"))
			{
				w.source=parseWaveformSource("source",s);
				if(w.source==WAVEFORM_BUFFER)
					w.sourceIndex=parseIntRange("buffer index",0,MAX_BUFFERS-1,s);
				else if(w.source==WAVEFORM_DECORATION_DENSITY)
				{
					s.matchToken("of");
					w.sourceIndex=parseDecorationType("decoration type",s);
				}
			}
			else if(strPrefix(str, "wavelength"))
				w.wavelength=parseFloatMin("wavelength",1,s);
			else if(strPrefix(str, "map"))
			{
				tMapping m;
				m.from=parseMappingValue("value to map from",s);
				s.matchToken("to");
				m.to=parseMappingValue("value to map to",s);
				while(s.hasOptionalParameter())
				{
					tString option=parseString("options",s);
					if(strPrefix(option,"smooth"))
						m.isSmooth=true;
					else if(strPrefix(option,"sharp"))
						m.isSmooth=false;
					else if(strPrefix(option,"closed"))
						m.isClosed=true;
					else if(strPrefix(option,"open"))
						m.isClosed=false;
					else
						throw tString("Please write the word 'smooth', 'sharp', 'closed', or 'open' at the end of the mapping.");
				}
	
				if(w.mappings.size()>0 && m.from<w.mappings.back().from)
					throw tString("You must enter non-decreasing values to map from (e.g., 1, 2, 2, 3, etc.)");
				w.mappings.push_back(m);
			}
			else if(strPrefix(str, "amplitude"))
				w.amplitude=parseFloat("amplitude",s);
			else if(strPrefix(str, "bias"))
				w.bias=parseFloat("bias",s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

    if(w.mappings.size()>0)
        if(w.mappings.front().isSmooth || w.mappings.back().isSmooth)
            throw tString("The first and last mappings cannot be smooth");

    return w;
}
int tTerrain::stratumLookup(const tString& name, bool exact)
{
    int i;
    tString n=STRTOLOWER(name);
    for(i=0; i<strata.size(); i++)
        if(STREQUAL(strata[i].name,n))
            return i;
    if(exact)
        return -1;
    for(i=0; i<strata.size(); i++)
        if(STRPREFIX(n,strata[i].name)) //STRPREFIX is case-insensitive
            return i;
    return -1;
}
int tTerrain::decorationTypeLookup(const tString& name, bool exact)
{
    int i;
    tString n=STRTOLOWER(name);
    for(i=0; i<decorationTypes.size(); i++)
        if(STREQUAL(decorationTypes[i].name,n))
            return i;
    if(exact)
        return -1;
    for(i=0; i<decorationTypes.size(); i++)
        if(STRPREFIX(n,decorationTypes[i].name)) //STRPREFIX is case-insensitive
            return i;
    return -1;
}
float tTerrain::parseMappingValue(const char *fieldName, tStream& s) throw(tString)
{
    tString str=s.peekAhead();
    if(STRLEN(str)>0 && isalpha(str[0]) || str[0]=='_')
    {
        parseString(fieldName, s);
        int index=stratumLookup(str,/*exact=*/false);
        if(index<0)
            throw tString("Unrecognized stratum '")+str+"'";
        return (float)(index+1);
    }
    return parseFloat(fieldName,s);
}
int tTerrain::parseDecorationType(const char *fieldName, tStream& s) throw(tString)
{
    int index;
    tString str=parseString(fieldName, s);
    index=decorationTypeLookup(str,/*exact=*/false);
    if(index<0)
        throw tString("Unrecognized decoration type '")+str+"'";
    return index;
}

void tTerrain::refresh()
{
    iImageIndex imageName;
    int i, stratumWidth=0, stratumHeight=0;
    int x,y;
    unsigned char *minimapData = NULL, *minimapPtr, *texturePtr;
    int stratumTexture3DHeight=nearestPowerOf2(strata.size());
    unsigned char *stratumData=NULL;
    float alpha,sx,sy;
    tTileInfo ti;
    tVector2D v;
    tCache minimapCache;
    FILE *minimapfp=NULL;
    bool minimapInCache=false;

    if(minimapTexture>0)
    {
        minimapData = mwNew unsigned char[128 * 128 * 3];

        minimapCache.addStringToHeader(" Machinations Minimap Cache ");
        addControlPointVariablesToCacheHeader(minimapCache);
        minimapfp=minimapCache.readFromCache(CACHE_PATH "minimap.dat");
        if(minimapfp)
        {
            fread(minimapData,sizeof(*minimapData),128 * 128 * 3,minimapfp);
            fclose(minimapfp);
            minimapfp=NULL;
            minimapInCache=true;
        }
        else
            memset((void*)minimapData,0,128 * 128 * 3);
    }

    for(i=0;i<strata.size();i++)
    {
        ilGenImages(1, &imageName);
        ilBindImage(imageName);
        if(ilLoadImage((char*)CHARPTR(strata[i].filename))==IL_FALSE) //DevIL hack
        {
            bug("tTerrain::refresh: failed to load %s",CHARPTR(strata[i].filename));
            //There's not much we can do, short of throwing an exception, so we just
            //ignore the error.  The terrain will no longer have a texture, but the game
            //won't crash.
        }

        if(stratumTexture3D>0)
        {
            if(stratumWidth==0)
            {
                stratumWidth=ilGetInteger(IL_IMAGE_WIDTH);
                stratumHeight=ilGetInteger(IL_IMAGE_HEIGHT);
                stratumData=mwNew unsigned char[stratumWidth*stratumHeight*3*stratumTexture3DHeight];
            }
            else
                iluScale(stratumWidth,stratumHeight, /*depth=*/ 1);

            ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE);
            texturePtr=ilGetData();
            if(stratumData && texturePtr)
                memcpy(stratumData+stratumWidth*stratumHeight*3*i,texturePtr,stratumWidth*stratumHeight*3);
        }
        else
        {
            glEnable(GL_TEXTURE_2D);
            mglBindTexture(strata[i].texture);
            ilutGLTexImage(0);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glDisable(GL_TEXTURE_2D);
        }

        if(minimapTexture>0 && !minimapInCache)
        {
            //Shrink texture to 128x128 for minimap:
            iluScale(128,128, /*depth=*/ 1);
            ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE);

            minimapPtr=minimapData;
            texturePtr=ilGetData();

            sx=mapWidth/(128-1);
            sy=mapHeight/(128-1);

            for(y=0;y<128;y++)
                for(x=0;x<128;x++)
                {
                    v.x=(int)(x*sx);
                    v.y=(int)(y*sy);

                    ti=getTileInfo(v,DOMAIN_NONE,SEA_FLOOR_DEPTH);
                    alpha=ti.stratumIndex-(float)i;
                    clampRange(alpha,0.0f,1.0f);

                    minimapPtr[0]=(unsigned char)(minimapPtr[0]*(1-alpha) + texturePtr[0]/2*alpha);
                    minimapPtr[1]=(unsigned char)(minimapPtr[1]*(1-alpha) + texturePtr[1]/2*alpha);
                    minimapPtr[2]=(unsigned char)(minimapPtr[2]*(1-alpha) + texturePtr[2]/2*alpha);
                    minimapPtr+=3;
                    texturePtr+=3;
                }
        }

        ilDeleteImages(1, &imageName);
    }

    if(stratumTexture3D>0)
    {
        glEnable(GL_TEXTURE_3D);
        glBindTexture(GL_TEXTURE_3D,stratumTexture3D);
        _glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB16, stratumWidth, stratumHeight, stratumTexture3DHeight, 0,
            GL_RGB, GL_UNSIGNED_BYTE, stratumData);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP);
        mwDelete[] stratumData;
        stratumData=NULL;
        glDisable(GL_TEXTURE_3D);
    }

    if(minimapTexture>0)
    {
        if(!minimapInCache)
        {
            applyWater(minimapData, 128, 128, /*bytes per pixel=*/ 3);

            minimapfp=minimapCache.writeToCache(CACHE_PATH "minimap.dat");
            if(minimapfp)
            {
                fwrite(minimapData,sizeof(*minimapData),128 * 128 * 3,minimapfp);
                fclose(minimapfp);
                minimapfp=NULL;
            }
        }

        glEnable(GL_TEXTURE_2D);
        mglBindTexture(minimapTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16, 128, 128, 0, GL_RGB, GL_UNSIGNED_BYTE, minimapData);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

        mwDelete[] minimapData;
        minimapData=NULL;
        glDisable(GL_TEXTURE_2D);
    }

    if(waterTexture>0)
    {
        ilGenImages(1, &imageName);
        ilBindImage(imageName);
        if(ilLoadImage((char*)CHARPTR(waterFilename))==IL_FALSE) //DevIL hack
            bug("tTerrain::refresh: failed to load %s",CHARPTR(waterFilename));
        glEnable(GL_TEXTURE_2D);
        mglBindTexture(waterTexture);
        ilutEnable(ILUT_OPENGL_CONV); //Use GL_RGB8 for this texture (e.g. 8-bits for reg, green, and blue)
        ilutGLTexImage(0);            //for better resolution.
        ilutDisable(ILUT_OPENGL_CONV);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glDisable(GL_TEXTURE_2D);
        ilDeleteImages(1, &imageName);
    }
    if(waveTexture>0)
    {
        ilGenImages(1, &imageName);
        ilBindImage(imageName);
        if(ilLoadImage((char*)CHARPTR(waveFilename))==IL_FALSE) //DevIL hack
            bug("tTerrain::refresh: failed to load %s",CHARPTR(waveFilename));
        glEnable(GL_TEXTURE_2D);
        mglBindTexture(waveTexture);
        ilutGLTexImage(0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glDisable(GL_TEXTURE_2D);
        ilDeleteImages(1, &imageName);
    }

    if(shadeMapSize>0)
        refreshShadeMap(shadeMapSize);
    if(alphaMapSize>0)
        refreshAlphaMaps(alphaMapSize);
    if(waterMapSize>0)
        refreshWaterMap(waterMapSize);
}
void tTerrain::refreshShadeMap(int size)
{
    float shadef;
    int shadei,x,y;
    tTileInfo ti;
    char filename[20];
    FILE *cachefp=NULL;

    if(shadeTexture>0)
    {
        unsigned char *shadingData=mwNew unsigned char[size*size];

        tCache cache;
        cache.addStringToHeader(" Machinations Shade Map Cache ");
        addControlPointVariablesToCacheHeader(cache);
        addLightsToCacheHeader(cache);
        cache.addFloatToHeader(shadeMapScale);
        cache.addFloatToHeader(shadeMapBias);
        cache.addFloatToHeader(shadeMapMin);
        cache.addFloatToHeader(shadeMapMax);

        snprintf(filename,20,CACHE_PATH "shade%d.dat",size);

        cachefp=cache.readFromCache(filename);
        if(cachefp)
        {
            fread(shadingData,sizeof(*shadingData),size*size,cachefp);
            fclose(cachefp);
            cachefp=NULL;
        }
        else
        {
            displayProgressBar("Building shade map...  Please Wait.");
            invalidateScreen();
            float scaleX=(float)mapWidth/size;
            float scaleY=(float)mapHeight/size;
            for(x=0;x<size;x++)
            {
                for(y=0;y<size;y++)
                {
                    tVector3D v={(float)x*scaleX, (float)y*scaleY};
                    ti=getTileInfo(to2D(v),DOMAIN_NONE,SEA_FLOOR_DEPTH);
                    v.z=ti.height;
                    shadef=calculateShade(v,ti.normal)*shadeMapScale+shadeMapBias;
                    clampRange(shadef,shadeMapMin,shadeMapMax);
                    shadei=(int)(shadef*256);
                    clampRange(shadei,0,255);
                    shadingData[y*size+x]=(unsigned char)shadei;
                }
                if(x%16==0)
                {
                    updateProgressBar((float)x/size);
                    invalidateScreen();
                }
            }
            updateProgressBar(1);
            invalidateScreen();
            closeProgressBar();

            cachefp=cache.writeToCache(filename);
            if(cachefp)
            {
                fwrite(shadingData,sizeof(*shadingData),size*size,cachefp);
                fclose(cachefp);
                cachefp=NULL;
            }
        }

        glEnable(GL_TEXTURE_2D);
        mglBindTexture(shadeTexture);
        glTexImage2D(GL_TEXTURE_2D,0,GL_LUMINANCE8,size,size,0,GL_LUMINANCE,GL_UNSIGNED_BYTE,shadingData);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glDisable(GL_TEXTURE_2D);
        mwDelete[] shadingData;
    }
}
void tTerrain::refreshAlphaMaps(int size)
{
    int i,x,y,val;
    tTileInfo ti;
    float index;
    char filename[20];
    FILE *cachefp=NULL;
    
    unsigned short *stratumData=mwNew unsigned short[size*size];

    float scaleX=(float)mapWidth/size;
    float scaleY=(float)mapHeight/size;

    tCache cache;
    cache.addStringToHeader(" Machinations Alpha Map Cache ");
    addControlPointVariablesToCacheHeader(cache);
    snprintf(filename,20,CACHE_PATH "alpha%d.dat",size);

    cachefp=cache.readFromCache(filename);
    if(cachefp)
    {
        fread(stratumData,sizeof(*stratumData),size*size,cachefp);
        fclose(cachefp);
        cachefp=NULL;
    }
    else
    {
        displayProgressBar("Building alpha maps...  Please Wait.");
        invalidateScreen();
        for(x=0;x<size;x++)
        {
            for(y=0;y<size;y++)
            {
                tVector2D v={(float)x*scaleX, (float)y*scaleY};
                ti=getTileInfo(v,DOMAIN_NONE,SEA_FLOOR_DEPTH);
                index=ti.stratumIndex;
                val=(int)(index*(USHRT_MAX/strata.size()));
                clampRange(val,0,(int)(USHRT_MAX-1));

                for(i=0;i<strata.size();i++)
                    stratumData[y*size+x]=(unsigned short)val;
            }
            if(x%16==0)
            {
                updateProgressBar((float)x/size);
                invalidateScreen();
            }
        }
        updateProgressBar(1);
        invalidateScreen();
        closeProgressBar();

        cachefp=cache.writeToCache(filename);
        if(cachefp)
        {
            fwrite(stratumData,sizeof(*stratumData),size*size,cachefp);
            fclose(cachefp);
            cachefp=NULL;
        }
    }

    for(i=0;i<strata.size();i++)
        if(strata[i].alphaMap>0)
        {
            glEnable(GL_TEXTURE_2D);
            mglBindTexture(strata[i].alphaMap);
            glPixelTransferf(GL_ALPHA_SCALE,(float)strata.size());
            glPixelTransferf(GL_ALPHA_BIAS,-(float)i);
            glTexImage2D(GL_TEXTURE_2D,0,GL_ALPHA4,size,size,0,GL_ALPHA,GL_UNSIGNED_SHORT,stratumData);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
            glDisable(GL_TEXTURE_2D);
        }
    glPixelTransferf(GL_ALPHA_SCALE,1);
    glPixelTransferf(GL_ALPHA_BIAS,0);

    mwDelete[] stratumData;
}
void tTerrain::refreshWaterMap(int size)
{
    int x,y;
    tTileInfo ti;
    float shadef;
    int shadei;
    char filename[20];
    FILE *cachefp=NULL;

    if(waterMapTexture>0)
    {
        unsigned char *waterData=mwNew unsigned char[size*size];

        tCache cache;
        cache.addStringToHeader(" Machinations Water Map Cache ");
        addControlPointVariablesToCacheHeader(cache);
        cache.addFloatToHeader(seaAlphaScale);
        cache.addFloatToHeader(seaAlphaBias);
        cache.addFloatToHeader(seaAlphaMin);
        cache.addFloatToHeader(seaAlphaMax);

        snprintf(filename,20,CACHE_PATH "water%d.dat",size);

        cachefp=cache.readFromCache(filename);
        if(cachefp)
        {
            fread(waterData,sizeof(*waterData),size*size,cachefp);
            fclose(cachefp);
            cachefp=NULL;
        }
        else
        {
            displayProgressBar("Building water map...  Please Wait.");
            invalidateScreen();
            float scaleX=(float)mapWidth/size;
            float scaleY=(float)mapHeight/size;
            for(x=0;x<size;x++)
            {
                for(y=0;y<size;y++)
                {
                    tVector2D v={(float)x*scaleX, (float)y*scaleY};
                    ti=getTileInfo(v,DOMAIN_NONE,SEA_FLOOR_DEPTH);
                    shadef=ti.height*seaAlphaScale + seaAlphaBias;
                    clampRange(shadef,seaAlphaMin,seaAlphaMax);
                    shadei=(int)(shadef*256);
                    clampRange(shadei,0,255);
                    waterData[y*size+x]=(unsigned char)shadei;
                }
                if(x%16==0)
                {
                    updateProgressBar((float)x/size);
                    invalidateScreen();
                }
            }
            updateProgressBar(1);
            invalidateScreen();
            closeProgressBar();

            cachefp=cache.writeToCache(filename);
            if(cachefp)
            {
                fwrite(waterData,sizeof(*waterData),size*size,cachefp);
                fclose(cachefp);
                cachefp=NULL;
            }
        }

        glEnable(GL_TEXTURE_2D);
        mglBindTexture(waterMapTexture);
        glTexImage2D(GL_TEXTURE_2D,0,GL_ALPHA8,size,size,0,GL_ALPHA,GL_UNSIGNED_BYTE,waterData);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glDisable(GL_TEXTURE_2D);
        mwDelete[] waterData;
    }
}
void tTerrain::applyWater(unsigned char *data, int width, int height, int bpp)
{
    int x,y;
    unsigned char *ptr=data;
    tVector2D position;
    float scaleX=(float)mapWidth/(width-1);
    float scaleY=(float)mapHeight/(height-1);
    float alpha;
    tTileInfo ti;

#ifdef DEBUG
    if(MEMORY_VIOLATION(data))
        return;
#endif
    for(y=0;y<height;y++)
        for(x=0;x<width;x++)
        {
            position.x=x*scaleX;
            position.y=y*scaleY;
            ti=getTileInfo(position, DOMAIN_NONE, SEA_FLOOR_DEPTH);
            if(ti.underwater)
            {
                alpha=(-ti.height+.3)/2;
                clampRange(alpha,0.0f,1.0f);

                ptr[0]=(unsigned char)(ptr[0]*(1-alpha));
                ptr[1]=(unsigned char)(ptr[1]*(1-alpha));
                ptr[2]=(unsigned char)(ptr[2]*(1-alpha) + 255*alpha);
            }
            ptr+=bpp;
        }
}

tTerrain::~tTerrain()
{
	destroy();
}

void tTerrain::destroy()
{
    int i;
    tMeshTriangleEx *ex,*next;
    if(minimapTexture>0)
    {
        glDeleteTextures(1,&minimapTexture);
        minimapTexture=0;
    }
    if(controlPoints!=NULL)
    {
        mwDelete[] controlPoints;
        controlPoints=NULL;
    }
    for(i=0;i<strata.size();i++)
    {
        if(strata[i].texture>0)
        {
            glDeleteTextures(1,&strata[i].texture);
            strata[i].texture=0;
        }
        if(strata[i].alphaMap>0)
        {
            glDeleteTextures(1,&strata[i].alphaMap);
            strata[i].alphaMap=0;
        }
        strata[i].solidTriangles.clear();
        strata[i].translucentTriangles.clear();
        strata[i].trimTriangles.clear();
    }
    if(waterTexture>0)
    {
        glDeleteTextures(1,&waterTexture);
        waterTexture=0;
    }
    if(waveTexture>0)
    {
        glDeleteTextures(1,&waveTexture);
        waveTexture=0;
    }
    if(shadeTexture>0)
    {
        glDeleteTextures(1,&shadeTexture);
        shadeTexture=0;
    }
    if(waterMapTexture>0)
    {
        glDeleteTextures(1,&waterMapTexture);
        waterMapTexture=0;
    }
    shorePoints.clear();

    meshEdges.clear();
    for(i=0;i<meshTriangles.size();i++)
    {
        ex=meshTriangles[i]->ex;
        while(ex)
        {
            next=ex->next;
            mwDelete ex;
            ex=next;
        }
    }
    if(meshTriangleBlock) //We allocated a huge block of memory for the mesh
        mwDelete[] meshTriangleBlock;
    else
        for(i=0;i<meshTriangles.size();i++)
            mwDelete meshTriangles[i];

    meshTriangles.clear();
    mesh.ancestor1=NULL;
    mesh.ancestor2=NULL;

    meshVertices.clear();

    if(stratumTexture3D>0)
    {
        glDeleteTextures(1,&stratumTexture3D);
        stratumTexture3D=0;
    }

    for(i=0;i<decorationTypes.size();i++)
    {
        if(decorationTypes[i].densityData)
            mwDelete[] decorationTypes[i].densityData;
        decorationTypes[i].decorations.clear();
    }
    decorationTypes.clear();
}

float tTerrain::getAlpha(int x, int y)
{
    float a;
    tControlPoint& node = getControlPoint(x,y);

    a=seaAlphaScale*node.height+seaAlphaBias;
    clampRange(a,0.0f,1.0f);
    return a;
}

void tTerrain::drawMinimap()
{
    float xf=mapWidth;
    float yf=mapHeight;

    glEnable(GL_TEXTURE_2D);
    mglBindTexture(minimapTexture);

    glBegin(GL_QUADS);
        glTexCoord2f(0,0);
        glVertex2f(0,0);
        glTexCoord2f(1,0);
        glVertex2f(xf,0);
        glTexCoord2f(1,1);
        glVertex2f(xf,yf);
        glTexCoord2f(0,1);
        glVertex2f(0,yf);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

float tTerrain::getBase(int x, int y, eDomain domain, float verticalOffset)
{
    float l;
    tControlPoint& node=getControlPoint(x,y);
    if(domain==DOMAIN_AIR)
        l=node.smoothHeight;
    else
        l=node.height;
    if((domain==DOMAIN_SEA || domain==DOMAIN_AMPHIBIOUS) && node.tileUnderwater)
        return MAX3(seaLevel+verticalOffset,l+verticalOffset,l); //bob
    else
        return MAX3(verticalOffset,l+verticalOffset,l);   //don't bob
}

tTileInfo tTerrain::getTileInfo(tVector2D position, eDomain domain, float verticalOffset)
{
    int x,y;
    tTileInfo info={0,0,{0,0,0},false};
    tVector3D normal;
    float a,b,h,height=0,zu=0,zv=0;
    float s,stratumIndex=0;
    int q,r;
    float *tableA,*tableB,*tableC,*tableD;

    //Determine which tile the position falls in:
    a=position.x/controlScaleX;
    b=position.y/controlScaleY;

    x=(int)a;
    y=(int)b;

    //Clamp x and y:
    clampRange(x,0,controlSizeX-2);
    clampRange(y,0,controlSizeY-2);

    //Find fractional offset within tile:
    a-=(float)x;
    b-=(float)y;
    clampRange(a,0.0f,1.0f);
    clampRange(b,0.0f,1.0f);

    tableA=lookupSplineArray(a);
    tableB=lookupSplineArray(b);
    tableC=lookupSplineDerivArray(a);
    tableD=lookupSplineDerivArray(b);

    for(q=0;q<4;q++)
        for(r=0;r<4;r++)
        {
            h=getBase(x+q-1,y+r-1,domain,verticalOffset);
            s=getControlPoint(x+q-1,y+r-1).stratumIndex;
            height+=h*tableA[q]*tableB[r];
            stratumIndex+=s*tableA[q]*tableB[r];
            zu+=h*tableC[q]*tableB[r];
            zv+=h*tableA[q]*tableD[r];
        }

    tVector3D du={1, 0, zu};
    tVector3D dv={0, 1, zv};
    normal=du.cross(dv);

    info.height=height;
    info.stratumIndex=stratumIndex;
    if(domain==DOMAIN_AIR || domain==DOMAIN_ANY)
        info.normal=kHat3D;
    else
        info.normal=normal.normalize();
    if(height<maxSeaElevation && getControlPoint(x,y).tileUnderwater)
        info.underwater=true;
    return info;
}

void tTerrain::tick(tWorldTime t)
{
    int     i;
    float   ratio,ratio2;

    seaLevel=minSeaElevation+(sin(t/2)+1)*(maxSeaElevation-minSeaElevation)/2;

    if(waveTexture>0)
    {
        for(i=0;i<shorePoints.size();i++)
        {
            tShorePoint& sp=shorePoints[i];
            if(sp.startTime<0)
                continue;

            ratio=(t-sp.startTime)/waveDuration;
            if(ratio>1)
            {
                sp.startTime=-1;
                continue;
            }

            if(ratio<0.333333)
                ratio2=1 - ratio*2;
            else
                ratio2=1 - (-4.5*ratio*ratio + 6*ratio - .5)/1.5;
            if(ratio>0.666666)
                ratio2=ratio2/4;
            sp.pos=sp.v - sp.gradient*(waveLength*ratio2);
            sp.length=waveScaleY*ratio*3;

            sp.alpha=MIN(1.5*(1-ratio),1);
        }
    }
}

void tTerrain::intervalTick(tWorldTime t)
{
    int i;

    if(waveTexture>0)
    {
        //How many waves do we need to spawn during this interval?
        int count=(int)(shorePoints.size()*waveFrequency*gameInterval/1000);

        for(i=0;i<count;i++)
        {
            tShorePoint& sp=shorePoints[rand()%shorePoints.size()];
            if(sp.startTime>0)
                continue;

            sp.startTime=t;
        }
    }
}

void tTerrain::drawNormals()
{
    int i;
    int size=meshVertices.size();
    tVector3D v;
    glBegin(GL_LINES);
    for(i=0;i<size;i++)
    {
        v=meshVertices[i].point;
        glVertex3fv((float*)&v);
        v+=meshVertices[i].normal;
        glVertex3fv((float*)&v);
    }
    glEnd();
}

void tTerrain::draw()
//Vertices are passed in this order with respect to the screen:
//{lower-left, lower-right, upper-right, upper-left}
{
    int i, textureIndex;
    bool genColors, genNormals;

    const float sPlane[4]={shadowTextureMatrix[0],
                            shadowTextureMatrix[4],
                            shadowTextureMatrix[8],
                            shadowTextureMatrix[12]};
    const float tPlane[4]={shadowTextureMatrix[1],
                            shadowTextureMatrix[5],
                            shadowTextureMatrix[9],
                            shadowTextureMatrix[13]};
    const float qPlane[4]={shadowTextureMatrix[3],
                            shadowTextureMatrix[7],
                            shadowTextureMatrix[11],
                            shadowTextureMatrix[15]};

    float sx=1,sy=1;
    if(mapWidth>0)
        sx=1/mapWidth;
    if(mapHeight>0)
        sy=1/mapHeight;

    const float mapScaleS[4]={sx,0,0,0};
    const float mapScaleT[4]={0,sy,0,0};

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
    glColor3f(1,1,1);

    if(strata.size()>0)
        i=0;
    else
        i=-1;

    for(;i<strata.size();i++)
    {
        textureIndex=0;
        if(i>=0 && strata[i].texture>0)
        {
            float sx=1,sy=1;
            if(strata[i].scaleX>0)
                sx=1/strata[i].scaleX;
            if(strata[i].scaleY>0)
                sy=1/strata[i].scaleY;
            const float scaleS[4]={sx,0,0,0};
            const float scaleT[4]={0,sy,0,0};

            mglSetActiveTexture(textureIndex++);
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, strata[i].texture);
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT);
            glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE);
            glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PREVIOUS_EXT);
            glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_MODULATE);
            glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_TEXTURE);
            glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_PREVIOUS_EXT);

            glEnable(GL_TEXTURE_GEN_S);
            glTexGeni(GL_S,GL_TEXTURE_GEN_MODE,GL_OBJECT_LINEAR);
            glTexGenfv(GL_S,GL_OBJECT_PLANE,scaleS);
            glEnable(GL_TEXTURE_GEN_T);
            glTexGeni(GL_T,GL_TEXTURE_GEN_MODE,GL_OBJECT_LINEAR);
            glTexGenfv(GL_T,GL_OBJECT_PLANE,scaleT);

            if(strata[i].blend && alphaMapSize>0 && strata[i].alphaMap>0)
            {
                mglSetActiveTexture(textureIndex++);
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, strata[i].alphaMap);
                glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT);
                glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE);
                glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_TEXTURE);
                glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_REPLACE);
                glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT);

                glEnable(GL_TEXTURE_GEN_S);
                glTexGeni(GL_S,GL_TEXTURE_GEN_MODE,GL_OBJECT_LINEAR);
                glTexGenfv(GL_S,GL_OBJECT_PLANE,mapScaleS);
                glEnable(GL_TEXTURE_GEN_T);
                glTexGeni(GL_T,GL_TEXTURE_GEN_MODE,GL_OBJECT_LINEAR);
                glTexGenfv(GL_T,GL_OBJECT_PLANE,mapScaleT);

                genColors=false;
            }
            else
                genColors=true;
        }
        else
            genColors=false;

        if(shadeMapSize>0 && shadeTexture>0)
        {
            mglSetActiveTexture(textureIndex++);
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, shadeTexture);
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT);
            glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE);
            glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PREVIOUS_EXT);
            glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_MODULATE);
            glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_TEXTURE);
            glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_PREVIOUS_EXT);

            glEnable(GL_TEXTURE_GEN_S);
            glTexGeni(GL_S,GL_TEXTURE_GEN_MODE,GL_OBJECT_LINEAR);
            glTexGenfv(GL_S,GL_OBJECT_PLANE,mapScaleS);
            glEnable(GL_TEXTURE_GEN_T);
            glTexGeni(GL_T,GL_TEXTURE_GEN_MODE,GL_OBJECT_LINEAR);
            glTexGenfv(GL_T,GL_OBJECT_PLANE,mapScaleT);

            genNormals=false;
        }
        else
        {
            glEnable(GL_LIGHTING);
            genNormals=true;
        }

        if(shadowMapSize>0 && shadowTexture>0)
        {
            mglSetActiveTexture(textureIndex++);
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, shadowTexture);
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT);
            glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE);
            glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PREVIOUS_EXT);
            glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_MODULATE);
            glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT);
            glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_TEXTURE);

            glEnable(GL_TEXTURE_GEN_S);
            glTexGeni(GL_S,GL_TEXTURE_GEN_MODE,GL_OBJECT_LINEAR);
            glTexGenfv(GL_S,GL_OBJECT_PLANE,sPlane);
            glEnable(GL_TEXTURE_GEN_T);
            glTexGeni(GL_T,GL_TEXTURE_GEN_MODE,GL_OBJECT_LINEAR);
            glTexGenfv(GL_T,GL_OBJECT_PLANE,tPlane);
            glEnable(GL_TEXTURE_GEN_Q);
            glTexGeni(GL_Q,GL_TEXTURE_GEN_MODE,GL_OBJECT_LINEAR);
            glTexGenfv(GL_Q,GL_OBJECT_PLANE,qPlane);
        }

        drawMesh(genColors, genNormals, i);

        for(textureIndex--;textureIndex>=0;textureIndex--)
        {
            mglSetActiveTexture(textureIndex);
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
            glDisable(GL_TEXTURE_2D);
            glDisable(GL_TEXTURE_GEN_S);
            glDisable(GL_TEXTURE_GEN_T);
            glDisable(GL_TEXTURE_GEN_R);
            glDisable(GL_TEXTURE_GEN_Q);
        }

        if(i<0)
            break;
    }

    glDisable(GL_CULL_FACE);
    glDisable(GL_LIGHTING);

    //This block of code draws a line indicating the normal at each vertex.
    //glColor3f(1,0,0);
    //drawNormals();
}

void tTerrain::postDraw()
{
    int i;
    tVector2D v2;

#ifdef DEBUG
    if(waterScaleX==0 || waterScaleY==0)
    {
        bug("tTerrain::postDraw: waterScaleX or waterScaleY is 0");
        return;
    }
#endif

    float tw=mapWidth/waterScaleX;
    float th=mapHeight/waterScaleY;

    START_GRAPHICS_TIMER(TIMER_DRAW_WATER)
    if(waterTexture>0)
    {
        glEnable(GL_BLEND);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glDepthMask(false);
        if(waterMapSize>0 && waterTexture>0 && waterMapTexture>0)
        {
            glColor4f(1,1,1,.5);

            mglSetActiveTexture(0);
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, waterTexture);
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT);
            glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE);
            glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_TEXTURE);
            glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_REPLACE);
            glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_TEXTURE);

            mglSetActiveTexture(1);
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, waterMapTexture);
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT);
            glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE);
            glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_TEXTURE);
            glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_REPLACE);
            glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT);

            float tw=mapWidth/waterScaleX;
            float th=mapHeight/waterScaleY;
            glBegin(GL_QUADS);
                _glMultiTexCoord2fARB(GL_TEXTURE0_ARB,0,0);
                _glMultiTexCoord2fARB(GL_TEXTURE1_ARB,0,0);
                glVertex3f(0,0,seaLevel);
                _glMultiTexCoord2fARB(GL_TEXTURE0_ARB,tw,0);
                _glMultiTexCoord2fARB(GL_TEXTURE1_ARB,1,0);
                glVertex3f(mapWidth,0,seaLevel);
                _glMultiTexCoord2fARB(GL_TEXTURE0_ARB,tw,th);
                _glMultiTexCoord2fARB(GL_TEXTURE1_ARB,1,1);
                glVertex3f(mapWidth,mapHeight,seaLevel);
                _glMultiTexCoord2fARB(GL_TEXTURE0_ARB,0,th);
                _glMultiTexCoord2fARB(GL_TEXTURE1_ARB,0,1);
                glVertex3f(0,mapHeight,seaLevel);
            glEnd();

            glDisable(GL_TEXTURE_2D);
            mglSetActiveTexture(0);
            glDisable(GL_TEXTURE_2D);
            glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

            glDepthMask(true);
        }
        else if(waterTexture>0)
        {
            glEnable(GL_TEXTURE_2D);
            mglBindTexture(waterTexture);
            glColor4f(1,1,1,0.5);

            glBegin(GL_QUADS);
                glTexCoord2f(0,0);
                glVertex3f(0,0,seaLevel);
                glTexCoord2f(tw,0);
                glVertex3f(mapWidth,0,seaLevel);
                glTexCoord2f(tw,th);
                glVertex3f(mapWidth,mapHeight,seaLevel);
                glTexCoord2f(0,th);
                glVertex3f(0,mapHeight,seaLevel);
            glEnd();

            glDisable(GL_TEXTURE_2D);
        }
        glDepthMask(true);
        glDisable(GL_BLEND);
        glDisable(GL_CULL_FACE);
    }
    STOP_GRAPHICS_TIMER(TIMER_DRAW_WATER)

    START_GRAPHICS_TIMER(TIMER_DRAW_WAVES)
    if(waveTexture>0)
    {
        glEnable(GL_TEXTURE_2D);
        mglBindTexture(waveTexture);
        glEnable(GL_BLEND);
        glDepthMask(false);

        glBegin(GL_QUADS);
        for(i=0;i<shorePoints.size();i++)
        {
            tShorePoint& sp=shorePoints[i];

            tVector3D center={sp.pos.x,sp.pos.y,seaLevel};
            if(sp.startTime<0 || !isWithinView(waveRadius,center))
                continue;

            glColor4f(1,1,1,sp.alpha);
            v2 = sp.pos + sp.gradient.perpendicularCCW()*waveScaleX/2;
            glTexCoord2f(0,1);
            glVertex3f(v2.x,v2.y,seaLevel);
            v2 += -sp.gradient*sp.length;
            glTexCoord2f(0,0);
            glVertex3f(v2.x,v2.y,seaLevel);
            v2 += sp.gradient.perpendicularCW()*waveScaleX;
            glTexCoord2f(1,0);
            glVertex3f(v2.x,v2.y,seaLevel);
            v2 += sp.gradient*sp.length;
            glTexCoord2f(1,1);
            glVertex3f(v2.x,v2.y,seaLevel);
        }
        glEnd();

        glDepthMask(true);
        glDisable(GL_BLEND);
        glDisable(GL_TEXTURE_2D);
    }
    STOP_GRAPHICS_TIMER(TIMER_DRAW_WAVES)
}

tVector3D tTerrain::pixelToTerrain3D(int x, int y, eDomain domain, float elevation)
{
    int i,j;
    const int intervals=1000;
    float scale=2.0/intervals;
    float z, last, left, right, center;
    tVector3D v;
    tTileInfo ti;
    for(i=0;i<=intervals;i++)
    {
        z=(i*scale-1);
        v=pixelToVector3D(x,y,z);

        //Determine the actual elevation of this point.  If the elevation is less
        //than the elevation of the point in question, we need to look further.
        ti=getTileInfo(to2D(v),domain,elevation);
        if(ti.height>v.z)
            break;

        last=z;
    }
    if(i>0 && i<intervals)
    {
        left=last;
        right=z;
        for(j=0;j<10;j++)
        {
            center=(left+right)/2;

            z=center;
            v=pixelToVector3D(x,y,z);

            ti=getTileInfo(to2D(v),domain,elevation);
            if(ti.height>v.z)
                right=center;
            else
                left=center;
        }
    }
    v.z=ti.height;
    return v;
}
void tTerrain::drawDecorations()
{
    glColor3f(1,1,1);

    glEnable(GL_LIGHTING);

    for(int i=0;i<decorationTypes.size();i++)
    {
        tDecorationType& dt=decorationTypes[i];
        if(dt.modelType)
        {
            dt.modelType->setupFastDraw();
            glEnable(GL_CULL_FACE);

            for(int j=0;j<dt.decorations.size();j++)
            {
                tDecoration& d=dt.decorations[j];
                if(isWithinView(dt.modelType->getRadius3D(), d.position))
                {
                    glPushMatrix();
                    glTranslatef(d.position.x,d.position.y,d.position.z);
                    //Translate this vector with forward3D, side3D, and up3D
                    GLfloat orientation[16]={d.forward.x,d.forward.y,d.forward.z,0,
                        d.side.x,d.side.y,d.side.z,0,
                        d.up.x,d.up.y,d.up.z,0, 0,0,0,1};
                    glMultMatrixf(orientation);

                    dt.modelType->fastDraw();

                    glPopMatrix();
                }
            }

            glDisable(GL_CULL_FACE);
            dt.modelType->cleanupFastDraw();
        }
    }

    glDisable(GL_LIGHTING);
}
void tTerrain::drawDecorationShadows()
{
    glColor3f(0,0,0);

    for(int i=0;i<decorationTypes.size();i++)
    {
        tDecorationType& dt=decorationTypes[i];
        if(dt.modelType)
        {
            dt.modelType->setupFastDraw();

            for(int j=0;j<dt.decorations.size();j++)
            {
                tDecoration& d=dt.decorations[j];

                if(isWithinView(dt.modelType->getRadius3D(),d.position))
                {
                    glPushMatrix();
                    glTranslatef(d.position.x,d.position.y,d.position.z);
                    //Translate this vector with forward3D, side3D, and up3D
                    GLfloat orientation[16]={d.forward.x,d.forward.y,d.forward.z,0,
                        d.side.x,d.side.y,d.side.z,0,
                        d.up.x,d.up.y,d.up.z,0, 0,0,0,1};
                    glMultMatrixf(orientation);

                    double ground[4]={0,0,1,0};
                    glClipPlane(GL_CLIP_PLANE0,ground);
                    dt.modelType->fastDraw();

                    glPopMatrix();
                }
            }

            dt.modelType->cleanupFastDraw();
        }
    }
}
void tTerrain::resetInterval()
{
    float maxRange;
    intervalFixed=false;

    maxRange=MAX(viewMaxX-viewMinX, viewMaxY-viewMinY);

    interval=nearestPowerOf2((int)(maxRange/15));
    clampRange(interval,1,8);
}
void tTerrain::setInterval(int _interval)
{
    int i;
#ifdef DEBUG
    for(i=64;i>0;i>>=1)
        if(_interval==i)
            break;
    if(i==0)
    {
        bug("tTerrain::setInterval: interval must be a power of 2 between 1 and 64");
        return;
    }
#endif
    intervalFixed=true;
    interval=_interval;
}
void tTerrain::onSetViewDimensions()
{
    if(!intervalFixed)
        resetInterval();
}
void tTerrain::onZoomMap()
{
    onSetViewDimensions();
}
void tTerrain::onTiltMap()
{
    onSetViewDimensions();
}
void tTerrain::onSetShadeMapSize(int size)
{
    if(size>0)
    {
        if(size==internalShadeSize)
            return;
        internalShadeSize=size;

        refreshShadeMap(size);
    }
}
void tTerrain::onSetAlphaMapSize(int size)
{
    if(size>0)
    {
        if(size==internalAlphaSize)
            return;
        internalAlphaSize=size;

        refreshAlphaMaps(size);
    }
}
void tTerrain::onSetWaterMapSize(int size)
{
    if(size>0)
    {
        if(size==internalWaterSize)
            return;
        internalWaterSize=size;

        refreshWaterMap(size);
    }
}

int tTerrain::addMeshVertex(const tVector2D& v)
{
    float index;
    tMeshVertex mv;
    tTileInfo ti=getTileInfo(v,DOMAIN_NONE,SEA_FLOOR_DEPTH);
    mv.point.x=v.x;
    mv.point.y=v.y;
    mv.point.z=ti.height;
    mv.normal=ti.normal;
    index=ti.stratumIndex;
    mv.stratumIndex=(int)index;
    mv.alpha=index-(float)mv.stratumIndex;

    meshVertices.push_back(mv);
    return meshVertices.size()-1;
}

static void checkMeshIntegrity(tMeshTriangle *tri)
{
    if(tri->child1 && tri->child2)
    {
        checkMeshIntegrity(tri->child1);
        checkMeshIntegrity(tri->child2);
        if(tri->hypotenuse==NULL)
            return;
        if(tri->hypotenuse->child1==NULL || tri->hypotenuse->child2==NULL)
            bug("checkMeshIntegrity: broken link");
        if(tri->child1->leg2!=tri->hypotenuse->child2 &&
            !(tri->hypotenuse->child2 && tri->child1->leg2==tri->hypotenuse->child2->child1) ||
            tri->child1!=tri->hypotenuse->child2->leg1 &&
            !(tri->child1 && tri->child1->child2==tri->hypotenuse->child2->leg1) ||
            tri->child2->leg1!=tri->hypotenuse->child1 &&
            !(tri->hypotenuse->child1 && tri->child2->leg1==tri->hypotenuse->child1->child2) ||
            tri->child2!=tri->hypotenuse->child1->leg2 &&
            !(tri->child2 && tri->child2->child1==tri->hypotenuse->child1->leg2))
            bug("checkMeshIntegrity: broken link");
        return;
    }

    if(tri->hypotenuse)
        if(tri->hypotenuse->hypotenuse!=tri &&
            tri->hypotenuse->leg1!=tri &&
            tri->hypotenuse->leg2!=tri)
            bug("checkMeshIntegrity: broken link");
    if(tri->leg1)
        if(tri->leg1->hypotenuse!=tri &&
            tri->leg1->leg1!=tri &&
            tri->leg1->leg2!=tri)
            bug("checkMeshIntegrity: broken link");
    if(tri->leg2)
        if(tri->leg2->hypotenuse!=tri &&
            tri->leg2->leg1!=tri &&
            tri->leg2->leg2!=tri)
            bug("checkMeshIntegrity: broken link");
}

void tTerrain::buildMesh()
{
    int i;
    tVector2D   v00={0,0},
                v01={0,mapHeight},
                v10={mapWidth,0},
                v11={mapWidth,mapHeight};
    int         mv00,mv01,mv10,mv11;

    displayProgressBar("Building mesh...  Please Wait.");
    invalidateScreen();

    mv00=addMeshVertex(v00); //0
    mv01=addMeshVertex(v01); //1
    mv10=addMeshVertex(v10); //2
    mv11=addMeshVertex(v11); //3

    tMeshTriangle *cur,*prev;

    stack<tMeshTriangle*> triangleStack;
    mesh.ancestor1=mwNew tMeshTriangle(nextMeshTriangleID++,NULL,NULL,NULL,mv01,mv00,mv10);
    meshTriangles.push_back(mesh.ancestor1);
    mesh.ancestor2=mwNew tMeshTriangle(nextMeshTriangleID++,NULL,NULL,NULL,mv10,mv11,mv01);
    meshTriangles.push_back(mesh.ancestor2);
    mesh.ancestor1->setHypotenuse(mesh.ancestor2);
    mesh.ancestor2->setHypotenuse(mesh.ancestor1);

    triangleStack.push(mesh.ancestor1);
    triangleStack.push(mesh.ancestor2);

    while(triangleStack.size()>0)
    {
        cur=triangleStack.top();
        triangleStack.pop();

        if(cur->child1)
            continue;
        if(needsDivision(cur))
        {
            divideTriangle(cur);

            triangleStack.push(cur->child1);
            triangleStack.push(cur->child2);

            //Propagate the division
            while(true)
            {
                prev=cur;
                cur=cur->hypotenuse;

                if(cur==NULL)
                    break;

                if(cur->hypotenuse==prev)
                {
                    divideTriangle(cur);

                    triangleStack.push(cur->child1);
                    triangleStack.push(cur->child2);

                    connectTriangles(prev,cur);
                    break;
                }
                else if(cur->leg1==prev)
                {
                    divideTriangle(cur);
                    divideTriangle(cur->child1);

                    triangleStack.push(cur->child1->child1);
                    triangleStack.push(cur->child1->child2);
                    triangleStack.push(cur->child2);

                    connectTriangles(prev,cur->child1);
                }
                else if(cur->leg2==prev)
                {
                    divideTriangle(cur);
                    divideTriangle(cur->child2);

                    triangleStack.push(cur->child1);
                    triangleStack.push(cur->child2->child1);
                    triangleStack.push(cur->child2->child2);

                    connectTriangles(prev,cur->child2);
                }
                else
                {
                    bug("tTerrain::buildMesh: broken link");
                    break;
                }
            }
        }
        else if(cur->leg1==NULL || cur->leg2==NULL || cur->hypotenuse==NULL)
        {
            cur->isEdge=true;
            meshEdges.push_back(cur);
        }
    }

    updateProgressBar(0.25);
    invalidateScreen();

    computeTransparency(mesh.ancestor1);
    computeTransparency(mesh.ancestor2);

    updateProgressBar(0.5);
    invalidateScreen();

    //checkMeshIntegrity(mesh.ancestor1);
    //checkMeshIntegrity(mesh.ancestor2);

    for(i=0;i<meshEdges.size();i++)
        addTrim(meshEdges[i]);
    addTrim(mesh.ancestor1);
    addTrim(mesh.ancestor2);

    updateProgressBar(0.75);
    invalidateScreen();

    completeTrim(mesh.ancestor1);
    completeTrim(mesh.ancestor2);

    updateProgressBar(1);
    invalidateScreen();
    closeProgressBar();
}

bool tTerrain::needsDivision(tMeshTriangle *tri)
{
    //bool below=false,above=false;
    tVector3D center;
    tTileInfo ti;
    float diff;
    float z;
    bool above=false,below=false;

#ifdef DEBUG
    if(tri->va<0 || tri->va>=meshVertices.size() ||
        tri->vb<0 || tri->vb>=meshVertices.size() ||
        tri->vc<0 || tri->vc>=meshVertices.size())
    {
        bug("tTerrain::needsDivision: invalid index");
        return false;
    }
#endif
    tMeshVertex& va=meshVertices[tri->va];
    tMeshVertex& vb=meshVertices[tri->vb];
    tMeshVertex& vc=meshVertices[tri->vc];

    float dx=ABS(va.point.x-vc.point.x);
    float dy=ABS(va.point.y-vc.point.y);
    if(dx+dy>=16)
        return true;

    center=(va.point+vc.point)/2;
    ti=getTileInfo(to2D(center),DOMAIN_NONE,SEA_FLOOR_DEPTH);
    diff=fabs(ti.height-center.z);

    z=va.point.z;
    if(z<maxSeaElevation) //fudge factor
        below=true;
    if(z>minSeaElevation)
        above=true;

    z=vb.point.z;
    if(z<maxSeaElevation)
        below=true;
    if(z>minSeaElevation)
        above=true;

    z=vc.point.z;
    if(z<maxSeaElevation)
        below=true;
    if(z>minSeaElevation)
        above=true;

    return diff>0.1 || above && diff>0.05 || above && below && diff>0.002;
}

void tTerrain::divideTriangle(tMeshTriangle *cur)
{
    tVector2D center;
    int mv;

#ifdef DEBUG
    if(cur->va<0 || cur->va>=meshVertices.size() ||
        cur->vc<0 || cur->vc>=meshVertices.size())
    {
        bug("tTerrain::divideTriangle: invalid index");
        return;
    }
#endif

    if(cur->child1)
        return;

    center=(to2D(meshVertices[cur->va].point)+to2D(meshVertices[cur->vc].point))/2;
    mv=addMeshVertex(center);

    cur->child1=mwNew tMeshTriangle(nextMeshTriangleID++, NULL, NULL, cur->leg1, cur->vb, mv, cur->va);
    meshTriangles.push_back(cur->child1);
    cur->child2=mwNew tMeshTriangle(nextMeshTriangleID++, NULL, NULL, cur->leg2, cur->vc, mv, cur->vb);
    meshTriangles.push_back(cur->child2);

    cur->child1->setLeg1(cur->child2);
    cur->child2->setLeg2(cur->child1);

    if(cur->leg1)
    {
        if(cur->leg1->leg2==cur)
            cur->leg1->setLeg2(cur->child1);
        else if(cur->leg1->hypotenuse==cur)
            cur->leg1->setHypotenuse(cur->child1);
        else
            bug("tTerrain::divideTriangle: broken link");
    }
    if(cur->leg2)
    {
        if(cur->leg2->leg1==cur)
            cur->leg2->setLeg1(cur->child2);
        else if(cur->leg2->hypotenuse==cur)
            cur->leg2->setHypotenuse(cur->child2);
        else
            bug("tTerrain::divideTriangle: broken link");
    }
}

void tTerrain::connectTriangles(tMeshTriangle *prev, tMeshTriangle *cur)
{
    if(prev->child2->child1)
        cur->child1->setLeg2(prev->child2->child1);
    else
        cur->child1->setLeg2(prev->child2);

    if(prev->child1->child2)
        cur->child2->setLeg1(prev->child1->child2);
    else
        cur->child2->setLeg1(prev->child1);

    if(cur->child1->child2)
        prev->child2->setLeg1(cur->child1->child2);
    else
        prev->child2->setLeg1(cur->child1);

    if(cur->child2->child1)
        prev->child1->setLeg2(cur->child2->child1);
    else
        prev->child1->setLeg2(cur->child2);
}

void tTerrain::computeTransparency(tMeshTriangle *tri)
{
    int min,max;
    int si;

#ifdef DEBUG
    if(tri->va<0 || tri->va>=meshVertices.size() ||
        tri->vb<0 || tri->vb>=meshVertices.size() ||
        tri->vc<0 || tri->vc>=meshVertices.size())
    {
        bug("tTerrain::scanAlphaMap: invalid index");
        return;
    }
#endif
    tMeshVertex& va=meshVertices[tri->va];
    tMeshVertex& vb=meshVertices[tri->vb];
    tMeshVertex& vc=meshVertices[tri->vc];

    if(tri->child1 && tri->child2)
    {
        computeTransparency(tri->child1);
        computeTransparency(tri->child2);
        tri->minStratumIndex=MIN(tri->child1->minStratumIndex,tri->child2->minStratumIndex);
        tri->maxStratumIndex=MAX(tri->child1->maxStratumIndex,tri->child2->maxStratumIndex);
    }
    else
    {
        //scanAlphaMap(tri,isOpaque,isTransparent,stratum);

        min=MAX_STRATA;
        max=0;

        si=va.stratumIndex-1;
        if(si<min)      min=si;
        if(va.alpha>0)
            si++;
        if(si>max)      max=si;

        si=vb.stratumIndex-1;
        if(si<min)      min=si;
        if(vb.alpha>0)
            si++;
        if(si>max)      max=si;

        si=vc.stratumIndex-1;
        if(si<min)      min=si;
        if(vc.alpha>0)
            si++;
        if(si>max)      max=si;

        clampRange(min,0,(int)(strata.size())-1);
        clampRange(max,0,(int)(strata.size())-1);
        tri->minStratumIndex=min;
        tri->maxStratumIndex=max;
    }
}

/*void tTerrain::findShadowHeight()
{
    int i;
    float x,y;
    int x1,x2,y1,y2;
    float w11,w12,w21,w22;
    float dx1,dx2,dy1,dy2;
    float expected,dz;

    for(i=0;i<meshVertices.size();i++)
    {
        tMeshVertex& mv=meshVertices[i];
        x=mv.point.x/controlScaleX;
        y=mv.point.y/controlScaleY;

        x1=(int)x;
        clampRange(x1,0,controlSizeX-1);
        y1=(int)y;
        clampRange(y1,0,controlSizeY-1);
        x2=x1+1;
        y2=y1+1;

        dx1=x-(float)x1;
        dy1=y-(float)y1;
        dx2=1-dx1;
        dy2=1-dy1;

        tControlPoint& n11=getControlPoint(x1,y1);
        tControlPoint& n21=getControlPoint(x2,y1);
        tControlPoint& n12=getControlPoint(x1,y2);
        tControlPoint& n22=getControlPoint(x2,y2);

        if(dx1>dy1)
        {
            w11=dx2;
            w22=dy1;
            w21=1-w11-w22;
            w12=0;
        }
        else
        {
            w11=dy2;
            w22=dx1;
            w12=1-w11-w22;
            w21=0;
        }

        expected=n11.shadowHeight*w11+
            n21.shadowHeight*w21+
            n12.shadowHeight*w12+
            n22.shadowHeight*w22;

        dz=mv.point.z-expected;
        if(dz>0)
        {
            n11.shadowHeight+=dz;
            if(dx1>dy1)
                n21.shadowHeight+=dz;
            else
                n12.shadowHeight+=dz;
            n22.shadowHeight+=dz;
        }
    }
}*/

void tTerrain::scanAlphaMap(tMeshTriangle *tri, bool& isOpaque, bool& isTransparent, int stratum)
{
    int i,total=0,count=0;
    int x,y,x1,x2,y1,y2,dy;

#ifdef DEBUG
    if(tri->va<0 || tri->va>=meshVertices.size() ||
        tri->vb<0 || tri->vb>=meshVertices.size() ||
        tri->vc<0 || tri->vc>=meshVertices.size())
    {
        bug("tTerrain::scanAlphaMap: invalid index");
        return;
    }
#endif
    tMeshVertex& va=meshVertices[tri->va];
    tMeshVertex& vb=meshVertices[tri->vb];
    tMeshVertex& vc=meshVertices[tri->vc];

    /*tVector3D points[4];
    points[0]=points[3]=va.point;
    points[1]=vb.point;
    points[2]=vc.point;

    for(i=0;i<3;i++)
    {
        if(points[i+1].x==points[i].x)
        {
            if(points[i+1].y>points[i].y)
            {
                x=(int)points[i].x;
                y1=((int)points[i].y)-1;
                y2=(int)points[i+1].y;
            }
            else
            {
                x=((int)points[i].x)-1;
                y1=((int)points[i+1].y)-1;
                y2=(int)points[i].y;
            }
            for(y=y1;y<=y2;y++)
                total+=(int)getAlphaMapPoint(x,y,stratum);
            count+=(y2-y1+1);
        }
        else if(points[i+1].y==points[i].y)
        {
            if(points[i+1].x>points[i].x)
            {
                y=((int)points[i].y)-1;
                x1=((int)points[i].x)-1;
                x2=(int)points[i+1].x;
            }
            else
            {
                y=(int)points[i].y;
                x1=((int)points[i+1].x)-1;
                x2=(int)points[i].x;
            }
            for(x=x1;x<=x2;x++)
                total+=(int)getAlphaMapPoint(x,y,stratum);
            count+=(x2-x1+1);
        }
        else
        {
            if(points[i+1].y>points[i].y)
            {
                if(points[i+1].x>points[i].x)
                {
                    x1=(int)points[i].x;
                    x2=(int)points[i+1].x;
                    y=((int)points[i].y)-1;
                    dy=1;
                }
                else
                {
                    x1=(int)points[i+1].x;
                    x2=(int)points[i].x;
                    y=(int)points[i].y;
                    dy=-1;
                }
            }
            else
            {
                if(points[i+1].x>points[i].x)
                {
                    x1=((int)points[i].x)-1;
                    x2=((int)points[i+1].x)-1;
                    y=((int)points[i].y)-1;
                    dy=-1;
                }
                else
                {
                    x1=((int)points[i+1].x)-1;
                    x2=((int)points[i].x)-1;
                    y=(int)points[i].y;
                    dy=1;
                }
            }

            for(x=x1;x<=x2;x++)
            {
                total+=(int)getAlphaMapPoint(x,y,stratum);
                y+=dy;
            }
            count+=(x2-x1+1);
        }
    }

    isTransparent=(total==0);
    isOpaque=(total==(255*count));
    */
    isOpaque=(va.stratumIndex-stratum>=1 &&
        vb.stratumIndex-stratum>=1 &&
        vc.stratumIndex-stratum>=1);
    isTransparent=(va.stratumIndex-stratum<0 &&
        vb.stratumIndex-stratum<0 &&
        vc.stratumIndex-stratum<0);
}

int tTerrain::interpolateMeshVertex(int a, int b, int stratum)
{
#ifdef DEBUG
    if(a<0 || a>=meshVertices.size() || b<0 || b>=meshVertices.size())
    {
        bug("tTerrain::interpolateMeshVertex: invalid index");
        return 0;
    }
#endif

    tVector3D left=meshVertices[a].point;
    tVector3D right=meshVertices[b].point;
    tVector3D center;
    tTileInfo ti;
    float index;
    int i;

    for(i=0;i<10;i++)
    {
        center=(left+right)/2;
        ti=getTileInfo(to2D(center),DOMAIN_NONE,SEA_FLOOR_DEPTH);
        index=ti.stratumIndex;
        if(index-(float)stratum>0.5)
            left=center;
        else
            right=center;
    }

    tMeshVertex mv;
    mv.point=center;
    mv.normal=ti.normal;
    mv.stratumIndex=(int)index;
    mv.alpha=index-(float)mv.stratumIndex;
    meshVertices.push_back(mv);
    return meshVertices.size()-1;
}

void tTerrain::addTrim(tMeshTriangle *tri)
{
    int i;
    tMeshTriangle *cur,*start;
    tMeshTriangleEx *ex;
    int curVertex,prevVertex,startVertex;

    tMeshVertex *va,*vb,*vc;

    if(tri->child1 && tri->child2)
    {
        addTrim(tri->child1);
        addTrim(tri->child2);
        return;
    }

    for(i=0;i<strata.size();i++)
        if(!strata[i].blend)
        {
            cur=tri;

            for(ex=cur->ex;ex;ex=ex->next)
                if(ex->stratum==i)
                    continue;

            va=&meshVertices[cur->va];
            vb=&meshVertices[cur->vb];
            vc=&meshVertices[cur->vc];

            if(va->stratumIndex-i<=0 && vb->stratumIndex-i>0)
                prevVertex=interpolateMeshVertex(cur->vb,cur->va,i);
            else if(vb->stratumIndex-i<=0 && vc->stratumIndex-i>0)
                prevVertex=interpolateMeshVertex(cur->vc,cur->vb,i);
            else if(vc->stratumIndex-i<=0 && va->stratumIndex-i>0)
                prevVertex=interpolateMeshVertex(cur->va,cur->vc,i);
            else
                continue;

            start=cur;
            startVertex=prevVertex;

            do
            {
                for(ex=cur->ex;ex;ex=ex->next)
                    if(ex->stratum==i)
                        goto break_while;

                va=&meshVertices[cur->va];
                vb=&meshVertices[cur->vb];
                vc=&meshVertices[cur->vc];

                if(va->stratumIndex-i>0 && vb->stratumIndex-i<=0)
                {
                    if(cur->leg1==start)
                        curVertex=startVertex;
                    else
                        curVertex=interpolateMeshVertex(cur->va,cur->vb,i);

                    if(vc->stratumIndex-i<=0)
                        cur->addEx(mwNew tMeshTriangleEx(i,TRIM_A,curVertex,prevVertex));
                    else
                        cur->addEx(mwNew tMeshTriangleEx(i,TRIM_CA,curVertex,prevVertex));

                    cur=cur->leg1;
                    if(cur==NULL)
                        break;
                }
                else if(vb->stratumIndex-i>0 && vc->stratumIndex-i<=0)
                {
                    if(cur->leg2==start)
                        curVertex=startVertex;
                    else
                        curVertex=interpolateMeshVertex(cur->vb,cur->vc,i);

                    if(va->stratumIndex-i<=0)
                        cur->addEx(mwNew tMeshTriangleEx(i,TRIM_B,curVertex,prevVertex));
                    else
                        cur->addEx(mwNew tMeshTriangleEx(i,TRIM_AB,curVertex,prevVertex));

                    cur=cur->leg2;
                    if(cur==NULL)
                        break;
                }
                else if(vc->stratumIndex-i>0 && va->stratumIndex-i<=0)
                {
                    if(cur->hypotenuse==start)
                        curVertex=startVertex;
                    else
                        curVertex=interpolateMeshVertex(cur->vc,cur->va,i);

                    if(vb->stratumIndex-i<=0)
                        cur->addEx(mwNew tMeshTriangleEx(i,TRIM_C,curVertex,prevVertex));
                    else
                        cur->addEx(mwNew tMeshTriangleEx(i,TRIM_BC,curVertex,prevVertex));

                    cur=cur->hypotenuse;
                    if(cur==NULL)
                        break;
                }

                prevVertex=curVertex;
            }
            while(cur!=start);
break_while: ;
        }
}

int tTerrain::duplicateMeshVertex(int a, int b, int x)
{
#ifdef DEBUG
    if(a<0 || a>=meshVertices.size() || b<0 || b>=meshVertices.size() || x<0 || x>=meshVertices.size())
    {
        bug("tTerrain::duplicateMeshVertex: invalid index");
        return 0;
    }
#endif
    tMeshVertex& va=meshVertices[a];
    tMeshVertex& vb=meshVertices[b];
    tMeshVertex& vx=meshVertices[x];

    float dx=(vb.point.x-va.point.x);
    float dy=(vb.point.y-va.point.y);
    float dz=(vb.point.z-va.point.z);

    tMeshVertex mv(vx);

#ifdef DEBUG
    if(dx==0 && dy==0)
    {
        bug("tTerrain::duplicateMeshVertex: a and b are the same point");
        return 0;
    }
#endif

    if(ABS(dx)>ABS(dy))
        mv.point.z=va.point.z + (mv.point.x-va.point.x)/dx*dz;
    else
        mv.point.z=va.point.z + (mv.point.y-va.point.y)/dy*dz;

    meshVertices.push_back(mv);
    return meshVertices.size()-1;
}

void tTerrain::completeTrim(tMeshTriangle *tri)
{
    int i;
    eTrimOrientation orientation1, orientation2;
    tMeshTriangleEx *ex1,*ex2;
    int newVertex;

    if(tri->child1 && tri->child2)
    {
        completeTrim(tri->child1);
        completeTrim(tri->child2);

        for(i=0; i<strata.size(); i++)
            if(!strata[i].blend)
            {
                orientation1=TRIM_NONE;
                orientation2=TRIM_NONE;
                for(ex1=tri->child1->ex;ex1;ex1=ex1->next)
                    if(ex1->stratum==i)
                    {
                        orientation1=ex1->orientation;
                        break;
                    }
                for(ex2=tri->child2->ex;ex2;ex2=ex2->next)
                    if(ex2->stratum==i)
                    {
                        orientation2=ex2->orientation;
                        break;
                    }

                switch(orientation1)
                {
                    case TRIM_NONE:
                        if(orientation2==TRIM_A)
                        {
                            newVertex=duplicateMeshVertex(tri->va,tri->vc, ex2->vi);
                            tri->addEx(mwNew tMeshTriangleEx(i, TRIM_C, newVertex, ex2->vj));
                        }
                        else if(orientation2==TRIM_BC)
                        {
                            newVertex=duplicateMeshVertex(tri->va,tri->vc,ex2->vj);
                            tri->addEx(mwNew tMeshTriangleEx(i, TRIM_AB, ex2->vi, newVertex));
                        }
                        break;

                    case TRIM_C:
                        if(orientation2==TRIM_A)
                            tri->addEx(mwNew tMeshTriangleEx(i, TRIM_CA, ex1->vi, ex2->vj));
                        else if(orientation2==TRIM_NONE)
                        {
                            newVertex=duplicateMeshVertex(tri->va,tri->vc,ex1->vj);
                            tri->addEx(mwNew tMeshTriangleEx(i, TRIM_A, ex1->vi, newVertex));
                        }
                        break;

                    case TRIM_AB:
                        if(orientation2==TRIM_BC)
                            tri->addEx(mwNew tMeshTriangleEx(i, TRIM_B, ex2->vi, ex1->vj));
                        else if(orientation2==TRIM_NONE)
                        {
                            newVertex=duplicateMeshVertex(tri->va,tri->vc,ex1->vi);
                            tri->addEx(mwNew tMeshTriangleEx(i, TRIM_BC, newVertex, ex1->vj));
                        }
                        break;

                    case TRIM_A:
                        if(orientation2==TRIM_C)
                            tri->addEx(mwNew tMeshTriangleEx(i, TRIM_B, ex2->vi, ex1->vj));
                        else if(orientation2==TRIM_CA)
                        {
                            newVertex=duplicateMeshVertex(tri->va,tri->vc,ex2->vi);
                            tri->addEx(mwNew tMeshTriangleEx(i, TRIM_BC, newVertex, ex1->vj));
                        }
                        break;
                    case TRIM_CA:
                        if(orientation2==TRIM_C)
                        {
                            newVertex=duplicateMeshVertex(tri->va,tri->vc,ex1->vj);
                            tri->addEx(mwNew tMeshTriangleEx(i, TRIM_AB, ex2->vi, newVertex));
                        }
                        break;

                    case TRIM_B:
                        if(orientation2==TRIM_AB)
                        {
                            newVertex=duplicateMeshVertex(tri->va,tri->vc,ex1->vi);
                            tri->addEx(mwNew tMeshTriangleEx(i, TRIM_C, newVertex, ex2->vj));
                        }
                        break;

                    case TRIM_BC:
                        if(orientation2==TRIM_B)
                        {
                            newVertex=duplicateMeshVertex(tri->va,tri->vc,ex2->vj);
                            tri->addEx(mwNew tMeshTriangleEx(i, TRIM_A, ex1->vi, newVertex));
                        }
                        else if(orientation2==TRIM_AB)
                            tri->addEx(mwNew tMeshTriangleEx(i, TRIM_CA, ex1->vi, ex2->vj));
                        break;
                }
            }
    }
}

void tTerrain::compileMesh(bool calcShadowTextureExtents)
{
    tTriangleNode nodes[MESH_STACK_SIZE];
    int nodeIndex=0;
    int i,min,max;

    tMeshTriangle *cur;
    tMeshTriangleEx *ex;

    int depth=0;
    bool isFullyVisible=false;
    unsigned char rpa,rpb,rpc,rpd,rpe;

    for(i=0;i<strata.size();i++)
    {
        strata[i].solidTriangles.clear();
        strata[i].translucentTriangles.clear();
        strata[i].trimTriangles.clear();
    }

    cur=mesh.ancestor1;
    nodes[nodeIndex].triangle=mesh.ancestor2;
    nodes[nodeIndex].depth=0;
    nodes[nodeIndex].isFullyVisible=false;
    nodeIndex++;

    while(true)
    {
        if(!isFullyVisible)
        {
            rpa=calculateRelativePosition(meshVertices[cur->va].point);
            rpb=calculateRelativePosition(meshVertices[cur->vb].point);
            rpc=calculateRelativePosition(meshVertices[cur->vc].point);

            isFullyVisible=((rpa|rpb|rpc)==0);
        }

        /* This bit of code takes care of a special case where a simple depth-first
         * search doesn't work.  In some cases, a triangle is invisible (e.g. does
         * not appear on the screen) but it contains smaller triangles which are
         * visible.  We can anticipate this case by looking at the neighboring
         * triangle.  If triangle A is visible, but the triangle adjacent to its
         * hypotenuse, B, is invisible and if breaking the triangles in half causes
         * triangle B to become partially visible, then we need to draw B's children.
         */
        if(cur->child1 && rpa!=0 && rpc!=0 && cur->hypotenuse)
        {
            rpd=calculateRelativePosition(meshVertices[cur->hypotenuse->vb].point);
            rpe=calculateRelativePosition(meshVertices[cur->child1->vb].point);
            if(rpd!=0 && rpe==0)
            {
                if(nodeIndex>=MESH_STACK_SIZE-1)
                {
                    bug("tTerrain::compileMesh: stack overflow");
                    break;
                }
                nodes[nodeIndex].triangle=cur->hypotenuse->child1;
                nodes[nodeIndex].depth=depth;
                nodes[nodeIndex].isFullyVisible=false;
                nodeIndex++;
                nodes[nodeIndex].triangle=cur->hypotenuse->child2;
                nodes[nodeIndex].depth=depth;
                nodes[nodeIndex].isFullyVisible=false;
                nodeIndex++;
            }
        }

        if(!isFullyVisible && (rpa&rpb&rpc))
        {
            if(nodeIndex==0)
                break;
            nodeIndex--;
            cur=nodes[nodeIndex].triangle;
            depth=nodes[nodeIndex].depth;
            isFullyVisible=nodes[nodeIndex].isFullyVisible;
        }
        else if(cur->child1 && cur->child2 && depth<testValue)
        {
            depth++;
            if(nodeIndex==MESH_STACK_SIZE)
            {
                bug("tTerrain::compileMesh: stack overflow");
                break;
            }
            nodes[nodeIndex].triangle=cur->child2;
            nodes[nodeIndex].depth=depth;
            nodes[nodeIndex].isFullyVisible=isFullyVisible;
            nodeIndex++;

            cur=cur->child1;
        }
        else
        {
            for(ex=cur->ex;ex;ex=ex->next)
                switch(ex->orientation)
                {
                    case TRIM_AB:
                        strata[ex->stratum].trimTriangles.push_back(cur->va);
                        strata[ex->stratum].trimTriangles.push_back(cur->vb);
                        strata[ex->stratum].trimTriangles.push_back(ex->vi);
                        //Break omitted intentionally
                    case TRIM_A:
                        strata[ex->stratum].trimTriangles.push_back(cur->va);
                        strata[ex->stratum].trimTriangles.push_back(ex->vi);
                        strata[ex->stratum].trimTriangles.push_back(ex->vj);
                        break;

                    case TRIM_BC:
                        strata[ex->stratum].trimTriangles.push_back(cur->vb);
                        strata[ex->stratum].trimTriangles.push_back(cur->vc);
                        strata[ex->stratum].trimTriangles.push_back(ex->vi);
                        //Break omitted intentionally
                    case TRIM_B:
                        strata[ex->stratum].trimTriangles.push_back(cur->vb);
                        strata[ex->stratum].trimTriangles.push_back(ex->vi);
                        strata[ex->stratum].trimTriangles.push_back(ex->vj);
                        break;

                    case TRIM_CA:
                        strata[ex->stratum].trimTriangles.push_back(cur->vc);
                        strata[ex->stratum].trimTriangles.push_back(cur->va);
                        strata[ex->stratum].trimTriangles.push_back(ex->vi);
                        //Break omitted intentionally
                    case TRIM_C:
                        strata[ex->stratum].trimTriangles.push_back(cur->vc);
                        strata[ex->stratum].trimTriangles.push_back(ex->vi);
                        strata[ex->stratum].trimTriangles.push_back(ex->vj);
                        break;
                }

            min=cur->minStratumIndex;
            max=cur->maxStratumIndex;

            if(min>=0)
            {
                strata[min].solidTriangles.push_back(cur->va);
                strata[min].solidTriangles.push_back(cur->vb);
                strata[min].solidTriangles.push_back(cur->vc);
            }
            for(i=min+1;i<=max;i++)
                if(strata[i].blend)
                {
                    strata[i].translucentTriangles.push_back(cur->va);
                    strata[i].translucentTriangles.push_back(cur->vb);
                    strata[i].translucentTriangles.push_back(cur->vc);
                }

            if(calcShadowTextureExtents && !isFullyVisible)
            {
                if(rpa!=0)
                    checkBoundaryPoint(meshVertices[cur->va].point);
                if(rpb!=0)
                    checkBoundaryPoint(meshVertices[cur->vb].point);
                if(rpc!=0)
                    checkBoundaryPoint(meshVertices[cur->vc].point);
            }

            if(nodeIndex==0)
                break;
            nodeIndex--;
            cur=nodes[nodeIndex].triangle;
            depth=nodes[nodeIndex].depth;
            isFullyVisible=nodes[nodeIndex].isFullyVisible;
        }
    }
}

void tTerrain::drawMesh(bool genColors, bool genNormals, int index)
{
    int i;

    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, sizeof(tMeshVertex), &meshVertices[0].point);
    if(genNormals)
    {
        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_FLOAT, sizeof(tMeshVertex), &meshVertices[0].normal);
    }

    if(terrainWireframe)
        glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
    glColor3f(1,1,1);

    if(index<0)
    {
        for(i=0;i<strata.size();i++)
            glDrawElements(GL_TRIANGLES, strata[i].solidTriangles.size(), GL_UNSIGNED_INT, &strata[i].solidTriangles[0]);
    }
    else
    {
        glDrawElements(GL_TRIANGLES, strata[index].solidTriangles.size(), GL_UNSIGNED_INT, &strata[index].solidTriangles[0]);

        glEnable(GL_POLYGON_OFFSET_FILL);
        glPolygonOffset(-1,-1);
        glDepthMask(false);
        glDrawElements(GL_TRIANGLES, strata[index].trimTriangles.size(), GL_UNSIGNED_INT, &strata[index].trimTriangles[0]);
        glDepthMask(true);
        glDisable(GL_POLYGON_OFFSET_FILL);

        glEnable(GL_BLEND);
        if(genColors)
        {
            glBegin(GL_TRIANGLES);
            for(i=0;i<strata[index].translucentTriangles.size();i++)
            {
                tMeshVertex& mv=meshVertices[strata[index].translucentTriangles[i]];
                if(mv.stratumIndex-index<0)
                    glColor4f(1,1,1,0);
                else if(mv.stratumIndex-index>0)
                    glColor4f(1,1,1,1);
                else
                    glColor4f(1,1,1,mv.alpha);

                glArrayElement(strata[index].translucentTriangles[i]);
            }
            glEnd();
        }
        else
            glDrawElements(GL_TRIANGLES, strata[index].translucentTriangles.size(), GL_UNSIGNED_INT, &strata[index].translucentTriangles[0]);
        glDisable(GL_BLEND);
    }

    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
}

//Bit 0: Is the point to the left of the screen?
//Bit 1: Is the point to the right of the screen?
//Bit 2: Is the point below the screen?
//Bit 3: Is the point above the screen?
//Bit 4: Is the point below the line y=-x?
//Bit 5: Is the point below the line y=(x-SW)?
//Bit 6: Is the point above the line (y-SH)=x?
//Bit 7: Is the point above the line (y-SH)=-(x-SW)?
unsigned char tTerrain::calculateRelativePosition(const tVector3D& v)
{
    int px,py;
    unsigned char rp=0;
    vector3DToPixel(v,px,py);
    if(px<0)
        rp|=1;
    if(px>=viewWidth)
        rp|=2;
    if(py<0)
        rp|=4;
    if(py>=viewHeight)
        rp|=8;
    if(px+py<0)
        rp|=16;
    if((px-viewWidth)>py)
        rp|=32;
    if(px<(py-viewHeight))
        rp|=64;
    if(px+py>viewWidth+viewHeight)
        rp|=128;
    return rp;
}

void tTerrain::onOrientMap()
{
}

void tTerrain::compressVector(const tVector3D& v, char out[3])
{
    int val;

    val=(int)(v.x*128);
    clampRange(val,-128,127);
    out[0]=(char)val;
    val=(int)(v.y*128);
    clampRange(val,-128,127);
    out[1]=(char)val;
    val=(int)(v.z*128);
    clampRange(val,-128,127);
    out[2]=(char)val;
}

eDomain parseDomain(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName, s);
    if(strPrefix(str,"any"))
        return DOMAIN_ANY;
    if(strPrefix(str,"air"))
        return DOMAIN_AIR;
    if(strPrefix(str,"hover"))
        return DOMAIN_HOVER;
    if(strPrefix(str,"amphibious"))
        return DOMAIN_AMPHIBIOUS;
    if(strPrefix(str,"ground"))
        return DOMAIN_GROUND;
    if(strPrefix(str,"sea"))
        return DOMAIN_SEA;
    if(strPrefix(str,"none"))
        return DOMAIN_NONE;

    throw tString("Unrecognized domain '")+str+"'";
}

eWaveformSource parseWaveformSource(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName, s);
    if(strPrefix(str,"none"))
        return WAVEFORM_NONE;
    if(strPrefix(str,"random"))
        return WAVEFORM_RANDOM;
    if(strPrefix(str,"elevation"))
        return WAVEFORM_ELEVATION;
    if(strPrefix(str,"stratum_type"))
        return WAVEFORM_STRATUM_TYPE;
    if(strPrefix(str,"buffer"))
        return WAVEFORM_BUFFER;
    if(strPrefix(str,"decoration_density"))
        return WAVEFORM_DECORATION_DENSITY;

    throw tString("Unrecognized domain '")+str+"'";
}

eSplineSurfaceTarget parseSplineSurfaceTarget(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName, s);
    if(strPrefix(str,"none"))
        return SPLINE_SURFACE_NONE;
    if(strPrefix(str,"elevation"))
        return SPLINE_SURFACE_ELEVATION;
    if(strPrefix(str,"stratum_type"))
        return SPLINE_SURFACE_STRATUM_TYPE;
    if(strPrefix(str,"buffer"))
        return SPLINE_SURFACE_BUFFER;
    if(strPrefix(str,"decoration_density"))
        return SPLINE_SURFACE_DECORATION_DENSITY;

    throw tString("Unrecognized domain '")+str+"'";
}

void  tTerrain::checkBoundaryPoint(const tVector3D& v)
{
    tVector3D w=multiplyMatrix4x4(shadowTextureMatrix,v);
    if(w.x<shadowTextureMinX) shadowTextureMinX=w.x;
    if(w.x>shadowTextureMaxX) shadowTextureMaxX=w.x;
    if(w.y<shadowTextureMinY) shadowTextureMinY=w.y;
    if(w.y>shadowTextureMaxY) shadowTextureMaxY=w.y;
}

void tTerrain::calcVisibleArea(bool calcShadowTextureExtents)
{
    if(calcShadowTextureExtents)
    {
        shadowTextureMinX=MAX_FLOAT;
        shadowTextureMaxX=-MAX_FLOAT;
        shadowTextureMinY=MAX_FLOAT;
        shadowTextureMaxY=-MAX_FLOAT;
    }

    compileMesh(calcShadowTextureExtents);
}

void tTerrain::addSplineSurfaceToCacheHeader(const tSplineSurface& ss, tCache& cache)
{
    for(int i=0;i<ss.waveforms.size();i++)
        addWaveformToCacheHeader(ss.waveforms[i],cache);
    cache.addCharToHeader((char)ss.target);
    cache.addIntToHeader(ss.targetIndex);
    cache.addBoolToHeader(ss.influenceFlightPath);
}
void tTerrain::addWaveformToCacheHeader(const tWaveform& w, tCache& cache)
{
    cache.addCharToHeader((char)w.source);
    cache.addIntToHeader(w.sourceIndex);
    cache.addFloatToHeader(w.wavelength);
    cache.addFloatToHeader(w.amplitude);
    cache.addFloatToHeader(w.bias);
    for(int i=0;i<w.mappings.size();i++)
        addMappingToCacheHeader(w.mappings[i],cache);
}
void tTerrain::addMappingToCacheHeader(const tMapping& m, tCache& cache)
{
    cache.addFloatToHeader(m.from);
    cache.addFloatToHeader(m.to);
    cache.addBoolToHeader(m.isSmooth);
}
void tTerrain::addControlPointVariablesToCacheHeader(tCache& cache)
{
    int i;
    cache.addShortToHeader(VERSION_CODE);
    cache.addIntToHeader(controlSizeX);
    cache.addIntToHeader(controlSizeY);
    cache.addIntToHeader(seed);
    cache.addFloatToHeader(minPoolDepth);
    for(i=0;i<splineSurfaces.size();i++)
        addSplineSurfaceToCacheHeader(splineSurfaces[i],cache);
}
void tTerrain::writeMeshToFile(FILE *fp)
{
    long l;
    char ch;
    tMeshTriangleEx* ex;
    int i;

    l=(long)meshTriangles.size();
    fwrite(&l,sizeof(l),1,fp);

    for(i=0;i<meshTriangles.size();i++)
    {
        tMeshTriangle *t=meshTriangles[i];
        if(t)
        {
            if(t->leg1)
                l=(long)t->leg1->id;
            else
                l=-1;
            fwrite(&l,sizeof(l),1,fp);

            if(t->leg2)
                l=(long)t->leg2->id;
            else
                l=-1;
            fwrite(&l,sizeof(l),1,fp);

            if(t->hypotenuse)
                l=(long)t->hypotenuse->id;
            else
                l=-1;
            fwrite(&l,sizeof(l),1,fp);

            if(t->child1)
                l=(long)t->child1->id;
            else
                l=-1;
            fwrite(&l,sizeof(l),1,fp);

            if(t->child2)
                l=(long)t->child2->id;
            else
                l=-1;
            fwrite(&l,sizeof(l),1,fp);

            l=(long)t->va;              fwrite(&l,sizeof(l),1,fp);
            l=(long)t->vb;              fwrite(&l,sizeof(l),1,fp);
            l=(long)t->vc;              fwrite(&l,sizeof(l),1,fp);

            ex=t->ex;
            while(ex)
            {
                ch=1;
                fwrite(&ch,sizeof(ch),1,fp);
                fwrite(&ex->stratum,sizeof(ex->stratum),1,fp);
                ch=(char)ex->orientation;   fwrite(&ch,sizeof(ch),1,fp);
                l=(long)ex->vi;             fwrite(&l,sizeof(l),1,fp);
                l=(long)ex->vj;             fwrite(&l,sizeof(l),1,fp);
                ex=ex->next;
            }
            ch=0;
            fwrite(&ch,sizeof(ch),1,fp);

            fwrite(&t->minStratumIndex,sizeof(t->minStratumIndex),1,fp);
            fwrite(&t->maxStratumIndex,sizeof(t->maxStratumIndex),1,fp);
            ch=(char)t->isEdge;
            fwrite(&ch,sizeof(ch),1,fp);
        }
    }
}

void tTerrain::readMeshFromFile(FILE *fp)
{
    long l;
    char ch;
    int i,count;
    tMeshTriangle *t;
    tMeshTriangleEx *ex;

    fread(&l,sizeof(l),1,fp);
    count=(int)l;

    meshTriangleBlock=mwNew tMeshTriangle[count];
#ifdef DEBUGCACHE
    const int vertexCount=meshVertices.size();
    const tMeshTriangle *lastTriangle=meshTriangleBlock+count;
#endif

    for(i=0;i<count;i++)
    {
        t=meshTriangleBlock+i;
        t->id=nextMeshTriangleID++;

        fread(&l,sizeof(l),1,fp);
        if(l>=0)
            t->leg1=meshTriangleBlock+l;

        fread(&l,sizeof(l),1,fp);
        if(l>=0)
            t->leg2=meshTriangleBlock+l;

        fread(&l,sizeof(l),1,fp);
        if(l>=0)
            t->hypotenuse=meshTriangleBlock+l;

        fread(&l,sizeof(l),1,fp);
        if(l>=0)
            t->child1=meshTriangleBlock+l;

        fread(&l,sizeof(l),1,fp);
        if(l>=0)
            t->child2=meshTriangleBlock+l;

#ifdef DEBUGCACHE
        if(t->leg1>=lastTriangle || t->leg2>=lastTriangle || t->hypotenuse>=lastTriangle ||
            t->child1>=lastTriangle || t->child2>=lastTriangle)
        {
            bug("tTerrain::readMeshFromFile: invalid link");
            t->leg1=t->leg2=t->hypotenuse=t->child1=t->child2=NULL;
        }
#endif

        fread(&l,sizeof(l),1,fp);   t->va=(int)l;
        fread(&l,sizeof(l),1,fp);   t->vb=(int)l;
        fread(&l,sizeof(l),1,fp);   t->vc=(int)l;
#ifdef DEBUGCACHE
        if(t->va<0 || t->vb<0 || t->vc<0 || t->va>=vertexCount || t->vb>=vertexCount || t->vc>=vertexCount)
        {
            bug("tTerrain::readMeshFromFile: invalid vertex index");
            t->va=t->vb=t->vc=0;
        }
#endif

        fread(&ch,sizeof(ch),1,fp);
        if(ch)
        {
            t->ex=mwNew tMeshTriangleEx;
            ex=t->ex;
            while(true)
            {
                fread(&ex->stratum,sizeof(ex->stratum),1,fp);
                fread(&ch,sizeof(ch),1,fp);    ex->orientation=(eTrimOrientation)ch;
                fread(&l,sizeof(l),1,fp);      ex->vi=(int)l;
                fread(&l,sizeof(l),1,fp);      ex->vj=(int)l;
#ifdef DEBUGCACHE
                if(ex->vi<0 || ex->vj<0 || ex->vi>=vertexCount || ex->vj>=vertexCount)
                {
                    bug("tTerrain::readMeshFromFile: invalid vertex index");
                    ex->vi=ex->vj=0;
                }
#endif

                fread(&ch,sizeof(ch),1,fp);
                if(ch==0)
                    break;

                ex->next=mwNew tMeshTriangleEx;
                ex=ex->next;
            }
        }

        fread(&t->minStratumIndex,sizeof(t->minStratumIndex),1,fp);
        fread(&t->maxStratumIndex,sizeof(t->maxStratumIndex),1,fp);
        fread(&ch,sizeof(ch),1,fp);
        t->isEdge=(bool)ch;
        if(t->isEdge)
            meshEdges.push_back(t);
    }

    mesh.ancestor1=meshTriangleBlock;
    mesh.ancestor2=meshTriangleBlock+1;
}

