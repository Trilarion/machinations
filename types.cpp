/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* TYPES.CPP:  Assorted classes and global functions for common tasks.       *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#ifndef CACHE_HEADERS
    #include <sys/stat.h> //for stat
    #include <stdarg.h> //for va_start
    #include <ctype.h> //for isdigit et al
#endif

#include "types.h"
#include "memwatch.h"
#include "texture.h"
#include "oglwrap.h"
#include "game.h"
#ifndef BORLAND
    #include "snprintf.h"
#endif

#pragma package(smart_init)

/****************************************************************************
                              Global Constants
 ****************************************************************************/
/* These array provide the x and y components of the eight directions enumerated
 * by eDir.  We repeated the last direction, DIR_RIGHT, for convenience.  We
 * also added a 0 terminator for convenience.
 */
const char dirX[] = {1, 1, 0, -1, -1, -1, 0, 1, 1, 0};
const char dirY[] = {0, 1, 1, 1, 0, -1, -1, -1, 0, 0};

//Empty string constant:
const tString EMPTY_STRING;



/****************************************************************************
                              Global Variables:
 ****************************************************************************/
//Define multi-purpose timers for debugging:
#ifdef PROFILE
tFrameRate timers[MAX_TIMERS]={"Game loop","Interface poll","Interface draw",
    "Swap buffers","Engine tick","Poll windows","Invalidate mouse position",
    "Draw map","Draw windows","Draw mouse pointer","Network tick","Compile visible units",
    "Calc visible area","Draw units shadows","Draw decoration shadows","glCopyTexSubImage2D",
    "Draw terrain","Postdraw terrain","Predraw units","Draw units","Draw decorations",
    "Draw missiles","Draw explosions","Postdraw units", "Draw unit overlays","Draw water",
    "Draw waves","Draw minimap"};
#endif
bool timersVisible=false;



 /***************************************************************************\
tLargeTexture::tLargeTexture

Initializes the tLargeTexture structure.  You will need to call loadBitmap to
load a texture.  For example:

tLargeTexture tex;
tex.loadBitmap(...);

Inputs:
    none
Outputs:
    none
\***************************************************************************/
tLargeTexture::tLargeTexture()
{
    horPanels = 0;
    vertPanels = 0;
    width = 0;
    height = 0;
}

/***************************************************************************\
tLargeTexture::setSize

Generates enough OpenGL textures to hold a graphic with the specified
dimensions.  Stores references to these textures in a dynamic array.
Currently, only loadBitmap uses this function.

Inputs:
    width, height   Dimensions of graphic
Outputs:
    none
\***************************************************************************/
void tLargeTexture::setSize(int _width, int _height)
{
    width=_width;
    height=_height;
    //Find the minimum number of horizontal and vertical panels needed to contain the graphic.
    horPanels = DIVIDE_UP(width, MAX_TEXTURE_SIZE);
    vertPanels = DIVIDE_UP(height, MAX_TEXTURE_SIZE);
    //Check boundary condition.
    if(horPanels == 0 || vertPanels == 0)
        return;
    textures.resize(horPanels * vertPanels);
    glGenTextures(horPanels * vertPanels, &textures[0]);
}

/***************************************************************************\
tLargeTexture::loadBitmap

Loads a bitmap from a Windows bitmap file and divides it into one or more
panels.  Function first removes existing bitmap.  Function loads bitmap
using auxiliary function and copies pixel data from each panel to a temporary
array.  Function then binds the pixel data to a unique texture.

Inputs:
    filename    Bitmap's filename excluding path and extension
Outputs:
    Returns true if function succeeds
    Returns false if bitmap fails to load, bitmap is empty, or there is insufficient memory
\***************************************************************************/
bool tLargeTexture::loadBitmap(const char *filename)
{
    int x, y, x2, y2, w;
    int sizeX, sizeY;
    unsigned char *imageData;
    iImageIndex imageName;

    //Delete an existing bitmap.
    glDeleteTextures(horPanels * vertPanels, &textures[0]);
    textures.clear();
    horPanels = 0;
    vertPanels = 0;
    width = 0;
    height = 0;

    //Attempt to load bitmap using an auxilary function.  Function will produce
    //a pointer to a dynamic array of pixel data.
    ilGenImages(1, &imageName);
    ilBindImage(imageName);
    if(ilLoadImage((char*)filename)==IL_FALSE)
        return false;
    ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE);
    sizeX=ilGetInteger(IL_IMAGE_WIDTH);
    sizeY=ilGetInteger(IL_IMAGE_HEIGHT);
    imageData=ilGetData();

    //Generate enough OpenGL textures to hold the image.
    setSize(width, height);
    if(horPanels == 0 || vertPanels == 0)
        return false;

    //Allocate enough memory for one panel.
    unsigned char *data;
    data=mwNew unsigned char[MAX_TEXTURE_SIZE * MAX_TEXTURE_SIZE * 3];
    //Check for insufficient memory.
    if (data==NULL)
        return false;

    //The pixel data is long-word aligned.
    w = ALIGN(sizeX * 3, 4);

    //For each panel, copy the color data to the array and bind the color data to a unique texture.
    for(y2=0;y2<vertPanels;y2++)
        for(x2=0;x2<horPanels;x2++)
        {
            for(y=0;y<MIN(MAX_TEXTURE_SIZE, sizeY-y2*MAX_TEXTURE_SIZE);y++)
                for(x=0;x<MIN(MAX_TEXTURE_SIZE, sizeX-x2*MAX_TEXTURE_SIZE);x++)
                {
                    data[(y*MAX_TEXTURE_SIZE+x)*3 + 0] =
                        imageData[ (y2*MAX_TEXTURE_SIZE+y) * w + (x2*MAX_TEXTURE_SIZE+x) * 3 + 0];
                    data[(y*MAX_TEXTURE_SIZE+x)*3 + 1] =
                        imageData[ (y2*MAX_TEXTURE_SIZE+y) * w + (x2*MAX_TEXTURE_SIZE+x) * 3 + 1];
                    data[(y*MAX_TEXTURE_SIZE+x)*3 + 2] =
                        imageData[ (y2*MAX_TEXTURE_SIZE+y) * w + (x2*MAX_TEXTURE_SIZE+x) * 3 + 2];
                }
            glEnable(GL_TEXTURE_2D);
            mglBindTexture(textures[y2 * horPanels + x2]);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16, MAX_TEXTURE_SIZE, MAX_TEXTURE_SIZE, 0,
                GL_RGB, GL_UNSIGNED_BYTE, data);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        }
    //Clean-up:
    glDisable(GL_TEXTURE_2D);
    mwDelete data;
    mwDelete imageData;
    return true;
}

/***************************************************************************\
tLargeTexture::draw

Draws the texture one panel at a time.  I interpolate the vertices of each
panel using the four specified vertices.

Inputs:
    v   A two-dimensional array containing the texture's vertices.  Each
        vertex consists of three coordinates (x,y,z).  Provide the vertices
        in this order:
            v[0]    Bottom-left corner (0,0)
            v[1]    Bottom-right corner (1,0)
            v[2]    Top-right corner (1,1)
            v[3]    Top-left corner (0,1)
Outputs:
    none
\***************************************************************************/
void tLargeTexture::draw(float v[4][3])
{
    int x, y, i;
    float d1[3], d2[3], d3[3], d4[3];
    float left1[3], left2[3], right1[3], right2[3];
    float bottom1[3], bottom2[3], top1[3], top2[3];

#ifdef DEBUG
    if(width<=0 || height<=0)
    {
        bug("tLargeTexture::draw: invalid dimensions");
        return;
    }
#endif
    //Find the slope of the left edge and the right edge.
    for(i=0;i<3;i++)
    {
        d1[i] = (v[3][i] - v[0][i]) / height;
        d2[i] = (v[2][i] - v[1][i]) / height;
    }
    for(y = 0; y < vertPanels; y++)
    {
        for(i=0;i<3;i++)
        {
            //Find the vertices of each horizontal strip.
            left1[i] = v[0][i] + d1[i] * y * MAX_TEXTURE_SIZE;
            left2[i] = v[0][i] + d1[i] * MIN((y+1) * MAX_TEXTURE_SIZE, height);
            right1[i] = v[1][i] + d2[i] * y * MAX_TEXTURE_SIZE;
            right2[i] = v[1][i] + d2[i] * MIN((y+1) * MAX_TEXTURE_SIZE, height);
            //Find the slope of each horizontal divider.
            d3[i] = (right1[i] - left1[i]) / width;
            d4[i] = (right2[i] - left2[i]) / width;
        }
        for(x = 0; x < horPanels; x++)
        {
            for(i=0;i<3;i++)
            {
                //Find the vertices of each panel.
                bottom1[i] = left1[i] + d3[i] * x * MAX_TEXTURE_SIZE;
                bottom2[i] = left1[i] + d3[i] * MIN((x+1) * MAX_TEXTURE_SIZE, width);
                top1[i] = left2[i] + d4[i] * x * MAX_TEXTURE_SIZE;
                top2[i] = left2[i] + d4[i] * MIN((x+1) * MAX_TEXTURE_SIZE, width);
            }
            //Bind to the appropriate texture and draw a textured quad.
            glEnable(GL_TEXTURE_2D);
            mglBindTexture(textures[y * horPanels + x]);
            glBegin(GL_QUADS);
                glTexCoord2f(0,0);
                glVertex3fv(bottom1);
                glTexCoord2f(MIN(1, (float) width / MAX_TEXTURE_SIZE - x),0);
                glVertex3fv(bottom2);
                glTexCoord2f(MIN(1, (float) width / MAX_TEXTURE_SIZE - x),
                    MIN(1, (float) height / MAX_TEXTURE_SIZE - y));
                glVertex3fv(top2);
                glTexCoord2f(0, MIN(1, (float) height / MAX_TEXTURE_SIZE - y));
                glVertex3fv(top1);
            glEnd();
        }
    }
    glDisable(GL_TEXTURE_2D);
}

/***************************************************************************\
tLargeTexture::~tLargeTexture

Cleans up the structure.  Delete any OpenGL textures.

Inputs:
    none
Ouputs:
    none
\***************************************************************************/
tLargeTexture::~tLargeTexture()
{
    glDeleteTextures(horPanels * vertPanels, &textures[0]);
}



/***************************************************************************\
tFrameRate::tFrameRate

Initialize the structure.  Clear the buffer and prepare the timer with the
current time.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
tFrameRate::tFrameRate(const tString& _name) : delayTotal(0), intervalTotal(0), delayIndex(0),
    intervalIndex(0), oldTime(glfwGetTime()), name(_name)
{
    for(int i=0; i<FRAME_BUFFER_SIZE; i++)
    {
        delayBuffer[i]=0;
        intervalBuffer[i]=0;
    }
}
tFrameRate::tFrameRate(const char *_name) : delayTotal(0), intervalTotal(0), delayIndex(0),
    intervalIndex(0), oldTime(glfwGetTime()), name(_name)
{
    for(int i=0; i<FRAME_BUFFER_SIZE; i++)
    {
        delayBuffer[i]=0;
        intervalBuffer[i]=0;
    }
}

/***************************************************************************\
tFrameRate::start

Starts the timer.  Call this function immediately before the routine you wish
to time.  To calculate the frame rate, call this function in the main loop
immediately after calling tFrameRate::stop.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
void tFrameRate::start()
{
    if(enabled)
    {
        double newTime=glfwGetTime();
        double timeElapsed=newTime-oldTime;
        intervalTotal-=intervalBuffer[intervalIndex];
        intervalBuffer[intervalIndex++]=timeElapsed;
        intervalTotal+=timeElapsed;

        if(intervalIndex==FRAME_BUFFER_SIZE)
            intervalIndex=0;

        oldTime=newTime;
    }
}

/***************************************************************************\
tFrameRate::stop

Stops the timer.  Call this function immediately after the routine you wish
to time.  To calculate the frame rate, call this function in the main loop
immediately before calling tFrameRate::start.
\***************************************************************************/
void tFrameRate::stop()
{
    if(enabled)
    {
        //Replace the current sample with a new one.  Update the total accordingly.
        delayTotal-=delayBuffer[delayIndex];
        float timeElapsed=(glfwGetTime()-oldTime);
        delayBuffer[delayIndex++]=timeElapsed;
        delayTotal+=timeElapsed;

        //Shift to the next position
        if(delayIndex==FRAME_BUFFER_SIZE)
           delayIndex=0;
    }
}



/***************************************************************************\
tFile::setName

Initialize the structure with a new filename.  Function will convert string to
lowercase and separate path from filename if string contains any front- or
back-slashes.  Function will also zero size and date.

Inputs:
    name    Fully-qualified filename (including path and extension)
Outputs:
    none
\***************************************************************************/
void tFile::setName(const tString& _name)
{
    tString s=STRTOLOWER(_name);
    const char *ptr=CHARPTR(s), *ptr2;
    bool found;
    //Traverse string from left to right in search of last slash:
    do
    {
        found=false;
        ptr2=strchr(ptr,'\\');
        if(ptr2)
        {
            ptr=ptr2+1;
            found=true;
        }
        ptr2=strchr(ptr,'/');
        if(ptr2)
        {
            ptr=ptr2+1;
            found=true;
        }
    }
    while(found);
    //Splice the string storing the path and filename separately:
    path=STRLEFT(s, ptr-CHARPTR(s));
    name=ptr;
    size=0;
    time=0;
}

/***************************************************************************\
tFile::open

Attempts to open the file referenced by this structure.  You must call setName
before calling this function.  Function will retrieve file's date and size.
Function will then attempt to open the file using the specified method.

Inputs:
    method      Same format as second parameter in fopen
Outputs:
    Returns handle to the newly opened file if the function succeeds
    Return NULL if the function fails
\***************************************************************************/
FILE *tFile::open(const char* method)
{
    struct stat s;
    if(stat(CHARPTR(getFullName()),&s)<0)
        return NULL;    //Failed to retrieve file properties
    size=s.st_size;
    time=s.st_mtime;    //Last time file was modified
    return fopen(CHARPTR(getFullName()),method);
}

/***************************************************************************\
tFile::operator==

Compares two tFile structures to determine if they reference the same file.
Files match if they have the same name, size, and date.  Paths do not need to match!

Inputs:
    Two tFile objects
Outputs
    Returns true if the files match
\***************************************************************************/
bool tFile::operator==(const tFile& arg)
{
    return STREQUAL(name,arg.name) && size==arg.size && time==arg.time;
}

/***************************************************************************\
tFile::exists

Determines whether the file with the given properties exists.

Inputs:
    none
Outputs:
    Returns true if a file with the given name, date, and size exists.
\***************************************************************************/
bool tFile::exists()
{
    struct stat s;
    if(stat(CHARPTR(getFullName()),&s)<0) //failed to retrieve file properties
        return false;
    //These parameters not specified
    if(time==0 && size==0)
        return true;
    if(time==s.st_mtime && size==s.st_size)
        return true;
    return false;
}

//Attempts to remove the current file referenced by this structure.
//The function also resets the structure for good measure.
void tFile::remove()
{
    if(STRLEN(getFullName())==0)
    {
        bug("tFile::remove: structure is empty");
        return;
    }
    ::remove(CHARPTR(getFullName()));
    name="";
    path="";
    size=0;
    time=0;
}

//Clears the structure
void tFile::clear()
{
    name="";
    path="";
    size=0;
    time=0;
}

/***************************************************************************\
longToIP

Converts a long integer to an array of four bytes which represent
a, b, c, and d in the notation:
                                a.b.c.d
Inputs:
    IPLong      Network address in host-byte-order
    values      Array containing four bytes
Outputs:
    Copies four components of network address into array
\***************************************************************************/
void longToIP(long IPLong, unsigned char values[])
{
    values[0]=(IPLong >> 24) & 0xFF;
    values[1]=(IPLong >> 16) & 0xFF;
    values[2]=(IPLong >> 8)  & 0xFF;
    values[3]= IPLong        & 0xFF;
}

/***************************************************************************\
IPToLong

Converts an array of four bytes (which represent a, b, c, and d in the
notation a.b.c.d) to a long integer.

Inputs:
    values      Array containing four components of network address
Outputs:
    Returns network address as long integer in host-byte-order
\***************************************************************************/
long IPToLong(const unsigned char values[])
{
    return (values[0] << 24) | (values[1] << 16) | (values[2] << 8) | values[3];
}

/***************************************************************************\
log

Log appends a line to the output file containing the time, date, user name,
and formatted string.  The formatted string has the same format as printf.
log operates the same as bug except the message contains 'LOG' instead of
'***BUG***'.

Inputs:
    format      Format string
    ...         Series of values referenced by the format string
Outputs:
    none
\***************************************************************************/
void log(const char *format, ...)
{
    #ifdef LOG
    char buffer[80];
    //Prepare to read optional parameters:
    va_list argPtr;
    va_start(argPtr, format);

    //Open the output file:
    FILE *fp = fopen(OUTPUT_FILE, "a");
    if(fp==NULL)
        return;

    //Format time and date:
    time_t t;
    time(&t);
    strcpy(buffer, ctime(&t));

    //Remove new line character:
    buffer[24]='\0';

    //Write time, date, user name, and message to output file:
    fprintf(fp, "LOG [%s %s] ", buffer, CHARPTR(globalName));
    vfprintf(fp, format, argPtr);
    fprintf(fp, "\n");

    //Write the same information to the console window:
    printf("LOG [%s %s] ", buffer, CHARPTR(globalName));
    vprintf(format, argPtr);
    printf("\n");

    //Clean-up:
    va_end(argPtr);
    fclose(fp);
    #endif
}

/***************************************************************************\
bug

Bug appends a line to the output file containing the time, date, user name,
and formatted string.  The formatted string has the same format as printf.
bug operates the same as log except the message contains '***BUG***' instead
of 'LOG'.

Inputs:
    format      Format string
    ...         Series of values referenced by the format string
Outputs:
    none
\***************************************************************************/
void bug(const char *format, ...)
{
#ifdef DEBUG
    char buffer[80];
    //Prepare to read optional parameters:
    va_list argPtr;
    va_start(argPtr, format);

    //Open the output file:
    FILE *fp = fopen(OUTPUT_FILE, "a");
    if(fp==NULL)
        return;

    //Format time and date:
    time_t t;
    time(&t);
    strcpy(buffer, ctime(&t));

    //Remove new line character:
    buffer[24]='\0';

    //Write time, date, user name, and message to output file:
    fprintf(fp, "***BUG*** [%s %s] ", buffer, CHARPTR(globalName));
    vfprintf(fp, format, argPtr);
    fprintf(fp, "\n");

    //Write the same information to the console window:
    printf("***BUG*** [%s %s] ", buffer, CHARPTR(globalName));
    vprintf(format, argPtr);
    printf("\n");

    //Clean-up:
    va_end(argPtr);
    fclose(fp);
#endif
}

/***************************************************************************\
windowXYToModelXY

Converts screen coordinates to model coordinates using the viewport, projection
matrix, and given matrix.  Function finds the point where a ray parallel to
the viewer's line of sight intersects the plane z=0.

Inputs:
    windowX, windowY    Screen coordinates
    matrix[]            Matrix to apply after projection matrix

Outputs:
    modelX, modelY      Model coordinates
    Returns true if conversion succeeded
    Returns false if converstion failed (e.g. z=0 plane parallel to viewer's
        line of sight)
\***************************************************************************/
bool windowXYToModelXY(int windowX, int windowY, float& modelX, float& modelY, float modelView[])
{
#ifdef DEBUG
    if(ARRAY_VIOLATION(modelView,16))
        return false;
#endif

    float viewport[4];
    float matrix[16];

    //Preserve the projection matrix:
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();

    //Multiply the projection matrix by the modelview matrix and store the product
    //in matrix:
    glMultMatrixf(modelView);

    //Restore the projection matrix:
    glGetFloatv(GL_PROJECTION_MATRIX, matrix);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);

    //Retreive the viewport:
    glGetFloatv(GL_VIEWPORT, viewport);
#ifdef DEBUG
    if(viewport[2]<=0 || viewport[3]<=0)
    {
        bug("windowXYToModelXY: invalid viewport");
        return false;
    }
#endif
    /* Find raw screen coordinates before multiplied by the viewport.
     * Hence x = -1 is the left edge, x = 1 is the right edge,
     * y = -1 is the bottom edge, and y = 1 is the top edge.
     */
    float x = (windowX - viewport[0]) / viewport[2] * 2 - 1;
    float y = (windowY - viewport[1]) / viewport[3] * 2 - 1;

    /* Solve the linear equation:
     *      ax + by + c = 0
     *      dx + ey + f = 0
     */
    float a = matrix[0] - matrix[3] * x;
    float b = matrix[4] - matrix[7] * x;
    float c = matrix[12] - matrix[15] * x;
    float d = matrix[1] - matrix[3] * y;
    float e = matrix[5] - matrix[7] * y;
    float f = matrix[13] - matrix[15] * y;
    float m = b * f - c * e;
    float n = a * e - b * d;
    if (n == 0)
        return false; //z = 0 plane parallel to viewer...nothing more we can do
    modelX = m / n;
    if (b != 0)
        modelY = (-a * modelX - c) / b;
    else if (e != 0)
        modelY = (-d * modelX - f) / e;
    else
        return false; //z = 0 plane parallel to viewer...nothing more we can do
    return true;
}

/***************************************************************************\
loadStringList

Loads a string list with a series of new-line-delimited strings.  Any existing
strings in the string list are erased.

Inputs:
    string      String containing new-line-delimited strings.
Outputs:
    list        String list
\***************************************************************************/
void loadStringList(tStringList& list, const char* string)
{
    //Erase any existing strings:
    list.clear();

    //Make a duplicate of the string so we can keep the original string in tact:
    char *s = strdup(string), *p1 = s, *p2;

    //Traverse the string looking for new-line characters.
    while( p2 = strchr(p1, '\n') )
    {
        //Replace the new-line character with a null-terminator so we can copy
        //the previous line to the string list:
        *p2 = '\0';
        list.push_back(p1);
        p1 = p2+1;
    }
    //Copy the last line if one exists:
    if(strlen(p1)>0)
        list.push_back(p1);

    //Clean-up:
    free(s);
}

/***************************************************************************\
stringListLookup

Finds the first occurrence of the string in the string list.  The match may
contain additional characters after the search string so long as they are
separated by a space.  Search is case-sensitive.

Inputs:
    list    String list to search
    string  Search string
Outputs:
    Returns index of first occurence or -1 if no match found
\***************************************************************************/
int stringListLookup(const tStringList& list, const tString& string)
{
    int l=STRLEN(string);
    for(int i=0; i<list.size(); i++)
    {
        if(STREQUAL(string,list[i]))
            return i;
        if(STRLEN(list[i])>l && STREQUAL(string,STRLEFT(list[i],l)) && list[i][l]==' ')
            return i;
    }
    return -1;
}

/***************************************************************************\
setViewingArea

Restricts drawing to a bounding rectangle.  Any drawing done outside this
rectangle will not appear.  Specify the rectangle's boundaries in raw
screen coordinates.  Hence, if the screen's resolution is 640x480, (0,0)
is the lower-left corner and (640,480) is the upper-right corner.

Inputs:
    x,y     Coordinates of lower-left corner of rectangle
    w,h     Dimensions of rectangle
Outputs:
    none
\***************************************************************************/
void setViewingArea(int x, int y, int w, int h)
{
//glScissor doesn't work on some systems, but a combination of glViewport and glOrtho does.
//Therefore, I add a compiler option for switching between the two methods.
#ifdef VIEWPORT
	glViewport(x, y, w, h);             //Reset the viewport
	glMatrixMode(GL_PROJECTION);
    glPushMatrix();                     //Preserve the projection matrix
	glLoadIdentity();
    glOrtho(x, x+w, y, y+h, 1, -1);     //Set up an orthographic projection
	glMatrixMode(GL_MODELVIEW);
#elif defined(SCISSOR)  //SCISSOR is the default if neither compiler-switch is set.
	glScissor(x,y,w,h);
	glEnable(GL_SCISSOR_TEST);
#elif defined(STENCIL_MASK)
    GLboolean colorMask[4],depthMask;
    glEnable(GL_STENCIL_TEST);
    glStencilFunc(GL_EQUAL,1,1);
    glStencilOp(GL_REPLACE,GL_REPLACE,GL_REPLACE);
    glGetBooleanv(GL_COLOR_WRITEMASK,colorMask);
    glGetBooleanv(GL_DEPTH_WRITEMASK,&depthMask);
    glColorMask(false,false,false,false);
    glDepthMask(false);
    glRecti(x,y,x+w,y+h);
    glColorMask(colorMask[0],colorMask[1],colorMask[2],colorMask[3]);
    glDepthMask(depthMask);
#else //CLIP_PLANE
    double equation1[4]={1,0,0,-x};
    double equation2[4]={-1,0,0,x+w};
    double equation3[4]={0,1,0,-y};
    double equation4[4]={0,-1,0,y+h};

    glClipPlane(GL_CLIP_PLANE1,equation1);
    glClipPlane(GL_CLIP_PLANE2,equation2);
    glClipPlane(GL_CLIP_PLANE3,equation3);
    glClipPlane(GL_CLIP_PLANE4,equation4);

    glEnable(GL_CLIP_PLANE1);
    glEnable(GL_CLIP_PLANE2);
    glEnable(GL_CLIP_PLANE3);
    glEnable(GL_CLIP_PLANE4);
#endif
}

/***************************************************************************\
resetViewingArea

Resets the drawing area to the entire screen.  Restores the projection matrix
if using the viewport method.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
void resetViewingArea(int x, int y, int w, int h)
{
#ifdef VIEWPORT
	glViewport(0,0,getScreenWidth(),getScreenHeight());           //Reset the viewport
	glMatrixMode(GL_PROJECTION);
    glPopMatrix();                                      //Restore the projection matrix
	glMatrixMode(GL_MODELVIEW);
#elif defined(SCISSOR) //SCISSOR is the default if neither compiler-switch is set.
	glDisable(GL_SCISSOR_TEST);
#elif defined(STENCIL_MASK)
    glStencilOp(GL_ZERO,GL_ZERO,GL_ZERO);
    glColorMask(false,false,false,false);
    glDepthMask(false);
    glRecti(x,y,x+w,y+h);
    glColorMask(true,true,true,true);
    glDepthMask(true);
    glDisable(GL_STENCIL_TEST);
#else //CLIP_PLANE
    glDisable(GL_CLIP_PLANE1);
    glDisable(GL_CLIP_PLANE2);
    glDisable(GL_CLIP_PLANE3);
    glDisable(GL_CLIP_PLANE4);
#endif
}

/***************************************************************************\
createRectShadow

Creates a texture with blury edges to create a shadow effect.  The texture has
the same color throughout; however, the alpha component tapers off around the
edges.  The dispersion governs how fuzzy the edges are.

Inputs:
    width, height       Dimensions of shadow (independent of dispersion)
    dispersion          Breadth of the "blurry margin"
    r, g, b             Color
    texture             Unique texture index (produced with glGenTextures)
Outputs:
    none
\***************************************************************************/
void createRectShadow(int width, int height, int dispersion, float r, float g, float b,
    iTextureIndex texture)
{
    int x, y, size = 64;
    float yFactor, xFactor;
#ifdef DEBUG
    if(dispersion<0)
    {
        bug("createRectShadow: invalid dispersion");
        return;
    }
#endif
    //Find the smallest texture which will contain the shadow.  Textures sizes
    //must be powers of two.
    while(width > size || height > size)
        size *= 2;

    //Allocate memory to hold the shadow:
    unsigned char *data;
    data=mwNew unsigned char[size*size*4];
    if(data==NULL)
        return;

    for(y = 0; y < height; y++)
    {
        //Taper the top and bottom edges:
        if(y<dispersion)
            yFactor = (float) (y+1) / (dispersion+1);
        else if (y>=height-dispersion)
            yFactor = (float) (height-y) / (dispersion+1);
        else
            yFactor = 1;
        for(x = 0; x < width; x++)
        {
            //Taper the left and right edges:
            if(x<dispersion)
                xFactor = (float) (x+1) / (dispersion+1);
            else if (x>=width-dispersion)
                xFactor = (float) (width-x) / (dispersion+1);
            else
                xFactor = 1;
            //Each point has the same color.  Only the alpha component varies.
            data[(y*size+x)*4]=(unsigned char)(r*255);
            data[(y*size+x)*4+1]=(unsigned char)(g*255);
            data[(y*size+x)*4+2]=(unsigned char)(b*255);
            data[(y*size+x)*4+3]=(unsigned char)(xFactor*yFactor*255);
        }
    }
    //Copy the texture to video RAM:
    glEnable(GL_TEXTURE_2D);
    mglBindTexture(texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16, size, size, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    //Clean-up:
    glDisable(GL_TEXTURE_2D);
    mwDelete data;
}

/***************************************************************************\
strToLower

Converts a string of characters to lowercase.

Inputs:
    buffer      Null-terminated string
Outputs:
    buffer      Same string with uppercase letters converted to lowercase
\***************************************************************************/
char *strToLower(char *buffer)
{
    char *ptr=buffer;
#ifdef DEBUG
    if(MEMORY_VIOLATION(buffer)) //just check first character
        return NULL;
#endif
    while(*ptr)
    {
        *ptr = tolower(*ptr);
        ptr++;
    }
    return buffer;
}

/***************************************************************************\
strToUpper

Converts a string of characters to uppercase.

Inputs:
    buffer      Null-terminated string
Outputs:
    buffer      Same string with lowercase letters converted to uppercase
\***************************************************************************/
char *strToUpper(char *buffer)
{
    char *ptr=buffer;
#ifdef DEBUG
    if(MEMORY_VIOLATION(buffer)) //just check first character
        return NULL;
#endif
    while(*ptr)
    {
        *ptr = toupper(*ptr);
        ptr++;
    }
    return buffer;
}

/***************************************************************************\
strPrefix

Determines if a string begins with a prefix.  Comparison is case-insensitive.

Inputs:
    prefix      Null-terminated prefix
    string      Null-terminated string
Outputs:
    Returns true if the string begins with the prefix
\***************************************************************************/
bool strPrefix(const char *prefix, const char *string)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(prefix)||MEMORY_VIOLATION(string)) //just check first character
        return false;
#endif
	if(*prefix=='\0')
		return false;
    if(strlen(prefix)>strlen(string))
        return false;
    return strnicmp(prefix,string,strlen(prefix))==0;
}

/***************************************************************************\
stringToLower

Makes a copy of a tString object and converts it to lowercase.

Inputs:
    str     tString object
Outputs:
    Returns a copy of the string with uppercase letters converted to lowercase
\***************************************************************************/
tString stringToLower(const tString& str)
{
    tString s=CHARPTR(str);
    for(int i=0;i<STRLEN(s);i++)
        s[i]=tolower(s[i]);
    return s;
}

/***************************************************************************\
stringToUpper

Makes a copy of a tString object and converts it to uppercase.

Inputs:
    str     tString object
Outputs:
    Returns a copy of the string with lowercase letters converted to uppercase
\***************************************************************************/
tString stringToUpper(const tString& str)
{
    tString s=CHARPTR(str);
    for(int i=0;i<STRLEN(s);i++)
        s[i]=toupper(s[i]);
    return s;
}

/***************************************************************************\
strCount

Counts the number of occurences of a specified character in a string.  The
null-terminator is not considered part of the string.

Inputs:
    str     Null-terminated string
    ch      Character for which to search
Outputs:
    Returns number of occurences
\***************************************************************************/
int strCount(const char *str, char ch)
{
    int count=0;
    const char *ptr=str;
#ifdef DEBUG
    if(MEMORY_VIOLATION(str)) //just check the first character
        return 0;
#endif
    while(*ptr!='\0')
        if(*(ptr++)==ch)
            count++;
    return count;
}

/***************************************************************************\
operator+

Visual C++ does not define the addition operator for two C strings.  Therefore,
we provide an implementation of our own.

Inputs:
    Two tString objects (C strings)
Outputs:
    Returns a new string with the two operators concatenated
\***************************************************************************/
#ifdef VISUALC
tString operator+(const tString& lhs, const tString& rhs)
{
    tString s=lhs;
    s+=rhs;
    return s;
}
#endif

/****************************************************************************/
void makeValid(int& x, int& y, int width, int height)
//Ensures that the point (x,y) lies within the region [0,width)x[0,height)
//If it doesn't, the function adjusts x and y by the minimum amount so that it does.
{
    if(x<0)         x=0;
    if(y<0)         y=0;
    if(x>=width)    x=width-1;
    if(y>=height)   y=height-1;
}
/****************************************************************************/
bool isValid(int x, int y, int width, int height)
//Determines if the point(x,y) lies within the region [0,width)x[0,height)
{
    return x>=0 && y>=0 && x<width && y<height;
}

bool containsExtension(const char *buffer)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(buffer))
        return false;
#endif
    const char *ptr=buffer;
    const char *start=buffer;
    while(*ptr!='\0')
    {
        if(*ptr=='/' || *ptr=='\\')
            start=ptr+1;
        ptr++;
    }

    ptr=strchr(start, '.');
    return (ptr!=NULL);
}

int mySprintf(tBufferIterator& iter, const char *format, ...) throw(int)
{
    int retval;
    int bytes=(iter.start+iter.length)-iter.current;
    if(bytes==0)
    {
        if(iter.length>0)
            iter.start[iter.length-1]='\0';
        throw 1;
    }

    va_list argPtr;
    va_start(argPtr, format);
    retval = vsnprintf(iter.current, bytes, format, argPtr);
    iter.current += retval;
    va_end(argPtr);

    if(iter.current>=iter.start+iter.length)
    {
        iter.current=iter.start+iter.length;
        if(iter.length>0)
            iter.start[iter.length-1]='\0';
        throw 1;
    }

    return retval;
}

int nearestPowerOf2(int x)
{
    int a=1;
    while(a<x && a>0) //Safe-guard against infinite loop
        a<<=1;
    return a;
}

void checkGLError()
{
    GLenum error=glGetError();
    if(error!=GL_NO_ERROR)
        bug("OpenGL error: %s",(char*)gluErrorString(error));
}

void showTimers()
{
    timersVisible=true;
#ifdef PROFILE
    for(int i=0;i<MAX_TIMERS;i++)
        timers[i].enable();
#endif
}
void hideTimers()
{
    timersVisible=false;
#ifdef PROFILE
    for(int i=0;i<MAX_TIMERS;i++)
        timers[i].disable();
#endif
}

void writeString(const tString& string, FILE *fp)
{
    tString s=string+"\n";
    fputs(s.c_str(),fp);
}
tString readString(FILE *fp)
{
    char buf[1000];
    fgets(buf,1000,fp);
    int len=strlen(buf);
    if(len>0 && buf[len-1]=='\n')
        buf[len-1]='\0';
    return tString(buf);
}

float transformValue(float val, const vector<tMapping>& mappings)
{
    int i;
    if(mappings.size()==0)
        return val;

    if(val<mappings.front().from || (val==mappings.front().from && mappings.front().isClosed))
        return mappings.front().to;

    for(i=1;i<mappings.size();i++)
    {
        if(val==mappings[i].from && mappings[i].isClosed)
            return mappings[i].to;
        else if(val<mappings[i].from)
        {
            const tMapping& previous=mappings[i-1];
            const tMapping& next=mappings[i];
            float delta=(val-previous.from)/(next.from-previous.from);
            if(!previous.isSmooth && !next.isSmooth)
                return delta*(next.to-previous.to)+previous.to;
            else
            {
                float first,last;
                float *table=lookupSplineArray(delta);
                if(previous.isSmooth&&i>=2)
                    first=mappings[i-2].to;
                else
                    first=previous.to*2-next.to;
                if(next.isSmooth&&i<mappings.size()-1)
                    last=mappings[i+1].to;
                else
                    last=next.to*2-previous.to;
                return first*table[0]+previous.to*table[1]+next.to*table[2]+last*table[3];
            }
        }
    }

    return mappings.back().to;
}

