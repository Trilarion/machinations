/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef unitH
#define unitH

/**************************************************************************
                                Includes
 **************************************************************************/
#include "model.h"
#include "missile.h"
#include "terrain2.h"
#include "gamedefs.h"

/**************************************************************************
                                Structures
 **************************************************************************/

//Contains information about one type of command (e.g. Move or Attack)
struct tCommandType
{
    tString     name;       //The string identifier to search for in the texture pack (cannot contain spaces!)
    tString     caption;    //The string that appears when the user positions the mouse over the icon
    eParamType  paramType;  //What information does the command need?
    bool        interfaceCommand;
                            //Does the command affect the user-interface only? (as with disband)
    bool        immediate;  //Will the command execute immediately or wait its turn
                                //in the unit command queue?
    bool        repeat;     //Will the command execute repeatedly?  Set this flag
                                //to true for 'patrol' only.
    bool        benevolent; //Will the player typically target friendly units (true)
                                //or enemy units (false)?  This flag only applies to
                                //commands with a unit parameter.
    tKey        hotKey[2];  //Player can initiate this command by pressing...
                            //May consist of one or two GLFW keys.  If there is only
                            //one hot key, set hotKey[1] to 0.  If there are no hot keys
                            //set hotkey[0] to 0 and hotkey[1] to 0.
	int			unitID;		//Unit to produce or construct (only applicable if paramType is
							//PARAM_PRODUCTION or PARAM_CONSTRUCTION)
    tCommandType(const tString& _name, eParamType _paramType, bool _interfaceCommand,
        bool _immediate, bool _repeat, bool _benevolent) : name(_name), paramType(_paramType),
        interfaceCommand(_interfaceCommand), immediate(_immediate), repeat(_repeat), benevolent(_benevolent),
        unitID(0) { hotKey[0]=0; hotKey[1]=0; }
    tCommandType() : paramType(PARAM_NONE), interfaceCommand(false), immediate(false), repeat(false),
        benevolent(false), unitID(0) { hotKey[0]=0; hotKey[1]=0; }
};

class tCommandMsg;

//Represents one instance of a unit command.  The engine dispatches commands
//to the tUnit class and packages them in the tCommandMsg class for delivery
//to other peers.
class tCommand
{
    public:
    eCommand    command;        //Command to execute
    eParamType  paramType;      //Which parameters (below) apply to this command?
    tVector2D   pos;            //Target position
    long*       targets;        //Dynamic array of unit ID's
    int         targetCount;    //Number of unit targets
    int         quantity;       //Number of units to produce
    bool        repeat;         //Produce the same unit over and over again?
    tCommand() : targets(NULL), targetCount(0) {}
    tCommand(const tCommandMsg& msg);
    tCommand(const tCommand& x);
    tCommand(eCommand _command, eParamType _paramType);
    tCommand(eCommand _command, eParamType _paramType, tVector2D _pos);
    tCommand(eCommand _command, eParamType _paramType, long target);
    tCommand(eCommand _command, eParamType _paramType, int quantity, bool _repeat);
    tCommand& operator=(const tCommand& x);
    ~tCommand();
};

struct tBehaviorType;

struct tAbilitySwitchType
{
    eAbility ability;
    tArgumentType<bool> argumentType;
    tAbilitySwitchType() : ability(ABILITY_NONE), argumentType(true,BOUNDARY_NONE) {}
};

//This structure contains blueprints for one type of unit.  There will always
//be one and only one instance of tUnitType for each type regardless of how
//many unit exists in the game.
struct tUnitType
{
    int             ID;                 //Unique integer identifier
    tString         name;               //Unique string identifier
    tString         caption;            //String visible to the player
    tString         buildDesc;          //Build command caption for this unit
    tKey            buildHotKey[2];     //Build command hot key for this unit
    tModelType*     modelType;          //Reference to model type used by this unit type
    float           HP;                 //Maximum hitpoints
    eUnitSize       size;               //SIZE_TINY, SIZE_SMALL, etc.
    bool            canMove;            //Can the unit move?
    float           vMax;               //Maximum velocity under ideal conditions (units/second)
    float           aMax;               //Maximum acceleration (units/second/second)
    bool            isBuilding;         //Building's don't gain substance until their hitpoints rise above zero
                                        //and don't gain functionality until they reach full hitpoints.
    eDomain         domain;             //DOMAIN_AIR, DOMAIN_LAND, DOMAIN_SEA, etc.
    float           turnRate;           //The unit's maximum turning rate (radians/second) or 0
                                        //if the unit can face any direction instantly.
    float           costs[MAX_RESOURCES];//How much does this unit cost to build EACH SECOND?
    float           buildRate;          //How fast can the unit be built? (HITPOINTS/SECOND)
    float           armor[MAX_ARMOR_CLASSES];
                                        //The unit's armor against each type of damage (e.g. bashing, piercing, etc.)
    vector<tBehaviorType *>             //Dynamic array of custom abilities belonging to this unit type
                    behaviorTypes;

    //Internal engine variables:
    //Does each player have sufficient resources to repair this type of unit?
    //This array is used to optimize automatic repairs (see acquireTarget).
    bool            repairable[MAX_PLAYERS];

    tArgumentType<float> elevationArg;    //Elevation above (+) or below (-) land/water surface
    tArgumentType<float> modelOpacityArg; //Set modelOpacity to 0 to completely cloak the unit.
                                          //The unit will remain partially visible, but the shadow
                                          //will disappear.
    tArgumentType<bool> alwaysVisibleArg; //Is the unit always visible regardless of cloaking?

    tProcedureType* onBirthProc;
    tProcedureType* onDeathProc;
    tProcedureType* onConceptionProc;

    vector<tAbilitySwitchType> abilitySwitchTypes;
    void parseAbilitySwitch(tStream& s) throw(tString);

    void destroy();
    tUnitType(tStream& s) throw(tString);
    ~tUnitType() { destroy(); }
};

//This structure contains all of the information needed to draw a unit's explosion
//I treat the explosion as a cylinder of flame centered at center whose dimensions
//are equal to tUnit::getMaxZ() and tUnit::getRadius() immediately before the unit dies.
class tUnitExplosion
{
    private:
    tVector3D       position3D;
    tVector3D       center;
    float           radius;
    float           height;
    float           radius3D;
    tAnim           anim;
    iSoundSource    soundSource;        //Handle of an OpenAL sound source (or 0 if no handle exists)
    tWorldTime      dt;
    tWorldTime      t0;

    public:
    const tVector3D& getPosition3D() { return position3D; }
    tVector2D   getPosition()   { return to2D(position3D); }
    const tVector3D& getCenter() { return center; }
    float       getRadius() { return radius; }
    float       getHeight() { return height; }
    float       getRadius3D() { return radius3D; }
    void        tick(tWorldTime t) { anim.tick(t); dt=t-t0; }
    void        draw();
    bool        isFinished();
    tUnitExplosion(const tVector3D& _position3D, const tVector3D& up3D,
        float _radius, float _height, tWorldTime t);
    ~tUnitExplosion();
};

struct tDeposit;
class tBehavior;

//The structure encapsulates one instance of a unit.  It contains a unique
//identifer, a reference to the unit's blueprints (tUnitType), functions for
//controlling the unit, functions for drawing the unit, and several events.
class tUnit
{
    private:
    tUnitType*      type;               //Reference to unit's type
    tModel*         model;              //Reference to unit's model
    
    long            ID;                 //Unique integer identifier
    int             owner;              //Player to which unit belongs

    tUnit*          attacker;           //Unit from which this unit is fleeing

	tUnit*				tractorBeamParent;
	tModelVectorType* 	tractorBeamOrigin;
	tReferenceFrameType* tractorBeamBasis;
	tVector3D			tractorBeamOffset;

    int             abilityCounters[MAX_ABILITIES];
                                        //Does the unit CURRENTLY have each ability? (>0 for true; <= 0 for false)
    bool            *abilitySwitches;   //Is each ability switch currently enabled?

    bool            built;              //Is the unit's incubation period complete? (buildings only)

    float           HP;                 //Hitpoints after last tick
    float           deltaHP;            //Rate at which hitpoints are changing each second
    eCommand        movementMode;       //COMMAND_WANDER, COMMAND_MANEUVER, COMMAND_HOLD_POSITION, COMMAND_EVADE
    eUnitState      state;              //STATE_ALIVE, STATE_SELECTABLE, or STATE_DEAD

    //The z-component of the following vectors must be zero!!
    tVector2D       position;           //Unit's 2D position after last tick
    tVector2D       velocity;           //Unit's velocity after last tick
    tVector2D       acceleration;       //Unit's acceleration after last intervalTick
    tVector2D       p0;                 //Unit's position after last intervalTick
    tVector2D       v0;                 //Unit's velocity after last intervalTick
    tVector2D       f0;                 //Unit's forward vector after last intervalTick
    tVector2D       forward;            //Usu. normalized velocity vector
    float           h0;                 //Unit's health after last intervalTick
    float           speed;              //Unit's speed in units/second (equals |velocity|)
    float           distance;           //The integral of speed (starting at 0)
    float           d0;                 //Unit's distance after last intervalTick
    tWorldTime      t0;                 //Time of last intervalTick

    bool            moving;             //Is the unit's velocity > 0?
    tVector2D       origin;             //Point to which the unit is anchored
    bool            isAnchored;         //Is the unit "anchored" to the origin?

    eCommand        curCommand;         //Command which is currently executing
    int             commandCounter;     //Number of behaviors that are executing this command

    int             commandIndex;       //Index of next command to execute in command queue
    vector<tCommand> commands;          //Queue of commands (current command is NOT in this vector)

    bool            selected;           //Is the unit selected? (typically, selection circle is visible)
    bool            flashing;           //Is the unit's selection circle flashing (i.e. right-click)
    bool            flashOn;            //Is the unit's indicator currently inverted since it's flashing?
    tTime           flashTimer;         //Used to coordinate flashing

    //The engine uses these variables to check synchronization:
    tVector2D       syncPos;            //Unit's position at last sync
    float           syncHP;             //Unit's health at last sync

    //Next unit in assorted linked lists
    long            hashValue;          //The hash linked list to which the unit currently belongs
    tUnit*          nextNeighbor;       //Next unit in hash

    int             group;              //The group to which the unit belongs (-1 for no group)

    //The following variables are not sync-safe!!  Use these for drawing only.
    //The z-component is determined by the terrain.
    tVector3D       position3D;         //Unit's position
    tVector3D       side3D;             //Unit's left side vector (equals up cross forward)
    tVector3D       up3D;               //Unit's up vector (equals forward cross side)
    tVector3D       forward3D;          //Unit's forward vector (equals side cross up)

    //Limited visibility variables:
    float           fieldOfView;        //Unit's current field of view

    //Custom behaviors
    vector<tBehavior *> behaviors;

    void            clearOrders(tWorldTime t);
    void            onCommand(const tCommand& c, tWorldTime t);
    void            transferValues(tWorldTime t);
    void            calculateHash();
    bool            canExecute(eCommand command);

    //Behaviors:
    tVector2D       seekPoint(tVector2D p, float range);
    tVector2D       interceptPoint(tVector2D p, float range);
    tVector2D       avoidPoint(tVector2D p, float range);
    tVector2D       fleePoint(tVector2D p);
    tVector2D       pursueUnit(tUnit *u, float range, bool overshoot);
    tVector2D       avoidUnit(tUnit *u, float range);
    tVector2D       evadeUnit(tUnit *u);
    tVector2D       brake();
    float           wanderSide;
    tVector2D       wander(float dt);

    void            loadNextCommand(tWorldTime t);
    float           getModelProgress();
    float           getModelOpacity() { return type->modelOpacityArg.evaluate(model); }

    public:
    //Data retrieval functions: (see descriptions above)
    tUnitType*      getType()       { return type; }
	tUnit*			getTractorBeamParent() { return tractorBeamParent; }
    tModel*         getModel()      { return model; }
    int             getID()         { return ID; }
    int             getOwner()      { return owner; }
    tUnit*          getAttacker()   { return attacker; }
    float           getHP()         { return HP; }
    eCommand        getMovementMode() { return movementMode; }
    const tVector2D& getPosition()   { return position; }
    const tVector2D& getVelocity()   { return velocity; }
    const tVector2D& getForward()    { return forward; }
    tVector2D       getSide()       { return forward.perpendicularCCW(); }
    float           getSpeed()      { return speed; }
    float           getDistance()   { return distance; }
    bool            isMoving()      { return moving; }
    bool            isSelected()    { return selected; }
    bool            isFlashing()    { return flashing; }
    bool            isFlashOn()     { return flashOn; }

    eUnitState      getState()      { return state; }

    bool            isBuilt()       { return built; }
    eCommand        getCurCommand() { return curCommand; }

    bool            canSee()                    //Can the unit see other units (e.g. not blind)
                        { return true; }
    bool            isVulnerable()              //Can the unit sustain damage? (e.g. not invincible)
                        { return state==STATE_ALIVE; }
    bool            isVisibleToAllies()         //Can allies see the unit?
                        { return state>STATE_DESTROYED; }
    bool            isVisibleToEnemies()        //Can enemies see the unit?
                        { return built || state>STATE_SELECTABLE; }
    bool            isAlwaysVisible() { return type->alwaysVisibleArg.evaluate(model); }
                                                //Can all players see the unit regardless its proximity
                                                //to a cloaking detection unit?
    bool            isUnitFriendly(tUnit *u);   //Is the target's owner allied with this unit's owner?

    eUnitSize       getSize()       { return type->size; }
    bool            repelNearbyUnits();

    //Buildings could override this function to allow the user to target position
    //commands (e.g. ranged bombardment) using the right mouse button.
    eCommand        getDefaultCommand();
    eCommand        getDefaultCommand(tUnit *u);

    //Behaviors may override these functions to accommodate all-terrain vehicles:
    eDomain         getDomain()     { return type->domain; }
    float           getElevation()  { return type->elevationArg.evaluate(model); }
    float           getTurnRate()   { return type->turnRate; }

    float           getMaxHP()      { return type->HP; }

    //Custom behaviors cannot override these two functions.  See tConstructionBehavior to
    //understand why.
    float           getBuildRate()  { return type->buildRate; }
    float           getCost(int i)  { return type->costs[i]; } //CAREFUL!  This function doesn't check i!

    float           getArmor(int i) { return type->armor[i]; } //CAREFUL!  This function doesn't check i!

    float           getMaxVelocity(){ return type->vMax; }
    float           getMaxAccel()   { return type->aMax; }

    bool            isBuilding()    { return type->isBuilding; }

    const tVector2D& getSyncPos()   { return syncPos; }
    float           getSyncHP()     { return syncHP; }
    tUnit*          getNextNeighbor()   { return nextNeighbor; }
    long            getHashValue()  { return hashValue; }
    int             getGroup()      { return group; }

    //The following variables are not sync-safe!  Use them for drawing only.
    const tVector3D& getPosition3D() { return position3D; }
    const tVector3D& getForward3D()  { return forward3D; }
    const tVector3D& getSide3D()     { return side3D; }
    const tVector3D& getUp3D()       { return up3D; }
    tVector3D getCenter()            { return position3D + up3D*(model->getMaxZ()/2); }

    float           getFieldOfView(){ return fieldOfView; }
    float           getAttackRange();

    //BEHAVIOR INTERFACE:
    bool            areCommandsVisible();
    void            setMovementMode(eCommand _movementMode, tWorldTime t);
    void            setCurCommand(eCommand command, tWorldTime t);
    void            resetCurCommand(tWorldTime t);

    void            setFieldOfView(float _fieldOfView);

    bool            isAbilityEnabled(eAbility a);
    void            disableAbility(eAbility a);
    void            enableAbility(eAbility a);

    bool            isFiringAt(tUnit *u);

    void            getRepairPoints(tVector3D& a, tVector3D& b);
    tVector3D       locatePoint(tModelVectorType *mvt)
                        { return pointToWorldCoords(model->locatePoint(mvt)); }
    tVector3D       locateUnitVector(tModelVectorType *mvt)
                        { return vectorToWorldCoords(model->locateUnitVector(mvt)); }
    void            locateReferenceFrame(tReferenceFrameType *rft,
                                         tVector3D& p,
                                         tVector3D& f,
                                         tVector3D& s,
                                         tVector3D& u);

    tVector3D       pointToWorldCoords(const tVector3D& v)
                        { return position3D + forward3D*v.x + side3D*v.y + up3D*v.z; }
    tVector3D       vectorToWorldCoords(const tVector3D& v)
                        { return forward3D*v.x + side3D*v.y + up3D*v.z; }
    tVector3D       pointToUnitCoordinates(const tVector3D& a) {
                        tVector3D b=a-position3D;
                        tVector3D c={b.dot(forward3D),b.dot(side3D),b.dot(up3D)};
                        return c; }
    tVector3D       vectorToUnitCoordinates(const tVector3D& a) {
                        tVector3D b={a.dot(forward3D),a.dot(side3D),a.dot(up3D)};
                        return b; }
    void            runAimProcedure(tAimProcedureType *apt, const tVector3D v, tWorldTime t)
                        { apt->run(model, pointToUnitCoordinates(v), t); }
    void            stopAimProcedure(tAimProcedureType *apt, tWorldTime t)
                        { apt->stop(model, t); }

	void			engageTractorBeam(tUnit *_parent, tModelVectorType *_origin,
									  const tVector3D& _offset);
	void			engageTractorBeam(tUnit *_parent, tReferenceFrameType *_basis,
									  const tVector3D& _offset);
	void			disengageTractorBeam();

    //EXTERNALS:
    //These functions should be called by engine only:
    void            select();
    void            deselect();
    void            flash();

    void            tick(tWorldTime t);
    //I divide the intervalTick into two passes to make the units more predictable.
    //If we call updateState inside intervalTick, some units may regard the unit
    //as selectable whereas others regard the unit as alive.
    void            updateState(tWorldTime t);
    void            intervalTick(tWorldTime t);

    //Up to six passes for drawing:
    //Pass 1: Draw visibility circle
    //Pass 2: Draw 3D model
    //Pass 3: Draw transparent effects such as repair beam
    //Pass 4: Draw 2D interface symbols such as health bar
    //Pass 5: Draw shadows
    //Pass 6: Draw unit statistics (showUnitStats must be enabled)
    void            preDraw();
    void            draw();
    void            postDraw();
    void            drawOverlay();
    void            drawShadow();
    void            drawStats();

    void            drawPickShell(bool convexShell);

    void            processCommand(const tCommand& c, tWorldTime t);
    tUnit(long _ID, int _owner, tUnitType *_type, const tVector2D& _position,
        const tVector2D& _forward, tWorldTime t) throw(int);
    ~tUnit();

    //Events provide an interface with the world:
    //These events must be synchronized and placed either in intervalTick or processCommand.
    private:        //Never call onBirth or onDeath directly; call destroy or kill instead.
    void            onBirth(tWorldTime t);                      //Unit created
    void            onDeath(tWorldTime t);                      //Unit dies
    void            onConception(tWorldTime t);                 //Unit construction begins
    void            onTransfer(int newOwner, tWorldTime t);     //Unit transferred to new owner
    public:
    void            destroy(tWorldTime t);                      //Unit disappears into the void
    void            kill(tWorldTime t);                         //Unit killed by another unit
    void            damage(tUnit *u, float amt, int armorClass, tWorldTime t);
                                                                //Armor class is one of the armor classes defined in the
                                                                //world file.
    void            setPosition(tVector2D pos, tWorldTime t);   //Unit just teleported
    void            setVelocity(tVector2D v, tWorldTime t);     //Unit's velocity just changed
    void            setForward(tVector2D v, tWorldTime t);      //Unit's forward vector just changed
    void            setHP(float HP, tWorldTime t);              //Unit's hitpoints just changed
    void            setOwner(int _owner, tWorldTime t);         //Unit now belongs to a different player
    void            adjustHealRate(tUnit *u, float amt, tWorldTime t);
                                                                //Adjust unit's delta hitpoints
    void            setBuilt(bool _built, tWorldTime t);        //Sets the unit's built flag

    void            onUnitDestroyed(tUnit *u, tWorldTime t);    //A unit was destroyed
    void            onDepositDepleted(tDeposit *d, tWorldTime t); //The deposit to which this unit was linked became depleted

    void            setSyncPos()                    { syncPos=position; }
    void            setSyncHP()                     { syncHP=HP; }
    void            setHashValue(long _hashValue)   { hashValue=_hashValue; }
    void            setNextNeighbor(tUnit* u)       { nextNeighbor=u; }
    void            setGroup(int _group)            { group = _group; }
    void            disbandGroup()                  { group = -1; }

    bool            inBoidNeighborhood (const tVector2D& queryLocation,
                                        const float minDistance,
                                        const float maxDistance,
                                        const float cosMaxAngle);

    //Debugging:
    void            dumpCommands();

    //Experimentation:
    void            spawnShockwave();
    tWorldTime      shockwaveTimer; //Set to -1 if a shockwave does not exist
};

extern tCommandType commandTable[MAX_COMMANDS];
extern int          commandCount;

extern tUnitType*   parseUnitTypeBlock(tStream& s) throw(tString);
extern void         destroyUnitTypes();

extern tUnitType*   unitTypeLookup(const tString& name, bool exact);
extern tUnitType*   unitTypeLookup(int ID);
extern tUnitType*   parseUnitType(const char *fieldName, tStream& s) throw(tString);

extern eCommand     commandLookup(const tString& name, bool exact);
extern eCommand     commandLookup(tUnitType *ut, eParamType paramType);
extern eCommand     parseCommand(const char *fieldName, tStream& s) throw(tString);
extern eCommand     parseImmediateCommand(const char *fieldName, tStream& s) throw(tString);

extern void         lookupUnitTypes() throw(tString);
extern void         updateUnitTypes();

extern eAbility     parseAbility(const char *fieldName, tStream& s) throw(tString);
extern void			parseCommandBlock(tStream& s) throw(tString);
extern eCommand     createCommand(const tString& name, const tString& caption,
                        eParamType paramType, bool immediate, bool benevolent,
                        tKey hotKey[2], int unitID) throw(tString);
extern eCommand     createCommand(const tString& name, const tString& caption, eParamType paramType,
                        bool immediate, bool benevolent) throw(tString);
extern eParamType	parseParamType(const char *fieldName, tStream& s) throw(tString);
extern void			resetCommandTable();
/****************************************************************************/
#endif
