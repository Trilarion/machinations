/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef soundH
#define soundH
/***************************************************************************/

//Includes:
#include "parse.h"

//Defines:
#define SOUND_PATH           "data/sounds/"     //Directory where wav files are stored

//The OpenAL implementation on Linux is slightly different than that of Windows
#if !defined(AL_LOOPING) && defined(AL_SOURCE_LOOPING_LOKI)
    #define AL_LOOPING AL_SOURCE_LOOPING_LOKI
#endif

//Typedefs:
typedef unsigned int iSoundBuffer;
typedef unsigned int iSoundSource;

//Structures:
struct tSoundType
{
    tString         name;
    tString         filename;
    bool            loop;
    iSoundBuffer    buffer;         //Handle of an OpenAL buffer
    int             quota;          //How many sources can use this buffer?
    int             instances;      //How many sources use this buffer?
    tWorldTime      lastTime;       //When was the last instance of this sound type created?
    tSoundType(tStream& s) throw(tString);
};

//Externals:
extern void soundStartup();
extern void soundShutdown();
extern bool loadSoundTypes(const char *filename);
extern void destroySoundTypes();

extern iSoundSource sndNewSource(tSoundType *type, const tVector3D& p, tWorldTime t);
extern void         sndDeleteSource(iSoundSource index);
extern bool         sndIsFinished(iSoundSource index);
extern void         sndPositionSource(iSoundSource index, const tVector3D& p);
extern void         sndPositionListener(const tVector3D& p);

extern tSoundType*  soundTypeLookup(const tString& name,bool exact);
extern tSoundType*  parseSoundType(const char *fieldName, tStream& s) throw(tString);
/***************************************************************************/
#endif
