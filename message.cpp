/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* MESSAGE.CPP:  Packages and unpackages network messages.                   *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#include "message.h"
#pragma package(smart_init)

/***************************************************************************\
tMessage::getMessage

Snarf a message from the input stream and parse the data into the appropriate
descendent of tMessage.  The function will allocate memory for a message object
and return a pointer to it.  It is the caller's responsibility to deallocate
the memory when finished.  The caller can determine where the next message begins
using bytesRead.

Inputs:
    buffer      Stream containing the packaged message
    length      Length of stream
Outputs:
    If the function succeeds, it stores the number of characters read from the
    stream in bytesRead and returns a pointer to the new message object.

    If length <= 0 or the stream contains an incomplete message, it stores 0
    in bytesRead and returns NULL.
\***************************************************************************/

/* I use this macro to avoid several dozen repetitious cases in getMessage.
 * The macro allocates memory for the appropriate message object and attempts
 * to unpackage the data.  If it fails, it deallocates the memory and ultimately
 * returns a NULL pointer.  Although one could implement this macro as a function,
 * I prefer the convenience of "passing" a structure directly to the macro.
 */

#define GET_MESSAGE_CASE(TYPE,type)  case TYPE: \
    pointer = mwNew type; \
    bytesRead = pointer->unpackageMessage(buffer,length); \
    if(bytesRead == 0) \
    { \
        mwDelete pointer; \
        pointer=NULL; \
    } \
    break;

tMessage *tMessage::getMessage(const char *buffer, int length, int& bytesRead)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(buffer))
        return NULL;
#endif

    bytesRead=0;
    tMessage *pointer = NULL;
    //If the stream is empty, abort with a NULL pointer.
    if(length <= 0)
        return NULL;

    //Select the appropriate message structure based on the first character
    //in the stream.
    switch(*buffer)
    {
        GET_MESSAGE_CASE(MSG_REFUSE,              tRefuseMsg)
        GET_MESSAGE_CASE(MSG_HOST_ADDRESS,        tHostAddressMsg)
        GET_MESSAGE_CASE(MSG_USER_DATA,           tUserDataMsg)
        GET_MESSAGE_CASE(MSG_ASSIGN,              tAssignMsg)
        GET_MESSAGE_CASE(MSG_CONNECT_USER,        tConnectUserMsg)
        GET_MESSAGE_CASE(MSG_ADD_USER,            tAddUserMsg)
        GET_MESSAGE_CASE(MSG_ID,                  tIDMsg)
        GET_MESSAGE_CASE(MSG_DROP_USER,           tDropUserMsg)
        GET_MESSAGE_CASE(MSG_TEXT,                tTextMsg)
        GET_MESSAGE_CASE(MSG_SET_TIMING,          tSetTimingMsg)
        GET_MESSAGE_CASE(MSG_SET_TIMING_IN_GAME,  tSetTimingMsg)
        GET_MESSAGE_CASE(MSG_TIMING_DIVIDER,      tSetTimingMsg)
        GET_MESSAGE_CASE(MSG_END_INTERVAL,        tGameMsg)
        GET_MESSAGE_CASE(MSG_PING,                tPingMsg)
        GET_MESSAGE_CASE(MSG_PONG,                tPongMsg)
        GET_MESSAGE_CASE(MSG_FILE_TRANSFER,       tFileTransferMsg)
        GET_MESSAGE_CASE(MSG_FILE_PACKET,         tDataMsg)
        GET_MESSAGE_CASE(MSG_CHOOSE_FILES,        tChooseFilesMsg)
        GET_MESSAGE_CASE(MSG_ACK_FILES,           tAckFilesMsg)
        GET_MESSAGE_CASE(MSG_SET_PLAYER,          tSetPlayerMsg)
        GET_MESSAGE_CASE(MSG_SET_ALLIANCE,        tSetAllianceMsg)     
        GET_MESSAGE_CASE(MSG_SET_RESOURCE,        tSetResourceMsg)
        GET_MESSAGE_CASE(MSG_SYNC,                tSyncMsg)
        GET_MESSAGE_CASE(MSG_SYNC_QUERY,          tGameMsg)

        GET_MESSAGE_CASE(MSG_COMMAND,             tCommandMsg)
        GET_MESSAGE_CASE(MSG_COMMAND_POS,         tCommandMsg)
        GET_MESSAGE_CASE(MSG_COMMAND_UNIT,        tCommandMsg)
        GET_MESSAGE_CASE(MSG_COMMAND_PRODUCTION,  tCommandMsg)

        GET_MESSAGE_CASE(MSG_UNIT_PLAN,           tUnitCreateMsg)
        GET_MESSAGE_CASE(MSG_UNIT_CREATE,         tUnitCreateMsg)
        GET_MESSAGE_CASE(MSG_UNIT_DESTROY,        tUnitMsg)
        GET_MESSAGE_CASE(MSG_UNIT_SET_OWNER,      tUnitLongMsg)
        GET_MESSAGE_CASE(MSG_UNIT_SET_POS,        tUnitVector2DMsg)
        GET_MESSAGE_CASE(MSG_UNIT_SET_VELOCITY,   tUnitVector2DMsg)
        GET_MESSAGE_CASE(MSG_UNIT_SET_FORWARD,    tUnitVector2DMsg)
        GET_MESSAGE_CASE(MSG_UNIT_SET_HP,         tUnitFloatMsg)

        GET_MESSAGE_CASE(MSG_DEPOSIT_CREATE,      tDepositCreateMsg)
        GET_MESSAGE_CASE(MSG_DEPOSIT_DESTROY,     tDepositDestroyMsg)
        GET_MESSAGE_CASE(MSG_DEPOSIT_SET,         tDepositSetMsg)

        //UDP ONLY
        GET_MESSAGE_CASE(MSG_RESEND,              tResendMsg)
        GET_MESSAGE_CASE(MSG_ACK_PACKETS,         tAckPacketsMsg)
        GET_MESSAGE_CASE(MSG_PACKET_RESENT,       tDataMsg)

        //Some messages only consist of one character.  In this case, we can
        //use a generic tMessage object.  Since the message only has one
        //character (the type), unpackageMessage is guaranteed to succeed.
        default:
            bytesRead = 1;
            pointer = mwNew tMessage;
            pointer->unpackageMessage(buffer, 1);
    }
    return pointer;
}

/***************************************************************************\
tMessage::duplicateMessage

Create a duplicate of the message on the heap.  It is the caller's
responsibility to deallocate the memory block.
Inputs:
    message     The message to duplicate
Outputs:
    A pointer to the duplicate
\***************************************************************************/
#define DUPLICATE_CASE(x,y) \
    case x: \
        m=mwNew y(*((y*)message)); \
        return m;

tMessage *tMessage::duplicateMessage(tMessage *message)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(message))
        return NULL;
#endif
    tMessage *m;
    switch(message->type)
    {
        DUPLICATE_CASE(MSG_REFUSE,              tRefuseMsg)
        DUPLICATE_CASE(MSG_HOST_ADDRESS,        tHostAddressMsg)
        DUPLICATE_CASE(MSG_USER_DATA,           tUserDataMsg)
        DUPLICATE_CASE(MSG_ASSIGN,              tAssignMsg)
        DUPLICATE_CASE(MSG_CONNECT_USER,        tConnectUserMsg)
        DUPLICATE_CASE(MSG_ADD_USER,            tAddUserMsg)
        DUPLICATE_CASE(MSG_ID,                  tIDMsg)
        DUPLICATE_CASE(MSG_DROP_USER,           tDropUserMsg)
        DUPLICATE_CASE(MSG_TEXT,                tTextMsg)
        DUPLICATE_CASE(MSG_SET_TIMING,          tSetTimingMsg)
        DUPLICATE_CASE(MSG_SET_TIMING_IN_GAME,  tSetTimingMsg)
        DUPLICATE_CASE(MSG_TIMING_DIVIDER,      tSetTimingMsg)
        DUPLICATE_CASE(MSG_END_INTERVAL,        tGameMsg)
        DUPLICATE_CASE(MSG_PING,                tPingMsg)
        DUPLICATE_CASE(MSG_PONG,                tPongMsg)
        DUPLICATE_CASE(MSG_FILE_TRANSFER,       tFileTransferMsg)
        DUPLICATE_CASE(MSG_FILE_PACKET,         tDataMsg)
        DUPLICATE_CASE(MSG_CHOOSE_FILES,        tChooseFilesMsg)
        DUPLICATE_CASE(MSG_ACK_FILES,           tAckFilesMsg)
        DUPLICATE_CASE(MSG_SET_PLAYER,          tSetPlayerMsg)
        DUPLICATE_CASE(MSG_SET_ALLIANCE,        tSetAllianceMsg)
        DUPLICATE_CASE(MSG_SET_RESOURCE,        tSetResourceMsg)
        DUPLICATE_CASE(MSG_SYNC,                tSyncMsg)
        DUPLICATE_CASE(MSG_SYNC_QUERY,          tGameMsg)

        DUPLICATE_CASE(MSG_COMMAND,             tCommandMsg)
        DUPLICATE_CASE(MSG_COMMAND_POS,         tCommandMsg)
        DUPLICATE_CASE(MSG_COMMAND_UNIT,        tCommandMsg)
        DUPLICATE_CASE(MSG_COMMAND_PRODUCTION,  tCommandMsg)

        DUPLICATE_CASE(MSG_UNIT_PLAN,           tUnitCreateMsg)
        DUPLICATE_CASE(MSG_UNIT_CREATE,         tUnitCreateMsg)
        DUPLICATE_CASE(MSG_UNIT_DESTROY,        tUnitMsg)
        DUPLICATE_CASE(MSG_UNIT_SET_OWNER,      tUnitLongMsg)
        DUPLICATE_CASE(MSG_UNIT_SET_POS,        tUnitVector2DMsg)
        DUPLICATE_CASE(MSG_UNIT_SET_VELOCITY,   tUnitVector2DMsg)
        DUPLICATE_CASE(MSG_UNIT_SET_FORWARD,    tUnitVector2DMsg)
        DUPLICATE_CASE(MSG_UNIT_SET_HP,         tUnitFloatMsg)

        DUPLICATE_CASE(MSG_DEPOSIT_CREATE,      tDepositCreateMsg)
        DUPLICATE_CASE(MSG_DEPOSIT_DESTROY,     tDepositDestroyMsg)
        DUPLICATE_CASE(MSG_DEPOSIT_SET,         tDepositSetMsg)

        //UDP ONLY
        DUPLICATE_CASE(MSG_RESEND,              tResendMsg)
        DUPLICATE_CASE(MSG_ACK_PACKETS,         tAckPacketsMsg)
        DUPLICATE_CASE(MSG_PACKET_RESENT,       tDataMsg)
    }
    bug("tMessage::duplicateMessage: invalid tag");
    return NULL;
}

/***************************************************************************\
tMessage::dumpMessage

Dump the contents of a message to an output file for debugging.  The data
appears as a string of space-delimited decimal values.  For instance:
MESSAGE DATA: 11 12 13 14 15
Inputs:
    message     The message to examine
Outputs:
    none
\***************************************************************************/
void tMessage::dumpMessage(tMessage *message)
{
    //NOTE: Messages above 2500 bytes may cause the buffer to overflow.
    char buf1[BUFFER_SIZE], buf2[BUFFER_SIZE];
    int bytes;
    tBufferIterator ptr(buf2, sizeof(buf2));

#ifdef DEBUG
    if(MEMORY_VIOLATION(message))
        return;
#endif

    bytes = message->packageMessage(buf1, sizeof(buf1));
    try
    {
        mySprintf(ptr, "MESSAGE DATA:");

        //Copy each value to the end of the string.
        for(int i=0; i<bytes; i++)
            mySprintf(ptr, " %d", buf1[i]);
    }
    catch(int x)
    {
        bug("tMessage::dumpMessage: buffer overflow");
    }

    //Write the string to the output file.
    log(buf2);
}
