/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* CLIENT01.CPP: Provides a standard OpenGL implementation for the controls  *
*               in client.cpp                                               *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#include "client01.h"
#include "font.h"
#include "memwatch.h"
#include "unit.h"
#include "oglwrap.h"
#pragma package(smart_init)

/****************************************************************************\
tStdLabel::tStdLabel

Creates and initializes a new standard label.  If you call setCaption, the label's
lower-left coordinates will not change.

Inputs:
    controlX, controlY      Coordinates of caption's lower-left corner
    caption                 Caption to display
    fontIndex               Font with which to draw caption
    r, g, b                 Color in which to draw caption
\****************************************************************************/
tStdLabel::tStdLabel(int controlX, int controlY, const tString& _caption, int _fontIndex,
    float _r, float _g, float _b) : tStatic(controlX, controlY, stringWidth(CHARPTR(_caption),_fontIndex),
    fontHeight(_fontIndex)), fontIndex(_fontIndex), caption(_caption), r(_r), g(_g), b(_b),
    cx(0), cy(0), centered(false)
{
    type=CTRL_STD_LABEL;
}

/****************************************************************************\
tStdLabel::tStdLabel

Alternate constructor that automatically centers the caption within the
specified rectangle.  The caption may overlap the rectangle on one or more sides.
If you call setCaption, the function will center the new caption inside the control.

Inputs:
    controlX, controlY      Coordinates of rectangle's lower-left corner
    width, height           Dimensions of rectangle
    caption                 Caption to display
    fontIndex               Font with which to draw caption
    r, g, b                 Color in which to draw caption
\****************************************************************************/
tStdLabel::tStdLabel(int controlX, int controlY, int width, int height, const tString& _caption, int _fontIndex,
    float _r, float _g, float _b) : tStatic(controlX, controlY, width, height),
    fontIndex(_fontIndex), caption(_caption), r(_r), g(_g), b(_b), centered(true)
{
    cx=(width-stringWidth(CHARPTR(caption),fontIndex))/2;
    cy=(height-fontHeight(_fontIndex))/2;
    type=CTRL_STD_LABEL;
}

/****************************************************************************\
tStdLabel::drawControl

Changes the color and branches to drawString with the proper parameters.
\****************************************************************************/
void tStdLabel::drawControl()
{
    glColor3f(r,g,b);
    drawString(CHARPTR(caption), getControlX()+cx, getControlY()+cy, fontIndex);
}

/****************************************************************************\
tStdLabel::setCaption

Assigns a new caption to the label.  If the label is centered, then only the caption's
position will change.  If the label is not centered, then the the control
will resize itself.

Inputs:
    caption     New caption to display
Outputs:
    none
\****************************************************************************/
void tStdLabel::setCaption(const tString& _caption)
{
    caption = _caption;
    if(centered)
    {
        cx=(getWidth()-stringWidth(CHARPTR(caption),fontIndex))/2;
        cy=(getHeight()-fontHeight(fontIndex))/2;
    }
    else
        tStatic::setSize(stringWidth(CHARPTR(caption), fontIndex), fontHeight(fontIndex));
}

/****************************************************************************\
tStdLabel::setSize

The size of this control must match the size of the caption.  Hence, this
function prevents anyone from adjusting the size outside the class.
\****************************************************************************/
void tStdLabel::setSize(int x, int y)
{
    bug("tStdLabel::setSize: this control cannot be resized");
}



/****************************************************************************\
tStdButton::tStdButton

Creates and initializes a new standard button.  Centers the caption within the
button.

Inputs:
    controlX, controlY      Coordinates of button's lower-left corner
    width, height           Dimensions of button
    caption                 Caption to display within the button
    fontIndex               Font with which to draw caption
    canFocus                Can the button receive focus?
    event                   Pointer to a function of the form 'void (tControl *)'
                                that will execute when the button is triggered
\****************************************************************************/
tStdButton::tStdButton(int controlX, int controlY, int width, int height,
    const tString& _caption, int _fontIndex, bool canFocus, tEvent _event) :
    tButton(controlX, controlY, width, height, canFocus, /* executeOnDown = */ false),
    fontIndex(_fontIndex), caption(_caption), event(_event)
{
    type=CTRL_STD_BUTTON;
    refresh();
}

/****************************************************************************\
tStdButton::drawControl

Sets the current color according to button status: gray if disabled, white if
selected or focused, cyan when normal.  Branches to drawString, passing the
proper parameters.
\****************************************************************************/
void tStdButton::drawControl()
{
    if(isDisabled())
        glColor3f(.5,.5,.5); //Gray
    else if(isSelected() || isFocused())
        glColor3f(1,1,1); //White
    else
        glColor3f(0,1,1); //Cyan
    //We calculate captionX and CaptionY in refresh every time the caption changes.
    drawString(CHARPTR(caption), getControlX()+captionX, getControlY()+captionY, fontIndex);

/*  Old code for drawing a Windows-style button:

    int cx=captionX, cy=captionY;
    if(isDepressed())
    {
        drawButton(getControlX(),getControlY(),getWidth(),getHeight(),bevel,.6,.6,.6,.8,.8,.8,.7,.7,.7); //Depressed
        cx++;
        cy--;
    }
    else if (isSelected() || isFocused())
        drawButton(getControlX(),getControlY(),getWidth(),getHeight(),bevel,1,1,1,.6,.6,.6,.8,.8,.8); //Selected
    else
    {
        drawButton(getControlX(),getControlY(),getWidth(),getHeight(),bevel,.9,.9,.9,.5,.5,.5,.7,.7,.7); //Normal
    }
    glColor3f(0,0,0);
    drawString(CHARPTR(caption), getControlX() + cx, getControlY() + cy, fontIndex);
*/
}

/****************************************************************************\
tStdButton::refresh

Centers the caption within the button.  Function calculates the caption's offset
from the lower-left corner and stores it in captionX and captionY.
\****************************************************************************/
void tStdButton::refresh()
{
    captionX = (getWidth() - stringWidth(CHARPTR(caption), fontIndex)) / 2;
    captionY = (getHeight() - fontHeight(fontIndex)) / 2;
}

/****************************************************************************\
tStdButton::onSetSize

Reposition the caption each time the dimensions change.
\****************************************************************************/
void tStdButton::onSetSize(int x, int y)
{
    tButton::onSetSize(x,y);
    refresh();
}

/****************************************************************************\
tStdButton::onExecute

Implements the onExecute event defined in tButton.  The function checks to
see if the control belongs to a window and references a valid event handler.
If so, it branches to the event handler.
\****************************************************************************/
void tStdButton::onExecute()
{
    if(getParent() && getParent()->isType(CTRL_WINDOW) && event)
        (((tWindow *)getParent())->*event)(this);
}

/****************************************************************************\
tStdButton::setCaption

Assigns a new caption to the button and centers it.

Inputs:
    caption         New caption
Outputs:
    none
\****************************************************************************/
void tStdButton::setCaption(const tString& _caption)
{
    caption = _caption;
    refresh();
}

/****************************************************************************\
tStdButton::setEvent

Assigns a new event to the button.

Inputs:
    event           New event
Outputs:
    none
\****************************************************************************/
void tStdButton::setEvent(tEvent _event)
{
    event=_event;
}



/****************************************************************************\
tWindowButton::tWindowButton

Creates and initializes a window button.  A window button controls a dialog box,
performing such actions as minimize, close, etc.  The action determines its
appearance.  BUTTON_SIZE determines its width and height (plus a two pixel
margin on all four sides).  The control cannot receive focus.

Inputs:
    controlX, controlY      Coordinates of lower-left corner
    action                  WNDACT_MINIMIZE, WNDACT_RESTORE, WNDACT_MAXIMIZE, or WNDACT_CLOSE
    event                   Function to call when the user triggers the button
\****************************************************************************/
tWindowButton::tWindowButton(int controlX, int controlY, eWindowAction _action,
    tEvent _event) : tButton(controlX, controlY, BUTTON_SIZE+4, BUTTON_SIZE+4,
    /* canFocus = */ false, /* executeOnDown = */ false), action(_action),
    event(_event), texture(NULL)
{
    type=CTRL_WINDOW_BUTTON;
    refreshTexture();
}

/****************************************************************************\
tWindowButton::onExecute

Same as tButton::onExecute, except the function expects the control to belong
to a dialog box.
\****************************************************************************/
void tWindowButton::onExecute()
{
    if(getParent() && getParent()->isType(CTRL_DIALOG_BOX) && event)
        (((tDialogBox *)getParent())->*event)(this);
}

/****************************************************************************\
tWindowButton::setAction

Call this function to change the button's action.  The function assigns the
new action and replaces the old icon with a new one.

Inputs:
    action      New action
Outputs:
    none
\****************************************************************************/
void tWindowButton::setAction(eWindowAction _action)
{
    action=_action;
    //Discard the old icon:
    if(texture)
        releaseTexture(texture);
    texture=NULL;
    //Retrieve a new icon:
    refreshTexture();
}

/****************************************************************************\
tWindowButton::setHandler

Assigns a new handler from outside the class.

Inputs:
    event       New handler
Outputs:
    none
\****************************************************************************/
void tWindowButton::setHandler(tEvent _event)
{
    event=_event;
}

/****************************************************************************\
tWindowButton::drawControl

Draws a Windows-style button and pastes the icon over it.  The button is a shade
brighter when selected or focused.  The icon is shifted one pixel down and
one pixel to the right when the button is depressed.
\****************************************************************************/
void tWindowButton::drawControl()
{
    int x, y;
    if(isDepressed())
    {
        drawButton(getControlX(),getControlY(),getWidth(),getHeight(),
            /* bevel */                         2,
            /* RGB for left/top edges */        .6,.6,.6,
            /* RGB for right/bottom edges */    .8,.8,.8,
            /* RGB for button face */           .7,.7,.7);
        x=getControlX()+3;
        y=getControlY()+1;
    }
    else if (isSelected() || isFocused())
    {
        drawButton(getControlX(),getControlY(),getWidth(),getHeight(),2,1,1,1,.6,.6,.6,.8,.8,.8);
        x=getControlX()+2;
        y=getControlY()+2;
    }
    else
    {
        drawButton(getControlX(),getControlY(),getWidth(),getHeight(),2,.9,.9,.9,.5,.5,.5,.7,.7,.7);
        x=getControlX()+2;
        y=getControlY()+2;
    }

    if(texture)
    {
        glColor3f(1,1,1);
        drawTexture(texture, x, y);
    }
}

/****************************************************************************\
tWindowButton::refreshTexture

Retrieves a reference to the proper icon via getTexture.  Call this function
each time the action changes.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tWindowButton::refreshTexture()
{
    switch(action)
    {
        case WNDACT_MINIMIZE:
            texture=getTexture("Buttons.Minimize");
            break;
        case WNDACT_RESTORE:
            texture=getTexture("Buttons.Restore");
            break;
        case WNDACT_MAXIMIZE:
            texture=getTexture("Buttons.Maximize");
            break;
        case WNDACT_CLOSE:
            texture=getTexture("Buttons.Close");
            break;
    }
}

/****************************************************************************\
tWindowButton::~tWindowButton

Frees the reference to the texture via releaseTexture.
\****************************************************************************/
tWindowButton::~tWindowButton()
{
    if(texture)
        releaseTexture(texture);
}



/****************************************************************************\
tStdToggle::tStdToggle

Creates and initializes a new standard toggle.  The toggle consists a square
check box followed by a caption.  The width and height of the checkbox match
the height of the control.  The caption is separated from the checkbox by a
five pixel margin.

Inputs:
    controlX, controlX      Coordinates of lower-left corner of checkbox
    caption                 Caption
    fontIndex               Font with which to draw caption
    r, g, b                 Color in which to draw caption
    canFocus                Can the control receive focus?
    isChecked               Is the control initialled checked?
\****************************************************************************/
tStdToggle::tStdToggle(int controlX, int controlY, const tString& _caption, int _fontIndex,
    float _r, float _g, float _b, bool canFocus, bool isChecked, tEvent _event) :
    tToggle(controlX, controlY, fontHeight(_fontIndex)+5+stringWidth(CHARPTR(_caption),_fontIndex),
    fontHeight(_fontIndex), canFocus, isChecked), caption(_caption), fontIndex(_fontIndex), r(_r), g(_g), b(_b),
    event(_event)
{
    type=CTRL_STD_TOGGLE;
}

/****************************************************************************\
tStdToggle::drawControl

The checkbox appears a shade brighter if selected or focused.  The checkbox
contains a white 'X' if checked.  The caption proceeds the checkbox.
\****************************************************************************/
void tStdToggle::drawControl()
{
    int a=fontHeight(fontIndex);
    int x=getControlX(), y=getControlY();

    if(isSelected() || isFocused())
        drawButton(x, y, a,a,2,.5,.5,.5,1,1,1,0,0,0);
    else
        drawButton(x, y, a,a,2,.4,.4,.4,.9,.9,.9,0,0,0);

/* Old code for drawing solid white border
    if(isSelected() || isFocused())
        glColor3f(1,1,1);
    else
        glColor3f(.8,.8,.8);

    //Draw a white square from (x,y) to (x+a,y+a) that is two pixels wide:
    glBegin(GL_QUADS);
        glVertex2i(x,y);
        glVertex2i(x+a,y);
        glVertex2i(x+a,y+a);
        glVertex2i(x,y+a);
        glColor3f(0,0,0);
        glVertex2i(x+2,y+2);
        glVertex2i(x+a-2,y+2);
        glVertex2i(x+a-2,y+a-2);
        glVertex2i(x+2,y+a-2);
    glEnd();
*/
    //Draw a white 'X' inside the box if checked.  Each stroke is three pixels wide:
    if(isChecked())
    {
        glColor3f(1,1,1);
        glLineWidth(3);
        glBegin(GL_LINES);
            glVertex2i(x+4,y+4);
            glVertex2i(x+a-4,y+a-4);

            glVertex2i(x+a-4,y+4);
            glVertex2i(x+4,y+a-4);
        glEnd();
        glLineWidth(1);
    }
    //Render the caption with the proper font and color via drawString:
    glColor3f(r,g,b);
    drawString(CHARPTR(caption),getControlX()+getHeight()+5,getControlY(),fontIndex);
}

/****************************************************************************\
tStdToggle::setCaption

Assigns a new caption from outside the class.  The function automatically
resizes the control accordingly.

Inputs:
    caption     New caption
Outputs:
    none
\****************************************************************************/
void tStdToggle::setCaption(const tString& _caption)
{
    caption=_caption;
    //This is the smallest rectangle that will enclose both checkbox and caption:
    tToggle::setSize(fontHeight(fontIndex)+5+stringWidth(CHARPTR(caption),fontIndex),
        fontHeight(fontIndex));
}

/****************************************************************************\
tStdToggle::setSize

Prevents anyone from explicitly resizing the control.  The control's size always
equals the smallest rectangle that will enclose both the checkbox and the caption.
\****************************************************************************/
void tStdToggle::setSize(int x, int y)
{
    bug("tStdToggle::setSize: this control cannot be resized");
}



/****************************************************************************\
tStdRadio::tStdRadio

Creates and initializes a new standard radio button.  A standard radio button
essentially works the same way as a standard toggle, except that it replaces
the checkbox with a ring.  The ring contains a dot when the control is 'checked'.
Also, when the user checks the radio button, all of the radio buttons in the
same window belonging to the same group will become unchecked.

Inputs:
    controlX        Left edge of ring
    controlY        Bottom edge of ring
    caption         Caption that proceeds ring
    fontIndex       Font with which to draw caption
    r, g, b         Color in which to draw caption
    canFocus        Can this control receive focus?
    isChecked       Is the control initially 'checked'?
    group           Unique identifier shared by all radio buttons in same group
\****************************************************************************/
tStdRadio::tStdRadio(int controlX, int controlY, const tString& _caption, int _fontIndex,
        float _r, float _g, float _b, bool canFocus, bool isChecked, int group, tEvent _event) :
    tRadio(controlX, controlY, fontHeight(_fontIndex)+5+stringWidth(CHARPTR(_caption),_fontIndex),
    fontHeight(_fontIndex), canFocus, isChecked, group), caption(_caption),
    fontIndex(_fontIndex), r(_r), g(_g), b(_b), event(_event)
{
    type=CTRL_STD_RADIO;
    //We need this quadric object to render the ring.
    quad=gluNewQuadric();
}

/****************************************************************************\
tStdRadio::drawControl

The ring appears a shade brighter if selected or focused.  The ring contains
a smaller circle if checked.  The caption proceeds the radio button.
\****************************************************************************/
void tStdRadio::drawControl()
{
    float rad=fontHeight(fontIndex)/2;
    int x=getControlX(), y=getControlY();

    //Draw a ring that fills the left end of the control.  The ring's thickness
    //is 20% of its radius.
    glPushMatrix();
    glTranslatef(x+rad,y+rad,0);
    if (isSelected() || isFocused())
        glColor3f(1,1,1);
    else
        glColor3f(.9,.9,.9);
    gluDisk(quad,0,rad,20,1);

    glPushMatrix();
    glTranslatef(-rad/10,rad/10,0);
    if (isSelected() || isFocused())
        glColor3f(.5,.5,.5);
    else
        glColor3f(.4,.4,.4);
    gluDisk(quad,0,rad*0.9,20,1);
    glPopMatrix();

    glColor3f(0,0,0);
    gluDisk(quad,0,rad*0.8,20,1);

/*  Old code for drawing a box instead of a ring:
    glBegin(GL_QUADS);
        glVertex2i(x,y);
        glVertex2i(x+a,y);
        glVertex2i(x+a,y+a);
        glVertex2i(x,y+a);
        glColor3f(0,0,0);
        glVertex2i(x+2,y+2);
        glVertex2i(x+a-2,y+2);
        glVertex2i(x+a-2,y+a-2);
        glVertex2i(x+2,y+a-2);
    glEnd();
*/

    //If the radio button is checked, fill the ring with a circle whose radius
    //is 60% that of the ring.
    if(isChecked())
    {
        glColor3f(1,1,1);
        gluDisk(quad,0,rad*.6,20,1);

    /* Old code for filling the box with a smaller square:
        glBegin(GL_QUADS);
            glVertex2i(x+3,y+3);
            glVertex2i(x+a-3,y+3);
            glVertex2i(x+a-3,y+a-3);
            glVertex2i(x+3,y+a-3);
        glEnd();
    */
    }

    //Render the caption with the proper font and color via drawString:
    glPopMatrix();
    glColor3f(r,g,b);
    drawString(CHARPTR(caption),getControlX()+getHeight()+5,getControlY(),fontIndex);
}

/****************************************************************************\
tStdRadio::~tStdRadio

Free the quadric object that we initialized in the constructor.
\****************************************************************************/
tStdRadio::~tStdRadio()
{
    gluDeleteQuadric(quad);
}

/****************************************************************************\
tStdRadio::setCaption

Assigns a new caption and resizes the control accordingly.  The function
calculates the smallest rectangle that will enclose both the ring and the caption.

Inputs:
    caption     New caption
Outputs:
    none
\****************************************************************************/
void tStdRadio::setCaption(const tString& _caption)
{
    caption=_caption;
    tToggle::setSize(fontHeight(fontIndex)+5+stringWidth(CHARPTR(caption),fontIndex),
        fontHeight(fontIndex));
}

/****************************************************************************\
tStdRadio::setSize

Prevents anyone from explicitly resizing the control.  The control's size always
equals the smallest rectangle that will enclose both the ring and the caption.
\****************************************************************************/
void tStdRadio::setSize(int x, int y)
{
    bug("tStdRadio::setSize: this control cannot be resized");
}



/****************************************************************************\
tStdScrollBar::tStdScrollBar

Creates and initializes a new standard scroll bar that mimics Windows.  The
scroll bar consists of two arrow buttons at either end, a sliding marker,
and a gray area in between.  The BREADTH constant determines the width (for
vertical) or height (for horizontal).  That arrows buttons are
square and contain a black triangle indicating the direction of motion.
The marker's length indicates the amount of the medium currently visible.
It's length will not shrink below BREADTH, however.

Inputs:
    controlX, controlY      Coordinates of the lower-left corner
    length                  Height (for vertical scroll bars) or
                            Width (for horiztonal scroll bars)
    canFocus                Can the control receive focus?
    isVertical              Is the scroll bar vertical?
    position                Initial position within medium
    range                   Length of medium
    pageSize                Amount of medium visible at one time
                                (or zero if scroll bar does not control window)
    scrollRate              Amount by which to adjust position when user presses arrow
\****************************************************************************/
tStdScrollBar::tStdScrollBar(int controlX, int controlY, int length, bool canFocus,
    bool isVertical, long position, long range, long pageSize, long scrollRate, tEvent _event) :
    tScrollBar(controlX, controlY, isVertical ? BREADTH : length,
    isVertical ? length : BREADTH, canFocus, isVertical, position,
    range, pageSize, scrollRate, /* arrowLength = */ BREADTH,
    /* minMarkerLength = */ BREADTH), event(_event)
{
    type=CTRL_STD_SCROLL_BAR;
}

//Vertical scroll bars are 20 pixels wide; Horizontal scroll bars are 20 pixels high
const int tStdScrollBar::BREADTH=20;

/****************************************************************************\
tStdScrollBar::drawControl

Draws a Windows-style scroll bar.
\****************************************************************************/
void tStdScrollBar::drawControl()
{
    float RGBoffset;
    int x=getControlX(),y=getControlY(),x2,y2;

    if(isDisabled())
    {
        //Only draw the scroll bar if there is enough space to squeeze in the arrow buttons.
        if(trackLength()>=0)
        {
            //Coat the control with dark gray:
            glColor3f(.4,.4,.4);
            glBegin(GL_QUADS);
                glVertex2i(x,y);
                glVertex2i(x+getWidth(),y);
                glVertex2i(x+getWidth(),y+getHeight());
                glVertex2i(x,y+getHeight());
            glEnd();

            //Draw the bottom/left button:
            x2=x; y2=y;
            drawButton(x2,y2,BREADTH, BREADTH,
                /* bevel */                         2,
                /* RGB for left/top edges */        .9,.9,.9,
                /* RGB for right/bottom edges */    .5,.5,.5,
                /* RGB for button face */           .7,.7,.7);

            //Draw arrow in a slightly darker shade of gray indicating that the
            //button is disabled.
            glColor3f(.45,.45,.45);
            glBegin(GL_TRIANGLES);
                if(isVertical())
                {
                    glVertex2i(x2+6, y2+BREADTH-6);     //Down arrow
                    glVertex2i(x2+BREADTH-6, y2+BREADTH-6);
                    glVertex2i(x2+BREADTH/2, y2+6);
                }
                else
                {
                    glVertex2i(x2+BREADTH-6, y2+6);     //Left arrow
                    glVertex2i(x2+BREADTH-6, y2+BREADTH-6);
                    glVertex2i(x2+6, y2+BREADTH/2);
                }
            glEnd();

            if(isVertical())
            {
                //Move up.
                x2=x;
                y2=y+getHeight()-BREADTH;
            }
            else
            {
                //Move right.
                x2=x+getWidth()-BREADTH;
                y2=y;
            }

            //Draw the top/right button:
            drawButton(x2,y2,BREADTH, BREADTH, 2, .9,.9,.9,.5,.5,.5,.7,.7,.7);

            //Draw second arrow:
            glColor3f(.45,.45,.45);
            glBegin(GL_TRIANGLES);
                if(isVertical())
                {
                    glVertex2i(x2+6, y2+6);     //Up arrow
                    glVertex2i(x2+BREADTH-6, y2+6);
                    glVertex2i(x2+BREADTH/2, y2+BREADTH-6);
                }
                else
                {
                    glVertex2i(x2+6, y2+6);     //Right arrow
                    glVertex2i(x2+6, y2+BREADTH-6);
                    glVertex2i(x2+BREADTH-6, y2+BREADTH/2);
                }
            glEnd();
        }
        return;
    }

    //The following lines will execute only if the control is enabled.
    //We want to draw the scroll bar a shade brighter if it is selected or focused.
    if (isSelected() || isFocused())
        RGBoffset=.1;
    else
        RGBoffset=0;

    //When the user clicks in the gray area, we want to draw it slightly brighter.
    glBegin(GL_QUADS);
        if(isFastIncDepressed())
            glColor3f(.5+RGBoffset,.5+RGBoffset,.5+RGBoffset);
        else
            glColor3f(.4+RGBoffset,.4+RGBoffset,.4+RGBoffset);
        if(isVertical())
        {
            //Area beneath marker
            glVertex2i(x,y);
            glVertex2i(x,y+getMarkerPosition());
            glVertex2i(x+BREADTH,y+getMarkerPosition());
            glVertex2i(x+BREADTH,y);
        }
        else
        {
            //Area to the right of marker
            glVertex2i(x+getMarkerPosition(),y);
            glVertex2i(x+length(),y);
            glVertex2i(x+length(),y+BREADTH);
            glVertex2i(x+getMarkerPosition(),y+BREADTH);
        }

        if(isFastDecDepressed())
            glColor3f(.5+RGBoffset,.5+RGBoffset,.5+RGBoffset);
        else
            glColor3f(.4+RGBoffset,.4+RGBoffset,.4+RGBoffset);
        if(isVertical())
        {
            //Area above marker
            glVertex2i(x,y+getMarkerPosition());
            glVertex2i(x,y+length());
            glVertex2i(x+BREADTH,y+length());
            glVertex2i(x+BREADTH,y+getMarkerPosition());
        }
        else
        {
            //Area to the left of marker
            glVertex2i(x,y);
            glVertex2i(x+getMarkerPosition(),y);
            glVertex2i(x+getMarkerPosition(),y+BREADTH);
            glVertex2i(x,y+BREADTH);
        }
    glEnd();

    x2=x; y2=y;
    if(!isVertical() && isDecDepressed() || isVertical() && isIncDepressed())
    {
        //Draw the button depressed and shift the arrow one pixel down-right:
        drawButton(x,y,BREADTH, BREADTH, 2, .6,.6,.6,.8,.8,.8,.7,.7,.7);
        x2++;
        y2--;
    }
    else
        //Draw the button embossed.  Highlight the button if the control is selected:
        drawButton(x,y,BREADTH, BREADTH, 2, .9+RGBoffset,.9+RGBoffset,.9+RGBoffset,
            .5+RGBoffset,.5+RGBoffset,.5+RGBoffset,.7+RGBoffset,.7+RGBoffset,.7+RGBoffset);

    glColor3f(0,0,0);
    glBegin(GL_TRIANGLES);
        if(isVertical())
        {
            glVertex2i(x2+6, y2+BREADTH-6);         //Down arrow
            glVertex2i(x2+BREADTH-6, y2+BREADTH-6);
            glVertex2i(x2+BREADTH/2, y2+6);
        }
        else
        {
            glVertex2i(x2+BREADTH-6, y2+6);         //Left arrow
            glVertex2i(x2+BREADTH-6, y2+BREADTH-6);
            glVertex2i(x2+6, y2+BREADTH/2);
        }
    glEnd();

    if(isVertical())
    {
        //Shift up:
        x2=x;
        y2=y+getHeight()-BREADTH;
    }
    else
    {
        //Shift to the right:
        x2=x+getWidth()-BREADTH;
        y2=y;
    }

    if(!isVertical() && isIncDepressed() || isVertical() && isDecDepressed())
    {
        //Draw the button depressed and shift the arrow one pixel down-right:
        drawButton(x2,y2,BREADTH, BREADTH, 2, .6,.6,.6,.8,.8,.8,.7,.7,.7);
        x2++;
        y2--;
    }
    else
        //Draw the button embossed.  Highlight the button if the control is selected:
        drawButton(x2,y2,BREADTH, BREADTH, 2, .9+RGBoffset,.9+RGBoffset,.9+RGBoffset,
            .5+RGBoffset,.5+RGBoffset,.5+RGBoffset,.7+RGBoffset,.7+RGBoffset,.7+RGBoffset);

    glColor3f(0,0,0);
    glBegin(GL_TRIANGLES);
        if(isVertical())
        {
            glVertex2i(x2+6, y2+6);                 //Up arrow
            glVertex2i(x2+BREADTH-6, y2+6);
            glVertex2i(x2+BREADTH/2, y2+BREADTH-6);
        }
        else
        {
            glVertex2i(x2+6, y2+6);                 //Right arrow
            glVertex2i(x2+6, y2+BREADTH-6);
            glVertex2i(x2+BREADTH-6, y2+BREADTH/2);
        }
    glEnd();

    //Draw the marker if visible:
    if(getMarkerLength()>0)
    {
        if(isVertical())
            drawButton(x,y+getMarkerPosition(),BREADTH,
                getMarkerLength(), 2, .9+RGBoffset,.9+RGBoffset,.9+RGBoffset,
                .5+RGBoffset,.5+RGBoffset,.5+RGBoffset,.7+RGBoffset,.7+RGBoffset,.7+RGBoffset);
        else
            drawButton(x+getMarkerPosition(),y,getMarkerLength(),
                BREADTH, 2, .9+RGBoffset,.9+RGBoffset,.9+RGBoffset,.5+RGBoffset,.5+RGBoffset,.5+RGBoffset,
                    .7+RGBoffset,.7+RGBoffset,.7+RGBoffset);
    }
}

/****************************************************************************\
tStdScrollBar::onSetSize

Restricts resizing to one axis.  The width of a vertical scroll bar must equal
BREADTH.  The height of a horizontal scroll bar must equal BREADTH, also.
\****************************************************************************/
void tStdScrollBar::onSetSize(int width, int height)
{
    if(isVertical())
        tScrollBar::onSetSize(BREADTH,height);
    else
        tScrollBar::onSetSize(width,BREADTH);
}



/****************************************************************************\
tStdScrollWindow::tStdScrollWindow

Creates and initializes a standard scroll window.  This class defines MARGIN,
implements the drawBorder method, and automatically creates scroll bars in the
constructor.  The scroll bars will overhang the control's boundaries.  Hence,
if you specify the coordinates (0,0) and the dimensions (100,100), the vertical
scroll bar will span the region [100,120)x[0,100) and the horizontal scroll
bar will span the region [0,100)x[100,120).

Inputs:
    controlX, controlY      Coordinates of lower-left corner
    width, height           Dimensions (including the border)
    canFocus                Can the control receive focus?
    windowX, windowY        Offset of window's contents:
                                As you scroll right, windowX will increase.
                                As you scroll down, windowY will increase.
    windowWidth,            Dimensions of window's contents
        windowHeight
    hasVertScrollBar        Do you wish to attach a vertical scroll bar?
    hasHorScrollBar         Do you wish to attach a horizontal scroll bar?
    fontIndex               The font associated with this control
\****************************************************************************/
tStdScrollWindow::tStdScrollWindow(int controlX, int controlY, int width, int height,
    bool canFocus, int windowX, int windowY, int windowWidth, int windowHeight,
    bool hasVertScrollBar, bool hasHorScrollBar, int _fontIndex) :
    tScrollWindow(controlX,controlY, width,height,canFocus,
    windowX,windowY,windowWidth,windowHeight), fontIndex(_fontIndex)
{
    type=CTRL_STD_SCROLL_WINDOW;
    tWindowScrollBar *s;
    int offset=0;

    //Make sure horizontal and vertical scroll bars don't overlap in the corner:
    if(hasVertScrollBar&&hasHorScrollBar)
        offset=tWindowScrollBar::BREADTH;

    if(hasVertScrollBar)
    {
        //Each click on an up/down arrow will advance the window's text by one row.
        s = mwNew tWindowScrollBar(controlX+getWidth()-tWindowScrollBar::BREADTH-2,
            controlY+2+offset, getHeight()-4-offset,
            /* isVertical = */ true,
            windowY, windowHeight, visWindowHeight(), fontHeight(fontIndex));
        createVertScrollBar(s);

    }
    if(hasHorScrollBar)
    {
        //Each click on a left/right arrow will shift the window's text by the width of an 'A'.
        s = mwNew tWindowScrollBar(controlX+2, controlY+2,
            getWidth()-4-offset, /* isVertical = */ false,
            windowX, windowWidth, visWindowWidth(), charWidth('A', fontIndex));
        createHorScrollBar(s);
    }
}

//This constant defines the thickness of the border around any scroll window.
//You may draw any design or pattern in the border, but it will be the same
//for every descendent.
const int tStdScrollWindow::MARGIN=4;

/****************************************************************************\
tStdScrollWindow::drawBorder

Surrounds the scroll window with a solid white border that is two pixels wide.
The function highlights the border if the control is selected.  Function also
fills the interior with black.
\****************************************************************************/
void tStdScrollWindow::drawBorder()
{
    int x=getControlX(), y=getControlY();

    if(isSelected() || isFocused())
        drawButton(x, y, getWidth(),getHeight(),2,.5,.5,.5,1,1,1,0,0,0);
    else
        drawButton(x, y, getWidth(),getHeight(),2,.4,.4,.4,.9,.9,.9,0,0,0);

/* Old code for drawing solid white border
    if(isSelected() || isFocused())
        glColor3f(1,1,1);
    else
        glColor3f(.8,.8,.8);
    glBegin(GL_QUADS);
        glVertex2i(x,y);
        glVertex2i(x+getWidth(),y);
        glVertex2i(x+getWidth(),y+getHeight());
        glVertex2i(x,y+getHeight());
        glColor3f(0,0,0);
        glVertex2i(x+2,y+2);
        glVertex2i(x+getWidth()-2,y+2);
        glVertex2i(x+getWidth()-2,y+getHeight()-2);
        glVertex2i(x+2,y+getHeight()-2);
    glEnd();
*/

/* Old code for drawing a thick box, one leg at a time:
    glBegin(GL_QUADS);
        glVertex2i(x,y);
        glVertex2i(x+getWidth(),y);
        glVertex2i(x+getWidth(),y+10);
        glVertex2i(x,y+10);
        glVertex2i(x,y+getHeight()-10);
        glVertex2i(x+getWidth(),y+getHeight()-10);
        glVertex2i(x+getWidth(),y+getHeight());
        glVertex2i(x,y+getHeight());
        glVertex2i(x,y+10);
        glVertex2i(x+10,y+10);
        glVertex2i(x+10,y+getHeight()-10);
        glVertex2i(x,y+getHeight()-10);
        glVertex2i(x+getWidth(),y+10);
        glVertex2i(x+getWidth()-10,y+10);
        glVertex2i(x+getWidth()-10,y+getHeight()-10);
        glVertex2i(x+getWidth(),y+getHeight()-10);
    glEnd();
*/
}
/****************************************************************************\
tScrollWindow::visWindowX

Finds the absolute x-coordinate of the left edge of the view.
NOTE: This value does -not- change as the user scrolls the window.

Inputs:
    none
Outputs:
    Returns the control's x-coordinate plus the margin
\****************************************************************************/
int tStdScrollWindow::visWindowX()
{
    return getControlX() + MARGIN;
}

/****************************************************************************\
tScrollWindow::visWindowY

Finds the absolute y-coordinate of the bottom edge of the view.
NOTE: This value does -not- change as the user scrolls the window.

Inputs:
    none
Outputs:
    Returns the control's y-coordinate plus the margin
\****************************************************************************/
int tStdScrollWindow::visWindowY()
{
    int y=getControlY() + MARGIN;
    if(getHorScrollBar()!=NULL)
        y+=tWindowScrollBar::BREADTH;
    return y;
}

/****************************************************************************\
tScrollWindow::visWindowWidth

Finds the width of the view (horizontal space between the left and right margins).

Inputs:
    none
Outputs:
    Returns the control's width minus the left and right margins
\****************************************************************************/
int tStdScrollWindow::visWindowWidth()
{
    int w=getWidth() - MARGIN * 2;
    if(getVertScrollBar()!=NULL)
        w-=tWindowScrollBar::BREADTH;
    return w;
}

/****************************************************************************\
tScrollWindow::visWindowHeight

Finds the height of the view (vertical space between upper and lower margins).

Inputs:
    none
Outputs:
    Returns the control's height minus the upper and lower margins
\****************************************************************************/
int tStdScrollWindow::visWindowHeight()
{
    int h=getHeight() - MARGIN * 2;
    if(getHorScrollBar()!=NULL)
        h-=tWindowScrollBar::BREADTH;
    return h;
}



/****************************************************************************\
tStdTextBox::tStdTextBox

Creates and initializes a new standard text box.  The constructor has the same
inputs at tTextBox.  See tTextBox::tTextBox for more info.
\****************************************************************************/
tStdTextBox::tStdTextBox(int controlX, int controlY, int width, int height,
    const tString& value, int maxLines, bool createVertScrollBar, bool createHorScrollBar,
    int fontIndex, bool wordWrap, bool readOnly) : tTextBox(controlX,controlY,width,height,value,maxLines,
    createVertScrollBar,createHorScrollBar,fontIndex,wordWrap,readOnly)
{
    type=CTRL_STD_TEXT_BOX;
}

/****************************************************************************\
tStdTextBox::drawContents

Draws the text, depicts the current selection, and draws the cursor if
isCursorVisible() returns true.
\****************************************************************************/
void tStdTextBox::drawContents()
{
    int x=visWindowX(), y=visWindowY(), wx, wy, cx, cy;

    //Prevent drawing beyond the window's borders.
    setViewingArea(x,y,visWindowWidth(),visWindowHeight());

    //Initialize the window with black.
    glColor3f(0,0,0);
    glBegin(GL_QUADS);
        glVertex2i(x,y);
        glVertex2i(x+visWindowWidth(),y);
        glVertex2i(x+visWindowWidth(),y+visWindowHeight());
        glVertex2i(x,y+visWindowHeight());
    glEnd();

    //These variables hold the coordinates of the canvas' upper-left corner.
    //As the user scrolls the window, these values will change.
    wx=x-getWindowX();
    wy=y+getWindowY()+visWindowHeight();

    /*      Canvas
     *      ..........................................+.........+.....-wy
     *      .                                         |         |    .
     *      .             Window                   windowY      |    .
     *      .             +----------------------+/\  |   +     |    -controlY
     *      .      +      | +------------------+ | .  +   |     |    -visWindowY
     *      .      |      | |                  | | .      |     |    .
     *      .  visWindow- | |<-VisWindowWidth->| |##    Height  |    .
     *      .    Height   | |                  | |##      |  Window- .
     *      .      |      | |                  | | .      |  Height  .
     *      .      +      | +------------------+ | .      |     |    .
     *      .             +----------------------+\/      +     |    .
     *      .             <...........##.........>              |    .
     *      |<-  WindowX  ->|                                   |    .
     *      .             |<------- Width ------>|              |    .
     *      |<--------------------- WindowWidth ----------------+--->|
     *      |.............|.|...................................+.....
     *      wx            controlX
     *                      visWindowX
     */

    //Draw selection in blue:
    if(isSelection())
    {
        int col1 = getSelStartCol();
        int row1 = getSelStartRow();
        int col2 = getColumn();
        int row2 = getRow();
        int i, x1, x2;

        //Swap col1/row1 with col2/row2 so that the first precedes the second.
        if (row1 > row2 || row1 == row2 && col1 > col2)
        {
            swap(col1, col2);
            swap(row1, row2);
        }

        glColor3f(0,0,1);
        for(i = row1; i <= row2 && i < getLineCount(); i++)
        {
            //Draw the selection one row at a time.  For each row, calculate
            //the left and right edges.
            if(i == row1)
                x1 = getCharOffsets(row1)[col1];
            else
                x1 = 0;
            if(i == row2)
                x2 = getCharOffsets(row2)[col2];
            else
                x2 = getCharOffsets(i)[STRLEN(getText()[i])];
            glBegin(GL_QUADS);
                glVertex2i(wx+x1,wy-i*fontHeight(getFontIndex()));
                glVertex2i(wx+x2,wy-i*fontHeight(getFontIndex()));
                glVertex2i(wx+x2,wy-(i+1)*fontHeight(getFontIndex()));
                glVertex2i(wx+x1,wy-(i+1)*fontHeight(getFontIndex()));
            glEnd();
        }
    }

    //Draw the entire block of text in white via drawText.  drawText will
    //clip the invisible edges.
    glColor3f(1,1,1);
    drawText(getText(),
        /* Top-left corner of text-block */     wx,wy,
        /* Bottom-left corner of viewport */    x,y,
        /* Dimensions of viewport */            visWindowWidth(),visWindowHeight(),
        getFontIndex());

    //Draw the cursor if visible:
    if(isCursorVisible())
    {
        //Convert the cursor's coordinates to x-y-coordinates:
        colRowToXY(getColumn(),getRow(),cx,cy);
        wx+=cx;
        wy-=cy;

        //Draw a vertical white bar two pixels wide whose height matches the
        //height of the tallest character.
        glColor3f(1,1,1);
        glBegin(GL_QUADS);
            glVertex2i(wx,wy);
            glVertex2i(wx+2,wy);
            glVertex2i(wx+2,wy-fontHeight(getFontIndex()));
            glVertex2i(wx,wy-fontHeight(getFontIndex()));
        glEnd();
    }

    //Restore drawing to the entire screen.
    resetViewingArea(x,y,visWindowWidth(),visWindowHeight());
}

/****************************************************************************\
tStdTextBox::getMousePointer

This function replaces the standard pointer when an I-beam pointer when the
user selects the text box.
\****************************************************************************/
bool tStdTextBox::getMousePointer(int x, int y, ePointer& p, int& tag)
{
    //Ancestors have precedence over descendents.
    if(tTextBox::getMousePointer(x,y,p,tag))
        return true;
    //The user cannot edit read-only text boxes.
    if(x>=visWindowX() && y>=visWindowY() && x<visWindowX()+visWindowWidth() &&
        y<visWindowY()+visWindowHeight() && !isReadOnly())
    {
        p=POINTER_I_BEAM;
        tag=fontHeight(getFontIndex());
        return true;
    }
    return false;
}



/****************************************************************************\
tStdListBox::tStdListBox

Creates and initializes a new standard list box.  The constructor has the same
inputs as tListBox.  See tListBox::tListBox for more info.
\****************************************************************************/
tStdListBox::tStdListBox(int controlX, int controlY, int width, int height, bool canFocus,
    const tString& value, int choice, bool createVertScrollBar, bool createHorScrollBar,
    int fontIndex, tEvent _event) : tListBox(controlX, controlY, width, height, canFocus, value,
    choice, createVertScrollBar, createHorScrollBar, fontIndex), event(_event)
{
    type=CTRL_STD_LIST_BOX;
}

/****************************************************************************\
tStdListBox::drawContents

Draws the choices and highlights the current choice.
\****************************************************************************/
void tStdListBox::drawContents()
{
    int x=visWindowX(), y=visWindowY(), wx, wy, cx, cy;
    //Prevent drawing outside the window's borders.
	setViewingArea(x, y, visWindowWidth(), visWindowHeight());

    //Initialize the window's interior with black.
    glColor3f(0,0,0);
    glBegin(GL_QUADS);
        glVertex2i(x,y);
        glVertex2i(x+visWindowWidth(),y);
        glVertex2i(x+visWindowWidth(),y+visWindowHeight());
        glVertex2i(x,y+visWindowHeight());
    glEnd();

    //These variables hold the coordinates of the canvas' upper-left corner.
    //As the user scrolls the window, these values will change.
	wx=x-getWindowX();
    wy=y+getWindowY()+visWindowHeight();

    //If a choice is highlighted, then place a blue bar beneath it.
    if(choice >= 0)
    {
        //These variables hold the coordinates of the bar's lower-left corner.
        cx=wx;
        cy=wy-choice * fontHeight(getFontIndex());

        //The bar spans the width of the canvas.  The height equals that of the
        //font.
        glColor3f(0,0,1);
        glBegin(GL_QUADS);
            glVertex2i(cx,cy);
            glVertex2i(cx+getWindowWidth(),cy);
            glVertex2i(cx+getWindowWidth(),cy-fontHeight(getFontIndex()));
            glVertex2i(cx,cy-fontHeight(getFontIndex()));
        glEnd();
    }

    //Draw the choices in white via drawText.
    glColor3f(1,1,1);
    drawText(choices,
        /* Top-left corner of text-block */     wx,wy,
        /* Bottom-left corner of viewport */    x,y,
        /* Dimensions of viewport */            visWindowWidth(),visWindowHeight(),
    getFontIndex());

    //Restore drawing to the entire screen.
	resetViewingArea(x, y, visWindowWidth(), visWindowHeight());
}



/****************************************************************************\
tStdComboBox::tStdComboBox

Creates and initializes a new standard combo box.  The constructor has the same
inputs as tComboBox.  See tComboBox::tComboBox for more info.
\****************************************************************************/
tStdComboBox::tStdComboBox(int controlX, int controlY, int width, bool canFocus,
    const tString& choices, int choice, int dropDownHeight,
    bool dropDownVertScrollBar, bool dropDownHorScrollBar, int fontIndex, tEvent _event) :
    tComboBox(controlX, controlY, width, canFocus, choices, choice,
    dropDownVertScrollBar, dropDownHorScrollBar, dropDownHeight, fontIndex), event(_event)
{
    type=CTRL_STD_COMBO_BOX;
}

/****************************************************************************\
tStdComboBox::drawContents

Draw the selected choice and display a down arrow on the right to indicate
that it's a combo box.
\****************************************************************************/
void tStdComboBox::drawContents()
{
    int x=visWindowX(), y=visWindowY();

    //Prevent drawing outside the window's borders.  We reserve a square
    //on the right for the down arrow (hence, the '-visWindowHeight()')
    setViewingArea(x,y,visWindowWidth()-visWindowHeight(),visWindowHeight());

    //Fill the window with black.
    glColor3f(0,0,0);
    glBegin(GL_QUADS);
        glVertex2i(x,y);
        glVertex2i(x+visWindowWidth(),y);
        glVertex2i(x+visWindowWidth(),y+visWindowHeight());
        glVertex2i(x,y+visWindowHeight());
    glEnd();

    //Draw the selected choice inside the window.
    glColor3f(1,1,1);
    if(getChoice() >= 0)
        drawString(CHARPTR(getChoices()[getChoice()]), x, y, getFontIndex());

    //Restore drawing to the entire screen.
    resetViewingArea(x,y,visWindowWidth()-visWindowHeight(),visWindowHeight());

    //If the combo box is disabled, draw the down arrow in gray.  Otherwise,
    //draw it in white.
    if(isDisabled())
        glColor3f(.5,.5,.5);
    glBegin(GL_TRIANGLES);
        glVertex2i(x+visWindowWidth()-visWindowHeight()+2,y+visWindowHeight()-2);
        glVertex2i(x+visWindowWidth()-2,y+visWindowHeight()-2);
        glVertex2i(x+visWindowWidth()-visWindowHeight()/2,y+2);
    glEnd();
}



/****************************************************************************\
tStdDialogBox::tStdDialogBox

Creates and initializes a new standard dialog box.  The class implements
drawWindow, introduces a caption, and loads a resize pointer in response
to an onResizeActivated event.  If you do not specify controlX and controlY,
the window will automatically center itself on the screen.
See tDialogBox::tDialogBox for more info.

Inputs:
    caption                 Title that will appear at the top
    fontIndex               Font with which to draw title
    controlX, controlY      Coordinates of dialog box's lower-left corner [OPTIONAL]
    width, height           Dimensions of dialog box
    state                   WINDOW_NORMAL, WINDOW_MAXIMIZED, or WINDOW_MINIMIZED
    modal                   Is this window modal?  A modal window stays on
                            top of other window and retains focus.
    destroyOnClose          Will the window destroy itself when closed?
                                If false, the window will hide itself.
    moveable                Can the user reposition the window?
    resizeable              Can the user resize the window?
    maximizeable            Is a maximize button visible?
    minimizeable            Is a minimize button visible?
    closeable               Is a close button visible?
    minWidth, maxWidth,     Resize boundaries.  Minimum value for minWidth
        minHeight, maxHeight    and minHeight is 50.  Maximum value for
                                maxWidth is screenWidth.  Maximum value for
                                maxHeight is screenHeight.  The maximum
                                width/height must equal or exceed the minimum
                                width/height.
\****************************************************************************/
tStdDialogBox::tStdDialogBox(const tString& _caption, int _fontIndex, int controlX,
    int controlY, int width, int height, eWindowState state, bool modal,
    bool destroyOnClose, bool moveable, bool resizeable, bool maximizeable,
    bool minimizeable, bool closeable, int minWidth, int maxWidth, int minHeight,
    int maxHeight) : tDialogBox(controlX,controlY,width,height,
    /* titleBarHeight = */ fontHeight(_fontIndex)+4, /* margin = */ 2,
    state,modal,destroyOnClose,moveable,resizeable,maximizeable,minimizeable,
    closeable,minWidth,maxWidth, minHeight,maxHeight), caption(_caption),
    fontIndex(_fontIndex)
{
    type=CTRL_STD_DIALOG_BOX;
    adjustCaption();
}
tStdDialogBox::tStdDialogBox(const tString& _caption, int _fontIndex, int width,
    int height, eWindowState state, bool modal, bool destroyOnClose, bool moveable,
    bool resizeable, bool maximizeable, bool minimizeable, bool closeable,
    int minWidth, int maxWidth, int minHeight, int maxHeight) : tDialogBox(0,0,
    width,height,fontHeight(_fontIndex)+4,2,state,modal,destroyOnClose,moveable,
    resizeable,maximizeable,minimizeable,closeable,minWidth,maxWidth,minHeight,
    maxHeight),caption(_caption),fontIndex(_fontIndex)
{
    type=CTRL_STD_DIALOG_BOX;
    adjustCaption();
    //Window will automatically center itself on the screen.
    setAnchors(/* horizontal anchor = */ ANCHOR_CENTER, /* vertical anchor = */ ANCHOR_CENTER);
}

/****************************************************************************\
tStdDialogBox::drawWindow

Draws the dialog box's background, title bar, and caption.
\****************************************************************************/
void tStdDialogBox::drawWindow()
{
    /*  Old code for modern dialog box:
    int x=getControlX(), y=getControlY();
    int w=getWidth(), h=getHeight();
    glBegin(GL_QUADS);
        //Fill the entire window with a white background.
        glColor3f(1,1,1);
        glVertex2i(x,y);
        glVertex2i(x+w,y);
        glVertex2i(x+w,y+h);
        glVertex2i(x,y+h);

        //Fill the window's interior with light gray, leaving a two-pixel thick border.
        x+=getMargin();
        y+=getMargin();
        w-=getMargin()*2;
        h-=getMargin()*2;
        glColor3f(.65,.65,.65);
        glVertex2i(x,y);
        glVertex2i(x+w,y);
        glVertex2i(x+w,y+h);
        glVertex2i(x,y+h);

        //Load the coordinates and dimensions of the title bar.
        y+=h-getTitleBarHeight()-getMargin();
        h=getTitleBarHeight()+getMargin();

        //Draw the title bar in blue if active or light gray if inactive.
        if(isFocused())
        {
            if(isSelected())
                glColor3f(0,0,1);
            else
                glColor3f(0,0,.8);
        }
        else
        {
            if(isSelected())
                glColor3f(.7,.7,.7);
            else
                glColor3f(.5,.5,.5);
        }
        glVertex2i(x,y);
        glVertex2i(x+w,y);
        glVertex2i(x+w,y+h);
        glVertex2i(x,y+h);

        //Draw a white strip beneath the title bar.
        h=getMargin();
        glColor3f(1,1,1);
        glVertex2i(x,y);
        glVertex2i(x+w,y);
        glVertex2i(x+w,y+h);
        glVertex2i(x,y+h);
    glEnd();

    //Always draw the window's caption in white.
    glColor3f(1,1,1);
    drawString(CHARPTR(visibleCaption),getControlX()+cx,getControlY()+cy,fontIndex);

    //This is a trick I learned for achieving boldface letters.  You can make a
    //string appear bold by shifting it to the left by one pixel.
    if(isFocused())
        drawString(CHARPTR(visibleCaption),getControlX()+cx+1,getControlY()+cy,fontIndex);
    */

    int x=getControlX(), y=getControlY();
    drawButton(x, y, getWidth(),getHeight(),2,.9,.9,.9,.5,.5,.5,.65,.65,.65);
    x+=getMargin();
    y+=getHeight()-getMargin()-getTitleBarHeight();
    if(isFocused())
    {
        if(isSelected())
            glColor3f(0,0,1);
        else
            glColor3f(0,0,.9);
    }
    else
    {
        if(isSelected())
            glColor3f(.7,.7,.7);
        else
            glColor3f(.6,.6,.6);
    }
    glBegin(GL_QUADS);
        glVertex2i(x,y+getTitleBarHeight());
        glVertex2i(x,y);
        glColor3f(.65,.65,.65);
        glVertex2i(x+getWidth()-getMargin()*2,y);
        glVertex2i(x+getWidth()-getMargin()*2,y+getTitleBarHeight());
    glEnd();
    glColor3f(1,1,1);
    drawString(CHARPTR(visibleCaption),x+2,y+2,fontIndex);
}

/****************************************************************************\
tStdDialogBox::adjustCaption

Trims the caption so that it will fit within the title bar without interfering
with the window buttons.  trimString will truncate the string (if necessary)
and append an ellipses.  Call this function each time the window's size changes.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tStdDialogBox::adjustCaption()
{
    int minX=INT_MAX;
    for(int i=0;i<3;i++)
        if(buttons[i]&&buttons[i]->isVisible()&&buttons[i]->getControlX()<minX)
            minX=buttons[i]->getControlX();
    visibleCaption = trimString(caption,minX-getMargin()-4, fontIndex);
    //Position caption near left edge and center it vertically.
    cx=4;
    cy=getHeight()-(getTitleBarHeight()+fontHeight(fontIndex)+4)/2;
}

/****************************************************************************\
tStdDialogBox::onSetSize

Appends adjustCaption to ancestor's implementation.
\****************************************************************************/
void tStdDialogBox::onSetSize(int w, int h)
{
    tDialogBox::onSetSize(w,h);
    adjustCaption();
}

/****************************************************************************\
tStdDialogBox::setCaption

Changes the caption from outside the class.  The function replaces the
caption and truncates it to fit.

Inputs:
    caption     New caption
Outputs
    none
\****************************************************************************/
void tStdDialogBox::setCaption(const tString& _caption)
{
    caption = _caption;
    adjustCaption();
}



/****************************************************************************\
tStdPointer::tStdPointer

Creates and initializes a standard pointer.  The constructor sets up an
animation object that references the "StdPointer" animation.

Inputs:
    none
\****************************************************************************/
tStdPointer::tStdPointer(int tag) : tMousePointer(POINTER_STD, tag),
    anim(animTypeLookup("StdPointer",/*exact=*/true))
{
}

/****************************************************************************\
tStdPointer::draw

Loads anim with the current frame and draws it at (x,y).  You will usually
pass the mouse coordinates to the function.

Inputs:
    x, y        Absolute mouse coordinates
Outputs:
    none
\****************************************************************************/
void tStdPointer::draw(int x, int y)
{
    anim.tick(currentTime);
    glColor3f(1,1,1);
    anim.draw(x,y);
}



/****************************************************************************\
tIBeamPointer::tIBeamPointer

Creates and initializes an I-Beam pointer.

Inputs:
    height      Pointer's height
\****************************************************************************/
tIBeamPointer::tIBeamPointer(int _height) : tMousePointer(POINTER_I_BEAM, height), height(_height)
{
}

/****************************************************************************\
tIBeamPointer::draw

Draws a vertical line centered at (x,y) with four serifs.  The pointer is
five pixels wide.  The height depends on the value passed to the constructor.
You will usually pass the mouse coordinates to the function.

Inputs:
    x, y        Absolute mouse coordinates
Outputs:
    none
\****************************************************************************/
void tIBeamPointer::draw(int x, int y)
{
    glColor3f(1,1,1);
    glBegin(GL_LINES);
        //Left arm
        glVertex2i(x-2,y-height/2);
        glVertex2i(x,y-height/2);

        //Right arm
        glVertex2i(x+1,y-height/2);
        glVertex2i(x+3,y-height/2);

        //Left leg
        glVertex2i(x-2,y+height/2);
        glVertex2i(x,y+height/2);

        //Right leg
        glVertex2i(x+1,y+height/2);
        glVertex2i(x+3,y+height/2);

        //Body
        glVertex2i(x+1,y-height/2+1);
        glVertex2i(x+1,y+height/2);
    glEnd();
}



/****************************************************************************\
tResizePointer::tResizePointer

Creates and initializes a resize pointer.  The constructor loads anim with
either "ResizePointerA" or "ResizePointerB" and initializes matrix so
that pointer will appear in correct orientation.

Inputs:
    r       Determines which edge/corner the mouse is over (e.g. DIR_UP, DIR_LEFT)
\****************************************************************************/
tResizePointer::tResizePointer(int _dir) : tMousePointer(POINTER_RESIZE, _dir), dir((eDir) _dir)
{
    //Initialize matrix with the identity matrix.
    float _matrix[16] = {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1};
    for(int i=0;i<16;i++)
        matrix[i]=_matrix[i];

    switch(dir)
    {
        case DIR_RIGHT:
            anim.load(animTypeLookup("ResizePointerA",/*exact=*/true));
            matrix[0]=0;    matrix[4]=1;
            matrix[1]=1;    matrix[5]=0;
            break;
        case DIR_UP_RIGHT:
            anim.load(animTypeLookup("ResizePointerB",/*exact=*/true));
            break;
        case DIR_UP:
            anim.load(animTypeLookup("ResizePointerA",/*exact=*/true));
            break;
        case DIR_UP_LEFT:
            anim.load(animTypeLookup("ResizePointerB",/*exact=*/true));
            matrix[0]=-1;
            break;
        case DIR_LEFT:
            anim.load(animTypeLookup("ResizePointerA",/*exact=*/true));
            matrix[0]=0;    matrix[4]=-1;
            matrix[1]=1;    matrix[5]=0;
            break;
        case DIR_DOWN_LEFT:
            anim.load(animTypeLookup("ResizePointerB",/*exact=*/true));
            matrix[0]=-1;
                            matrix[5]=-1;
            break;
        case DIR_DOWN:
            anim.load(animTypeLookup("ResizePointerA",/*exact=*/true));
                            matrix[5]=-1;
            break;
        case DIR_DOWN_RIGHT:
            anim.load(animTypeLookup("ResizePointerB",/*exact=*/true));
                            matrix[5]=-1;
            break;
    }
}

/****************************************************************************\
tResizePointer::draw

Loads anim with the current frame, translates to (x,y), applies the matrix
transformation, and draws the animation at the new origin.  You will usually
pass the mouse coordinates to the function.

Inputs:
    x, y        Absolute mouse coordinates
Outputs:
    none
\****************************************************************************/
void tResizePointer::draw(int x, int y)
{
    anim.tick(currentTime);
    glPushMatrix();
    glTranslatef(x,y,0);
    glMultMatrixf(matrix);
    glColor3f(1,1,1);
    anim.draw(0,0);
    glPopMatrix();
}



/****************************************************************************\
tScrollPointer::tScrollPointer

Creates and initializes a new scroll pointer.  The scroll pointer loads anim
with one form of "ScrollPointer", initializes a transformation matrix, and
loads offset with the direction of motion.  These variables ensure that the
pointer will appear at the correct position.

Inputs:
    d       Determines which edge/corner of screen the mouse is near
\****************************************************************************/
tScrollPointer::tScrollPointer(int _dir) : tMousePointer(POINTER_SCROLL, _dir), dir((eDir) _dir),
    start(currentTime)
{
    //Initialize matrix with the identity matrix.
    float _matrix[16] = {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1};
    for(int i=0;i<16;i++)
        matrix[i]=_matrix[i];

    switch(dir)
    {
        case DIR_RIGHT:
            offsetX=16; offsetY=0;
            anim.load(animTypeLookup("ScrollPointerA",/*exact=*/true));
            matrix[0]=0;    matrix[4]=1;
            matrix[1]=1;    matrix[5]=0;
            break;
        case DIR_UP_RIGHT:
            offsetX=10; offsetY=10;
            anim.load(animTypeLookup("ScrollPointerC",/*exact=*/true));
            break;
        case DIR_UP:
            offsetX=0; offsetY=16;
            anim.load(animTypeLookup("ScrollPointerA",/*exact=*/true));
            break;
        case DIR_UP_LEFT:
            offsetX=-10; offsetY=10;
            anim.load(animTypeLookup("ScrollPointerD",/*exact=*/true));
            break;
        case DIR_LEFT:
            offsetX=-16; offsetY=0;
            anim.load(animTypeLookup("ScrollPointerB",/*exact=*/true));
            matrix[0]=0;    matrix[4]=1;
            matrix[1]=1;    matrix[5]=0;
            break;
        case DIR_DOWN_LEFT:
            offsetX=-10; offsetY=-10;
            anim.load(animTypeLookup("ScrollPointerD",/*exact=*/true));
                matrix[5]=-1;
            break;
        case DIR_DOWN:
            offsetX=0; offsetY=-16;
            anim.load(animTypeLookup("ScrollPointerB",/*exact=*/true));
            break;
        case DIR_DOWN_RIGHT:
            offsetX=10; offsetY=-10;
            anim.load(animTypeLookup("ScrollPointerD",/*exact=*/true));
            matrix[0]=0;    matrix[4]=1;
            matrix[1]=1;    matrix[5]=0;
            break;
    }
}

/****************************************************************************\
tScrollPointer::draw

Loads anim with the current frame, translates to (x,y), applies the matrix
transformation, and draws the animation at the new origin.  You will usually
pass the mouse coordinates to the function.

Inputs:
    x, y        Absolute mouse coordinates
Outputs:
    none
\****************************************************************************/
void tScrollPointer::draw(int x, int y)
{
	float i = currentTime-start;
    if(i>.5)
        start+=.5;
    anim.tick(currentTime);
    glPushMatrix();
    //We gradually slide the pointer in the direction of motion.  The time
    //delay is sychronized with the animation, which depicts the cones from
    //different angles.
    glTranslatef(x+2*i*offsetX,y+2*i*offsetY,0);
    glMultMatrixf(matrix);
    glColor3f(1,1,1);
    anim.draw(0,0);
    glPopMatrix();
}



/****************************************************************************\
tTargetPointer::tTargetPointer

Creates and initializes a standard pointer.  The constructor sets up an
animation object that references the "StdPointer" animation.

Inputs:
    none
\****************************************************************************/
tTargetPointer::tTargetPointer(int tag) : tMousePointer(POINTER_TARGET, tag),
    anim(animTypeLookup("TargetPointer",/*exact=*/true))
{
}

/****************************************************************************\
tTargetPointer::draw

Loads anim with the current frame and draws it at (x,y).  You will usually
pass the mouse coordinates to the function.

Inputs:
    x, y        Absolute mouse coordinates
Outputs:
    none
\****************************************************************************/
void tTargetPointer::draw(int x, int y)
{
    anim.tick(currentTime);
    glColor3f(1,1,1);
    anim.draw(x,y);
}



/************
 * OLD CODE *
 ************/
//Standard pointer
/*
    float size =.5;
    float v[][3] = {{0,0,0}, {0,size,1}, {-.866*size, -.5*size, 1}, {.866*size, -.5*size, 1}};
    float normal[3];
    pointerlist = glGenLists(1);
    glNewList(pointerlist, GL_COMPILE);
        glRotatef(90, 0, 1, 0);
        glRotatef(45, 1, 0, 0);
        glScalef(3,3,3);
        glColor3f(1,1,0);
        glBegin(GL_TRIANGLES);
            calcNormal(v[0], v[2], v[1], normal);
            glNormal3fv(normal);
            glVertex3fv(v[0]);
            glVertex3fv(v[2]);
            glVertex3fv(v[1]);

            calcNormal(v[0], v[1], v[3], normal);
            glNormal3fv(normal);
            glVertex3fv(v[0]);
            glVertex3fv(v[1]);
            glVertex3fv(v[3]);

            calcNormal(v[0], v[3], v[2], normal);
            glNormal3fv(normal);
            glVertex3fv(v[0]);
            glVertex3fv(v[3]);
            glVertex3fv(v[2]);
        glEnd();
    glEndList();

  	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glClear(GL_DEPTH_BUFFER_BIT);
	glViewport(x - 30, y - 30, 60, 60);
	glMatrixMode(GL_PROJECTION);
    glPushMatrix();
	glLoadIdentity();
    gluPerspective(45,1,1,10);
	glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
	glLoadIdentity();
	glTranslatef(0,0,-8);
    glRotatef(rotation, -1, 1, 0);
    glCallList(pointerlist);

	glViewport(0,0,screenWidth,screenHeight);           // Reset The Current Viewport
    glPopMatrix();
	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
    glPopMatrix();
	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
    glDisable(GL_COLOR_MATERIAL);
  	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
*/
//Resize pointer
/*
    GLUquadricObj *quad=gluNewQuadric();
    pointerlist=glGenLists(1);
    glNewList(pointerlist, GL_COMPILE);
        glColor3f(1,1,0);
        glPushMatrix();
        glRotatef(-90,0,1,0);           //rotate 90 degrees around y-axis
        glTranslatef(0,0,-1);           //translate to 1 unit behind origin
        gluDisk(quad,0,.5,10,1);        //draw a disk of radius .5 facing origin
        gluCylinder(quad,.2,.2,2,10,1); //draw a cylinder of radius .2 and height 2 which is centered around origin
        glTranslatef(0,0,2);            //translate to 1 unit in front of origin
        gluCylinder(quad,.5,0,1,10,1);  //draw a cone with a radius of .5 and a height of 1
        glPopMatrix();
        glRotatef(90,0,1,0);            //rotate 90 degrees the other way around y-axis
        glTranslatef(0,0,-1);           //translate 1 unit behind origin
        gluDisk(quad,0,.5,10,1);        //draw the other disk of radius .5 facing origin
        glTranslatef(0,0,2);            //translate to 1 unit in front of origin
        gluCylinder(quad,.5,0,1,10,1);  //draw a cone with a radius of .5 and a height of 1
    glEndList();
    gluDeleteQuadric(quad);

   	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glClear(GL_DEPTH_BUFFER_BIT);
	glViewport(x - 30, y - 30, 60, 60);
	glMatrixMode(GL_PROJECTION);
    glPushMatrix();
	glLoadIdentity();
    gluPerspective(45,1,1,10);
	glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
	glLoadIdentity();
	glTranslatef(0,0,-8);
    glRotatef(angle, 0,0,1);
    glRotatef(rotation, 0, 1, 0);

    glCallList(pointerlist);

	glViewport(0,0,screenWidth,screenHeight);           // Reset The Current Viewport
    glPopMatrix();
	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
    glPopMatrix();
	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
    glDisable(GL_COLOR_MATERIAL);
  	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
*/
//Scroll pointer
/*
    GLUquadricObj *quad1=gluNewQuadric();
    GLUquadricObj *quad2=gluNewQuadric();
    gluQuadricOrientation(quad2, GLU_INSIDE);

    pointerlist=glGenLists(1);
    glNewList(pointerlist, GL_COMPILE);
        glColor3f(1,1,0);
        glRotatef(90,0,1,0);            //rotate 90 degrees around y-axis
        gluCylinder(quad1,1,0,1,20,1);  //draw a cone with a radius of 1 and a height of 1
        gluDisk(quad2,0,1,20,1);        //draw a disk of radius 1 facing -z direction
        glTranslatef(0,0,1);            //translate to 1 unit in front of origin
        gluCylinder(quad1,1,0,1,20,1);  //draw a cone with a radius of 1 and a height of 1
        gluDisk(quad2,0,1,20,1);        //draw a disk of radius 1 facing -z direction
        glTranslatef(0,0,1);            //translate to 2 units in front of origin
        gluCylinder(quad1,1,0,1,20,1);  //draw a cone with a radius of 1 and a height of 1
        gluDisk(quad2,0,1,20,1);        //draw a disk of radius 1 facing -z direction
    glEndList();
    gluDeleteQuadric(quad1);
    gluDeleteQuadric(quad2);

  	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glClear(GL_DEPTH_BUFFER_BIT);
	glViewport(x+offsetX, y+offsetY, 100, 100);
	glMatrixMode(GL_PROJECTION);
    glPushMatrix();
	glLoadIdentity();
    gluPerspective(45,1,1,10);
	glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
	glLoadIdentity();
	glTranslatef(0,0,-8.5);
    glRotatef(angle, 0,0,1);
    glTranslatef(-1.5+index, 0, 0);

    glCallList(pointerlist);

	glViewport(0,0,screenWidth,screenHeight);           // Reset The Current Viewport
    glPopMatrix();
	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
    glPopMatrix();
	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
    glDisable(GL_COLOR_MATERIAL);
  	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
*/
//Cross hairs
/*
    for(int i=0; i<8; i++)
    {
        glColor3f(1,1,0);
        glEnable(GL_LIGHTING);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_COLOR_MATERIAL);
        glClear(GL_DEPTH_BUFFER_BIT);
        glViewport(i*60, 240-50, 100, 100);

        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();
        gluPerspective(45,1,1,10);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();
        glTranslatef(0,0,-10);

        mglTorus(.7+i*.05, .1, 30, 30);

        glPushMatrix();
        glRotatef(90,1,0,0);
        glTranslatef(0,0,-1.25);
        gluCylinder(quad, .1, .1, 2.5, 30, 1);
        glPopMatrix();
        glPushMatrix();
        glRotatef(90,0,1,0);
        glTranslatef(0,0,-1.25);
        gluCylinder(quad, .1, .1, 2.5, 30, 1);
        glPopMatrix();

        glViewport(0,0,screenWidth,screenHeight);           // Reset The Current Viewport
        glPopMatrix();
        glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
        glPopMatrix();
        glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
        glDisable(GL_COLOR_MATERIAL);
        glDisable(GL_LIGHTING);
        glDisable(GL_DEPTH_TEST);
    }
*/
//Bulls eye
/*
    int i;
    for(i=0;i<16;i++)
    {
        float r=4.65+i*4.65,a;

        glPushMatrix();
        glTranslatef(75+(i%4)*150,75+(i/4)*150,0);

        if(r>0)
        {
            a=1-MAX(0,r-50)/32.0;
            glColor3f(0,a,0);
            gluDisk(quad, MAX(r-10,0), r, 100, 1);
        }
        if(r>20)
        {
            a=1-MAX(0,r-55)/29.0;
            glColor3f(0,a,0);
            gluDisk(quad, MAX(r-30,0), r-20, 100, 1);
        }
        if(r>40)
        {
            a=1-MAX(0,r-60)/25.0;
            glColor3f(0,a,0);
            gluDisk(quad, MAX(r-50,0), r-40, 100, 1);
        }
        glPopMatrix();
    }
*/
