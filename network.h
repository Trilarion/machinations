/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef networkH
#define networkH

/**************************************************************************
                                  Headers
 **************************************************************************/
#include "types.h"
#include "sockwrap.h"

/**************************************************************************
                                  Defines
 **************************************************************************/
#define BUFFER_SIZE         100000  //Size of descriptor's in/out buffers
#define MAX_BLOCK_SIZE      1024    //Maximum packet size (if packet exceeds this value, it is sent
                                    //in pieces)
#define MAX_DESCRIPTORS     20      //Maximum number of descriptors.  There may be more descriptors
                                    //than users if some descriptors have not established a connection yet.
                                    //If additional descriptors try to connect, they will be refused.
#define CONNECTION_TIMEOUT  10      //Seconds to wait for a connection to be established
                                    //(either receiving a packet with UDP or socket writability with TCP/IP)
#define CONNECTION_DELAY    2       //Interval (in seconds) between successive reconnect attempts
                                    //(If a connection is lost with TCP/IP, it will automatically try to reconnect)
#define LAG_INTERVAL        10      //Interval (in seconds) between ping messages, which are used to calculate lag
#define OVERFLOW_TIMEOUT    1       //Seconds to wait if the winsock out buffer overflows
#define RESEND_INTERVAL     .1      //Seconds between each resent message (prevents flooding)
#define FILE_PACKET_SIZE    400     //Maximum file packet size in bytes

//For UDP only
#define REDUNDANT_PACKETS    1      //Store the last 1 packet sent in each outgoing packet
#define ACK_INTERVAL         10     //Interval at which network acknowledges it has received a series of packets
#define MAX_BACKLOG          10     //Maximum number of packets that can be sent without the recipient's acknowledgement
#define DOWNLOAD_PATH        "download/"   //Directory where downloaded files are stored

/**************************************************************************
                               Enumerations
 **************************************************************************/
//Connection refused codes:
enum eRefuse {REF_HOST_FULL, REF_GAME_STARTED, REF_NAME_USED, REF_DROPPED, REF_OLD_VERSION};

//eTextMessageType enumerates different types of text messages.  The user-interface
//may display the message differently based on its type.
enum eTextMessage { TEXT_MESSAGE_ECHO, TEXT_MESSAGE_CHAT, TEXT_MESSAGE_SHELL,
    TEXT_MESSAGE_STATUS };

/**************************************************************************
                                Structures
 **************************************************************************/
struct tDescriptor;
class tMessage;

//The user structure represents a user who has successfully connected.
//It essentially stores the user's name.
//Later, it may store user info also (such as gender and ranking).
struct tUser
{
    int         ID;                 //Unique value - matches user's descriptor's ID
    tString     name;               //User's name
    tDescriptor* descriptor;        //Pointer to corresponding descriptor
    bool        haveMap;            //Does this user have the map which the host selected?
    bool        haveWorld;          //Does this user have the world file which the host selected?
    bool        ackReceived;        //Has the user acknowledged the map and world files?
    tUser*      next;               //Next user in linked list
};

//A packet represents a single packet of data which may contain one or more messages.
//It is used to store outgoing messages, in case they are lost, and incoming messages,
//in case they are received out of order.
struct tPacket
{
    long            ID;         //unique ID (used to ensure that no packets are lost)
    int             length;     //Length of data
    unsigned char   checkSum;   //(Sum of all data bytes) & 0xff
    char            immediate;  //Should the recipient execute the message immediately? (0=false; 1=true)
    char*           data;       //Pointer to a dynamically allocated character array
    bool            resend;     //Does the packet need to be resent? (outgoing packets only)
    tPacket*        next;       //Next packet in linked list
};

//A descriptor represents a network connection.  It interfaces between winsock and the user structure.
//NOTE:  Not all users have descriptors (i.e. the game host doesn't have a descriptor).
//Likewise, not all descriptors have users (i.e. a new connection has just been detected, and the
//descriptor has not yet introduced itself as a user.
struct tDescriptor
{
    int         ID;                         //Unique ID, matches user ID

    /*TCPIP only*/
    tSocket     socket;                     //Handle to a unique windows socket (TCP/IP only)

    /*UDP only*/
    int         nextPacketOut;              //ID of the next outgoing packet
    int         nextPacketIn;               //ID of the next incoming packet
    long        packetAcks[ACK_INTERVAL];   //Series of packets which have just been received
    int         packetAckCount;             //Number of unacknowledged received packets
                                            //When packetAckCount == ACK_INTERVAL, an acknowledgement is sent notifying
                                            //the remote computer that a series of packets has been received.
    tPacket*    inPackets;                  //Linked list of incoming packets (numerical order)
    tPacket*    outPackets;                 //Linked list of unacknowledged outgoing packets (reverse numerical order)
    int         outPacketCount;             //Number of packets in outPackets
    tTime       resendTimer;                //Used for timing resends (see RESEND_INTERVAL)

    char        inBuffer[BUFFER_SIZE];      //String of incoming data
                                            //(in UDP, data is extracted and assembled here from packets)
    int         inBufferTop;                //Bytes waiting in inBuffer
    char        outBuffer[BUFFER_SIZE];     //String of outgoing data
                                            //(mutliple messages can be assembled back to back in this buffer)
    int         outBufferTop;               //Bytes stored in outBuffer
    bool        connected;                  //Is the descriptor connected?
    tString     peerName;                   //Proper name of peer
    long        IPAddress;                  //Peer's IP address
    int         port;                       //Peer's port
    tTime       connectTimer;               //Used initially for connection time out
    tUser*      user;                       //Pointer to corresponding user structure
    tDescriptor* next;                      //Next descriptor in linked list
    tTime       pingTime;                   //Used for timing pings and pongs to calculate lag
    float       lag;                        //In seconds (lag = (<time pong received> - <time ping sent>) / 2)
    bool        reconnect;                  //Is the descriptor trying to reconnect?
    long        lastPingID;                 //Last ping sent (pings and pongs must have ids in case lag > LAG_INTERVAL)
    bool        bufferFull;                 //Has the winsock out buffer overflowed?
    tTime       bufferFullTimer;            //See OVERFLOWTIMER

    //The following variables are used for file transfer only.
    //NOTE: It's possible (though not advisable) to upload and download a file at once
    FILE*       fOut;                       //Outgoing file handle
    FILE*       fIn;                        //Incoming file handle
    tFile       fileIn;                     //Structure containing information about incoming file
    tFile       fileOut;                    //Structure containing information about outgoing file

    long        bytesReceived;              //Bytes received of incoming file
    long        bytesSent;                  //Bytes sent of outgoing file

    bool        destroyed;                  //Has the descriptor been deleted?
                                            //Lazy delete; descriptor isn't deleted until next pass
};

/**************************************************************************
                            Global Variables
 **************************************************************************/
//Declarations for global variables: (see descriptions in network.cpp)
extern bool     useTCPIP;
extern tUser*   firstUser;
extern tUser*   localUser;
extern long     networkIP;
extern tString  peerName;
extern int      networkPort;
extern bool     networkGame;

extern bool     isGameHost;
extern bool     isGameClient;

/**************************************************************************
                             Global Functions
 **************************************************************************/
//Declarations for global functions: (see descriptions in network.cpp)
//Functions below return true if successful, false if failed.
extern bool singlePlayer(const char *username);
extern bool hostGame(long IP, int port, const char *username, bool TCPIP);
extern bool connectToHost(long IP, int port, const char *username, bool TCPIP,
                long localIP, int localPort);
extern bool reconnect(long IP, int port);
extern bool networkTick();
extern bool disconnect();
extern void writeToBuffer(tDescriptor *desc, tMessage *message);
extern void broadcast(tMessage *message);
extern bool dropUser(tUser *user);
extern void sendText(const char *text, eTextMessage type);
extern void sendTextToUser(tUser *user, const char *text, eTextMessage type);
extern void sendToHost(tMessage *message);

extern bool sendFile(tDescriptor *desc, tFile& f);
extern bool abortDownload(tDescriptor *desc);
extern bool abortUpload(tDescriptor *desc);
extern void abortAllUploads();
extern void simulateError(tDescriptor *desc);

extern const char** getLocalIPs();
extern const char*  getLocalName();
extern bool         isUsernameValid(const char *username);
extern tUser*       userLookup(int ID);
extern tUser*       userLookup(const tString& name, bool exact);

extern bool         networkStartup();
extern void         networkShutdown();
extern void         sessionShutdown();

//Statistics:
extern long getPacketsSent();
extern long getPacketsRecv();
extern long getPacketsResent();
extern long getPacketsRerecv();

/**************************************************************************
                                   Events
 **************************************************************************/
//These functions are called by the network in response to certain events:

//User-interface events:
extern void onAddUser(tUser *user, bool initial);   //User connects
    //If initial = true, user was connected before you connected.
    //If initial = false, user connected after you.
extern void onConnect(tDescriptor *desc);           //You successfully establish a connection
extern void onReconnect();                          //You successfully re-established a connection
extern void onConnectionRefused(eRefuse code);      //Your connection is refused (reason stored in code)
extern void onDisconnect();                         //Your session ended
extern void onDropUser(tUser *user);                //User disconnects
extern void onHostLeft();                           //The host leaves
extern void onHostGame();                           //You successfully host a game
extern void onNetworkError();                       //A network error occurs
extern void onReceiveText(const char *text, eTextMessage type);
                                                    //You receive a text message
extern void onUpdateLag(tUser *user);               //User's lag has just been updated
extern void onDownloadFile(tDescriptor *desc);      //You begin downloading a file
extern void onDownloadComplete(tDescriptor *desc);  //The download is complete
extern void onUploadFile(tDescriptor *desc);        //You begin uploading a file
extern void onUploadComplete(tDescriptor *desc);    //The upload is complete
extern void onAbortDownload(tDescriptor *desc);     //The download was aborted
extern void onAbortUpload(tDescriptor *desc);       //The upload was aborted
//I used these four events to monitor packets, users, and descriptors.
/*
extern void onPacketSent();                         //A packet is sent (TCP/IP or UDP)
extern void onPacketLost();                         //A packet is lost (UDP only)
extern void onCountUsers(int count);                //The number of users changes
extern void onCountDescriptors(int count);          //The number of descriptors changes
*/
extern void onSinglePlayer();                       //You started a single-player session

//Engine events:
extern void onFileSent(tDescriptor *desc);          //A file just finished uploading
extern void onFileReceived(tDescriptor *desc);      //A file just finished downloading
extern void onDeleteUser(tUser *user);              //A user is about to be deleted
extern void onClientConnect(tUser *user);           //Client connects to host and negotiates connection
extern void onClientDisconnect(tUser *user);        //Client disconnects from host
extern void onSessionStartup();                     //Occurs immediately after onHost, onConnect, or onSinglePlayer and
                                                        //the subsequent onAddUser event
extern void onSessionShutdown();                    //Occurs immediately after onDisconnect

//Declare these structures here since the events below reference them:
class tSetTimingMsg;
class tChooseFilesMsg;
class tAckFilesMsg;
class tSetPlayerMsg;
class tDataMsg;
typedef tDataMsg tSyncMsg;

extern void onSetTimingMsg   (tDescriptor *desc, tSetTimingMsg*  message) throw(int);
extern void onStartGameMsg   (tDescriptor *desc, tMessage*       message) throw(int);
extern void onEndGameMsg     (tDescriptor *desc, tMessage*       message) throw(int);
extern void onPauseGameMsg   (tDescriptor *desc, tMessage*       message) throw(int);
extern void onResumeGameMsg  (tDescriptor *desc, tMessage*       message) throw(int);
extern void onResetPlayersMsg(tDescriptor *desc, tMessage*       message) throw(int);
extern void onChooseFilesMsg (tDescriptor *desc, tChooseFilesMsg*message) throw(int);
extern void onAckFilesMsg    (tDescriptor *desc, tAckFilesMsg*   message) throw(int);
extern void onSetPlayerMsg   (tDescriptor *desc, tSetPlayerMsg*  message) throw(int);
extern void onSyncMsg        (tDescriptor *desc, tSyncMsg*       message) throw(int);
/****************************************************************************/
#endif
