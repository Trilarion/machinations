//---------------------------------------------------------------------------
#ifndef pathalt1H
#define pathalt1H
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <StdCtrls.hpp>
//---------------------------------------------------------------------------
#define FLOAT_ERROR .0001
//---------------------------------------------------------------------------
struct tPoint
{
    float x;
    float y;
};

struct tLine
{
    tPoint e1;
    tPoint e2;
    float a;
    float b;
    float c;
    float greaterThan;
    float x0,x1,y0,y1; //bounding box
    bool lineContains(tPoint p)
    {
        return a*p.x + b*p.y < c+FLOAT_ERROR && a*p.x + b*p.y+FLOAT_ERROR > c;
    }
    bool segmentContains(tPoint p)
    //determines if point is on line segment given point is on line
    {
        if(e1.x == e2.x)
            return e1.y+FLOAT_ERROR < p.y && p.y+FLOAT_ERROR < e2.y;
        else
            return e1.x+FLOAT_ERROR < p.x && p.x+FLOAT_ERROR < e2.x;
    }
    bool regionContains(tPoint p)
    {
        if(greaterThan)
            return a*p.x + b*p.y+FLOAT_ERROR >= c;
        else
            return a*p.x + b*p.y+FLOAT_ERROR <= c;
    }
    void init(tPoint p1, tPoint p2)
    {
        x0=x1=p1.x;
        y0=y1=p1.y;
        if(p2.x<x0) x0=p2.x;
        if(p2.x>x1) x1=p2.x;
        if(p2.y<y0) y0=p2.y;
        if(p2.y>y1) y1=p2.y;
        if(p1.x<p2.x || p1.x==p2.x&&p1.y<p2.y)
        {
            e1=p1;
            e2=p2;
        }
        else
        {
            e1=p2;
            e2=p1;
        }
        if(p1.x == p2.x)
        {
            a=1;
            b=0;
            c=p1.x;
        }
        else
        {
            float m = (p2.y-p1.y)/(p2.x-p1.x);
            a = -m;
            b = 1;
            c = p1.y - m*p1.x;
        }
    }
    tLine(tPoint p1, tPoint p2)
    {
        init(p1,p2);
    }
    tLine(tPoint p1, tPoint p2, tPoint r)
    {
        init(p1,p2);
        greaterThan = false;
        if(!regionContains(r))
            greaterThan = true;
    }
    tLine() {}
};

bool findIntersection(tLine l1, tLine l2, tPoint& p)
//returns true if l1 intersects l2
//returns false if l1 is parallel to l2
{
    float a=l1.a, b=l1.b, c=-l1.c;
    float d=l2.a, e=l2.b, f=-l2.c;
    if(a*e-b*d == 0)
        return false;
    p.x = (b*f-c*e) / (a*e-b*d);
    if(b != 0)
        p.y = (-a*p.x - c)/b;
    else
        p.y = (-d*p.x - f)/e;
    return true;
}

struct tRectangle
{
    float angle;
    tPoint points[4]; //four corners (used for pathfinding)
    tPoint visPoints[4]; //visible corners--used for drawing rectangle
    tLine lines[4]; //four bounding lines
    float x0,x1,y0,y1; //bounding box
    tRectangle(tPoint _points[], float _angle) : angle(_angle)
    {
        int i;
        for(i=0;i<4;i++)
            points[i]=_points[i];
        x0=x1=points[0].x;
        y0=y1=points[0].y;
        for(i=1;i<4;i++)
        {
            if(points[i].x<x0)  x0=points[i].x;
            if(points[i].x>x1)  x1=points[i].x;
            if(points[i].y<y0)  y0=points[i].y;
            if(points[i].y>y1)  y1=points[i].y;
        }
        lines[0] = tLine(points[0], points[1], points[2]);
        lines[1] = tLine(points[1], points[2], points[3]);
        lines[2] = tLine(points[2], points[3], points[0]);
        lines[3] = tLine(points[3], points[0], points[1]);
    }
    bool contains(tPoint p)
    {
        if(p.x>x1||x0>p.x||p.y>y1||y0>p.y)
            return false;
        return  lines[0].regionContains(p) &&
                lines[1].regionContains(p) &&
                lines[2].regionContains(p) &&
                lines[3].regionContains(p);
    }
    bool intersects(tLine l)
    {
        tPoint p;
        bool less=false,greater=false;
        float minu=MAX_FLOAT, maxu=-MAX_FLOAT, u, u0, u1;
        int i;
        if(l.x0>x1||x0>l.x1||l.y0>y1||y0>l.y1)
            return false;
        for(i=0;i<4;i++)
            if(l.a*points[i].x+l.b*points[i].y<l.c)
                less=true;
            else
                greater=true;
        if(!less||!greater)
            return false;
        for(i=0;i<4;i++)
        {
            u=l.b*points[i].x-l.a*points[i].y;
            if(u<minu)  minu=u;
            if(u>maxu)  maxu=u;
        }
        u0=l.b*l.e1.x-l.a*l.e1.y;
        u1=l.b*l.e2.x-l.a*l.e2.y;
        if(u0>u1)   SWAP(float,u0,u1)
        if(u0<minu && u1>maxu)
            return true;
        for(int i=0;i<4;i++)
            if(findIntersection(lines[i], l, p) && lines[i].segmentContains(p) && l.segmentContains(p))
                return true;
        return false;
    }
};
//---------------------------------------------------------------------------
#define HASH_SIZE    16
#define HASH_DEPTH   16

struct tVertex
{
    tPoint point;
    tRectangle *rectangle;
    tLine *l0, *l1;
    tVertex *previous; //vertex index
    float distance; //current distance to this point
    float destDistance; //shortest distance from this point to the destination
};

struct tNode
{
    tVertex *vertex;
    float value; //least distance to destination
    bool CCW;
};

struct tEdge
{
    tVertex *start;
    bool startCCW;
    tVertex *end;
    bool endCCW;
    float length; //distance from start to end
};

class TMain : public TForm
{
__published:	// IDE-managed Components
    TPanel *Panel1;
    TLabel *Label1;
    TLabel *Label2;
    TEdit *edCount;
    TLabel *Label3;
    TEdit *edSize;
    TButton *btcreateObstacles;
    TLabel *Label4;
    TLabel *Label5;
    TLabel *Label6;
    TCheckBox *cbVertices;
    TButton *btFindPath;
    TImage *imWorld;
    TButton *btExit;
    TGroupBox *GroupBox2;
    TLabel *laPathFound;
    TLabel *laSetupTime;
    TLabel *laAStarTime;
    TLabel *laVertices;
    TLabel *Label10;
    TLabel *Label11;
    TLabel *laEdges;
    TLabel *Label12;
    TLabel *Label13;
    TCheckBox *cbEdges;
    TLabel *laNodes;
    TLabel *Label8;
    TLabel *Label7;
    TEdit *edUnitSize;
    void __fastcall btcreateObstaclesClick(TObject *Sender);
    void __fastcall btExitClick(TObject *Sender);
    void __fastcall btFindPathClick(TObject *Sender);
    void __fastcall imWorldMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall cbClick(TObject *Sender);
private:	// User declarations
    vector<tRectangle *> rectangles;
    vector<tVertex *> vertices;
    vector<tEdge *> edges;
    tPoint origin;
    tPoint destination;
    tVertex *startVertex;
    tVertex *endVertex;
    tRectangle *rectHash[HASH_SIZE][HASH_SIZE][HASH_DEPTH];
    int hashFunc(float a) { int i = a/25; CHECK_RANGE(i,0,HASH_SIZE-1) return i; }
    float invHashFunc(int a) { return a * 25.0; }
    int nodes;
    void createObstacles();
    void drawObstacles();
    void drawEdges();
    void drawVertices();
    bool calcPath();
    void calcVertices();
    void removeRectangle(tRectangle *r);
    void addVertex(tVertex *v);
    void removeVertex(tVertex *v);
    void cleanup();
    void addRectangle(tRectangle *r);
    void addHash(int x, int y, tRectangle *r);
    bool isLineBlocked(const tLine& l, tRectangle *r1, tRectangle *r2);
public:		// User declarations
    __fastcall TMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMain *Main;
//---------------------------------------------------------------------------
#endif
