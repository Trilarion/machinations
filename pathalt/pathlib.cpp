//---------------------------------------------------------------------------
#include <dir>
#include <io>
#include <math>
#include <stdarg>
#include <stdio>
#include <stdlib>
#include <string>
#include <time>
#include <float>
#include <values.h>
#include <vector.h>
#include <windows>
#include <winsock2>
#ifdef SGI
    #include <c:\oglsdk\include\gl\gl>		// Header File For The SGI OpenGL Library
    #include <c:\oglsdk\include\gl\glu>		// Header File For The SGI GLu Library
#else
    #include <gl\gl>		    // Header File For The OpenGL32 Library
    #include <gl\glu>		    // Header File For The GLu32 Library
#endif
#pragma hdrstop
//---------------------------------------------------------------------------
#include "types.h"
#include "pathlib.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
