//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USERES("pathalt.res");
USEUNIT("types.cpp");
USEUNIT("timer.cpp");
USEFORM("pathalt1.cpp", Main);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    try
    {
         Application->Initialize();
         Application->CreateForm(__classid(TMain), &Main);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    return 0;
}
//---------------------------------------------------------------------------
