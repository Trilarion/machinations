//---------------------------------------------------------------------------
#ifndef pathlibH
#define pathlibH

#define FLOATERROR .0001
//---------------------------------------------------------------------------
struct tpoint
{
    float x;
    float y;
};

struct tline
{
    tpoint e1;
    tpoint e2;
    float a;
    float b;
    float c;
    float greaterthan;
    float x0,x1,y0,y1; //bounding box
    bool linecontains(tpoint p)
    {
        return a*p.x + b*p.y < c+FLOATERROR && a*p.x + b*p.y+FLOATERROR > c;
    }
    bool segmentcontains(tpoint p)
    //determines if point is on line segment given point is on line
    {
        if(e1.x == e2.x)
            return e1.y+FLOATERROR < p.y && p.y+FLOATERROR < e2.y;
        else
            return e1.x+FLOATERROR < p.x && p.x+FLOATERROR < e2.x;
    }
    bool regioncontains(tpoint p)
    {
        if(greaterthan)
            return a*p.x + b*p.y+FLOATERROR >= c;
        else
            return a*p.x + b*p.y+FLOATERROR <= c;
    }
    void init(tpoint p1, tpoint p2)
    {
        x0=x1=p1.x;
        y0=y1=p1.y;
        if(p2.x<x0) x0=p2.x;
        if(p2.x>x1) x1=p2.x;
        if(p2.y<y0) y0=p2.y;
        if(p2.y>y1) y1=p2.y;
        if(p1.x<p2.x || p1.x==p2.x&&p1.y<p2.y)
        {
            e1=p1;
            e2=p2;
        }
        else
        {
            e1=p2;
            e2=p1;
        }
        if(p1.x == p2.x)
        {
            a=1;
            b=0;
            c=p1.x;
        }
        else
        {
            float m = (p2.y-p1.y)/(p2.x-p1.x);
            a = -m;
            b = 1;
            c = p1.y - m*p1.x;
        }
    }
    tline(tpoint p1, tpoint p2)
    {
        init(p1,p2);
    }
    tline(tpoint p1, tpoint p2, tpoint r)
    {
        init(p1,p2);
        greaterthan = false;
        if(!regioncontains(r))
            greaterthan = true;
    }
    tline() {}
};

bool findintersection(tline l1, tline l2, tpoint& p)
//returns true if l1 intersects l2
//returns false if l1 is parallel to l2
{
    float a=l1.a, b=l1.b, c=-l1.c;
    float d=l2.a, e=l2.b, f=-l2.c;
    if(a*e-b*d == 0)
        return false;
    p.x = (b*f-c*e) / (a*e-b*d);
    if(b != 0)
        p.y = (-a*p.x - c)/b;
    else
        p.y = (-d*p.x - f)/e;
    return true;
}

struct trectangle
{
    float angle;
    tpoint points[4]; //four corners (used for pathfinding)
    tpoint vispoints[4]; //visible corners--used for drawing rectangle
    tline lines[4]; //four bounding lines
    float x0,x1,y0,y1; //bounding box
    trectangle(tpoint _points[], float _angle) : angle(_angle)
    {
        int i;
        for(i=0;i<4;i++)
            points[i]=_points[i];
        x0=x1=points[0].x;
        y0=y1=points[0].y;
        for(i=1;i<4;i++)
        {
            if(points[i].x<x0)  x0=points[i].x;
            if(points[i].x>x1)  x1=points[i].x;
            if(points[i].y<y0)  y0=points[i].y;
            if(points[i].y>y1)  y1=points[i].y;
        }
        lines[0] = tline(points[0], points[1], points[2]);
        lines[1] = tline(points[1], points[2], points[3]);
        lines[2] = tline(points[2], points[3], points[0]);
        lines[3] = tline(points[3], points[0], points[1]);
    }
    bool contains(tpoint p)
    {
        if(p.x>x1||x0>p.x||p.y>y1||y0>p.y)
            return false;
        return  lines[0].regioncontains(p) &&
                lines[1].regioncontains(p) &&
                lines[2].regioncontains(p) &&
                lines[3].regioncontains(p);
    }
    bool intersects(tline l)
    {
        tpoint p;
        bool less=false,greater=false;
        float minu=MAXFLOAT, maxu=-MAXFLOAT, u, u0, u1;
        int i;
        if(l.x0>x1||x0>l.x1||l.y0>y1||y0>l.y1)
            return false;
        for(i=0;i<4;i++)
            if(l.a*points[i].x+l.b*points[i].y<l.c)
                less=true;
            else
                greater=true;
        if(!less||!greater)
            return false;
        for(i=0;i<4;i++)
        {
            u=l.b*points[i].x-l.a*points[i].y;
            if(u<minu)  minu=u;
            if(u>maxu)  maxu=u;
        }
        u0=l.b*l.e1.x-l.a*l.e1.y;
        u1=l.b*l.e2.x-l.a*l.e2.y;
        if(u0>u1)   SWAP(float,u0,u1)
        if(u0<minu && u1>maxu)
            return true;
        for(int i=0;i<4;i++)
            if(findintersection(lines[i], l, p) && lines[i].segmentcontains(p) && l.segmentcontains(p))
                return true;
        return false;
    }
};
//---------------------------------------------------------------------------
#endif
