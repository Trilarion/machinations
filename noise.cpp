/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* NOISE.CPP:  Generates noise matrix using diamond/square method            *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#ifndef CACHE_HEADERS
    #include <stdlib.h> //for NULL
#endif

#include "noise.h"
#include "types.h"
#include "memwatch.h"
#pragma package(smart_init)

/****************************************************************************/
static int sizeX = 0;
static int sizeY = 0;
static float *data = NULL;
static float variation = 0;
static float power=1;

static void put(int x, int y, float c)
{
    data[x+sizeX*y] = c;
}

static float get(int x, int y)
{
    if(x<0)
        x+=sizeX;
    if(x>=sizeX)
        x-=sizeX;
    if(y<0)
        y+=sizeY;
    if(y>=sizeY)
        y-=sizeY;
    return data[x+sizeX*y];
}

static void adjustSquare(int x, int y, int size, MTRand_int32& randomNumber)
{
	float v;
	v = (get(x-size,y-size) + get(x+size,y-size) + get(x-size,y+size) + get(x+size,y+size)) / 4;
	v += (( (randomNumber() % 1024) / 1024.0) - 0.5) * pow(size,power) * variation;
	if (v < 0   ) v = 0;
	if (v > 1) v = 1;
	put(x, y, v);
}

static void adjustDiamond(int x, int y, int size, MTRand_int32& randomNumber)
{
	float v;

	v = (get(x-size,y) + get(x+size,y) + get(x,y-size) + get(x,y+size)) / 4;
	v += (( (randomNumber() % 1024) / 1024.0) - 0.5) * pow(size,power) * variation;
	if (v < 0   ) v = 0;
	if (v > 1) v = 1;
	put(x, y, v);
}

void generateNoise(float *_data, int _sizeX, int _sizeY, float _variation, float _power, MTRand_int32& randomNumber)
{
    int x,y, i = 1;
    data = _data;
    sizeX = _sizeX;
    sizeY = _sizeY;
	variation = _variation;
    power = _power;

    put(0,0,.5);

    while(i<sizeX)
        i*=2;

    while(i>1)
    {
        i/=2;
        for(x=i;x<sizeX;x+=i*2)
            for(y=i;y<sizeY;y+=i*2)
                adjustSquare(x,y,i,randomNumber);

        for(x=i;x<sizeX;x+=i*2)
            for(y=0;y<sizeY;y+=i*2)
                adjustDiamond(x,y,i,randomNumber);

        for(x=0;x<sizeX;x+=i*2)
            for(y=i;y<sizeY;y+=i*2)
                adjustDiamond(x,y,i,randomNumber);
    }
}
void smoothNoise(float *in, float *out, int sizeX, int sizeY, float amount)
{
    int x,y,i,j;
    int xx,yy;
    float total,count;

    float amounts[3]={amount/(1+amount*2),1/(1+amount*2),amount/(1+amount*2)};

    for(x=0;x<sizeX;x++)
        for(y=0;y<sizeY;y++)
        {
            count=0;
            total=0;
            for(i=-1;i<=1;i++)
                for(j=-1;j<=1;j++)
                {
                    xx=x+i;
                    yy=y+j;
                    if(xx<0 || yy<0 || xx>=sizeX || yy>=sizeY)
                        continue;
                    total+=in[yy*sizeX+xx]*amounts[i+1]*amounts[j+1];
                    count+=amounts[i+1]*amounts[j+1];
                }
            out[y*sizeX+x]=total/count;
        }
}

static inline int wrap(int a, int b)
{
    int r=a%b;
    if(r<0)
        return r+b;
    return r;
}

float interpolateSplinePoint(float *data, int sizeX, int sizeY, float x, float y)
{
    int i,j;
    int xi=(int)x;
    int yi=(int)y;
    float xr=x-(float)xi;
    float yr=y-(float)yi;
    int m,n;
    float total=0;
    float u,u2,u3;

    u=xr;
    u2=u*u;
    u3=u2*u;
    float tableX[4]={
        (-u3 + 3*u2 - 3*u + 1)/6,
        (3*u3 - 6*u2 + 4)/6,
        (-3*u3 + 3*u2 + 3*u + 1)/6,
        u3/6};

    u=yr;
    u2=u*u;
    u3=u2*u;
    float tableY[4]={
        (-u3 + 3*u2 - 3*u + 1)/6,
        (3*u3 - 6*u2 + 4)/6,
        (-3*u3 + 3*u2 + 3*u + 1)/6,
        u3/6};

    for(i=0;i<4;i++)
        for(j=0;j<4;j++)
        {
            m=(xi+i-1)%sizeX;
            if(m<0) m+=sizeX;
            n=(yi+j-1)%sizeY;
            if(n<0) n+=sizeY;

            total+=data[n*sizeX+m]*tableX[i]*tableY[j];
        }

    return total;
}

tVector3D interpolateSplineNormal(float *data, int sizeX, int sizeY, float x, float y)
{
    int i,j;
    int xi=(int)x;
    int yi=(int)y;
    float xr=x-(float)xi;
    float yr=y-(float)yi;
    int m,n;
    float zu=0,zv=0;
    tVector3D normal;
    float h;

    float *tableX=lookupSplineArray(xr);
    float *tableDX=lookupSplineDerivArray(xr);
    float *tableY=lookupSplineArray(yr);
    float *tableDY=lookupSplineDerivArray(yr);

    for(i=0;i<4;i++)
        for(j=0;j<4;j++)
        {
            m=(xi+i-1)%sizeX;
            if(m<0) m+=sizeX;
            n=(yi+j-1)%sizeY;
            if(n<0) n+=sizeY;

            h=data[n*sizeX+m];
            zu+=h*tableDX[i]*tableY[j];
            zv+=h*tableX[i]*tableDY[j];
        }

    tVector3D du={1, 0, zu};
    tVector3D dv={0, 1, zv};
    normal=du.cross(dv);

    return normal.normalize();
}

void interpolateSplineSurface(float *in, float *out, int inX, int inY, int outX, int outY)
{
    int x,y;
    float scaleX=(float)inX/outX;
    float scaleY=(float)inY/outY;

    for(x=0;x<outX;x++)
        for(y=0;y<outY;y++)
            out[y*outX+x]=interpolateSplinePoint(in,inX,inY,x*scaleX,y*scaleY);
}

void generateRandomValues(float *data, int sizeX, int sizeY, MTRand_int32& randomNumber)
{
    int x,y;
    for(x=0;x<sizeX;x++)
        for(y=0;y<sizeY;y++)
            data[y*sizeX+x]=(randomNumber()%1025)/1024.0;
}

void generateRandomSplineSurface(float *out, int inX, int inY, int outX, int outY, MTRand_int32& randomNumber)
{
    float *in=mwNew float[inX*inY];
    generateRandomValues(in,inX,inY,randomNumber);
    interpolateSplineSurface(in,out,inX,inY,outX,outY);
    mwDelete[] in;
}

void copyNoise(float *in, float *out, int sizeX, int sizeY)
{
    memcpy((void*)out,(void*)in,sizeof(float)*sizeX*sizeY);
}


