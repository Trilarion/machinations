/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* MODEL.CPP:    Loads and draws unit models                                 *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
#include <GL/glfw.h> //////////////////
#include <IL/il.h>
#include <IL/ilut.h>
/****************************************************************************/
#include "model.h"
#include "oglwrap.h"
#include "engine.h"
#include "mathexpr.h"
#pragma package(smart_init)
/****************************************************************************/
//Model container
static vector<tModelType *> modelTypes;
static const long machModelCode='M'+('A'<<8)+('C'<<16)+('H'<<24);
static tVector3D *vertexPointer=NULL;

static int compareVertices(const void *a, const void *b);

int compareVertices(const void *a, const void *b)
{
    int i;
    float za=0,zb=0;
    tTriangle *ta=(tTriangle *)a;
    tTriangle *tb=(tTriangle *)b;
    for(i=0;i<3;i++)
        za+=vertexPointer[ta->indices[i]].z;
    for(i=0;i<3;i++)
        zb+=vertexPointer[tb->indices[i]].z;

    if(za<zb)
        return -1;
    else if(za>zb)
        return 1;
    else
        return 0;
}

tVariableType::tVariableType(int _ID, tModelType *owner, tStream& s) throw(tString) :
    ID(_ID), initialX(0), minX(-MAX_FLOAT), maxX(MAX_FLOAT), initialVelocity(0),
    minVelocity(-MAX_FLOAT), maxVelocity(MAX_FLOAT),
    initialAcceleration(0), minAcceleration(-MAX_FLOAT), maxAcceleration(MAX_FLOAT),
    circumference(0), center(0), xConstrained(false), velocityConstrained(false),
	accelerationConstrained(false)
{
    bool initialXSpecified=false;
    bool minXSpecified=false;
    bool maxXSpecified=false;

#ifdef DEBUG
    if(MEMORY_VIOLATION(owner))
        throw tString("Unexpected error occurred");
#endif

	if(s.doesBlockHaveName())
	{
		name=parseIdentifier("name",s);
		if(owner->variableTypeLookup(name, /*exact=*/true)!=NULL)
			throw tString("A variable by the name of '")+name+"' already exists";
		if(owner->functionTypeLookup(name, /*exact=*/true)!=NULL)
			throw tString("A function by the name of '")+name+"' already exists";
		if(unitVariableLookup(name, /*exact=*/true)!=UNITVAR_NONE)
			throw tString("A unit variable by the name of '")+name+"' already exists";
	}

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"initial_position"))
			{
				initialX=parseFloat("initial position",s);
				initialXSpecified=true;
			}
			else if(strPrefix(str,"min_position"))
			{
				minX=parseFloat("minimum position",s);
				minXSpecified=true;
				xConstrained=true;
			}
			else if(strPrefix(str,"max_position"))
			{
				maxX=parseFloat("maximum position",s);
				maxXSpecified=true;
				xConstrained=true;
			}
			else if(strPrefix(str,"initial_rate")||strPrefix(str,"initial_velocity"))
				initialVelocity=parseFloat("initial rate",s);
			else if(strPrefix(str,"min_rate")||strPrefix(str,"min_velocity"))
			{
				minVelocity=parseFloat("minimum rate",s);
				velocityConstrained=true;
			}
			else if(strPrefix(str,"max_rate")||strPrefix(str,"max_velocity"))
			{
				maxVelocity=parseFloat("maximum rate",s);
				velocityConstrained=true;
			}
			else if(strPrefix(str,"initial_acceleration"))
				initialAcceleration=parseFloat("initial acceleration",s);
			else if(strPrefix(str,"min_acceleration"))
			{
				minAcceleration=parseFloat("minimum acceleration",s);
				accelerationConstrained=true;
			}
			else if(strPrefix(str,"max_acceleration"))
			{
				maxAcceleration=parseFloat("maximum acceleration",s);
				accelerationConstrained=true;
			}
			else if(strPrefix(str,"circumference"))
				circumference=parseFloatMin("circumference",0,s);
			else if(strPrefix(str,"channel"))
				parseChannelBlock(s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
    }

    if(minX>initialX || initialX>maxX)
        throw tString("You must enter non-decreasing values for min_position, initial_position, and max_position");
    if(minVelocity>initialVelocity || initialVelocity>maxVelocity)
        throw tString("You must enter non-decreasing values for min_rate, initial_rate, and max_rate");
    if(minAcceleration>initialAcceleration || initialAcceleration>maxAcceleration)
        throw tString("You must enter non-decreasing values for min_acceleration, initial_acceleration, and max_acceleration");

    if(circumference>0)
    {
        if(minXSpecified)
        {
            if(minX<-circumference || minX>circumference)
                throw tString("Please specify a minimum position between -circumference and circumference");
        }
        else
            minX=0;
        if(maxXSpecified)
        {
            if(maxX<-circumference || maxX>circumference)
                throw tString("Please specify a maximum position between -circumference and circumference");
        }
        else
            maxX=circumference;
        if(maxX-minX>circumference)
            throw tString("Please specify a range of values that is smaller than the circumference");

        if(initialXSpecified)
        {
			if(initialX<minX)
				throw tString("The initial position is invalid since the minimum position defaults to 0");
			if(initialX>maxX)
				throw tString("The initial position is invalid since the maximum position defaults to circumference");
		}
        else
            initialX=minX;

        center=(minX+maxX)/2;
    }
}
/****************************************************************************/
void tVariableType::parseChannelBlock(tStream& s) throw(tString)
{
    tChannel c;

	if(s.doesBlockHaveName())
	{
		c.name=parseIdentifier("name",s);
		if(channelLookup(c.name, /*exact=*/true)>=0)
			throw tString("A channel by the name of '")+name+"' already exists";
	}

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"attribute"))
				c.attribute=parseVariableAttribute("variable attribute",s);
			else if(strPrefix(str,"initial_value") || strPrefix(str,"value"))
				c.value=parseFloat("initial value",s);
			else if(strPrefix(str,"initially_active") || strPrefix(str,"active"))
				c.active=parseBool("initially active",s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

	if(c.attribute==VARIABLE_NONE)
		throw tString("Please specify an attribute for this channel");

    channels.push_back(c);
}
/****************************************************************************/
int tVariableType::channelLookup(const tString& n, bool exact)
{
    int i;
    tString ln=STRTOLOWER(n);
    for(i=0;i<channels.size();i++)
        if(STREQUAL(channels[i].name,ln))
            return i;
    if(exact)
        return -1;
    for(i=0; i<channels.size(); i++)
        if(STRPREFIX(ln,channels[i].name))
            return i;
    return -1;
}
/****************************************************************************/
float tVariableType::getInitialAttribute(eVariableAttribute attr)
{
    switch(attr)
    {
        case VARIABLE_POSITION:
            return initialX;
        case VARIABLE_VELOCITY:
            return initialVelocity;
        case VARIABLE_ACCELERATION:
            return initialAcceleration;
    }
    return 0;
}
/****************************************************************************/
void tVariable::tick(tWorldTime t)
{
    float dt;
    if(t<t1)
    {
        dt=t-t0;
        x=x0+(v0+acceleration/2*dt)*dt;
        velocity=v0+acceleration*dt;
    }
    else
    {
        dt=t-t1;
        x=x1+v1*dt;
        velocity=v1;
    }
    if(type->xConstrained)
        clampRange(x,type->minX,type->maxX);
    if(type->circumference>0 && !type->xConstrained)
    {
        x=fmod(x,type->circumference);
        if(x<0)
            x+=type->circumference;
    }
	if(type->velocityConstrained)
		clampRange(velocity,type->minVelocity,type->maxVelocity);
}
/****************************************************************************/
//This is a specialized function for aim procedures.
//Basically, it returns the position plus the velocity times the game interval.
//If this value falls outside the boundaries, then it clamps it.
/****************************************************************************/
float tVariable::getExpectedX()
{
    float a=x+velocity*gameInterval*gameSpeed/1000.0;
    if(type->xConstrained)
        clampRange(a,type->minX,type->maxX);
    return a;
}
/****************************************************************************/
void tVariable::intervalTick(tWorldTime t)
{
    if(hasTargetX)
		calcVelocity();
	if(hasTargetVelocity)
		calcAcceleration();
	transferValues(t);
}
/****************************************************************************/
void tVariable::addToChannel(int index, float value, tWorldTime t)
{
#ifdef DEBUG
    if(index<0 || index>=channelCount)
    {
        bug("tVariable::addToChannel: invalid channel index");
        return;
    }
#endif
    tChannel& c=channels[index];
    c.value+=value;
    if(index==curChannel)
    {
		setupVariables(c.attribute,c.value);
        transferValues(t);
    }
}
/****************************************************************************/
void tVariable::assignToChannel(int index, float value, tWorldTime t)
{
#ifdef DEBUG
    if(index<0 || index>=channelCount)
    {
        bug("tVariable::assignToChannel: invalid channel index");
        return;
    }
#endif
    tChannel& c=channels[index];
    c.value=value;
    if(index==curChannel)
    {
		setupVariables(c.attribute,c.value);
        transferValues(t);
    }
}
/****************************************************************************/
void tVariable::activateChannel(int index, tWorldTime t)
{
#ifdef DEBUG
    if(index<0 || index>=channelCount)
    {
        bug("tVariable::activateChannel: invalid channel index");
        return;
    }
#endif
    tChannel &c=channels[index];
    c.active=true;
    if(index<=curChannel) //this channel has equal or greater priority than the old one
    {
        curChannel=index;
		setupVariables(c.attribute,c.value);
        transferValues(t);
    }
}
/****************************************************************************/
void tVariable::deactivateChannel(int index, tWorldTime t)
{
#ifdef DEBUG
    if(index<0 || index>=channelCount)
    {
        bug("tVariable::deactivateChannel: invalid channel index");
        return;
    }
#endif
    tChannel &c=channels[index];
    c.active=false;
    if(index==curChannel) //this channel currently has the highest priority
    {
        for(curChannel++;curChannel<channelCount;curChannel++)
            if(channels[curChannel].active)
                break;
        if(curChannel<channelCount)
		{
            setupVariables(channels[curChannel].attribute,channels[curChannel].value);
		}
		else
        {
            acceleration=type->initialAcceleration;
            velocity=type->initialVelocity;
			type->initialX;
		}
        transferValues(t);
    }
}
/****************************************************************************/
void tVariable::setupVariables(eVariableAttribute attribute, float value)
{
    hasTargetX=false;
    hasTargetVelocity=false;
    switch(attribute)
    {
        case VARIABLE_POSITION:
            x=value;
            velocity=0;
            acceleration=0;
            break;
        case VARIABLE_TARGET_POSITION:
            targetX=value;
			hasTargetX=true;
			acceleration=0;
			calcVelocity();
            break;
        case VARIABLE_VELOCITY:
            velocity=value;
            acceleration=0;
            break;
        case VARIABLE_TARGET_VELOCITY:
            targetVelocity=value;
			hasTargetVelocity=true;
			calcAcceleration();
            break;
        case VARIABLE_ACCELERATION:
            acceleration=value;
            break;
    }
}
/****************************************************************************/
void tVariable::calcVelocity()
{
	if(type->circumference>0)
	{
		float d1,d2;
		if(type->xConstrained)
		{
			d1=type->center-type->circumference/2;
			d2=type->center+type->circumference/2;
		}
		else
		{
			d1=x-type->circumference/2;
			d2=x+type->circumference/2;
		}
		while(targetX<d1)
			targetX+=type->circumference;
		while(targetX>d2)
			targetX-=type->circumference;
	}
	velocity=(targetX-x)/gameInterval/gameSpeed*1000.0;

	if(type->velocityConstrained)
		clampRange(velocity,type->minVelocity,type->maxVelocity);
}
/****************************************************************************/
void tVariable::calcAcceleration()
{
	acceleration=(targetVelocity-velocity)/gameInterval/gameSpeed*1000.0;

	if(type->accelerationConstrained)
		clampRange(acceleration,type->minAcceleration,type->maxAcceleration);
}
/****************************************************************************/
tVariable::tVariable(tVariableType *_type, tWorldTime t) throw(int) : type(_type),
	channels(NULL), curChannel(0), channelCount(0), t0(0), x0(0), v0(0), t1(0), x1(0), v1(0),
	hasTargetX(false), targetX(0), hasTargetVelocity(false), targetVelocity(0)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(type))
        throw -1;
#endif
    x=type->initialX;
    velocity=type->initialVelocity;
    acceleration=type->initialAcceleration;

    channelCount=type->channels.size();
	if(channelCount>0)
	{
		channels = mwNew tChannel[channelCount];
        for(int i=0;i<channelCount;i++)
            channels[i]=type->channels[i];
	}
    for(curChannel=0;curChannel<channelCount;curChannel++)
        if(channels[curChannel].active)
            break;
    if(curChannel<channelCount)
        setupVariables(channels[curChannel].attribute,channels[curChannel].value);

    transferValues(t);
}
/****************************************************************************/
tVariable::~tVariable()
{
    if(channels)
        mwDelete[] channels;
}
/****************************************************************************/
void tVariable::transferValues(tWorldTime t)
{
    float dt;
    t0=t;
    x0=x;
    v0=velocity;
    if(acceleration>0)
    {
        dt=(type->maxVelocity-v0)/acceleration;
        t1=t0+dt;
        x1=x0+(v0+acceleration/2*dt)*dt;
        v1=type->maxVelocity;
    }
    else if(acceleration<0)
    {
        dt=(type->minVelocity-v0)/acceleration;
        t1=t0+dt;
        x1=x0+(v0+acceleration/2*dt)*dt;
        v1=type->minVelocity;
    }
    else
        t1=MAX_FLOAT; //t1 is infinitely large since acceleration is zero.
}
/****************************************************************************/
float tVariable::getAttribute(eVariableAttribute attr)
{
    switch(attr)
    {
        case VARIABLE_POSITION:
            return x;
        case VARIABLE_VELOCITY:
            return velocity;
        case VARIABLE_ACCELERATION:
            return acceleration;
    }
    return 0;
}
/****************************************************************************/
void tVariable::assignToAttribute(eVariableAttribute attribute, float value, tWorldTime t)
{
#ifdef DEBUG
    if(curChannel<0)
    {
        bug("tVariable::addToChannel: invalid channel index");
        return;
    }
#endif
	if(curChannel<channelCount) //One channel is active
	{
		tChannel& c=channels[curChannel];
	
		switch(attribute)
		{
			case VARIABLE_POSITION:
				if(c.attribute==VARIABLE_POSITION)
					return;
				break;
			case VARIABLE_TARGET_POSITION:
				return;
			case VARIABLE_VELOCITY:
				if(c.attribute==VARIABLE_POSITION || c.attribute==VARIABLE_TARGET_POSITION ||
				   c.attribute==VARIABLE_VELOCITY)
					return;
				break;
			case VARIABLE_TARGET_VELOCITY:
				return;
			case VARIABLE_ACCELERATION:
				return;
		}

        //This switch differs from the one in setupVariables in the following ways:
        //Setting the position does not affect the velocity or acceleration.
        //Setting the target position or velocity does not affect the acceleration.
        switch(attribute)
        {
            case VARIABLE_POSITION:
                x=value;
                break;
            case VARIABLE_TARGET_POSITION:
                targetX=value;
                hasTargetX=true;
                calcVelocity();
                break;
            case VARIABLE_VELOCITY:
                velocity=value;
                break;
            case VARIABLE_TARGET_VELOCITY:
                targetVelocity=value;
                hasTargetVelocity=true;
                calcAcceleration();
                break;
            case VARIABLE_ACCELERATION:
                acceleration=value;
                break;
        }
	}
    else //If none of the channels are active, then the variable is "floating" and setting
         //the position or velocity should release the variable's target position or velocity.
		setupVariables(attribute,value);

	transferValues(t);
}
/****************************************************************************/
void tVariable::addToAttribute(eVariableAttribute attribute, float value, tWorldTime t)
{
	switch(attribute)
	{
		case VARIABLE_POSITION:
			assignToAttribute(attribute,x+value,t);
			break;
		case VARIABLE_TARGET_POSITION:
			if(!hasTargetX)
				return;
			assignToAttribute(attribute,targetX+value,t);
			break;
		case VARIABLE_VELOCITY:
			assignToAttribute(attribute,velocity+value,t);
			break;
		case VARIABLE_TARGET_VELOCITY:
			if(!hasTargetVelocity)
				return;
			assignToAttribute(attribute,targetVelocity+value,t);
			break;
		case VARIABLE_ACCELERATION:
			assignToAttribute(attribute,acceleration+value,t);
			break;
	}
}
/****************************************************************************/
void tProcedureType::parseInstructionType(tModelType *owner, tStream& s) throw(tString)
{
    tInstructionType it;
    tString channelName;
#ifdef DEBUG
    if(MEMORY_VIOLATION(owner))
        throw tString("Unexpected error occurred");
#endif

    it.action=parseInstructionAction("instruction action",s);

    if(it.action==INSTRUCTION_NONE)
        ;
    else if(it.action==INSTRUCTION_WAIT)
    {
        executionTime+=parseFloatMin("delay in seconds",0,s);
        it.value=executionTime;
        s.matchOptionalToken("seconds");
    }
    else if(it.action==INSTRUCTION_SHOW || it.action==INSTRUCTION_HIDE)
        it.referenceFrameType=owner->parseReferenceFrameType("reference frame",s);
    else
    {
        if(it.action==INSTRUCTION_ASSIGN || it.action==INSTRUCTION_ADD || it.action==INSTRUCTION_SUBTRACT)
        {
            it.value=parseFloat("value to add/subtract",s);
            s.matchToken("to");
        }

		if(it.action==INSTRUCTION_ACTIVATE || it.action==INSTRUCTION_DEACTIVATE)
		{
			s.matchOptionalToken("channel");
			channelName=parseIdentifier("channel name",s);
		}
		else if(strPrefix(s.peekAhead(),"channel"))
		{
			parseString("channel",s);
			channelName=parseIdentifier("channel name",s);
		}
		else
			it.attribute=parseVariableAttribute("attribute to modify",s);

        s.matchToken("of");

        it.variableType=owner->parseVariableType("variable name",s);
		if(it.attribute==VARIABLE_NONE) //user specified a channel
		{
			it.channelIndex=it.variableType->channelLookup(channelName,/*exact=*/false);
			if(it.channelIndex<0)
				throw tString("The variable '")+it.variableType->name+"' does not have a channel by the name of '"+
					channelName+"'";
		}
    }
    instructionTypes.push_back(it);
}
/****************************************************************************/
tProcedureType::tProcedureType(tModelType *owner, tStream& s) throw(tString) :
    runExclusive(false), executionTime(0)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(owner))
        throw tString("Unexpected error occurred");
#endif

	if(s.doesBlockHaveName())
	{
		name=parseIdentifier("name",s);

		if(owner->procedureTypeLookup(name,/*exact=*/true)!=NULL)
			throw tString("A procedure by the name of '")+name+"' already exists";
	}
	
    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
			if(strPrefix(s.peekAhead(),"run_exclusive"))
			{
				parseString("tag", s);
				s.matchOptionalToken("=");
				runExclusive=parseBool("run exclusive",s);
			}
			else
				parseInstructionType(owner,s);

			s.endStatement();
		}
    }
}
/****************************************************************************/
tProcedure::tProcedure(tProcedureType *_type, tModel *_owner, tWorldTime t) throw(int) :
    type(_type), owner(_owner), t0(t)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(type))
        return;
    if(MEMORY_VIOLATION(owner))
        return;
#endif

    for(ip=type->instructionTypes.begin(); ip!=type->instructionTypes.end(); ip++)
        if(!executeInstruction(*ip,t))
            break;
}
/****************************************************************************/
void tProcedure::tick(tWorldTime t)
{
    for( ; ip!=type->instructionTypes.end(); ip++)
        if(!executeInstruction(*ip,t))
            break;
}
/****************************************************************************/
bool tProcedure::isFinished()
{
    return (ip==type->instructionTypes.end());
}
/****************************************************************************/
bool tProcedure::executeInstruction(const tInstructionType& it, float t)
{
    tVariable *v;
    float dt=t-t0;

    switch(it.action)
    {
        case INSTRUCTION_WAIT:
            if(dt<it.value)
                return false;
            return true;
        case INSTRUCTION_ASSIGN:
        case INSTRUCTION_ADD:
        case INSTRUCTION_SUBTRACT:
        case INSTRUCTION_ACTIVATE:
        case INSTRUCTION_DEACTIVATE:
            v=owner->findVariable(it.variableType);
            if(v)
            {
                switch(it.action)
                {
					case INSTRUCTION_ASSIGN:
						if(it.attribute==VARIABLE_NONE)
							v->assignToChannel(it.channelIndex,it.value,t);
						else
							v->assignToAttribute(it.attribute,it.value,t);
                        break;
					case INSTRUCTION_ADD:
						if(it.attribute==VARIABLE_NONE)
							v->addToChannel(it.channelIndex,it.value,t);
						else
							v->addToAttribute(it.attribute,it.value,t);
                        break;
					case INSTRUCTION_SUBTRACT:
						if(it.attribute==VARIABLE_NONE)
                            v->addToChannel(it.channelIndex,-it.value,t);
						else
							v->addToAttribute(it.attribute,-it.value,t);
                        break;
                    case INSTRUCTION_ACTIVATE:
                        v->activateChannel(it.channelIndex,t);
                        break;
                    case INSTRUCTION_DEACTIVATE:
                        v->deactivateChannel(it.channelIndex,t);
                        break;
                }
            }
            return true;
		case INSTRUCTION_SHOW:
			owner->showReferenceFrame(it.referenceFrameType,t);
			return true;
        case INSTRUCTION_HIDE:
            owner->hideReferenceFrame(it.referenceFrameType,t);
            return true;
    }
    return true;
}
/****************************************************************************/
tAimProcedureType::tAimProcedureType(tModelType *owner, tStream& s) throw(tString)
    : missileOriginType(NULL), missileDirectionType(NULL), topReferenceFrameType(NULL)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(owner))
        throw tString("Unexpected error occurred");
#endif

	if(s.doesBlockHaveName())
	{
		name=parseIdentifier("name",s);
		if(owner->aimProcedureTypeLookup(name, /*exact=*/true)!=NULL)
			throw tString("An aim procedure by the name of '")+name+"' already exists";
	}

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"missile_origin") || strPrefix(str,"turret_origin"))
				missileOriginType=owner->parseModelVectorType("missile origin",s);
			else if(strPrefix(str,"missile_direction") || strPrefix(str,"turret_direction"))
				missileDirectionType=owner->parseModelVectorType("missile direction",s);
			else if(strPrefix(str,"transform"))
				parseJoint(owner,s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}
    if(missileOriginType==NULL)
        throw tString("Please specify a missile origin for this aim procedure");
    if(missileDirectionType==NULL)
        throw tString("Please specify a missile direction for this aim procedure");
    if(joints.size()==0)
        throw tString("Please specify at least one transform");

    topReferenceFrameType=missileOriginType->parent;
    if(topReferenceFrameType!=missileDirectionType->parent)
        throw tString("The missile origin and missile direction must belong to the same reference frame");

    tReferenceFrameType *rft=topReferenceFrameType;
    list<tJoint>::iterator it;
	list<tVertexTransformType *>::iterator it2;
    int curHeight=0;
    while(rft)
    {
		/*for(it2=rft->vertexTransformTypes.begin();it2!=rft->vertexTransformTypes.end();it2++)
			if((*it2)->transformKind==TRANSFORM_SCALE)
				throw tString("Currently, you cannot scale any reference frames that control the missile origin and direction");*/
        for(it=joints.begin();it!=joints.end();it++)
            if(it->referenceFrameType==rft)
            {
                it->height=curHeight;
                it++;
                break;
            }
        for( ;it!=joints.end();it++)
            if(it->referenceFrameType==rft)
                throw tString("You may only declare each reference frame once");

        rft=rft->parent;
        curHeight++;
    }
    joints.sort();
	//Uninitialized joints should propagate to the front
    if(joints.front().height==-1) 
		throw tString("All of the transforms must control the missile origin and direction");
}
/****************************************************************************/
void tAimProcedureType::parseJoint(tModelType *owner, tStream& s) throw(tString)
{
    tString channelName;
    tJoint j;
    int instances;

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"variable"))
				j.variableType=owner->parseVariableType("variable name",s);
			else if(strPrefix(str,"channel"))
				channelName=parseIdentifier("channel name",s);
			else if(strPrefix(str,"approximate"))
				j.approximate=parseBool("approximate?",s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

	if(STRLEN(channelName)>0)
	{
		j.channelIndex=j.variableType->channelLookup(channelName,/*exact=*/false);
		if(j.channelIndex<0)
			throw tString("The variable '")+j.variableType->name+"' does not have a channel by the name of '"+
				channelName+"'";
	}

    j.referenceFrameType=owner->referenceFrameTypeLookup(j.variableType,instances);
    if(j.referenceFrameType==NULL)
        throw tString("The variable '")+j.variableType->name+" does not directly drive any reference frames yet";
    if(instances>1)
        throw tString("The variable '")+j.variableType->name+" drives two reference frames.  Please assign one variable to each reference frame.";
	if(j.referenceFrameType->vertexTransformTypes.size()!=1)
		throw tString("The reference frame must contain exactly one vertex_transform block");
	j.vertexTransformType=j.referenceFrameType->vertexTransformTypes.front();

    if(j.vertexTransformType->transformKind!=TRANSFORM_ROTATE &&
        j.vertexTransformType->transformKind!=TRANSFORM_TRANSLATE)
        throw tString("Currently, the aim procedure supports only rotation and translation transforms");

    joints.push_back(j);
}
/****************************************************************************/
void tAimProcedureType::run(tModel *model, const tVector3D& target, tWorldTime t)
{
    int matrixMode;
    tVariable *v;
    tReferenceFrameType *rft;
    tReferenceFrame *rf;
    tVertexTransformType *vtt;

#ifdef DEBUG
    if(MEMORY_VIOLATION(model))
        return;
#endif

    glGetIntegerv(GL_MATRIX_MODE,&matrixMode);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();

    tReferenceFrame *top=model->findReferenceFrame(topReferenceFrameType);
    glLoadMatrixf(top->getGLVertexMatrix());

    tVector3D mo=missileOriginType->v;
    tVector3D md=missileDirectionType->v.normalize();

    float mat[16]={
        mo.x, mo.y, mo.z, 1,    //Column 1
        md.x, md.y, md.z, 0,    //Column 2
        0, 0, 0, 0,             //Column 3
        0, 0, 0, 0};            //Column 4

    list<tJoint>::iterator it;

    for(it=joints.begin();it!=joints.end();it++)
    {
        glMultMatrixf(mat);
        glGetFloatv(GL_MODELVIEW_MATRIX,mat);

        tVector3D missileOrigin={mat[0],mat[1],mat[2]};
        tVector3D missileDirection={mat[4],mat[5],mat[6]};
        //NOTE: the reference frame's matrix might not be orthonormal if there are scales
        missileDirection=missileDirection.normalize();

        rft=it->referenceFrameType;
		vtt=it->vertexTransformType;
        rf=model->findReferenceFrame(rft);
        v=model->findVariable(it->variableType);

        if(rf)
        {
            tVector3D newOrigin=zeroVector3D;
            tVector3D axis=vtt->axis;
            if(vtt->hasStaticMatrix1)
            {
                newOrigin.x=vtt->staticMatrix1[12];
                newOrigin.y=vtt->staticMatrix1[13];
                newOrigin.z=vtt->staticMatrix1[14];

                axis=multiplyMatrix3x3(vtt->staticMatrix1,axis);
            }

            if(rf->getParent())
            {
                newOrigin=multiplyMatrix3x4(rf->getParent()->getGLVertexMatrix(),newOrigin);
                //NOTE: the reference frame's matrix might not be orthonormal if there are scales
                axis=multiplyMatrix3x3(rf->getParent()->getGLVertexMatrix(),axis);
            }
            axis=axis.normalize();

            tVector3D tar=target-newOrigin;
            tVector3D misOrig=missileOrigin-newOrigin;
            tVector3D misDir=missileDirection;
            float desired=0;

            if(vtt->transformKind==TRANSFORM_ROTATE)
            {
                misOrig=misOrig.perpendicularComponent(axis);
                misDir=misDir.perpendicularComponent(axis).normalize();
                tar=tar.perpendicularComponent(axis);

                if(it->approximate)
                {
                    tVector3D offset=(tar-misOrig).normalize();
                    float sinDesired;
                    desired=calcAngleBetween(misDir,offset,axis,sinDesired);
                }
                else
                {
                    float sinAlpha,sinTheta;
                    float alpha=calcAngleBetween(-misOrig,misDir,axis,sinAlpha);
                    float beta=asin(misOrig.length()*sinAlpha/tar.length());
                    float gamma=M_PI-beta-ABS(alpha);
                    if(alpha<0)
                        gamma*=-1;

                    float theta=calcAngleBetween(misOrig,tar,axis,sinTheta);
                    desired=gamma+theta;
                }
            }
            else //TRANSFORM_TRANSLATE
            {
                tVector3D offset=tar-misOrig;
                tVector3D normal=axis.perpendicularComponent(misDir); //need not be normalized
                //This finds a constant c such that offset+c*axis lies on the plane defined
                //by normal that passes through the origin.
                float denominator=normal.dot(axis);
                if(denominator!=0)
                    desired=normal.dot(offset)/denominator;
            }

			//Future optimization: check to see whether channel is active first.
			//If channel is inactive, then there's no reason to perform this calculation.
			if(it->channelIndex>=0)
            {
				v->assignToChannel(it->channelIndex,v->getX()+desired,t);
				v->activateChannel(it->channelIndex,t);
			}
			else
				v->assignToAttribute(VARIABLE_TARGET_POSITION,v->getX()+desired,t);

            //Optimization: we only need to execute the following statements if there
            //are more joints.
            if(it==joints.end())
                break;

			//This is a very rough approximation to prevent jitter
            float expected=v->getExpectedX()-v->getX();

            glLoadIdentity();
            if(vtt->transformKind==TRANSFORM_ROTATE)
            {
                glTranslatef(newOrigin.x,newOrigin.y,newOrigin.z);
                glRotatef(expected*180/M_PI,axis.x,axis.y,axis.z);
                glTranslatef(-newOrigin.x,-newOrigin.y,-newOrigin.z);
            }
            else //TRANSFORM_TRANSLATE
                glTranslatef(axis.x*expected,axis.y*expected,axis.z*expected);
        }
    }

    glPopMatrix();
    glMatrixMode(matrixMode);
}
/****************************************************************************/
void tAimProcedureType::stop(tModel *model, tWorldTime t)
{
    list<tJoint>::iterator it;
    tVariable *v;
    for(it=joints.begin();it!=joints.end();it++)
		if(it->channelIndex>=0)
		{
			v=model->findVariable(it->variableType);
			v->deactivateChannel(it->channelIndex,t);
		}
}
/****************************************************************************/
tModelVectorType::tModelVectorType(tReferenceFrameType *_parent,
    tModelType *owner, tStream& s) throw(tString) : parent(_parent)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(parent))
        throw tString("Unexpected error occurred");
    if(MEMORY_VIOLATION(owner))
        throw tString("Unexpected error occurred");
#endif

    name=parseIdentifier("name",s);
    if(owner->modelVectorTypeLookup(name, /*exact=*/true)!=NULL)
        throw tString("A point/vector by the name of '")+name+"' already exists";

    s.matchOptionalToken("=");
    
    v=parseVector3D("coordinates of point/vector",s);
}
/****************************************************************************/
void tComponentType::draw(int polyIndex, int flags)
{
    oldBlending=glIsEnabled(GL_BLEND);
    oldAlphaTesting=glIsEnabled(GL_ALPHA_TEST);
    float currentColor[4];
    glGetFloatv(GL_CURRENT_COLOR,currentColor);

    if((flags&DRAW_MODEL) || (flags&DRAW_SHADOW) || (flags&DRAW_CONVEX_SHELL))
    {
        if(currentColor[3]<1 || skin && skin->hasAlphaChannel() && (flags & (DRAW_TEXTURE|DRAW_SHADOW)))
        {
            glEnable(GL_ALPHA_TEST);
            glEnable(GL_BLEND);
        }
        if(skin && ((flags&DRAW_TEXTURE) || skin->hasAlphaChannel() && (flags&DRAW_SHADOW)))
        {
            glEnable(GL_TEXTURE_2D);
            bindSkin(skin);
        }
        if((flags&DRAW_SHADOW)==0)
            glEnable(GL_CULL_FACE);
        drawMesh(polyIndex-polyBase);
        glDisable(GL_TEXTURE_2D);
        if(!oldBlending)
            glDisable(GL_BLEND);
        if(!oldAlphaTesting)
            glDisable(GL_ALPHA_TEST);
    }
    if((flags&DRAW_GREEN_WIREFRAME) && currentColor[3]>0)
    {
        glPushAttrib(GL_CURRENT_BIT);
        glColor3f(0,1,0);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glEnable(GL_CULL_FACE);
        drawMesh(triangleCount);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glPopAttrib();
    }
    if(flags&DRAW_GREY_WIREFRAME && currentColor[3]>0)
    {
        glPushAttrib(GL_CURRENT_BIT);
        glColor3f(.5,.5,.5);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glEnable(GL_CULL_FACE);
        drawMesh(triangleCount);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glPopAttrib();
    }
    glDisable(GL_CULL_FACE);
}
/****************************************************************************/
void tComponentType::setupFastDraw()
{
    oldBlending=glIsEnabled(GL_BLEND);
    oldAlphaTesting=glIsEnabled(GL_ALPHA_TEST);
    if(skin)
    {
        if(skin->hasAlphaChannel())
        {
            glEnable(GL_ALPHA_TEST);
            glEnable(GL_BLEND);
        }
        glEnable(GL_TEXTURE_2D);
        bindSkin(skin);
    }
}
/****************************************************************************/
void tComponentType::fastDraw()
{
    drawMesh(triangleCount);
}
/****************************************************************************/
void tComponentType::cleanupFastDraw()
{
    glDisable(GL_TEXTURE_2D);
    if(!oldBlending)
        glDisable(GL_BLEND);
    if(!oldAlphaTesting)
        glDisable(GL_ALPHA_TEST);
}
/****************************************************************************/
void tComponentType::refresh()
{
    //Ignore any errors.  The model type may not display properly, but the game won't crash.
    refreshMesh();
}
/****************************************************************************/
tComponentType::tComponentType(int _polyBase, tStream& s) throw(tString) : skin(NULL),
    oldBlending(false), list(0), vertexCount(0), vertices(NULL), normals(NULL), texCoords(NULL),
    triangleCount(0), triangles(NULL), minX(MAX_FLOAT), maxX(-MAX_FLOAT),
    minY(MAX_FLOAT), maxY(-MAX_FLOAT), minZ(MAX_FLOAT), maxZ(-MAX_FLOAT), polyBase(_polyBase)
{
    tVector3D v;
    float x,y,z,angle;
	int matrixMode;

	glGetIntegerv(GL_MATRIX_MODE,&matrixMode);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
    glLoadIdentity();
	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

    try
    {
		if(s.beginBlock())
		{
			while(!s.endBlock())
			{
				tString str=parseString("tag",s);
				s.matchOptionalToken("=");

				if(strPrefix(str,"model"))
				{
					meshFilename=parseString("mesh model filename",s);
					meshFilename=tString(MODEL_PATH)+meshFilename;
				}
				else if(strPrefix(str,"skin") || strPrefix(str,"texture"))
				{
					skinFilename=parseString("skin filename",s);
					skinFilename=tString(MODEL_PATH)+skinFilename;
					skin=getSkin(skinFilename);
				}
				else if(strPrefix(str,"vertex_translate") || strPrefix(str,"vtranslate"))
				{
					x=parseFloat("x-component of translation",s);
					y=parseFloat("y-component of translation",s);
					z=parseFloat("z-component of translation",s);
	
					glMatrixMode(GL_MODELVIEW);
					glGetFloatv(GL_MODELVIEW_MATRIX,vertexMatrix);
					glLoadIdentity();
					glTranslatef(x,y,z);
					glMultMatrixf(vertexMatrix);
				}
				else if(strPrefix(str,"texture_translate") || strPrefix(str,"ttranslate"))
				{
					x=parseFloat("x-component of translation",s);
					y=parseFloat("y-component of translation",s);
	
					glMatrixMode(GL_TEXTURE);
					glGetFloatv(GL_TEXTURE_MATRIX,textureMatrix);
					glLoadIdentity();
					glTranslatef(x,y,0);
					glMultMatrixf(textureMatrix);
				}
				else if(strPrefix(str,"vertex_rotate") || strPrefix(str,"vrotate"))
				{
					angle=parseAngleRange("angle of rotation",-2/*PI*/,2/*PI*/,s);
					v=parseVector3D("axis of rotation",s).normalize();
	
					glMatrixMode(GL_MODELVIEW);
					glGetFloatv(GL_MODELVIEW_MATRIX,vertexMatrix);
					glLoadIdentity();
					glRotatef(angle*180/M_PI,v.x,v.y,v.z);
					glMultMatrixf(vertexMatrix);
				}
				else if(strPrefix(str,"texture_rotate") || strPrefix(str,"trotate"))
				{
					angle=parseAngleRange("angle of rotation",-2/*PI*/,2/*PI*/,s);
	
					glMatrixMode(GL_TEXTURE);
					glGetFloatv(GL_TEXTURE_MATRIX,textureMatrix);
					glLoadIdentity();
					glRotatef(angle*180/M_PI,0,0,1);
					glMultMatrixf(textureMatrix);
				}
				else if(strPrefix(str,"vertex_scale") || strPrefix(str,"vscale"))
				{
					x=parseFloat("x-component of scaling",s);
					y=parseFloat("y-component of scaling",s);
					z=parseFloat("z-component of scaling",s);
	
					glMatrixMode(GL_MODELVIEW);
					glGetFloatv(GL_MODELVIEW_MATRIX,vertexMatrix);
					glLoadIdentity();
					glScalef(x,y,z);
					glMultMatrixf(vertexMatrix);
				}
				else if(strPrefix(str,"texture_scale") || strPrefix(str,"tscale"))
				{
					x=parseFloat("x-component of scaling",s);
					y=parseFloat("y-component of scaling",s);
	
					glMatrixMode(GL_TEXTURE);
					glGetFloatv(GL_TEXTURE_MATRIX,textureMatrix);
					glLoadIdentity();
					glScalef(x,y,1);
					glMultMatrixf(textureMatrix);
				}
				else
					throw tString("Unrecognized tag '")+str+"'";

				s.endStatement();
			}
		}

        glGetFloatv(GL_MODELVIEW_MATRIX,vertexMatrix);
		glGetFloatv(GL_TEXTURE_MATRIX,textureMatrix);

        if(STRLEN(meshFilename)==0)
            throw tString("Please specify a model for this component");

        loadMesh();
    }
    catch(const tString& s)
    {
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
		glMatrixMode(GL_TEXTURE);
		glPopMatrix();
		glMatrixMode(matrixMode);
        destroy();
        throw s;
    }
	glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();
	glMatrixMode(matrixMode);
}
/****************************************************************************/
void tComponentType::destroy()
{
    if(skin)
        releaseSkin(skin);
    destroyMesh();
}
/****************************************************************************/
void tComponentType::loadMesh() throw(tString)
{
	FILE* file=NULL;

    long l;
    int i,j;

    try
    {
        //Open the .mod model file
        file=fopen(CHARPTR(meshFilename), "rb");
        if(file==NULL)
            throw tString("Failed to open '")+meshFilename+"'";

        //Read Machinations model code
        if(fread(&l, sizeof(l), 1, file)<1 || l!=machModelCode)
            throw tString("'")+meshFilename+"' is not a valid Machinations model";

        if(fread(&l, sizeof(l), 1, file)<1)
            throw tString("Error loading '")+meshFilename+"': unexpected end-of-file";

        vertexCount=(int)l;
        if(fread(&l, sizeof(l), 1, file)<1)
            throw tString("Error loading '")+meshFilename+"': unexpected end-of-file";

        triangleCount=(int)l;

        vertices = mwNew tVector3D[vertexCount];
        normals = mwNew tVector3D[vertexCount];
        texCoords = mwNew tVector2D[vertexCount];
        triangles = mwNew tTriangle[triangleCount];

        if(fread(vertices, sizeof(tVector3D), vertexCount, file)<vertexCount ||
            fread(normals, sizeof(tVector3D), vertexCount, file)<vertexCount ||
            fread(texCoords, sizeof(tVector2D), vertexCount, file)<vertexCount ||
            fread(triangles, sizeof(tTriangle), triangleCount, file)<triangleCount)
            throw tString("Error loading '")+meshFilename+"': unexpected end-of-file";

        for(i=0;i<vertexCount;i++)
        {
            vertices[i]=multiplyMatrix3x4(vertexMatrix,vertices[i]);
            normals[i]=multiplyMatrix3x3(vertexMatrix,normals[i]);
			texCoords[i]=multiplyMatrix2x3(textureMatrix,texCoords[i]);
            //Re-normalize normals since scale transforms may change their length
            normals[i]=normals[i].normalize();
             
            if(vertices[i].x<minX)  minX=vertices[i].x;
            if(vertices[i].x>maxX)  maxX=vertices[i].x;
            if(vertices[i].y<minY)  minY=vertices[i].y;
            if(vertices[i].y>maxY)  maxY=vertices[i].y;
            if(vertices[i].z<minZ)  minZ=vertices[i].z;
            if(vertices[i].z>maxZ)  maxZ=vertices[i].z;
        }

        for(i=0;i<triangleCount;i++)
            for(j=0;j<3;j++)
                if(triangles[i].indices[j]>=vertexCount)
                    throw tString("Error loading '")+meshFilename+"': invalid vertex index";

        //Export the vertex pointer so that compareVertices can access it.
        vertexPointer=vertices;
        qsort(triangles, triangleCount, sizeof(tTriangle), compareVertices);
		vertexPointer=NULL;

        list = glGenLists(1);

        refreshMesh();
    }
    catch(const tString& s)
    {
        if(file)
            fclose(file);
        throw s;
    }
    if(file)
        fclose(file);
}
/****************************************************************************/
void tComponentType::refreshMesh()
{
#ifdef DEBUG
    if(list==0 || vertexCount==0 || triangleCount==0)
    {
        bug("tComponentType::refreshMesh: mesh not initialized");
        return;
    }
#endif
    glNewList(list, GL_COMPILE);
        renderMesh(triangleCount);
    glEndList();
}
/****************************************************************************/
void tComponentType::drawMesh(int polyIndex)
{
    if( list>0 && (polyIndex>=triangleCount || triangleCount==0) )
        glCallList(list);
    else if(triangleCount>0)
        renderMesh(polyIndex);
    else
        bug("tComponentType::drawMesh: mesh not loaded");
}
/****************************************************************************/
void tComponentType::destroyMesh()
{
    if(list>0)
    {
        glDeleteLists(list, 1);
        list=0;
    }
    vertexCount=0;
    triangleCount=0;
    if(vertices)
    {
        mwDelete[] vertices;
        vertices=NULL;
    }
    if(normals)
    {
        mwDelete[] normals;
        normals=NULL;
    }
    if(texCoords)
    {
        mwDelete[] texCoords;
        texCoords=NULL;
    }
    if(triangles)
    {
        mwDelete[] triangles;
        triangles=NULL;
    }
}
/****************************************************************************/
void tComponentType::renderMesh(int count)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(vertices) || MEMORY_VIOLATION(normals) ||
        MEMORY_VIOLATION(texCoords) || MEMORY_VIOLATION(triangles))
        return;
#endif
    if(count<=0)
        return;
    if(count>triangleCount)
        count=triangleCount;

    glVertexPointer(3, GL_FLOAT, 0, vertices);
    glNormalPointer(GL_FLOAT, 0, normals);
    glTexCoordPointer(2, GL_FLOAT, 0, texCoords);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glDrawElements(GL_TRIANGLES, count*3, GL_UNSIGNED_SHORT, triangles);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}
/****************************************************************************/
bool tComponentType::getRepairPoints(int polyIndex, tVector3D& a, tVector3D& b)
{
#ifdef DEBUG
    if(vertexCount==0)
    {
        bug("tComponentType::getRepairPoints: mesh not loaded");
        return false;
    }
#endif
	polyIndex-=polyBase;
    if(polyIndex>=0 && polyIndex<triangleCount)
    {
        a=vertices[triangles[polyIndex].indices[0]];
        b=vertices[triangles[polyIndex].indices[1]];
        return true;
    }
    return false;
}
/****************************************************************************/
tFunctionType::tFunctionType(int _ID, tModelType *owner, tStream& s) throw(tString) :
    ID(_ID), expression(NULL)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(owner))
        throw tString("Unexpected error occurred");
#endif

    try
    {
        if(s.doesBlockHaveName())
        {
            name=parseIdentifier("name",s);
            if(owner->variableTypeLookup(name, /*exact=*/true)!=NULL)
                throw tString("A variable by the name of '")+name+"' already exists";
            if(owner->functionTypeLookup(name, /*exact=*/true)!=NULL)
                throw tString("A function by the name of '")+name+"' already exists";
            if(unitVariableLookup(name, /*exact=*/true)!=UNITVAR_NONE)
                throw tString("A unit variable by the name of '")+name+"' already exists";
        }

        if(s.beginBlock())
        {
            while(!s.endBlock())
            {
                tString str=parseString("tag",s);
                s.matchOptionalToken("=");

                if(strPrefix(str,"expression"))
                {
                    tString expr=parseString("mathematical expression",s);
                    expression=parseExpression(CHARPTR(expr),owner);
                }
                else if(strPrefix(str,"map"))
                {
                    tMapping m;
                    m.from=parseFloat("value from which to map",s);
                    s.matchToken("to");
                    m.to=parseFloat("value to which to map",s);

                    while(s.hasOptionalParameter())
                    {
                        tString option=parseString("options",s);
                        if(strPrefix(option,"smooth"))
                            m.isSmooth=true;
                        else if(strPrefix(option,"sharp"))
                            m.isSmooth=false;
                        else if(strPrefix(option,"closed"))
                            m.isClosed=true;
                        else if(strPrefix(option,"open"))
                            m.isClosed=false;
                        else
                            throw tString("Please write the word 'smooth', 'sharp', 'closed', or 'open' at the end of the mapping.");
                    }

                    if(mappings.size()>0 && m.from<mappings.back().from)
                        throw tString("You must enter non-decreasing values to map from (e.g., 1, 2, 2, 3, etc.)");
                    mappings.push_back(m);
                }
                else
                    throw tString("Unrecognized tag '")+str+"'";

                s.endStatement();
            }
        }

        if(expression==NULL)
            throw tString("You need to specify an expression for this function");
        initialValue=evaluate(NULL);
    }
    catch(const tString& msg)
    {
        destroy();
        throw msg;
    }
}
/****************************************************************************/
float tFunctionType::evaluate(tModel *model)
{
    float value=evaluateExpression(expression,model);
    return transformValue(value, mappings);
}
/****************************************************************************/
void tFunctionType::destroy()
{
    if(expression)
        mwDelete expression;
}
/****************************************************************************/
tFunction::tFunction(tFunctionType *_type, tModel *_owner) throw(int) : type(_type),
    owner(_owner)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(type))
        return;
    if(MEMORY_VIOLATION(owner))
        return;
#endif

    value=type->evaluate(owner);
}
/****************************************************************************/
void tFunction::tick(tWorldTime t)
{
    value=type->evaluate(owner);
}
/****************************************************************************/
tVertexTransformType::tVertexTransformType(tModelType *modelType, tArgumentType<float> *at, tStream& s)
    throw(tString) : domainMin(0), domainMax(1), scale(1), bias(0), hasStaticMatrix1(false),
	hasStaticMatrix2(false), transformKind(TRANSFORM_NONE), axis(zeroVector3D)
{
	//NOTE: 'at' can be NULL

	float rangeMin=0;
	float rangeMax=1;

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"static_translate") || strPrefix(str,"stranslate"))
			{
				float x=parseFloat("x-component of translation",s);
				float y=parseFloat("y-component of translation",s);
				float z=parseFloat("z-component of translation",s);
				glTranslatef(x,y,z);
			}
			else if(strPrefix(str,"static_rotate") || strPrefix(str,"srotate"))
			{
				float angle=parseAngleRange("angle of rotation",-2/*PI*/,2/*PI*/,s);
				tVector3D v=parseVector3D("axis of rotation",s).normalize();
				glRotatef(angle*180/M_PI,v.x,v.y,v.z);
			}
			else if(strPrefix(str,"static_scale") || strPrefix(str,"sscale"))
			{
				float x=parseFloat("x-component of scaling",s);
				float y=parseFloat("y-component of scaling",s);
				float z=parseFloat("z-component of scaling",s);
				glScalef(x,y,z);
			}
			else if(strPrefix(str,"dynamic_translate") || strPrefix(str,"dtranslate"))
				parseDynamicTransform(TRANSFORM_TRANSLATE,rangeMin,rangeMax,modelType,at,s);
			else if(strPrefix(str,"dynamic_rotate") || strPrefix(str,"drotate"))
				parseDynamicTransform(TRANSFORM_ROTATE,rangeMin,rangeMax,modelType,at,s);
			else if(strPrefix(str,"dynamic_scale") || strPrefix(str,"dscale"))
				parseDynamicTransform(TRANSFORM_SCALE,rangeMin,rangeMax,modelType,at,s);
			else if(strPrefix(str,"domain"))
			{
				domainMin=parseFloat("minimum value",s);
				s.matchToken("to");
				domainMax=parseFloat("maximum value",s);
				if(domainMin>=domainMax)
					throw tString("The domain must span a positive interval");
			}
			else if(strPrefix(str,"argument"))
			{
				if(at==NULL)
					throw tString("Please specify the argument inside the piecewise_vertex_transform block");
				at->load(modelType, s);
			}
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

	//If the argument is NULL, the transform belongs to a piecewise vertex transformation, so
	//the model type will ensure that the argument type is loaded.
	if(transformKind!=TRANSFORM_NONE && at && at->loaded==false)
		throw tString("Please specify an argument for this vertex transformation");

	//Our check above should prevent division-by-zero
	scale=(rangeMax-rangeMin)/(domainMax-domainMin);
	bias=-domainMin*(rangeMax-rangeMin)/(domainMax-domainMin)+rangeMin;

	if(transformKind==TRANSFORM_NONE)
    {
		glGetFloatv(GL_MODELVIEW_MATRIX,staticMatrix1);
        hasStaticMatrix1=!isIdentityMatrix(staticMatrix1);
    }
	else
    {
		glGetFloatv(GL_MODELVIEW_MATRIX,staticMatrix2);
        hasStaticMatrix2=!isIdentityMatrix(staticMatrix2);
    }
}
/****************************************************************************/
void tVertexTransformType::parseDynamicTransform(eTransformKind kind, float& rangeMin, float& rangeMax,
    tModelType *modelType, tArgumentType<float> *at, tStream& s) throw(tString)
{
	tVector3D center=zeroVector3D;

	if(transformKind!=TRANSFORM_NONE)
		throw tString("A vertex transformation can have at most one dynamic transformation");

	transformKind=kind;

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"axis"))
				axis=parseVector3D("axis",s);
			else if(strPrefix(str,"x_scale"))
				axis.x=parseFloat("x scale",s);
			else if(strPrefix(str,"y_scale"))
				axis.y=parseFloat("y scale",s);
			else if(strPrefix(str,"z_scale"))
				axis.z=parseFloat("z scale",s);
			else if(strPrefix(str,"center"))
				center=parseVector3D("center",s);
			else if(strPrefix(str,"range"))
			{
				rangeMin=parseFloat("minimum value",s);
				s.matchToken("to");
				rangeMax=parseFloat("maximum value",s);
			}
			else if(strPrefix(str,"argument"))
			{
				if(at==NULL)
					throw tString("Please specify the argument inside the piecewise_vertex_transform block");
				at->load(modelType, s);
			}
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

	if(axis==zeroVector3D)
		throw tString("Please specify a non-zero axis for this vertex transform");
	if(transformKind==TRANSFORM_ROTATE || transformKind==TRANSFORM_TRANSLATE)
		axis=axis.normalize();

	glTranslatef(center.x,center.y,center.z);
	glGetFloatv(GL_MODELVIEW_MATRIX,staticMatrix1);
    hasStaticMatrix1=!isIdentityMatrix(staticMatrix1);

	glLoadIdentity();
	glTranslatef(-center.x,-center.y,-center.z);
}
/****************************************************************************/
void tVertexTransformType::multGLMatrix(float value)
{
	value*=scale;
	value+=bias;

    float sx,sy,sz;
	if(hasStaticMatrix1)
		glMultMatrixf(staticMatrix1);
    switch(transformKind)
    {
        case TRANSFORM_TRANSLATE:
            glTranslatef(value*axis.x,value*axis.y,value*axis.z);
            break;
        case TRANSFORM_ROTATE:
            glRotatef(value*180/M_PI,axis.x,axis.y,axis.z);
            break;
        case TRANSFORM_SCALE:
            if(axis.x==0)       sx=1;
            else if(axis.x==1)  sx=value;
            else                sx=pow(value,axis.x);

            if(axis.y==0)       sy=1;
            else if(axis.y==1)  sy=value;
            else                sy=pow(value,axis.y);

            if(axis.z==0)       sz=1;
            else if(axis.z==1)  sz=value;
            else                sz=pow(value,axis.z);

            glScalef(sx,sy,sz);
            break;
    }
	if(hasStaticMatrix2)
		glMultMatrixf(staticMatrix2);
}
/****************************************************************************/
tTextureTransformType::tTextureTransformType(tModelType *modelType, tArgumentType<float> *at,
	bool *affectsChildren, tStream& s) throw(tString) : domainMin(0), domainMax(1), scale(1),
	bias(0), hasStaticMatrix1(false), hasStaticMatrix2(false), transformKind(TRANSFORM_NONE),
	axis(zeroVector2D)
{
	//NOTE: at and affectsChildren can be NULL

	float rangeMin=0;
	float rangeMax=1;

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"static_translate") || strPrefix(str,"stranslate"))
			{
				float x=parseFloat("x-component of translation",s);
				float y=parseFloat("y-component of translation",s);
				glTranslatef(x,y,0);
			}
			else if(strPrefix(str,"static_rotate") || strPrefix(str,"srotate"))
			{
				float angle=parseAngleRange("angle of rotation",-2/*PI*/,2/*PI*/,s);
				glRotatef(angle*180/M_PI,0,0,1);
			}
			else if(strPrefix(str,"static_scale") || strPrefix(str,"sscale"))
			{
				float x=parseFloat("x-component of scaling",s);
				float y=parseFloat("y-component of scaling",s);
				glScalef(x,y,0);
			}
			else if(strPrefix(str,"dynamic_translate") || strPrefix(str,"dtranslate"))
				parseDynamicTransform(TRANSFORM_TRANSLATE,rangeMin,rangeMax,modelType,at,s);
			else if(strPrefix(str,"dynamic_rotate") || strPrefix(str,"drotate"))
				parseDynamicTransform(TRANSFORM_ROTATE,rangeMin,rangeMax,modelType,at,s);
			else if(strPrefix(str,"dynamic_scale") || strPrefix(str,"dscale"))
				parseDynamicTransform(TRANSFORM_SCALE,rangeMin,rangeMax,modelType,at,s);
			else if(strPrefix(str,"domain"))
			{
				domainMin=parseFloat("minimum value",s);
				s.matchToken("to");
				domainMax=parseFloat("maximum value",s);
				if(domainMin>=domainMax)
					throw tString("The domain must span a positive interval");
			}
			else if(strPrefix(str,"argument"))
			{
				if(at==NULL)
					throw tString("Please specify the argument inside the piecewise_texture_transform block");
				at->load(modelType, s);
			}
			else if(strPrefix(str,"affects_children"))
			{
				if(affectsChildren==NULL)
					throw tString("Please specify the affects_children parameter inside the piecewise_texture_transform block");
				*affectsChildren=parseBool("affects children?",s);
			}
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

	//If the argument is NULL, the transform belongs to a piecewise texture transformation, so
	//the model type will ensure that the argument type is loaded.
	if(transformKind!=TRANSFORM_NONE && at && at->loaded==false)
		throw tString("Please specify an argument for this texture transformation");

	//Our check above should prevent division-by-zero
	scale=(rangeMax-rangeMin)/(domainMax-domainMin);
	bias=-domainMin*(rangeMax-rangeMin)/(domainMax-domainMin)+rangeMin;

	if(transformKind==TRANSFORM_NONE)
    {
		glGetFloatv(GL_TEXTURE_MATRIX,staticMatrix1);
        hasStaticMatrix1=!isIdentityMatrix(staticMatrix1);
    }
	else
    {
        glGetFloatv(GL_TEXTURE_MATRIX,staticMatrix2);
        hasStaticMatrix2=!isIdentityMatrix(staticMatrix2);
    }
}
/****************************************************************************/
void tTextureTransformType::parseDynamicTransform(eTransformKind kind, float& rangeMin, float& rangeMax,
    tModelType *modelType, tArgumentType<float> *at, tStream& s) throw(tString)
{
	tVector2D center=zeroVector2D;

	if(transformKind!=TRANSFORM_NONE)
		throw tString("A texture transformation can have at most one dynamic transformation");

	transformKind=kind;

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"axis"))
				axis=parseVector2D("axis",s);
			else if(strPrefix(str,"x_scale"))
				axis.x=parseFloat("x scale",s);
			else if(strPrefix(str,"y_scale"))
				axis.y=parseFloat("y scale",s);
			else if(strPrefix(str,"center"))
				center=parseVector2D("center",s);
			else if(strPrefix(str,"range"))
			{
				rangeMin=parseFloat("minimum value",s);
				s.matchToken("to");
				rangeMax=parseFloat("maximum value",s);
			}
			else if(strPrefix(str,"argument"))
			{
				if(at==NULL)
					throw tString("Please specify the argument inside the piecewise_texture_transform block");
				at->load(modelType, s);
			}
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

	if(transformKind!=TRANSFORM_ROTATE && axis==zeroVector2D)
		throw tString("Please specify a non-zero axis for this texture transform");
	//Axis is unused for rotations
	if(transformKind==TRANSFORM_TRANSLATE)
		axis=axis.normalize();

	glTranslatef(center.x,center.y,0);
	glGetFloatv(GL_TEXTURE_MATRIX,staticMatrix1);
    hasStaticMatrix1=!isIdentityMatrix(staticMatrix1);

	glLoadIdentity();
	glTranslatef(-center.x,-center.y,0);
}
/****************************************************************************/
void tTextureTransformType::multGLMatrix(float value)
{
	value*=scale;
	value+=bias;

	float sx,sy;
	if(hasStaticMatrix1)
		glMultMatrixf(staticMatrix1);
    switch(transformKind)
    {
        case TRANSFORM_TRANSLATE:
            glTranslatef(value*axis.x,value*axis.y,0);
            break;
        case TRANSFORM_ROTATE:
            glRotatef(value*180/M_PI,0,0,1);
            break;
        case TRANSFORM_SCALE:
            if(axis.x==0)       sx=1;
            else if(axis.x==1)  sx=value;
            else                sx=pow(value,axis.x);

            if(axis.y==0)       sy=1;
            else if(axis.y==1)  sy=value;
            else                sy=pow(value,axis.y);

            glScalef(sx,sy,1);
            break;
    }
	if(hasStaticMatrix2)
		glMultMatrixf(staticMatrix2);
}
/****************************************************************************/
void tColorTransformType::load(tModelType *modelType, tArgumentType<float> *argumentType, bool *affectsChildren,
							   tStream& s) throw(tString)
{
    float previousFrom=-MAX_FLOAT;

#ifdef DEBUG
	if(MEMORY_VIOLATION(argumentType))
		return;
	if(MEMORY_VIOLATION(affectsChildren))
		return;
#endif

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"kind") || strPrefix(str,"color_component"))
			{
				if(colorComponent!=COLOR_NONE)
					throw tString("You may specify the transform kind only once");
				colorComponent=parseColorComponent("color transform kind",s);
	
				switch(colorComponent)
				{
					case COLOR_RED:
						hasRedComponent=true;
						break;
					case COLOR_GREEN:
						hasGreenComponent=true;
						break;
					case COLOR_BLUE:
						hasBlueComponent=true;
						break;
					case COLOR_ALPHA:
						hasAlphaComponent=true;
						break;
					case COLOR_RGB:
						hasRedComponent=true;
						hasGreenComponent=true;
						hasBlueComponent=true;
						break;
					case COLOR_RGBA:
						hasRedComponent=true;
						hasGreenComponent=true;
						hasBlueComponent=true;
						hasAlphaComponent=true;
						break;
				}
			}
			else if(strPrefix(str,"mapping"))
			{
				if(colorComponent==COLOR_NONE)
					throw tString("Please specify the transform kind before the mappings");
	
				float from=parseFloat("value from which to map",s);
				s.matchToken("to");
	
				float redTo=0,greenTo=0,blueTo=0,alphaTo=0;
				if(hasRedComponent)
					redTo=parseFloat("red value to which to map",s);
				if(hasGreenComponent)
					greenTo=parseFloat("green value to which to map",s);
				if(hasBlueComponent)
					blueTo=parseFloat("blue value to which to map",s);
				if(hasAlphaComponent)
					alphaTo=parseFloat("alpha value to which to map",s);
	
				bool isClosed=true,isSmooth=false;
				while(s.hasOptionalParameter())
				{
					tString option=parseString("options",s);
					if(strPrefix(option,"smooth"))
						isSmooth=true;
					else if(strPrefix(option,"sharp"))
						isSmooth=false;
					else if(strPrefix(option,"closed"))
						isClosed=true;
					else if(strPrefix(option,"open"))
						isClosed=false;
					else
						throw tString("Please write the word 'smooth', 'sharp', 'closed', or 'open' at the end of the mapping.");
				}
	
				if(from<previousFrom)
					throw tString("You must enter non-decreasing values from which to map (e.g., 1, 2, 2, 3, etc.)");
	
				if(hasRedComponent)
				{
					tMapping m(from,redTo,isClosed,isSmooth);
					redMappings.push_back(m);
				}
				if(hasGreenComponent)
				{
					tMapping m(from,greenTo,isClosed,isSmooth);
					greenMappings.push_back(m);
				}
				if(hasBlueComponent)
				{
					tMapping m(from,blueTo,isClosed,isSmooth);
					blueMappings.push_back(m);
				}
				if(hasAlphaComponent)
				{
					tMapping m(from,alphaTo,isClosed,isSmooth);
					alphaMappings.push_back(m);
				}
	
				previousFrom=from;
			}
			else if(strPrefix(str,"argument"))
				argumentType->load(modelType, s);
			else if(strPrefix(str,"affects_children"))
				*affectsChildren=parseBool("affects children?",s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}
    if(colorComponent==COLOR_NONE)
        throw tString("Please specify the transform kind for this color transform");

    loaded=true;
}
/****************************************************************************/
void tColorTransformType::multGLMatrix(float value, bool RGBMask)
{
    float color[4];
    glGetFloatv(GL_CURRENT_COLOR,color);

	if(RGBMask)
	{
        if(hasRedComponent)
    		color[0]*=transformValue(value,redMappings);
        if(hasGreenComponent)
    		color[1]*=transformValue(value,greenMappings);
        if(hasBlueComponent)
            color[2]*=transformValue(value,blueMappings);
	}
    if(hasAlphaComponent)
    	color[3]*=transformValue(value,alphaMappings);

    glColor4fv(color);
}
/****************************************************************************/
tReferenceFrameType::tReferenceFrameType(tReferenceFrameType *_parent,
    tModelType *owner, tStream& s) throw(tString) : ID(0), parent(_parent),
    initiallyVisible(true), polyCount(0), maxRadius(-MAX_FLOAT), selRadius(-MAX_FLOAT),
    radius3D(-MAX_FLOAT), maxZ(-MAX_FLOAT), vertexArgumentType(0,BOUNDARY_NONE),
	textureArgumentType(0,BOUNDARY_NONE), colorArgumentType(0,BOUNDARY_NONE),
	textureTransformAffectsChildren(false), colorTransformAffectsChildren(false)
{
#ifdef DEBUG
    //parent CAN be NULL if the reference frame is at the root of the model type.
    if(MEMORY_VIOLATION(owner))
        throw tString("Unexpected error occurred");
#endif

	for(int i=0;i<16;i++)
		GLVertexMatrix[i]=0;

	if(s.doesBlockHaveName())
	{
		name=parseIdentifier("name",s);
		//We will check for duplicate names later.
	}
    
	try
    {
		if(s.beginBlock())
		{
			while(!s.endBlock())
			{
				tString str=parseString("tag",s);
				s.matchOptionalToken("=");
	
				if(strPrefix(str,"vertex_transformation") ||
					strPrefix(str,"vtransformation") ||
					strPrefix(str,"piecewise_vertex_transformation"))
					parseVertexTransform(strPrefix(str,"piecewise_vertex_transformation"),owner,s);
				else if(strPrefix(str,"texture_transformation") ||
					strPrefix(str,"ttransformation") ||
					strPrefix(str,"piecewise_texture_transformation"))
					parseTextureTransform(strPrefix(str,"piecewise_texture_transformation"),owner,s);
				else if(strPrefix(str,"color_transformation") || strPrefix(str,"ctransformation"))
				{
					if(colorTransformType.loaded)
						throw tString("You may define only one color transformation in each reference frame");
					colorTransformType.load(owner,&colorArgumentType,&colorTransformAffectsChildren,s);
				}
				else if(strPrefix(str,"component"))
				{
					tComponentType *ct=mwNew tComponentType(polyCount,s);
					if(ct)
					{
						polyCount+=ct->getPolyCount();
						componentTypes.push_back(ct);
					}
				}
				else if(strPrefix(str,"point") || strPrefix(str,"vector"))
				{
					tModelVectorType *mvt=owner->parseModelVectorTypeBlock(this,s);
					if(mvt)
						modelVectorTypes.push_back(mvt);
				}
				else if(strPrefix(str,"reference_frame"))
				{
					tReferenceFrameType *rft=owner->parseReferenceFrameTypeBlock(this,s);
					if(rft)
						referenceFrameTypes.push_back(rft);
				}
				else if(strPrefix(str,"initially_visible"))
					initiallyVisible=parseBool("initially visible",s);
				else if(strPrefix(str,"max_radius"))
					maxRadius=parseFloatMin("maximum radius",0,s);
				else if(strPrefix(str,"sel_radius"))
					selRadius=parseFloatMin("selection radius",0,s);
				else if(strPrefix(str,"max_z"))
					maxZ=parseFloatMin("maximum z",0,s);
				else
					throw tString("Unrecognized tag '")+str+"'";

				s.endStatement();
			}
		}
	}
    catch(const tString& s)
    {
        destroy();
        throw s;
    }
}
/****************************************************************************/
void tReferenceFrameType::parseVertexTransform(bool isPiecewise, tModelType *owner, tStream& s) throw(tString)
{
    if(vertexTransformTypes.size()>0)
        throw tString("You may specify only one vertex transform in each reference frame");

    int matrixMode;
    glGetIntegerv(GL_MATRIX_MODE,&matrixMode);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    try
    {
        if(isPiecewise)
            parsePiecewiseVertexTransform(owner,s);
        else
        {
            tVertexTransformType *vtt=mwNew tVertexTransformType(owner,&vertexArgumentType,s);
            vertexTransformTypes.push_back(vtt);
        }
    }
    catch(const tString& msg)
    {
        glPopMatrix();
        glMatrixMode(matrixMode);
        throw msg;
    }

    glPopMatrix();
    glMatrixMode(matrixMode);
}
/****************************************************************************/
void tReferenceFrameType::parseTextureTransform(bool isPiecewise, tModelType *owner, tStream& s) throw(tString)
{
    if(textureTransformTypes.size()>0)
        throw tString("You may specify only one texture transform in each reference frame");

    int matrixMode;
    glGetIntegerv(GL_MATRIX_MODE,&matrixMode);
    glMatrixMode(GL_TEXTURE);
    glPushMatrix();
    glLoadIdentity();

    try
    {
        if(isPiecewise)
            parsePiecewiseTextureTransform(owner,s);
        else
        {
            tTextureTransformType *ttt=mwNew
                tTextureTransformType(owner,&textureArgumentType,
                &textureTransformAffectsChildren,s);
            textureTransformTypes.push_back(ttt);
        }
    }
    catch(const tString& msg)
    {
        glPopMatrix();
        glMatrixMode(matrixMode);
        throw msg;
    }

    glPopMatrix();
    glMatrixMode(matrixMode);
}
/****************************************************************************/
void tReferenceFrameType::parsePiecewiseVertexTransform(tModelType *owner, tStream& s) throw(tString)
{
	bool inheritPreviousTransform=false;

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"argument"))
				vertexArgumentType.load(owner,s);
			else if(strPrefix(str,"inherit_previous_transformation"))
			{
				if(vertexTransformTypes.size()>0)
					throw tString("Please specify the inherit_previous_transformation parameter at the top of the block");
				inheritPreviousTransform=parseBool("inherit previous transform?",s);
			}
			else if(strPrefix(str,"vertex_transform") ||
				strPrefix(str,"vtransform"))
			{
				//NULL means that the user cannot declare the argument inside the vertex_transform
				//block since he/she has already declared the argument inside the
				//piecewise_vertex_transform block.
				tVertexTransformType *vtt=mwNew tVertexTransformType(owner,NULL,s);
				if(vertexTransformTypes.size()>0 &&
				   vtt->domainMin != vertexTransformTypes.back()->domainMax)
				{
					mwDelete vtt; //This object will not get deallocated in the destructor
					throw tString("Please specify a continuous series of domains so that the matrix function is defined everywhere");
				}
				vertexTransformTypes.push_back(vtt);
	
				//Setup the matrix for the next transformation
				glLoadIdentity();
				if(inheritPreviousTransform)
					vtt->multGLMatrix(vtt->domainMax);
			}
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

	if(!vertexArgumentType.loaded)
		throw tString("Please specify an argument for this piece-wise vertex transform");
	if(vertexTransformTypes.size()==0)
		throw tString("Please specify at least one vertex tranform in this piece-wise vertex transform");
}
/****************************************************************************/
void tReferenceFrameType::parsePiecewiseTextureTransform(tModelType *owner, tStream& s) throw(tString)
{
	bool inheritPreviousTransform=false;

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"argument"))
				textureArgumentType.load(owner,s);
			else if(strPrefix(str,"affects_children"))
				textureTransformAffectsChildren=parseBool("affects children?",s);
			else if(strPrefix(str,"inherit_previous_transformation"))
			{
				if(textureTransformTypes.size()>0)
					throw tString("Please specify the inherit_previous_transformation parameter at the top of the block");
				inheritPreviousTransform=parseBool("inherit previous transform?",s);
			}
			else if(strPrefix(str,"texture_transform") || strPrefix(str,"ttransform"))
			{
				//NULL means that the user cannot declare the argument inside the texture_transform
				//block since he/she has already declared the argument inside the
				//piecewise_texture_transform block.  The same holds for the affects_children parameters.
				tTextureTransformType *ttt=mwNew tTextureTransformType(owner,NULL,NULL,s);
				if(textureTransformTypes.size()>0 &&
				   ttt->domainMin != textureTransformTypes.back()->domainMax)
				{
					mwDelete ttt; //This object will not get deallocated in the destructor
					throw tString("Please specify a continuous series of domains so that the matrix function is defined everywhere");
				}
				textureTransformTypes.push_back(ttt);
	
				//Setup the matrix for the next transformation
				glLoadIdentity();
				if(inheritPreviousTransform)
					ttt->multGLMatrix(ttt->domainMax);
			}
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

	if(!textureArgumentType.loaded)
		throw tString("Please specify an argument for this piece-wise texture transform");
	if(textureTransformTypes.size()==0)
		throw tString("Please specify at least one texture tranform in this piece-wise texture transform");
}
/****************************************************************************/
void tReferenceFrameType::destroy()
{
    list<tComponentType *>::iterator ct;
	list<tVertexTransformType *>::iterator vtt;
	list<tTextureTransformType *>::iterator ttt;

    for(ct=componentTypes.begin();ct!=componentTypes.end();ct++)
        mwDelete (*ct);
	for(vtt=vertexTransformTypes.begin();vtt!=vertexTransformTypes.end();vtt++)
		mwDelete (*vtt);
	for(ttt=textureTransformTypes.begin();ttt!=textureTransformTypes.end();ttt++)
		mwDelete (*ttt);
	
    componentTypes.clear();
    modelVectorTypes.clear();
	vertexTransformTypes.clear();
	textureTransformTypes.clear();
}
/****************************************************************************/
void tReferenceFrameType::refresh()
{
    list<tComponentType *>::iterator ct;

    for(ct=componentTypes.begin();ct!=componentTypes.end();ct++)
        (*ct)->refresh();
}
/****************************************************************************/
void tReferenceFrameType::initializeMatrices()
{
    list<tReferenceFrameType *>::iterator rft;
    list<tComponentType *>::iterator ct;
    tVector3D v;
    int i;
    float r=0,r3D=0,z=-MAX_FLOAT;

    glPushMatrix();
    multGLVertexMatrix(NULL);
    glGetFloatv(GL_MODELVIEW_MATRIX,GLVertexMatrix);

    for(ct=componentTypes.begin();ct!=componentTypes.end();ct++)
        for(i=0;i<(*ct)->getVertexCount();i++)
        {
            v=multiplyMatrix3x4(GLVertexMatrix,(*ct)->getVertex(i));
            r=MAX(r,to2D(v).lengthSquared());
            r3D=MAX(r3D,v.lengthSquared());
            z=MAX(z,v.z);
        }

    if(maxRadius==-MAX_FLOAT)   maxRadius=sqrt(r);
    if(selRadius==-MAX_FLOAT)   selRadius=sqrt(r);
    if(radius3D==-MAX_FLOAT)    radius3D=sqrt(r3D);
    if(maxZ==-MAX_FLOAT)        maxZ=z;

    for(rft=referenceFrameTypes.begin();rft!=referenceFrameTypes.end();rft++)
        (*rft)->initializeMatrices();
    glPopMatrix();
}
/****************************************************************************/
void tReferenceFrameType::draw(int flags)
{
    if(initiallyVisible)
    {
		int matrixMode;
		list<tReferenceFrameType *>::iterator it;
		bool hasTextureTransform=(textureTransformTypes.size()>0);
        bool hasColorTransform=colorTransformType.loaded;
		if(hasTextureTransform)
		{
			glGetIntegerv(GL_MATRIX_MODE,&matrixMode);
			glMatrixMode(GL_TEXTURE);
			glPushMatrix();
			multGLTextureMatrix(NULL);
			glMatrixMode(matrixMode);
		}
		if(hasColorTransform)
		{
			glPushAttrib(GL_CURRENT_BIT);
			multGLColorMatrix(NULL,/*RGBMask=*/(flags&DRAW_SHADOW)==0);
		}

        glPushMatrix();
        glMultMatrixf(GLVertexMatrix);
		list<tComponentType *>::iterator ct;
		for(ct=componentTypes.begin();ct!=componentTypes.end();ct++)
			(*ct)->draw(polyCount,flags);
		glPopMatrix();

		if(hasColorTransform && !colorTransformAffectsChildren)
			glPopAttrib();
		if(hasTextureTransform && !textureTransformAffectsChildren)
		{
			glMatrixMode(GL_TEXTURE);
			glPopMatrix();
			glMatrixMode(matrixMode);
		}
		  
		for(it=referenceFrameTypes.begin();it!=referenceFrameTypes.end();it++)
			(*it)->draw(flags);
    
		if(hasColorTransform && colorTransformAffectsChildren)
			glPopAttrib();
		if(hasTextureTransform && textureTransformAffectsChildren)
		{
			glMatrixMode(GL_TEXTURE);
			glPopMatrix();
			glMatrixMode(matrixMode);
		}
	}
}
/****************************************************************************/
void tReferenceFrameType::multGLVertexMatrix(tModel *m)
{
	if(vertexTransformTypes.size()==0)
		return;

	float val=vertexArgumentType.evaluate(m);
	list<tVertexTransformType *>::iterator it;
	for(it=vertexTransformTypes.begin();it!=vertexTransformTypes.end();it++)
		if(val<=(*it)->domainMax)
		{
			(*it)->multGLMatrix(val);
			return;
		}

	//Checked for list empty above
	vertexTransformTypes.back()->multGLMatrix(val);
}
/****************************************************************************/
void tReferenceFrameType::multGLTextureMatrix(tModel *m)
{
	if(textureTransformTypes.size()==0)
		return;

	float val=textureArgumentType.evaluate(m);
	list<tTextureTransformType *>::iterator it;
	for(it=textureTransformTypes.begin();it!=textureTransformTypes.end();it++)
		if(val<=(*it)->domainMax)
		{
			(*it)->multGLMatrix(val);
			return;
		}

	//Checked for list empty above
	textureTransformTypes.back()->multGLMatrix(val);
}
/****************************************************************************/
void tReferenceFrameType::multGLColorMatrix(tModel *m, bool RGBMask)
{
	float val=colorArgumentType.evaluate(m);
    colorTransformType.multGLMatrix(val, RGBMask);
}
/****************************************************************************/
void tReferenceFrame::load() throw(int)
{
    list<tReferenceFrameType *>::iterator rft;
    tReferenceFrame *rf;
#ifdef DEBUG
    if(MEMORY_VIOLATION(type))
        throw -1;
    if(MEMORY_VIOLATION(owner))
        throw -1;
#endif

	for(int i=0;i<16;i++)
		GLVertexMatrix[i]=0;

    visible=type->initiallyVisible;
    if(type->parent)
        parent=owner->findReferenceFrame(type->parent);

    for(rft=type->referenceFrameTypes.begin();rft!=type->referenceFrameTypes.end();rft++)
    {
        rf=owner->findReferenceFrame(*rft);
        if(rf)
            referenceFrames.push_back(rf);
    }
}
/****************************************************************************/
void tReferenceFrame::updateMatrices()
{
    list<tReferenceFrame *>::iterator rf;

    glPushMatrix();
    type->multGLVertexMatrix(owner);
    glGetFloatv(GL_MODELVIEW_MATRIX,GLVertexMatrix);

    for(rf=referenceFrames.begin();rf!=referenceFrames.end();rf++)
        (*rf)->updateMatrices();
    glPopMatrix();
}
/****************************************************************************/
void tReferenceFrame::draw(int polyIndex, int flags)
{
    if(visible)
    {	
		int matrixMode;
		bool hasTextureTransform=(type->textureTransformTypes.size()>0);
        bool hasColorTransform=type->colorTransformType.loaded;
		list<tReferenceFrame *>::iterator it;

		if(hasTextureTransform)
		{
			glGetIntegerv(GL_MATRIX_MODE,&matrixMode);
			glMatrixMode(GL_TEXTURE);
			glPushMatrix();
			type->multGLTextureMatrix(owner);
			glMatrixMode(matrixMode);
		}
		if(hasColorTransform)
		{
			glPushAttrib(GL_CURRENT_BIT);
			type->multGLColorMatrix(owner, /*RGBMask=*/(flags&DRAW_SHADOW)==0);
		}

        glPushMatrix();
        glMultMatrixf(GLVertexMatrix);
		list<tComponentType *>::iterator ct;
		for(ct=type->componentTypes.begin();ct!=type->componentTypes.end();ct++)
			(*ct)->draw(polyIndex-polyBase,flags);
		glPopMatrix();

		if(hasColorTransform && !type->colorTransformAffectsChildren)
			glPopAttrib();
		if(hasTextureTransform && !type->textureTransformAffectsChildren)
		{
			glMatrixMode(GL_TEXTURE);
			glPopMatrix();
			glMatrixMode(matrixMode);
		}

		for(it=referenceFrames.begin();it!=referenceFrames.end();it++)
			(*it)->draw(polyIndex,flags);

		if(hasColorTransform && type->colorTransformAffectsChildren)
			glPopAttrib();
		if(hasTextureTransform && type->textureTransformAffectsChildren)
		{
			glMatrixMode(GL_TEXTURE);
			glPopMatrix();
			glMatrixMode(matrixMode);
		}
    }
}
/****************************************************************************/
void tReferenceFrame::updatePolyBase(int& index)
{
    list<tReferenceFrame *>::iterator rf;

    polyBase=index;
	if(visible)
		index+=type->polyCount;

    for(rf=referenceFrames.begin();rf!=referenceFrames.end();rf++)
        (*rf)->updatePolyBase(index);
}
/****************************************************************************/
void tReferenceFrame::getRepairPoints(int polyIndex, tVector3D& a, tVector3D& b)
{
    list<tComponentType *>::iterator ct;
    list<tReferenceFrame *>::iterator rf;
    tReferenceFrame *last=NULL;

    int i=polyIndex-polyBase;
    if(i>=0 && i<type->polyCount)
    {
        for(ct=type->componentTypes.begin();ct!=type->componentTypes.end();ct++)
            if((*ct)->getRepairPoints(i,a,b))
            {
                a=multiplyMatrix3x4(GLVertexMatrix,a);
                b=multiplyMatrix3x4(GLVertexMatrix,b);
                return;
            }
    }
    for(rf=referenceFrames.begin();rf!=referenceFrames.end();rf++)
    {
        if(polyIndex<(*rf)->getPolyBase())
            break;
        last=*rf;
    }
    if(last)
        last->getRepairPoints(polyIndex,a,b);
}
/****************************************************************************/
tVariableType *tModelType::variableTypeLookup(const tString& n, bool exact)
{
    list<tVariableType*>::iterator it;
    tString ln=STRTOLOWER(n);

    for(it=variableTypes.begin(); it!=variableTypes.end(); it++)
        if(STREQUAL((*it)->name,ln))
            return *it;
    if(exact)
        return NULL;
    for(it=variableTypes.begin(); it!=variableTypes.end(); it++)
        if(STRPREFIX(ln,(*it)->name))
            return *it;
    return NULL;
}
/****************************************************************************/
tFunctionType *tModelType::functionTypeLookup(const tString& n, bool exact)
{
    list<tFunctionType*>::iterator it;
    tString ln=STRTOLOWER(n);

    for(it=functionTypes.begin(); it!=functionTypes.end(); it++)
        if(STREQUAL((*it)->name,ln))
            return *it;
    if(exact)
        return NULL;
    for(it=functionTypes.begin(); it!=functionTypes.end(); it++)
        if(STRPREFIX(ln,(*it)->name))
            return *it;
    return NULL;
}
/****************************************************************************/
tProcedureType *tModelType::procedureTypeLookup(const tString& n, bool exact)
{
    list<tProcedureType*>::iterator it;
    tString ln=STRTOLOWER(n);

    for(it=procedureTypes.begin(); it!=procedureTypes.end(); it++)
        if(STREQUAL((*it)->name,ln))
            return *it;
    if(exact)
        return NULL;
    for(it=procedureTypes.begin(); it!=procedureTypes.end(); it++)
        if(STRPREFIX(ln,(*it)->name))
            return *it;
    return NULL;
}
/****************************************************************************/
tModelType::tModelType(tStream& s) throw(tString) : ancestor(NULL), nextVariableID(0),
    nextFunctionID(0), nextReferenceFrameID(0), initialMaxRadius(0), initialSelRadius(0),
    initialRadius3D(0), initialMaxZ(0), primaryComponentType(NULL), maxRadiusArg(0,BOUNDARY_MIN,0),
    selRadiusArg(0,BOUNDARY_MIN,0), maxZArg(0,BOUNDARY_MIN,0)
{
    list<tReferenceFrameType *>::iterator it1,it2;

	name=parseIdentifier("name",s);
	if(modelTypeLookup(name, /*exact=*/true)!=NULL)
		throw tString("A model type by the name of '")+name+"' already exists";
    
	try
    {
		if(s.beginBlock())
		{
			while(!s.endBlock())
			{
				tString str=parseString("tag",s);
				s.matchOptionalToken("=");
	
				if(strPrefix(str,"variable") ||
					strPrefix(str,"define_variable") || strPrefix(str,"defvariable"))
					parseVariableTypeBlock(s);
				else if(strPrefix(str,"function") ||
					strPrefix(str,"define_function") || strPrefix(str,"deffunction"))
					parseFunctionTypeBlock(s);
				else if(strPrefix(str,"procedure") ||
					strPrefix(str,"define_procedure") || strPrefix(str,"defprocedure"))
					parseProcedureTypeBlock(s);
				else if(strPrefix(str,"reference_frame"))
				{
					if(ancestor)
						throw tString("Each model type must have exactly one outer reference frame");
	
					ancestor=parseReferenceFrameTypeBlock(NULL,s);
					if(ancestor->componentTypes.size()>0)
						primaryComponentType=ancestor->componentTypes.front();
				}
				else if(strPrefix(str,"max_radius") || strPrefix(str,"maximum_radius"))
					maxRadiusArg.load(this,s);
				else if(strPrefix(str,"sel_radius") || strPrefix(str,"selection_radius"))
					selRadiusArg.load(this,s);
				else if(strPrefix(str,"max_z") || strPrefix(str,"maximum_z"))
					maxZArg.load(this,s);
				else if(strPrefix(str,"define_aim_procedure") || strPrefix(str,"aim_procedure"))
					parseAimProcedureTypeBlock(s);
				else
					throw tString("Unrecognized tag '")+str+"'";

				s.endStatement();
			}
		}

        //Check for duplicate reference frame type names:
        for(it1=referenceFrameTypes.begin();it1!=referenceFrameTypes.end();it1++)
            if(STRLEN((*it1)->name)>0)
                for(it2=it1,it2++;it2!=referenceFrameTypes.end();it2++)
                    if(STREQUAL((*it1)->name,(*it2)->name))
                        throw tString("Two reference frames cannot share the same name '")+(*it1)->name+"'";

        if(ancestor)
        {
            int matrixMode;
            glGetIntegerv(GL_MATRIX_MODE,&matrixMode);
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            glLoadIdentity();
            ancestor->initializeMatrices();
            glPopMatrix();
            glMatrixMode(matrixMode);
        }
    }
    catch(const tString& s)
    {
        destroy();
        throw s;
    }
}
/****************************************************************************/
tVariableType *tModelType::parseVariableTypeBlock(tStream& s)
{
    tVariableType *vt=mwNew tVariableType(nextVariableID,this,s);
    if(vt)
    {
        variableTypes.push_back(vt);
        nextVariableID++;
    }
    return vt;
}
/****************************************************************************/
tFunctionType *tModelType::parseFunctionTypeBlock(tStream& s)
{
    tFunctionType *ft=mwNew tFunctionType(nextFunctionID,this,s);
    if(ft)
    {
        functionTypes.push_back(ft);
        nextFunctionID++;
    }
    return ft;
}
/****************************************************************************/
tProcedureType* tModelType::parseProcedureTypeBlock(tStream& s)
{
    tProcedureType *pt=mwNew tProcedureType(this,s);
    if(pt)
        procedureTypes.push_back(pt);
    return pt;
}
/****************************************************************************/
tReferenceFrameType *tModelType::parseReferenceFrameTypeBlock(tReferenceFrameType *parent, tStream& s)
{
    tReferenceFrameType *rft=mwNew tReferenceFrameType(parent,this,s);
    if(rft)
    {
        if(rft->initiallyVisible)
        {
            initialMaxRadius=MAX(initialMaxRadius,rft->maxRadius);
            initialSelRadius=MAX(initialSelRadius,rft->selRadius);
            initialRadius3D=MAX(initialRadius3D,rft->radius3D);
            initialMaxZ=MAX(initialMaxZ,rft->maxZ);
        }

        rft->setID(nextReferenceFrameID++);
        referenceFrameTypes.push_back(rft);
    }
    return rft;
}
/****************************************************************************/
tModelVectorType *tModelType::parseModelVectorTypeBlock(tReferenceFrameType *parent, tStream& s)
{
    tModelVectorType *mvt=mwNew tModelVectorType(parent,this,s);
    if(mvt)
		modelVectorTypes.push_back(mvt);
    return mvt;
}
/****************************************************************************/
tAimProcedureType *tModelType::parseAimProcedureTypeBlock(tStream& s)
{
    tAimProcedureType *apt=mwNew tAimProcedureType(this,s);
    if(apt)
        aimProcedureTypes.push_back(apt);
    return apt;
}
/****************************************************************************/
void tModelType::destroy()
{
    list<tVariableType *>::iterator vt;
    list<tFunctionType *>::iterator ft;
    list<tProcedureType *>::iterator pt;
    list<tReferenceFrameType *>::iterator rft;
    list<tModelVectorType *>::iterator mvt;
    list<tAimProcedureType *>::iterator apt;

    for(vt=variableTypes.begin();vt!=variableTypes.end();vt++)
        mwDelete (*vt);
    for(ft=functionTypes.begin();ft!=functionTypes.end();ft++)
        mwDelete (*ft);
    for(pt=procedureTypes.begin();pt!=procedureTypes.end();pt++)
        mwDelete (*pt);
    for(rft=referenceFrameTypes.begin();rft!=referenceFrameTypes.end();rft++)
        mwDelete (*rft);
    //We don't delete ancestor here since it's an alias for a pointer in referenceFrameTypes
    for(mvt=modelVectorTypes.begin();mvt!=modelVectorTypes.end();mvt++)
        mwDelete (*mvt);
    for(apt=aimProcedureTypes.begin();apt!=aimProcedureTypes.end();apt++)
        mwDelete (*apt);

    variableTypes.clear();
    functionTypes.clear();
    procedureTypes.clear();
    referenceFrameTypes.clear();
    modelVectorTypes.clear();
    aimProcedureTypes.clear();
}
/****************************************************************************/
void tModelType::refresh()
{
    list<tReferenceFrameType*>::iterator it;
    for(it=referenceFrameTypes.begin();it!=referenceFrameTypes.end();it++)
        (*it)->refresh();
}
/****************************************************************************/
tVariableType *tModelType::parseVariableType(const char *fieldName, tStream& s)
{
    tVariableType *vt;
    tString str=parseString(fieldName, s);
    vt=variableTypeLookup(str,/*exact=*/false);
    if(vt==NULL)
        throw tString("Unrecognized variable '")+str+"'";
    return vt;
}
/****************************************************************************/
tFunctionType *tModelType::parseFunctionType(const char *fieldName, tStream& s)
{
    tFunctionType *ft;
    tString str=parseString(fieldName, s);
    ft=functionTypeLookup(str,/*exact=*/false);
    if(ft==NULL)
        throw tString("Unrecognized function '")+str+"'";
    return ft;
}
/****************************************************************************/
tReferenceFrameType *tModelType::parseReferenceFrameType(const char *fieldName, tStream& s)
{
    tReferenceFrameType *rft;
    tString str=parseString(fieldName, s);
    rft=referenceFrameTypeLookup(str,/*exact=*/false);
    if(rft==NULL)
        throw tString("Unrecognized reference frame '")+str+"'";
    return rft;
}
/****************************************************************************/
tProcedureType* tModelType::parseProcedureType(const char *fieldName, tStream& s)
{
    tProcedureType *pt;

    if(strPrefix(s.peekAhead(),"define_procedure") || strPrefix(s.peekAhead(),"defprocedure"))
    {
        parseString(fieldName, s);
        pt=parseProcedureTypeBlock(s);
    }
    else
    {
        tString identifier=parseIdentifier("procedure name",s);
        pt=procedureTypeLookup(identifier,/*exact=*/false);
        if(pt==NULL)
            throw tString("Unrecognized procedure '")+identifier+"'";
    }

    return pt;
}
/****************************************************************************/
tModelVectorType *tModelType::parseModelVectorType(const char *fieldName, tStream& s)
{
    tModelVectorType *mvt;
    tString str=parseString(fieldName, s);
    mvt=modelVectorTypeLookup(str,/*exact=*/false);
    if(mvt==NULL)
        throw tString("Unrecognized point or vector '")+str+"'";
    return mvt;
}
/****************************************************************************/
tAimProcedureType *tModelType::parseAimProcedureType(const char *fieldName, tStream& s)
{
    tAimProcedureType *apt;

    if(strPrefix(s.peekAhead(),"define_aim_procedure"))
    {
        parseString(fieldName, s);
        apt=parseAimProcedureTypeBlock(s);
    }
    else
    {
        tString identifier=parseIdentifier("aim procedure name",s);
        apt=aimProcedureTypeLookup(identifier,/*exact=*/false);
		if(apt==NULL)
			throw tString("Unrecognized aim procedure '")+identifier+"'";
    }

	return apt;
}
/****************************************************************************/
tModelVectorType *tModelType::modelVectorTypeLookup(const tString& n, bool exact)
{
    list<tModelVectorType *>::iterator it;
    tString ln=STRTOLOWER(n);

    for(it=modelVectorTypes.begin();it!=modelVectorTypes.end();it++)
        if(STREQUAL((*it)->name,ln))
            return *it;
    if(exact)
        return NULL;
    for(it=modelVectorTypes.begin();it!=modelVectorTypes.end();it++)
        if(STRPREFIX(ln,(*it)->name))
            return *it;

    return NULL;
}
/****************************************************************************/
tReferenceFrameType *tModelType::referenceFrameTypeLookup(const tString& n, bool exact)
{
    list<tReferenceFrameType*>::iterator it;
    tString ln=STRTOLOWER(n);

    for(it=referenceFrameTypes.begin(); it!=referenceFrameTypes.end(); it++)
        if(STREQUAL((*it)->name,ln))
            return *it;
    if(exact)
        return NULL;
    for(it=referenceFrameTypes.begin(); it!=referenceFrameTypes.end(); it++)
        if(STRPREFIX(ln,(*it)->name))
            return *it;

    return NULL;
}
/****************************************************************************/
tAimProcedureType *tModelType::aimProcedureTypeLookup(const tString& n, bool exact)
{
    list<tAimProcedureType *>::iterator it;
    tString ln=STRTOLOWER(n);

    for(it=aimProcedureTypes.begin(); it!=aimProcedureTypes.end(); it++)
        if(STREQUAL((*it)->name,ln))
            return *it;
    if(exact)
        return NULL;
    for(it=aimProcedureTypes.begin(); it!=aimProcedureTypes.end(); it++)
        if(STRPREFIX(ln,(*it)->name))
            return *it;

    return NULL;
}
/****************************************************************************/
void tModelType::draw(int flags)
{
	if(ancestor)
		ancestor->draw(flags);
}
/****************************************************************************/
float tModelType::getUnitVariable(eUnitVariable var)
{
    switch(var)
    {
        case UNITVAR_HEALTH:
            return 1;
        case UNITVAR_DISTANCE:
            return 0;
        case UNITVAR_SPEED:
            return 0;
    }
    return 0;
}
/****************************************************************************/
float tModelType::getMaxRadius()
{
    if(maxRadiusArg.loaded)
        return maxRadiusArg.initialValue;
    return initialMaxRadius;
}
/****************************************************************************/
float tModelType::getSelRadius()
{
    if(selRadiusArg.loaded)
        return selRadiusArg.initialValue;
    return initialSelRadius;
}
/****************************************************************************/
float tModelType::getMaxZ()
{
    if(maxZArg.loaded)
        return maxZArg.initialValue;
    return initialMaxZ;
}
/****************************************************************************/
tReferenceFrameType *tModelType::referenceFrameTypeLookup(tVariableType *vt, int& instances)
{
    list<tReferenceFrameType*>::iterator it;
    tReferenceFrameType* rf=NULL;
    instances=0;

    for(it=referenceFrameTypes.begin(); it!=referenceFrameTypes.end(); it++)
        if((*it)->vertexArgumentType.variableType==vt)
        {
            rf=*it;
            instances++;
        }

    return rf;
}
/****************************************************************************/
tModel::tModel(tModelType *_type, tUnit *_parent, tWorldTime t) throw(int) : type(_type),
    parent(_parent), polyCount(0)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(type))
        throw -1;
    if(MEMORY_VIOLATION(parent))
        throw -1;
#endif
    list<tVariableType *>::iterator vt;
    list<tFunctionType *>::iterator ft;
    list<tReferenceFrameType *>::iterator rft;
	vector<tReferenceFrame *>::iterator it;

    tVariable *v;
    tFunction *f;
    tReferenceFrame *rf;

    try
    {
        for(vt=type->variableTypes.begin();vt!=type->variableTypes.end();vt++)
        {
            v=mwNew tVariable(*vt,t);
            variables.push_back(v);
        }
        for(ft=type->functionTypes.begin();ft!=type->functionTypes.end();ft++)
        {
            f=mwNew tFunction(*ft,this);
            functions.push_back(f);
        }
        for(rft=type->referenceFrameTypes.begin();rft!=type->referenceFrameTypes.end();rft++)
        {
            rf=mwNew tReferenceFrame(*rft,this);
            referenceFrames.push_back(rf);
        }
        for(it=referenceFrames.begin();it!=referenceFrames.end();it++)
        {
            (*it)->load();
            if((*it)->isVisible())
                polyCount+=(*it)->getPolyCount();
        }

        ancestor=findReferenceFrame(type->ancestor);
		int base=0;
        if(ancestor)
            ancestor->updatePolyBase(base);
		updateMatrices();
        updateDimensions();
    }
    catch(int i)
    {
        destroy();
        throw i;
    }
}
/****************************************************************************/
void tModel::destroy()
{
    vector<tVariable *>::iterator v;
    vector<tFunction *>::iterator f;
    list<tProcedure *>::iterator p;
    vector<tReferenceFrame *>::iterator rf;

    for(v=variables.begin();v!=variables.end();v++)
        mwDelete (*v);
    for(f=functions.begin();f!=functions.end();f++)
        mwDelete (*f);
    for(p=procedures.begin();p!=procedures.end();p++)
        mwDelete (*p);
    for(rf=referenceFrames.begin();rf!=referenceFrames.end();rf++)
        mwDelete (*rf);

    variables.clear();
    functions.clear();
    procedures.clear();
    referenceFrames.clear();
}
/****************************************************************************/
void tModel::runProcedure(tProcedureType *pt, tWorldTime t)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(pt))
        return;
#endif

	if(pt->runExclusive)
	{
		list<tProcedure *>::iterator p;
		for(p=procedures.begin();p!=procedures.end(); )
		{
			if((*p)->getType()==pt)
			{
				mwDelete (*p);
				p=procedures.erase(p);
			}
			else
				p++;
		}
	}

    try
    {
        tProcedure *p=mwNew tProcedure(pt,this,t);
        if(p)
        {
            if(p->isFinished())
                mwDelete p;
            else
                procedures.push_back(p);
        }
    }
    catch(int i)
    {
        bug("tModel::runProcedure: procedure '%s' failed to start",CHARPTR(pt->name));
    }
}
/****************************************************************************/
tVariable *tModel::findVariable(tVariableType *vt)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(vt))
        return NULL;
    if(vt->ID<0 || vt->ID>=variables.size())
        return NULL;
#endif

    return variables[vt->ID];
}
/****************************************************************************/
tFunction *tModel::findFunction(tFunctionType *ft)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(ft))
        return NULL;
    if(ft->ID<0 || ft->ID>=functions.size())
        return NULL;
#endif

    return functions[ft->ID];
}
/****************************************************************************/
tReferenceFrame *tModel::findReferenceFrame(tReferenceFrameType *rft)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(rft))
        return NULL;
    if(rft->ID<0 || rft->ID>=referenceFrames.size())
    {
        bug("tModel::findReferenceFrame: invalid ID");
        return NULL;
    }
#endif

    return referenceFrames[rft->ID];
}
/****************************************************************************/
void tModel::draw(float progress, int flags)
{
    int polyIndex=(int)(progress*polyCount);
    clampRange(polyIndex,0,polyCount);

	if(ancestor)
		ancestor->draw(polyIndex,flags);
}
/****************************************************************************/
float tModel::getUnitVariable(eUnitVariable var)
{
    if(parent)
        switch(var)
        {
            case UNITVAR_HEALTH:
                return parent->getHP()/parent->getMaxHP();
            case UNITVAR_DISTANCE:
                return parent->getDistance();
            case UNITVAR_SPEED:
                return parent->getSpeed();
        }
    return 0;
}
/****************************************************************************/
tVector3D tModel::locatePoint(tModelVectorType *mvt)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(mvt))
        return zeroVector3D;
#endif
    tReferenceFrame *rf=findReferenceFrame(mvt->parent);
#ifdef DEBUG
    if(MEMORY_VIOLATION(rf))
        return zeroVector3D;
#endif

    return multiplyMatrix3x4(rf->getGLVertexMatrix(),mvt->v);
}
/****************************************************************************/
void tModel::locateReferenceFrame(tReferenceFrameType *rft,
                                  tVector3D& position,
                                  tVector3D& forward,
                                  tVector3D& side,
                                  tVector3D& up)
{
	tReferenceFrame *rf=findReferenceFrame(rft);
#ifdef DEBUG
	if(MEMORY_VIOLATION(rf))
        return;
#endif

    const float *mat=rf->getGLVertexMatrix();
    forward.x= mat[0];
    forward.y= mat[1];
    forward.z= mat[2];
    side.x=    mat[4];
    side.y=    mat[5];
    side.z=    mat[6];
    up.x=      mat[8];
    up.y=      mat[9];
    up.z=      mat[10];
    position.x=mat[12];
    position.y=mat[13];
    position.z=mat[14];
}
/****************************************************************************/
tVector3D tModel::locateUnitVector(tModelVectorType *mvt)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(mvt))
        return zeroVector3D;
#endif
    tReferenceFrame *rf=findReferenceFrame(mvt->parent);
#ifdef DEBUG
    if(MEMORY_VIOLATION(rf))
        return zeroVector3D;
#endif

    return multiplyMatrix3x3(rf->getGLVertexMatrix(),mvt->v).normalize();
}
/****************************************************************************/
void tModel::tick(tWorldTime t)
{
    vector<tVariable *>::iterator v;
    vector<tFunction *>::iterator f;
    list<tProcedure *>::iterator p;

    for(v=variables.begin();v!=variables.end();v++)
        (*v)->tick(t);
    for(f=functions.begin();f!=functions.end();f++)
        (*f)->tick(t);
    for(p=procedures.begin();p!=procedures.end();)
    {
        (*p)->tick(t);
        if((*p)->isFinished())
        {
            mwDelete (*p);
            p=procedures.erase(p);
        }
        else
            p++;
    }

	updateMatrices();
}
/****************************************************************************/
void tModel::updateMatrices()
{
	if(ancestor)
	{
		int matrixMode;
		glGetIntegerv(GL_MATRIX_MODE,&matrixMode);
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();
		ancestor->updateMatrices();
		glPopMatrix();
		glMatrixMode(matrixMode);
	}
}
/****************************************************************************/
void tModel::intervalTick(tWorldTime t)
{
	vector<tVariable *>::iterator v;
    for(v=variables.begin();v!=variables.end();v++)
        (*v)->intervalTick(t);
}
/****************************************************************************/
float tModel::getMaxRadius()
{
    if(type->maxRadiusArg.loaded)
        return type->maxRadiusArg.evaluate(this);
    return defaultMaxRadius;
}
/****************************************************************************/
float tModel::getSelRadius()
{
    if(type->selRadiusArg.loaded)
        return type->selRadiusArg.evaluate(this);
    return defaultSelRadius;
}
/****************************************************************************/
float tModel::getMaxZ()
{
    if(type->maxZArg.loaded)
        return type->maxZArg.evaluate(this);
    return defaultMaxZ;
}
/****************************************************************************/
void tModel::showReferenceFrame(tReferenceFrameType *rft, tWorldTime t)
{
    int base=0;
    tReferenceFrame *rf=findReferenceFrame(rft);
    if(rf && !rf->isVisible())
    {
        rf->onShow(t);
        polyCount+=rft->getPolyCount();
        if(ancestor)
            ancestor->updatePolyBase(base);
        updateDimensions();
    }
}
/****************************************************************************/
void tModel::hideReferenceFrame(tReferenceFrameType *rft, tWorldTime t)
{
    int base=0;
    tReferenceFrame *rf=findReferenceFrame(rft);
    if(rf && rf->isVisible())
    {
        rf->onHide(t);
        polyCount-=rft->getPolyCount();
        if(ancestor)
            ancestor->updatePolyBase(base);
        updateDimensions();
    }
}
/****************************************************************************/
void tModel::updateDimensions()
{
    vector<tReferenceFrame *>::iterator rf;
    tReferenceFrameType *rft;

    defaultMaxRadius=0;
    defaultSelRadius=0;
    defaultRadius3D=0;
    defaultMaxZ=0;
    for(rf=referenceFrames.begin();rf!=referenceFrames.end();rf++)
        if((*rf)->isVisible())
        {
            rft=(*rf)->getType();
            defaultMaxRadius=MAX(defaultMaxRadius,rft->maxRadius);
            defaultSelRadius=MAX(defaultSelRadius,rft->selRadius);
            defaultRadius3D=MAX(defaultRadius3D,rft->radius3D);
            defaultMaxZ=MAX(defaultMaxZ,rft->maxZ);
        }
}
/****************************************************************************/
void tModel::getRepairPoints(float progress, tVector3D& a, tVector3D& b)
{
    int polyIndex=(int)(progress*polyCount);
    clampRange(polyIndex,0,polyCount-1);

    if(ancestor)
        ancestor->getRepairPoints(polyIndex,a,b);
}
/****************************************************************************/
void parseModelTypeBlock(tStream& s) throw(tString)
{
    tModelType *mt = mwNew tModelType(s);
    if(mt)
        modelTypes.push_back(mt);
}
/****************************************************************************/
//refresh model type database after user toggles full-screen
//Try to refresh each model type.  If a model type fails to refresh, log an error message
//and proceed to the next model type.
void refreshModelTypes()
{
    for(int i=0; i<modelTypes.size(); i++)
        modelTypes[i]->refresh();
}
/****************************************************************************/
//destroy model type database
void destroyModelTypes()
{
    for(int i=0; i<modelTypes.size(); i++)
        mwDelete modelTypes[i];
    modelTypes.clear();
}

/****************************************************************************/
//look-up model type name (NULL if not found)
tModelType *modelTypeLookup(const tString& name, bool exact)
{
    int i;
    tString n=STRTOLOWER(name);
    for(i=0; i<modelTypes.size(); i++)
        if(STREQUAL(modelTypes[i]->name,n))
            return modelTypes[i];
    if(exact)
        return NULL;
    for(i=0; i<modelTypes.size(); i++)
        if(STRPREFIX(n,modelTypes[i]->name))
            return modelTypes[i];
    return NULL;
}
/****************************************************************************/
tModelType *parseModelType(const char *fieldName, tStream& s) throw(tString)
{
    tModelType *m;
    tString str=parseString(fieldName, s);
    m=modelTypeLookup(str,/*exact=*/false);
    if(m==NULL)
        throw tString("Unrecognized model type '")+str+"'";
    return m;
}
/****************************************************************************/
eInstructionAction parseInstructionAction(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName, s);
    if(strPrefix(str,"wait"))
        return INSTRUCTION_WAIT;
    if(strPrefix(str,"assign"))
        return INSTRUCTION_ASSIGN;
    if(strPrefix(str,"add"))
        return INSTRUCTION_ADD;
    if(strPrefix(str,"subtract"))
        return INSTRUCTION_SUBTRACT;
    if(strPrefix(str,"activate"))
        return INSTRUCTION_ACTIVATE;
    if(strPrefix(str,"deactivate"))
        return INSTRUCTION_DEACTIVATE;
    if(strPrefix(str,"show"))
        return INSTRUCTION_SHOW;
    if(strPrefix(str,"hide"))
        return INSTRUCTION_HIDE;

    throw tString("Unrecognized instruction action '")+str+"'";
}
/****************************************************************************/
eVariableAttribute variableAttributeLookup(const tString& str, bool exact)
{
    tString ln=STRTOLOWER(str);
    if(exact)
    {
        if(ln=="position")
            return VARIABLE_POSITION;
        if(ln=="target_position")
            return VARIABLE_TARGET_POSITION;
        if(ln=="rate" || ln=="velocity")
            return VARIABLE_VELOCITY;
        if(ln=="target_rate" || ln=="target_velocity")
            return VARIABLE_TARGET_VELOCITY;
        if(ln=="acceleration")
            return VARIABLE_ACCELERATION;
    }
    else
    {
        if(strPrefix(ln,"position"))
            return VARIABLE_POSITION;
        if(strPrefix(ln,"target_position"))
            return VARIABLE_TARGET_POSITION;
        if(strPrefix(ln,"rate")||strPrefix(ln,"velocity"))
            return VARIABLE_VELOCITY;
        if(strPrefix(ln,"target_rate")||strPrefix(ln,"target_velocity"))
            return VARIABLE_TARGET_VELOCITY;
        if(strPrefix(ln,"acceleration"))
            return VARIABLE_ACCELERATION;
    }

    return VARIABLE_NONE;
}
/****************************************************************************/
eVariableAttribute parseVariableAttribute(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName, s);
    if(strPrefix(str,"position"))
        return VARIABLE_POSITION;
    if(strPrefix(str,"target_position"))
        return VARIABLE_TARGET_POSITION;
    if(strPrefix(str,"rate")||strPrefix(str,"velocity"))
        return VARIABLE_VELOCITY;
    if(strPrefix(str,"target_rate")||strPrefix(str,"target_velocity"))
        return VARIABLE_TARGET_VELOCITY;
    if(strPrefix(str,"acceleration"))
        return VARIABLE_ACCELERATION;

    throw tString("Unrecognized variable attribute '")+str+"'";
}
/****************************************************************************/
eTransformKind parseTransformKind(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName, s);
    if(strPrefix(str,"translate"))
        return TRANSFORM_TRANSLATE;
    if(strPrefix(str,"rotate"))
        return TRANSFORM_ROTATE;
    if(strPrefix(str,"scale"))
        return TRANSFORM_SCALE;

    throw tString("Unrecognized transform kind '")+str+"'";
}
/****************************************************************************/
eUnitVariable unitVariableLookup(const tString& name, bool exact)
{
    tString ln=STRTOLOWER(name);
    if(exact)
    {
        if(ln=="unit_health")
            return UNITVAR_HEALTH;
        if(ln=="unit_distance")
            return UNITVAR_DISTANCE;
        if(ln=="unit_speed" || ln=="unit_velocity")
            return UNITVAR_SPEED;
    }
    else
    {
        if(strPrefix(ln,"unit_health"))
            return UNITVAR_HEALTH;
        if(strPrefix(ln,"unit_distance"))
            return UNITVAR_DISTANCE;
        if(strPrefix(ln,"unit_speed") || strPrefix(ln,"unit_velocity"))
            return UNITVAR_SPEED;
    }

    return UNITVAR_NONE;
}
/****************************************************************************/
eUnitVariable parseUnitVariable(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName, s);
    eUnitVariable uv=unitVariableLookup(str,/*exact=*/false);

    if(uv==UNITVAR_NONE)
        throw tString("Unrecognized unit variable '")+str+"'";
    return uv;
}
/****************************************************************************/
eColorComponent parseColorComponent(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName, s);
    if(strPrefix(str,"red"))
        return COLOR_RED;
    if(strPrefix(str,"green"))
        return COLOR_GREEN;
    if(strPrefix(str,"blue"))
        return COLOR_BLUE;
    if(strPrefix(str,"alpha"))
        return COLOR_ALPHA;
    if(strPrefix(str,"rgb"))
        return COLOR_RGB;
    if(strPrefix(str,"rgba"))
        return COLOR_RGBA;

    throw tString("Unrecognized color component '")+str+"'";
}
/****************************************************************************/
void validateModelType(tModelType *mt) throw(tString)
{
    if(mt==NULL)
        throw tString("Please specify the model type before event handlers and functions");
}
/****************************************************************************/
