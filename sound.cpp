/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* SOUND.CPP:    Sound manager that grants requests to play sounds according *
*               to a quota.                                                 *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
#include <AL/al.h>
#include <AL/alut.h>
/****************************************************************************/
#include "sound.h"
#include "memwatch.h"
#pragma package(smart_init)

//The OpenAL implementation on Linux is slightly different than that of Windows
#ifdef LINUX_OPENAL_SUSE
static ALboolean alutLoadWAVFile(ALbyte *fname,	ALsizei *format, ALvoid **wave,
    ALsizei *size, ALsizei *freq, ALboolean *loop)
{
	ALsizei bits=0; /* unused */
	return alutLoadWAV((const char*)fname, wave, format, size, &bits, freq);
}

static ALboolean alutUnloadWAV(ALenum format, ALvoid *wave, ALsizei size,
    ALsizei freq)
{
	free(wave);
	return AL_TRUE;
}
#endif

//Static variables:
static int maxSources=0;
static vector<tSoundType *> soundTypes;

//Static functions:
static void countMaxSources();

tSoundType::tSoundType(tStream& s) throw(tString) :
    loop(false), buffer(0), quota(0), instances(0), lastTime(0)
{
    name=parseString("name", s);
    name=STRTOLOWER(name);
    filename=parseString("filename", s);
    loop=parseBool("loop", s);
}

void countMaxSources()
{
    //Determine how many sound sources we can allocate up to 1000:
    ALenum error;
    iSoundSource soundSources[1000];
    int i;

    alGetError(); //clear error

    for(i=0; i<1000; i++)
    {
        alGenSources(1, &soundSources[i]);
        error=alGetError();
        if(error!=AL_NO_ERROR)
            break;
    }
    maxSources=i;
    log("This system supports %d sound sources",maxSources);
    for(i=0;i<maxSources;i++)
        alDeleteSources(1,&soundSources[i]);
}

void soundStartup()
{
    //Assign default values to listener position, velocity, and orientation.
    //These values will change as the user pans the view.
    float listenerPos[]={0.0,0.0,0.0};
    float listenerVel[]={0.0,0.0,0.0};
    float listenerOri[]={0.0,0.0,-1.0, 0.0,1.0,0.0}; //forward vector, up vector

	alListenerfv(AL_POSITION,listenerPos);
	alListenerfv(AL_VELOCITY,listenerVel);
	alListenerfv(AL_ORIENTATION,listenerOri);

    countMaxSources();
}

void soundShutdown()
{
}

void sndPositionListener(const tVector3D& p)
{
    alListenerfv(AL_POSITION,(float *)&p);
}

tSoundType *soundTypeLookup(const tString& name,bool exact)
{
    int i;
    tString n=STRTOLOWER(name);
    for(i=0;i<soundTypes.size();i++)
        if(STREQUAL(soundTypes[i]->name,n))
            return soundTypes[i];
    if(exact)
        return NULL;
    for(i=0;i<soundTypes.size();i++)
        if(STRPREFIX(n,soundTypes[i]->name))
            return soundTypes[i];
    return NULL;
}

iSoundSource sndNewSource(tSoundType *type, const tVector3D& p, tWorldTime t)
{
    iSoundSource soundSource;
    ALenum error;
#ifdef DEBUG
    if(MEMORY_VIOLATION(type))
        return 0;
    //These lines added to trap NaN causing an OpenAL assertion failure
    if(p.x!=p.x || p.y!=p.y || p.z!=p.z)
    {
        bug("sndNewSource: invalid position");
        return 0;
    }
#endif

    if(type->instances==type->quota) //not enough sound sources!
        return 0;
    if(t==type->lastTime)
        return 0;
    type->lastTime=t;

    alGetError(); //clear error
    alGenSources(1, &soundSource);
    error=alGetError();
    if(error != AL_NO_ERROR)
    {
        bug("sndNewSource: Error creating sound source with error code %d",error);
        return 0;
    }

    alSourcefv(soundSource,AL_POSITION,(float *)&p);
    alSourcei(soundSource,AL_BUFFER,type->buffer);
    alSourcef(soundSource, AL_REFERENCE_DISTANCE, 3);
    alSourcePlay(soundSource);

    type->instances++;

    return soundSource;
}

void sndDeleteSource(iSoundSource index)
{
    int i;
    iSoundBuffer buf;
#ifdef DEBUG
    if(!alIsSource(index))
    {
        bug("sndDeleteSource: invalid source");
        return;
    }
#endif
    alGetSourcei(index, AL_BUFFER, (int *) &buf);
    for(i=0;i<soundTypes.size();i++)
        if(soundTypes[i]->buffer==buf)
            break;
    if(i<soundTypes.size())
        soundTypes[i]->instances--;
    else
        bug("sndDeleteSource: buffer not found");
    alSourceStop(index);
    alDeleteSources(1,&index);
}

void sndPositionSource(iSoundSource index, const tVector3D& p)
{
#ifdef DEBUG
    if(!alIsSource(index))
    {
        bug("sndPositionSource: invalid source");
        return;
    }
    //These lines added to trap NaN causing an OpenAL assertion failure
    if(p.x!=p.x || p.y!=p.y || p.z!=p.z)
    {
        bug("sndPositionSource: invalid position");
        return;
    }
#endif
    alSourcefv(index,AL_POSITION,(float *)&p);
}

bool sndIsFinished(iSoundSource index)
{
    int state;
#ifdef DEBUG
    if(!alIsSource(index))
    {
        bug("sndIsFinished: invalid source");
        return false;
    }
#endif
    alGetSourcei(index,AL_SOURCE_STATE,&state);
    return state==AL_STOPPED;
}

bool loadSoundTypes(const char *filename)
{
    ALboolean ALLoop;
    ALsizei size,freq;
    ALenum format;
    ALvoid *data=NULL;
    ALenum error;
    iSoundBuffer *soundBuffers=NULL;
    tString soundFile,errorMessage;
    tSoundType *st;

    int i;

    //Destroy any existing sound types:
    destroySoundTypes();

    FILE *fp=fopen(filename, "r");
    tStream stream;

    if(fp==NULL)
    {
        bug("loadSoundTypes: failed to open '%s'", filename);
        return false;
    }

    try
    {
        stream.load(fp);
        while(!stream.atEndOfStream())
        {
            st=mwNew tSoundType(stream);
            soundTypes.push_back(st);
        }
    }
    catch(const tString& message)
    {
        errorMessage=stream.parseErrorMessage(filename,message);
        bug(CHARPTR(errorMessage));
        fclose(fp);
        return false;
    }

    fclose(fp);

    soundBuffers = mwNew iSoundBuffer[soundTypes.size()];

    alGetError(); //clear error

	alGenBuffers(soundTypes.size(), soundBuffers);
    error=alGetError();
	if(error != AL_NO_ERROR)
    {
        bug("soundStartup: failed to create OpenAL buffers with error code %d",error);
        mwDelete[] soundBuffers;
        return false;
    }

    for(i=0;i<soundTypes.size();i++)
        soundTypes[i]->buffer=soundBuffers[i];
    mwDelete[] soundBuffers;
    soundBuffers=NULL;

    for(i=0;i<soundTypes.size();i++)
    {
        soundFile=SOUND_PATH+soundTypes[i]->filename;
        ALLoop=(ALboolean)soundTypes[i]->loop;
        alutLoadWAVFile((ALbyte*) CHARPTR(soundFile),&format,&data,&size,&freq,&ALLoop);

        if(data==NULL)
        {
            bug("soundStartup: failed to load '%s'", CHARPTR(soundFile));
            return false;
        }
	    alBufferData(soundTypes[i]->buffer,format,data,size,freq);
	    alutUnloadWAV(format,data,size,freq);
    }

    for(i=0;i<soundTypes.size();i++)
        soundTypes[i]->quota=maxSources/soundTypes.size(); //round down

    return true;
}

void destroySoundTypes()
{
    for(int i=0;i<soundTypes.size();i++)
    {
        if(soundTypes[i]->buffer>0)
            alDeleteBuffers(1, &soundTypes[i]->buffer);
        mwDelete soundTypes[i];
    }
    soundTypes.clear();
}
tSoundType *parseSoundType(const char *fieldName, tStream& s) throw(tString)
{
    tSoundType *st;
    tString str=parseString(fieldName, s);
    st=soundTypeLookup(str,/*exact=*/false);
    if(st==NULL)
        throw tString("Unrecognized sound type '")+str+"'";
    return st;
}

