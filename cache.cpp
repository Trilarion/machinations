/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* CACHE.CPP:   General-purpose functions for caching data.                  *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#include "memwatch.h"
#include "types.h"
#include "cache.h"

#pragma package(smart_init)
/****************************************************************************/
#ifndef CACHE_HEADERS
    #ifdef LINUX
        #include <sys/stat.h> //for mkdir
    #else
        #ifdef BORLAND
            #include <dir.h> //for mkdir
        #else
            #include <direct.h> //for mkdir
        #endif
    #endif
#endif

tCache::tCache()
{
    headerLength=0;
    headerCapacity=100;
    header=mwNew char[headerCapacity];
}
tCache::~tCache()
{
    if(header)
        mwDelete[] header;
}
tCache& tCache::operator=(const tCache& c)
{
    if(header)
        mwDelete[] header;

    headerLength=c.headerLength;
    headerCapacity=c.headerCapacity;
    header=mwNew char[headerCapacity];
    memcpy(header,c.header,headerLength);
    
    return *this;
}

void tCache::addDataToHeader(const char *data, int length)
{
    while(headerLength+length>headerCapacity)
    {
        headerCapacity*=2;
        char *newHeader=mwNew char[headerCapacity];
        memcpy(newHeader,header,headerLength);
        mwDelete[] header;
        header=newHeader;
    }

    memcpy(header+headerLength,data,length);
    headerLength+=length;
};

FILE *tCache::readFromCache(const char *filename)
{
    long l;
    int fileHeaderLength;
    FILE *fp=fopen(filename,"rb");
    if(fp==NULL)
        return NULL;

    if(fread(&l,sizeof(l),1,fp)<1)
    {
        fclose(fp);
        return NULL;
    }
    fileHeaderLength=(int)l;
    if(fileHeaderLength!=headerLength)
    {
        fclose(fp);
        return NULL;
    }

    char *fileHeader = mwNew char[headerLength];

    if(fread(fileHeader,headerLength,1,fp)<1 ||
        memcmp(header,fileHeader,headerLength)!=0)
    {
        fclose(fp);
        fp=NULL;
    }
    mwDelete[] fileHeader;
    return fp;
}

FILE *tCache::writeToCache(const char *filename)
{
    long l;

    my_mkdir(CACHE_PATH);
    FILE *fp=fopen(filename,"wb");
    if(fp==NULL)
    {
        bug("tCache::writeToCache: failed to open '%s' for writing",filename);
        return NULL;
    }
    l=(long)headerLength;
    if(fwrite(&l,sizeof(l),1,fp)<1 ||
        fwrite(header,headerLength,1,fp)<1)
    {
        bug("tCache::writeToCache: failed to write data to '%s'",filename);
        fclose(fp);
        return NULL;
    }

    return fp;
}

