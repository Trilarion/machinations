/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* VECTOR.CPP    Mathematical vector operations                              *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#include "vec.h"
#include "types.h"
#include "memwatch.h"


/****************************************************************************
                             Static Variables
 ****************************************************************************/
static float *sinTable=NULL;
static float *cosTable=NULL;
static float *asinTable=NULL;
static float *acosTable=NULL;
static float splineTables[SPLINE_TABLE_SIZE+1][4];
static float splineDerivTables[SPLINE_TABLE_SIZE+1][4];

/****************************************************************************
                                Constants
 ****************************************************************************/
const tVector3D iHat3D={1,0,0};
const tVector3D jHat3D={0,1,0};
const tVector3D kHat3D={0,0,1};
const tVector3D zeroVector3D={0,0,0};

const tVector2D iHat2D={1,0};
const tVector2D jHat2D={0,1};
const tVector2D zeroVector2D={0,0};



//Conversion functions:
tVector2D to2D(const tVector3D& v)
{
    tVector2D a={v.x,v.y};
    return a;
}
tVector3D to3D(const tVector2D& v)
{
    tVector3D a={v.x,v.y,0};
    return a;
}

/***************************************************************************\
angleCompare

Determines the orientation of two vectors.  Suppose two vectors, v1 and v2,
are centered at the origin.  If you draw the minor arc between them, three
cases will ensue.  (1) The vectors point in the same direction. (2) v1 is
clockwise of v2 around the arc. (3) v1 is counter-clockwise of v2 around the
arc.

Inputs:
    x1,y1   The x and y components of vector v1
    x2,y2   The x and y components of vector v2
Outputs:
    Returns 0 if the vectors are coincidental
    Returns 1 if v1 is clockwise of v2
    Return -1 if v1 is counter-clockwise of v2
\***************************************************************************/
int angleCompare(int x1, int y1, int x2, int y2)
{
    float q1 = 0;
    float q2 = 0;
    if(x1 != 0)
        q1 = (float) y1 / x1;
    if(x2 != 0)
        q2 = (float) y2 / x2;
    if(x1 > 0 && x2 > 0 || x1 < 0 && x2 < 0)
    {
        if(q2 > q1)
            return 1;
        else if(q2 == q1)
            return 0;
        else
            return -1;
    }
    if(x1 > 0 && x2 < 0 || x1 < 0 && x2 > 0)
    {
        if(q2 < q1)
            return 1;
        else if(q2 == q1)
            return 0;
        else
            return -1;
    }
    if(x1 == 0 && y1 > 0)
    {
        if(x2 < 0)
            return 1;
        else if (x2 == 0 && y2 > 0)
            return 0;
        else
            return -1;
    }
    if(x1 == 0 && y1 < 0)
    {
        if(x2 > 0)
            return 1;
        else if (x2 == 0 && y2 < 0)
            return 0;
        else
            return -1;
    }
    if(x1 > 0)
    {
        if(y2 > 0)
            return 1;
        else
            return -1;
    }
    if(y2 < 0)
        return 1;
    else
        return -1;
}

/***************************************************************************\
reduceAngle

Reduces an angle by adding or subtracting multiples of 2Pi.

Inputs:
    angle   Original angle (radians)
Outputs:
    Returns an equivalent angle which fits in the range [0,2Pi)
\***************************************************************************/
float reduceAngle(float angle)
{
    angle=fmod(angle,M_PI*2);
    if(angle<0)
        angle+=M_PI*2;
    return angle;
}

/***************************************************************************\
tVector2D::angle

Converts a pair of rectangular coordinates to polar coordinates and discards
the radius.  Mathematically, find 'a' such that:
    x = c*cos(a) and y = c*sin(a) where c > 0

Inputs:
    none
Output:
    Angle in radians
\***************************************************************************/
float tVector2D::angle()
{
    float angle;
    if(x<0)
        angle = atan( y/x ) + M_PI;
    else if(x==0)
    {
        if(y>0)
            angle=M_PI/2;
        else
            angle=M_PI*3/2;
    }
    else
        angle = atan( y/x );
    if(angle<0)
        angle+=M_PI*2;
    return angle;
}

bool createTrigTables()
{
    int i;
    float u,u2,u3;

    sinTable=mwNew float[SIN_TABLE_SIZE];
    if(sinTable)
        for(i=0;i<SIN_TABLE_SIZE;i++)
            sinTable[i]=sin((float)i/SIN_SCALE);

    cosTable=mwNew float[COS_TABLE_SIZE];
    if(cosTable)
        for(i=0;i<COS_TABLE_SIZE;i++)
            cosTable[i]=cos((float)i/COS_SCALE);

    asinTable=mwNew float[ASIN_TABLE_SIZE];
    if(asinTable)
        for(i=0;i<ASIN_TABLE_SIZE;i++)
            asinTable[i]=asin(((float)i-ASIN_BIAS)/ASIN_SCALE);

    acosTable=mwNew float[ACOS_TABLE_SIZE];
    if(acosTable)
        for(i=0;i<ACOS_TABLE_SIZE;i++)
            acosTable[i]=acos(((float)i-ACOS_BIAS)/ACOS_SCALE);

    for(i=0;i<=SPLINE_TABLE_SIZE;i++)
    {
        u=(float)i/SPLINE_TABLE_SIZE;
        u2=u*u;
        u3=u2*u;
        splineTables[i][0]=(-u3 + 3*u2 - 3*u + 1)/6;
        splineTables[i][1]=(3*u3 - 6*u2 + 4)/6;
        splineTables[i][2]=(-3*u3 + 3*u2 + 3*u + 1)/6;
        splineTables[i][3]=u3/6;

        splineDerivTables[i][0]=(-3*u2 + 6*u - 3)/6;
        splineDerivTables[i][1]=(9*u2 - 12*u)/6;
        splineDerivTables[i][2]=(-9*u2 + 6*u + 3)/6;
        splineDerivTables[i][3]=3*u2/6;
    }

    if(sinTable==NULL || cosTable==NULL ||
        asinTable==NULL || acosTable==NULL)
    {
        destroyTrigTables();
        return false;
    }

    return true;
}

void destroyTrigTables()
{
    if(sinTable)
    {
        mwDelete[] sinTable;
        sinTable=NULL;
    }
    if(cosTable)
    {
        mwDelete[] cosTable;
        cosTable=NULL;
    }
    if(asinTable)
    {
        mwDelete[] asinTable;
        asinTable=NULL;
    }
    if(acosTable)
    {
        mwDelete[] acosTable;
        acosTable=NULL;
    }
}

float lookupSin(float angle)
{
#ifdef DEBUG
    if(sinTable==NULL)
        return sin(angle);
#endif
    return sinTable[(int)(angle*SIN_SCALE) & SIN_MASK];
}

float lookupCos(float angle)
{
#ifdef DEBUG
    if(cosTable==NULL)
        return cos(angle);
#endif
    return cosTable[(int)(angle*COS_SCALE) & COS_MASK];
}

float lookupAsin(float val)
{
#ifdef DEBUG
    if(asinTable==NULL)
        return asin(val);
#endif
    int index=(int)(val*ASIN_SCALE)+ASIN_BIAS;
    if(index<0)
        index=0;
    if(index>=ASIN_TABLE_SIZE)
        index=ASIN_TABLE_SIZE-1;
    return asinTable[index];
}

float lookupAcos(float val)
{
#ifdef DEBUG
    if(acosTable==NULL)
        return acos(val);
#endif
    int index=(int)(val*ACOS_SCALE)+ACOS_BIAS;
    if(index<0)
        index=0;
    if(index>=ACOS_TABLE_SIZE)
        index=ACOS_TABLE_SIZE-1;
    return acosTable[index];
}

float lookupSpline(float val, int index)
{
#ifdef DEBUG
    if(index<0 || index>=4)
    {
        bug("lookupSpline: invalid index");
        return 0;
    }
#endif
    int vi=(int)(val*SPLINE_TABLE_SIZE);
    clampRange(vi,0,SPLINE_TABLE_SIZE);
    return splineTables[vi][index];
}
float* lookupSplineArray(float val)
{
    int vi=(int)(val*SPLINE_TABLE_SIZE);
    clampRange(vi,0,SPLINE_TABLE_SIZE);
    return &splineTables[vi][0];
}

float lookupSplineDeriv(float val, int index)
{
#ifdef DEBUG
    if(index<0 || index>=4)
    {
        bug("lookupSpline: invalid index");
        return 0;
    }
#endif
    int vi=(int)(val*SPLINE_TABLE_SIZE);
    clampRange(vi,0,SPLINE_TABLE_SIZE);
    return splineDerivTables[vi][index];
}
float* lookupSplineDerivArray(float val)
{
    int vi=(int)(val*SPLINE_TABLE_SIZE);
    clampRange(vi,0,SPLINE_TABLE_SIZE);
    return &splineDerivTables[vi][0];
}

// Does a "ceiling" or "floor" operation on the angle by which a given vector
// deviates from a given reference basis vector.  Consider a cone with "basis"
// as its axis and slope of "cosineOfConeAngle".  The first argument controls
// whether the "source" vector is forced to remain inside or outside of this
// cone.  Called by vecLimitMaxDeviationAngle and vecLimitMinDeviationAngle.
// [by Craig Reynolds]
template<class A>
A vecLimitDeviationAngleUtility (const bool insideOrOutside,
                                 const A& source,
                                 const float cosineOfConeAngle,
                                 const A& basis)
{
    // immediately return zero length input vectors
    float sourceLength = source.length();
    if (sourceLength == 0)
        return source;

    // measure the angular deviation of "source" from "basis"
    const A direction = source / sourceLength;
    float cosineOfSourceAngle = direction.dot(basis);

    // Simply return "source" if it already meets the angle criteria.
    // (note: we hope this top "if" gets compiled out since the flag
    // is a constant when the function is inlined into its caller)
    if (insideOrOutside)
    {
	// source vector is already inside the cone, just return it
	if (cosineOfSourceAngle >= cosineOfConeAngle) return source;
    }
    else
    {
	// source vector is already outside the cone, just return it
	if (cosineOfSourceAngle <= cosineOfConeAngle) return source;
    }

    // find the portion of "source" that is perpendicular to "basis"
    const A perp = source.perpendicularComponent(basis);

    // normalize that perpendicular
    const A unitPerp = perp.normalize();

    // construct a new vector whose length equals the source vector,
    // and lies on the intersection of a plane (formed the source and
    // basis vectors) and a cone (whose axis is "basis" and whose
    // angle corresponds to cosineOfConeAngle)
    float perpDist = sqrt(1 - (cosineOfConeAngle * cosineOfConeAngle));
    const A c0 = basis * cosineOfConeAngle;
    const A c1 = unitPerp * perpDist;
    return (c0 + c1) * sourceLength;
}

tVector3D multiplyMatrix4x4(const float mat[16], const tVector3D& v)
{
    float x,y,z,c;
    x=mat[0]*v.x + mat[4]*v.y + mat[8]*v.z + mat[12];
    y=mat[1]*v.x + mat[5]*v.y + mat[9]*v.z + mat[13];
    z=mat[2]*v.x + mat[6]*v.y + mat[10]*v.z + mat[14];
    c=mat[3]*v.x + mat[7]*v.y + mat[11]*v.z + mat[15];
    if(c==0)
    {
        bug("multiplyMatrix4x4: c is zero");
        return zeroVector3D;
    }
    tVector3D out={x/c,y/c,z/c};
    return out;
}

tVector3D multiplyMatrix3x4(const float mat[16], const tVector3D& v)
{
    tVector3D out;
    out.x=mat[0]*v.x + mat[4]*v.y + mat[8]*v.z + mat[12];
    out.y=mat[1]*v.x + mat[5]*v.y + mat[9]*v.z + mat[13];
    out.z=mat[2]*v.x + mat[6]*v.y + mat[10]*v.z + mat[14];
    return out;
}
tVector3D multiplyMatrix3x3(const float mat[16], const tVector3D& v)
{
    tVector3D out;
    out.x=mat[0]*v.x + mat[4]*v.y + mat[8]*v.z;
    out.y=mat[1]*v.x + mat[5]*v.y + mat[9]*v.z;
    out.z=mat[2]*v.x + mat[6]*v.y + mat[10]*v.z;
    return out;
}
tVector2D multiplyMatrix2x3(const float mat[16], const tVector2D& v)
{
    tVector2D out;
    out.x=mat[0]*v.x + mat[4]*v.y + mat[12];
    out.y=mat[1]*v.x + mat[5]*v.y + mat[13];
    return out;
}
float calcAngleBetween(const tVector3D& a,const tVector3D& b,const tVector3D& axis,
    float& sinAngle) //axis must be normalized
{
    sinAngle=(a.normalize()).cross(b.normalize()).dot(axis);
    clampRange(sinAngle,-1.0f,1.0f);
    float angle=asin(sinAngle);
    if(a.dot(b)<0) //a and b need not be normalized since it won't affect the inequality
    {
        if(angle>0)
            return M_PI-angle;
        else
            return -M_PI-angle;
    }
    return angle;
}

bool isIdentityMatrix(const float mat[16])
{
    int i;
    for(i=0;i<16;i+=5)
        if(mat[i]!=1)
            return false;
    for(i=0;i<16;i++)
        if((i%5)>0 && mat[i]!=0)
            return false;
    return true;
}
