/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* TEXTURE.CPP:  Texture management system                                   *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
#include <GL/glfw.h> //////////////////
#include <IL/il.h>
#include <IL/ilut.h>
/****************************************************************************/
#include "texture.h"
#include "memwatch.h"
#include "oglwrap.h"
#ifndef BORLAND
    #include "snprintf.h"
#endif

#pragma package(smart_init)

/****************************************************************************
                               Static Variables
 ****************************************************************************/
static vector<tTexturePack *>   texturePacks;
static vector<tSkin *>          skins;
static vector<tAnimType *>      animTypes;
static vector<tPanel *>         panels;
static char*                    TPKFileSig = "MACHTPK1";

/****************************************************************************
                               Static Functions
 ****************************************************************************/
static tTexturePack*    addTexturePack(const tString& name);
static tSkin*           addSkin(const tString& name);
static tTexturePack*    findTexturePack(const tString& name);
static tSkin*           findSkin(const tString& filename);
static bool             createTexture(const tString& name, iTextureIndex& texture,
                            int& width, int& height, int& bpp);


/****************************************************************************/
bool tTexturePack::load(const tString& _name)
{
    int i;
    long l;
    char ch;
    long textureCount;

    name=STRTOLOWER(_name);
    tString filename=TEXTURE_PATH+name+TEXTURE_EXTENSION;

    FILE *fp=fopen(CHARPTR(filename),"rb");
    if(fp)
    {
        char buf[8];
        if(fread(buf,sizeof(buf),1,fp)<1 || memcmp(buf,TPKFileSig,8)!=0)
        {
            bug("tTexturePack::load: '%s' is not a valid texture pack file",CHARPTR(filename));
            fclose(fp);
            return false;
        }

        imageFile=readString(fp);

        fread(&textureCount,sizeof(textureCount),1,fp);
        for(i=0;i<textureCount;i++)
        {
            tTexture t;

            t.name=STRTOLOWER(readString(fp));
            t.pack=this;

            fread(&l,sizeof(l),1,fp);   t.x=(int)l;
            fread(&l,sizeof(l),1,fp);   t.y=(int)l;
            fread(&l,sizeof(l),1,fp);   t.width=(int)l;
            fread(&l,sizeof(l),1,fp);   t.height=(int)l;
            fread(&l,sizeof(l),1,fp);   t.xOffset=-(int)l;
            fread(&l,sizeof(l),1,fp);   t.yOffset=-(int)l;

            fread(&ch,1,1,fp);
            t.alphaBlending=(bool)ch;
            fread(&ch,1,1,fp);
            t.alphaTesting=(bool)ch;

            textures.push_back(t);
        }
        fclose(fp);
    }

    //Generate a new texture:
    glGenTextures(1,&index);

    if(!refresh())
        return false;

    return true;
}
/****************************************************************************/
bool tTexturePack::refresh()
{
    return createTexture(TEXTURE_PATH+imageFile,index,width,height,bpp);
}
/****************************************************************************/
tTexture* tTexturePack::getTexture(const tString& name)
{
    tString lowerName=STRTOLOWER(name);
    for(int i=0;i<textures.size();i++)
        if(STREQUAL(textures[i].name,lowerName))
        {
            references++;
            return &textures[i];
        }
    return NULL;
}
/****************************************************************************/
void tTexturePack::releaseTexture(tTexture* texture)
{
    references--;
}
/****************************************************************************/
tTexturePack::~tTexturePack()
{
    if(index!=0)
        glDeleteTextures(1,&index);
}
/****************************************************************************/
bool tSkin::load(const tString& _filename)
{
    filename=_filename;

    //Generate a new texture:
    glGenTextures(1,&index);

    if(!refresh())
        return false;

    return true;
}
/****************************************************************************/
bool tSkin::refresh()
{
    return createTexture(filename,index,width,height,bpp);
}
/****************************************************************************/
tSkin::~tSkin()
{
    if(index!=0)
        glDeleteTextures(1,&index);
}
/***************************************************************************\
tAnimType::tAnimType

Initializes the animation type with one or more textures from a texture pack file.
Label these textures in the following manner.

PackName.AnimationName0
PackName.AnimationName1
PackName.AnimationName2
...

The function will automatically detect the number of textures.

Inputs:
    s       The function will load its parameters from the animation data file.
Outputs:
    none
\***************************************************************************/
tAnimType::tAnimType(tStream& s) throw(tString) : frameRate(0), repeat(false)
{
    tString textureName;
    char buf[100];
    tTexture *t;

    try
    {
        name=parseString("name", s);
        name=STRTOLOWER(name);
        textureName=parseString("texture", s);
        frameRate=parseFloatMin("frame rate",0, s);
        repeat=parseBool("repeat", s);

        for(int i=0;;i++)
        {
            snprintf(buf,sizeof(buf),"%s%d",CHARPTR(textureName),i);
            t=getTexture(buf);
            if(t==NULL)
                break;
            frames.push_back(t);
        }
    }
    catch(const tString& s)
    {
        destroy();
        throw s;
    }
}

/***************************************************************************\
tAnimType::destroy

Releases textures used by this animation type.
\***************************************************************************/
void tAnimType::destroy()
{
    for(int i=0;i<frames.size();i++)
        releaseTexture(frames[i]);
}

/***************************************************************************\
tAnim::tAnim

Creates empty instance.  You must call load to load an animation type.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
tAnim::tAnim() : type(NULL), startTime1(currentTime), startTime2(0), curFrame(0),
    finished(false)
{
}

/***************************************************************************\
tAnim::tAnim

Creates an animation which will display a specified animation type.  The animation
will appear in its original scale and orientation.

Inputs:
    type    Non-null pointer to an animation type
Outputs:
    none
\***************************************************************************/
tAnim::tAnim(tAnimType* _type) : type(_type), startTime1(currentTime), startTime2(),
    curFrame(0), finished(false)
{
#ifdef DEBUG
    //Check precondition.
    if(MEMORY_VIOLATION(type))
        type=NULL;
#endif
}

/***************************************************************************\
tAnim::load

Loads an existing instance of tAnim with a new animation type.  The animation
will appear in its original scale and orientation.

Inputs:
    type    Non-null pointer to an animation type
Outputs:
    none
\***************************************************************************/
void tAnim::load(tAnimType *_type)
{
    type=_type;
#ifdef DEBUG
    if(MEMORY_VIOLATION(type))
        type=NULL;
#endif
}

/***************************************************************************\
tAnim::start

Resets the animation to the first frame.  The frame timer will begin from
the specified system time.

Inputs:
    startTime   Time when animation will begin (or has already began)
Outputs:
    none
\***************************************************************************/
void tAnim::start(tTime _startTime)
{
    finished=false;
    curFrame=0;
    startTime1=_startTime;
}

/***************************************************************************\
tAnim::start

Resets the animation to the first frame.  The frame timer will begin from
the specified game time.

Inputs:
    startTime   Time when animation will begin (or has already began)
Outputs:
    none
\***************************************************************************/
void tAnim::start(tWorldTime _startTime)
{
    finished=false;
    curFrame=0;
    startTime2=_startTime;
}

/***************************************************************************\
tAnim::tick

Updates the animation with a specified system time.  Function determines
current frame using frame rate and time elapsed.  If the animation has
completed, function sets finished if repeat equals true, or restarts the
animation if repeat equals false.

Inputs:
    curTime     Current system time
Outputs:
    none
\***************************************************************************/
void tAnim::tick(tTime curTime)
{
    if(finished || MEMORY_VIOLATION(type) || type->frames.size()==0)
    {
        finished=true;
        return;
    }
    curFrame=(int) ((curTime-startTime1)*type->frameRate);
    if(curFrame>=type->frames.size())
    {
        if(!type->repeat)
        {
            finished=true;
            return;
        }
        else
            curFrame %= type->frames.size();
    }
}

/***************************************************************************\
tAnim::tick

Updates the animation with a specified game time.  Function determines
current frame using frame rate and time elapsed.  If the animation has
completed, function sets finished if repeat equals true, or restarts the
animation if repeat equals false.

Inputs:
    curTime     Current game time
Outputs:
    none
\***************************************************************************/
void tAnim::tick(tWorldTime curTime)
{
    if(finished || MEMORY_VIOLATION(type) || type->frames.size()==0)
    {
        finished=true;
        return;
    }
    curFrame=(int) ((curTime-startTime2)*type->frameRate);
    if(curFrame>=type->frames.size())
    {
        if(!type->repeat)
        {
            finished=true;
            return;
        }
        else
            curFrame %= type->frames.size();
    }
}

/***************************************************************************\
tAnim::getProgress

Retrieves the animation's progress as a percentage between 0 and 1.  0 indicates
the animation just started.  1 indicates the animation just ended.  If the
animation resets, the progress will reset to 0 also.

Inputs:
    none
Outputs:
    A percentage between 0 and 1
\***************************************************************************/
float tAnim::getProgress()
{
    if(type && type->frames.size()>0)
        return (float)curFrame/type->frames.size();
    else
        return 0;
}

/***************************************************************************\
tAnim::draw

Draws the current frame at the specified position.  Update the current frame
by calling tick just prior to this function.  Function will immediately return
if no animation type is loaded.

Inputs:
    x,y     Position at which to draw animation.  Usually, this is the center
                or "hot spot" on the animation.
Outputs:
    none
\***************************************************************************/
void tAnim::draw(int x, int y)
{
    //Ensure user has loaded an animation type:
    if(!type || finished)
        return;

    drawTexture(type->frames[curFrame],x,y);
}



/****************************************************************************/
void textureStartup()
{
}
/****************************************************************************/
void textureShutdown()
{
    int i;
    for(i=0;i<texturePacks.size();i++)
        mwDelete texturePacks[i];
    for(i=0;i<skins.size();i++)
        mwDelete skins[i];
    texturePacks.clear();
    skins.clear();
}
/****************************************************************************/
void textureRefresh()
{
    int i;
    for(i=0;i<texturePacks.size();i++)
        texturePacks[i]->refresh();
    for(i=0;i<skins.size();i++)
        skins[i]->refresh();
}
/****************************************************************************/
bool createTexture(const tString& name, iTextureIndex& texture, int& width, int& height, int& bpp)
{
    iImageIndex imageName;

#ifdef DEBUG
    if(texture==0)
    {
        bug("createTexture: generate a texture first with glGenTextures");
        return false;
    }
#endif

    ilGenImages(1, &imageName);
    ilBindImage(imageName);
    //The cast is a hack for DevIL.  Presumably, this function does not modify
    //the filename.
    if(ilLoadImage((char*)CHARPTR(name))==IL_FALSE)
    {
        bug("createTexture: Failed to load '%s'", name.c_str());
        return false;
    }

    width=ilGetInteger(IL_IMAGE_WIDTH);
    height=ilGetInteger(IL_IMAGE_HEIGHT);
    bpp=ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL);

    glEnable(GL_TEXTURE_2D);
    mglBindTexture(texture);

    ilutGLTexImage(0);
    ilDeleteImages(1, &imageName);

    //Set texture parameters for best performance:
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //Clean-up:
    glDisable(GL_TEXTURE_2D);
    return true;
}
/****************************************************************************/
void drawTexture(tTexture *texture, int x, int y, int w, int h, bool repeat)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(texture))
        return;
    if(MEMORY_VIOLATION(texture->pack))
        return;
    if(texture->pack->getWidth()<=0 || texture->pack->getHeight()<=0)
    {
        bug("drawTexture: invalid dimensions");
        return;
    }
#endif

    if(texture->alphaBlending)
        glEnable(GL_BLEND);
    if(texture->alphaTesting)
        glEnable(GL_ALPHA_TEST);
    glEnable(GL_TEXTURE_2D);

    mglBindTexture(texture->pack->getIndex());

    float left=(float) texture->x/texture->pack->getWidth();
    float right=(float) (texture->x+texture->width)/texture->pack->getWidth();
    float bottom=(float) texture->y/texture->pack->getHeight();
    float top=(float) (texture->y+texture->height)/texture->pack->getHeight();

    x+=texture->xOffset;
    y+=texture->yOffset;

    glBegin(GL_QUADS);

	if(w<0)
		w=texture->width;
	if(h<0)
		h=texture->height;

	if(repeat==false)
	{
        glTexCoord2f(left,bottom);
        glVertex2i(x,y);
        glTexCoord2f(right,bottom);
        glVertex2i(x+w,y);
        glTexCoord2f(right,top);
        glVertex2i(x+w,y+h);
        glTexCoord2f(left,top);
        glVertex2i(x,y+h);
	}
	else
	{
		float xf=(float)x, yf=(float)y;
		int hPanels=w/texture->width;
		int vPanels=h/texture->height;
		float panelWidth=(float)w/hPanels;
		float panelHeight=(float)h/vPanels;
		for(int i=0;i<hPanels;i++)
			for(int j=0;j<vPanels;j++)
			{
				glTexCoord2f(left,bottom);
				glVertex2f(xf+panelWidth*i,yf+panelHeight*j);
				glTexCoord2f(right,bottom);
				glVertex2f(xf+panelWidth*(i+1),yf+panelHeight*j);
				glTexCoord2f(right,top);
				glVertex2f(xf+panelWidth*(i+1),yf+panelHeight*(j+1));
				glTexCoord2f(left,top);
				glVertex2f(xf+panelWidth*i,yf+panelHeight*(j+1));
			}
	}

    glEnd();

    glDisable(GL_BLEND);
    glDisable(GL_ALPHA_TEST);
    glDisable(GL_TEXTURE_2D);
}
/****************************************************************************/
void bindTexture(tTexture *texture)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(texture))
        return;
#endif
    mglBindTexture(texture->pack->getIndex());
}
/****************************************************************************/
void bindSkin(tSkin *skin)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(skin))
        return;
#endif
    mglBindTexture(skin->getIndex());
}
/****************************************************************************/
tTexturePack* addTexturePack(const tString& name)
{
    tTexturePack *t = mwNew tTexturePack;
    if(t->load(name))
    {
        texturePacks.push_back(t);
        return t;
    }
    mwDelete t;
    return NULL;
}
/****************************************************************************/
tTexturePack* findTexturePack(const tString& name)
{
    tString lowerName=STRTOLOWER(name);
    for(int i=0;i<texturePacks.size();i++)
        if(STREQUAL(texturePacks[i]->getName(),lowerName))
            return texturePacks[i];
    return NULL;
}
/****************************************************************************/
tSkin* findSkin(const tString& filename)
{
    tString lowerName=STRTOLOWER(filename);
    for(int i=0;i<skins.size();i++)
        if(STREQUAL(skins[i]->getFilename(),lowerName))
            return skins[i];
    return NULL;
}
/****************************************************************************/
tTexture* getTexture(const tString& name)
{
    tTexturePack *tp;
    tStringBuffer buf(name);
    char *ptr=strchr(buf.begin(),'.');

    if(ptr==NULL)
    {
        bug("getTexture: name must contain pack name and texture name separated by period");
        return NULL;
    }
    *(ptr++)='\0';
    tp=findTexturePack(buf.begin());
    if(tp==NULL)
        tp=addTexturePack(buf.begin());
    if(tp==NULL)
        return NULL;
    return tp->getTexture(ptr);
}
/****************************************************************************/
void releaseTexture(tTexture *texture)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(texture))
        return;
#endif
    tTexturePack *pack=texture->pack;
    pack->releaseTexture(texture);
    if(pack->getReferences()==0)
    {
        for(int i=0;i<texturePacks.size();i++)
            if(texturePacks[i]==pack)
            {
                texturePacks.erase(texturePacks.begin()+i);
                break;
            }
        //texture no longer exists after this loop!!!
        mwDelete pack;
    }
}
/****************************************************************************/
tSkin* getSkin(const tString& filename)
{
    tSkin *sk;
    sk=findSkin(filename);
    if(sk==NULL)
        sk=addSkin(filename);
    if(sk==NULL)
        return NULL;
    sk->incrementReferences();
    return sk;
}
/****************************************************************************/
void releaseSkin(tSkin *skin)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(skin))
        return;
#endif
    skin->decrementReferences();
    if(skin->getReferences()==0)
    {
        for(int i=0;i<skins.size();i++)
            if(skins[i]==skin)
            {
                skins.erase(skins.begin()+i);
                break;
            }
        //texture no longer exists after this loop!!!
        mwDelete skin;
    }
}
/****************************************************************************/
tSkin* addSkin(const tString& filename)
{
    tSkin *sk = mwNew tSkin;
    if(sk->load(filename))
    {
        skins.push_back(sk);
        return sk;
    }
    mwDelete sk;
    return NULL;
}




/****************************************************************************/
//Look-up an animation type by string identifier
tAnimType *animTypeLookup(const tString& name, bool exact)
{
    int i;
    tString n=STRTOLOWER(name);
    for(i=0; i<animTypes.size(); i++)
        if(STREQUAL(animTypes[i]->name,n))
            return animTypes[i];
    if(exact)
        return NULL;
    for(i=0; i<animTypes.size(); i++)
        if(STRPREFIX(n,animTypes[i]->name))
            return animTypes[i];
    return NULL;
}
/****************************************************************************/
bool loadAnimTypes(const char *filename)
//Load animation types from file
{
    tString error;
    tAnimType *at;
    //Destroy any existing animation types
    destroyAnimTypes();

    FILE *fp=fopen(filename, "r");
    tStream stream;
    if(fp==NULL)
    {
        bug("loadAnimTypes: failed to open '%s'", filename);
        return false;
    }

    try
    {
        stream.load(fp);
        while(!stream.atEndOfStream())
        {
            at=mwNew tAnimType(stream);
            animTypes.push_back(at);
        }
    }
    catch(const tString& message)
    {
        error=stream.parseErrorMessage(filename,message);
        bug(CHARPTR(error));
        fclose(fp);
        return false;
    }
    fclose(fp);
    return true;
}
/****************************************************************************/
void destroyAnimTypes()
//Destroy animation types
{
    for(int i=0; i<animTypes.size(); i++)
        mwDelete animTypes[i];
    animTypes.clear();
}
/****************************************************************************/
tPanel::tPanel(tStream& s) throw(tString)
{
	name=parseIdentifier("panel name",s);
	if(panelLookup(name,/*exact=*/true))
		throw tString("A panel by the name of '")+name+"'";

	try
	{
		if(s.beginBlock())
		{
			while(!s.endBlock())
			{
				tString str=parseString("tag",s);
				s.matchOptionalToken("=");
	
				if(strPrefix(str,"texture"))
					parseTextureBlock(s);
				else if(strPrefix(str,"composite_texture"))
					parseCompositeTextureBlock(s);
				else
					throw tString("Unrecognized tag '")+str+"'";

				s.matchOptionalToken("=");
			}
		}
	}
	catch(const tString& msg)
	{
		destroy();
		throw msg;
	}
}
/****************************************************************************/
void tPanel::parseTextureBlock(tStream& s) throw(tString)
{
    int top,right;
	eStatus leftStatus=STATUS_UNSPECIFIED, widthStatus=STATUS_UNSPECIFIED,
		bottomStatus=STATUS_UNSPECIFIED, heightStatus=STATUS_UNSPECIFIED,
        topStatus=STATUS_UNSPECIFIED, rightStatus=STATUS_UNSPECIFIED;

	tPanelTexture pt={NULL, ANCHOR_LEFT, 0, 0, ANCHOR_BOTTOM, 0, 0, false};
	tString name=parseString("texture name",s);
	pt.texture=getTexture(name);
	if(pt.texture==NULL)
		throw tString("Texture not found: '")+name+"'";

	try
	{
		if(s.beginBlock())
		{
			while(!s.endBlock())
			{
				tString str=parseString("tag",s);
				s.matchOptionalToken("=");

				if(strPrefix(str,"left"))
					parseValue(pt.left,leftStatus,s);
				else if(strPrefix(str,"bottom"))
					parseValue(pt.bottom,bottomStatus,s);
				else if(strPrefix(str,"width"))
					parseValue(pt.width,widthStatus,s);
				else if(strPrefix(str,"height"))
					parseValue(pt.height,heightStatus,s);
				else if(strPrefix(str,"right"))
					parseValue(right,rightStatus,s);
				else if(strPrefix(str,"top"))
					parseValue(top,topStatus,s);
				else if(strPrefix(str,"repeat"))
					pt.repeat=parseBool("repeat?",s);
				else
					throw tString("Unrecognized tag '")+str+"'";

				s.matchOptionalToken(";");
			}
		}

        if(rightStatus==STATUS_ABSOLUTE)
        {
            if(leftStatus==STATUS_UNSPECIFIED)
            {
                if(widthStatus==STATUS_UNSPECIFIED) //##-##=##
                {
                    pt.left=right-pt.texture->width;
                    leftStatus=STATUS_ABSOLUTE;
                }
                else if(widthStatus==STATUS_ABSOLUTE) //##-##=##
                {
                    pt.left=right-pt.width;
                    leftStatus=STATUS_ABSOLUTE;
                }
                else if(widthStatus==STATUS_RELATIVE) //##-(width-##)=-width+## ERROR
                    throw tString("Invalid values for 'width' and 'right'");
            }
            else if(leftStatus==STATUS_ABSOLUTE) //##-##=##
            {
                if(widthStatus!=STATUS_UNSPECIFIED)
                    throw tString("You may specify only two of the following: 'left', 'width', 'right'");
                pt.width=right-pt.left;
                widthStatus=STATUS_ABSOLUTE;
            }
            else if(leftStatus==STATUS_RELATIVE) //##-(width-##)=-width+## ERROR
                throw tString("Invalid values for 'left' and 'right'");
        }
        else if(rightStatus==STATUS_RELATIVE)
        {
            if(leftStatus==STATUS_UNSPECIFIED)
            {
                if(widthStatus==STATUS_UNSPECIFIED) //(width-##)-##=width-##
                {
                    pt.left=right-pt.texture->width;
                    leftStatus=STATUS_RELATIVE;
                }
                else if(widthStatus==STATUS_ABSOLUTE) //(width-##)-##=width-##
                {
                    pt.left=right-pt.width;
                    leftStatus=STATUS_RELATIVE;
                }
                else if(widthStatus==STATUS_RELATIVE) //(width-##)-(width-##)=##
                {
                    pt.left=right-pt.width;
                    leftStatus=STATUS_ABSOLUTE;
                }
            }
            else if(leftStatus==STATUS_ABSOLUTE) //(width-##)-##=width-##
            {
                if(widthStatus!=STATUS_UNSPECIFIED)
                    throw tString("You may specify only two of the following: 'left', 'width', 'right'");
                pt.width=right-pt.left;
                widthStatus=STATUS_RELATIVE;
            }
            else if(leftStatus==STATUS_RELATIVE) //(width-##)-(width-##)=##
            {
                if(widthStatus!=STATUS_UNSPECIFIED)
                    throw tString("You may specify only two of the following: 'left', 'width', 'right'");
                pt.width=right-pt.left;
                widthStatus=STATUS_ABSOLUTE;
            }
        }

		if(widthStatus==STATUS_ABSOLUTE && pt.width<0)
			throw tString("The width must be non-negative");

		if(widthStatus==STATUS_UNSPECIFIED)
		{
			pt.width=pt.texture->width;
			if(leftStatus==STATUS_UNSPECIFIED)
				pt.hAlignment=ANCHOR_CENTER;
			else if(leftStatus==STATUS_ABSOLUTE)
				pt.hAlignment=ANCHOR_LEFT;
			else //leftStatus==STATUS_RELATIVE
				pt.hAlignment=ANCHOR_RIGHT;
		}
		else if(widthStatus==STATUS_ABSOLUTE)
		{
			if(leftStatus==STATUS_UNSPECIFIED)
				pt.hAlignment=ANCHOR_CENTER;
			else if(leftStatus==STATUS_ABSOLUTE)
				pt.hAlignment=ANCHOR_LEFT;
			else //leftStatus==STATUS_RELATIVE
				pt.hAlignment=ANCHOR_RIGHT;
		}
		else //widthStatus==STATUS_RELATIVE
		{
			if(leftStatus==STATUS_UNSPECIFIED)
			{
				pt.left=-pt.width/2;
				pt.hAlignment=ANCHOR_BOTH;
			}
			else if(leftStatus==STATUS_ABSOLUTE)
				pt.hAlignment=ANCHOR_BOTH;
			else //leftStatus==STATUS_RELATIVE
				throw tString("You cannot specify both 'left' and 'width' in terms of width");
		}

        if(topStatus==STATUS_ABSOLUTE)
        {
            if(bottomStatus==STATUS_UNSPECIFIED)
            {
                if(heightStatus==STATUS_UNSPECIFIED) //##-##=##
                {
                    pt.bottom=top-pt.texture->height;
                    bottomStatus=STATUS_ABSOLUTE;
                }
                else if(heightStatus==STATUS_ABSOLUTE) //##-##=##
                {
                    pt.bottom=top-pt.height;
                    bottomStatus=STATUS_ABSOLUTE;
                }
                else if(heightStatus==STATUS_RELATIVE) //##-(height-##)=-height+## ERROR
                    throw tString("Invalid values for 'height' and 'top'");
            }
            else if(bottomStatus==STATUS_ABSOLUTE) //##-##=##
            {
                if(heightStatus!=STATUS_UNSPECIFIED)
                    throw tString("You may specify only two of the following: 'bottom', 'height', 'top'");
                pt.height=top-pt.bottom;
                heightStatus=STATUS_ABSOLUTE;
            }
            else if(bottomStatus==STATUS_RELATIVE) //##-(height-##)=-height+## ERROR
                throw tString("Invalid values for 'bottom' and 'top'");
        }
        else if(topStatus==STATUS_RELATIVE)
        {
            if(bottomStatus==STATUS_UNSPECIFIED)
            {
                if(heightStatus==STATUS_UNSPECIFIED) //(height-##)-##=height-##
                {
                    pt.bottom=top-pt.texture->height;
                    bottomStatus=STATUS_RELATIVE;
                }
                else if(heightStatus==STATUS_ABSOLUTE) //(height-##)-##=height-##
                {
                    pt.bottom=top-pt.height;
                    bottomStatus=STATUS_RELATIVE;
                }
                else if(heightStatus==STATUS_RELATIVE) //(height-##)-(height-##)=##
                {
                    pt.bottom=top-pt.height;
                    bottomStatus=STATUS_ABSOLUTE;
                }
            }
            else if(bottomStatus==STATUS_ABSOLUTE) //(height-##)-##=height-##
            {
                if(heightStatus!=STATUS_UNSPECIFIED)
                    throw tString("You may specify only two of the following: 'bottom', 'height', 'top'");
                pt.height=top-pt.bottom;
                heightStatus=STATUS_RELATIVE;
            }
            else if(bottomStatus==STATUS_RELATIVE) //(height-##)-(height-##)=##
            {
                if(heightStatus!=STATUS_UNSPECIFIED)
                    throw tString("You may specify only two of the following: 'bottom', 'height', 'top'");
                pt.height=top-pt.bottom;
                heightStatus=STATUS_ABSOLUTE;
            }
        }

		if(heightStatus==STATUS_ABSOLUTE && pt.height<0)
			throw tString("The height must be non-negative");

		if(heightStatus==STATUS_UNSPECIFIED)
		{
			pt.height=pt.texture->height;
			if(bottomStatus==STATUS_UNSPECIFIED)
				pt.vAlignment=ANCHOR_CENTER;
			else if(bottomStatus==STATUS_ABSOLUTE)
				pt.vAlignment=ANCHOR_BOTTOM;
			else //bottomStatus==STATUS_RELATIVE
				pt.vAlignment=ANCHOR_TOP;
		}
		else if(heightStatus==STATUS_ABSOLUTE)
		{
			if(bottomStatus==STATUS_UNSPECIFIED)
				pt.vAlignment=ANCHOR_CENTER;
			else if(bottomStatus==STATUS_ABSOLUTE)
				pt.vAlignment=ANCHOR_BOTTOM;
			else //bottomStatus==STATUS_RELATIVE
				pt.vAlignment=ANCHOR_TOP;
		}
		else //heightStatus==STATUS_RELATIVE
		{
			if(bottomStatus==STATUS_UNSPECIFIED)
			{
				pt.bottom=-pt.height/2;
				pt.vAlignment=ANCHOR_BOTH;
			}
			else if(bottomStatus==STATUS_ABSOLUTE)
				pt.vAlignment=ANCHOR_BOTH;
			else //bottomStatus==STATUS_RELATIVE
				throw tString("You cannot specify both 'bottom' and 'height' in terms of height");
		}
	}
	catch(const tString& msg)
	{
		releaseTexture(pt.texture);
		throw msg;
	}
	panelTextures.push_back(pt);
}
/****************************************************************************/
void tPanel::parseValue(int& value, eStatus& status, tStream& s) throw(tString)
{
	if(s.matchOptionalToken("width") || s.matchOptionalToken("height"))
	{
        if(s.hasOptionalParameter())
        {
            bool negative=s.matchOptionalToken("-");
            s.matchOptionalToken("+");
            value=parseInt("value",s);
            if(negative)
                value=-value;
        }
        else
            value=0;
		status=STATUS_RELATIVE;
	}
	else
	{
		value=parseInt("value",s);
		status=STATUS_ABSOLUTE;
	}
}
/****************************************************************************/
void tPanel::parseCompositeTextureBlock(tStream& s) throw(tString)
{
	int i,j;
	bool hasLeftMargin=false,hasBottomMargin=false,hasRightMargin=false,hasTopMargin=false;
	vector<tTexture *> textures;
    bool repeat=false;

	try
	{
		if(s.beginBlock())
		{
			while(!s.endBlock())
			{
				tString str=parseString("tag",s);
				s.matchOptionalToken("=");

				if(strPrefix(str,"left_margin"))
					hasLeftMargin=parseBool("has left margin?",s);
				else if(strPrefix(str,"bottom_margin"))
					hasBottomMargin=parseBool("has bottom margin?",s);
				else if(strPrefix(str,"right_margin"))
					hasRightMargin=parseBool("has right margin?",s);
				else if(strPrefix(str,"top_margin"))
					hasTopMargin=parseBool("has top margin?",s);
				else if(strPrefix(str,"textures"))
				{
					tString name;
					tTexture *t;
					do
					{
						name=parseString("texture name",s);
						t=getTexture(name);
						if(t==NULL)
							throw tString("Texture not found: '")+name+"'";
						textures.push_back(t);
					}
					while(s.matchOptionalToken(","));
				}
				else if(strPrefix(str,"repeat"))
				    repeat=parseBool("repeat?",s);
				else
					throw tString("Unrecognized tag '")+str+"'";

				s.matchOptionalToken(";");
			}
		}
	}
	catch(const tString& msg)
	{
		for(i=0;i<textures.size();i++)
			releaseTexture(textures[i]);
		throw msg;
	}

	int columns=1+(hasLeftMargin?1:0)+(hasRightMargin?1:0);
	int rows=1+(hasBottomMargin?1:0)+(hasTopMargin?1:0);
	if(textures.size()!=columns*rows)
	{
		char buf[100];
		snprintf(buf,sizeof(buf),"You need to enter exactly %d for the margins you have selected");
		throw tString(buf);
	}

	int w1=0,w2=0,w3=0;
	int h1=0,h2=0,h3=0;

	i=0;
	if(hasTopMargin)
	{
		h1=textures[i]->height;
		for(j=1;j<columns;j++)
			if(textures[i+j]->height!=h1)
				throw tString("All textures in the top row must have the same height");
        i+=columns;
	}
	h2=textures[i]->height;
	for(j=1;j<columns;j++)
		if(textures[i+j]->height!=h2)
			throw tString("All textures in the center row must have the same height");
    i+=columns;
	if(hasBottomMargin)
	{
		h3=textures[i]->height;
		for(j=1;j<columns;j++)
			if(textures[i+j]->height!=h3)
				throw tString("All textures in the bottom row must have the same height");
        i+=columns;
	}

	i=0;
	if(hasLeftMargin)
	{
		w1=textures[i]->width;
		for(j=1;j<rows;j++)
			if(textures[i+j*columns]->width!=w1)
				throw tString("All textures in the left column must have the same width");
		i++;
	}
	w2=textures[i]->width;
	for(j=1;j<rows;j++)
		if(textures[i+j*columns]->width!=w2)
			throw tString("All textures in the center column must have the same width");
	i++;
	if(hasRightMargin)
	{
		w3=textures[i]->width;
		for(j=1;j<rows;j++)
			if(textures[i+j*columns]->width!=w3)
				throw tString("All textures in the right column must have the same width");
		i++;
	}

	int l[3],w[3],b[3],h[3];
	eAnchor va[3],ha[3];
	int index;

	index=0;
	if(hasTopMargin)
	{
		b[index]=-h1;	h[index]=h1;		va[index++]=ANCHOR_TOP;
	}
		b[index]=h3;	h[index]=-h1-h3;	va[index++]=ANCHOR_BOTH;
	if(hasBottomMargin)
	{
		b[index]=0;		h[index]=h3;		va[index++]=ANCHOR_BOTTOM;
	}

	index=0;
	if(hasLeftMargin)
	{
		l[index]=0;  	w[index]=w1;		ha[index++]=ANCHOR_LEFT;
	}
		l[index]=w1;	w[index]=-w1-w3;	ha[index++]=ANCHOR_BOTH;
	if(hasRightMargin)
	{
		l[index]=-w3;	w[index]=w3;		ha[index++]=ANCHOR_RIGHT;
	}

	index=0;
	for(i=0;i<rows;i++)
		for(j=0;j<columns;j++)
		{
			tPanelTexture pt={textures[index],
							 ha[j],l[j],w[j],
							 va[i],b[i],h[i],
							 repeat};
            panelTextures.push_back(pt);
			index++;
		}
}
/****************************************************************************/
void tPanel::destroy()
{
	itPanelTexture it;
	for(it=panelTextures.begin();it!=panelTextures.end();it++)
		releaseTexture(it->texture);

	panelTextures.clear();
}
/****************************************************************************/
void tPanel::draw(int x, int y, int width, int height)
{
	int l,b,w,h;
	itPanelTexture it;
	for(it=panelTextures.begin();it!=panelTextures.end();it++)
	{
		switch(it->hAlignment)
		{
			case ANCHOR_LEFT:
				l=x+it->left;
				w=it->width;
				break;
			case ANCHOR_RIGHT:
				l=x+width+it->left;
				w=it->width;
				break;
			case ANCHOR_BOTH:
				l=x+it->left;
				w=width+it->width;
				break;
			case ANCHOR_CENTER:
				l=x+(width-it->width)/2;
				w=it->width;
				break;
		}
		switch(it->vAlignment)
		{
			case ANCHOR_BOTTOM:
				b=y+it->bottom;
				h=it->height;
				break;
			case ANCHOR_TOP:
				b=y+height+it->bottom;
				h=it->height;
				break;
			case ANCHOR_BOTH:
				b=y+it->bottom;
				h=height+it->height;
				break;
			case ANCHOR_CENTER:
				b=y+(height-it->height)/2;
				h=it->height;
				break;
		}
		drawTexture(it->texture,l,b,w,h,it->repeat);
	}
}
/****************************************************************************/
tPanel *panelLookup(const tString& name, bool exact)
{
    int i;
    tString n=STRTOLOWER(name);
    for(i=0; i<panels.size(); i++)
        if(STREQUAL(panels[i]->getName(),n))
            return panels[i];
    if(exact)
        return NULL;
    for(i=0; i<panels.size(); i++)
        if(STRPREFIX(n,panels[i]->getName()))
            return panels[i];
    return NULL;
}
/****************************************************************************/
bool loadPanels(const char *filename)
{
    tString error;
    destroyPanels();

    FILE *fp=fopen(filename, "r");
    tPanel *p;
    tStream stream;
    if(fp==NULL)
    {
        bug("loadPanels: failed to open '%s'", filename);
        return false;
    }

    try
    {
        stream.load(fp);
        while(!stream.atEndOfStream())
        {
            tString str=parseString("tag",stream);
            stream.matchOptionalToken("=");

            if(strPrefix(str,"panel"))
            {
                p=mwNew tPanel(stream);
                panels.push_back(p);
            }
            else if(strPrefix(str,"end"))
                break;
            else
                throw tString("Unrecognized tag '")+str+"'";

            stream.matchOptionalToken(";");
        }
    }
    catch(const tString& message)
    {
        error=stream.parseErrorMessage(filename,message);
        bug(CHARPTR(error));
        fclose(fp);
        return false;
    }
    fclose(fp);
    return true;
}
/****************************************************************************/
void destroyPanels()
{
    for(int i=0; i<panels.size(); i++)
        mwDelete panels[i];
    panels.clear();
}

