/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* BEHAVIOR.CPP: Defines a variety of custom behaviors that endow units      *
*               with special abilities.                                     *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop

/****************************************************************************/
#include "behavior.h"
#include "client02.h"
#include "memwatch.h"
#include "unit.h"
#include "oglwrap.h"

#pragma package(smart_init)

#define RUN_PROCEDURE(x) \
    { \
        if(parent->getModel() && x) \
            parent->getModel()->runProcedure(x,t); \
    }
#define ARGUMENT_EXISTS(x) \
    (parent->getModel() && type->x.loaded)
#define EVALUATE_ARGUMENT(x) \
    (type->x.evaluate(parent->getModel()))
#define PARSE_PROCEDURE(x) \
    { \
        validateModelType(parent->modelType); \
        x=parent->modelType->parseProcedureType("procedure name",s); \
    }
#define PARSE_ARGUMENT(x) \
    { \
        validateModelType(parent->modelType); \
        x.load(parent->modelType,s); \
    }
#define PARSE_MODEL_VECTOR(x) \
    { \
        validateModelType(parent->modelType); \
        x=parent->modelType->parseModelVectorType("model vector name",s); \
    }

static void parseBuildOptions(tStringList& names, tStream& s) throw(tString);
static void convertBuildOptions(const tStringList& names, bool buildOptions[],
								eParamType paramType) throw(tString);
static eBehaviorName parseBehaviorName(const char *fieldName, tStream& s) throw(tString);

/****************************************************************************/
// CONSTRUCTION BEHAVIOR
/****************************************************************************/
tConstructionType::tConstructionType(tUnitType *parent, tStream& s) throw(tString) :
    range(4), onBeginRepairingProc(NULL), onBeginDisassemblingProc(NULL),
    onEndRepairingProc(NULL), onEndDisassemblingProc(NULL), repairRateArg(1,BOUNDARY_MIN,0),
    disassembleRateArg(1,BOUNDARY_MIN,0), onEngageProc(NULL), onDisengageProc(NULL),
    primaryRepairOriginType(NULL), primaryRepairDirectionType(NULL),
    limitAngle(false), cosTheta(0), aimProcedureType(NULL), canAimArg(true,BOUNDARY_NONE)
{
    tModelVectorType *mvt;

#ifdef DEBUG
    if(MEMORY_VIOLATION(parent))
        throw "Unexpected error occurred";
#endif

    for(int i=0;i<MAX_COMMANDS;i++)
        constructionOptions[i]=false;
    constructionOptions[COMMAND_REPAIR]=true;
    constructionOptions[COMMAND_DISASSEMBLE]=true;
    constructionOptions[COMMAND_AUTO_REPAIR]=true;
    constructionOptions[COMMAND_MANUAL_REPAIR]=true;

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

            if(strPrefix(str,"range"))
                range=parseFloatMin("range",0,s);
            else if(strPrefix(str,"origins") || strPrefix(str,"repair_origins"))
            {
				do
				{
					PARSE_MODEL_VECTOR(mvt)
					if(mvt)
						repairOriginTypes.push_back(mvt);
                }
				while(s.matchOptionalToken(","));
            }
            else if(strPrefix(str,"build_options") || strPrefix(str,"commands"))
                parseBuildOptions(names,s);
            else if(strPrefix(str,"repair_rate"))
                PARSE_ARGUMENT(repairRateArg)
            else if(strPrefix(str,"disassemble_rate"))
                PARSE_ARGUMENT(disassembleRateArg)
            else if(strPrefix(str,"on_engage"))
                PARSE_PROCEDURE(onEngageProc)
            else if(strPrefix(str,"on_disengage"))
                PARSE_PROCEDURE(onDisengageProc)
            else if(strPrefix(str,"on_begin_repairing") || strPrefix(str,"on_start_repairing"))
                PARSE_PROCEDURE(onBeginRepairingProc)
            else if(strPrefix(str,"on_begin_disassembling") || strPrefix(str,"on_start_disassembling"))
                PARSE_PROCEDURE(onBeginDisassemblingProc)
            else if(strPrefix(str,"on_end_repairing") || strPrefix(str,"on_stop_repairing"))
                PARSE_PROCEDURE(onEndRepairingProc)
            else if(strPrefix(str,"on_end_disassembling") || strPrefix(str,"on_stop_disassembling"))
                PARSE_PROCEDURE(onEndDisassemblingProc)
			else if(strPrefix(str, "on_aim"))
			{
				validateModelType(parent->modelType);
				aimProcedureType=parent->modelType->parseAimProcedureType("aim procedure name",s);
			}
			else if(strPrefix(str, "angular_tolerance"))
			{
				float angularTolerance=parseAngleRange("angular tolerance",0/*PI*/,1/*PI*/,s);
				cosTheta=cos(angularTolerance);
				limitAngle=true;
			}
			else if(strPrefix(str, "primary_repair_origin"))
				PARSE_MODEL_VECTOR(primaryRepairOriginType)
			else if(strPrefix(str, "primary_repair_direction"))
				PARSE_MODEL_VECTOR(primaryRepairDirectionType)
            else if(strPrefix(str, "can_aim"))
                PARSE_ARGUMENT(canAimArg)
            else
                throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
        }
    }

    if(primaryRepairOriginType==NULL)
        throw tString("Please specify the primary_missile_origin");
    if(limitAngle && primaryRepairDirectionType==NULL)
        throw tString("You need to specify the primary_missile_direction if you limit the angle");
    if(repairOriginTypes.size()==0)
        repairOriginTypes.push_back(primaryRepairOriginType);
}
tBehavior *tConstructionType::createBehavior(tUnit *parent)
{
    return mwNew tConstructionBehavior(this, parent);
}
void tConstructionType::lookupUnitTypes() throw(tString)
{
    convertBuildOptions(names, constructionOptions, PARAM_CONSTRUCTION);
    names.clear(); //No need to save the names any longer
}

eCommand tConstructionBehavior::getDefaultCommand(tUnit *u)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(u))
        return COMMAND_NONE;
#endif
    if(getAlliance(parent->getOwner(),u->getOwner())<=ALLIANCE_ALLY)
        return COMMAND_REPAIR;
    return COMMAND_NONE;
}
void tConstructionBehavior::onCommand(eCommand command, tUnit *u, tWorldTime t)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(u))
        return;
#endif
    if(command!=COMMAND_REPAIR && command!=COMMAND_DISASSEMBLE)
	{
		if(type->constructionOptions[command]) //it's a construction order
			command=COMMAND_REPAIR;
		else
			return;
	}

    if(command==COMMAND_REPAIR && u->getHP()<u->getMaxHP() ||
        command==COMMAND_DISASSEMBLE && u->getState()==STATE_ALIVE && canControl(parent->getOwner(),u))
        //You can only disassemble units belonging to you or your allies
    {
        setCurCommand(command,t);
        targets.push_back(u);
    }
}
void tConstructionBehavior::onCommand(eCommand command, tWorldTime t)
{
    if(command==COMMAND_AUTO_REPAIR)
        setAutoRepair(true);
    else if(command==COMMAND_MANUAL_REPAIR)
        setAutoRepair(false);
}
bool tConstructionBehavior::canExecute(eCommand command)
{
#ifdef DEBUG
    if(command<0 || command>=commandCount)
    {
        bug("tConstructionBehavior::canExecute: invalid command");
        return false;
    }
#endif
    return type->constructionOptions[command];
}
void tConstructionBehavior::intervalTick(tWorldTime t)
{
    int i;
    float fieldOfView;

    //This block of code is an optimization for acquireTarget:
    if(parent->getMovementMode()==COMMAND_WANDER)
        fieldOfView=PURSUE_RADIUS;
    else
        fieldOfView=type->range;

    tVector3D repairOrigin=parent->locatePoint(type->primaryRepairOriginType);
    tVector3D repairDirection=zeroVector3D;
    if(type->primaryRepairDirectionType)
    	repairDirection=parent->locateUnitVector(type->primaryRepairDirectionType);

    for(i=0;i<targets.size(); )
    {
        if(curCommand==COMMAND_REPAIR && targets[i]->getHP()==targets[i]->getMaxHP()
            || curCommand==COMMAND_DISASSEMBLE && targets[i]->getState()!=STATE_ALIVE)
        {
            if(targets[i]==primary)
            {
                setWorkingOnPrimary(false,t);
                setPrimary(NULL,t);
            }
            if(targets[i]==secondary)
            {
                setRepairingSecondary(false,t);
                setSecondary(NULL,t);
            }
            targets.erase(targets.begin()+i);
        }
        else
            i++;
    }
    if(targets.size()==0)
        setCurCommand(COMMAND_NONE,t);

    //This variable prevents an extraneous call to setPrimary that might call
    //a model script.  This is undesirable since we refresh primary at every
    //interval tick, often with the same target.
    bool setPrimaryToNULL=false;
    if(primary)
    {
        setWorkingOnPrimary(canWorkOn(primary) && CAN_SEE(parent,primary) &&
            withinRange(primary,repairOrigin) && withinAngle(primary,repairOrigin,repairDirection), t);
        if(!workingOnPrimary)
            setPrimaryToNULL=true;
    }
    if(curCommand!=COMMAND_NONE && (primary==NULL || setPrimaryToNULL))
        if(parent->isAbilityEnabled(ABILITY_REPAIR))
        {
            setPrimaryToNULL=false;
            int flags=0;
            if(curCommand==COMMAND_REPAIR)
                flags|=ACQUIRE_REPAIRABLE_ONLY;
            if(type->limitAngle)
                flags|=ACQUIRE_LIMIT_ANGLE;
            setPrimary(acquireTarget(parent,type->range, flags,
                repairOrigin,/*forward=*/repairDirection,/*cosAngle=*/type->cosTheta,targets),t);
        }
    if(setPrimaryToNULL)
        setPrimary(NULL,t);

    bool setSecondaryToNULL=false; //see above
    if(secondary)
    {
        setRepairingSecondary(canRepair(secondary) && CAN_SEE(parent,secondary)
            && isUnitFriendly(secondary) && autoRepair && withinRange(secondary,repairOrigin) &&
            withinAngle(secondary,repairOrigin,repairDirection), t);
        if(!repairingSecondary)
            setSecondaryToNULL=true;
    }
    if(secondary==NULL || setSecondaryToNULL)
        if(autoRepair && parent->isAbilityEnabled(ABILITY_REPAIR) && !workingOnPrimary)
        {
            setSecondaryToNULL=false;
            int flags=ACQUIRE_ALLY_ONLY|ACQUIRE_ALIVE_ONLY|ACQUIRE_DAMAGED_ONLY|ACQUIRE_REPAIRABLE_ONLY;
            if(type->limitAngle)
                flags|=ACQUIRE_LIMIT_ANGLE;
            setSecondary(acquireTarget(parent,type->range,fieldOfView, flags,
                repairOrigin,/*forward=*/repairDirection,/*cosAngle=*/type->cosTheta),t);
        }
    if(setSecondaryToNULL)
        setSecondary(NULL,t);

    //Hack for dissasemble command.  Target may become built during the course
    //of the disassembly if other units repair the target faster than we
    //can disassemble it.  This check ensures that the target remains unbuilt.
    if(curCommand==COMMAND_DISASSEMBLE && workingOnPrimary)
        primary->setBuilt(false,t);

    if(workingOnPrimary)
    {
        if(curCommand==COMMAND_REPAIR)
            setRepairRate(primary,EVALUATE_ARGUMENT(repairRateArg),t);
        else
            setDisassembleRate(primary,EVALUATE_ARGUMENT(disassembleRateArg),t);
    }
    if(repairingSecondary)
        setRepairRate(secondary,EVALUATE_ARGUMENT(repairRateArg),t);

    tUnit *u=primary;
    if(u==NULL)
        u=secondary;
    setAiming(u && EVALUATE_ARGUMENT(canAimArg)==true,u,t);
}
void tConstructionBehavior::onBeginRepairing(tUnit *u, tWorldTime t)
{
    setRepairRate(u,EVALUATE_ARGUMENT(repairRateArg),t);
    RUN_PROCEDURE(type->onBeginRepairingProc)
}
void tConstructionBehavior::onBeginDisassembling(tUnit *u, tWorldTime t)
{
    setDisassembleRate(u,EVALUATE_ARGUMENT(disassembleRateArg),t);
    RUN_PROCEDURE(type->onBeginDisassemblingProc)
}
void tConstructionBehavior::onEndRepairing(tUnit *u, tWorldTime t)
{
    setRepairRate(u,0,t);
    RUN_PROCEDURE(type->onEndRepairingProc)
}
void tConstructionBehavior::onEndDisassembling(tUnit *u, tWorldTime t)
{
    setDisassembleRate(u,0,t);
    RUN_PROCEDURE(type->onEndDisassemblingProc)
}
bool tConstructionBehavior::canWorkOn(tUnit *target)
{
    if(curCommand==COMMAND_REPAIR)
        return canRepair(target);
    else
        return canDisassemble(target);
}
bool tConstructionBehavior::canRepair(tUnit *target)
{
    if(!parent->isAbilityEnabled(ABILITY_REPAIR))
        return false;
    if(target->getHP()>=target->getMaxHP())
        return false;
#ifdef DEBUG
    if(parent->getOwner()<0 || parent->getOwner()>=MAX_PLAYERS)
    {
        bug("tConstructionBehavior::canRepair: invalid owner");
        return false;
    }
#endif
    if(!target->getType()->repairable[parent->getOwner()]) //Resources
        return false;
    return true;
}
bool tConstructionBehavior::canDisassemble(tUnit *target)
{
    if(!canControl(parent->getOwner(),target))
        return false;
    if(!parent->isAbilityEnabled(ABILITY_REPAIR))
        return false;
    if(target->getState()!=STATE_ALIVE)
        return false;
    return true;
}
void tConstructionBehavior::setWorkingOnPrimary(bool _working, tWorldTime t)
{
    if(!workingOnPrimary && _working)
    {
        workingOnPrimary=true;

        if(repairingSecondary)
            onEndRepairing(secondary,t);

        if(curCommand==COMMAND_REPAIR)
            onBeginRepairing(primary,t);
        else
            onBeginDisassembling(primary,t);
    }
    else if(workingOnPrimary && !_working)
    {
        workingOnPrimary=false;

        if(curCommand==COMMAND_REPAIR)
            onEndRepairing(primary,t);
        else
            onEndDisassembling(primary,t);

        if(repairingSecondary)
            onBeginRepairing(secondary,t);
    }
}
void tConstructionBehavior::setRepairingSecondary(bool _repairing, tWorldTime t)
{
    if(!repairingSecondary && _repairing)
    {
        repairingSecondary=true;
        if(!workingOnPrimary)
            onBeginRepairing(secondary,t);
    }
    else if(repairingSecondary && !_repairing)
    {
        repairingSecondary=false;
        if(!workingOnPrimary)
            onEndRepairing(secondary,t);
    }
}
void tConstructionBehavior::onSelect()
{
    if(autoRepair)
        highlightCommand(COMMAND_AUTO_REPAIR);
    else
        highlightCommand(COMMAND_MANUAL_REPAIR);
}
void tConstructionBehavior::onDeselect()
{
    if(autoRepair)
        resetCommand(COMMAND_AUTO_REPAIR);
    else
        resetCommand(COMMAND_MANUAL_REPAIR);
}
void tConstructionBehavior::onTransfer(int newOwner, tWorldTime t)
{
    //Temporarily suspend repairing and/or disassembling so that the resource
    //consumption will transfer to the other player.
    setWorkingOnPrimary(false,t);
    setRepairingSecondary(false,t);
}
void tConstructionBehavior::onClearOrders(tWorldTime t)
{
    setWorkingOnPrimary(false,t);
    setRepairingSecondary(false,t);
    setPrimary(NULL,t);
    setSecondary(NULL,t);
    targets.clear();
    setCurCommand(COMMAND_NONE,t);
}
void tConstructionBehavior::onUnitDestroyed(tUnit *u, tWorldTime t)
{
    if(u==primary)
    {
        setWorkingOnPrimary(false,t);
        setPrimary(NULL,t);
    }
    if(u==secondary)
    {
        setRepairingSecondary(false,t);
        setSecondary(NULL,t);
    }
    for(int i=0;i<targets.size(); )
    {
        if(targets[i]==u)
            targets.erase(targets.begin()+i);
        else
            i++;
    }
    if(targets.size()==0)
        setCurCommand(COMMAND_NONE,t);
}
void tConstructionBehavior::setCurCommand(eCommand command, tWorldTime t)
{
    if(curCommand==command)
        return;
    if(curCommand==COMMAND_NONE)
        parent->setCurCommand(command,t);
    else if(command==COMMAND_NONE)
        parent->resetCurCommand(t);
    else
    {
        bug("tConstructionBehavior::setCurCommand: already processing a different command");
        return;
    }
    curCommand=command;
}
void tConstructionBehavior::setAutoRepair(bool _autoRepair)
{
    if(!autoRepair && _autoRepair)
    {
        autoRepair=true;
        if(parent->areCommandsVisible())
        {
            resetCommand(COMMAND_MANUAL_REPAIR);
            highlightCommand(COMMAND_AUTO_REPAIR);
        }
    }
    else if(autoRepair && !_autoRepair)
    {
        autoRepair=false;
        if(parent->areCommandsVisible())
        {
            resetCommand(COMMAND_AUTO_REPAIR);
            highlightCommand(COMMAND_MANUAL_REPAIR);
        }
    }
}
bool tConstructionBehavior::isUnitFriendly(tUnit *u)
{
    if(u)
        return getAlliance(parent->getOwner(),u->getOwner())<=ALLIANCE_ALLY;
    else
        return false;
}
ePriority tConstructionBehavior::getLeader(tUnit*& u, float& distance, bool& overshoot)
{
    if(primary)
    {
        u=primary;
        distance=type->range*.75; //Fudge factor
        overshoot=false;
        return PRIORITY_PRIMARY;
    }
    else if(secondary)
    {
        u=secondary;
        distance=type->range*.75; //Fudge factor
        overshoot=false;
        return PRIORITY_SECONDARY;
    }
    return PRIORITY_NONE;
}
void tConstructionBehavior::postDraw()
{
    int i;
    tVector3D a,b,c;
    tUnit *u;
    float color[4]={0,0,0,0};

    if(workingOnPrimary && curCommand==COMMAND_REPAIR && repairRate>0)
    {
        u=primary;
        color[0]=1;
    }
    else if(workingOnPrimary && curCommand==COMMAND_DISASSEMBLE && disassembleRate>0)
    {
        u=primary;
        color[2]=1;
    }
    else if(repairingSecondary && repairRate>0)
    {
        u=secondary;
        color[0]=1;
    }
    else
        return;

    u->getRepairPoints(b,c);

    glEnable(GL_BLEND);
    glBegin(GL_TRIANGLES);

    for(i=0;i<type->repairOriginTypes.size();i++)
    {
        a=parent->locatePoint(type->repairOriginTypes[i]);

        color[3]=1;
        glColor4fv(color);
        glVertex3fv((float *)&a);

        color[3]=0;
        glColor4fv(color);
        glVertex3fv((float *)&b);
        glVertex3fv((float *)&c);
    }

    glEnd();
    glDisable(GL_BLEND);
}
void tConstructionBehavior::setPrimary(tUnit *_primary, tWorldTime t)
{
    if(secondary==NULL)
    {
        if(primary==NULL && _primary)
            RUN_PROCEDURE(type->onEngageProc)
        else if(primary && _primary==NULL)
        {
            setAiming(false,primary,t);
            RUN_PROCEDURE(type->onDisengageProc)
        }
    }
    primary=_primary;
}
void tConstructionBehavior::setSecondary(tUnit *_secondary, tWorldTime t)
{
    if(primary==NULL)
    {
        if(secondary==NULL && _secondary)
            RUN_PROCEDURE(type->onEngageProc)
        else if(secondary && _secondary==NULL)
        {
            setAiming(false,secondary,t);
            RUN_PROCEDURE(type->onDisengageProc)
        }
    }
    secondary=_secondary;
}
void tConstructionBehavior::setRepairRate(tUnit *u, float _repairRate, tWorldTime t)
{
    int i;
#ifdef DEBUG
    if(MEMORY_VIOLATION(u))
        return;
#endif
    if(repairRate!=_repairRate)
    {
        u->adjustHealRate(parent,-u->getBuildRate()*repairRate,t);
        for(i=0;i<MAX_RESOURCES;i++)
            adjustCost(parent->getOwner(),i,-u->getCost(i)*repairRate);

        repairRate=_repairRate;

        u->adjustHealRate(parent,u->getBuildRate()*repairRate,t);
        for(i=0;i<MAX_RESOURCES;i++)
            adjustCost(parent->getOwner(),i,u->getCost(i)*repairRate);
    }
}
void tConstructionBehavior::setDisassembleRate(tUnit *u, float _disassembleRate, tWorldTime t)
{
    int i;
#ifdef DEBUG
    if(MEMORY_VIOLATION(u))
        return;
#endif
    if(disassembleRate!=_disassembleRate)
    {
        u->adjustHealRate(parent,u->getBuildRate()*disassembleRate,t);
        for(i=0;i<MAX_RESOURCES;i++)
            adjustRevenue(parent->getOwner(),i,-u->getCost(i)*disassembleRate);

        disassembleRate=_disassembleRate;

        u->adjustHealRate(parent,-u->getBuildRate()*disassembleRate,t);
        for(i=0;i<MAX_RESOURCES;i++)
            adjustRevenue(parent->getOwner(),i,u->getCost(i)*disassembleRate);
    }
}
bool tConstructionBehavior::withinAngle(tUnit *u, const tVector3D& origin, const tVector3D& direction)
{
    if(type->limitAngle)
    {
        tVector3D offset=(u->getCenter()-origin).normalize();
        return ((direction.dot(offset))>=type->cosTheta);
    }
    return true;
}
void tConstructionBehavior::setAiming(bool _aiming, tUnit *u, tWorldTime t)
{
    if(_aiming)
    {
        if(type->aimProcedureType)
            parent->runAimProcedure(type->aimProcedureType,u->getCenter(),t);
    }
    else if(aiming && !_aiming)
    {
        if(type->aimProcedureType)
            parent->stopAimProcedure(type->aimProcedureType,t);
    }
    aiming=_aiming;
}
/****************************************************************************/
// DIVISION BEHAVIOR
/****************************************************************************/
tDivisionType::tDivisionType(tUnitType *parent, tStream& s) throw(tString) : resourceConsumed(-1),
	consumptionRate(0), onInitiateDivisionProc(NULL), onTerminateDivisionProc(NULL),
    onBeginDivisionProc(NULL), onEndDivisionProc(NULL), divisionCompleteArg(true,BOUNDARY_NONE),
    spawn1BasisType(NULL), spawn2BasisType(NULL)
{
    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");
        
			if(strPrefix(str,"resource_consumed"))
				resourceConsumed=parseResource("resource consumed",s);
			else if(strPrefix(str,"consumption_rate"))
				consumptionRate=parseFloatMin("consumption rate",0,s);
			else if(strPrefix(str,"division_complete"))
				PARSE_ARGUMENT(divisionCompleteArg)
			else if(strPrefix(str,"on_initiate_division"))
				PARSE_PROCEDURE(onInitiateDivisionProc)
			else if(strPrefix(str,"on_terminate_division"))
				PARSE_PROCEDURE(onTerminateDivisionProc)
			else if(strPrefix(str,"on_begin_division") || strPrefix(str,"on_start_division"))
				PARSE_PROCEDURE(onBeginDivisionProc)
			else if(strPrefix(str,"on_end_division") || strPrefix(str,"on_stop_division"))
				PARSE_PROCEDURE(onEndDivisionProc)
			else if(strPrefix(str,"spawn1_reference_frame"))
			{
				validateModelType(parent->modelType);
				spawn1BasisType=parent->modelType->parseReferenceFrameType("reference frame name",s);
			}
			else if(strPrefix(str,"spawn2_reference_frame"))
			{
				validateModelType(parent->modelType);
				spawn2BasisType=parent->modelType->parseReferenceFrameType("reference frame name",s);
			}
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}
	if(consumptionRate>0 && resourceConsumed==-1)
		throw tString("Please specify the resource consumed");
}
tBehavior *tDivisionType::createBehavior(tUnit *parent)
{
    return mwNew tDivisionBehavior(this, parent);
}
void tDivisionBehavior::setDividing(bool _dividing, tWorldTime t)
{
    if(_dividing && !dividing)
    {
        dividing=true;
		if(type->resourceConsumed>=0)
			adjustCost(parent->getOwner(),type->resourceConsumed,type->consumptionRate);
        RUN_PROCEDURE(type->onBeginDivisionProc)
    }
    else if(!_dividing && dividing)
    {
        dividing=false;
		if(type->resourceConsumed>=0)
			adjustCost(parent->getOwner(),type->resourceConsumed,-type->consumptionRate);
        RUN_PROCEDURE(type->onEndDivisionProc)
    }
}
void tDivisionBehavior::initiateDivision(tWorldTime t)
{
    if(!divisionStarted)
    {
        if(parent->areCommandsVisible())
        {
            disableCommand(COMMAND_DIVIDE);
            enableCommand(COMMAND_CANCEL_DIVIDE);
        }
        divisionStarted=true;
        RUN_PROCEDURE(type->onInitiateDivisionProc)
    }
}
void tDivisionBehavior::terminateDivision(tWorldTime t)
{
    if(divisionStarted)
    {
        if(parent->areCommandsVisible())
        {
            disableCommand(COMMAND_CANCEL_DIVIDE);
            enableCommand(COMMAND_DIVIDE);
        }
        divisionStarted=false;
        setDividing(false, t);
        RUN_PROCEDURE(type->onTerminateDivisionProc)
    }
}
tDivisionBehavior::tDivisionBehavior(tDivisionType *_type, tUnit *parent) : tBehavior(parent),
    type(_type), divisionStarted(false), dividing(false)
{
}
void tDivisionBehavior::intervalTick(tWorldTime t)
{
    if(divisionStarted && EVALUATE_ARGUMENT(divisionCompleteArg)==true)
    {
        tVector2D spawn1Origin=parent->getPosition();
        tVector2D spawn1Forward=iHat2D;
        tVector2D spawn2Origin=parent->getPosition();
        tVector2D spawn2Forward=iHat2D;
        tVector3D p,f,s,up;
        if(type->spawn1BasisType)
        {
            parent->locateReferenceFrame(type->spawn1BasisType,p,f,s,up);
            spawn1Origin=to2D(p);
            spawn1Forward=to2D(f);
        }
        if(type->spawn2BasisType)
        {
            parent->locateReferenceFrame(type->spawn2BasisType,p,f,s,up);
            spawn2Origin=to2D(p);
            spawn2Forward=to2D(f);
        }

        terminateDivision(t);

        tUnit *u=createUnit(parent->getOwner(), parent->getType(), spawn1Origin, spawn1Forward, t);
        if(u)
        {
            u->setHP(parent->getMaxHP(),t); //Complete the unit's construction phase
            u->setHP(parent->getHP(),t);
            u->setVelocity(parent->getVelocity(),t);
            u->setForward(parent->getForward(),t);
            u->setMovementMode(parent->getMovementMode(),t);
            u->setGroup(parent->getGroup());

            if(parent->isSelected())
                selectUnit(u);                                        
        }
		parent->setPosition(spawn2Origin, t);
		parent->setForward(spawn2Forward, t);
    }
    else
        setDividing(divisionStarted && (type->resourceConsumed==-1 ||
			isResourceAvailable(parent->getOwner(), type->resourceConsumed)), t);
}
void tDivisionBehavior::onDeath(tWorldTime t)
{
    terminateDivision(t);
}
void tDivisionBehavior::onTransfer(int newOwner, tWorldTime t)
{
	setDividing(false,t);
}
bool tDivisionBehavior::canExecute(eCommand command)
{
    return (parent->isBuilt() &&
        (command==COMMAND_DIVIDE && !divisionStarted ||
        command==COMMAND_CANCEL_DIVIDE && divisionStarted));
}
void tDivisionBehavior::onCommand(eCommand command, tWorldTime t)
{
    if(command==COMMAND_DIVIDE)
    {
        if(parent->isBuilt() && !divisionStarted)
            initiateDivision(t);
    }
    else if(command==COMMAND_CANCEL_DIVIDE)
    {
        if(parent->isBuilt() && divisionStarted)
            terminateDivision(t);
    }
}
/****************************************************************************/
// EXTRACTION BEHAVIOR
/****************************************************************************/
tExtractionType::tExtractionType(tUnitType *parent, tStream& s) throw(tString) :
    resourceExtracted(-1), resourceConsumed(-1), onBeginExtractionProc(NULL),
    onEndExtractionProc(NULL), consumptionRateArg(0,BOUNDARY_MIN,0),
	extractionRateArg(0,BOUNDARY_MIN,0), onInitiateExtractionProc(NULL),
    onTerminateExtractionProc(NULL)
{
    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");
        
			if(strPrefix(str,"resource_extracted"))
				resourceExtracted=parseResource("resource extracted",s);
			else if(strPrefix(str,"extraction_rate"))
				PARSE_ARGUMENT(extractionRateArg)
			else if(strPrefix(str,"resource_consumed"))
				resourceConsumed=parseResource("resource consumed",s);
			else if(strPrefix(str,"consumption_rate"))
				PARSE_ARGUMENT(consumptionRateArg)
			else if(strPrefix(str,"on_begin_extraction") || strPrefix(str,"on_start_extraction"))
				PARSE_PROCEDURE(onBeginExtractionProc)
			else if(strPrefix(str,"on_end_extraction") || strPrefix(str,"on_stop_extraction"))
				PARSE_PROCEDURE(onEndExtractionProc)
			else if(strPrefix(str,"on_initiate_extraction"))
				PARSE_PROCEDURE(onInitiateExtractionProc)
			else if(strPrefix(str,"on_terminate_extraction"))
				PARSE_PROCEDURE(onTerminateExtractionProc)
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}
	if(resourceExtracted==-1)
		throw tString("Please specify the resource extracted");
	if(consumptionRateArg.loaded && resourceConsumed==-1)
		throw tString("Please specify the resource consumed");
}
tBehavior *tExtractionType::createBehavior(tUnit *parent)
{
    return mwNew tExtractionBehavior(this, parent);
}
void tExtractionBehavior::setRunning(bool _running, tWorldTime t)
{
    if(_running && !running)
    {
        running=true;
        RUN_PROCEDURE(type->onBeginExtractionProc)
    }
    else if(!_running && running)
    {
        running=false;
        setConsumptionRate(0);
        setExtractionRate(0);
        RUN_PROCEDURE(type->onEndExtractionProc)
    }
}
void tExtractionBehavior::linkDeposit(tWorldTime t)
{
    setRunning(false,t);
    if(canExtract())
        deposit=findDepositAt(parent->getPosition(), type->resourceExtracted);
}
void tExtractionBehavior::unlinkDeposit(tWorldTime t)
{
    setRunning(false,t);
    deposit=NULL;
}
bool tExtractionBehavior::canExecute(eCommand command)
{
    return (command==COMMAND_COLLECTION_ON || command==COMMAND_COLLECTION_OFF);
}
void tExtractionBehavior::onCommand(eCommand command, tWorldTime t)
{
    if(command==COMMAND_COLLECTION_ON)
    {
        if(!turnedOn)
        {
            if(parent->areCommandsVisible())
            {
                resetCommand(COMMAND_COLLECTION_OFF);
                highlightCommand(COMMAND_COLLECTION_ON);
            }
            turnedOn=true;
            linkDeposit(t);
            RUN_PROCEDURE(type->onInitiateExtractionProc)
        }
    }
    else if(command==COMMAND_COLLECTION_OFF)
    {
        if(turnedOn)
        {
            if(parent->areCommandsVisible())
            {
                resetCommand(COMMAND_COLLECTION_ON);
                highlightCommand(COMMAND_COLLECTION_OFF);
            }
            turnedOn=false;
            unlinkDeposit(t);
            RUN_PROCEDURE(type->onTerminateExtractionProc)
        }
    }
}
void tExtractionBehavior::intervalTick(tWorldTime t)
{
    if(deposit)
    {
        setRunning(canExtract(deposit),t);
        if(!running)
            deposit=NULL;
    }
    if(deposit==NULL)
        linkDeposit(t);

    if(running)
    {
        setConsumptionRate(EVALUATE_ARGUMENT(consumptionRateArg));
        setExtractionRate(EVALUATE_ARGUMENT(extractionRateArg));
    }
}
void tExtractionBehavior::onBirth(tWorldTime t)
{
    linkDeposit(t);
}
void tExtractionBehavior::onDeath(tWorldTime t)
{
    unlinkDeposit(t);
}
void tExtractionBehavior::onTransfer(int newOwner, tWorldTime t)
{
    //Temporarily stop extraction so that resources transfer to new player properly.
	//Unit remains linked to the same deposit.
	setRunning(false,t);
}
void tExtractionBehavior::onDepositDepleted(tDeposit *d, tWorldTime t)
{
    unlinkDeposit(t);
}
void tExtractionBehavior::onSelect()
{
    if(turnedOn)
        highlightCommand(COMMAND_COLLECTION_ON);
    else
        highlightCommand(COMMAND_COLLECTION_OFF);
}
void tExtractionBehavior::onDeselect()
{
    if(turnedOn)
        resetCommand(COMMAND_COLLECTION_ON);
    else
        resetCommand(COMMAND_COLLECTION_OFF);
}
bool tExtractionBehavior::canExtract()
{
    if(!turnedOn)
        return false;
    if(!parent->isBuilt())
        return false;
    if(type->resourceConsumed>=0 &&
	   !isResourceAvailable(parent->getOwner(),type->resourceConsumed))
        return false;
    if(!canCollectResource(parent->getOwner(),type->resourceExtracted))
        return false;

    return true;
}
bool tExtractionBehavior::canExtract(tDeposit *d)
{
    if(!canExtract())
        return false;
    return isWithinRange(d,parent->getPosition());
}
void tExtractionBehavior::setConsumptionRate(float _consumptionRate)
{
    if(_consumptionRate!=consumptionRate)
    {
        //This method (though inefficient) hopefully prevents float-point errors
		if(type->resourceConsumed>=0)
			adjustCost(parent->getOwner(),type->resourceConsumed,-consumptionRate);
        consumptionRate=_consumptionRate;
		if(type->resourceConsumed>=0)
			adjustCost(parent->getOwner(),type->resourceConsumed,consumptionRate);
    }
}
void tExtractionBehavior::setExtractionRate(float _extractionRate)
{
    if(_extractionRate!=extractionRate)
    {
        //This method (though inefficient) hopefully prevents float-point errors
        adjustRevenue(parent->getOwner(),type->resourceExtracted,-extractionRate);
        adjustExtractionRate(deposit,-extractionRate);
        extractionRate=_extractionRate;
        adjustRevenue(parent->getOwner(),type->resourceExtracted,extractionRate);
        adjustExtractionRate(deposit,extractionRate);
    }
}
/****************************************************************************/
// PRODUCTION BEHAVIOR
/****************************************************************************/
tProductionType::tProductionType(tUnitType *parent, tStream& s) throw(tString) :
    productionRateArg(1,BOUNDARY_MIN,0), onInitiateProductionProc(NULL),
    onCompleteProductionProc(NULL), onCancelProductionProc(NULL), onBeginProductionProc(NULL),
	onEndProductionProc(NULL), spawnBasisType(NULL), readyForNextUnitArg(true,BOUNDARY_NONE)
{
	tModelVectorType *mvt;

    for(int i=0;i<MAX_COMMANDS;i++)
        productionOptions[i]=false;

    productionOptions[COMMAND_PRODUCTION_ON]=true;
    productionOptions[COMMAND_PRODUCTION_OFF]=true;
    productionOptions[COMMAND_CANCEL_PRODUCTION]=true;
    
    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");
        
			if(strPrefix(str,"origins") || strPrefix(str,"repair_origins"))
			{
				do
				{
					PARSE_MODEL_VECTOR(mvt);
					if(mvt)
						repairOriginTypes.push_back(mvt);
				}
			    while(s.matchOptionalToken(","));
			}
			else if(strPrefix(str,"build_options") || strPrefix(str,"commands"))
				parseBuildOptions(names, s);
			else if(strPrefix(str,"production_rate"))
				PARSE_ARGUMENT(productionRateArg)
			else if(strPrefix(str,"on_initiate_production"))
				PARSE_PROCEDURE(onInitiateProductionProc)
			else if(strPrefix(str,"on_complete_production"))
				PARSE_PROCEDURE(onCompleteProductionProc)
			else if(strPrefix(str,"on_cancel_production"))
				PARSE_PROCEDURE(onCancelProductionProc)
			else if(strPrefix(str,"on_begin_production") || strPrefix(str,"on_start_production"))
				PARSE_PROCEDURE(onBeginProductionProc)
			else if(strPrefix(str,"on_end_production") || strPrefix(str,"on_stop_production"))
				PARSE_PROCEDURE(onEndProductionProc)
			else if(strPrefix(str,"spawn_reference_frame"))
			{
				validateModelType(parent->modelType);
				spawnBasisType=parent->modelType->parseReferenceFrameType("reference frame name",s);
			}
			else if(strPrefix(str,"ready_for_next_unit"))
				PARSE_ARGUMENT(readyForNextUnitArg)
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

    if(spawnBasisType==NULL)
        throw tString("Please specify the reference frame in which to spawn units");
}
tBehavior *tProductionType::createBehavior(tUnit *parent)
{
    return mwNew tProductionBehavior(this, parent);
}
void tProductionType::lookupUnitTypes() throw(tString)
{
    convertBuildOptions(names, productionOptions, PARAM_PRODUCTION);
    names.clear(); //No need to save the names any longer
}

bool tProductionBehavior::canExecute(eCommand command)
{
#ifdef DEBUG
    if(command<0 || command>=commandCount)
    {
        bug("tProductionBehavior::canExecute: invalid command");
        return false;
    }
#endif
    return type->productionOptions[command];
}
void tProductionBehavior::onCommand(eCommand command, tWorldTime t)
{
    if(command==COMMAND_CANCEL_PRODUCTION)
    {
        cancelProduction(t); //this function may invoke onProductDestroyed.
        clearProductionQueue(t);
    }
    else if(command==COMMAND_PRODUCTION_ON)
    {
        if(productionPaused)
        {
            if(parent->areCommandsVisible())
            {
                resetCommand(COMMAND_PRODUCTION_OFF);
                highlightCommand(COMMAND_PRODUCTION_ON);
            }
            productionPaused=false;
            parent->enableAbility(ABILITY_PRODUCE);
        }
    }
    else if(command==COMMAND_PRODUCTION_OFF)
    {
        if(!productionPaused)
        {
            if(parent->areCommandsVisible())
            {
                resetCommand(COMMAND_PRODUCTION_ON);
                highlightCommand(COMMAND_PRODUCTION_OFF);
            }
            productionPaused=true;
            parent->disableAbility(ABILITY_PRODUCE);
        }
    }
}
void tProductionBehavior::onCommand(eCommand command, int quantity, bool repeat, tWorldTime t)
{
    int i,q;

    if(!type->productionOptions[command])
        return;
        
    //clear continuous orders:
    if(productionQueue.size()>0 && productionQueue.front().repeat)
        clearProductionQueue(t);

    if(repeat)
    {
        clearProductionQueue(t);
        tProduct p={command, 0, true};
        productionQueue.push_back(p);
        if(parent->areCommandsVisible())
            adjustCommandRepeat(command,1);
    }
    else if(quantity>0)
    {
        if(productionQueue.size()==0 || productionQueue.back().command!=command)
        {
            tProduct p={command, quantity, false};
            productionQueue.push_back(p);
        }
        else
            productionQueue.back().quantity+=quantity;
        if(parent->areCommandsVisible())
            adjustCommandQuantity(command,quantity);
    }
    else if(quantity<0)
    {
        q=-quantity;
        for(i=0; i<productionQueue.size(); )
        {
            if(productionQueue[i].command==command)
            {
                if(q>=productionQueue[i].quantity)
                {
                    q-=productionQueue[i].quantity;
                    if(parent->areCommandsVisible())
                        adjustCommandQuantity(command,-productionQueue[i].quantity);
                    productionQueue.erase(productionQueue.begin()+i);

                    if(q==0)
                        break;
                }
                else
                {
                    productionQueue[i].quantity-=q;
                    if(parent->areCommandsVisible())
                        adjustCommandQuantity(command,-q);
                    break;
                }
            }
            else
                i++;
        }
    }

    /* If you're not producting anything, or you're not producing the correct
     * type of unit (e.g. the user cancelled the build order), then abort
     * production and load the next build order.
     */
    eCommand nextProduct=COMMAND_NONE;
    if(productionQueue.size()>0)
        nextProduct=productionQueue.front().command;

    if(curProduct!=nextProduct)
    {
        if(curProduct!=COMMAND_NONE)
            cancelProduction(t); //This function may invoke onProductDestroyed.
        if(nextProduct!=COMMAND_NONE)
            loadNextProduct(t);
    }
}

void tProductionBehavior::onSelect()
{
    int i;
    //Show production queue:
    for(i=0;i<productionQueue.size();i++)
    {
        if(productionQueue[i].repeat)
            adjustCommandRepeat(productionQueue[i].command,1);
        else
            adjustCommandQuantity(productionQueue[i].command,productionQueue[i].quantity);
    }

    if(curProduct!=COMMAND_NONE)
        highlightCommand(curProduct);

    if(productionPaused)
        highlightCommand(COMMAND_PRODUCTION_OFF);
    else
        highlightCommand(COMMAND_PRODUCTION_ON);
}
void tProductionBehavior::onDeselect()
{
    int i;
    if(curProduct!=COMMAND_NONE)
        resetCommand(curProduct);

    if(productionPaused)
        resetCommand(COMMAND_PRODUCTION_OFF);
    else
        resetCommand(COMMAND_PRODUCTION_ON);

    //Hide production queue:
    for(i=0;i<productionQueue.size();i++)
    {
        if(productionQueue[i].repeat)
            adjustCommandRepeat(productionQueue[i].command,-1);
        else
            adjustCommandQuantity(productionQueue[i].command,-productionQueue[i].quantity);
    }
}
void tProductionBehavior::onBeginProduction(eCommand command, tWorldTime t)
//precondition: product may be NULL
{
    tUnitType *ut;
    if(product==NULL)
    {
        ut=unitTypeLookup(commandTable[command].unitID);
#ifdef DEBUG
        if(ut==NULL)
        {
            bug("tProductionBehavior::onBeginProduction: unknown unit type");
            return;
        }
#endif                                                
		tUnit *u=createUnit(parent->getOwner(), ut, parent->getPosition(),
							parent->getForward(), t);
		u->engageTractorBeam(parent,type->spawnBasisType,zeroVector3D);
        product=u;
		RUN_PROCEDURE(type->onInitiateProductionProc)
    }
    if(product)
        RUN_PROCEDURE(type->onBeginProductionProc)
}
void tProductionBehavior::onEndProduction(eCommand command, tWorldTime t)
//precondition: product may be NULL
{
    if(product)
    {
        setProductionRate(0, t);
        RUN_PROCEDURE(type->onEndProductionProc)
    }
}
void tProductionBehavior::cancelProduction(tWorldTime t)
//precondition: product may be NULL
{
    if(product)
    {
        if(product->getMaxHP()>0)
            for(int i=0;i<MAX_RESOURCES;i++)
                adjustResource(parent->getOwner(),i,product->getCost(i)*product->getHP()/product->getBuildRate());
        destroyUnit(product,t);

        //This function will automatically invoke onTargetDestroyed which will clear product.
    }

    setCurProduct(COMMAND_NONE,t);
}
void tProductionBehavior::intervalTick(tWorldTime t)
{
	if(product)
	{	
		if(!productionComplete && product->getHP()==product->getMaxHP())
		{
			productionComplete=true;
			RUN_PROCEDURE(type->onCompleteProductionProc)
		}
		if(productionComplete && EVALUATE_ARGUMENT(readyForNextUnitArg)==true)
		{
			setProducing(false, t);
			//Break the unit's umbilical cord:
			product->disengageTractorBeam();
			product=NULL;
			productionComplete=false;
			loadNextProduct(t);
		}
	}

	/***************
     Unit Production
     ***************/
    if(curProduct!=COMMAND_NONE)
        setProducing(canProduce(curProduct), t);
    if(producing && product)
        setProductionRate(EVALUATE_ARGUMENT(productionRateArg), t);
}
bool tProductionBehavior::canProduce(eCommand command)
{
    if(!parent->isAbilityEnabled(ABILITY_PRODUCE))
        return false;
	if(productionComplete)
		return false;

#ifdef DEBUG
    if(parent->getOwner()<0 || parent->getOwner()>=MAX_PLAYERS)
    {
        bug("tProductionBehavior::canProduce: invalid owner");
        return false;
    }
#endif
    tUnitType *ut = unitTypeLookup(commandTable[command].unitID);
    if(ut)
        return ut->repairable[parent->getOwner()];
    return false;
}
void tProductionBehavior::onDeath(tWorldTime t)
{
    if(product)
        product->kill(t);
}
void tProductionBehavior::onTransfer(int newOwner, tWorldTime t)
{
    //Stop production so that resource consumption will transfer over properly.
    setProducing(false,t);
    if(product)
        product->setOwner(newOwner,t);
}
void tProductionBehavior::onUnitDestroyed(tUnit *u, tWorldTime t)
//precondition: unit still exists
{
    if(u==product)
    {
        setProducing(false,t);
        product=NULL;
		RUN_PROCEDURE(type->onCancelProductionProc)

        //curProduct remains set so that the next call to onBeginProduction will instantiate
        //a new unit object.
    }
}
void tProductionBehavior::dumpCommands()
{
    int i;
    char buf[BUFFER_SIZE];
    tBufferIterator ptr(buf,sizeof(buf));

    try
    {
        mySprintf(ptr, "-Production Queue for Unit %ld-", parent->getID());
        for(i=0;i<productionQueue.size();i++)
            mySprintf(ptr, "\n    Command: %s    Count: %d    Repeat: %s",
                CHARPTR(commandTable[productionQueue[i].command].name),
                productionQueue[i].quantity,
                productionQueue[i].repeat?"yes":"no");
    }
    catch(int x)
    {
        bug("tProductionBehavior::dumpCommands: buffer overflow");
    }
    log(buf);
}
void tProductionBehavior::setCurProduct(eCommand command, tWorldTime t)
{
    if(command==curProduct)
        return;
    if(parent->areCommandsVisible() && curProduct!=COMMAND_NONE)
        resetCommand(curProduct);
    curProduct=command;
    if(parent->areCommandsVisible() && curProduct!=COMMAND_NONE)
        highlightCommand(curProduct);
}
void tProductionBehavior::clearProductionQueue(tWorldTime t)
{
    int i;
    if(parent->areCommandsVisible())
        for(i=0;i<productionQueue.size();i++)
        {
            if(productionQueue[i].repeat)
                adjustCommandRepeat(productionQueue[i].command,-1);
            else
                adjustCommandQuantity(productionQueue[i].command,-productionQueue[i].quantity);
        }
    productionQueue.clear();
}
void tProductionBehavior::loadNextProduct(tWorldTime t)
{
    //Erase the production order AFTER we complete it
    if(curProduct!=COMMAND_NONE)
    {
#ifdef DEBUG
        if(productionQueue.size()==0)
        {
            bug("loadNextProduct: production queue is empty");
            return;
        }
#endif
        if(!productionQueue.front().repeat)
        {
#ifdef DEBUG
            if(productionQueue.front().quantity<1)
            {
                bug("loadNextProduct: empty production order");
                return;
            }
#endif
            productionQueue.front().quantity--;
            if(parent->areCommandsVisible())
                adjustCommandQuantity(productionQueue.front().command,-1);
            if(productionQueue.front().quantity<=0)
                productionQueue.erase(productionQueue.begin());
        }
        setCurProduct(COMMAND_NONE,t);
    }

    while(productionQueue.size()>0)
    {
        onProduce(productionQueue.front().command,t);

        if(curProduct!=COMMAND_NONE || productionQueue.front().repeat)
            return;

        //If the first unit failed, then all units will fail so delete this
        //production order.
        if(parent->areCommandsVisible())
            adjustCommandQuantity(productionQueue.front().command,-productionQueue.front().quantity);
        productionQueue.erase(productionQueue.begin());
    }
}
void tProductionBehavior::onProduce(eCommand command, tWorldTime t)
{
    setCurProduct(command,t);
}
void tProductionBehavior::setProducing(bool _producing, tWorldTime t)
{
    if(!producing && _producing)
    {
        producing=true;
        onBeginProduction(curProduct,t);
    }
    else if(producing && !_producing)
    {
        producing=false;
        onEndProduction(curProduct,t);
    }
}
void tProductionBehavior::postDraw()
{
    int i;
    tVector3D a,b,c;
    if(product && producing && productionRate>0)
    {
        product->getRepairPoints(b,c);

        glEnable(GL_BLEND);
        glBegin(GL_TRIANGLES);

        for(i=0;i<type->repairOriginTypes.size();i++)
        {
            a=parent->locatePoint(type->repairOriginTypes[i]);

            glColor4f(1,0,0,1);
            glVertex3fv((float *)&a);

            glColor4f(1,0,0,0);
            glVertex3fv((float *)&b);
            glVertex3fv((float *)&c);
        }

        glEnd();
        glDisable(GL_BLEND);
    }
}
void tProductionBehavior::setProductionRate(float _productionRate, tWorldTime t)
{
    int i;
    if(_productionRate!=productionRate)
    {
        product->adjustHealRate(parent,-product->getBuildRate()*productionRate,t);
        for(i=0;i<MAX_RESOURCES;i++)
            adjustCost(parent->getOwner(),i,-product->getCost(i)*productionRate);

        productionRate=_productionRate;

        product->adjustHealRate(parent,product->getBuildRate()*productionRate,t);
        for(i=0;i<MAX_RESOURCES;i++)
            adjustCost(parent->getOwner(),i,product->getCost(i)*productionRate);
    }
}
/****************************************************************************/
// GENERIC BEHAVIOR
/****************************************************************************/
tGenericType::tGenericType(tUnitType *parent, tStream& s) throw(tString) :
    onStartProc(NULL), onStopProc(NULL), startCommand(COMMAND_NONE), stopCommand(COMMAND_NONE)
{
    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");
        
			if(strPrefix(str,"on_start"))
				PARSE_PROCEDURE(onStartProc)
			else if(strPrefix(str,"on_stop"))
				PARSE_PROCEDURE(onStopProc)
			else if(strPrefix(str,"start_command"))
				startCommand=parseImmediateCommand("command name",s);
			else if(strPrefix(str,"stop_command"))
				stopCommand=parseImmediateCommand("command name",s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}
	if(startCommand==COMMAND_NONE || stopCommand==COMMAND_NONE)
		throw tString("Please specify a start_command and stop_command for this behavior");
}
tBehavior *tGenericType::createBehavior(tUnit *parent)
{
    return mwNew tGenericBehavior(this, parent);
}
bool tGenericBehavior::canExecute(eCommand command)
{
    return (parent->isBuilt() && (command==type->startCommand && canStart ||
        command==type->stopCommand && canStop));
}
void tGenericBehavior::onCommand(eCommand command, tWorldTime t)
{
    if(command==type->startCommand)
    {
        if(parent->isBuilt() && canStart)
        {
            if(parent->areCommandsVisible())
            {
                disableCommand(type->startCommand);
                enableCommand(type->stopCommand);
            }
            canStart=false;
            canStop=true;
            RUN_PROCEDURE(type->onStartProc)
        }
    }
    else if(command==type->stopCommand)
    {
        if(parent->isBuilt() && canStop)
        {
            if(parent->areCommandsVisible())
            {
                disableCommand(type->stopCommand);
                enableCommand(type->startCommand);
            }
            canStop=false;
            canStart=true;
            RUN_PROCEDURE(type->onStopProc)
        }
    }
}
/****************************************************************************/
// CLOAKING BEHAVIOR
/****************************************************************************/
tGenericConsumptionType::tGenericConsumptionType(tUnitType *parent, tStream& s) throw(tString) :
	resourceConsumed(-1), consumptionRateArg(0,BOUNDARY_MIN,0), onStartProc(NULL), onStopProc(NULL),
    onInitiateProc(NULL), onTerminateProc(NULL), startCommand(COMMAND_NONE), stopCommand(COMMAND_NONE)
{
    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");
        
			if(strPrefix(str,"resource_consumed"))
				resourceConsumed=parseResource("resource consumed",s);
			else if(strPrefix(str,"consumption_rate"))
				PARSE_ARGUMENT(consumptionRateArg)
			else if(strPrefix(str,"on_start"))
				PARSE_PROCEDURE(onStartProc)
			else if(strPrefix(str,"on_stop"))
				PARSE_PROCEDURE(onStopProc)
			else if(strPrefix(str,"on_initiate"))
				PARSE_PROCEDURE(onInitiateProc)
			else if(strPrefix(str,"on_terminate"))
				PARSE_PROCEDURE(onTerminateProc)
			else if(strPrefix(str,"start_command"))
				startCommand=parseImmediateCommand("command name",s);
			else if(strPrefix(str,"stop_command"))
				stopCommand=parseImmediateCommand("command name",s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}
	if(consumptionRateArg.loaded && resourceConsumed==-1)
		throw tString("Please specify the resource consumed");
	if(startCommand==COMMAND_NONE || stopCommand==COMMAND_NONE)
		throw tString("Please specify a start_command and stop_command for this behavior");
}
tBehavior *tGenericConsumptionType::createBehavior(tUnit *parent)
{
    return mwNew tGenericConsumptionBehavior(this, parent);
}
tGenericConsumptionBehavior::tGenericConsumptionBehavior(tGenericConsumptionType *_type,
	tUnit *parent) : tBehavior(parent), type(_type), turnedOn(false), consumptionRate(0),
	running(false)
{
}
void tGenericConsumptionBehavior::setRunning(bool _running, tWorldTime t)
{
    if(!running && _running)
        RUN_PROCEDURE(type->onStartProc)
    else if(running && !_running)
    {
        setConsumptionRate(0);
        RUN_PROCEDURE(type->onStopProc)
    }
    running=_running;
}
void tGenericConsumptionBehavior::update(tWorldTime t)
{
    setRunning(turnedOn && parent->isBuilt() && (type->resourceConsumed==-1 ||
        isResourceAvailable(parent->getOwner(),type->resourceConsumed)),t);
}
bool tGenericConsumptionBehavior::canExecute(eCommand command)
{
    return (parent->isBuilt()&&
        (command==type->startCommand && !turnedOn ||
        command==type->stopCommand && turnedOn));
}
void tGenericConsumptionBehavior::onCommand(eCommand command, tWorldTime t)
{
    if(command==type->startCommand)
    {
        if(parent->isBuilt() && !turnedOn)
        {
            turnedOn=true;
            if(parent->areCommandsVisible())
            {
                disableCommand(type->startCommand);
                enableCommand(type->stopCommand);
            }
            RUN_PROCEDURE(type->onInitiateProc)
        }
    }
    else if(command==type->stopCommand)
    {
        if(parent->isBuilt() && turnedOn)
        {
            turnedOn=false;
            if(parent->areCommandsVisible())
            {
                disableCommand(type->stopCommand);
                enableCommand(type->startCommand);
            }
            setRunning(false,t);
            RUN_PROCEDURE(type->onTerminateProc)
        }
    }
}
void tGenericConsumptionBehavior::intervalTick(tWorldTime t)
{
    update(t);
    if(running)
        setConsumptionRate(EVALUATE_ARGUMENT(consumptionRateArg));
}
void tGenericConsumptionBehavior::onDeath(tWorldTime t)
{
    setRunning(false,t);
}
void tGenericConsumptionBehavior::onTransfer(int newOwner, tWorldTime t)
{
    setRunning(false,t);
}
void tGenericConsumptionBehavior::setConsumptionRate(float _consumptionRate)
{
    if(_consumptionRate!=consumptionRate)
    {
		if(type->resourceConsumed>=0)
			adjustCost(parent->getOwner(),type->resourceConsumed,-consumptionRate);
        consumptionRate=_consumptionRate;
		if(type->resourceConsumed>=0)
			adjustCost(parent->getOwner(),type->resourceConsumed,consumptionRate);
    }
}
/****************************************************************************/
// STORAGE BEHAVIOR
/****************************************************************************/
tStorageType::tStorageType(tUnitType *parent, tStream& s) throw(tString) : resourceStored(-1),
    capacity(0), resourceConsumed(-1), consumptionRate(0), onBeginStorageProc(NULL),
	onEndStorageProc(NULL)
{
    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");
        
			if(strPrefix(str,"resource_stored"))
				resourceStored=parseResource("resource stored",s);
			else if(strPrefix(str,"capacity")||strPrefix(str,"amount"))
				capacity=parseFloatMin("capacity",0,s);
			else if(strPrefix(str,"resource_consumed"))
				resourceConsumed=parseResource("resource consumed",s);
			else if(strPrefix(str,"consumption_rate"))
				consumptionRate=parseFloatMin("consumption rate",0,s);
			else if(strPrefix(str,"on_begin_storage") || strPrefix(str,"on_start_storage"))
				PARSE_PROCEDURE(onBeginStorageProc)
			else if(strPrefix(str,"on_end_storage") || strPrefix(str,"on_stop_storage"))
				PARSE_PROCEDURE(onEndStorageProc)
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}
	if(capacity>0 && resourceStored==-1)
		throw tString("Please specify the resource stored");
	if(consumptionRate>0 && resourceConsumed==-1)
		throw tString("Please specify the resource consumed");
}
tBehavior *tStorageType::createBehavior(tUnit *parent)
{
    return mwNew tStorageBehavior(this, parent);
}
void tStorageBehavior::setRunning(bool _running, tWorldTime t)
{
    if(_running && !running)
    {
        running=true;
		if(type->resourceConsumed>=0)
			adjustCost(parent->getOwner(),type->resourceConsumed,type->consumptionRate);
		if(type->resourceStored>=0)
			adjustCapacity(parent->getOwner(), type->resourceStored, type->capacity);
		RUN_PROCEDURE(type->onBeginStorageProc);
    }
    if(!_running && running)
    {
        running=false;
		if(type->resourceConsumed>=0)
			adjustCost(parent->getOwner(),type->resourceConsumed,-type->consumptionRate);
		if(type->resourceStored>=0)
			adjustCapacity(parent->getOwner(), type->resourceStored, -type->capacity);
		RUN_PROCEDURE(type->onEndStorageProc);
    }
}
void tStorageBehavior::update(tWorldTime t)
{
    setRunning(parent->isBuilt() && (type->resourceConsumed==-1 ||
		isResourceAvailable(parent->getOwner(), type->resourceConsumed)),t);
}
void tStorageBehavior::intervalTick(tWorldTime t)
{
    update(t);
}
void tStorageBehavior::onBirth(tWorldTime t)
{
    update(t);
}
void tStorageBehavior::onDeath(tWorldTime t)
{
    setRunning(false,t);
}
void tStorageBehavior::onTransfer(int newOwner, tWorldTime t)
{
	setRunning(false,t);
}
/****************************************************************************/
// DETECTION BEHAVIOR
/****************************************************************************/
tDetectionType::tDetectionType(tUnitType *parent, tStream& s) throw(tString) :
    resourceConsumed(-1), consumptionRateArg(0,BOUNDARY_MIN,0),onInitiateDetectionProc(NULL),
    onTerminateDetectionProc(NULL),onBeginDetectionProc(NULL),onEndDetectionProc(NULL),
    fieldOfViewArg(0,BOUNDARY_MIN,0)
{
    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");
        
			if(strPrefix(str,"resource_consumed"))
				resourceConsumed=parseResource("resource consumed",s);
			else if(strPrefix(str,"consumption_rate"))
				PARSE_ARGUMENT(consumptionRateArg)
			else if(strPrefix(str,"field_of_view"))
				PARSE_ARGUMENT(fieldOfViewArg)
			else if(strPrefix(str,"on_initiate_detection"))
				PARSE_PROCEDURE(onInitiateDetectionProc)
			else if(strPrefix(str,"on_terminate_detection"))
				PARSE_PROCEDURE(onTerminateDetectionProc)
			else if(strPrefix(str,"on_begin_detection") || strPrefix(str,"on_start_detection"))
				PARSE_PROCEDURE(onBeginDetectionProc)
			else if(strPrefix(str,"on_end_detection") || strPrefix(str,"on_stop_detection"))
				PARSE_PROCEDURE(onEndDetectionProc)
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}
	if(consumptionRateArg.loaded && resourceConsumed==-1)
		throw tString("Please specify the resource consumed");
}
tBehavior *tDetectionType::createBehavior(tUnit *parent)
{
    return mwNew tDetectionBehavior(this, parent);
}
void tDetectionBehavior::setRunning(bool _running, tWorldTime t)
{
    if(_running && !running)
    {
        running=true;
        RUN_PROCEDURE(type->onBeginDetectionProc)
    }
    else if(!_running && running)
    {
        running=false;
        setConsumptionRate(0);
        parent->setFieldOfView(0);
        RUN_PROCEDURE(type->onEndDetectionProc)
    }
}
void tDetectionBehavior::update(tWorldTime t)
{
    setRunning(turnedOn && parent->isBuilt() && (type->resourceConsumed==-1 ||
		isResourceAvailable(parent->getOwner(),type->resourceConsumed)),t);
}
bool tDetectionBehavior::canExecute(eCommand command)
{
    return(command==COMMAND_DETECTION_ON || command==COMMAND_DETECTION_OFF);
}
void tDetectionBehavior::onCommand(eCommand command, tWorldTime t)
{
    if(command==COMMAND_DETECTION_ON)
    {
        if(!turnedOn)
        {
            if(parent->areCommandsVisible())
            {
                resetCommand(COMMAND_DETECTION_OFF);
                highlightCommand(COMMAND_DETECTION_ON);
            }
            turnedOn=true;
            RUN_PROCEDURE(type->onInitiateDetectionProc)
        }
    }
    else if(command==COMMAND_DETECTION_OFF)
    {
        if(turnedOn)
        {
            if(parent->areCommandsVisible())
            {
                resetCommand(COMMAND_DETECTION_ON);
                highlightCommand(COMMAND_DETECTION_OFF);
            }
            turnedOn=false;
            setRunning(false,t);
            RUN_PROCEDURE(type->onTerminateDetectionProc)
        }
    }
}
void tDetectionBehavior::intervalTick(tWorldTime t)
{
    update(t);
    if(running)
    {
        setConsumptionRate(EVALUATE_ARGUMENT(consumptionRateArg));
        parent->setFieldOfView(EVALUATE_ARGUMENT(fieldOfViewArg));
    }
}
void tDetectionBehavior::onBirth(tWorldTime t)
{
    update(t);
}
void tDetectionBehavior::onDeath(tWorldTime t)
{
    setRunning(false,t);
}
void tDetectionBehavior::onTransfer(int newOwner, tWorldTime t)
{
    setRunning(false,t);
}
void tDetectionBehavior::onSelect()
{
    if(turnedOn)
        highlightCommand(COMMAND_DETECTION_ON);
    else
        highlightCommand(COMMAND_DETECTION_OFF);
}
void tDetectionBehavior::onDeselect()
{
    if(turnedOn)
        resetCommand(COMMAND_DETECTION_ON);
    else
        resetCommand(COMMAND_DETECTION_OFF);
}
void tDetectionBehavior::setConsumptionRate(float _consumptionRate)
{
    if(_consumptionRate!=consumptionRate)
    {
		if(type->resourceConsumed>=0)
			adjustCost(parent->getOwner(),type->resourceConsumed,-consumptionRate);
        consumptionRate=_consumptionRate;
		if(type->resourceConsumed>=0)
			adjustCost(parent->getOwner(),type->resourceConsumed,consumptionRate);
    }
}
/****************************************************************************/
// COLLECTION BEHAVIOR
/****************************************************************************/
tCollectionType::tCollectionType(tUnitType *parent, tStream& s) throw(tString) : resourceCollected(-1),
    resourceConsumed(-1), collectionRateArg(0,BOUNDARY_MIN,0), consumptionRateArg(0,BOUNDARY_MIN,0),
    onInitiateCollectionProc(NULL), onTerminateCollectionProc(NULL), onBeginCollectionProc(NULL),
    onEndCollectionProc(NULL)
{
    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");
        
			if(strPrefix(str,"resource_collected"))
				resourceCollected=parseResource("resource collected",s);
			else if(strPrefix(str,"collection_rate"))
				PARSE_ARGUMENT(collectionRateArg)
			else if(strPrefix(str,"resource_consumed"))
				resourceConsumed=parseResource("resource consumed",s);
			else if(strPrefix(str,"consumption_rate"))
				PARSE_ARGUMENT(consumptionRateArg)
			else if(strPrefix(str,"on_initiate_collection"))
				PARSE_PROCEDURE(onInitiateCollectionProc)
			else if(strPrefix(str,"on_terminate_collection"))
				PARSE_PROCEDURE(onTerminateCollectionProc)
			else if(strPrefix(str,"on_begin_collection") || strPrefix(str,"on_start_collection"))
				PARSE_PROCEDURE(onBeginCollectionProc)
			else if(strPrefix(str,"on_end_collection") || strPrefix(str,"on_stop_collection"))
				PARSE_PROCEDURE(onEndCollectionProc)
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}
	if(resourceCollected==-1)
		throw tString("Please specify the resource collected");
	if(consumptionRateArg.loaded && resourceConsumed==-1)
		throw tString("Please specify the resource consumed");
}
tBehavior *tCollectionType::createBehavior(tUnit *parent)
{
    return mwNew tCollectionBehavior(this, parent);
}
void tCollectionBehavior::setRunning(bool _running, tWorldTime t)
{
    if(_running && !running)
    {
        running=true;
        RUN_PROCEDURE(type->onBeginCollectionProc)
    }
    else if(!_running && running)
    {
        running=false;
        setCollectionRate(0);
        setConsumptionRate(0);
        RUN_PROCEDURE(type->onEndCollectionProc)
    }
}
void tCollectionBehavior::update(tWorldTime t)
{
    setRunning(turnedOn && parent->isBuilt() && (type->resourceConsumed==-1 ||
		isResourceAvailable(parent->getOwner(),type->resourceConsumed)) &&
        canCollectResource(parent->getOwner(), type->resourceCollected),t);
}
bool tCollectionBehavior::canExecute(eCommand command)
{
    return (command==COMMAND_COLLECTION_ON || command==COMMAND_COLLECTION_OFF);
}
void tCollectionBehavior::onCommand(eCommand command, tWorldTime t)
{
    if(command==COMMAND_COLLECTION_ON)
    {
        if(!turnedOn)
        {
            if(parent->areCommandsVisible())
            {
                resetCommand(COMMAND_COLLECTION_OFF);
                highlightCommand(COMMAND_COLLECTION_ON);
            }
            turnedOn=true;
            RUN_PROCEDURE(type->onInitiateCollectionProc)
        }
    }
    else if(command==COMMAND_COLLECTION_OFF)
    {
        if(turnedOn)
        {
            if(parent->areCommandsVisible())
            {
                resetCommand(COMMAND_COLLECTION_ON);
                highlightCommand(COMMAND_COLLECTION_OFF);
            }
            turnedOn=false;
            setRunning(false,t);
            RUN_PROCEDURE(type->onTerminateCollectionProc)
        }
    }
}
void tCollectionBehavior::intervalTick(tWorldTime t)
{
    update(t);

    if(running)
    {
        setConsumptionRate(EVALUATE_ARGUMENT(consumptionRateArg));
        setCollectionRate(EVALUATE_ARGUMENT(collectionRateArg));
    }
}
void tCollectionBehavior::onBirth(tWorldTime t)
{
    update(t);
}
void tCollectionBehavior::onDeath(tWorldTime t)
{
    setRunning(false,t);
}
void tCollectionBehavior::onTransfer(int newOwner, tWorldTime t)
{
    setRunning(false,t);
}
void tCollectionBehavior::onSelect()
{
    if(turnedOn)
        highlightCommand(COMMAND_COLLECTION_ON);
    else
        highlightCommand(COMMAND_COLLECTION_OFF);
}
void tCollectionBehavior::onDeselect()
{
    if(turnedOn)
        resetCommand(COMMAND_COLLECTION_ON);
    else
        resetCommand(COMMAND_COLLECTION_OFF);
}
void tCollectionBehavior::setCollectionRate(float _collectionRate)
{
    if(_collectionRate!=collectionRate)
    {
        adjustRevenue(parent->getOwner(),type->resourceCollected,-collectionRate);
        collectionRate=_collectionRate;
        adjustRevenue(parent->getOwner(),type->resourceCollected,collectionRate);
    }
}
void tCollectionBehavior::setConsumptionRate(float _consumptionRate)
{
    if(_consumptionRate!=consumptionRate)
    {
		if(type->resourceConsumed>=0)
			adjustCost(parent->getOwner(),type->resourceConsumed,-consumptionRate);
        consumptionRate=_consumptionRate;
		if(type->resourceConsumed>=0)
			adjustCost(parent->getOwner(),type->resourceConsumed,consumptionRate);
    }
}
/****************************************************************************/
// WEAPON BEHAVIOR
/****************************************************************************/
tWeaponType::tWeaponType(tUnitType *parent, tStream& s) throw(tString) : missileType(NULL),
    coolDown(1), resourceConsumed(-1), cost(0), range(1), aimProcedureType(NULL),
    cosTheta(0), onEngageProc(NULL), onDisengageProc(NULL), onTargetAcquiredProc(NULL),
    onTargetLostProc(NULL), limitAngle(false), canFireArg(true,BOUNDARY_NONE), onFireProc(NULL),
    missileOriginType(NULL), missileDirectionType(NULL), canAimArg(true,BOUNDARY_NONE)
{
    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"missile_name") || strPrefix(str,"missile_type"))
				missileType=parseMissileType("missile name",s);
			else if(strPrefix(str, "cool_down") || strPrefix(str, "cooldown"))
				coolDown=parseFloatMin("cool down",.01,s);
			else if(strPrefix(str, "resource_consumed"))
				resourceConsumed=parseResource("resource consumed",s);
			else if(strPrefix(str, "cost"))
				cost=parseFloatMin("firing cost",0,s);
			else if(strPrefix(str, "range"))
				range=parseFloatMin("range",0,s);
			else if(strPrefix(str, "on_aim"))
			{
				validateModelType(parent->modelType);
				aimProcedureType=parent->modelType->parseAimProcedureType("aim procedure name",s);
			}
			else if(strPrefix(str, "angular_tolerance"))
			{
				float angularTolerance=parseAngleRange("angular tolerance",0/*PI*/,1/*PI*/,s);
				cosTheta=cos(angularTolerance);
				limitAngle=true;
			}
			else if(strPrefix(str, "on_engage"))
				PARSE_PROCEDURE(onEngageProc)
			else if(strPrefix(str, "on_disengage"))
				PARSE_PROCEDURE(onDisengageProc)
			else if(strPrefix(str, "on_target_acquired"))
				PARSE_PROCEDURE(onTargetAcquiredProc)
			else if(strPrefix(str, "on_target_lost"))
				PARSE_PROCEDURE(onTargetLostProc)
			else if(strPrefix(str, "on_fire"))
				PARSE_PROCEDURE(onFireProc)
			else if(strPrefix(str, "salvo"))
				parseSalvo(parent, s);
			else if(strPrefix(str, "missile_origin") || strPrefix(str, "primary_missile_origin"))
				PARSE_MODEL_VECTOR(missileOriginType)
			else if(strPrefix(str, "missile_direction") || strPrefix(str, "primary_missile_direction"))
				PARSE_MODEL_VECTOR(missileDirectionType)
            else if(strPrefix(str, "can_fire"))
                PARSE_ARGUMENT(canFireArg)
            else if(strPrefix(str, "can_aim"))
                PARSE_ARGUMENT(canAimArg)
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

	if(cost>0 && resourceConsumed==-1)
		throw tString("Please specify the resource consumed");
    if(missileOriginType==NULL)
        throw tString("Please specify the primary_missile_origin");
    if(limitAngle && missileDirectionType==NULL)
        throw tString("You need to specify the primary_missile_direction if you limit the angle");
    if(salvos.size()==0)
    {
        tSalvo s;
        s.missileOrigins.push_back(missileOriginType);
        salvos.push_back(s);
    }
}
void tWeaponType::parseSalvo(tUnitType *parent, tStream& s) throw(tString)
{
    tSalvo salvo;
    tModelVectorType *mvt;
    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"origins") || strPrefix(str,"missile_origins"))
			{
				do
				{
					PARSE_MODEL_VECTOR(mvt)
					salvo.missileOrigins.push_back(mvt);
				}
				while(s.matchOptionalToken(","));
			}
			else if(strPrefix(str,"on_fire"))
				PARSE_PROCEDURE(salvo.onFireProc)
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}
    salvos.push_back(salvo);
}
tBehavior *tWeaponType::createBehavior(tUnit *parent)
{
    return mwNew tWeaponBehavior(this, parent);
}



bool tWeaponBehavior::canExecute(eCommand command)
{
    return (command==COMMAND_ATTACK || command==COMMAND_FIRE_AT_WILL ||
        command==COMMAND_RETURN_FIRE || command==COMMAND_HOLD_FIRE);
}
eCommand tWeaponBehavior::getDefaultCommand(tUnit *u)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(u))
        return COMMAND_NONE;
#endif
    if(getAlliance(parent->getOwner(), u->getOwner())==ALLIANCE_ENEMY)
        return COMMAND_ATTACK;
    return COMMAND_NONE;
}
void tWeaponBehavior::onCommand(eCommand command, tUnit *u, tWorldTime t)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(u))
        return;
#endif
    if(command==COMMAND_ATTACK)
    {
        if(u->getState()==STATE_ALIVE)
        {
            targets.push_back(u);
            setHasTargets(true,t);
        }
    }
}
void tWeaponBehavior::onCommand(eCommand command, tWorldTime t)
{
    if(command==COMMAND_FIRE_AT_WILL || command==COMMAND_RETURN_FIRE ||
        command==COMMAND_HOLD_FIRE)
        setCombatMode(command);
}
bool tWeaponBehavior::isFiringAt(tUnit *u)
{
    return (primary==u || secondary==u);
}
void tWeaponBehavior::onDamage(tUnit *u, float amt, tWorldTime t)
//precondition: u may be NULL!
{
    if(u==NULL) //Attacker has perished
        return;
    /* CONDITIONS FOR ENGAGING NEW SECONDARY TARGET:
     *     (1) The parent can attack the aggressor
     *     (2) The parent can see the aggressor
     *     (3) The unit is in fire-at-will mode
     *     (4) The unit is in wander mode OR the aggressor is within range
     *     (5) The unit is an enemy
	 *     (6) There is no secondary target
	 *              OR
	 *		   The parent's orientation permits it to fire at the aggressor but not the existing
	 *         secondary target
	 *		        OR
	 *		   The parent's orientation permits it to fire at the aggressor and the existing
	 *	       secondary target but the aggressor is closer
	 *              OR
	 *         The existing secondary target is not firing at the parent
     */
	bool aggressorWithinRange=withinRange(u);
    if(canAttack(u) && CAN_SEE(parent,u) &&
        combatMode!=COMMAND_HOLD_FIRE &&
        (parent->getMovementMode()==COMMAND_WANDER || aggressorWithinRange)
        && !isUnitFriendly(u))
    {
		bool canFireAtOld=false;
		if(secondary)
			canFireAtOld=withinRange(secondary) && withinAngle(secondary);
		bool canFireAtNew=aggressorWithinRange && withinAngle(u);
		if(secondary==NULL || (canFireAtNew && !canFireAtOld) ||
		   (canFireAtNew && canFireAtOld &&
		   UNIT_DISTANCE_SQUARED(parent,u) < UNIT_DISTANCE_SQUARED(parent,secondary)) ||
		   !secondary->isFiringAt(parent))
		{
			setFiringAtSecondary(false,t);
			//secondary=NULL; //omitted because we reset the pointer on the line below.
			setSecondary(u,t);
		}
    }
}
void tWeaponBehavior::onClearOrders(tWorldTime t)
{
    setFiringAtPrimary(false, t);
    setFiringAtSecondary(false, t);
    setPrimary(NULL,t);
    setSecondary(NULL,t);
    targets.clear();
    setHasTargets(false,t);
}
void tWeaponBehavior::intervalTick(tWorldTime t)
{
    int i,flags;
    float fieldOfView;

    //This block of code is an optimization for acquireTarget.  Units with a limited angle
    //need to begin rotating their turret before their target is in range.
    if(parent->getMovementMode()==COMMAND_WANDER || type->limitAngle)
        fieldOfView=PURSUE_RADIUS;
    else
		fieldOfView=type->range;
	missileOrigin=parent->locatePoint(type->missileOriginType);
    if(type->missileDirectionType)
    	missileDirection=parent->locateUnitVector(type->missileDirectionType);

    for(i=0;i<targets.size(); )
    {
        if(targets[i]->getState()!=STATE_ALIVE)
        {
            if(targets[i]==primary)
            {
                setFiringAtPrimary(false,t);
                setPrimary(NULL,t);
            }
            if(targets[i]==secondary)
            {
                setFiringAtSecondary(false,t);
                setSecondary(NULL,t);
            }
            targets.erase(targets.begin()+i);
        }
        else
            i++;
    }
    setHasTargets(targets.size()>0,t);

    bool setPrimaryToNULL=false;
    if(primary)
    {
        setFiringAtPrimary(canAttack(primary) && CAN_SEE(parent,primary) &&
            withinRange(primary) && withinAngle(primary), t);
        if(!firingAtPrimary)
            setPrimaryToNULL=true;
    }
    if(hasTargets && (primary==NULL || setPrimaryToNULL))
    {
        if(parent->isAbilityEnabled(ABILITY_ATTACK))
        {
            flags=ACQUIRE_VULNERABLE_ONLY;
            if(type->limitAngle)
                flags|=ACQUIRE_LIMIT_ANGLE;
            setPrimaryToNULL=false;
            setPrimary(acquireTarget(parent,type->range,flags,missileOrigin,
									 missileDirection,type->cosTheta,targets),t);
        }
    }
    if(setPrimaryToNULL)
        setPrimary(NULL,t);

    bool setSecondaryToNULL=false;
    if(secondary)
    {
        setFiringAtSecondary(canAttack(secondary) && CAN_SEE(parent,secondary)
            && (secondary->isFiringAt(parent) || !isUnitFriendly(secondary))
            && combatMode!=COMMAND_HOLD_FIRE && withinRange(secondary) && withinAngle(secondary), t);
        if(!firingAtSecondary)
            setSecondaryToNULL=true;
    }
    if(secondary==NULL || setSecondaryToNULL)
    {
        if(combatMode==COMMAND_FIRE_AT_WILL && parent->isAbilityEnabled(ABILITY_ATTACK) && !firingAtPrimary)
        {
            flags=ACQUIRE_ENEMY_ONLY|ACQUIRE_ALIVE_ONLY|ACQUIRE_VULNERABLE_ONLY;
            if(type->limitAngle)
                flags|=ACQUIRE_LIMIT_ANGLE;
            setSecondaryToNULL=false;
            setSecondary(acquireTarget(parent,type->range,fieldOfView,flags,missileOrigin,
									   missileDirection,type->cosTheta),t);
        }
    }
    if(setSecondaryToNULL)
        setSecondary(NULL,t);

    //Don't even think of moving this to tick...
    //The tMissile constructor calculates endTime based on unit positions!
    if((isFiringAtPrimary() || isFiringAtSecondary()) && EVALUATE_ARGUMENT(canFireArg)==true)
    {
        if(t>=nextRound)
        {
            if(type->resourceConsumed>=0)
                adjustResource(parent->getOwner(),type->resourceConsumed,-type->cost);

            tUnit *u=primary;
            if(u==NULL)
                u=secondary;

#ifdef DEBUG
            if(curSalvo<0 || curSalvo>=type->salvos.size())
            {
                bug("tWeaponBehavior::intervalTick invalid curSalvo");
                curSalvo=0;
            }
#endif
            tSalvo& s=type->salvos[curSalvo];
            vector<tModelVectorType*>::iterator it;
            for(it=s.missileOrigins.begin();it!=s.missileOrigins.end();it++)
                createMissile(parent, parent->locatePoint(*it), u, type->missileType, t);
            RUN_PROCEDURE(s.onFireProc)

            nextRound=t+type->coolDown;
            curSalvo++;
            if(curSalvo==type->salvos.size())
                curSalvo=0;

            RUN_PROCEDURE(type->onFireProc)
        }
    }

    tUnit *u=primary;
    if(u==NULL)
        u=secondary;
    setAiming(u && EVALUATE_ARGUMENT(canAimArg)==true,u,t);
}

void tWeaponBehavior::onUnitDestroyed(tUnit *u, tWorldTime t)
{
    if(u==primary)
    {
        setFiringAtPrimary(false,t);
        setPrimary(NULL,t);
    }
    if(u==secondary)
    {
        setFiringAtSecondary(false,t);
        setSecondary(NULL,t);
    }
    for(int i=0;i<targets.size(); )
    {
        if(targets[i]==u)
            targets.erase(targets.begin()+i);
        else
            i++;
    }
    setHasTargets(targets.size()>0,t);
}
void tWeaponBehavior::setFiringAtPrimary(bool _firing, tWorldTime t)
{
    if(!firingAtPrimary && _firing)
    {
        firingAtPrimary=true;
        if(firingAtSecondary)
            onEndFiring(/*primary=*/false,t);
        onBeginFiring(/*primary=*/true,t);
    }
    else if(firingAtPrimary && !_firing)
    {
        firingAtPrimary=false;
        onEndFiring(/*primary=*/true,t);
        if(firingAtSecondary)
            onBeginFiring(/*primary=*/false,t);
    }
}
void tWeaponBehavior::setFiringAtSecondary(bool _firing, tWorldTime t)
{
    if(!firingAtSecondary && _firing)
    {
        firingAtSecondary=true;
        if(!firingAtPrimary)
            onBeginFiring(/*primary=*/false,t);
    }
    else if(firingAtSecondary && !_firing)
    {
        firingAtSecondary=false;
        if(!firingAtPrimary)
            onEndFiring(/*primary=*/false,t);
    }
}
bool tWeaponBehavior::isUnitFriendly(tUnit *u)
{
    if(u)
        return getAlliance(parent->getOwner(),u->getOwner())<=ALLIANCE_ALLY;
    else
        return false;
}
void tWeaponBehavior::onSelect()
{
    highlightCommand(combatMode);
}
void tWeaponBehavior::onDeselect()
{
    resetCommand(combatMode);
}
void tWeaponBehavior::onTransfer(int newOwner, tWorldTime t)
{
    //This is here in case a descendent implements the weapon as a continuous "damage beam"
    setFiringAtPrimary(false,t);
    setFiringAtSecondary(false,t);
}
void tWeaponBehavior::setHasTargets(bool _hasTargets, tWorldTime t)
{
    if(!hasTargets && _hasTargets)
    {
        hasTargets=true;
        parent->setCurCommand(COMMAND_ATTACK,t);
    }
    else if(hasTargets && !_hasTargets)
    {
        hasTargets=false;
        parent->resetCurCommand(t);
    }
}
void tWeaponBehavior::setCombatMode(eCommand _combatMode)
{
    if(combatMode==_combatMode)
        return;
    if(parent->areCommandsVisible())
        resetCommand(combatMode);
    combatMode=_combatMode;
    if(parent->areCommandsVisible())
        highlightCommand(combatMode);
}
bool tWeaponBehavior::withinRange(tUnit *u) //Is this unit close enough to initiate fire?
{
    return (u->getCenter()-missileOrigin).lengthSquared() <= type->range*type->range;
}
bool tWeaponBehavior::withinAngle(tUnit *u)
{
    if(type->limitAngle)
    {
        tVector3D offset=(u->getCenter()-missileOrigin).normalize();
        return ((missileDirection.dot(offset))>=type->cosTheta);
    }
    return true;
}
ePriority tWeaponBehavior::getLeader(tUnit*& u, float& distance, bool& overshoot)
{
    if(primary)
    {
        u=primary;
        distance=type->range*.75; //Fudge factor
        overshoot=false;
        return PRIORITY_PRIMARY;
    }
    else if(secondary)
    {
        u=secondary;
        distance=type->range*.75; //Fudge factor
        overshoot=false;
        return PRIORITY_SECONDARY;
    }
    return PRIORITY_NONE;
}
void tWeaponBehavior::setAiming(bool _aiming, tUnit *u, tWorldTime t)
{
    if(_aiming)
    {
        if(type->aimProcedureType)
            parent->runAimProcedure(type->aimProcedureType,u->getCenter(),t);
    }
    else if(aiming && !_aiming)
    {
        if(type->aimProcedureType)
            parent->stopAimProcedure(type->aimProcedureType,t);
    }
    aiming=_aiming;
}
void tWeaponBehavior::setPrimary(tUnit *_primary, tWorldTime t)
{
    if(secondary==NULL)
    {
        if(primary==NULL && _primary)
            RUN_PROCEDURE(type->onEngageProc)
        else if(primary && _primary==NULL)
        {
            setAiming(false,primary,t);
            RUN_PROCEDURE(type->onDisengageProc)
        }
    }
    primary=_primary;
}
void tWeaponBehavior::setSecondary(tUnit *_secondary, tWorldTime t)
{
    if(primary==NULL)
    {
        if(secondary==NULL && _secondary)
            RUN_PROCEDURE(type->onEngageProc)
        else if(secondary && _secondary==NULL)
        {
            setAiming(false,secondary,t);
            RUN_PROCEDURE(type->onDisengageProc)
        }
    }
    secondary=_secondary;
}
void tWeaponBehavior::onBeginFiring(bool primary, tWorldTime t)
{
    RUN_PROCEDURE(type->onTargetAcquiredProc)
}
void tWeaponBehavior::onEndFiring(bool primary, tWorldTime t)
{
    RUN_PROCEDURE(type->onTargetLostProc)
}



/****************************************************************************/
// MOVEMENT BEHAVIOR
/****************************************************************************/
void tMovementBehavior::onCommand(eCommand command, tUnit *u, tWorldTime t)
{
    if(command==COMMAND_FOLLOW)
    {
        leaders.push_back(u);
        setCurCommand(command,t);
        if(parent->getMovementMode()==COMMAND_EVADE || parent->getMovementMode()==COMMAND_HOLD_POSITION)
            parent->setMovementMode(COMMAND_MANEUVER,t);
    }
}
void tMovementBehavior::onCommand(eCommand command, const tVector2D& v, tWorldTime t)
{
    if(command==COMMAND_MOVE || command==COMMAND_PATROL)
    {
        destination=v;
        setCurCommand(command,t);
        if(parent->getMovementMode()==COMMAND_EVADE || parent->getMovementMode()==COMMAND_HOLD_POSITION)
            parent->setMovementMode(COMMAND_MANEUVER,t);
    }
}
void tMovementBehavior::onClearOrders(tWorldTime t)
{
    leader=NULL;
    leaders.clear();
    setCurCommand(COMMAND_NONE,t);
}
void tMovementBehavior::onUnitDestroyed(tUnit *u, tWorldTime t)
{
    if(leader==u)
        leader=NULL;
    for(int i=0;i<leaders.size(); )
    {
        if(leaders[i]==u)
            leaders.erase(leaders.begin()+i);
        else
            i++;
    }
    if(curCommand==COMMAND_FOLLOW && leaders.size()==0)
        setCurCommand(COMMAND_NONE,t);
}
void tMovementBehavior::intervalTick(tWorldTime t)
{
    if(curCommand==COMMAND_MOVE || curCommand==COMMAND_PATROL)
    {
        if( (destination-parent->getPosition()).lengthSquared() <
            (curCommand==COMMAND_MOVE ? MOVE_PRECISION_SQUARED : PATROL_PRECISION_SQUARED ))
            setCurCommand(COMMAND_NONE, t);
    }
    else if(curCommand==COMMAND_FOLLOW)
    {
        //We could optimize this routine to follow the nearest leader, but it
        //would significantly increase the behavior's time complexity.
        //Notice that we never finish a follow command.

        //It doesn't make sense to follow units we can't see:
        if(leader && !CAN_SEE(parent,leader))
            leader=NULL;
        if(leader==NULL)
            //Range is only important if we limit the angle.
            leader=acquireTarget(parent,/*range=*/0,/*flags=*/0,parent->getPosition3D(),
                /*forward=*/zeroVector3D, /*cosTheta=*/0,leaders);
    }
}
eCommand tMovementBehavior::getDefaultCommand(tUnit *u)
{
    return COMMAND_FOLLOW;
}
eCommand tMovementBehavior::getDefaultCommand()
{
    return COMMAND_MOVE;
}
bool tMovementBehavior::canExecute(eCommand command)
{
    return (command==COMMAND_MOVE || command==COMMAND_PATROL || command==COMMAND_FOLLOW);
}
ePriority tMovementBehavior::getLeader(tUnit*& u, float& distance, bool& overshoot)
{
    if(leader)
    {
        u=leader;
        if(leader->getModel()->getMaxZ()>0)
            distance=parent->getModel()->getMaxRadius()+leader->getModel()->getMaxRadius();
        else
            distance=0;
        overshoot=false;
        if(parent->getMovementMode()==COMMAND_WANDER)
            return PRIORITY_SECONDARY;
        else
            return PRIORITY_PRIMARY;
    }
    return PRIORITY_NONE;
}
ePriority tMovementBehavior::getDestination(tVector2D& v, float& distance, bool& overshoot)
{
    if(curCommand==COMMAND_MOVE || curCommand==COMMAND_PATROL)
    {
        v=destination;
        distance=0;
        overshoot=(curCommand==COMMAND_PATROL);
        if(parent->getMovementMode()==COMMAND_WANDER)
            return PRIORITY_SECONDARY;
        else
            return PRIORITY_PRIMARY;
    }
    return PRIORITY_NONE;
}
void tMovementBehavior::setCurCommand(eCommand command, tWorldTime t)
{
    if(curCommand==command)
        return;
    if(curCommand==COMMAND_NONE)
        parent->setCurCommand(command,t);
    else if(command==COMMAND_NONE)
        parent->resetCurCommand(t);
    else
    {
        bug("tMovementBehavior::setCurCommand: already processing a different command");
        return;
    }
    curCommand=command;
}



/****************************************************************************/
// GLOBAL FUNCTIONS
/****************************************************************************/
tBehaviorType *parseBehaviorTypeBlock(tUnitType *parent, tStream& s) throw(tString)
{
    tString str=parseIdentifier("behavior name",s);

    if(strPrefix(str, "construction"))
        return mwNew tConstructionType(parent,s);
    if(strPrefix(str, "division"))
        return mwNew tDivisionType(parent,s);
    if(strPrefix(str, "extraction"))
        return mwNew tExtractionType(parent,s);
    if(strPrefix(str, "production"))
        return mwNew tProductionType(parent,s);
    if(strPrefix(str, "generic"))
        return mwNew tGenericType(parent,s);
    if(strPrefix(str, "generic_consumption"))
        return mwNew tGenericConsumptionType(parent,s);
    if(strPrefix(str, "storage"))
        return mwNew tStorageType(parent,s);
    if(strPrefix(str, "detection"))
        return mwNew tDetectionType(parent,s);
    if(strPrefix(str, "collection"))
        return mwNew tCollectionType(parent,s);
    if(strPrefix(str, "weapon"))
        return mwNew tWeaponType(parent,s);

    throw tString("Unrecognized behavior name '")+str+"'";
}

void parseBuildOptions(tStringList& names, tStream& s) throw(tString)
{
    do
	{
		tString str=parseIdentifier("unit type name",s);
		names.push_back(tString(str));
	}
	while(s.matchOptionalToken(","));
}

void convertBuildOptions(const tStringList& names, bool buildOptions[],
						 eParamType paramType) throw(tString)
{
    tUnitType *ut;
    int i;
    eCommand cmd;
    
    for(i=0;i<names.size();i++)
    {
        ut=unitTypeLookup(names[i], /*exact=*/ false);
        if(ut==NULL)
            throw tString("Unrecognized unit type '")+names[i]+"'";

        cmd=commandLookup(ut,paramType);
        if(cmd==COMMAND_NONE) //command doesn't yet exist
            cmd=createCommand(tString("build_")+ut->name,ut->buildDesc,paramType,
                paramType==PARAM_PRODUCTION, false, ut->buildHotKey, ut->ID);

#ifdef DEBUG
        if(cmd<0 || cmd>=commandCount)
        {
            bug("convertBuildOptions: invalid command");
            return;
        }
#endif
        buildOptions[cmd]=true;
    }
}

eBehaviorName parseBehaviorName(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName, s);
    if(strPrefix(str, "construction"))
        return BEHAVIOR_CONSTRUCTION;
    else if(strPrefix(str, "division"))
        return BEHAVIOR_DIVISION;
    else if(strPrefix(str, "extraction"))
        return BEHAVIOR_EXTRACTION;
    else if(strPrefix(str, "production"))
        return BEHAVIOR_PRODUCTION;
    else if(strPrefix(str, "generic"))
        return BEHAVIOR_GENERIC;
    else if(strPrefix(str, "generic_consumption"))
        return BEHAVIOR_GENERIC_CONSUMPTION;
    else if(strPrefix(str, "storage"))
        return BEHAVIOR_STORAGE;
    else if(strPrefix(str, "detection"))
        return BEHAVIOR_DETECTION;
    else if(strPrefix(str, "collection"))
        return BEHAVIOR_COLLECTION;
    else if(strPrefix(str, "weapon"))
        return BEHAVIOR_WEAPON;

    throw tString("Unrecognized behavior name '")+str+"'";
}

