/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef gameH
#define gameH
/***************************************************************************/
#include "types.h"

#define INITIALIZATION_FILE     "game.ini"

enum eMouseButton { MOUSE_BUTTON_LEFT, MOUSE_BUTTON_RIGHT, MOUSE_BUTTON_MIDDLE };
enum eMouseWheel { MOUSE_WHEEL_UP, MOUSE_WHEEL_DOWN, MOUSE_WHEEL_OTHER };

extern void endApplication();                       //End the application
extern bool setVideoMode(bool _fullScreen, int width, int height);
extern bool setFullScreen(bool _fullScreen);
extern bool setResolution(int width, int height);
extern void clampFrameRate(int frameRate);          //Prevents the frame rate from increasing above this value.
                                                    //(for testing)
extern void invalidateScreen();                     //Redraw the entire screen.  Call this function if you want
                                                    //to display a message during a blocking process.
extern void invalidateMousePosition();              //Generate a mouse move event.  Call this function when the
                                                    //matrix of a 3D window changes.

//Keyboard functions:
extern bool isModifierKey(tKey key);
extern bool isShiftPressed();
extern bool isAltPressed();
extern bool isCtrlPressed();
extern bool isKeyPressed(tKey key);

//Mouse variables:
extern bool isMousePressed();
extern bool isLeftButtonPressed();
extern bool isRightButtonPressed();
extern int getMouseX();
extern int getMouseY();
extern int getMouseWheel();

//Program state variables:
extern bool isWindowActive();
extern bool isFullScreen();
extern int getScreenWidth();
extern int getScreenHeight();

//User's name (used throughout program):
extern tString  globalName;
extern tString  initialMap;
extern tString  initialWorld;

//Event handlers (defined in client.cpp):
extern void onMouseDown(int x, int y, eMouseButton button);
extern void onMouseMove(int x, int y);
extern void onMouseUp(int x, int y, eMouseButton button);
extern void onMouseWheel(int pos, eMouseWheel direction);
extern void onKeyDown(tKey key, char ch);
extern void onKeyUp(tKey key, char ch);
extern void onKeyPress(char ch);
extern void onSetResolution(int w, int h);

extern void interfaceStartup() throw(tException);   //called during program startup
extern void interfaceRefresh();                     //called when user toggles full screen mode
extern void interfaceShutdown();                    //called during program shutdown
extern void interfacePoll();                        //called prior to each frame
extern void interfaceDraw();                        //draws the whole screen

//Current time:
extern tTime currentTime;
/***************************************************************************/
#endif

