/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* MEMWATCH.CPP:  Simple memory manager that checks for invalid delete       .
                 operations and memory leaks.                               *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#include "memwatch.h"
#include "types.h"

#ifdef MEMWATCH
static tMemoryRecord memoryRecords[MAX_RECORDS];
static int recordIndex=0;

static char* heapStart=(char *)(-1);
static char* heapEnd=(char *)(0);

static bool programEnded=false;

const char *mwFile="(unknown)";
int mwLine=0;

void memoryLeakReport()
{
#ifdef MEMWATCH
    int i;
    FILE *fp=fopen("memleak.txt","w");
    if(fp==NULL)
        return;
    for(i=0;i<recordIndex;i++)
    {
        fprintf(fp,"Failed to delete %u bytes starting at address %p allocated in %s:%d\n",
            memoryRecords[i].bytes,memoryRecords[i].address,
            memoryRecords[i].file,memoryRecords[i].line);
    }
    fclose(fp);
#endif
}

void *operator new(size_t size)
{
    if(size>50000000) //50 megabytes
    {
        bug("Suspicious memory allocation of %u bytes",size);
        throw std::bad_alloc();
    }
	void *ptr = malloc(size);
    if(ptr==NULL)
    {
        bug("Out of memory!  'malloc(%u)' returned with NULL.",size);
        throw std::bad_alloc();
    }
    if(recordIndex>=MAX_RECORDS)
        bug("Memory record overflow!  Increase MAX_RECORDS.");
    else
    {
        memoryRecords[recordIndex].address=ptr;
        memoryRecords[recordIndex].bytes=size;
        memoryRecords[recordIndex].file="(unknown)";
        memoryRecords[recordIndex].line=0;
        recordIndex++;
        
        if((char*)ptr<heapStart)
            heapStart=(char*)ptr;
        if((char*)ptr+size>heapEnd)
            heapEnd=(char*)ptr+size;
    }
    return ptr;
}

void *operator new(size_t size, const char *file, int line)
{
    if(size>50000000) //50 megabytes
    {
        bug("Suspicious memory allocation of %u bytes in %s:%d",size,file,line);
        throw std::bad_alloc();
    }
	void *ptr = malloc(size);
    if(ptr==NULL)
    {
        bug("Out of memory!  'malloc(%u)' returned with NULL in %s:%d.",size,file,line);
        throw std::bad_alloc();
    }
    if(recordIndex>=MAX_RECORDS)
        bug("Memory record overflow!  Increase MAX_RECORDS.");
    else
    {
        memoryRecords[recordIndex].address=ptr;
        memoryRecords[recordIndex].bytes=size;
        memoryRecords[recordIndex].file=file;
        memoryRecords[recordIndex].line=line;
        recordIndex++;
    
        if((char*)ptr<heapStart)
            heapStart=(char*)ptr;
        if((char*)ptr+size>heapEnd)
            heapEnd=(char*)ptr+size;
    }
    return ptr;
}
                                    
void operator delete(void *ptr)
{
    //Ordinarily, this would constitute a bug, but the OS seems to treat
    //delete NULL as a valid NOP.
    if(ptr==NULL)
        return;
    for(int i=0;i<recordIndex;i++)
        if(memoryRecords[i].address==ptr)
        {
            memoryRecords[i]=memoryRecords[recordIndex-1];
            recordIndex--;
            free(ptr);
            if(programEnded)
                memoryLeakReport();
            return;
        }
    bug("Attempted to deallocate an invalid memory address in %s:%d!",mwFile,mwLine);
    mwFile="(unknown)";
    mwLine=0;
}

void *operator new[](size_t size)
    { return operator new(size); }
void *operator new[](size_t size, const char *file, int line)
    { return operator new(size,file,line); }
void operator delete[](void *ptr)
    { operator delete(ptr); }

bool isMemoryValid(void *ptr, size_t bytes, const char *file, int line)
{
    if(ptr==NULL)
    {
        bug("NULL pointer detected in %s:%d!",file,line);
        return false;
    }
    //We can't do much about static memory and the stack, so we'll have to assume
    //that the pointer is valid.
    if((char*)ptr<heapStart || (char*)ptr+bytes>=heapEnd)
        return true;
    
    for(int i=0;i<recordIndex;i++)
        if(ptr >= memoryRecords[i].address && (char *)ptr+bytes <= (char *)memoryRecords[i].address+memoryRecords[i].bytes)
            return true;

    bug("Access violation in %s:%d!",file,line);
    return false;
}
#endif

void memoryStartup()
{
}

void memoryShutdown()
{
#ifdef MEMWATCH
    programEnded=true;
    memoryLeakReport();
#endif
}
