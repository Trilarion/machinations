/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* CLIENT.CPP:   Abstract control library which supports buttons, check      *
*               boxes, radio buttons, text boxes, list boxes, combo boxes,  *
*               and dialog boxes.                                           *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
#include <GL/glfw.h>
/****************************************************************************/
#include "client.h"
#include "font.h"
#include "memwatch.h"
#include "game.h"
#pragma package(smart_init)

/****************************************************************************
                            Static Variables
 ****************************************************************************/
/* Windows are stored in the reverse order that they are drawn, with windows[0]
 * drawn last.  This variable is static since other modules should use
 * createWindow, destroyWindow, etc. to update windows.
 */
static vector<tWindow *>    windows;
static tWindow*             selectedWindow = NULL;      //Currently selected window
                                                            //(window that mouse covers)
static tWindow*             focusedWindow = NULL;       //Currently focused window

static tMousePointer*       mousePointer = NULL;        //Pointer to the mouse pointer that is currently visible.

static bool getMousePointer(int x, int y, ePointer& p, int& tag);

/****************************************************************************
                               Class Ancestors
 ****************************************************************************/
//This array defines the ancestor of each control.  This is useful for tracing
//heredity and determining if one control is a descendent of another.  Make
//sure this array remains synchronized with eControl!!

eControl ancestors[CTRL_MAXIMUM] = {
    CTRL_NONE,                  CTRL_NONE,                  CTRL_CONTROL,
    CTRL_CONTROL,               CTRL_CONTROL,               CTRL_CONTROL,
    CTRL_CONTROL,               CTRL_SCROLL_BAR,            CTRL_CONTROL,
    CTRL_SCROLL_WINDOW,         CTRL_TEXT_BOX,              CTRL_SCROLL_WINDOW,
    CTRL_LIST_BOX,              CTRL_SCROLL_WINDOW,         CTRL_CONTROL,
    CTRL_BUTTON,                CTRL_WINDOW,                CTRL_STATIC,
    CTRL_BUTTON,                CTRL_TOGGLE,                CTRL_RADIO,
    CTRL_SCROLL_BAR,            CTRL_SCROLL_WINDOW,         CTRL_TEXT_BOX,
    CTRL_LIST_BOX,              CTRL_COMBO_BOX,             CTRL_DIALOG_BOX};

int saveMouseX=0;
int saveMouseY=0;


/****************************************************************************\
tControl::resetHint

Hides the hint and resets the timer so that the hint will reappear in HINT_DELAY
with proper conditions.  A hint is a small caption that momentarily appears
over the control.  The hint will only appear if the control overrides the
drawHint function.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tControl::resetHint()
{
    if(hintVisible)
        hintVisible=false;
    lastMouseMove = currentTime;
}

/****************************************************************************\
tControl::poll

The interface calls each control's poll function at each pass to scroll, update
animations, etc.  If you choose to override this function, be sure to call
the ancestor's counterpart at the beginning of your function.  The base
definition (below) automatically shows the hint under proper conditions.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tControl::poll()
{
    //Show the hint only if the control is selected, the user is not holding down
    //a mouse button, and HINT_DELAY has elapsed without movement.
    if(selected && !isMousePressed() && currentTime - lastMouseMove > HINT_DELAY)
        hintVisible=true;
}

/****************************************************************************\
tControl::onMouseDown

This event occurs when the user presses the left or right mouse button over
the control in the region defined by objectActivated.  If you choose to override
this function, be sure to call the ancestor's counterpart at the beginning
of your function.  The base definition (below) hides the hint and saves the position
of the mouse.

Inputs:
    x,y     Absolute coordinates where mouse was pressed
                (They are -not- relative to the control)
    button  MOUSE_BUTTON_LEFT or MOUSE_BUTTON_RIGHT
Outputs:
    none
\****************************************************************************/
void tControl::onMouseDown(int x, int y, eMouseButton button)
{
    resetHint();
    saveMouseX=x;
    saveMouseY;y;
}

/****************************************************************************\
tControl::onMouseMove

This event occurs when the user moves the mouse while the control is selected.
The mouse buttons may be up or down.  If the user drags the mouse off the
control with at least one button pressed, the event will continue to execute.
If, however, the user moves the mouse away from the control without pressing
a button, the event will cease to execute and an onDeselect event will occur.
If you choose to override this function, be sure to call the ancestor's
counterpart at the beginning of your function.  The base definition (below)
hides the hint and saves the position of the mouse.

Inputs:
    x,y     New absolute coordinates of the mouse
                (They are -not- relative to the control)
Outputs:
    none
\****************************************************************************/
void tControl::onMouseMove(int x, int y)
{
    //10/8/03: I moved resetHint from onMouseMove to onSelect because I now invoke
    //a mouse move event before each frame.
    //resetHint();
    saveMouseX=x;
    saveMouseY=y;
}

/****************************************************************************\
tControl::onMouseUp

This event occurs when the user releases the mouse while the control is selected.
The mouse may or may not be over the control in the region defined by
objectActivated.  If you choose to override this function, be sure to call the
ancestor's counterpart at the beginning of your function.  The base definition
(below) hides the hint and saves the position of the mouse.

Inputs:
    x,y     Absolute coordinates where mouse was released
                (They are -not- relative to the control)
    button  MOUSE_BUTTON_LEFT or MOUSE_BUTTON_RIGHT
Outputs:
    none
\****************************************************************************/
void tControl::onMouseUp(int x, int y, eMouseButton button)
{
    resetHint();
    saveMouseX=x;
    saveMouseY=y;
}

//This function serves the same purpose as onMouseUp et al.
void tControl::onMouseWheel(int pos, eMouseWheel direction)
{
    resetHint();
}

/****************************************************************************\
tControl::onKeyDown

This event occurs when the user presses a key while the control has focus.
IMPORTANT:  This function will repeatedly execute as long as the user holds
down the key due to automatic key repetition.  For instance, if you hold down
the 'A' key for three seconds, this event will occur each time a new 'A'
appears on the screen.  However, an onKeyUp event will not occur until the
user releases the key.  If you choose to override this function, be sure to call
the ancestor's counterpart at the beginning of your function.  The base definition
(below) hides the hint.

Inputs:
    key     GLFW key code.  Usually the virtual key code will equal its
                ASCII code, except for special GLFW keys.
    ch      Corresponding ASCII code.  If the key doesn't have one, ch will
                equal zero.
\****************************************************************************/
void tControl::onKeyDown(tKey key, char ch)
{
    resetHint();
}

/****************************************************************************\
tControl::onKeyUp

This event occurs when the user releases a key while the control has focus.
If you choose to override this function, be sure to call the ancestor's
counterpart at the beginning of your function.  The base definition (below)
does nothing.

Inputs:
    key     Virtual key code.  Usually the virtual key code will equal its
                ASCII code, except for special keys (see UIKEY constants in
                header file).
    ch      Corresponding ASCII code.  If the key doesn't have one, ch will
                equal zero.
Outputs:
    none
\****************************************************************************/
void tControl::onKeyUp(tKey key, char ch)
{
}

/****************************************************************************\
tControl::onSelect

This event occurs when the user moves the mouse onto the control.  You may
wish to highlight the control or change the mouse pointer in some controls.
If you choose to override this function, be sure to call the ancestor's
counterpart at the beginning of your function.  The base definition (below)
sets a state variable and saves the position of the mosue.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tControl::onSelect(int x, int y)
{
    selected = true;
    saveMouseX=x;
    saveMouseY=y;
    //10/8/03: I moved resetHint from onMouseMove to onSelect because I now invoke
    //a mouse move event before each frame.
    resetHint();
}

/****************************************************************************\
tControl::onDeselect

This event occurs when the user moves the mouse off of the control.
IMPORTANT: A control does lose its selection if the user is pressing a mouse
button -even if- the mouse pointer leaves the control.  If you choose to
override this function, be sure to call the ancestor's counterpart at the
beginning of your function.  The base definition (below) hides the hint and
sets a state variable.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tControl::onDeselect()
{
    resetHint();
    selected = false;
}

/****************************************************************************\
tControl::onFocus

This event occurs when the user focuses the control by clicking on it or
pressing TAB/SHIFT+TAB.  You can also generate this event indirectly by
calling 'focus'.  Not all controls can receive focus.  The constructor's last
parameter determines whether the control can receive focus.  If you choose to
override this function, be sure to call the ancestor's counterpart at the
beginning of your function.  The base definition (below) sets a state variable.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tControl::onFocus()
{
    focused = true;
}

/****************************************************************************\
tControl::onDefocus

This event occurs when the control loses focus.  The control loses focus when
the user clicks on another control/window or presses TAB/SHIFT+TAB.  You can
also generate this event indirectly by calling 'defocus'.  If you choose to
override this function, be sure to call the ancestor's counterpart at the
beginning of your function.  The base definition (below) hides the hint and
sets a state variable.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tControl::onDefocus()
{
    resetHint();
    focused = false;
}

/****************************************************************************\
tControl::onDisable

This event occurs when a control is disabled.  A disabled control loses its
user-interface and usually appears dim and gray.  You can generate this event
indirectly by calling 'disable'.  If you choose to override this function, be
sure to call the ancestor's counterpart at the beginning of your function.  The
base definition (below) sets a state variable, deselects and defocuses the
control, and hides the hint.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tControl::onDisable()
{
    if(disabled==false)
    {
        disabled=true;
        if(selected)
            onDeselect();
        if(focused)
            onDefocus();
        resetHint();
    }
}

/****************************************************************************\
tControl::onEnable

This event occurs when a control is enabled.  You can generate this event
indirectly by calling 'enable'.  If you choose to override this function, be
sure to call the ancestor's counterpart at the beginning of your function.  The
base definition (below) sets a state variable and hides the hint.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tControl::onEnable()
{
    if(disabled==true)
    {
        disabled=false;
        resetHint();
    }
}

/****************************************************************************\
tControl::onHide

This event occurs when a control is hidden.  A hidden control is not drawn,
nor does it have a user-interface.  You can generate this event indirectly
by calling 'hide'.  If you choose to override this function, be sure to call
the ancestor's counterpart at the beginning of your function.  The base
definition (below) sets a state variable, deselects and defocuses the control,
and hides the hint.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tControl::onHide()
{
    if(visible==true)
    {
        visible=false;
        if(selected)
            onDeselect();
        if(focused)
            onDefocus();
        resetHint();
    }
}

/****************************************************************************\
tControl::onShow

This event occurs when a control is shown after it was formerly invisible.
You can generate this event indirectly by calling 'show'.  If you choose to
override this function, be sure to call the ancestor's counterpart at the
beginning of your function.  The base definition (below) sets a state variable.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tControl::onShow()
{
    visible = true;
}

/****************************************************************************\
tControl::onSetPosition

This event occurs when the control's position is changed, usually in response
to 'setPosition'.  If you choose to override this function, be sure to call the
ancestor's counterpart at the beginning of your function.  The base definition
(below) adjusts the control's position.

Inputs:
    _controlx, _controlY        Control's new position in absolute coordinates
                                    measured from bottom-left corner of screen/
                                    client area
Outputs:
    none
\****************************************************************************/
void tControl::onSetPosition(int _controlX, int _controlY)
{
    controlX=_controlX;
    controlY=_controlY;
}

/****************************************************************************\
tControl::onSetSize

This event occurs when the control's size is changed, usually in response to
'setSize'.  If you choose to override this function, be sure to call the
ancestor's counterpart at the beginning of your function.  The base definition
(below) adjusts the control's size.

Inputs:
    _width, _height         Control's new size measured from the control's
                                bottom-left corner
Outputs:
    none
\****************************************************************************/
void tControl::onSetSize(int _width, int _height)
{
    width=_width;
    height=_height;
}

/****************************************************************************\
tControl::onDestroy

This event occurs when the control is destroyed, usually in response to
'destroy'.  At one point, I deleted the control immediately, but this created
a dangling pointer nightmare, so I decided to implement a "lazy delete" system.
Once you generate this event, the control becomes invisble until it is deleted
during the next pass.  If you choose to override this function, be sure to call the
ancestor's counterpart at the beginning of your function.  The base definition
(below) sets a state variable and hides the control.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tControl::onDestroy()
{
    if(destroyed==false)
    {
        destroyed=true;
        if(visible)
            onHide();
    }
}

/****************************************************************************\
tControl::isType

Determines if this control is a descendent of the specified type.  The function
iterates through the control's ancestors and compares each with the specified
type.

Inputs:
    c           CTRL_BUTTON, CTRL_TOGGLE, etc.
Outputs:
    Returns true if the control is a descendent
\****************************************************************************/
bool tControl::isType(eControl c)
{
    eControl t=type;
#ifdef DEBUG
    if(c<0||c>=CTRL_MAXIMUM||t<0||t>=CTRL_MAXIMUM)
    {
        bug("tControl::isType: invalid control type");
        return false;
    }
#endif
    while(t!=CTRL_NONE)
    {
        if(c==t)
            return true;
        t=ancestors[t];
    }
    return false;
}

/****************************************************************************\
tControl::setLockSelect

Sets the control's lock select state.  If false, the control behaves normally.
If true, the control remains selected when the user moves the mouse away from
the control (button state does not matter).  The control does not lose focus
until the user -clicks- elsewhere on the window or screen.  We use this
property exclusively for combo boxes.  We want the drop down box to remain
visible until the user clicks outside the control.

Inputs:
    _lockSelect         New lock select state
Outputs:
    none
\****************************************************************************/
void tControl::setLockSelect(bool _lockSelect)
{
    lockSelect=_lockSelect;
    if(parent)
        parent->setLockSelect(lockSelect);
}

/****************************************************************************\
tControl::setParent

Sets the control's parent.  The control's parent is typically the control or
window that it acts on.  Although the parent only needs to be set once
during construction, I don't the parent through the constructor to avoid
clutter.

Inputs:
    p           Valid pointer to any descendent of tControl
Outputs:
    none
\****************************************************************************/
void tControl::setParent(tControl *p)
{
    parent=p;
}



/* This macro defines the default behavior for common functions in embedded
 * controls.  For instance, when you write, control->show(), the computer
 * may call control->parent->showControl(control), which will in turn call
 * control->onShow().  This allows the control's parent to update state variables
 * (such as selectedControl and focusedControl) before calling the control's
 * event.  If the control does not have a parent, the macro will simply call
 * the control's event (e.g. control->onShow()).
 */
DEFINE_CTRL_FUNCTION(focus(),       focusControl(this),     onFocus())
DEFINE_CTRL_FUNCTION(defocus(),     defocusControl(this),   onDefocus())
DEFINE_CTRL_FUNCTION(disable(),     disableControl(this),   onDisable())
DEFINE_CTRL_FUNCTION(enable(),      enableControl(this),    onEnable())
DEFINE_CTRL_FUNCTION(show(),        showControl(this),      onShow())
DEFINE_CTRL_FUNCTION(hide(),        hideControl(this),      onHide())
DEFINE_CTRL_FUNCTION(setPosition(int controlX,int controlY),
                                    setControlPosition(this,controlX,controlY),
                                                            setPosition(controlX,controlY))
DEFINE_CTRL_FUNCTION(setSize(int width,int height),
                                    setControlSize(this,width,height),
                                                            setSize(width,height))
DEFINE_CTRL_FUNCTION(destroy(),     destroyControl(this),   onDestroy())



/****************************************************************************\
tStatic::draw

Calls virtual drawControl function if the control is visible.
\****************************************************************************/
void tStatic::draw()
{
    if(!isVisible())
        return;
    drawControl();
}

/****************************************************************************\
tStatic::tStatic

Creates and initializes a new tStatic object.  Since the control has no user-
interface, the function automatically disables it.  In addition, this control
cannot receive focus.

Inputs:
    controlX, controlY      Coordinates of control's lower-left corner
    width, height           Control's dimensions
\****************************************************************************/
tStatic::tStatic(int controlX, int controlY, int width, int height) :
    tControl(controlX, controlY, width, height, false)
{
    onDisable();
    type=CTRL_STATIC;
}



/****************************************************************************\
tButton::onMouseDown (event handler)

When the user presses the mouse over the control, it becomes depressed.
The control also executes if the executeOnDown flag is set.
\****************************************************************************/
void tButton::onMouseDown(int x, int y, eMouseButton button)
{
    tControl::onMouseDown(x,y,button);
    depressed=true;
    if(executeOnDown)
        onExecute();
}

/****************************************************************************\
tButton::onMouseMove (event handler)

Depresses or releases the control depending on the mouse position.  If the
user drags the mouse away from the control with at least one button pressed,
the control reverts to normal.  If the user drags the mouse onto the control
with at least one button pressed, the control becomes depressed.
\****************************************************************************/
void tButton::onMouseMove(int x, int y)
{
    tControl::onMouseMove(x,y);
    if(isMousePressed())
    {
        if(activated(x,y))
            depressed=true;
        else if(!activated(x,y))
            depressed=false;
    }
}

/****************************************************************************\
tButton::onMouseUp (event handler)

When the user releases the mouse, it reverts to normal.  The control also
executes if the executeOnDown flag is cleared and the mouse covers the control.
\****************************************************************************/
void tButton::onMouseUp(int x, int y, eMouseButton button)
{
    tControl::onMouseUp(x,y,button);
    if(depressed)
    {
        depressed=false;
        if(!executeOnDown)
            onExecute();
    }
}

/****************************************************************************\
tButton::onKeyDown (event handler)

If the control receives focus, the user can execute it by pressing enter.
\****************************************************************************/
void tButton::onKeyDown(tKey key, char ch)
{
    tControl::onKeyDown(key,ch);
    //Alternatively, show the button depressed momentarily:
    if(key == GLFW_KEY_ENTER)
        onExecute();
}

/****************************************************************************\
tButton::draw

Calls virtual drawControl function if the control is visible.
\****************************************************************************/
void tButton::draw()
{
    if(!isVisible())
        return;
    drawControl();
}

/****************************************************************************\
tButton::onDefocus (event handler)

Releases the control when it loses its focus.  NOTE: This will only occur if
the button is simultaneously focused and depressed with mouse.
\****************************************************************************/
void tButton::onDefocus()
{
    tControl::onDefocus();
    depressed=false;
}

/****************************************************************************\
tButton::onDeselect (event handler)

Releases the control when the user moves the mouse away from it.  This will only
occur if mouse button is released and control is deselected at the same instant.
\****************************************************************************/
void tButton::onDeselect()
{
    tControl::onDeselect();
    depressed=false;
}

/****************************************************************************\
tButton::tButton

Creates and initializes a new tButton object.

Inputs:
    controlX, controlY      Coordinates of control's lower-left corner
    width, height           Control's dimensions
    canFocus                Can the control receive focus?
    _executeOnDown          Will the button execute when the user presses it (true)
                                or releases it (false)?
\****************************************************************************/
tButton::tButton(int controlX, int controlY, int width, int height,
    bool canFocus, bool _executeOnDown) :
    tControl(controlX, controlY, width, height, canFocus), depressed(false),
    executeOnDown(_executeOnDown)
{
    type = CTRL_BUTTON;
}



/****************************************************************************\
tToggle::onMouseDown (event handler)

Toggles the check box and generates an onChange event.
\****************************************************************************/
void tToggle::onMouseDown(int x, int y, eMouseButton button)
{
    tControl::onMouseDown(x,y,button);
    checked = !checked;
    onChange();
}

/****************************************************************************\
tToggle::onKeyDown (event handler)

Toggles the check box and generates an onChange event when the user presses
any key.  We only pay attention to keys with an ASCII code (e.g. 'A' and space).
\****************************************************************************/
void tToggle::onKeyDown(tKey key, char ch)
{
    tControl::onKeyDown(key,ch);
    if(key<GLFW_KEY_SPECIAL)
    {
        checked = !checked;
        onChange();
    }
}

/****************************************************************************\
tToggle::draw()

Calls virtual drawControl function if the control is visible.
\****************************************************************************/
void tToggle::draw()
{
    if(!isVisible())
        return;
    drawControl();
}

/****************************************************************************\
tToggle::set

Manually sets the toggle button's state from outside the class.  The function
does not generate an onChange event.

Inputs:
    _checked        Toggle button's new state
Outputs:
    none
\****************************************************************************/
void tToggle::set(bool _checked)
{
    checked = _checked;
}

/****************************************************************************\
tToggle::tToggle

Creates and initializes a new tToggle object.

Inputs:
    controlX, controlY      Coordinates of control's lower-left corner
    width, height           Control's dimensions
    canFocus                Can the control receive focus?
    _checked                Is the control initially checked?
\****************************************************************************/
tToggle::tToggle(int controlX, int controlY, int width, int height, bool canFocus,
    bool _checked) : tControl(controlX, controlY, width, height, canFocus),
    checked(_checked)
{
    type = CTRL_TOGGLE;
}



/****************************************************************************\
tRadio::set

Sets this radio button and clears the other radio buttons within the same
group.  Call this function to set the control from outside the class.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tRadio::set()
{
    //If the radio button is already checked, no action is needed:
    if(!isChecked())
    {
        //Check this radio button:
        tToggle::set(true);

        //Ensure that parent is a descendent of tWindow:
        if(getParent() && getParent()->isType(CTRL_WINDOW))
        {
            tWindow *w = (tWindow *) getParent();

            //Iterate through each control in this window and clear radio
            //buttons with the same group ID (excluding this one).
            for(tControl *current = w->getFirstControl(); current; current=w->getNextControl())
            {
                if(current->isType(CTRL_RADIO) && ((tRadio *) current)->group == group &&
                    ((tRadio *) current) != this)
                    ((tToggle *) current)->set(false);
            }
        }
    }
}

/****************************************************************************\
tRadio::onMouseDown

Sets this radio button and clears the other radio buttons within the same
group.  Also, generates onChange event if the control was formerly unchecked.
\****************************************************************************/
void tRadio::onMouseDown(int x, int y, eMouseButton button)
{
    //Preserve the former state of the radio button:
    bool oldstate = isChecked();
    tControl::onMouseDown(x,y,button);
    set();
    if(!oldstate)
        onChange();
}

/****************************************************************************\
tRadio::onKeyDown

Sets this radio button and clears the other radio buttons within the same
group when the user presses any key.  Also, generates an onChange event if the
control was formerly unchecked.  We only pay attention to keys with an ASCII
code (e.g. 'A' and space).
\****************************************************************************/
void tRadio::onKeyDown(tKey key, char ch)
{
    //Preserve the former state of the radio button:
    bool oldstate = isChecked();
    tControl::onKeyDown(key,ch);
    if(key<GLFW_KEY_SPECIAL)
    {
        set();
        if(!oldstate)
            onChange();
    }
}

/****************************************************************************\
tRadio::tRadio

Creates and initializes a new tRadio object.

Inputs:
    controlX, controlY      Coordinates of control's lower-left corner
    width, height           Control's dimensions
    canFocus                Can the control receive focus?
    _checked                Is the control initially checked?
    _group                  To which group does the control belong?
\****************************************************************************/
tRadio::tRadio(int controlX, int controlY, int width, int height, bool canFocus,
    bool isChecked, int _group) : tToggle(controlX, controlY, width, height,
    canFocus, isChecked), group(_group)
{
    type = CTRL_RADIO;
}



/****************************************************************************\
tScrollBar::tScrollBar

Creates and initializes a new tScrollBar object.

Inputs:
    controlX, controlY      Coordinates of control's lower-left corner
    width, height           Control's dimensions
    canFocus                Can the control receive focus?
    _vertical               Is the control vertical (true) or horizontal (false)?
    _position               Initial position within the medium
                                (e.g. the current row in a text box)
    _range                  Length of the medium
                                (e.g. the total number of rows in a text box)
    _pageSize               Amount of the medium visible at one time
                                (e.g. the number of rows that will fit inside a text box)
                                Set this value to zero for independent scroll bars.
    _scrollRate             Amount by which to scroll the medium when the
                                user presses the up or down arrow
    _arrowLength            Height of each arrow for vertical scroll bars
                                The width of each arrow for horizontal scroll bars
    _minMarkerLength        Minimum length (in pixels) that the marker can assume
\****************************************************************************/
tScrollBar::tScrollBar(int controlX, int controlY, int width, int height, bool canFocus,
    bool _vertical, long _position, long _range, long _pageSize, long _scrollRate,
    int _arrowLength, int _minMarkerLength) :
    tControl(controlX, controlY, width, height, canFocus),
    vertical(_vertical), position(_position), range(_range), pageSize(_pageSize),
    scrollRate(_scrollRate), arrowLength(_arrowLength), minMarkerLength(_minMarkerLength)
{
    type=CTRL_SCROLL_BAR;

    //The user has not clicked on the scroll bar.
    fastDecDepressed=false;
    decDepressed=false;
    markerDepressed=false;
    incDepressed=false;
    fastIncDepressed=false;

    //Initialize the marker's position and size:
    updateMarker();
}

/****************************************************************************\
tScrollBar::updateMarker

Refreshes the scroll bar based on position, pageSize, and range.  The function
disables the scroll bar if the whole range is visible or there is insufficient
space.  It then calculates the position and size of the scroll marker.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tScrollBar::updateMarker()
{
    //If the whole range is visible or there isn't enough room for two arrows
    //or the range is zero, then disable scroll bar.
    if(maxPosition() == 0 || trackLength() < 0 || range==0)
    {
        disable();
        markerLength = 0;
        markerPosition = 0;
        return;
    }

    //Otherwise, make sure control is enabled:
    enable();

    //Ensure that the position is within range:
    clampRange(position, 0l, maxPosition());

    //If there isn't enough space for the marker, then don't show it.
    //Descendents should not draw the marker if markerLength equals zero.
    if(trackLength() < minMarkerLength)
    {
        markerLength=0;
        markerPosition=0;
    }
    else
    {
        //Find the marker's length and ensure it is greater than or equal to
        //minMarkerLength:
        markerLength=pageSize * trackLength() / range;
        if(markerLength < minMarkerLength)
            markerLength = minMarkerLength;

        //Find the marker's position.  Calculation for vertical and horizontal
        //scroll bars differs due to coordinate system:
        if(vertical)
            markerPosition = length() - arrowLength - position * movementRange() / maxPosition() - markerLength;
        else
            markerPosition = arrowLength + position * movementRange() / maxPosition();
    }
}

/****************************************************************************\
tScrollBar::calculatePosition

This function is the inverse of updateMarker, so to speak.  It calculates the
position based on the scroll marker's offset.  We use this function to scroll
the medium when the user drags the scroll marker.

Inputs:
    markerPos       Scroll marker's offset from the control's lower or left edge
Outputs:
    Returns zero if the scroll marker is not visible
    Otherwise, returns a value between zero and maxPosition()
\****************************************************************************/
long tScrollBar::calculatePosition(int markerPos)
{
    //Ensure that marker position fits within upper and lower boundaries:
    clampRange(markerPos, arrowLength, trackLength()-markerLength+arrowLength);

    //If the marker is not visible (either there's not enough space or the whole
    //range is visible) then return 0.
    if (maxPosition() == 0 || movementRange() == 0)
        return 0;

    //Calculation for vertical and horizontal scroll bars differs due to
    //coordinate system.
    if(vertical)
    {
        if(markerPos==arrowLength)  //This extra condition ensures that user can see
            return maxPosition();   //bottom of medium when marker is fully lowered.
        else
            return ROUND((float)(movementRange() - markerPos + arrowLength) * maxPosition() / movementRange());
    }
    else
    {
        if(markerPos-arrowLength==movementRange())  //This extra condition ensures that user can see
            return maxPosition();                   //right of medium when marker is at far right.
        else
            return ROUND((float)(markerPos-arrowLength) * maxPosition() / movementRange());
    }
}

/****************************************************************************\
tScrollBar::setScrollPosition

Modifies the position from outside the class and refreshes the scroll bar.
The position must fit between zero and range-pageSize.

Inputs:
    _position       New position within the medium
Outputs:
    none
\****************************************************************************/
void tScrollBar::setScrollPosition(long _position)
{
    position=_position;
    updateMarker();
}

/****************************************************************************\
tScrollBar::setScrollRange

Modifies the range from outside the class and refreshes the scroll bar.  If the
former position no longer fits within the range, the function adjusts it.

Inputs:
    _range          New range within the medium
\****************************************************************************/
void tScrollBar::setScrollRange(long _range)
{
    range=_range;
    updateMarker();
}

/****************************************************************************\
tScrollBar::setPageSize

Modifies the page size from outside the class and refreshes the scroll bar.  If
the former position no longer fits within the range, the function adjusts it.

Inputs:
    _pageSize       New page size within the medium
Outputs:
    none
\****************************************************************************/
void tScrollBar::setPageSize(long _pageSize)
{
    pageSize=_pageSize;
    updateMarker();
}

/****************************************************************************\
tScrollBar::setValues

Modifies the position, page size, and range all at once and refreshes the scroll
bar.  We use this function to avoid multiple updates.  If the former position no
longer fits within the range, the function adjusts it.

Inputs:
    _position       New position within the medium
    _pageSize       New page size within the medium
    _range          New range within the medium
Outputs:
    none
\****************************************************************************/
void tScrollBar::setValues(long _position, long _pageSize, long _range)
{
    position=_position;
    pageSize=_pageSize;
    range=_range;
    updateMarker();
}

/****************************************************************************\
tScrollBar::onSetSize

Refreshes the scroll bar when the user resizes it.
\****************************************************************************/
void tScrollBar::onSetSize(int w, int h)
{
    tControl::onSetSize(w,h);
    updateMarker();
}

/****************************************************************************\
tScrollBar::onMouseDown

Calls one of up(), down(), pageUp() and pageDown() depending on where the user
pressed the mouse.  Sets one of the five 'depressed' flags.  Initializes
mouseDownTime and anchor in the event that the user holds the mouse down for
more than 0.5 seconds.
\****************************************************************************/
void tScrollBar::onMouseDown(int x, int y, eMouseButton button)
{
    tControl::onMouseDown(x,y,button);
    int s, length;

    //Ignore mouse press if disabled:
    if(isDisabled())
        return;

    //Set timer for repeat scroll after delay:
    mouseDownTime = currentTime;

    //Use s and length so same code can be used for both vertical and horizontal
    //scroll bar.  Shift x and y so they are relative to scroll bar.
    if(vertical)
    {
        s=y-=getControlY();
        length=getHeight();
    }
    else
    {
        s=x-=getControlX();
        length=getWidth();
    }

    if(s<arrowLength)   //The user pressed an arrow.
    {
        //Due to the coordinate system, the orientation of horizontal and vertical
        //scroll bars is reversed.
        if(vertical)
        {
            incDepressed=true;
            down();
        }
        else
        {
            decDepressed=true;
            up();
        }
    }
    else if(markerLength>0 && s<markerPosition) //The user pressed the region between the
                                                //arrow and the marker.
    {
        if(vertical)
        {
            fastIncDepressed=true;
            pageDown();
        }
        else
        {
            fastDecDepressed=true;
            pageUp();
        }
    }
    else if(markerLength>0 && s<markerPosition+markerLength)    //The user pressed the marker.
    {
        markerDepressed=true;
        //Anchor is a convenient way of calculating new marker position based on mouse position:
        //<new marker position> = <mouse position> + anchor
        anchor=markerPosition - s;
    }
    else if(markerLength>0 && s<length-arrowLength) //The user pressed the region between
                                                    //the arrow and the marker.
    {
        if(vertical)
        {
            fastDecDepressed=true;
            pageUp();
        }
        else
        {
            fastIncDepressed=true;
            pageDown();
        }
    }
    else if(s>=length-arrowLength)  //The user pressed an arrow.
    {
        if(vertical)
        {
            decDepressed=true;
            up();
        }
        else
        {
            incDepressed=true;
            down();
        }
    }
}

/****************************************************************************\
tScrollBar::onMouseMove

Adjusts the scroll marker when the user drags it.  If the user has moved it
to a new location, then the function updates the position, calls updateMarker,
and generates an onChange event.
\****************************************************************************/
void tScrollBar::onMouseMove(int x, int y)
{
    tControl::onMouseMove(x,y);
    int s;
    //Determine if you are dragging the scroll marker:
    if(!isDisabled() && markerDepressed)
    {
        //Adjust x and y so they are relative to scroll bar:
        if(vertical)
            s=y-=getControlY();
        else
            s=x-=getControlX();


        //Calculate the new value of position:
        int newpos=calculatePosition(s+anchor);

        //Update the marker only if position has changed to reduce computations:
        if(newpos!=position)
        {
            position=newpos;
            //We call updateMarker here so that the scroll marker "snaps" from
            //one position to the next if the scroll bar's length exceeds the
            //range.
            updateMarker();
            onChange();
        }
    }
}

/****************************************************************************\
tScrollBar::onMouseUp

Ends all scrolling when you release the mouse.
\****************************************************************************/
void tScrollBar::onMouseUp(int x, int y, eMouseButton button)
{
    tControl::onMouseUp(x,y,button);
    fastDecDepressed=false;
    decDepressed=false;
    markerDepressed=false;
    incDepressed=false;
    fastIncDepressed=false;
    fastScroll=false;
}

/****************************************************************************\
tScrollBar::draw

Calls virtual drawControl function if the control is visible.
\****************************************************************************/
void tScrollBar::draw()
{
    if(!isVisible())
        return;
    drawControl();
}

/****************************************************************************\
tScrollBar::onKeyDown

Scrolls the control when the user presses a directional key.  The keyboard
interface applies to focusable controls only.  Notice that left/right and
up/down have the same effect, combining the behavior of vertical and horizontal
scroll bars.
\****************************************************************************/
void tScrollBar::onKeyDown(tKey key, char ch)
{
    tControl::onKeyDown(key,ch);
    switch(key)
    {
        case GLFW_KEY_RIGHT:
        case GLFW_KEY_DOWN:
            down();
            break;
        case GLFW_KEY_LEFT:
        case GLFW_KEY_UP:
            up();
            break;
        case GLFW_KEY_PAGEUP:
            pageUp();
            break;
        case GLFW_KEY_PAGEDOWN:
            pageDown();
            break;
        case GLFW_KEY_HOME:
            home();
            break;
        case GLFW_KEY_END:
            end();
            break;
    }
}

/****************************************************************************\
tScrollBar::pageUp

Subtracts pageSize from position, updates the marker, and generates an onChange
event.  Call this function from outside the class to simulate a Page Up key press.

Inputs:
    none
Outputs:
    Returns true if position has changed
\****************************************************************************/
bool tScrollBar::pageUp()
{
    long amt;
    //If position equals zero, we cannot decrease it any further.
    if(position>0)
    {
        //Allow fast scroll for independent scroll bars without page sizes:
        if(pageSize>0)
            amt=pageSize;
        else
            amt=scrollRate;
        //Ensure that position does not fall below zero:
        position=MAX(position-amt,0);
        updateMarker();
        onChange();
        return true;
    }
    return false;
}

/****************************************************************************\
tScrollBar::up

Subtracts scrollRate from position, updates the marker, and generates an onChange
event.  Call this function from outside the class to simulate an Up key press.

Inputs:
    none
Outputs:
    Returns true if position has changed
\****************************************************************************/
bool tScrollBar::up()
{
    //If position equals zero, we cannot decrease it any further.
    if(position>0)
    {
        //Ensure that position does not fall below zero:
        position=MAX(position-scrollRate,0);
        updateMarker();
        onChange();
        return true;
    }
    return false;
}

/****************************************************************************\
tScrollBar::down

Adds scrollRate to position, updates the marker, and generates an onChange
event.  Call this function from outside the class to simulate an Down key press.

Inputs:
    none
Outputs:
    Returns true if position has changed
\****************************************************************************/
bool tScrollBar::down()
{
    //If position equals maxPosition(), we cannot increase it any further.
    if(position<maxPosition())
    {
        //Ensure that position does not rise above maxPosition:
        position=MIN(position+scrollRate,maxPosition());
        updateMarker();
        onChange();
        return true;
    }
    return false;
}

/****************************************************************************\
tScrollBar::pageDown

Adds pageSize to position, updates the marker, and generates an onChange
event.  Call this function from outside the class to simulate a Page Down key
press.

Inputs:
    none
Outputs:
    Returns true if position has changed
\****************************************************************************/
bool tScrollBar::pageDown()
{
    long amt;
    //If position equals maxPosition(), we cannot increase it any further.
    if(position<maxPosition())
    {
        //Allow fast scroll for independent scroll bars without page sizes:
        if(pageSize>0)
            amt=pageSize;
        else
            amt=scrollRate;
        //Ensure that position does not rise above maxPosition:
        position=MIN(position+amt,maxPosition());
        updateMarker();
        onChange();
        return true;
    }
    return false;
}

/****************************************************************************\
tScrollBar::home

Sets position to zero, updates the marker, and generates an onChange event.
Call this function from outside the class to simulate a Home key press.

Inputs:
    none
Outputs:
    Returns true if position has changed
\****************************************************************************/
bool tScrollBar::home()
{
    //If position already equals zero, there is no point in setting it.
    if(position>0)
    {
        position=0;
        updateMarker();
        onChange();
        return true;
    }
    return false;
}

/****************************************************************************\
tScrollBar::home

Sets position to maxPosition(), updates the marker, and generates an onChange
event.  Call this function from outside the class to simulate an End key press.

Inputs:
    none
Outputs:
    Returns true if position has changed
\****************************************************************************/
bool tScrollBar::end()
{
    //If position already equals maxPosition(), there is no point in setting it.
    if(position<maxPosition())
    {
        position=maxPosition();
        updateMarker();
        onChange();
        return true;
    }
    return false;
}

/****************************************************************************\
tScrollBar::poll

After you hold down a button for 0.5 seconds, the function sets fastScroll.
fastScroll causes the button to execute repeatedly 20 times each second.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tScrollBar::poll()
{
    tControl::poll();

    if(fastScroll)
    {
        if(currentTime - mouseDownTime > 0.05)
        {
            mouseDownTime = currentTime;
            if(fastDecDepressed && position>0)
                pageUp();
            else if(decDepressed && position>0)
                up();
            else if(incDepressed && position<maxPosition())
                down();
            else if(fastIncDepressed && position<maxPosition())
                pageDown();
        }
    }
    else if((fastDecDepressed || decDepressed || incDepressed || fastIncDepressed) &&
        currentTime - mouseDownTime > 0.5)
        fastScroll=true;
}

/****************************************************************************\
tScrollBar::onEnable

Enables the scroll bar -if- it can be enabled (whole range is not visible and
there is enough space).
\****************************************************************************/
void tScrollBar::onEnable()
{
    if(maxPosition()>0 && trackLength()>=0)
        tControl::onEnable();
}

/****************************************************************************\
tScrollBar::length

Determines the length of the scroll bar parallel to its major axis.  If it's
vertical, then length equals height; if it's horizontal, then length equals
width.

Inputs:
    none
Outputs:
    Returns scroll bar's length
\****************************************************************************/
int tScrollBar::length()
{
    if(vertical)
        return getHeight();
    else
        return getWidth();
}

/****************************************************************************\
tScrollBar::trackLength

Finds the distance between opposite arrows.  This value is the range in which the
scroll marker can slide.

Inputs:
    none
Outputs:
    Returns the track length
\****************************************************************************/
int tScrollBar::trackLength()
{
    return  length() - arrowLength * 2;
}

/****************************************************************************\
tScrollBar::movementRange

Finds the maximum distance you can slide the scroll marker from one end to the
other.  This value equals the track length minus the marker's length.

Inputs:
    none
Outputs:
    Returns zero if the user cannot move the marker
    Otherwise, returns the movement range
\****************************************************************************/
int tScrollBar::movementRange()
{
    if(trackLength() - markerLength > 0)
        return trackLength() - markerLength;
    else
        return 0;
}

/****************************************************************************\
tScrollBar::maxPosition

Finds the maximum value that position can equal.  The value equals the range
minus the pageSize.  If pageSize equals zero, then the value equals the range.

Inputs:
    none
Outputs:
    Returns zero if the media cannot be scrolled (pageSize > range)
    Otherwise, returns the maximum position
\****************************************************************************/
long tScrollBar::maxPosition()
{
    if(range - pageSize > 0)
        return range - pageSize;
    else
        return 0;
}



/****************************************************************************\
tWindowScrollBar::onChange

Notifies the scroll bar's parent when its position changes.  We override this
event to call the parent's onUpdatePosition function.
\****************************************************************************/
void tWindowScrollBar::onChange()
{
    ((tScrollWindow *)getParent())->onUpdatePosition();
}

/****************************************************************************\
tWindowScrollBar::enable

Instructs the scroll bar's parent to enable it.  If it does not have a parent,
then it calls onEnable.  We must override this function since the default
definition for 'enable' does not apply to tWindowScrollBar.
\****************************************************************************/
void tWindowScrollBar::enable()
{
    if(getParent())
        ((tScrollWindow *) getParent())->enableScrollBar(this);
    else
        onEnable();
}

/****************************************************************************\
tWindowScrollBar::disable

Instructs the scroll bar's parent to disable it.  If it does not have a parent,
then it calls onDisable.  We must override this function since the default
definition for 'disable' does not apply to tWindowScrollBar.
\****************************************************************************/
void tWindowScrollBar::disable()
{
    if(getParent())
        ((tScrollWindow *) getParent())->disableScrollBar(this);
    else
        onDisable();
}

/****************************************************************************\
tWindowScrollBar::onHide

tScrollWindow has no reason to hide its scroll bars, so disable this function.
NOTE:  If a parent becomes invisible, its children will become invisible also,
since the parent is responsible for drawing its children.
\****************************************************************************/
void tWindowScrollBar::onHide()
{
    bug("tWindowScrollBar::onHide: this control doesn't support onHide");
}

/****************************************************************************\
tWindowScrollBar::onShow

tScrollWindow has no reason to hide its scroll bars, so disable this function.
NOTE:  If a parent becomes invisible, its children will become invisible also,
since the parent is responsible for drawing its children.
\****************************************************************************/
void tWindowScrollBar::onShow()
{
    bug("tWindowScrollBar::onShow: this control doesn't support onShow");
}

/****************************************************************************\
tWindowScrollBar::tWindowScrollBar

Creates and initializes a new tWindowScrollBar object.

Inputs:
    controlX, controlY      Coordinates of control's lower-left corner
    length                  Length of control's major axis
    isVertical              Is the control vertical (true) or horizontal (false)?
    position                Initial position within the medium
                                (e.g. the current row in a text box)
    range                   Length of the medium
                                (e.g. the total number of rows in a text box)
    pageSize                Amount of the medium visible at one time
                                (e.g. the number of rows that will fit inside a text box)
    scrollRate              Amount by which to scroll the medium when the
                                user presses the up or down arrow
\****************************************************************************/
tWindowScrollBar::tWindowScrollBar(int controlX, int controlY, int length, bool isVertical,
    long position, long range, long pageSize, long scrollRate) :
    tStdScrollBar(controlX, controlY, length, false, isVertical, position,
    range, pageSize, scrollRate)
{
    type = CTRL_WINDOW_SCROLL_BAR;
}



/****************************************************************************\
tScrollWindow::tScrollWindow

Creates and initializes a new tScrollWindow object.  If you want to attach one
or more scroll bars to this control, you must call createHorScrollBar and/or
createVertScrollBar.

Inputs:
    controlX, controlY      Coordinates of control's lower-left corner
    width, height           Control's dimensions
    canFocus                Can the control receive focus?
    _windowX, _windowY      Offset of window's contents:
                                As you scroll right, windowX will increase.
                                As you scroll down, windowY will increase.
    _windowWidth,           Dimensions of window's contents
        _windowHeight
\****************************************************************************/
tScrollWindow::tScrollWindow(int controlX, int controlY, int width, int height,
    bool canFocus, int _windowX, int _windowY, int _windowWidth,
    int _windowHeight) : tControl(controlX, controlY, width, height, canFocus),
    windowX(_windowX), windowY(_windowY),
    windowWidth(_windowWidth), windowHeight(_windowHeight)
{
    type=CTRL_SCROLL_WINDOW;

    //Initially, scroll window does not have scroll bars:
    vertScrollBar = NULL;
    horScrollBar = NULL;

    //User has not selected the main window yet:
    windowSelected = false;
}

/****************************************************************************\
tScrollWindow::onUpdatePosition

This function interfaces with tWindowScrollBar.  It pans the window's contents
when the user adjusts the horizontal or vertical scroll bar.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tScrollWindow::onUpdatePosition()
{
    //Assign the scroll bars' position to windowX and/or windowY:
    if(horScrollBar)
        windowX=horScrollBar->getPosition();
    if(vertScrollBar)
        windowY=vertScrollBar->getPosition();
}

/****************************************************************************\
tScrollWindow::onDisable

In addition to disabling the main window, this function also disables its
children.
\****************************************************************************/
void tScrollWindow::onDisable()
{
    tControl::onDisable();
    if(horScrollBar)
        horScrollBar->onDisable();
    if(vertScrollBar)
        vertScrollBar->onDisable();
}

/****************************************************************************\
tScrollWindow::onEnable

In addition to enabling the main window, this function also attempts to enable
its children.  NOTE: A scroll bar may remain disabled if there is not enough
space or the whole range is visible.
\****************************************************************************/
void tScrollWindow::onEnable()
{
    tControl::onEnable();
    if(horScrollBar)
        horScrollBar->onEnable();
    if(vertScrollBar)
        vertScrollBar->onEnable();
}

/****************************************************************************\
tScrollWindow::setWindowPosition

Manually sets the window's position from outside the class.  The function
assigns the new values to windowX and windowY, and adjusts the scroll bars
accordingly.  If the supplied values cause the view to exceed the window's
boundaries, then it adjusts the scroll bars' range to accomodate the overflow.

Inputs:
    _x, _y          New values from windowX and windowY.  You must supply
                        non-negative values.
Outputs:
    none
\****************************************************************************/
void tScrollWindow::setWindowPosition(int _x, int _y)
{
    windowX = _x;
    windowY = _y;
    if(windowX<0)
        windowX=0;
    if(windowY<0)
        windowY=0;
    if(horScrollBar)
        horScrollBar->setValues(windowX,visWindowWidth(),MAX(windowX+visWindowWidth(),windowWidth));
    if(vertScrollBar)
        vertScrollBar->setValues(windowY,visWindowHeight(),MAX(windowY+visWindowHeight(),windowHeight));
}

/****************************************************************************\
tScrollWindow::setWindowSize

Manually sets the window's dimensions from outside the class.  The function
assigns the new values to windowWidth and windowHeight and adjusts the scroll
bars accordingly.   If the supplied values cause the view to exceed the window's
boundaries, then it adjusts the scroll bars' range to accomodate the overflow.

Inputs:
    _width, _height     New values for windowWidth and windowHeight.  You must
                            supply non-negative values.
Outputs:
    none
\****************************************************************************/
void tScrollWindow::setWindowSize(int _width, int _height)
{
    windowWidth = _width;
    windowHeight = _height;
    if(windowWidth<0)
        windowWidth=0;
    if(windowHeight<0)
        windowHeight=0;
    if(horScrollBar)
        horScrollBar->setValues(windowX,visWindowWidth(),MAX(windowX+visWindowWidth(),windowWidth));
    if(vertScrollBar)
        vertScrollBar->setValues(windowY,visWindowHeight(),MAX(windowY+visWindowHeight(),windowHeight));
}

/****************************************************************************\
tScrollWindow::onSetPosition

In addition to repositioning the main window, this function repositions the
scroll bars.  Since we are merely moving the control, we need not resize or
update the scroll bars.
\****************************************************************************/
void tScrollWindow::onSetPosition(int x, int y)
{
    //Added 5/10/03 so that descendents can place scroll bars wherever they like:
    int dx=x-getControlX();
    int dy=y-getControlY();
    tControl::onSetPosition(x,y);
    if(horScrollBar)
        horScrollBar->onSetPosition(horScrollBar->getControlX()+dx,horScrollBar->getControlY()+dy);
    if(vertScrollBar)
        vertScrollBar->onSetPosition(vertScrollBar->getControlX()+dx,vertScrollBar->getControlY()+dy);
}

/****************************************************************************\
tScrollWindow::onSetSize

In addition to resizing the main window, this function resizes and updates
the scroll bars.  Due to the coordinate system, we must reposition the
vertical scroll bar also.
\****************************************************************************/
void tScrollWindow::onSetSize(int w, int h)
{
    //Added 5/10/03 so that descendents can place scroll bars wherever they like:
    int dw=w-getWidth();
    int dh=h-getHeight();
    tControl::onSetSize(w,h);
    if(horScrollBar)
    {
        horScrollBar->onSetSize(horScrollBar->getWidth()+dw,horScrollBar->getHeight());
        horScrollBar->setValues(windowX,visWindowWidth(),MAX(windowX+visWindowWidth(),windowWidth));
    }
    if(vertScrollBar)
    {
        vertScrollBar->onSetPosition(vertScrollBar->getControlX()+dw,vertScrollBar->getControlY());
        vertScrollBar->onSetSize(vertScrollBar->getWidth(),vertScrollBar->getHeight()+dh);
        vertScrollBar->setValues(windowY,visWindowHeight(),MAX(windowY+visWindowHeight(),windowHeight));
    }
}

/****************************************************************************\
tScrollWindow::draw

Draws the control's border, contents, and children if the control is visible.
\****************************************************************************/
void tScrollWindow::draw()
{
    if(!isVisible())
        return;
    drawBorder();
    drawContents();
    if(vertScrollBar)
        vertScrollBar->draw();
    if(horScrollBar)
        horScrollBar->draw();
}

/****************************************************************************\
tScrollWindow::checkXY

Ensures that the specified point is visible in the current view.  If the point
is not visible, then the function adjusts the view so that the point is barely
visible.  'x' and 'y' may exceed windowWidth and windowHeight respectively,
but they cannot be negative.

Inputs:
    x, y            Coordinates of point to examine within window's contents.
                        NOTE: The origin is at the upper-left corner with
                        the y-axis pointing down.  You must supply non-negative
                        values.
Outputs:
    none
\****************************************************************************/
void tScrollWindow::checkXY(int x, int y) //coordinates relative to scroll canvas
{
    //Keep track of whether windowX and windowY have been modified to determine
    //whether to update scroll bars.
    bool foundX=false,foundY=false;

    //Adjust the view the minimum amount so that the point become visible:
    if(x < windowX)
    {
        windowX=MAX(x,0);
        foundX=true;
    }
    if(x > windowX + visWindowWidth())
    {
        windowX = x - visWindowWidth();
        foundX=true;
    }
    if(y < windowY)
    {
        windowY=MAX(y,0);
        foundY=true;
    }
    if(y > windowY + visWindowHeight())
    {
        windowY = y - visWindowHeight();
        foundY=true;
    }

    //For each scroll bar, determine whether a call to setValues is necessary
    //and adjust range to accomodate overflow:
    long newWidth = MAX(windowX+visWindowWidth(),windowWidth);
    long newHeight = MAX(windowY+visWindowHeight(),windowHeight);

    if(horScrollBar && (foundX || newWidth!=horScrollBar->getRange()))
        horScrollBar->setValues(windowX,visWindowWidth(),newWidth);
    if(vertScrollBar && (foundY || newHeight!=vertScrollBar->getRange()))
        vertScrollBar->setValues(windowY,visWindowHeight(),newHeight);
}

/****************************************************************************\
tScrollWindow::checkMousePosition

Determines which part of the control is selected.  If the main window is
selected, it sets windowSelected.  If the horizontal scroll bar is selected,
it sets horSelected.  If the vertical scroll bar is selected, it sets
vertSelected.  It also calls onSelectWindow when the main window is selected
and onDeselectWindow when the main window is deselected.

Inputs:
    x, y            Absolute mouse coordinates
Outputs:
    none
\****************************************************************************/
void tScrollWindow::checkMousePosition(int x, int y)
{
    if(vertScrollBar && vertScrollBar->activated(x,y))
        vertSelected=true;
    else
        vertSelected=false;
    if (horScrollBar && horScrollBar->activated(x,y))
        horSelected=true;
    else
        horSelected=false;
    if(vertSelected || horSelected)
    {
        if(windowSelected)
        {
            windowSelected = false;
            onDeselectWindow();
        }
        return;
    }
    else if (!windowSelected)
    {
        windowSelected = true;
        onSelectWindow();
    }
}

/****************************************************************************\
tScrollWindow::onMouseDown

Adjusts the selection and passes the event to a scroll bar if selected.
NOTE: The main window always receives onMouseDown events even if windowSelected
is false.  Therefore, descendents should write this at the beginning of mouse
events:

    if(vertSelected || horSelected)
        return;
\****************************************************************************/
void tScrollWindow::onMouseDown(int x, int y, eMouseButton button)
{
    tControl::onMouseDown(x,y,button);
    checkMousePosition(x,y);
    if(vertSelected && vertScrollBar)
        vertScrollBar->onMouseDown(x,y,button);
    if(horSelected && horSelected)
        horScrollBar->onMouseDown(x,y,button);
}

/****************************************************************************\
tScrollWindow::onMouseMove

As long as the user holds down the mouse, the main window, horizontal scroll bar,
or vertical scroll bar retains its selection and continues to receive onMouseMove
events.  When the user releases the mouse, the function adjusts the selection
and passes the onMouseMove event to the appropriate component.  NOTE: The main
window always receives onMouseMove events even if windowSelected is false.
Therefore, descendents should write this at the beginning of mouse events:

    if(vertSelected || horSelected)
        return;
\****************************************************************************/
void tScrollWindow::onMouseMove(int x, int y)
{
    tControl::onMouseMove(x,y);
    if(isMousePressed())
    {
        if (vertSelected && vertScrollBar)
            vertScrollBar->onMouseMove(x,y);
        if (horSelected && horScrollBar)
            horScrollBar->onMouseMove(x,y);
    }
    else
    {
        checkMousePosition(x,y);
        if(vertSelected && vertScrollBar)
            vertScrollBar->onMouseMove(x,y);
        if(horSelected && horSelected)
            horScrollBar->onMouseMove(x,y);
    }
}

/****************************************************************************\
tScrollWindow::onMouseUp

Passes event to scroll bar if selected and adjusts selection.  We call
checkMousePosition at end of function to ensure that scroll bar receives final
onMouseUp event.  NOTE: The main window always receives onMouseUp events even if
windowSelected is false.  Therefore, descendents should write this at the
beginning of mouse events:

    if(vertSelected || horSelected)
        return;
\****************************************************************************/
void tScrollWindow::onMouseUp(int x, int y, eMouseButton button)
{
    tControl::onMouseUp(x,y,button);
    if (vertSelected && vertScrollBar)
        vertScrollBar->onMouseUp(x,y,button);
    if (horSelected && horScrollBar)
        horScrollBar->onMouseUp(x,y,button);
    checkMousePosition(x,y);
}

//This function serves the same purpose as onMouseUp et al.
void tScrollWindow::onMouseWheel(int pos, eMouseWheel direction)
{
    tControl::onMouseWheel(pos,direction);
    if (vertSelected && vertScrollBar)
        vertScrollBar->onMouseWheel(pos,direction);
    if (horSelected && horScrollBar)
        horScrollBar->onMouseWheel(pos,direction);
}

/****************************************************************************\
tScrollWindow::onSelect

Adjusts selection and passes event to scroll bar if selected.
\****************************************************************************/
void tScrollWindow::onSelect(int x, int y)
{
    tControl::onSelect(x,y);
    checkMousePosition(x,y);
    if (vertScrollBar)
        vertScrollBar->onSelect(x,y);
    if (horScrollBar)
        horScrollBar->onSelect(x,y);
}

/****************************************************************************\
tScrollWindow::onDeselect

Clear all selections.  Generates onDeselectWindow event if main window was
formerly selected.  Passes event to children.
\****************************************************************************/
void tScrollWindow::onDeselect()
{
    tControl::onDeselect();
    vertSelected = false;
    horSelected = false;
    if (windowSelected)
    {
        windowSelected = false;
        onDeselectWindow();
    }
    if (vertScrollBar)
        vertScrollBar->onDeselect();
    if (horScrollBar)
        horScrollBar->onDeselect();
}

/****************************************************************************\
tScrollWindow::poll

Polls self and children.
\****************************************************************************/
void tScrollWindow::poll()
{
    tControl::poll();
    if (vertScrollBar)
        vertScrollBar->poll();
    if (horScrollBar)
        horScrollBar->poll();
}

/****************************************************************************\
tScrollWindow::refresh

Refreshes self and children.
\****************************************************************************/
void tScrollWindow::refresh()
{
    if (vertScrollBar)
        vertScrollBar->refresh();
    if (horScrollBar)
        horScrollBar->refresh();
}

/****************************************************************************\
tScrollWindow::~tScrollWindow

Deletes children.
\****************************************************************************/
tScrollWindow::~tScrollWindow()
{
    if(vertScrollBar)
        mwDelete vertScrollBar;
    if(horScrollBar)
        mwDelete horScrollBar;
}

/****************************************************************************\
tScrollWindow::enableScrollBar

Enables the specified window scroll bar.  Selects the window scroll bar also if
the scroll window is selected.  We added this function to support the virtual
'enable' call in tScrollBar::update.

Inputs:
    s           Control's horizontal or vertical scroll bar
Outputs:
    none
\****************************************************************************/
void tScrollWindow::enableScrollBar(tWindowScrollBar *s)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(s))
        return;
#endif
    s->onEnable();

    if(isSelected())
        s->onSelect(saveMouseX,saveMouseY);
}

/****************************************************************************\
tScrollWindow::disableScrollBar

Disables the specified window scroll bar.  We added this function to support
the virtual 'disable' call in tScrollBar::update.

Inputs:
    s           Control's horizontal or vertical scroll bar
Outputs:
    none
\****************************************************************************/
void tScrollWindow::disableScrollBar(tWindowScrollBar *s)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(s))
        return;
#endif
    s->onDisable();
}

/****************************************************************************\
tScrollWindow::createHorScrollBar

Associates a horizontal scroll bar with this control.  The function saves the
scroll bar's address in horScrollBar and sets the scroll bar's parent.

Inputs:
    s           The address of a tWindowScrollBar object
                    (created with 'new tWindowScrollBar(...)')
Outputs:
    none
\****************************************************************************/
void tScrollWindow::createHorScrollBar(tWindowScrollBar *s)
{
    horScrollBar=s;
    horScrollBar->setParent(this);
}

/****************************************************************************\
tScrollWindow::createVertScrollBar

Associates a vertical scroll bar with this control.  The function saves the
scroll bar's address in vertScrollBar and sets the scroll bar's parent.

Inputs:
    s           The address of a tWindowScrollBar object
                    (created with 'new tWindowScrollBar(...)')
Outputs:
    none
\****************************************************************************/
void tScrollWindow::createVertScrollBar(tWindowScrollBar *s)
{
    vertScrollBar=s;
    vertScrollBar->setParent(this);
}

/****************************************************************************\
tScrollWindow::activated

This control is activated if the main window or either scroll bar is activated.
\****************************************************************************/
bool tScrollWindow::activated(int x, int y)
{
    return objectActivated(x,y) || (horScrollBar && horScrollBar->activated(x,y)) ||
        (vertScrollBar && vertScrollBar->activated(x,y));
}
/****************************************************************************\
tScrollWindow::getMousePointer

Relinquishes control of the mouse pointer to a scroll bar if one of the scroll
bars is selected.
\****************************************************************************/
bool tScrollWindow::getMousePointer(int x, int y, ePointer& p, int& tag)
{
    //Ancestors have precedence over descendents.
    if(tControl::getMousePointer(x,y,p,tag))
        return true;
    if(horScrollBar && horScrollBar->activated(x,y))
    {
        horScrollBar->getMousePointer(x,y,p,tag);
        return true;
    }
    if(vertScrollBar && vertScrollBar->activated(x,y))
    {
        vertScrollBar->getMousePointer(x,y,p,tag);
        return true;
    }
    return false;
}


/****************************************************************************\
tTextBox::XYToColRow

Converts a pair of coordinates to the nearest row and column.  For instance,
when the user clicks in the text box, we use this function to reposition
the cursor.  We added a special case for pressing the mouse with word wrap
enabled.  When the line ends in space that overlaps the right edge, we want to
position the cursor -before- the space.  Set pastEndOfLine to false in the
onMouseDown event and to true elsewhere.

Inputs:
    x, y            Pair of coordinates relative to top-left corner
    pastEndOfLine   False for onMouseDown event; true elsewhere
Outputs:
    col, row        Nearest row and column
\****************************************************************************/
void tTextBox::XYToColRow(int x, int y, int &col, int &row, bool pastEndOfLine)
{
    int a, b, c;
    int adjustedLength;
    int h;

    h=fontHeight(getFontIndex());
#ifdef DEBUG
    if(h<=0)
    {
        bug("tTextBox::XYToColRow: invalid font height");
        col=0; row=0;
        return;
    }
#endif

    //Since all lines have the same height, we divide y by the font's height:
    row = y / h;

    //Check boundaries:
    if(row<0)
        row = 0;
    if(row >= text.size())
    {
        /* If the point is below the last line of text, we position row on the
         * line beneath the last line and column at the left edge.  On the other
         * hand, if the text box already contains the maximum number of lines,
         * we position the row on the last line and column at the end.
         */
        row = MIN(text.size(),maxLines-1);
        if(row<0)
            row=0;
        if(row >= text.size())
            col = 0;
        else
            col = STRLEN(text[row]);
        return;
    }

    //We use a binary search to determine where to position the cursor within
    //the line.  If the line ends in a space that overlaps the right edge in
    //word wrap mode, we exclude the space from our search.
    a = 0;
    adjustedLength=STRLEN(text[row]);   //Number of characters to include in our search
    if(wordWrap&&!pastEndOfLine&&lineData[row].extended)
        adjustedLength--;

    b = adjustedLength;
    while(a != b)
    {
        c = (a+b+1)/2;
        if(x < lineData[row].charOffsets[c])
            b = c-1;
        else if(x > lineData[row].charOffsets[c])
            a = c;
        else
        {
            a = c;
            break;
        }
    }
    col=a;

    //If the user clicks on the rear half of a character, we position the cursor
    //before the next character:
    if(col<adjustedLength && x - lineData[row].charOffsets[col] > charWidth(text[row][col],getFontIndex()) / 2)
        col++;
}

/****************************************************************************\
tTextBox::colRowToXY

Finds the coordinates of the upper-left corner of a character located at
column and row.  If no character exists at this location, the function
returns zero for x.  'y' always equals 'row' multiplied by the font's height.

Inputs:
    col, row        Location of a character
Outputs:
    x, y            Coordinates of the character's upper-left corner
\****************************************************************************/
void tTextBox::colRowToXY(int col, int row, int& x, int& y)
{
    //We store character offsets in the line data container.
    if(row < 0 || row >= text.size() || col < 0 || col > STRLEN(text[row]))
        x = 0;
    else
        x = lineData[row].charOffsets[col];

    //We calculate 'y' directly from 'row' since all lines have the same height.
    y = row * fontHeight(getFontIndex());
}

/****************************************************************************\
tTextBox::checkCurrentPos

Ensures that the current character (preceded by the cursor) is fully visible.
The function calculates the coordinates of the character's upper-left corner
and lower-right corner using colRowToXY.  It then calls checkXY to scroll
the window if necessary.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::checkCurrentPos()
{
    int x,y,xoffset;

    //Find the coordinates of the character's upper-left corner:
    colRowToXY(column, row, x, y);

    //Make sure this point is visible:
    checkXY(x,y);

    /* Now examine the lower-right corner by adding the character's width and
     * height.  NOTE: In word wrap mode, we need not scroll the window
     * horizontally.  When the cursor is at the end of the line, we pretend
     * it covers a thin character so that the whole cursor is visible.
     */
    if(wordWrap)
        xoffset=0;
    else if (row >= 0 && row < text.size() && column >= 0 && column < STRLEN(text[row]))
        xoffset = charWidth(text[row][column],getFontIndex());
    else
        xoffset = 2;
    checkXY(x + xoffset, y + fontHeight(getFontIndex()));
}

/****************************************************************************\
tTextBox::clearSelection

Cancels the selection by resetting a state variable.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::clearSelection()
{
    selection = false;
}

/****************************************************************************\
tTextBox::removeSelection

Removes the highlighted text, bounded by selStartCol, selStartRow, column, and
row.  The function updates the line data, wraps the text in word wrap mode,
repositions the cursor, and adjusts the window size.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::removeSelection()
{
    tString buffer;

    //Store the selection's start and end position in col1/row1 and col2/row2 and
    //ensure that the former is front of the latter:
    int col1 = selStartCol;
    int row1 = selStartRow;
    int col2 = column;
    int row2 = row;
    if (row1 > row2 || row1 == row2 && col1 > col2)
    {
        swap(col1, col2);
        swap(row1, row2);
    }

    //Since the selection will always include at least once character or span
    //multiple lines, the selection should never start and end on the line after
    //the last line.
    if(row1 < text.size())
    {
        if(row2 < text.size() || col1 > 0)
        {
            //Include the first part of row1 up to selection and last part of
            //row2 after selection:
            if(row2 < text.size())
            {
                text[row1] = STRLEFT(text[row1], col1) + STRLTRIM(text[row2], col2);
            }
            else
            {
                text[row1] = STRLEFT(text[row1], col1);
                row2 = text.size()-1;
            }

            //Preserve the hard/soft return at the end of row2:
            lineData[row1].hardReturn=lineData[row2].hardReturn;

            //Remove lines excluding row1 but including row2:
            text.erase(text.begin()+row1+1, text.begin()+row2+1);
            lineData.erase(lineData.begin()+row1+1, lineData.begin()+row2+1);

            //Reposition the cursor at the selection's origin:
            column = col1;
            row = row1;

            //Parse the text box beginning at the point where text was joined
            //from different lines.  The function will wrap up or down as needed.
            parseLine(row1);
        }
        else    //The selection begins at column 0 and includes the remainder of
                //the text.  A special case is needed here since the blank line
                //which the cursor is on after deleting selection needs to be
                //removed (unlike case above).
        {
            row2 = text.size()-1;

            //Remove lines including row1:
            text.erase(text.begin()+row1, text.begin()+row2+1);
            lineData.erase(lineData.begin()+row1, lineData.begin()+row2+1);

            //Conclude the last line with a hard return:
            if(row1>0)
                lineData[row1-1].hardReturn=true;

            //Reposition the cursor at the selection's origin:
            column = col1;
            row = row1;

            //No need to parse lines here.
        }

        //Adjust the scroll bars, since the text width or height may have changed:
        adjustWindowSize();
    }
    else
        bug("tTextBox::removeSelection: row1 >= text.size()");

    //Cancel the selection and ensure the cursor is still visible:
    selection = false;
    checkCurrentPos();
}

/****************************************************************************\
tTextBox::adjustWindowSize

Finds the smallest rectangle which will contain the text.  The function then
resizes the window's contents via setWindowSize.  NOTE: This does not affect
the control's size.  It merely adjusts the scroll bars' range.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::adjustWindowSize()
{
    int neww=0;

    //In word wrap mode, the window's width always equals the control's width
    //minus the margin.
    if(wordWrap)
        neww=wordWrapWidth();

    //In normal mode, we find the longest line.
    else
        for(int i=0; i<text.size(); i++)
            if(lineData[i].charOffsets.back() > neww)
                neww = lineData[i].charOffsets.back();

    //The height always equals the number of lines multiplied by the line height.
    int newh=text.size()*fontHeight(getFontIndex());

    //Do we need to adjust the size?  If so, call setWindowSize in ancestor.
    //Conditional avoids unnecessary computations.
    if(neww != getWindowWidth() || newh != getWindowHeight())
        setWindowSize(neww,newh);
}

/****************************************************************************\
tTextBox::adjustSelection

Ends the selection at the current position.  If the selection begins and ends
at the same location, then a selection does not exist.  The function sets the
selection flag accordingly.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::adjustSelection()
{
    if(selStartCol == column && selStartRow == row)
        selection=false;
    else
        selection=true;
}

/****************************************************************************\
tTextBox::findLineData

Calculates the metrics of one line (length, extended, and overflow).  It also
sets modified, meaning we still need to call findOffsets to calculate character
offsets.  parseLine uses this function to determine whether to wrap the line.

Inputs:
    r           Valid line index (0 <-> text.size()-1)
Outputs:
    none
\****************************************************************************/
void tTextBox::findLineData(int r)
{
#ifdef DEBUG
    if(r < 0 || r >= text.size() || r >= lineData.size())
    {
        bug("tTextBox::findLineData: invalid row %d",r);
        return;
    }
#endif
    int i,l=0;

    //Add up the width of each character on the line:
    for(i=0;i<STRLEN(text[r]);i++)
        l+=charWidth(text[r][i],getFontIndex());

    lineData[r].length=l;
    lineData[r].modified=true;

    //'extended' has a different meaning in this context.  Here, 'extended'
    //indicates that the line ends in a letter/space combination whose
    //final space could -potentially- overlap the right edge.  If the final
    //space does not overlap the right edge, we reset 'extended' to false
    //in findOffsets.
    lineData[r].extended=(STRLEN(text[r])>1&&text[r][STRLEN(text[r])-1]==' '&&text[r][STRLEN(text[r])-2]!=' ');

    //Determine if the line will fit within the window width.  If the line
    //"overflows", we will need to wrap some words to the next line.  NOTE:  In
    //the special case where the line ends with a letter/space combination, the
    //final space is allowed to overlap the right edge.
    if(lineData[r].extended)
        lineData[r].overflow=(lineData[r].length-charWidth(' ',getFontIndex())>wordWrapWidth());
    else
        lineData[r].overflow=(lineData[r].length>wordWrapWidth());
}

/****************************************************************************\
tTextBox::findOffsets

Calculates character offsets and clears the 'modified' flag.  Call this function
only after calculating the line's metrics and ensuring that the line fits
within the window width.

Inputs:
    r           Valid line index (0 <-> text.size()-1)
Outputs:
    none
\****************************************************************************/
void tTextBox::findOffsets(int r)
{
    int curOffset=0, i;
#ifdef DEBUG
    if(r < 0 || r >= text.size() || r >= lineData.size())
    {
        bug("tTextBox::findOffsets: invalid row %d",r);
        return;
    }
    if(wordWrap&&lineData[r].overflow)
        bug("tTextBox::findOffsets: line exceeds width of window");
#endif
    //Calculate the offset of each character from scratch:
    lineData[r].charOffsets.clear();
    for(i=0;i<STRLEN(text[r]);i++)
    {
        lineData[r].charOffsets.push_back(curOffset);
        curOffset+=charWidth(text[r][i],getFontIndex());
    }

    //If the line exceeds the window width, we assume that it ends in a
    //letter/space combination and set 'extended' accordingly (although it
    //should already be set from findLineData).  If the line fits within
    //the window width, we clear 'extended' and treat it the same as any
    //other line.
    if(wordWrap && curOffset>wordWrapWidth())
    {
        //Condense the final space so that it fits within the window window:
        curOffset=wordWrapWidth();
        lineData[r].extended=true;
    }
    else
        lineData[r].extended=false;

    //Append the offset -after- the last character and reset 'modified':
    lineData[r].charOffsets.push_back(curOffset);
    lineData[r].modified=false;
}

/****************************************************************************\
tTextBox::wrapUp

Wraps words from left of line r+1 to right of line r if there is space.  The
function updates the line metrics of lines r and r+1.  It also shift the cursor
if necessary and loads r with the line where wrapping should occur next.

Inputs:
    r           Valid line index (0 <-> text.size()-1)
Outputs
    r           If the function returns true, you should call wrapUp again
                    with this new value for 'r'.
    Returns true if wrapping occurred
\****************************************************************************/
bool tTextBox::wrapUp(int& r)
{
    //curOffset stores the width in pixels of the string we are considering
    //wrapping up:
    int curOffset=0, oldOffset=0, i, lastIndex=0;
    bool breakWord=false,extended=false;

#ifdef DEBUG
    if(r < 0 || r >= text.size() || r >= lineData.size())
    {
        bug("tTextBox::wrapUp: invalid row %d",r);
        return false;
    }
    if(r+1==text.size()&&lineData[r].hardReturn==false)
    {
        bug("tTextBox::wrapUp: last line ends in a soft return");
        return false;
    }
#endif

    //If the line doesn't end in a space then the last word might continue on
    //the next line.  In this case, we should append as many letters to this
    //word from the next line as we can.
    if(STRLEN(text[r])==0 || text[r][STRLEN(text[r])-1]!=' ')
        breakWord=true;

    //Main Conditional:
    //If there is room at the end of the line and it ends in a soft return, then
    //we may be able to wrap some words up from the next line.
    if(lineData[r].length<wordWrapWidth()&&lineData[r].hardReturn==false)
    {
        //Traverse each character on the next line from left to right:
        for(i=0;i<STRLEN(text[r+1]);i++)
        {
            //Once we reach a space, we only want to wrap up whole words;
            //whereas, earlier we might have been content with wrapping up
            //single letters (see comment above regarding breakWord).
            if(text[r+1][i]==' ')
                breakWord=false;

            //Consider breaking the line just before text[r+1][i] if there is a
            //space on either side or we have permission to break word:
            if(text[r+1][i]==' '||i>0&&text[r+1][i-1]==' '||breakWord)
            {
                //Is there space on previous line for i characters?
                //NOTE: When i is zero, we are considering placing 0 characters
                //on the line above.  Therefore, when i is zero, the following
                //condition will always be true.
                if(lineData[r].length+curOffset<=wordWrapWidth())
                {
                    //If the string fits on the line, there is no need to extend
                    //the line (e.g. compress the final space).  (See comment below
                    //regarding extended).
                    extended=false;

                    //lastIndex stores the length in characters of the best
                    //string to wrap up so far:
                    lastIndex=i;

                    //oldOffset stores the length in pixels of the best string
                    //to wrap up so far:
                    oldOffset=curOffset;

                    //If we're breaking in front of a space and the previous
                    //character was not a space then wrap up the space also:
                    if(text[r+1][i]==' '&&i>0&&text[r+1][i-1]!=' ')
                    {
                        //Let's assume there isn't enough room for the trailing
                        //space.  Hence, we set extended to true meaning we're
                        //extending the line slightly past wordWrapWidth to fit
                        //the space in.  If there is enough room for trailing
                        //space, we will reset 'extended' during the next pass.
                        extended=true;
                        lastIndex++;
                        oldOffset+=charWidth(' ',getFontIndex());
                    }
                }
                else    //We can't fit any more characters on the line.
                    break;
            }
            //Add the length of current character to curOffset:
            curOffset+=charWidth(text[r+1][i],getFontIndex());
        }

        //If there is enough space to wrap up the entire line, then obviously
        //there's enough room for trailing space.  Set lastIndex and oldOffset
        //to include the whole line.
        if(/*(i>0&&text[r+1][i-1]==' '||breakWord||lineData[r+1].hardReturn)&&*/
            lineData[r].length+curOffset<wordWrapWidth())
        {
            extended=false;
            lastIndex=i;
            oldOffset=curOffset;
        }

        //Determine if there is enough room for at least one additional character:
        if(lastIndex>0)
        {
            //Determine if the cursor is on line r+1:
            if(row==r+1)
            {
                //If the cursor is within the string which is going to get
                //wrapped up, then move the cursor to appropriate location in
                //line r.
                if(column<=lastIndex)
                {
                    row--;
                    column+=STRLEN(text[r]);
                }

                //If the cursor follows the string which is going to get wrapped
                //up, then move the cursor left the correct number of characters.
                else
                    column-=lastIndex;
            }

            //Append correct number of characters from left of line r+1 to line r:
            text[r]+=STRLEFT(text[r+1],lastIndex);

            //Adjust line metrics:
            lineData[r].length+=oldOffset;
            lineData[r].modified=true;
            lineData[r].extended=extended;

            //Determine if we're wrapping up all of line r+1:
            if(lastIndex==STRLEN(text[r+1]))
            {
                //Delete line r+1, but preserve hard/soft return.
                lineData[r].hardReturn=lineData[r+1].hardReturn;
                text.erase(text.begin()+r+1);
                lineData.erase(lineData.begin()+r+1);

                //Adjust row if necessary:
                if(row>r+1)
                    row--;

                //There's a chance we can wrap up some more words from the new
                //line r+1 so don't adjust r (parseLine will call this function
                //again with the same value for r).
                return true;    //Wrapping did occur.
            }

            //Remove correct number of chararacters from left of line r+1:
            text[r+1]=STRLTRIM(text[r+1],lastIndex);

            //Adjust line metrics:
            lineData[r+1].length-=oldOffset;
            lineData[r+1].modified=true;
            if(lineData[r+1].extended)
                lineData[r+1].overflow=(lineData[r+1].length-charWidth(' ',getFontIndex())>wordWrapWidth());
            else
                lineData[r+1].overflow=(lineData[r+1].length>wordWrapWidth());

            //We have wrapped as much as we can to line r, so move to the next
            //line by incrementing r.  parseLine will call this function again
            //with the new value for r.
            r++;
            return true;    //Wrapping did occur.
        }
    }
    return false;   //Wrapping did not occur.
}

/****************************************************************************\
tTextBox::wrapDown

Wrap words from right of line r to left of line r+1 if there is space.  The
function updates the line metrics of lines r and r+1.  It also shift the cursor
if necessary and loads r with the line where wrapping should occur next.

Inputs:
    r           Valid line index (0 <-> text.size()-1)
Outputs
    r           If the function returns true, you should call wrapDown again
                    with this new value for 'r'.
    Returns true if wrapping occurred
\****************************************************************************/
bool tTextBox::wrapDown(int& r)
{
#ifdef DEBUG
    if(r < 0 || r >= text.size() || r >= lineData.size())
    {
        bug("tTextBox::wrapDown: invalid row %d",r);
        return false;
    }
#endif

    int curOffset=lineData[r].length, i;
    bool extended=false;
    const char *ptr=strchr(CHARPTR(text[r]), ' ');

    //firstSpace contains the index of the first space in line r.  If there is
    //no space, first space indexes character after last character.
    int firstSpace=ptr?(ptr-CHARPTR(text[r])):STRLEN(text[r]);

    //Wrapping should occur (1) if the line is too long; or (2) a word has been
    //broken and there's now a space within the line.
    if(wordWrap&&(lineData[r].overflow||ptr&&!lineData[r].hardReturn&&text[r][STRLEN(text[r])-1]!=' '))
    {
        //Traverse the characters in line r from right to left:
        for(i=STRLEN(text[r])-1;i>=1;i--)
        {
            //curOffset stores the length of line r if it were broken before
            //character i.
            curOffset-=charWidth(text[r][i],getFontIndex());

            //Consider breaking the line before character i if there is a space
            //on either side or we have passed to the left of the first space.
            //If the trimmed line fits, we break out of the loop.
            if((text[r][i]==' '||i>0&&text[r][i-1]==' '||i<firstSpace)&&curOffset<=wordWrapWidth())
                break;
        }

        //This should never occur since we should always be able to fit at least
        //one character on line:
#ifdef DEBUG
        if(i<=0)
        {
            bug("tTextBox::wrapDown: couldn't fit any characters on line");
            return false;
        }
#endif

        //If we're breaking the line just after a word and before a space, then
        //squeeze the final space onto the end of the line:
        if(text[r][i]==' '&&text[r][i-1]!=' ')
        {
            extended=true;  //The final space overlaps the right edge.
            i++;
            curOffset+=charWidth(' ',getFontIndex());
        }

        //If line r ends in hard return (meaning a new line will need to be
        //inserted) then adjust row as necessary:
        if(lineData[r].hardReturn==true&&row>=r+1)
            row++;

        //If the cursor is within the string which is about to be wrapped down,
        //then move cursor to correct location on line r+1:
        if(row==r&&column>i)
        {
            row++;
            column-=i;
        }
        else if(row==r+1)               //If cursor is on line r+1, then shift cursor
            column+=STRLEN(text[r])-i;  //right by the correct number of characters.

        //If line r continues on line r+1 (soft return) then insert wrapped
        //string before line r+1.
        if(lineData[r].hardReturn==false&&r+1<text.size())
        {
            text[r+1]=STRLTRIM(text[r],i)+text[r+1];
        }
        else    //Otherwise, insert a line which contains the wrapped string.
        {
            lineData[r].hardReturn=false;
            text.insert(text.begin()+r+1,STRLTRIM(text[r],i));
            tLineData data;
            data.hardReturn=true;
            data.extended=(STRLEN(text[r+1])>1&&text[r+1][STRLEN(text[r+1])-1]==' '&&text[r+1][STRLEN(text[r+1])-2]!=' ');
            data.length=0;
            lineData.insert(lineData.begin()+r+1,data);
        }

        //Remove wrapped string from line r:
        text[r]=STRLEFT(text[r],i);

        //Adjust line metrics:
        lineData[r+1].length+=(lineData[r].length-curOffset);
        lineData[r].length=curOffset;
        lineData[r].modified=true;
        lineData[r].extended=extended;
        lineData[r].overflow=false;

        lineData[r+1].modified=true;
        if(lineData[r+1].extended)
            lineData[r+1].overflow=(lineData[r+1].length-charWidth(' ',getFontIndex())>wordWrapWidth());
        else
            lineData[r+1].overflow=(lineData[r+1].length>wordWrapWidth());

        r++;            //We have finished processing line r, so process next line.
        return true;    //Wrapping did occur.
    }
    return false;       //Wrapping did not occur.
}

/****************************************************************************\
tTextBox::parseLine

Parses line r and lines affected by line r (above or below).  Calculates
character offsets, and wraps lines up or down as necessary.

Inputs:
    r           Valid line index (0 <-> text.size()-1)
Outputs:
    none
Precondition:
    Only text on line r has changed (although lines can be inserted or deleted).
\****************************************************************************/
void tTextBox::parseLine(int r)
{
    int i;
#ifdef DEBUG
    if(r < 0 || r >= text.size() || r >= lineData.size())
    {
        bug("tTextBox::parseLine: invalid row %d",r);
        return;
    }
#endif

    //Find basic metrics for line r:
    //I don't calculate character offsets until later since these may change
    //with wrapping.
    findLineData(r);

    //First, determine if I can wrap words/characters up from line r to line r-1.
    //I only do this once unless line r is removed, in which case I need to
    //check again.
    if(r>0)
    {
        i=r-1;
        while(wrapUp(i))
            if(i+1>r)
                break;
    }

    //Begin at line r and wrap each successive line until no further wrapping
    //occurs.  If line was shortened, wrap up previous lines.  If line was
    //lengthened, wrap down successive lines.
    i=r;
    if(wrapDown(i))
    {
        while(wrapDown(i))
        ;
    }
    else
    {
        while(wrapUp(i))
        ;
    }

    //If the previous line was modified, then calculate its character offsets:
    if(r>0&&lineData[r-1].modified)
        findOffsets(r-1);

    //Calculate character offsets of line r and lines below r if modified:
    for(i=r;i<text.size()&&i<lineData.size()&&lineData[i].modified;i++)
        findOffsets(i);
}

/****************************************************************************\
tTextBox::poll

Blinks the cursor and scrolls the window when the user drags the mouse beyond
its boundaries.
\****************************************************************************/
void tTextBox::poll()
{
    tStdScrollWindow::poll();

    //If text box is read-only or isn't focused, then don't show blinking cursor:
    if(readOnly || !isFocused())
        return;

    //Scroll window during mouse selection by repeatedly calling onMouseMove
    //every 0.1 seconds.
    if (isMousePressed() && isSelected() && !vertSelected && !horSelected && scrollX != -1)
    {
        if(currentTime - scrollTime > 0.1)
        {
            scrollTime = currentTime;
            onMouseMove(scrollX,scrollY);
        }
        return; //Don't blink cursor
    }

    //Blink cursor every 0.5 seconds:
    if (currentTime - cursorTime > 0.5)
    {
        cursorVisible = !cursorVisible;
        cursorTime = currentTime;
    }
}

/****************************************************************************\
tTextBox::onMouseDown

Cancels the selection.  Repositions the cursor.  Resets the cursor blinking
cycle.  Positions the attractor.  Disables mouse scrolling.
\****************************************************************************/
void tTextBox::onMouseDown(int x, int y, eMouseButton button)
{
    tStdScrollWindow::onMouseDown(x,y,button);

    //Ignore mouse down event if scroll bars are selected or text box is read-only:
    if(vertSelected || horSelected || readOnly)
        return;

    //Adjust x and y so that they are relative to upper-left corner of viewing window:
    x-=visWindowX();
    y=visWindowHeight()+visWindowY()-y;

    //Cancel the selection:
    clearSelection();

    //Add window position so that x and y are relative to text area.  Convert
    //this position to a column and row.
    XYToColRow(x+getWindowX(),y+getWindowY(),column,row,false);

    //Make sure character at this position is fully visible:
    checkCurrentPos();

    //Possibly begin selection at this location (if mouse is dragged).
    selStartCol=column;
    selStartRow=row;

    //Reset cursor blinking cycle:
    cursorVisible = true;
    cursorTime = currentTime;

    //Position the attractor.  The cursor tries to align itself with this
    //value when the user presses the up or down arrow keys:
    if(row<text.size())
        attractorX = lineData[row].charOffsets[column];
    else
        attractorX = 0;

    //Disable mouse scrolling until the user drags the mouse beyond the window's
    //boundaries:
    scrollX = -1;
    scrollTime = currentTime;
}

/****************************************************************************\
tTextBox::onMouseMove

Repositions the cursor, adjusts the selection, and resets the cursor blinking
cycle when the user drags the mouse to a new location.  The function also
initiates scrolling when the user drags the mouse beyond the viewing window.
\****************************************************************************/
void tTextBox::onMouseMove(int x, int y)
{
    int newcol, newrow;
    int absx = x, absy = y;
    tStdScrollWindow::onMouseMove(x,y);

    //Ignore mouse move event if scroll bars are selected or text box is read-only:
    if(vertSelected || horSelected || readOnly)
        return;

    //Adjust x and y so that they are relative to upper-left corner of viewing window:
    x-=visWindowX();
    y=visWindowHeight()+visWindowY()-y;

    //Determine if the user is holding down a mouse button:
    if(isMousePressed())
    {
        //If the user drags the mouse outside the viewing window, then begin
        //scrolling.  Setting scrollX and scrollY will cause 'poll' to
        //repeatedly call this function every 0.1 seconds.
        if (x < 0 || x >= visWindowWidth() || y < 0 || y >= visWindowHeight())
        {
            scrollX = absx;
            scrollY = absy;
        }
        else
            scrollX = -1; //Don't scroll.

        //Convert position to closest column and row:
        XYToColRow(x+getWindowX(), y+getWindowY(), newcol, newrow, true);

        //If you've moved the cursor to a new location, then reposition the
        //window, adjust the selection, and the reset cursor blinking cycle:
        if (newcol != column || newrow != row)
        {
            column=newcol;
            row=newrow;
            checkCurrentPos();
            adjustSelection();
            cursorVisible=true;
            cursorTime = currentTime;
        }
    }
}

/****************************************************************************\
tTextBox::onKeyDown

Dispatches keystrokes to appropriate key handlers.  Cancels the selection
unless the user is holding down Shift.  Resets the cursor blinking cycle.
\****************************************************************************/
void tTextBox::onKeyDown(tKey key, char ch)
{
    tStdScrollWindow::onKeyDown(key,ch);

    //Ignore modifier keys (e.g. shift, alt, and ctrl):
    if(isModifierKey(key))
        return;

    //Added 12/29/02: Ignore keyboard events if the control is read-only.
    if(readOnly)
        return;

    //If the user entered an ordinary letter, number, or symbol, then pass
    //the character's ASCII code to the character handler:
    if(ch >= 32 && ch < 127)
        charHandler(ch);
	else
		switch (key)
			{
			case GLFW_KEY_INSERT:
                //Toggle insert/type-over mode:
				if (cursorMode == CURSOR_INSERT)
					cursorMode = CURSOR_TYPE_OVER;
				else
					cursorMode = CURSOR_INSERT;
				break;
			case GLFW_KEY_DOWN:
				down();
				break;
			case GLFW_KEY_UP:
				up();
				break;
			case GLFW_KEY_LEFT:
				left();
				break;
			case GLFW_KEY_RIGHT:
				right();
				break;
			case GLFW_KEY_PAGEUP:
				pageUp();
				break;
			case GLFW_KEY_PAGEDOWN:
				pageDown();
				break;
			case GLFW_KEY_HOME:
                if (isCtrlPressed())
                    ctrlHome();
                else
                    home();
				break;
			case GLFW_KEY_END:
                if (isCtrlPressed())
                    ctrlEnd();
                else
                    end();
				break;
            //Holding down shift has no effect on these three keys:
			case GLFW_KEY_BACKSPACE:
				backspace();
				break;
			case GLFW_KEY_ENTER:
				enter();
				break;
			case GLFW_KEY_DEL:
				del();
				break;
 		}

    //If the user is not holding down Shift, then begin the selection at the
    //cursor's new location.  This way, if you begin a selection with SHIFT,
    //the selection start location will already be available.
    if (!isShiftPressed())
    {
        selStartCol=column;
        selStartRow=row;
    }

    //Reset cursor blinking cycle:
    cursorVisible=true;
    cursorTime = currentTime;
}

/****************************************************************************\
tTextBox::onFocus

Resets the cursor blinking cycle when the text box is focused.  This function
has no effect on read-only text boxes.
\****************************************************************************/
void tTextBox::onFocus()
{
    tStdScrollWindow::onFocus();
    if(!readOnly)
    {
        cursorVisible=true;
        cursorTime = currentTime;
    }
}

/****************************************************************************\
tTextBox::onDefocus

Hides the cursor when the text box loses focus.
\****************************************************************************/
void tTextBox::onDefocus()
{
    cursorVisible=false;
    tStdScrollWindow::onDefocus();
}

/****************************************************************************\
tTextBox::onUpdatePosition

Prevents the cursor from blinking when the user scrolls.  This function
has no effect on read-only text boxes.
\****************************************************************************/
void tTextBox::onUpdatePosition()
{
    tStdScrollWindow::onUpdatePosition();
    if(!readOnly)
    {
        cursorVisible=true;
        cursorTime = currentTime;
    }
}

/****************************************************************************\
tTextBox::onSetSize

Wraps the text to fit the viewing window's new dimensions and adjusts the scroll
bars.  The function only affects text boxes with word wrap.
\****************************************************************************/
void tTextBox::onSetSize(int width, int height)
{
    //Have we widened the text box?  This line must be above call to ancestor.
    bool widen=(width>getWidth());
    int i;
    tStdScrollWindow::onSetSize(width,height);

    if(wordWrap)
    {
        //Clear selection here to avoid shifting selection:
        clearSelection();

        //For each line, determine whether the line exceeds the word wrap width:
        for(i=0; i<text.size(); i++)
        {
            if(lineData[i].extended)
                lineData[i].overflow=(lineData[i].length-charWidth(' ',getFontIndex())>wordWrapWidth());
            else
                lineData[i].overflow=(lineData[i].length>wordWrapWidth());
        }

        //Now, either wrap all of the lines up if we're widening the text box,
        //or wrap all of the lines down if we're shrinking the text box:
        if(widen)
            for(i=0; i<text.size(); i++)
                while(wrapUp(i))
                ;
        else
            for(i=0; i<text.size(); i++)
                while(wrapDown(i))
                ;

        //Find character offsets for each line:
        //There are some cases where we want to recalculate charOffsets even if
        //the text hasn't changed.  For instance, the final space was condensed
        //(extended=true), but now there is enough room for space.
        for(i=0; i<text.size(); i++)
            /*if(lineData[i].modified)*/
                findOffsets(i);

        //Adjust the window's size and ensure that viewing window does not exceed
        //bottom of text box.  This is so that the text does not disappear above
        //the viewing window when widening.
        adjustWindowSize();
        if(getWindowY()>0&&getWindowY()+visWindowHeight()>getWindowHeight())
            setWindowPosition(getWindowX(),MAX(getWindowHeight()-visWindowHeight(),0));
    }
}

/****************************************************************************\
tTextBox::backspace

Defines the text box's response to the Backspace key.  Typically, the function
removes the character to the left of the cursor and moves the cursor one column
to the left.  It may also remove the selection (if one exists) or remove a hard
return (if the cursor is positioned in the leftmost column).  It parses the line,
adjusts the window size, resets the attractor, and scrolls the window if
necessary.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::backspace()
{
    //If the user has highlighted a selection, then remove it.
    if(selection)
    {
        removeSelection();
        return;
    }

    //CASE 1: The cursor is not in the leftmost column.
	if (column > 0)
    {
        //If the cursor is within the line, then remove the character before it
        //and decremement the column.  Otherwise, just decrement the column
        //as if the user pressed the left arrow key.
		if (row < text.size() && column <= STRLEN(text[row]))
        {
            text[row] = STRLEFT(text[row], column-1) + STRLTRIM(text[row], column);
            column--;
            parseLine(row);
            adjustWindowSize();
        }
        else
            column--;
    }
    //CASE 2: The cursor is in the leftmost column but -not- on the topmost row.
	else if (row > 0)
    {
        //If the cursor is on the row after the last row and the row above it is
        //empty, then remove it.
        if(row == text.size() && STRLEN(text[row-1])==0)
        {
            row--;
            column=0;
            text.erase(text.begin() + row);
            lineData.erase(lineData.begin() + row);
            if(row>0)
                lineData[row-1].hardReturn=true;    //Preserve hard return.
            adjustWindowSize();
            //No need to parse line here since we removed an empty line.
        }
        //If the cursor is within the text area or on the line just below, then
        //append the current line to the line above it and position the cursor
        //accordingly.
        else if(row-1 < text.size())
        {
            //Move to previous line:
            row--;

            //If the line above ends in a soft return, then remove the last
            //character from it (since we're not backspacing a carriage return
            //in this case).
            if(!lineData[row].hardReturn&&STRLEN(text[row])>0)
                text[row]=STRRTRIM(text[row],1);

            //Position the cursor at the end of the line:
            column = STRLEN(text[row]);

            //If the original line was within the text area, then append the text,
            //preserve the hard return, and erase the line.
            if(row+1 < text.size())
            {
                text[row] += text[row+1];
                lineData[row].hardReturn=lineData[row+1].hardReturn;
                text.erase(text.begin() + row+1);
                lineData.erase(lineData.begin() + row+1);
            }

            parseLine(row);
            adjustWindowSize();
        }
        //If the cursor is far below the text area, then move up a line.
        else
        {
            row--;
            column = 0;
        }
    }

    //Reset the attractor and ensure that the cursor is fully visible at its
    //new location:
    if(row < text.size())
        attractorX = lineData[row].charOffsets[column];
    else
        attractorX = 0;
    checkCurrentPos();
}

/****************************************************************************\
tTextBox::charHandler

Defines the text box's response to any key with an ASCII code between 32 and
127.  In insert mode, the function inserts the character after the cursor.
In type-over mode, the function replaces the character after the cursor.  In
both modes, the cursor moves one column to the right.  The function also parses
the line, adjusts the window size, resets the attractor, and scrolls the window.

Inputs:
    ch          ASCII code (32-127)
Outputs:
    none
\****************************************************************************/
void tTextBox::charHandler(char ch)
{
    //Remove selection if one exists, but don't return since we want to
    //overwrite it:
    if(selection)
        removeSelection();

    //CASE 1: If the cursor is within the text area (meaning text[row][column]
    //is valid), then either insert the character into the line or overwrite it
    //depending on insert/type-over mode.
	if (row < text.size() && column < STRLEN(text[row]))
	{
		if (cursorMode == CURSOR_INSERT)
            text[row] = STRLEFT(text[row],column) + CHRTOSTR(ch) + STRLTRIM(text[row],column);
		else
            text[row][column] = ch;
	}

    //CASE 2: If the cursor is beyond the end of the line, then pad the line
    //with spaces and tack the character on the end.  NOTE: Padding should not
    //occur in this implementation since the user cannot cursor beyond the end
    //of the line.
	else if(row < text.size())
        text[row] += STROFCHR(' ', column - STRLEN(text[row])) + CHRTOSTR(ch);

    //CASE 3: If the cursor is on the line beneath the last line, then add a new
    //line, pad it with spaces, and tack the character on the end.
    else
    {
        text.push_back(STROFCHR(' ', column) + CHRTOSTR(ch));
        tLineData data;
        data.hardReturn=true;
        lineData.push_back(data);
    }

    //Move one space to the right:
    column++;

    parseLine(row);
    adjustWindowSize();
    attractorX = lineData[row].charOffsets[column];
    checkCurrentPos();
}

/****************************************************************************\
tTextBox::ctrlEnd

Defines the text box's response to the Ctrl-End key combination.  The function
moves to the last row and calls end.  It also cancels the selection if the user
is not holding down shift.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::ctrlEnd()
{
    if(!isShiftPressed())
        clearSelection();
    //If the text box is empty, then position the cursor on row 0:
    if(text.size()==0)  //CAREFUL!  size() is unsigned
        row=0;
    else
        row = text.size()-1;
	end();
}

/****************************************************************************\
tTextBox::ctrlHome

Defines the text box's response to the Ctrl-Home key combination.  The function
moves to the first row and calls home.  It also cancels the selection if the user
is not holding down shift.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::ctrlHome()
{
    if(!isShiftPressed())
        clearSelection();
	row = 0;
	home();
}

/****************************************************************************\
tTextBox::del

Defines the text box's response to the Delete key.  Typically, the function
removes the character after the cursor.  It may also remove the selection (if
one exists) or remove a hard return (if the cursor is at the end of the line).
It parses the line, adjusts the window size, resets the attractor, and scrolls
the window if necessary.  NOTE: Although 'delete' seems the logical name for
this function, 'delete' is a reserved word.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::del()
{
    //If the user has highlighted a selection, then remove it.
    if(selection)
    {
        removeSelection();
        return;
    }

    //CASE 1: If the cursor is within the text area (meaning text[row][column]
    //is valid), then remove the character after the cursor.
	if (row < text.size() && column < STRLEN(text[row]))
    {
        text[row] = STRLEFT(text[row], column) + STRLTRIM(text[row], column+1);
        parseLine(row);
        adjustWindowSize();
    }

    //CASE 2: If the cursor is beyond the end of a line and the next line exists,
    //then pad the line with spaces and append the next line.  If the current
    //line ends in a soft return, then remove the first character of the next line.
	else if (row+1 < text.size())
    {
        if(!lineData[row].hardReturn && STRLEN(text[row+1])>0)
            text[row] += STROFCHR(' ', column - STRLEN(text[row])) + STRLTRIM(text[row+1],1);
        else
            text[row] += STROFCHR(' ', column - STRLEN(text[row])) + text[row+1];

        //Preserve the hard / soft return:
        lineData[row].hardReturn=lineData[row+1].hardReturn;

        //Erase the line below:
        text.erase(text.begin() + row+1);
        lineData.erase(lineData.begin() + row+1);

        parseLine(row);
        adjustWindowSize();
    }

    //CASE 3: If the cursor is on the last line but it is empty, then remove it.
    else if (row < text.size() && column==0)
    {
        text.erase(text.begin() + row);
        lineData.erase(lineData.begin() + row);
        //Ensure the line above ends in a hard return:
        if(row>0)
            lineData[row-1].hardReturn=true;
        adjustWindowSize();
        //No need to parse any lines here since we merely removed a blank line.
    }

    //Reset the attractor and ensure that the cursor is fully visible at its
    //new location:
    if(row < text.size())
        attractorX = lineData[row].charOffsets[column];
    else
        attractorX = 0;
    checkCurrentPos();
}

/****************************************************************************\
tTextBox::down

Defines the text box's response to the Down key.  The function increments row
if possible and finds the column nearest to the cursor's previous column (using
attractorX).  The function also cancels/adjusts the selection based on the
shift state.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::down()
{
    if(!isShiftPressed())
        clearSelection();
	if (row < text.size() && row < maxLines-1)
    {
        row++;
        //We use XYToColRow to find the column nearest to the attrator.  NOTE:
        //this function will not change the value of row.
        XYToColRow(attractorX, row*fontHeight(getFontIndex()), column, row, true);
    }
    checkCurrentPos();
    if(isShiftPressed())
        adjustSelection();
}

/****************************************************************************\
tTextBox::end

Defines the text box's response the End key.  The function moves the cursor
to the end of the line and ensures that the last character is fully visible.
It also resets the attractor and cancels/adjusts the selection based on the
shift state.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::end()
{
    if(!isShiftPressed())
        clearSelection();

    //CASE 1: If the cursor is within the text area, then position the cursor
    //-after- the last character.
    if(row < text.size())
    {
    	column = STRLEN(text[row]);
    	if (column > 0)
        {
            //Temporarily shift the column one to the left to ensure that the
            //character before the cursor is fully visible
	    	column--;
            checkCurrentPos();
            column++;
        }
    }

    //CASE 2: If the cursor is below the text area, then position the cursor
    //in the first column.
    else
        column = 0;

	checkCurrentPos();
    if (isShiftPressed())
        adjustSelection();
    if(row < text.size())
        attractorX = lineData[row].charOffsets[column];
    else
        attractorX = 0;
}

/****************************************************************************\
tTextBox::enter

Defines the text box's response to the Enter key.  Typically, the function
splices the current line and moves the latter half to the next line.  The
function may also remove the selection, replace a soft return with a hard
return, or append a blank line to the bottom.  It parses the line, adjusts
the window size, scroll's the window, and resets the attractor if necessary.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::enter()
{
	int i;

    //If the user has highlighted a selection, then remove it.  Do not return
    //since we want to "replace" the selection with a carriage return:
    if(selection)
        removeSelection();

    //Will inserting a new line cause the text area to exceed the maximum number
    //of lines?  If so, we ignore the keystroke:
	if (row < maxLines - 1)
    {
        //If the cursor is at the end of a line ending in a soft return, then
        //simply end it with a hard return and move to the beginning of the next
        //line.
        if(row < text.size() && column >= STRLEN(text[row]) && !lineData[row].hardReturn)
        {
            lineData[row].hardReturn=true;
            row++;
            column=0;
        }

        //CASE 1: If the cursor is not beyond the end of the last line, then
        //splice the current line at the cursor's location:
        else if(row+1 < text.size() || row+1 == text.size() && column < STRLEN(text[row]))
        {
            tLineData data;

            //Insert the text left of the cursor in front of the current line.
            //Terminate the new line with a hard return.
            data.hardReturn=true;
            text.insert(text.begin() + row, STRLEFT(text[row], column));
            lineData.insert(lineData.begin() + row, data);

            //If the cursor is not past the end of the line, then put the text
            //right of the cursor on the next line.  Eliminate spaces from the
            //beginning of this line.
            if (column < STRLEN(text[row+1]))
            {
                for (i = column; i < STRLEN(text[row+1]); i++)
                    if (text[row+1][i] != ' ')
                        break;
                text[row+1] = STRLTRIM(text[row+1], i);
            }
            //Otherwise, this line becomes empty.
            else
                text[row+1] = "";

            //Move to the beginning of the next line:
            row++;
            column = 0;

            //Parse both halves of the line.  Since the lines are separated by a
            //hard return, they will not interfere when each is parsed.
            parseLine(row-1);
            parseLine(row);
        }
        //CASE 2: If the cursor is on the last row, but beyond the end of the
        //line, then simply move to the beginning of the next line.
        else if(row+1 == text.size())   //CAREFUL!  size() is unsigned
        {
            row++;
            column = 0;
            //No need to parse line here since text hasn't changed.
        }
        //CASE 3: Otherwise, the cursor is below the text area.  Add a blank
        //line and move to the beginning of the next line.
        else
        {
            text.push_back("");
            tLineData data;
            data.hardReturn=true;
            lineData.push_back(data);
            row++;
            column = 0;
            //Fill in line data (although no wrapping will occur).
            parseLine(row-1);
        }
        adjustWindowSize();
        if(row < text.size())
            attractorX = lineData[row].charOffsets[column];
        else
            attractorX = 0;
        checkCurrentPos();
    }
}

/****************************************************************************\
tTextBox::home

Defines the text box's response to the Home key.  The function moves to the
cursor to the beginning of the line.  It also cancels/adjusts the selection,
resets the attractor, and scrolls the window.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::home()
{
    if(!isShiftPressed())
        clearSelection();
	column = 0;
	checkCurrentPos();
    if(isShiftPressed())
        adjustSelection();
    if(row < text.size())
        attractorX = lineData[row].charOffsets[column];
    else
        attractorX = 0;
}

/****************************************************************************\
tTextBox::left

Defines the text box's response to the Left key.  Usually, the function
moves the cursor one column to the left.  When the cursor is on the left edge,
the function moves the cursor to the end of the previous line, ensuring that
the last character is fully visible.  In word wrapping mode, it positions the
cursor to the left of the last character on lines ending in soft returns.
Finally, it cancels/adjusts the selection, scrolls the window, and resets the
attractor if necessary.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::left()
{
    if(!isShiftPressed())
        clearSelection();

    //If the cursor is on the left edge, we want to move it to the end of the
    //previous line.
    if(column==0&&row>0)
    {
        row--;
        if(row < text.size())
        {
            column=STRLEN(text[row]);

            //In word wrapping mode, we position the cursor to the left of
            //the last character on lines ending in soft returns.
            if(!lineData[row].hardReturn&&column>0)
                column--;

            //This block of code resembles the 'end' function.  We temporarily
            //decrement column to ensure that the last character on the line
            //is fully visible.
            if(column>0)
            {
                column--;
                checkCurrentPos();
                column++;
            }
            checkCurrentPos();
        }
        else            //Text does not exist on this line.
            column = 0;
    }
    else if (column>0)  //Otherwise, move the cursor one column to the left.
		column--;

	checkCurrentPos();
    if(isShiftPressed())
        adjustSelection();
    if(row < text.size())
        attractorX = lineData[row].charOffsets[column];
    else
        attractorX = 0;
}

/****************************************************************************\
tTextBox::pageDown

Defines the text box's response to the Page Down key.  The function moves the
viewing window and the cursor down by the same amount, aligning the cursor's
column with the attractor.  The function also cancels/adjusts the selection
and scrolls the window if necessary.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::pageDown()
//shift the viewing window downward and increase row
{
    int h=fontHeight(getFontIndex());
#ifdef DEBUG
    if(h<=0)
    {
        bug("tTextBox::pageDown: invalid font height");
        return;
    }
#endif

    //Estimate how many lines we can fit in the viewing window:
    int lines = visWindowHeight() / h;

    if(!isShiftPressed())
        clearSelection();

    //Pan the viewing window down by approximately one viewing window height:
    setWindowPosition(getWindowX(), getWindowY() + lines*fontHeight(getFontIndex()));

    //Increase rows such that the cursor remains in the same relative location
    //after the operation:
    row+=lines;
    if(text.size()==0)  //CAREFUL!  size() is unsigned
        row=0;
    else if(row >= text.size())
        row = text.size()-1;

    //Find closest column match using attractorX:
    XYToColRow(attractorX, row*fontHeight(getFontIndex()), column, row, true);

	checkCurrentPos();
    if(isShiftPressed())
        adjustSelection();
}

/****************************************************************************\
tTextBox::pageUp

Defines the text box's response to the Page Up key.  The function moves the
viewing window and the cursor up by the same amount, aligning the cursor's
column with the attractor.  The function also cancels/adjusts the selection
and scrolls the window if necessary.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::pageUp()
{
    int h=fontHeight(getFontIndex());
#ifdef DEBUG
    if(h<=0)
    {
        bug("tTextBox::pageUp: invalid font height");
        return;
    }
#endif

    //Estimate how many lines we can fit in the viewing window:
    int lines = visWindowHeight() / h;

    if(!isShiftPressed())
        clearSelection();

    //Pan the viewing window up by approximately one viewing window height:
    setWindowPosition(getWindowX(), getWindowY() - lines*fontHeight(getFontIndex()));

    //Decrease rows such that the cursor remains in the same relative location
    //after the operation:
    row-=lines;
    if(row < 0)
        row = 0;

    //Find closest column match using attractorX:
    XYToColRow(attractorX, row*fontHeight(getFontIndex()), column, row, true);

	checkCurrentPos();
    if(isShiftPressed())
        adjustSelection();
}

/****************************************************************************\
tTextBox::insertText

Inserts a string at the cursor location.  This function closely resembles
charHandler, except we are inserting a string of text instead of a character.
The insert/type-over mode does not influence the function's behavior.  The
function also highlights the string (if it isn't empty).

Inputs:
    string          Null-terminated string to insert
Outputs:
    none
\****************************************************************************/
void tTextBox::insertText(const char *string)
{
    if(selection)
        removeSelection();

    //CASE 1: If the cursor is within a line of text, we splice the line and
    //insert the string.
	if (row < text.size() && column < STRLEN(text[row]))
        text[row] = STRLEFT(text[row],column) + string + STRLTRIM(text[row],column);

    //CASE 2: If the cursor is past the end of a line, we pad the line with
    //spaces and append the string.
	else if(row < text.size())
        text[row] += STROFCHR(' ', column - STRLEN(text[row])) + string;

    //CASE 3: If the cursor is below the text area, we append a new line
    //containing the string.
    else
    {
        //Pad the line with spaces if the cursor is not on the left edge.
        text.push_back(STROFCHR(' ', column) + string);
        tLineData data;
        data.hardReturn=true;
        lineData.push_back(data);
    }
	column+=strlen(string);
    parseLine(row);
    adjustWindowSize();
    attractorX = lineData[row].charOffsets[column];
    checkCurrentPos();

    //If the string is not empty, then select it.
    if(*string)
    {
        selection=true;
        selStartCol=column;
        selStartRow=row;
    }
}

/****************************************************************************\
tTextBox::right

Defines the text box's response to the Right key.  The function usually moves
the cursor one column to the right.  When the cursor is at or near the end of
the line, the cursor moves to the first or second column on the next line.
NOTE: The cursor behaves intuitively in both word-wrap mode and normal mode.
The function also cancels/adjusts the selection, scrolls the window, and resets
the attractor if necessary.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::right()
{
    //This function has no effect if the cursor is not in the text area.
    if(row < text.size())
    {
        if(!isShiftPressed())
            clearSelection();

        //If the cursor is at the end of the line, then move the cursor to the
        //beginning of the next line.  In word-wrap mode, we ignore soft returns.
        //Consequently, if you press Right at the end of the line, the cursor
        //moves to the -second- column.  If you press Right when the cursor
        //is -before- the last character, the cursor moves to the -first- column.
        if(column >= STRLEN(text[row]) || !lineData[row].hardReturn && column==STRLEN(text[row])-1)
        {
            if(row<maxLines-1)
            {
                if(!lineData[row].hardReturn && column >= STRLEN(text[row]))
                    column=1;
                else
                    column=0;
            	row++;
            }
        }
        else    //Otherwise, move the cursor one column to the right.
            column++;

        checkCurrentPos();
        if(isShiftPressed())
            adjustSelection();
        if(row < text.size())
            attractorX = lineData[row].charOffsets[column];
        else
            attractorX = 0;
    }
}

/****************************************************************************\
tTextBox::up

Defines the text box's response to the Up key.  The function moves the cursor
to the previous row and finds the closest match for the cursor's column.  The
function also cancels/adjusts the selection and scrolls the window if necessary.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::up()
{
    if(!isShiftPressed())
        clearSelection();
	if (row > 0)
    {
        //Move the cursor to the previous row and find closest match for column
        //based on the attractor.
        row--;
        XYToColRow(attractorX, row*fontHeight(getFontIndex()), column, row, true);
    }
    checkCurrentPos();
    if(isShiftPressed())
        adjustSelection();
}

/****************************************************************************\
tTextBox::clear

Clears the contents of the text box.  Removes all text.  Positions the cursor
in the upper-left corner and scrolls the window likewise.  Resets the attractor
and cancels the selection.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tTextBox::clear()
{
    text.clear();
    lineData.clear();
    column=0;
    row=0;
    setWindowPosition(0,0);
    setWindowSize(0,0);
    attractorX=0;
    selection=false;
}

/****************************************************************************\
tTextBox::replaceString

Replaces one line with the specified string.  The function also parses the line,
adjusts the window size, and clears the selection (so we don't have to worry
about adjusting it).  The line retains its hard or soft return.  It is legal
to replace the line beneath the last line; this is equivalent to calling
addString.  NOTE: The new line may not be visible.

Inputs:
    index       Index of the line to replace (0 <= index <= text.size())
    s           String to substitute
Outputs:
    none
\****************************************************************************/
void tTextBox::replaceString(const tString& s, int index)
{
#ifdef DEBUG
    if(index < 0)
    {
        bug("tTextBox::replaceString: invalid index %d", index);
        return;
    }
#endif
    //replaceString(s, index >= text.size()) is equivalent to addString(s):
    if(index >= text.size())
    {
        addString(s);
        return;
    }
    text[index]=s;
    parseLine(index);
    adjustWindowSize();
    clearSelection();
}

/****************************************************************************\
tTextBox::addString

Appends a line to the bottom of the text box.  The line above retains
its hard return.  The function also parses the line and adjusts the window
size.  NOTE: The new line may not be visible.

Inputs:
    s           String to add
Outputs:
    none
\****************************************************************************/
void tTextBox::addString(const tString& s)
{
#ifdef DEBUG
    if(text.size()>=maxLines)
    {
        bug("tTextBox::addString: adding a line would cause line count to exceed max lines");
        return;
    }
#endif
    text.push_back(s);
    tLineData data;
    data.hardReturn=true;
    lineData.push_back(data);
    parseLine(text.size()-1);
    adjustWindowSize();
    //No need to clear selection since new line won't interfere.
}

/****************************************************************************\
tTextBox::insertString

Inserts a line at the specified index ending in a hard return.  It is legal to
insert a line after the last line; this is equivalent to calling addString.
The function increments the row if the cursor is beneath the new line.  It
also parses the line, adjusts the window size, and cancels the selection (so
we don't have to worry about adjusting it).  NOTE: The new line may not be
visible.

Inputs:
    index       Index of the new line (0 <= index <= text.size());
                    lines beneath will be pushed down.
    s           String to insert
Outputs:
    none
\****************************************************************************/
void tTextBox::insertString(const tString& s, int index)
{
#ifdef DEBUG
    if(text.size()>=maxLines)
    {
        bug("tTextBox::insertString: inserting a line would cause line count to exceed max lines");
        return;
    }
    if(index < 0)
    {
        bug("tTextBox::insertString: invalid index %d", index);
        return;
    }
#endif
    //insertString(s, index >= text.size()) is equivalent to addString(s):
    if(index >= text.size())
    {
        addString(s);
        return;
    }
    text.insert(text.begin() + index, s);
    tLineData data;
    data.hardReturn=true;
    lineData.insert(lineData.begin() + index, data);
    if(row>=index)
        row++;
    parseLine(index);
    adjustWindowSize();
    clearSelection();
}

/****************************************************************************\
tTextBox::deleteString

Removes a line from the text box.  The line above retains the old line's hard
or soft return.  The function decrements the row if the cursor is beneath
the old line.  It also parses the line, adjusts the window size, and cancels
the selection (so we don't have to worry about adjusting it).

Inputs:
    index       Index (0 <= index < text.size())
Outputs:
    none
\****************************************************************************/
void tTextBox::deleteString(int index)
{
#ifdef DEBUG
    if(index < 0 || index >= text.size())
    {
        bug("tTextBox::deleteString: invalid index %d", index);
        return;
    }
#endif
    //Preserve hard return:
    if(index>0&&!lineData[index-1].hardReturn)
        lineData[index-1].hardReturn=lineData[index].hardReturn;
    text.erase(text.begin() + index);
    lineData.erase(lineData.begin() + index);
    clearSelection();

    //If we removed the line that the cursor was on, then move the cursor to the
    //left column.  The cursor is beneath the line we removed, then decrement
    //row.
    if(row==index)
        column=0;
    else if(row>index)
        row--;

    //If we delete the last line, there's nothing to parse since the line above
    //ended in a hard return.
    if(index<text.size())
        parseLine(index);
    adjustWindowSize();
}

/****************************************************************************\
tTextBox::tTextBox

Creates and initializes a new instance of tTextBox.  NOTES: (1) You may
not specify word-wrap mode and a maximum line count in the same control.
If you specify word-wrap mode, you must set maxLines to -1.  (2) If value
contains more lines than the maximum line count would allow, the constructor
increases the maximum line count.  (3) If you specify readOnly, the user
will not see the cursor nor will the user be able to select a block of text.

Inputs:
    controlX, controlY      Coordinates of control's lower-left corner
    width, height           Control's dimensions
    value                   New-line-delimited string containing the control's
                                initial contents.  Ex: "Hello\nWorld" puts
                                "Hello" on line 1 and "World" on line 2.
    _maxLines               The maximum number of lines the control can hold.
                                Specify -1 if the user can theoretically enter
                                an infinite number of lines.
    createVertScrollBar     Will the text box have a vertical scroll bar?
    createHorScrollBar      Will the text box have a horizontal scroll bar?
    fontIndex               Font with which to render the control's contents
    _wordWrap               Will lines wrap when they exceed the width of the
                                viewing window?
    _readOnly               Is the user prohibited from changing the control's
                                contents?
\****************************************************************************/
tTextBox::tTextBox(int controlX, int controlY, int width, int height,
    const tString& value, int _maxLines, bool createVertScrollBar,
    bool createHorScrollBar, int fontIndex, bool _wordWrap, bool _readOnly) :
    tStdScrollWindow(controlX,controlY,width,height,true,0,0,0,0,
    createVertScrollBar, createHorScrollBar, fontIndex), maxLines(_maxLines),
    wordWrap(_wordWrap), readOnly(_readOnly)
{
    type=CTRL_TEXT_BOX;

    //Text box begins in insert mode.
    cursorMode=CURSOR_INSERT;

    //Cursor begins in the upper-left corner.
    column = 0;
    row = 0;

    //Use loadStringList to parse 'value' into one or more lines:
    loadStringList(text, CHARPTR(value));

    while(lineData.size()<text.size())
    {
        //For each line, append a corresponding line data structure with hard
        //return and call parse line.
        tLineData data;
        data.hardReturn=true;
        lineData.push_back(data);
        //This will always correspond to line i, although it may no longer be
        //at index i after wrapping.
        parseLine(lineData.size()-1);
    }
    adjustWindowSize();

    //Setting maxLines to 0 or a negative number means you don't care about the
    //number of lines.
    if(maxLines <= 0)
        maxLines = INT_MAX;
    //I can see no practical purpose of imposing a maximum number of lines in
    //word-wrap mode.  Therefore, when the user specifies both, I eliminate
    //the upper limit on line count.
    else if(wordWrap)
    {
        bug("tTextBox::tTextBox: you cannot specify a maximum number of lines and wordWrap together");
        maxLines = INT_MAX;
    }

    //If the initial value pushes the line count over the maximum line count,
    //then adjust maximum line count.
    if(text.size() > maxLines)
        maxLines = text.size();

    //Initialize other state variables:
    cursorVisible=false;
    attractorX = 0;
    selection = false;
    scrollX = -1;
}



/****************************************************************************\
tEdit::tEdit

Creates and initializes a new instance of tEdit.  A tEdit control only has
one line and does not contain scroll bars.  NOTE: The constructor bases the
control's height on the font's height.

Inputs:
    controlX, controlY      Coordinates of control's lower-left corner
    width                   Control's width
    value                   Control's initial contents
    fontIndex               Font with which to render the control's contents
    readOnly                Is the user prohibited from changing the control's
                               contents?

\****************************************************************************/
tEdit::tEdit(int controlX, int controlY, int width, const tString& value,
    int fontIndex, bool readOnly) : tStdTextBox(controlX, controlY, width,
    tStdTextBox::MARGIN*2 + fontHeight(fontIndex), value, 1, false, false,
    fontIndex, false, readOnly)
{
    type=CTRL_EDIT;
}

/****************************************************************************\
tEdit::onSetSize

Prohibits the user from changing the control's height.  The control's height
always equals the margin (x2) plus the height of one line.
\****************************************************************************/
void tEdit::onSetSize(int width, int height)
{
    tStdTextBox::onSetSize(width, tStdTextBox::MARGIN*2 + fontHeight(getFontIndex()));
}

/****************************************************************************\
tEdit::getString

Retrieves the first and only line of text.

Inputs:
    none
Outputs:
    Returns EMPTY_STRING if the control is empty
    Otherwise, returns the first line as a tString object.
\****************************************************************************/
const tString& tEdit::getString()
{
    if(getLineCount() > 0)
        return getText()[0];
    else
        return EMPTY_STRING;
}

/****************************************************************************\
tEdit::setString

Sets the first and only line of text.  Moves the cursor to the end of the line.

Inputs:
    s           New contents of control
Outputs:
    none
\****************************************************************************/
void tEdit::setString(const tString& s)
{
    //replaceString will call addString if the text box is empty.
    replaceString(s, 0);
    end();
}


   
/****************************************************************************\
tListBox::adjustWindowSize

Calculates the dimensions of the control's contents.  The width equals the
length of the longest line.  The height equals the number of choices multiplied
by the font height.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tListBox::adjustWindowSize()
{
    int i, len, max=0;
    for(i=0;i<choiceCount;i++)
    {
        len=stringWidth(CHARPTR(choices[i]),getFontIndex());
        if(len>max)
            max=len;
    }
    setWindowSize(max, choiceCount * fontHeight(getFontIndex()));
}

/****************************************************************************\
tListBox::checkCurrentPos

Ensures that the selected choice is fully visible.  The function makes two
calls to tScrollWindow::checkXY.  The first examines the top of the choice;
the second examines the bottom of the choice.  NOTE: The function does not
scroll the window horizontally.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tListBox::checkCurrentPos()
{
    //The function has no effect if no choice is selected.
    if(choice >= 0)
    {
        checkXY(getWindowX(),choice*fontHeight(getFontIndex()));
        checkXY(getWindowX(),(choice+1)*fontHeight(getFontIndex()));
    }
}

/****************************************************************************\
tListBox::poll

Scrolls the window when the user drags the mouse beyond the viewing window's
boundaries.  The function operates the same way as tTextBoc::poll.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tListBox::poll()
{
    tStdScrollWindow::poll();
    if (isMousePressed() && isSelected() && !vertSelected && !horSelected && scrollX != -1)
    {
        if(currentTime - scrollTime > .1)
        {
            scrollTime = currentTime;
            onMouseMove(scrollX,scrollY);
        }
    }
}

/****************************************************************************\
tListBox::onMouseDown

Determine which choice the user has selected.  If the user selects a new choice,
then generate an onChange event.  In any event, ensure the current choice is
visible and reset the scroll cycle.
\****************************************************************************/
void tListBox::onMouseDown(int x, int y, eMouseButton button)
{
    int newChoice;
    int h=fontHeight(getFontIndex());
#ifdef DEBUG
    if(h<=0)
    {
        bug("tListBox::onMouseDown: invalid font height");
        return;
    }
#endif

    tStdScrollWindow::onMouseDown(x,y,button);
    //Ignore mouse events when the user positions the mouse over a scroll bar.
    if (vertSelected || horSelected)
        return;

    //Adjust x and y so that they are relative to the top-left corner of the
    //viewing window.
    x-=visWindowX();
    y=visWindowHeight()+visWindowY()-y;

    //Determine which choice the user has selected.  If user clicks below the
    //last choice, assume he meant to click the last choice.
    newChoice = (y+getWindowY())/h;
    if (newChoice < 0)
        newChoice = 0;
    if(newChoice >= choiceCount)
        newChoice = choiceCount - 1;

    if(newChoice != choice)
    {
        choice=newChoice;

        //Ensure the new choice is visible and generate an onChange event.
        //This statement was placed before onChange intentionally; do not collect
        //checkCurrentPos statements after if-else block.
        checkCurrentPos();
        onChange();
    }
    else
        checkCurrentPos();  //Ensure the old choice is still visible.

    //Reset the scroll cycle.
    scrollX = -1;
    scrollTime = currentTime;
}

/****************************************************************************\
tListBox::onMouseMove

Permits the user to scroll the choices by holding the mouse down and dragging it
above or below the viewing window.  The function initiates or ceases scrolling
based on the mouse position.  It also updates the current choice, ensures
that it is fully visible, and generates an onChange event if it has changed.
\****************************************************************************/
void tListBox::onMouseMove(int x, int y)
{
    int newChoice;
    int absx = x, absy = y;
    int h=fontHeight(getFontIndex());
#ifdef DEBUG
    if(h<=0)
    {
        bug("tListBox::onMouseMove: invalid font height");
        return;
    }
#endif

    tStdScrollWindow::onMouseMove(x,y);

    //Ignore this event unless the user drags the mouse across the main window.
    if (vertSelected || horSelected || !isMousePressed())
        return;

    //Adjust x and y so that they are relative to the top-left corner of the
    //viewing window.
    x-=visWindowX();
    y=visWindowHeight()+visWindowY()-y;

    //Start scrolling when the user drags the mouse beyond the viewing window's
    //boundaries and vice versa.
    if (x < 0 || x >= visWindowWidth() || y < 0 || y >= visWindowHeight())
    {
        scrollX = absx;
        scrollY = absy;
    }
    else
        scrollX = -1;

    //Determine which choice the user has selected.  If user drags the mouse
    //beneath the last choice, leave the last choice highlighted.
    newChoice = (y+getWindowY())/h;
    if (newChoice < 0)
        newChoice = 0;
    if(newChoice >= choiceCount)
        newChoice = choiceCount - 1;

    if(newChoice != choice)
    {
        choice=newChoice;

        //Ensure the new choice is visible and generate an onChange event.
        //This statement was placed before onChange intentionally; do not collect
        //checkCurrentPos statements after if-else block.
        checkCurrentPos();
        onChange();
    }
    else
        checkCurrentPos();  //Ensure the old choice is still visible.
}

/****************************************************************************\
tListBox::onKeyDown

Responds to keystrokes by calling the appropriate key handler.
\****************************************************************************/
void tListBox::onKeyDown(tKey key, char ch)
{
    tStdScrollWindow::onKeyDown(key,ch);
    switch(key)
    {
        case GLFW_KEY_DOWN:
            down();
            break;
        case GLFW_KEY_UP:
            up();
            break;
        case GLFW_KEY_PAGEUP:
            pageUp();
            break;
        case GLFW_KEY_PAGEDOWN:
            pageDown();
            break;
        case GLFW_KEY_HOME:    //Home and Ctrl+Home have the same effect.
            home();
            break;
        case GLFW_KEY_END:     //End and Ctrl+End have the same effect.
            end();
            break;
    }
}

/****************************************************************************\
tListBox::home

Defines the control's response to the Home key.  This function highlights the
first choice and generates an onChange event if the current choice changes.
It also ensures that the current choice is fully visible.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tListBox::home()
{
    //The function has no effect if the control does not contain any choices.
    if(choiceCount > 0)
    {
        if(choice != 0)
        {
            choice=0;
            checkCurrentPos();
            onChange();
        }
        else
            checkCurrentPos();
    }
}

/****************************************************************************\
tListBox::end

Defines the control's response to the End key.  The function highlights the
last choice and generates an onChange event if the current choice changes.
It also ensures that the current choice is fully visible.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tListBox::end()
{
    int newChoice;

    //The function has no effect if the control does not contain any choices.
    if(choiceCount > 0)
    {
        newChoice = choiceCount - 1;
        if(newChoice != choice)
        {
            choice=newChoice;
            checkCurrentPos();
            onChange();
        }
        else
            checkCurrentPos();
    }
}

/****************************************************************************\
tListBox::up

Defines the control's response to the Up key.  The highlights the previous
choice (if one exists) and generates an onChange event if the current choice
changes.  It also ensures that the current choice is fully visible.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tListBox::up()
{
    //The function has no effect if the control does not contain any choices.
    if(choiceCount > 0)
    {
        if(choice>0)
        {
            choice--;
            checkCurrentPos();
            onChange();
        }
        else
            checkCurrentPos();
    }
}

/****************************************************************************\
tListBox::down

Defines the control's response to the Down key.  The function highlights the
next choice (if one exists) and generates and onChange event if the current
choice changes.  It also ensures that the current choice is fully visible.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tListBox::down()
{
    //The function has no effect if the control does not contain any choices.
    if(choiceCount>0)
    {
        if(choice<choiceCount-1)
        {
            choice++;
            checkCurrentPos();
            onChange();
        }
        else
            checkCurrentPos();
    }
}

/****************************************************************************\
tListBox::pageUp

Pans the window up by a discrete number of lines.  Decreases the current choice
by the same number.  Generates an onChange event if the current choice changes
and ensures that the choice is fully visible.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tListBox::pageUp()
{
    int h=fontHeight(getFontIndex());
#ifdef DEBUG
    if(h<=0)
    {
        bug("tListBox::pageUp: invalid font height");
        return;
    }
#endif

    //Estimate how many choices will fit in the viewing window.
    int lines = visWindowHeight() / h;
    if(choice > 0)
    {
        //Scroll the window up and reduce the current choice so that the choice
        //remains in the same relative location.
        setWindowPosition(getWindowX(), getWindowY() - lines*fontHeight(getFontIndex()));
        choice-=lines;

        //Clamp the choice at 0.
        if(choice < 0)
            choice = 0;
        checkCurrentPos();
        onChange();
    }
}

/****************************************************************************\
tListBox::pageDown

Pans the window down by a discrete number of lines.  Increases the current choice
by the same number.  Generates an onChange event if the current choice changes
and ensures that the choice is fully visible.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tListBox::pageDown()
{
    int h=fontHeight(getFontIndex());
#ifdef DEBUG
    if(h<=0)
    {
        bug("tListBox::pageDown: invalid font height");
        return;
    }
#endif

    //Estimate how many choices will fit in the viewing window.
    int lines = visWindowHeight() / h;
    if(choice < choiceCount - 1)
    {
        //Scroll the window up and reduce the current choice so that the choice
        //remains in the same relative location.
        setWindowPosition(getWindowX(), getWindowY() + lines*fontHeight(getFontIndex()));
        choice+=lines;

        //Clamp the choice at choiceCount-1.
        if(choice >= choiceCount)
            choice = choiceCount - 1;
        checkCurrentPos();
        onChange();
    }
}

/****************************************************************************\
tListBox::tListBox

Creates and initializes a new instance of tListBox.

Inputs:
    controlX, controlY      Coordinates of control's lower-left corner
    width, height           Control's dimensions
    canFocus                Can the control receive focus?
    value                   New-line-delimited series of choices.  Ex:
                                "Hello\nWorld" creates two choices,
                                "Hello" and "World".
    _choice                 Index of highlighted control.  Specify -1 for no
                                initial selection.
    createVertScrollBar     Will the control have a vertical scroll bar?
    createHorScrollBar      Will the control have a horizontal scroll bar?
    fontIndex               Font with which to render the choices
\****************************************************************************/
tListBox::tListBox(int controlX, int controlY, int width, int height, bool canFocus,
    const tString& value, int _choice, bool createVertScrollBar, bool createHorScrollBar,
    int fontIndex) : tStdScrollWindow(controlX,controlY,
    width,height,canFocus,0,0,0,0,createVertScrollBar, createHorScrollBar,
    fontIndex), choice(_choice)
{
    type = CTRL_LIST_BOX;

    //Load the new-line-delimited series of choices from value.
    loadStringList(choices, CHARPTR(value));

    choiceCount = choices.size();   //Initialize choiceCount.
    adjustWindowSize();             //Adjust text area's size.

    //Verify that the _choice argument is valid (-1 means no choice is selected)
    if(choice < -1)
        choice = -1;
    if(choice >= choiceCount)
        choice = choiceCount-1;

    //Position window so that choice is visible:
    checkCurrentPos();

    scrollX = -1;
}

/****************************************************************************\
tListBox::clear

Removes all choices from the control and resets variables.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tListBox::clear()
{
    choices.clear();
    choiceCount = choices.size();
    adjustWindowSize();
    choice = -1;
}

/****************************************************************************\
tListBox::replaceChoice

Replaces an existing choice with a new choice.  NOTE: Replacing the choice
after the last choice is equivalent to calling addChoice.  The function also
updates the size of the control's contents.

Inputs:
    index       Index of the choice to replace (0 <= index <= choiceCount)
    s           Choice to substitute
Outputs:
    none
\****************************************************************************/
void tListBox::replaceChoice(const tString& s, int index)
{
#ifdef DEBUG
    if(index < 0)
    {
        bug("tListBox::replaceChoice: invalid index %d",index);
        return;
    }
#endif
    if(index >= choiceCount)
    {
        addChoice(s);
        return;
    }
    choices[index]=s;
    adjustWindowSize();
}

/****************************************************************************\
tListBox::addChoice

Adds a choice to the end of the list.  Increments choiceCount and updates the
size of the control's contents.

Inputs:
    s           Choice to add
Outputs:
    none
\****************************************************************************/
void tListBox::addChoice(const tString& s)
{
    choices.push_back(s);
    choiceCount = choices.size();
    adjustWindowSize();
}

/****************************************************************************\
tListBox::insertChoice

Inserts a choice at the specified index.  Inserting a choice after the last
choice is equivalent to calling addChoice.  The function also increments
choiceCount, updates the size of the control's contents, and increments
the current choice if necessary.

Inputs:
    index       Index at which to insert choice (0 <= index <= choiceCount)
    s           String to insert
Outputs:
    none
\****************************************************************************/
void tListBox::insertChoice(const tString& s, int index)
{
#ifdef DEBUG
    if(index < 0)
    {
        bug("tListBox::insertChoice: index<0");
        return;
    }
#endif
    if(index >= choiceCount)
    {
        addChoice(s);
        return;
    }
    choices.insert(choices.begin() + index, s);
    choiceCount = choices.size();
    adjustWindowSize();
    if(choice >= index)
        choice++;
}

/****************************************************************************\
tListBox::deleteChoice

Removes the choice at the specified index, pushing up subsequent choices to fill
the gap.  The function also decrements choiceCount, updates the size of the
control's contents, and decrements the current choice if necessary.

Inputs:
    index       Index of the choice to remove
Outputs:
    none
\****************************************************************************/
void tListBox::deleteChoice(int index)
{
#ifdef DEBUG
    if(index < 0 || index >= choiceCount)
    {
        bug("tListBox::deleteChoice: invalid index %d",index);
        return;
    }
#endif
    choices.erase(choices.begin() + index);
    choiceCount = choices.size();
    adjustWindowSize();
    //If the old choice was highlighted, then do not highlight any choices.
    if(choice == index)
        choice = -1;
    else if(choice > index)
        choice--;
    if(choice >= choiceCount)
        choice = choiceCount-1;
}

/****************************************************************************\
tListBox::setChoice

Sets the current choice from outside the class.  You may specify -1 if you do
not want any choices to be highlighted.  The function verifies that the
specified index is valid and ensures that the new choice is fully visible.
NOTE: This function does -not- generate an onChange event.

Inputs:
    _choice         Index of new selection
Outputs:
    none
\****************************************************************************/
void tListBox::setChoice(int _choice)
{
    choice=_choice;
    if(choice<-1)
        choice=-1;
    if(choice>=choiceCount)
        choice=choiceCount-1;
    checkCurrentPos();
}

/****************************************************************************\
tListBox::getChoiceString

Retrieves the value of the highlighted choice.

Inputs:
    none
Outputs:
    Returns EMPTY_STRING if no choice is highlighted
    Otherwise, returns a reference to the highlighted choice's text.
\****************************************************************************/
const tString& tListBox::getChoiceString()
{
    if(choice>=0&&choice<choiceCount)
        return choices[choice];
    else
        return EMPTY_STRING;
}



/****************************************************************************\
tDropDown::onMouseMove

This routine is identical to tListBox::onMouseMove, except that the user does
not need to press a mouse button to select a new choice.
\****************************************************************************/
void tDropDown::onMouseMove(int x, int y)
{
    int newChoice;
    int absx=x, absy=y;
    int h=fontHeight(getFontIndex());
#ifdef DEBUG
    if(h<=0)
    {
        bug("tDropDown::onMouseMove: invalid font height");
        return;
    }
#endif

    //Ignore tListBox's onMouseMove event handler.
    tStdScrollWindow::onMouseMove(x,y);

    //Ignore this event unless the user -moves- the mouse across the main window.
    //This is the only line which differs from tListBox::onMouseMove.  Note the
    //omission of "|| !isMousePressed()".
    if (vertSelected || horSelected)
        return;

    //Adjust x and y so that they are relative to the top-left corner of the
    //viewing window.
    x-=visWindowX();
    y=visWindowHeight()+visWindowY()-y;

    //Start scrolling when the user moves the mouse beyond the viewing window's
    //boundaries and vice versa.
    if (x < 0 || x >= visWindowWidth() || y < 0 || y >= visWindowHeight())
    {
        scrollX = absx;
        scrollY = absy;
    }
    else
        scrollX = -1;

    //Determine which choice the user has selected.  If user moves the mouse
    //beneath the last choice, leave the last choice highlighted.
    newChoice = (y+getWindowY())/h;
    if (newChoice < 0)
        newChoice = 0;
    if(newChoice >= choiceCount)
        newChoice = choiceCount - 1;

    if(newChoice != choice)
    {
        choice=newChoice;

        //Ensure the new choice is visible and generate an onChange event.
        //This statement was placed before onChange intentionally; do not collect
        //checkCurrentPos statements after if-else block.
        checkCurrentPos();
        onChange();
    }
    else
        checkCurrentPos();  //Ensure the old choice is still visible.
}

/****************************************************************************\
tDropDown::onMouseUp

Unlike its ancestor, releasing the mouse button over the viewing window hides
(closes) the drop-down box and informs the parent which choice was selected.
\****************************************************************************/
void tDropDown::onMouseUp(int x, int y, eMouseButton button)
{
    //Ignore tListBox's onMouseUp event handler.
    tStdScrollWindow::onMouseUp(x,y,button);

    //Ignore the event unless it occurred in the main window.
    if (vertSelected || horSelected)
        return;

    hide();
    if(choice >= 0)
        ((tComboBox *) getParent())->onConfirmChoice(choice);
}

/****************************************************************************\
tDropDown::onKeyDown

Inherits the behavior of tListBox but adds one additional case for Return.
Pressing Return on a choice will hide (close) the drop-down box and inform
the parent which choice was selected.
\****************************************************************************/
void tDropDown::onKeyDown(tKey key, char ch)
{
    //Include tListBox's onKeyDown event handler.
    tListBox::onKeyDown(key,ch);
    if(key == GLFW_KEY_ENTER)
    {
        hide();
        ((tComboBox *) getParent())->onConfirmChoice(choice);
    }
}

/****************************************************************************\
tDropDown::onChange

Notifies the parent when the user selects a new choice from the drop-down box.
\****************************************************************************/
void tDropDown::onChange()
{
    ((tComboBox *) getParent())->onSetChoice(choice);
}

/****************************************************************************\
tDropDown::tDropDown

Creates and initializes a new instance of tDropDown.  Do not instantiate
this class in other modules.  We have specially tailored tDropDown to serve
the tComboBox class.  It will not function properly alone.

Inputs:
    controlX, controlY      Coordinates of control's lower-left corner
    width, height           Control's dimensions
    canFocus                Can the control receive focus?
    value                   New-line-delimited series of choices.  Ex:
                                "Hello\nWorld" creates two choices,
                                "Hello" and "World".
    choice                  Index of highlighted control.  Specify -1 for no
                                initial selection.
    createVertScrollBar     Will the control have a vertical scroll bar?
    createHorScrollBar      Will the control have a horizontal scroll bar?
    fontIndex               Font with which to render the choices
\****************************************************************************/
tDropDown::tDropDown(int controlX, int controlY, int width, int height,
    bool canFocus, const tString& value, int choice, bool createVertScrollBar,
    bool createHorScrollBar, int fontIndex) :
    tStdListBox(controlX, controlY, width, height, canFocus, value,
    choice, createVertScrollBar, createHorScrollBar, fontIndex)
{
    type = CTRL_DROP_DOWN;
    hide();     //Control is initially invisible.
}

/****************************************************************************\
tDropDown::hide

Instructs the control's parent to hide it.  If it does not have a parent,
then it calls onHide.  We must override this function since the default
definition for 'hide' does not apply to tDropDown.
\****************************************************************************/
void tDropDown::hide()
{
    if(getParent())
        ((tComboBox *)getParent())->hideDropDownBox(this);
    else
        onHide();
}

/****************************************************************************\
tDropDown::onDisable

We see no need to disable this control, as we will always associate it with a
combo box.
\****************************************************************************/
void tDropDown::onDisable()
{
    bug("tDropDown::onDisable: this control doesn't support onDisable");
}

/****************************************************************************\
tDropDown::onEnable

We see no need to enable this control, as we will always associate it with a
combo box.
\****************************************************************************/
void tDropDown::onEnable()
{
    bug("tDropDown::onEnable: this control doesn't support onEnable");
}



/****************************************************************************\
tComboBox::poll

Polls self and drop down box (if one exists).
\****************************************************************************/
void tComboBox::poll()
{
    tStdScrollWindow::poll();
    if(dropDownBox)
        dropDownBox->poll();
}

/****************************************************************************\
tComboBox::onMouseDown

Routes the event to the drop-down box if the user clicks inside of it.
Otherwise, toggles the visibility of the drop-down box and sets lockSelect
accordingly.  The function also highlights the correct choice when the drop-down
box becomes visible.
\****************************************************************************/
void tComboBox::onMouseDown(int x, int y, eMouseButton button)
{
    tStdScrollWindow::onMouseDown(x,y,button);

    //Combo box should always have a drop down box but just to be on the safe side...
    if (dropDownBox == NULL)
        return;

    if(dropDownBox->isVisible())
    {
        if(dropDownBox->activated(x,y))
            dropDownBox->onMouseDown(x,y,button);  //Route the event to the drop-down box.
        else
        {
            //Hide the drop-down box and clear lockSelect (meaning the user can
            //select other controls).
            dropDownBox->onHide();
            setLockSelect(false);
        }
    }
    else
    {
        //Show the drop-down box and generate an onSelect event at an arbitrary
        //point.  Initialize it with the correct selection.
        dropDownBox->onShow();
        dropDownBox->onSelect(0,0);
        dropDownBox->setChoice(choice);

        //isLatched determines whether the user has moved the mouse over the
        //drop-down box since it was last shown.  We use this value to determine
        //whether to pass onMouseMove events to the drop-down box.
        isLatched=false;

        //Set lockSelect (meaning the user cannot select other controls;
        //this control retains selection wherever the user moves the mouse).
        setLockSelect(true);
    }
}

/****************************************************************************\
tComboBox::onMouseMove

Routes the event to the drop-down box and latches it under the following
circumstances:
(1) The drop-down box is visible.
(2) The mouse is within the box or the box is already latched.
\****************************************************************************/
void tComboBox::onMouseMove(int x, int y)
{
    tStdScrollWindow::onMouseMove(x,y);

    //Combo box should always have a drop down box but just to be on the safe side...
    if(dropDownBox == NULL)
        return;

    //When the user moves the mouse across the drop-down box, we route the
    //event to it.  When the user moves the mouse -outside- the drop-down box,
    //we route the event to it -only if- the box is latched.
    if (dropDownBox->isVisible() && (dropDownBox->activated(x,y) || isLatched))
    {
        //The drop-down box becomes latched, meaning it will continue to receive
        //onMouseMove events regardless of where the user moves the mouse.
        isLatched=true;
        dropDownBox->onMouseMove(x,y);
    }
}

/****************************************************************************\
tComboBox::onMouseUp

Routes the event to the drop-down box when the user releases the mouse anywhere
outside the combo box.  Releasing the mouse inside the combo box has no effect.
The function also resets lockSelect if the drop-down box is no longer visible.
\****************************************************************************/
void tComboBox::onMouseUp(int x, int y, eMouseButton button)
{
    tStdScrollWindow::onMouseUp(x,y,button);

    //Combo box should always have a drop down box but just to be on the safe side...
    if(dropDownBox == NULL)
        return;

    if (dropDownBox->isVisible() && !objectActivated(x,y))
    {
        //Route the event to the drop-down box.
        dropDownBox->onMouseUp(x,y,button);

        //dropDownBox::onMouseUp will not always hide itself (e.g. the user
        //clicks on a scroll bar).  Therefore, we reset lockSelect only if
        //the drop-down box is no longer visible.
        if (!dropDownBox->isVisible())
            setLockSelect(false);
    }
}

//This function serves the same purpose as onMouseUp et al.
void tComboBox::onMouseWheel(int pos,eMouseWheel direction)
{
    tStdScrollWindow::onMouseWheel(pos,direction);
    if(dropDownBox == NULL)
        return;
    if (dropDownBox->isVisible() && !objectActivated(saveMouseX,saveMouseY))
    {
        dropDownBox->onMouseWheel(pos,direction);
        if (!dropDownBox->isVisible())
            setLockSelect(false);
    }
}

/****************************************************************************\
tComboBox::onKeyDown

Shows and initializes the drop-down if it was formerly hiden.  Routes the event
to the drop-down box if it is visible.  The function sets isLatched when the
box becomes visible and resets isLatched when it becomes hidden.
\****************************************************************************/
void tComboBox::onKeyDown(tKey key, char ch)
{
    tStdScrollWindow::onKeyDown(key,ch);

    //Ignore modifier keys (e.g. shift, alt, and ctrl):
    if(isModifierKey(key))
        return;

    //Combo box should always have a drop down box but just to be on the safe side...
    if(dropDownBox == NULL)
        return;

    if(!dropDownBox->isVisible())
    {
        //Same instructions as onMouseDown:
        dropDownBox->onShow();
        dropDownBox->onSelect(0,0);
        dropDownBox->setChoice(choice);
        isLatched = false;
        setLockSelect(true);
    }
    else
    {
        //dropDownBox::onMouseUp may hide itself.  Therefore, we reset
        //lockSelect if the box becomes invisible.
        dropDownBox->onKeyDown(key,ch);
        if (!dropDownBox->isVisible())
            setLockSelect(false);
    }
}

/****************************************************************************\
tComboBox::onDefocus

Hides the drop-down box and clears lockSelect when the control loses its
focus.
\****************************************************************************/
void tComboBox::onDefocus()
{
    tStdScrollWindow::onDefocus();

    //Combo box should always have a drop down box but just to be on the safe side...
    if(dropDownBox == NULL)
        return;

    if(dropDownBox->isVisible())
    {
        dropDownBox->onHide();
        setLockSelect(false);
    }
}

/****************************************************************************\
tComboBox::onDeselect

Hides the drop-down box and clears lockSelect when the control loses its
selection.  NOTE: The control will not lose its selection until the user
clicks elsewhere if lockSelect is set.
\****************************************************************************/
void tComboBox::onDeselect()
{
    tStdScrollWindow::onDeselect();

    //Combo box should always have a drop down box but just to be on the safe side...
    if(dropDownBox == NULL)
        return;

    if(dropDownBox->isVisible())
    {
        dropDownBox->onHide();
        setLockSelect(false);
    }
}

/****************************************************************************\
tComboBox::onSetChoice

The drop-down box calls this function when the user highlights a new choice
from the list.  The combo box responds by displaying the choice in its own
box.  NOTE: This function does -not- generate an onChange event.

Inputs:
    _choice         Index of the choice that the user highlighted
Outputs:
    none
\****************************************************************************/
void tComboBox::onSetChoice(int _choice)
{
    setChoice(_choice);
}

/****************************************************************************\
tComboBox::onConfirmChoice

The drop-down box calls this function when the user confirms a choice from the
list by pressing Enter or releasing the mouse.  The combo box responds by
displaying the choice in its own box and generating an onChange event.

Inputs:
    _choice         Index of the choice that the user confirmed
Outputs:
    none
\****************************************************************************/
void tComboBox::onConfirmChoice(int _choice)
{
    setChoice(_choice);
    onChange();
}

/****************************************************************************\
tComboBox::setChoice

Call this function to set the choice from outside the class.  The function
ensures that the choice is valid and passes the request to the drop down
box.  NOTE: -1 -is- valid.  Specify -1 to clear the combo box.

Inputs:
    _choice         Index of new choice (-1 <= choice < choiceCount)
Outputs:
    none
\****************************************************************************/
void tComboBox::setChoice(int _choice)
{
    choice=_choice;
    if(choice<-1)
        choice=-1;
    if(choice>=choiceCount)
        choice=choiceCount-1;
    if(dropDownBox)
        dropDownBox->setChoice(choice);
}

/****************************************************************************\
tComboBox::clear

Clears the contents of the combo box and its drop-down box.  Resets choiceCount
and choice.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tComboBox::clear()
{
    choices.clear();
    choiceCount = choices.size();
    choice = -1;
    if(dropDownBox)
        dropDownBox->clear();
}

/****************************************************************************\
tComboBox::replaceChoice

Replaces choice at the specified index with a new choice.  NOTE: Replacing
the choice after the last choice is valid.  This is equivalent to calling
addChoice.  The function repeats the action on the drop-down box.

Inputs:
    index           Index of choice to replace
    s               Choice to substitute
Outputs:
    none
\****************************************************************************/
void tComboBox::replaceChoice(const tString& s, int index)
{
#ifdef DEBUG
    if(index < 0)
    {
        bug("tComboBox::replaceChoice: invalid index %d",index);
        return;
    }
#endif
    if(index >= choiceCount)
    {
        addChoice(s);
        return;
    }
    choices[index]=s;

    //Combo box should always have a drop down box but just to be on the safe side...
    if(dropDownBox)
        dropDownBox->replaceChoice(s,index);
}

/****************************************************************************\
tComboBox::addChoice

Adds a new choice to the end of the combo box and increments choiceCount.
Repeats the operation on the drop-down box.

Inputs:
    s               Choice to add
Outputs:
    none
\****************************************************************************/
void tComboBox::addChoice(const tString& s)
{
    choices.push_back(s);
    choiceCount = choices.size();

    //combo box should always have a drop down box but just to be on the safe side...
    if(dropDownBox)
        dropDownBox->addChoice(s);
}

/****************************************************************************\
tComboBox::insertChoice

Inserts a choice at the specified index.  NOTE: You may insert a choice after
the last choice.  This is equivalent to calling addChoice.  The function
increments choiceCount and pushes the current choice down one if necessary.
It then repeats the operation on the drop-down box.

Inputs:
    index           Index at which to insert the choice (0 <= index <= choiceCount)
    s               String to insert
Outputs:
    none
\****************************************************************************/
void tComboBox::insertChoice(const tString& s, int index)
{
#ifdef DEBUG
    if(index < 0)
    {
        bug("tComboBox::insertChoice: index<0");
        return;
    }
#endif
    if(index >= choiceCount)
    {
        addChoice(s);
        return;
    }
    choices.insert(choices.begin() + index, s);
    choiceCount = choices.size();
    if(choice >= index)
        choice++;

    //combo box should always have a drop down box but just to be on the safe side...
    if(dropDownBox)
        dropDownBox->insertChoice(s,index);
}

/****************************************************************************\
tComboBox::deleteChoice

Removes the choice at the specified index and pushes the subsequent choices up
to fill the gap.  The function decrements choiceCount and pushes the current
choice up one if necessary.  It then repeats the operation on the drop-down
box.

Inputs:
    index           Index of the choice to delete (0 <= index < choiceCount)
Outputs:
    none
\****************************************************************************/
void tComboBox::deleteChoice(int index)
{
#ifdef DEBUG
    if(index < 0 || index >= choiceCount)
    {
        bug("tComboBox::deleteChoice: invalid index %d",index);
        return;
    }
#endif
    choices.erase(choices.begin() + index);
    choiceCount = choices.size();

    //If the old choice was highlighted, then don't highlight any choices.
    if(choice == index)
        choice = -1;
    else if(choice > index)
        choice--;
    if(choice >= choiceCount)
        choice = choiceCount-1;

    //combo box should always have a drop down box but just to be on the safe side...
    if(dropDownBox)
        dropDownBox->deleteChoice(index);
}

/****************************************************************************\
tComboBox::tComboBox

Creates and initializes an instance of tComboBox.  The constructor bases the
height of the control on the font height.  It automatically creates a drop
down box that appears beneath the combo box with the specified height.  The
drop-down's width (excluding the scroll bars) matches the combo's width.

                        Layout of Combo Box:

                    | |< Margin        Margin >| |
           Margin { +----------------------------+     ---
                    | +------------------------+ |      |
                    | | Apples              \/ | |     Height
           Margin { +-+------------------------+-+---+  |   ---
                    +-+------------------------+-+ S | ---   |
                    | |#Apples#################| | c |       |
                    | | Oranges                | | r |      dropDownHeight
                    | | Bananas                | | o |       |
                    | | Grapes                 | | l |       |
           Margin { | +------------------------+ | l |       |
                    +----------------------------+---+      ---
                    |         Scroll Bar         |
                    +----------------------------+
                    |<--         Width        -->|
Inputs:
    controlX, controlY      Coordinates of control's lower-left corner
                                (ignoring drop-down box)
    width                   Control's width
    canFocus                Can the control receive focus?
    value                   New-line-delimited series of choices.  Ex:
                                "Hello\nWorld" creates two choices,
                                "Hello" and "World".
    _choice                 Index of highlighted control.  Specify -1 for no
                                initial selection.
    dropDownVertScrollBar   Will the drop-down box have a vertical scroll bar?
    dropDownHorScrollBar    Will the drop-down box have a horizontal scroll bar?
    dropDownHeight          Drop-down box's height (excluding scroll bars)
    fontIndex               Font with which to render the choices

\****************************************************************************/
tComboBox::tComboBox(int controlX, int controlY, int width, bool canFocus,
    const tString& value, int _choice, bool dropDownVertScrollBar,
    bool dropDownHorScrollBar, int dropDownHeight, int fontIndex) :
    tStdScrollWindow(controlX, controlY, width, tStdScrollWindow::MARGIN*2 +
    fontHeight(fontIndex), canFocus, 0, 0, width, fontHeight(fontIndex),
    false, false, fontIndex), choice(_choice)
{
    type = CTRL_COMBO_BOX;

    //Load a new-line-delimited series of choices.
    loadStringList(choices, CHARPTR(value));

    //Initialize choice count.
    choiceCount = choices.size();

    //Ensure that initial choice is valid.
    if(choice<-1)
        choice=-1;
    if(choice>=choiceCount)
        choice=choiceCount-1;

    //Create a drop-down box and associate it with this control.
    dropDownBox = mwNew tDropDown(controlX, controlY-dropDownHeight + MARGIN,
        width, dropDownHeight, false, value, choice, dropDownVertScrollBar,
        dropDownHorScrollBar, fontIndex);
    dropDownBox->setParent(this);
}

/****************************************************************************\
tComboBox::~tComboBox

Automatically deletes the drop-down box when the control is deleted.
\****************************************************************************/
tComboBox::~tComboBox()
{
    if (dropDownBox)
        mwDelete dropDownBox;
}

/****************************************************************************\
tComboBox::drawDropDownBox

Draws the drop-down box separately since it should overlap other controls.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tComboBox::drawDropDownBox()
{
    //Combo box should always have a drop-down box but just to be on the safe side...
    if(dropDownBox)
        dropDownBox->draw();
}

/****************************************************************************\
tComboBox::onDisable

Disables self and hides drop-down box.
\****************************************************************************/
void tComboBox::onDisable()
{
    tStdScrollWindow::onDisable();
    hideDropDownBox(dropDownBox);
}

/****************************************************************************\
tComboBox::onSetSize

Resizes self and drop-down box.  NOTE: Only the width has any effect on the
control's dimensions.
\****************************************************************************/
void tComboBox::onSetSize(int width, int height)
{
    //A combo box has a fixed height determined by MARGIN and font height.
    tStdScrollWindow::onSetSize(width, tStdTextBox::MARGIN*2 + fontHeight(getFontIndex()));

    //Combo box should always have a drop-down box but just to be on the safe side...
    //NOTE: The drop-down box retains its former height regardless of the height
    //argument.
    if(dropDownBox)
        dropDownBox->onSetSize(width, dropDownBox->getHeight());
}

/****************************************************************************\
tComboBox::onSetPosition

Repositions self and drop-down box.  The drop-down box remains in the same
location relative to the combo box.
\****************************************************************************/
void tComboBox::onSetPosition(int x, int y)
{
    tStdScrollWindow::onSetPosition(x,y);
    if(dropDownBox)
        dropDownBox->onSetPosition(x, y-dropDownBox->getHeight()+MARGIN);
}

/****************************************************************************\
tComboBox::setDropDownHeight

Explicitly sets the drop-down box's height from outside the class.

Inputs:
    dropDownHeight      New height of drop-down box
Outputs:
    none
\****************************************************************************/
void tComboBox::setDropDownHeight(int dropDownHeight)
{
    //Combo box should always have a drop-down box but just to be on the safe side...
    if(dropDownBox)
    {
        //Move the drop-down box 'dropDownHeight' pixels beneath combo box and
        //resize it.  NOTE: calling setPosition/setSize instead of
        //onSetPosition/onSetSize below will cause an error.
        dropDownBox->onSetPosition(getControlX(), getControlY()-dropDownHeight + MARGIN);
        dropDownBox->onSetSize(getWidth(), dropDownHeight);
    }
}

/****************************************************************************\
tComboBox::hideDropDownBox

tDropDown::hide links to this function.  The function takes no action besides
calling the drop-down box's onHide event.

Inputs:
    d           Child that called the function; always equals dropDownBox
Outputs:
    none
\****************************************************************************/
void tComboBox::hideDropDownBox(tDropDown *d)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(d))
        return;
#endif
    d->onHide();
}

/****************************************************************************\
tComboBox::refresh

Refreshes self and drop-down box.
\****************************************************************************/
void tComboBox::refresh()
{
    //Combo box should always have a drop-down box but just to be on the safe side...
    if(dropDownBox)
        dropDownBox->refresh();
}

/****************************************************************************\
tComboBox::getChoiceString

Retrieves the value of the highlighted choice.

Inputs:
    none
Outputs:
    Returns EMPTY_STRING if no choice is highlighted
    Otherwise, returns a reference to the text of the highlighted choice
\****************************************************************************/
const tString& tComboBox::getChoiceString()
{
    if(choice>=0&&choice<choiceCount)
        return choices[choice];
    else
        return EMPTY_STRING;
}

/****************************************************************************\
tComboBox::activated

Determines if the user has moved the mouse over the control or its child.
\****************************************************************************/
bool tComboBox::activated(int x, int y)
{
    return objectActivated(x,y) ||
        (dropDownBox->isVisible() && dropDownBox->activated(x,y));
}
/****************************************************************************\
tComboBox::getMousePointer

Relinquishes control of the mouse pointer to the drop down box if the drop down
box is selected.
\****************************************************************************/
bool tComboBox::getMousePointer(int x, int y, ePointer& p, int& tag)
{
    //Ancestors have precedence over descendents.
    if(tStdScrollWindow::getMousePointer(x,y,p,tag))
        return true;
    if(dropDownBox && dropDownBox->activated(x,y))
    {
        dropDownBox->getMousePointer(x,y,p,tag);
        return true;
    }
    return false;
}



/****************************************************************************\
tWindow::createControl

Associates a control with this window.  Create the control first using 'new'.
Specify the control's position -relative- to this window, as the function will
adjust it accordingly.  Then, pass the control's pointer to this function.
This function will record the pointer and set the control's parent.

Inputs:
    control         Non-null pointer to a newly-created control
Outputs:
    none
\****************************************************************************/
void tWindow::createControl(tControl *control)
{
    int x=getControlX(),y=getControlY();
#ifdef DEBUG
    if(MEMORY_VIOLATION(control))
        return;
#endif
    control->setParent(this);
    //Adjust control's position according to location of window:
    if(control->getHorAnchor()==ANCHOR_CENTER)
        x+=(getWidth()-control->getWidth())/2;
    else
        x+=control->getControlX();
    if(control->getVertAnchor()==ANCHOR_CENTER)
        y+=(getHeight()-control->getHeight())/2;
    else
        y+=control->getControlY();
    control->onSetPosition(x,y);
    controls.push_back(control);
}

/****************************************************************************\
tWindow::destroyControl

Removes all references to a control and prepares to a delete it.  At one
point, I deleted the control immediately, but this left dangling pointers on
the stack, which caused me immense grief.  Instead, I call the control's
onDestroy event, which temporarily hides it until it is actually deleted
during the next pass.

Inputs:
    control         Non-null pointer to a control belonging to this window
Outputs:
    none
\****************************************************************************/
void tWindow::destroyControl(tControl *control)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(control))
        return;
#endif
    if (selectedControl == control)
        selectedControl = NULL;
    if (focusedControl == control)
        focusedControl = NULL;
    if (defaultControl == control)
        defaultControl = NULL;
    if (initialControl == control)
        initialControl = NULL;
    if (oldFocusedControl == control)
        oldFocusedControl = NULL;
    control->onDestroy();
}

/****************************************************************************\
tWindow::draw

Draws the window in two stages if it is visible.  First, the function draws
the window's background, margins, and title bar.  Then, it draws the window's
controls.  NOTE: drawWindow is virtual, but drawControls is not.  If you wish
to add special effects to the window, then add additional instructions to
drawWindow.
\****************************************************************************/
void tWindow::draw()
{
    if(!isVisible())
        return;
    drawWindow();
    drawControls();
}

/****************************************************************************\
tWindow::drawControls

Draws the controls belonging to this window in three stages.  Stage 1: We
draw ordinary controls.  Stage 2: We draw visible drop-down boxes belonging
to combo boxes.  Stage 3: We draw visible hints.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tWindow::drawControls()
{
    int i;
    for(i=0; i<controls.size(); i++)
        controls[i]->draw();
    for(i=0; i<controls.size(); i++)
        if(controls[i]->isType(CTRL_COMBO_BOX))
            ((tComboBox *) controls[i])->drawDropDownBox();
    for(i=0; i<controls.size(); i++)
        if(controls[i]->isHintVisible())
            controls[i]->drawHint();
}

/****************************************************************************\
tWindow::onDefocus

This event occurs when the window becomes inactive (grayed).  The function
defocuses the focused control (if one exists), but does not deselect the
selected control.  It also does not reset oldFocusedControl as we want this
control to become focused when the window becomes active again.
\****************************************************************************/
void tWindow::onDefocus()
{
    tControl::onDefocus();
    if(focusedControl)
    {
        focusedControl->onDefocus();
        focusedControl=NULL;
    }
}

/****************************************************************************\
tWindow::onFocus

This event occurs when the window becomes active (highlighted).  The function
focuses 'oldFocusedControl' (the control that was focused before the window
became inactive).
\****************************************************************************/
void tWindow::onFocus()
{
    tControl::onFocus();
    if(!focusedControl)
    {
        focusedControl=oldFocusedControl;
        if(focusedControl)
            focusedControl->onFocus();
    }
}

/****************************************************************************\
tWindow::onSelect

Determines which control is selected when the user moves the mouse over this
window.
\****************************************************************************/
void tWindow::onSelect(int x, int y)
{
    tControl::onSelect(x,y);
    adjustSelection(x,y,false);
}

/****************************************************************************\
tWindow::onDeselect

Cancels the selection when the user moves the mouse away from the window.
\****************************************************************************/
void tWindow::onDeselect()
{
    tControl::onDeselect();
    if(selectedControl)
    {
        selectedControl->onDeselect();
        selectedControl=NULL;
    }
}

/****************************************************************************\
tWindow::onShow

Focuses the initial control when the window becomes visible if it meets
four criteria: (1) It exists; (2) It is visible; (3) It is enabled; and (4)
It can be focused.
\****************************************************************************/
void tWindow::onShow()
{
    if(!isVisible())
    {
        tControl::onShow();
        if(initialControl&&initialControl->isVisible()&&!initialControl->isDisabled()&&initialControl->canFocus())
        {
            oldFocusedControl=focusedControl=initialControl;
            initialControl->onFocus();
        }
    }
}

/****************************************************************************\
tWindow::~tWindow

Deselects the selected control (if one exists).  Defocuses the focused control
(if one exists).  Deletes all controls and clears the controls vector.
NOTE: The controls will not receive an onDestroy event in this case.
\****************************************************************************/
tWindow::~tWindow()
{
    int i;
    if(selectedControl)
    {
        selectedControl->onDeselect();
        selectedControl = NULL;
    }
    if(focusedControl)
    {
        focusedControl->onDefocus();
        focusedControl = NULL;
    }
    for(i=0; i<controls.size(); i++)
        mwDelete controls[i];
    controls.clear();
}

/****************************************************************************\
tWindow::onKeyDown

Dispatches the event to the focused control, except in the following cases.
When the user triggers the default button, the function focuses and executes
it.  When the user presses TAB, the function focuses the next eligible
control in the window.  When the user presses SHIFT+TAB, the function focuses
the previous eligible control in the window.
\****************************************************************************/
void tWindow::onKeyDown(tKey virtualKey, char ch)
{
    tControl::onKeyDown(virtualKey,ch);
    int i,j;

    /* The user can trigger the default control by pressing ENTER under the
     * following conditions:
     *  (1) A default control exists.
     *  (2) The default control is visible.
     *  (3) The default control is enabled.
     *  (4) The window's selection is -not- locked.  The window's selection will
     *      become locked when the user clicks on a combo box revealing its
     *      drop down box.  In this case, we want to pass the ENTER keystroke
     *      to the combo box instead of executing the default control.
     *  (5) No control has focus
     *         -OR-
     *      The user presses ENTER in a non-button control which does not
     *      explicitly support ENTER.  All controls meet this criteria
     *      except for buttons and multi-line text boxes.
     */
    if (virtualKey == GLFW_KEY_ENTER && defaultControl && defaultControl->isVisible() &&
        !defaultControl->isDisabled() && !isLockSelect() &&
        (focusedControl==NULL || (!focusedControl->isType(CTRL_TEXT_BOX)||
        focusedControl->isType(CTRL_EDIT)) &&
        !focusedControl->isType(CTRL_BUTTON)))
    {
        //Defocus the formerly focused control only if the default control can
        //receive focus.
        if(defaultControl->canFocus())
        {
            //Defocus the formerly focused control.
            if(focusedControl)
                focusedControl->onDefocus();
            oldFocusedControl=focusedControl=(tControl *) defaultControl;
            //Focus the default control.
            focusedControl->onFocus();
        }
        //Execute the default control.
        defaultControl->execute();
        return;
    }

    //If no control has focus, then we discard the keystroke.
    if (focusedControl==NULL)
        return;

    //Check for TAB and SHIFT+TAB.
    if (virtualKey == GLFW_KEY_TAB)
    {
        //Find the index of the focused control.
        for(i=0;i<controls.size();i++)
            if(controls[i]==focusedControl)
                break;
        //Safe-guard against dangling pointers:
        if(i==controls.size())
        {
            bug("tWindow::onKeyDown: focused control not found in controls vector");
            return;
        }

        //SHIFT+TAB:
        //Move to the previous control in the container which is focusable,
        //visible, and enabled.
        if (isShiftPressed())
        {
            //First, search to the left of the control in the list from right to left.
            for(j=i-1;j>=0;j--)
                if (controls[j]->canFocus() && controls[j]->isVisible() && !controls[j]->isDisabled())
                    break;
            //If we did not find a suitable control, then search to the right of
            //the control from right to left.
            if (j<0)
                for(j=controls.size()-1;j>i;j--)
                    if (controls[j]->canFocus() && controls[j]->isVisible() && !controls[j]->isDisabled())
                        break;
            //If we ended up where we started, then abort.
            if (j==i)
                return;
        }
        //TAB:
        //Move to the next control in the container which is focusable,
        //visible, and enabled.
        else
        {
            //First, search to the right of the control in the list from left to right.
            for(j=i+1;j<controls.size();j++)
                if (controls[j]->canFocus() && controls[j]->isVisible() && !controls[j]->isDisabled())
                    break;
            //If we did not find a suitable control, then search to the left of
            //the control from left to right.
            if (j==controls.size())
                for(j=0;j<i;j++)
                    if (controls[j]->canFocus() && controls[j]->isVisible() && !controls[j]->isDisabled())
                        break;
            //If we ended up where we started, then abort.
            if (j==i)
                return;
        }

        //Defocus the old control and focus the new control:
        focusedControl->onDefocus();
        oldFocusedControl=focusedControl=controls[j];
        focusedControl->onFocus();
        return;
    }

    //Otherwise, dispatch the event to the control's keystroke handler:
    focusedControl->onKeyDown(virtualKey,ch);
}

/****************************************************************************\
tWindow::onKeyUp

Dispatches the event to the focused control.
\****************************************************************************/
void tWindow::onKeyUp(tKey virtualKey, char ch)
{
    tControl::onKeyUp(virtualKey,ch);

    //Ignore the keystroke if no control has focus.
    if (focusedControl==NULL)
        return;

    //Dispatch the event to the focused control.
    focusedControl->onKeyUp(virtualKey,ch);
}

/****************************************************************************\
tWindow::adjustSelection

Selects a new control based on the position of the mouse.  When the window
is not selected or visible, the function clears the selection.  Otherwise,
it adjusts the selection only if the selected control is unlocked or you
specify overrideLock.

Inputs:
    x, y            Current position of the mouse
    overrideLock    Will the function ignore the selected control's lock-select
                        mode?  Specify 'true' only for onMouseDown events.
Outputs:
    none
\****************************************************************************/
void tWindow::adjustSelection(int x, int y, bool overrideLock)
{
    int i;
    tControl *c=NULL;

    //When the window is not selected or visible, no controls should be selected.
    if(!isSelected()||!isVisible())
    {
        if(selectedControl != NULL)
        {
            selectedControl->onDeselect();
            selectedControl=NULL;
        }
        return;
    }

    /* We wish to adjust the selection under the following conditions:
     *  (1) No control is selected.
     *      -OR-
     *  (2) The user has moved the mouse away from the control whose selection
     *      is not locked.
     *      -OR-
     *  (3) The user has moved the mouse away from a control whose selection
     *      is locked -AND- overrideLock=true.
     */
    if (selectedControl == NULL ||
        !selectedControl->activated(x,y) && (overrideLock || !isLockSelect()))
    {
        //Determine which control is activated:
        //Examine only visible and enabled controls.
        for(i=0;i<controls.size();i++)
            if (controls[i]->isVisible() &&
                !controls[i]->isDisabled() && controls[i]->activated(x,y))
            {
                c=controls[i];
                break;
            }

        //If the new selection does not match the former selection, then deselect
        //the old control and select the new one.
        if (c != selectedControl)
        {
            if(selectedControl != NULL)
                selectedControl->onDeselect();
            selectedControl = c;
            if(selectedControl != NULL)
                selectedControl->onSelect(x,y);
        }
    }
}

/****************************************************************************\
tWindow::onMouseDown

Adjusts the selection to reflect the current control beneath the mouse pointer.
Focuses the selected control if it can receive focus.  Dispatches the event to
the selected control.
\****************************************************************************/
void tWindow::onMouseDown(int x, int y, eMouseButton button)
{
    tControl::onMouseDown(x,y,button);

    //Determine which control the mouse is over:
    adjustSelection(x,y,/* overrideLock = */ true);

    /* We want to focus the selected control under the following conditions:
     * (Notice that selectedControl now references the control beneath the
     * mouse pointer.)
     *  (1) The selectedControl exists (meaning there is a control beneath the
     *      mouse pointer.^
     *  (2) The selectedControl does not have focus already.
     *  (3) The selectedControl can receive focus.
     *
     * ^This condition prevents the user from clearing the focus by clicking in
     *  a blank area of the window.
     */
    if (selectedControl && selectedControl!=focusedControl && selectedControl->canFocus())
    {
        if (focusedControl)
        {
            focusedControl->onDefocus();
            oldFocusedControl=focusedControl=NULL;
        }
        oldFocusedControl=focusedControl=selectedControl;
        focusedControl->onFocus();
    }

    //If a control has focus, then dispatch the event to it.
    if (selectedControl)
        selectedControl->onMouseDown(x,y,button);
}

/****************************************************************************\
tWindow::onMouseMove

Adjusts the selection if the user is not pressing a mouse button.  Dispatches
the event to the selected control.
\****************************************************************************/
void tWindow::onMouseMove(int x, int y)
{
    tControl::onMouseMove(x,y);

    //Don't adjust the selection when the user drags the mouse.  The selected
    //control remains selected as long as the user holds at least one mouse
    //button down.
    if(isMousePressed())
    {
        if(selectedControl)
            selectedControl->onMouseMove(x,y);
    }
    else
    {
        //Determine which control the mouse is over:
        adjustSelection(x,y, /* overrideLock = */ false);

        //If a control is selected, then dispatch the event to that control:
        if (selectedControl)
            selectedControl->onMouseMove(x,y);
    }
}

/****************************************************************************\
tWindow::onMouseUp

Dispatches the event to the selected control.
\****************************************************************************/
void tWindow::onMouseUp(int x, int y, eMouseButton button)
{
    tControl::onMouseUp(x,y,button);

    if(selectedControl != NULL)
        selectedControl->onMouseUp(x,y,button);

    //We cannot assume selectedControl still references the same control here.
    //The user may have moved the mouse away from the control and released the
    //mouse button simulataneously.
    adjustSelection(x,y, /* overrideLock = */ false);
}

//This function serves the same purpose as onMouseUp et al:
void tWindow::onMouseWheel(int pos,eMouseWheel direction)
{
    tControl::onMouseWheel(pos,direction);
    if(selectedControl != NULL)
        selectedControl->onMouseWheel(pos,direction);
}

/****************************************************************************\
tWindow::poll

Deletes and removes controls whose 'destroyed' flag is set.  Polls the remaining
controls.
\****************************************************************************/
void tWindow::poll()
{
    int i;
    for(i=0; i<controls.size(); )
    {
        if(controls[i]->isDestroyed())
        {
            mwDelete controls[i];
            controls.erase(controls.begin()+i);
        }
        else
            i++;
    }
    for(i=0;i<controls.size();i++)
        controls[i]->poll();
}

/****************************************************************************\
tWindow::refresh

Refreshes each control.
\****************************************************************************/
void tWindow::refresh()
{
    int i;
    for(i=0;i<controls.size();i++)
        controls[i]->refresh();
}

/****************************************************************************\
tWindow::disableControl

Generates an onDisable event for the specified control.  Ensures that
selectedControl, focusedControl, and oldFocusedControl no longer reference
it.

Inputs:
    c           Non-null pointer to a descendent of tControl
Outputs:
    none
\****************************************************************************/
void tWindow::disableControl(tControl *c)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(c))
        return;
#endif
    c->onDisable();
    if(selectedControl==c)
        selectedControl=NULL;
    if(focusedControl==c)
        oldFocusedControl=focusedControl=NULL;
}

/****************************************************************************\
tWindow::enableControl

Generates an onEnable event for the specified control.

Inputs:
    c           Non-null pointer to a descendent of tControl
Outputs:
    none
\****************************************************************************/
void tWindow::enableControl(tControl *c)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(c))
        return;
#endif
    c->onEnable();

    //The control may appear beneath the mouse pointer so adjust that
    //selection if the window is selected and visible.
    //Added 12/29/02: We don't want to adjust the selection if the mouse is depressed.
    if(isSelected() && isVisible() && !isMousePressed())
        adjustSelection(saveMouseX,saveMouseY,false);
}

/****************************************************************************\
tWindow::hideControl

Generates an onHide event for the specified control.  Ensures that
selectedControl, focusedControl, and oldFocusedControl no longer reference
it.

Inputs:
    c           Non-null pointer to a descendent of tControl
Outputs:
    none
\****************************************************************************/
void tWindow::hideControl(tControl *c)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(c))
        return;
#endif
    c->onHide();
    if(selectedControl==c)
        selectedControl=NULL;
    if(focusedControl==c)
        oldFocusedControl=focusedControl=NULL;
}

/****************************************************************************\
tWindow::showControl

Generates an onShow event for the specified control.  Focuses the control if it
matches oldFocusedControl.  Adjusts the selection.

Inputs:
    c           Non-null pointer to a descendent of tControl
Outputs:
    none
\****************************************************************************/
void tWindow::showControl(tControl *c)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(c))
        return;
#endif
    //Show the control only if it was formerly invisible:
    if(!c->isVisible())
    {
        c->onShow();

        //When the user minimizes and restores a dialog box, we want to focus
        //the oldFocused control.  NOTE: 'restore' will call showControl for
        //every visible control in the dialog box.
        if(oldFocusedControl==c&&isFocused()&&isVisible())
        {
            focusedControl=c;
            focusedControl->onFocus();
        }

        //The control may appear beneath the mouse pointer so adjust that
        //selection if the window is selected and visible.
        //Added 12/29/02: We don't want to adjust the selection if the mouse is depressed.
        if(isSelected() && isVisible() && !isMousePressed())
            adjustSelection(saveMouseX,saveMouseY,false);
    }
}

/****************************************************************************\
tWindow::setControlPosition

Generates an onSetPosition event for the specified control.

Inputs:
    c           Non-null pointer to a descendent of tControl
Outputs:
    none
\****************************************************************************/
void tWindow::setControlPosition(tControl *c, int x, int y)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(c))
        return;
#endif
    c->onSetPosition(x,y);

    //We do not adjust the selection here to avoid problems with dialog boxes
    //when moving and repositioning.
}

/****************************************************************************\
tWindow::setControlSize

Generates an onSetSize event for the specified control.

Inputs:
    c           Non-null pointer to a descendent of tControl
Outputs:
    none
\****************************************************************************/
void tWindow::setControlSize(tControl *c, int w, int h)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(c))
        return;
#endif
    c->onSetSize(w,h);

    //We do not adjust the selection here to avoid problems with dialog boxes
    //when moving and repositioning.
}

/****************************************************************************\
tWindow::focusControl

Focuses a visible, enabled, and focusable control.  The function generates an
onDefocus event for the old control and an onFocus event for the new control.
The focus will not take effect until the window becomes visible and focused.

Inputs:
    c           Non-null pointer to a descendent of tControl
Outputs:
    none
\****************************************************************************/
void tWindow::focusControl(tControl *c)
{
    if(focusedControl)
    {
        focusedControl->onDefocus();
        focusedControl=NULL;
    }
#ifdef DEBUG
    if(MEMORY_VIOLATION(c))
        return;
    if(!c->isVisible())
    {
        bug("tWindow::focusControl: control is not visible");
        return;
    }
    if(c->isDisabled())
    {
        bug("tWindow::focusControl: control is disabled");
        return;
    }
    if(!c->canFocus())
    {
        bug("tWindow::focusControl: control cannot be focused");
        return;
    }
#endif
    oldFocusedControl=c;
    if(isVisible()&&isFocused())
    {
        focusedControl=c;
        focusedControl->onFocus();
    }
}

/****************************************************************************\
tWindow::defocusControl

Generates an onDefocus event for the specified control and ensures that
focusedControl and oldFocusedControl no longer reference it.

Inputs:
    c           Non-null pointer to a descendent of tControl
Outputs:
    none
\****************************************************************************/
void tWindow::defocusControl(tControl *c)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(c))
        return;
    if(oldFocusedControl!=c && focusedControl!=c)
    {
        bug("tWindow::defocusControl: control is not focused");
        return;
    }
#endif
    if(focusedControl)
        focusedControl->onDefocus();
    oldFocusedControl=focusedControl=NULL;
}

/****************************************************************************\
tWindow::deselectControl

Generates an onDeselect event for the selected control and clears
selectedControl.  We added this function specifically for tDialogBox.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tWindow::deselectControl()
{
    if(selectedControl)
    {
        selectedControl->onDeselect();
        selectedControl=NULL;
    }
}

/****************************************************************************\
tWindow::onSetPosition

Generates an onSetPosition event for each control.  The controls remain in the
same position relative to the window.  We accomplish this by adding an offset
to each control's position based on the window's former position and the
window's new position.
\****************************************************************************/
void tWindow::onSetPosition(int x, int y)
{
    //Calculate the amount to add to each control's position.  NOTE: These
    //statements must be above the call to 'tControl::onSetPosition'.
    int fx=x-getControlX();
    int fy=y-getControlY();
    tControl::onSetPosition(x,y);
    for(int i=0; i<controls.size(); i++)
        controls[i]->onSetPosition(controls[i]->getControlX()+fx, controls[i]->getControlY()+fy);
}

/****************************************************************************\
tWindow::onSetSize

Moves/resizes the controls based on their anchors.
\****************************************************************************/
void tWindow::onSetSize(int w, int h)
{
    //NOTE: These statements must be above the call to 'tControl::onSetSize'.
    int cx,cy,cw,ch;
    bool needMove,needResize;
    int fw=w-getWidth();
    int fh=h-getHeight();
    tControl::onSetSize(w,h);
    for(int i=0; i<controls.size(); i++)
    {
        cx=controls[i]->getControlX();
        cy=controls[i]->getControlY();
        cw=controls[i]->getWidth();
        ch=controls[i]->getHeight();
        needMove=false;
        needResize=false;
        switch(controls[i]->getHorAnchor())
        {
            case ANCHOR_RIGHT:
                cx+=fw;
                needMove=true;
                break;
            case ANCHOR_BOTH:
                cw+=fw;
                needResize=true;
                break;
            case ANCHOR_CENTER:
                cx=getControlX()+(getWidth()-controls[i]->getWidth())/2;
                needMove=true;
                break;
        }
        switch(controls[i]->getVertAnchor())
        {
            case ANCHOR_TOP:
                cy+=fh;
                needMove=true;
                break;
            case ANCHOR_BOTH:
                ch+=fh;
                needResize=true;
                break;
            case ANCHOR_CENTER:
                cy=getControlY()+(getHeight()-controls[i]->getHeight())/2;
                needMove=true;
                break;
        }
        if(needMove)
            controls[i]->onSetPosition(cx,cy);
        if(needResize)
            controls[i]->onSetSize(cw,ch);
    }
}

/****************************************************************************\
tWindow::controlLookup

Searches for the first control with a matching tag.  This function enables us
to label important controls with unique integer identifiers and access them
later.

Inputs:
    tag             Tag to look for
Outputs:
    Returns the address of the first matching control or NULL if no matches
        were found
\****************************************************************************/
tControl *tWindow::controlLookup(int tag)
{
    int i;
    for(i=0;i<controls.size();i++)
        if(controls[i]->tag==tag&&!controls[i]->isDestroyed())
            return controls[i];
    return NULL;
}

/****************************************************************************\
tWindow::close

Closes the window.  The close action depends on the destroyOnClose flag.  When
destroyOnClose is set, the window is deleted (during the next pass).  When
destroyOnClose is cleared, the window box is simply hidden.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tWindow::close()
{
    if(destroyOnClose)
        destroyWindow(this);
    else
        hideWindow(this);
}

/****************************************************************************\
tWindow::onDisable
tWindow::onEnable

This class does not support these two events, so we disable them.
\****************************************************************************/
void tWindow::onDisable()
{
    bug("tWindow::onDisable: this control doesn't support disable");
}
void tWindow::onEnable()
{
    bug("tWindow::onEnable: this control doesn't support enable");
}

/****************************************************************************\
tWindow::setDefault

Sets the default control.  The window executes the default control when the
user presses ENTER in certain focused controls (e.g. an edit control).  See
tWindow::onKeyDown for more info.  You may also reset the default control
by passing a NULL pointer to this function.

Inputs:
    d               Pointer to a descendent of tButton
                        -OR-
                    NULL if you want to reset the default control
Outputs:
    none
\****************************************************************************/
void tWindow::setDefault(tButton *d)
{
    defaultControl=d;
}

/****************************************************************************\
tWindow::setInitial

Sets the initial control.  The window focuses the initial control when the
window is created or shown.  Typically, you will call this function at the
bottom of a window's constructor.  You may also reset the initial control
by passing a NULL pointer to this function.

Inputs:
    i               Pointer to an existing control in this window
                        -OR-
                    NULL if you want to reset the initial control
Outputs:
    none
\****************************************************************************/
void tWindow::setInitial(tControl *i)
{
    initialControl=i;
    if(i)
        focusControl(i);
}

/****************************************************************************\
tWindow::getFirstControl

Provides access to the window's controls (since the controls container is
private).  Call this function to retrieve the first control in the window.
Then call getNextControl to retrieve the remaining controls.

Inputs:
    none
Outputs:
    Returns a pointer to the first control or NULL if the window does not have
        any controls
\****************************************************************************/
tControl* tWindow::getFirstControl()
{
    current=0;
    if(current<controls.size())
        return controls[current];
    else
        return NULL;
}

/****************************************************************************\
tWindow::getNextControl

Provides access to the window's controls (since the controls container is
private).  Call getFirstControl to retrieve the first control.  Then call
this function repeatedly to retrieve the remaining controls.  The function
will return NULL when you have reached the end.

Inputs:
    none
Outputs:
    Returns a pointer to the next control or NULL if you have reached the end
\****************************************************************************/
tControl* tWindow::getNextControl()
{
    current++;
    if(current<controls.size())
        return controls[current];
    else
        return NULL;
}

/****************************************************************************
 * The following functions "link" window actions to their global counterparts.
 * For instance, calling myWindow->show() will invoke showWindow, which will
 * update global state variables and call myWindow->onShow().  Notice the
 * strong similarity between tWindow and tControl.
 */
void tWindow::show()                    { showWindow(this); }
void tWindow::hide()                    { hideWindow(this); }
void tWindow::focus()                   { focusWindow(this); }
void tWindow::defocus()                 { defocusWindow(this); }
void tWindow::setPosition(int x, int y) { setWindowPosition(this,x,y); }
void tWindow::setSize(int w, int h)     { setWindowSize(this,w,h); }
void tWindow::destroy()                 { destroyWindow(this); }

//This class does not support enable/disable:
void tWindow::enable()                  { bug("tWindow::enable: this control doesn't support enable"); }
void tWindow::disable()                 { bug("tWindow::disable: this control doesn't support disable"); }

/****************************************************************************\
tWindow::tWindow

Creates and initializes a new instance of tWindow.  The class is ideally suited
for full-screen, static backgrounds, but does not support repositioning or
resizing.  Therefore, it serves primarily as a base class for tDialogBox.
A modal window remains above other windows and retains focus until the user
closes or hides it.  A background window always remains behind other windows.

Inputs:
    controlX, controlY          Coordinates of the control's lower-left corner
    width, height               Window's dimensions
    _modal                      Is this window modal?
    _background                 Is this a background window?
    _destroyOnClose             Defines the action to take when the user "closes"
                                    the window (usually by clicking the 'X').
                                    If true, the window will delete itself.
                                    If false, the window will hide itself.
\****************************************************************************/
tWindow::tWindow(int controlX, int controlY, int width, int height, bool _modal, bool _background,
    bool _destroyOnClose) : tControl(controlX,controlY,width,height,true), modal(_modal),
    background(_background), focusedControl(NULL), selectedControl(NULL), defaultControl(NULL),
    initialControl(NULL), oldFocusedControl(NULL), current(0), destroyOnClose(_destroyOnClose)
{
    type=CTRL_WINDOW;
}
/****************************************************************************\
tWindow::getMousePointer

Relinquishes control of the mouse pointer to a selected child.
\****************************************************************************/
bool tWindow::getMousePointer(int x, int y, ePointer& p, int& tag)
{
    //Ancestors have precedence over descendents.
    if(tControl::getMousePointer(x,y,p,tag))
        return true;
    if(selectedControl)
    {
        selectedControl->getMousePointer(x,y,p,tag);
        return true;
    }
    return false;
}



/****************************************************************************\
tDialogBox::titleBarActivated

Determines whether the user has moved the mouse over the title bar.  This
function defines the title bar as the rectangular region that is bounded by the
margin on the top, left, and right edges, and whose height equals titleBarHeight.
The user presses the mouse in this region to reposition the dialog box.
NOTE: You may override this function to place the title bar elsewhere.

Inputs:
    x,y         Current position of the mouse (absolute coordinates)
Outputs:
    Returns true if the mouse is over the title bar
\****************************************************************************/
bool tDialogBox::titleBarActivated(int x, int y)
{
    x-=getControlX();
    y-=getControlY();
    return x>=RESIZE_MARGIN && x<getWidth()-RESIZE_MARGIN &&
        y>=getHeight()-RESIZE_MARGIN-titleBarHeight && y<getHeight()-RESIZE_MARGIN;
}

/****************************************************************************\
tDialogBox::checkX1

Adjusts x1 such that the distance between x1 and x2 fits within the dialog box's
horizontal resize boundaries.  This function simplifies the calculations in
onMouseMove.

Inputs:
    x1              x-component of the box's left edge
    x2              x-component of the box's right edge
Outputs:
    none
\****************************************************************************/
void tDialogBox::checkX1(int& x1, int& x2)
{
    if(x1>x2-minWidth)
        x1=x2-minWidth;
    if(x1<x2-maxWidth)
        x1=x2-maxWidth;
}

/****************************************************************************\
tDialogBox::checkX2

Adjusts x2 such that the distance between x1 and x2 fits within the dialog box's
horizontal resize boundaries.  This function simplifies the calculations in
onMouseMove.

Inputs:
    x1              x-component of the box's left edge
    x2              x-component of the box's right edge
Outputs:
    none
\****************************************************************************/
void tDialogBox::checkX2(int& x1, int& x2)
{
    if(x2<x1+minWidth)
        x2=x1+minWidth;
    if(x2>x1+maxWidth)
        x2=x1+maxWidth;
}

/****************************************************************************\
tDialogBox::checkY1

Adjusts y1 such that the distance between y1 and y2 fits within the dialog box's
vertical resize boundaries.  This function simplifies the calculations in
onMouseMove.

Inputs:
    y1              y-component of the box's bottom edge
    y2              y-component of the box's top edge
Outputs:
    none
\****************************************************************************/
void tDialogBox::checkY1(int& y1, int& y2)
{
    if(y1>y2-minHeight)
        y1=y2-minHeight;
    if(y1<y2-maxHeight)
        y1=y2-maxHeight;
}

/****************************************************************************\
tDialogBox::checkY2

Adjusts y2 such that the distance between y1 and y2 fits within the dialog box's
vertical resize boundaries.  This function simplifies the calculations in
onMouseMove.

Inputs:
    y1              y-component of the box's bottom edge
    y2              y-component of the box's top edge
Outputs:
    none
\****************************************************************************/
void tDialogBox::checkY2(int& y1, int& y2)
{
    if(y2<y1+minHeight)
        y2=y1+minHeight;
    if(y2>y1+maxHeight)
        y2=y1+maxHeight;
}

/****************************************************************************\
tDialogBox::maximize

Call this function from outside the class to maximize the dialog box.  The
function takes no action if the dialog box cannot be maximized or it is already
maximized.  Otherwise, it function repositions/resizes the dialog box, shows
invisible controls (if the window was formerly minimized), and updates the
window buttons.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tDialogBox::maximize()
{
    int i=0;
    //Ensure that we can maximize the dialog box.  This check is necessary since
    //other sources besides the window buttons may call this function.
    if(maximizeable==false)
    {
        bug("tDialogBox::maximize: window cannot be maximized");
        return;
    }

    //No need to continue further if the dialog box is already maximized.
    if(state==WINDOW_MAXIMIZED)
        return;

    //If the dialog box was formerly normal, then store its current position
    //and dimensions.  We will use these to restore the dialog box in the future.
    if(state==WINDOW_NORMAL)
    {
        oldX=getControlX();
        oldY=getControlY();
        oldWidth=getWidth();
        oldHeight=getHeight();
    }

    state=WINDOW_MAXIMIZED;

    //Reposition and resize the dialog box:
    onMaximize();

    //Show any controls which were formerly hidden by 'minimize':
    for(tControl *c=getFirstControl();c;c=getNextControl())
        if(!c->isVisible()&&c->oldVisible)
            tWindow::showControl(c);

    //Refresh the window buttons.  The user will see a subset of the following
    //buttons from right to left: close, restore, minimize.
    if(closeable&&buttons[i])       //Visible only if the user can close the window
    {
        buttons[i]->setAction(WNDACT_CLOSE);
        showControl(buttons[i++]);
    }
    if(buttons[i])                  //Always visible
    {
        buttons[i]->setAction(WNDACT_RESTORE);
        showControl(buttons[i++]);
    }
    if(minimizeable&&buttons[i])    //Visible only if the user can minimize the window
    {
        buttons[i]->setAction(WNDACT_MINIMIZE);
        showControl(buttons[i++]);
    }
    for(;i<3;i++)                   //Hide the remaining buttons.
        hideControl(buttons[i]);
}

/****************************************************************************\
tDialogBox::onMaximize

Adjusts the dialog box's position and size in response to a "maximize" event.
The dialog box overhangs all four edges of the screen by an amount equal to the
margin, effectively hiding the dialog box's beveling.  Descendents may override
this function to position the dialog box elsewhere.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tDialogBox::onMaximize()
{
    setWindowPosition(this, -margin,-margin);
    setWindowSize(this, getScreenWidth()+margin*2,getScreenHeight()+margin*2);
}

/****************************************************************************\
tDialogBox::minimize

Call this function from outside the class to minimize the dialog box.  The
function takes no action if (1) the window cannot be minimized; or (2) the
window is already minimized.  Otherwise, it repositions the window, hides all
controls (excluding the window buttons), and updates the window buttons.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tDialogBox::minimize()
{
    int i=0;

    //Ensure that we can minimize the dialog box.  This check is necessary since
    //other sources besides the window buttons may call this function.
    if(minimizeable==false)
    {
        bug("tDialogBox::minimize: window cannot be minimized");
        return;
    }

    //No need to continue further if the dialog box is already minimized.
    if(state==WINDOW_MINIMIZED)
        return;

    //If the dialog box was formerly normal, then store its current position
    //and dimensions.  We will use these to restore the dialog box in the future.
    if(state==WINDOW_NORMAL)
    {
        oldX=getControlX();
        oldY=getControlY();
        oldWidth=getWidth();
        oldHeight=getHeight();
    }

    state=WINDOW_MINIMIZED;

    //Reposition and resize the dialog box:
    onMinimize();

    //Hide all controls except for the window buttons.  We store the control's
    //old visibility in oldVisible, so that when the user restores the dialog
    //box, we can restore the control's to their former states.
    for(tControl *c=getFirstControl();c;c=getNextControl())
        if(!c->isType(CTRL_WINDOW_BUTTON))  //Window buttons not affected by minimize.
        {
            c->oldVisible=c->isVisible();   //Store the old visibility.
            tWindow::hideControl(c);
        }

    //Refresh the window buttons:  The user will see a subset of the following
    //buttons from right to left: close, maximize, restore.
    if(closeable&&buttons[i])                   //Visible only if user can close window
    {
        buttons[i]->setAction(WNDACT_CLOSE);
        showControl(buttons[i++]);
    }
    if(maximizeable&&buttons[i])                //Visible only if user can maximize window
    {
        buttons[i]->setAction(WNDACT_MAXIMIZE);
        showControl(buttons[i++]);
    }
    if(buttons[i])                              //Always visible
    {
        buttons[i]->setAction(WNDACT_RESTORE);
        showControl(buttons[i++]);
    }
    for(;i<3;i++)                               //Hide the remaining buttons.
        hideControl(buttons[i]);
}

/****************************************************************************\
tDialogBox::onMinimize

Adjusts the window's position and size in response to a "minimize" event.  The
default implementation leaves the title bar in the same position, but hides the
window's contents.  Descendents may override this function to position the
window elsewhere.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tDialogBox::onMinimize()
{
    setWindowPosition(this, oldX, oldY+oldHeight-titleBarHeight-2*margin);
    setWindowSize(this, oldWidth, titleBarHeight+2*margin);
}

/****************************************************************************\
tDialogBox::restore

Call this function from outside the class to restore the dialog box to its
former position before maximizing or minimizing.  The function takes no action
if the window is already "normal".  Otherwise, it repositions the window,
shows invisible controls (if the window was formerly minimized), and updates
the window buttons.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tDialogBox::restore()
{
    int i=0;

    //No need to continue further if the dialog box is already normal.
    if(state==WINDOW_NORMAL)
        return;

    state=WINDOW_NORMAL;

    //Restore the dialog box to its former size and position:
    onRestore();

    //Restore the controls to their former visibility (if the dialog box was
    //previously minimized.)
    for(tControl *c=getFirstControl();c;c=getNextControl())
        if(!c->isVisible()&&c->oldVisible)
            tWindow::showControl(c);

    //Refresh the window buttons:  The user will see a subset of the following
    //buttons from right to left: close, maximize, minimize.
    if(closeable&&buttons[i])                   //Visible only if user can close window
    {
        buttons[i]->setAction(WNDACT_CLOSE);
        showControl(buttons[i++]);
    }
    if(maximizeable&&buttons[i])                //Visible only if user can maximize window
    {
        buttons[i]->setAction(WNDACT_MAXIMIZE);
        showControl(buttons[i++]);
    }
    if(minimizeable&&buttons[i])                //Visible only if user can minimize window
    {
        buttons[i]->setAction(WNDACT_MINIMIZE);
        showControl(buttons[i++]);
    }
    for(;i<3;i++)                               //Hide the remaining buttons.
        hideControl(buttons[i]);
}

/****************************************************************************\
tDialogBox::onRestore

Adjusts the window's position and size in response to a "restore" event.  The
default implementation restores the window to its previous position before
minimizing or maximizing.  Descendents may override this function to position
the window elsewhere.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void tDialogBox::onRestore()
{
    setWindowPosition(this, oldX, oldY);
    setWindowSize(this, oldWidth, oldHeight);
}

/****************************************************************************\
tDialogBox::getMousePointer

Changes the shape of the mouse pointer when the user moves the mouse over the
edges of the dialog box.  The function sets p and tag depending on resizeDir.
\****************************************************************************/
bool tDialogBox::getMousePointer(int x, int y, ePointer& p, int& tag)
{
    //Ancestors have precedence over descendents.
    if(tWindow::getMousePointer(x,y,p,tag))
        return true;
    if(titleBarActivated(x,y))
    {
        p=POINTER_STD;
        tag=0;
        return true;
    }
    if(resizeDir!=DIR_NONE)
    {
        p=POINTER_RESIZE;
        tag=(int)resizeDir;
        return true;
    }
    return false;
}

/****************************************************************************\
tDialogBox::adjustResize

Generates onResizeActivated and onResizeDeactivated events depending on the mouse
pointer's position.  Descendents should handle these events by changing the shape
of the mouse pointer.

Inputs:
    x, y            Absolute coordinates of the mouse pointer
Outputs:
    none
\****************************************************************************/
void tDialogBox::adjustResize(int x, int y)
{
    /* We show the resize pointer under the following conditions:
     *  (1) The window is selected (meaning the mouse is inside the window)
     *  (2) The window's selection is unlocked (e.g. the user has not activated
     *      a combo box)
     *  (3) The window can be resized (resizeable)
     *  (4) The window is neither maximized not minimized
     *  (5) The mouse is within the resize area defined by resizeActivated
     *  (6) The mouse is not over a control.  This prevents the resize pointer
     *      from appearing when a control overlaps the window's edge.
     */
    if(isSelected()&&!isLockSelect()&&resizeActivated(x,y)&& getSelectedControl()==NULL)
    {
        //Determine the direction in which to resize the dialog box:
        if(x-getControlX()<RESIZE_MARGIN)
        {
            if(y-getControlY()<RESIZE_MARGIN)
                resizeDir=DIR_DOWN_LEFT;
            else if(y-getControlY()<getHeight()-RESIZE_MARGIN)
                resizeDir=DIR_LEFT;
            else
                resizeDir=DIR_UP_LEFT;
        }
        else if(x-getControlX()<getWidth()-RESIZE_MARGIN)
        {
            if(y-getControlY()<RESIZE_MARGIN)
                resizeDir=DIR_DOWN;
            else
                resizeDir=DIR_UP;
        }
        else
        {
            if(y-getControlY()<RESIZE_MARGIN)
                resizeDir=DIR_DOWN_RIGHT;
            else if(y-getControlY()<getHeight()-RESIZE_MARGIN)
                resizeDir=DIR_RIGHT;
            else
                resizeDir=DIR_UP_RIGHT;
        }
    }
    else
        resizeDir=DIR_NONE;
}

/****************************************************************************\
tDialogBox::onMouseDown

Introduces support for repositioning and resizing to the window's onMouseDown
event handler.  The function determines whether the user has clicked on the
title bar or the window's edges.  If so, it sets a flag and initializes anchors.
Otherwise, it passes the event to the ancestor.
\***************************************************************************/
void tDialogBox::onMouseDown(int x, int y, eMouseButton button)
{
    //If the dialog box can be repositioned and the mouse if over the title bar
    //(but not over any controls), then begin repositioning it.
    if(moveable && state!=WINDOW_MAXIMIZED && titleBarActivated(x,y) && getSelectedControl()==NULL)
    {
        titleBarDepressed=true;

        //Place an anchor at the lower-left corner.  This allows us to quickly
        //calculate the window's new position by adding the mouse coordinates.
        anchorX=getControlX()-x;
        anchorY=getControlY()-y;

        return; //Don't pass event to ancestor.
    }

    //Change the mouse pointer to a resize pointer if necessary:
    adjustResize(x,y);

    //If the user clicks on the edge of the dialog box, then begin resizing:
    if(resizeActivated(x,y))
    {
        resizing=true;

        //Store all four anchors for convenience.  These allow us to quickly
        //calculate the x- or y-component of any side by adding the mouse
        //coordinates.
        anchorX1=getControlX()-x;
        anchorY1=getControlY()-y;
        anchorX2=getControlX()+getWidth()-x;
        anchorY2=getControlY()+getHeight()-y;

        return; //Don't pass event to ancestor.
    }

    //The dialog box's operations (movement and resizing) take precedence over
    //the window's operations (controls).  If the user is moving or resizing, we
    //don't want the window to handle the event
    tWindow::onMouseDown(x,y,button);
}

/****************************************************************************\
tDialogBox::onMouseMove

Introduces repositioning and resizing to the window's onMouseMove event handler.
The function adjusts the window's position and/or size if the user drags the
window's title bar or edges.  Otherwise, it updates the shape of the mouse
pointer and passes the event to the ancestor.
\****************************************************************************/
void tDialogBox::onMouseMove(int x, int y)
{
    //Is the user dragging the title bar?
    if(titleBarDepressed)
    {
        //Reposition the dialog box:
        onSetPosition(x+anchorX, y+anchorY);
    }

    //Is the user dragging the window's edges?
    if(resizing)
    {
        //Store the old dialog box's boundaries:
        int x1=getControlX(),y1=getControlY();
        int x2=getControlX()+getWidth(),y2=getControlY()+getHeight();

        //Adjust x1, x2, y1, y2 or a combination of two.  Ensure that the
        //window's new dimensions fit within the resize boundaries.
        switch(resizeDir)
        {
            case DIR_UP:
                y2=anchorY2+y;
                checkY2(y1,y2);
                break;
            case DIR_RIGHT:
                x2=anchorX2+x;
                checkX2(x1,x2);
                break;
            case DIR_UP_RIGHT:
                x2=anchorX2+x;
                checkX2(x1,x2);
                y2=anchorY2+y;
                checkY2(y1,y2);
                break;
            case DIR_DOWN:
                y1=anchorY1+y;
                checkY1(y1,y2);
                break;
            case DIR_LEFT:
                x1=anchorX1+x;
                checkX1(x1,x2);
                break;
            case DIR_DOWN_LEFT:
                x1=anchorX1+x;
                checkX1(x1,x2);
                y1=anchorY1+y;
                checkY1(y1,y2);
                break;
            case DIR_UP_LEFT:
                x1=anchorX1+x;
                checkX1(x1,x2);
                y2=anchorY2+y;
                checkY2(y1,y2);
                break;
            case DIR_DOWN_RIGHT:
                x2=anchorX2+x;
                checkX2(x1,x2);
                y1=anchorY1+y;
                checkY1(y1,y2);
                break;
        }

        //Reposition and resize the dialog box:
        onSetPosition(x1,y1);
        onSetSize(x2-x1,y2-y1);

        return; //Do not pass the event to ancestor.
    }

    //Otherwise, change the shape of the mouse pointer as the user moves the
    //mouse across the window's edges.
    if(!isMousePressed())
        adjustResize(x,y);

    //The dialog box's operations (movement and resizing) take precedence over
    //the window's operations (controls).  If the user is moving or resizing, we
    //don't want the window to handle the event
    tWindow::onMouseMove(x,y);
}

/****************************************************************************\
tDialogBox::onMouseUp

Introduces repositioning and resizing to the window's onMouseUp event handler.
The function resets state variables and updates the shape of the mouse pointer.
\****************************************************************************/
void tDialogBox::onMouseUp(int x, int y, eMouseButton button)
{
    //Stop repositioning:
    titleBarDepressed=false;

    //Stop resizing:
    resizing=false;

    //Update the shape of the mouse pointer:
    adjustResize(x,y);

    //Always pass the event to the ancestor:
    tWindow::onMouseUp(x,y,button);
}

/****************************************************************************\
tDialogBox::onSelect

Updates the shape of the mouse pointer when the user moves the mouse onto this
window.
\****************************************************************************/
void tDialogBox::onSelect(int x, int y)
{
    tWindow::onSelect(x,y);

    //Change the mouse pointer to a resize pointer if necessary:
    adjustResize(x,y);
}

/****************************************************************************\
tDialogBox::onDeselect

Cancels resize mode when the user moves the mouse away from the window.
\****************************************************************************/
void tDialogBox::onDeselect()
{
    tWindow::onDeselect();
    resizeDir=DIR_NONE;
}

/****************************************************************************\
tDialogBox::windowButtonHandler

This function interfaces with the window buttons.  When the user clicks on
a button (e.g. Close), the button's onExecute function branches to here.  This
function takes the appropriate action depending on the sender.

Inputs:
    sender          Non-null pointer to a tWindowButton object
Outputs:
    none
\****************************************************************************/
void tDialogBox::windowButtonHandler(tControl *sender)
{
    //Minimize, maximize, restore, or close depending on the sender.
    switch(((tWindowButton *) sender)->getAction())
    {
        case WNDACT_MINIMIZE:
            minimize();
            break;
        case WNDACT_MAXIMIZE:
            maximize();
            break;
        case WNDACT_RESTORE:
            restore();
            break;
        case WNDACT_CLOSE:
            close();
            break;
    }
}

/****************************************************************************\
tDialogBox::onSetSize

Resizes the dialog box and repositions the window buttons.
\****************************************************************************/
void tDialogBox::onSetSize(int width, int height)
{
    tWindow::onSetSize(width,height);

    /* Calculate the lower-left corner of the right button.  See diagram below:
     *
     *                                      |<---->| <-Button Width
     *     Margin { ...--------------------------------+
     *            - ...------------------------------+ |
     *            |                       . +------+ | |  -
     *        Title Bar                   . |######| | |  | Button
     *          Height                    . |######| | |  | Height
     *            |                       . +------+ | |  -
     *            - .................................| |
     *                                               . .
     *                                       Title   | | <-Margin
     *                                    |<- Bar  ->|
     *                                       Height
     */
    int x=getControlX()+getWidth()-margin-buttons[0]->getWidth()+MIN(buttons[0]->getWidth()-titleBarHeight,-2)/2;
    int y=getControlY()+getHeight()-margin-buttons[0]->getHeight()+MIN(buttons[0]->getHeight()-titleBarHeight,-2)/2;

    //Move each window button to the correct location:
    if(buttons[0])
    {
        //I must use onSetPosition instead of setControlPosition in order to
        //avoid unwanted side effects when resizing.  When the user resizes the
        //window, I don't want to select any controls, but setControlPosition
        //calls adjustSelection.
        buttons[0]->onSetPosition(x,y);
        x-=buttons[0]->getWidth();
    }
    if(buttons[1])
    {
        buttons[1]->onSetPosition(x,y);
        x-=buttons[0]->getWidth();
    }
    if(buttons[2])
        buttons[2]->onSetPosition(x,y);
}

/****************************************************************************\
tDialogBox::hideControl

Extends tWindow::hideControl to support minimizing.  The function clears
the control's oldVisible flag, but hides it only if the dialog box isn't
minimized.  If minimized, we assume that the control is already hidden.
\****************************************************************************/
void tDialogBox::hideControl(tControl *c)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(c))
        return;
#endif
    c->oldVisible=false;
    if(state!=WINDOW_MINIMIZED)
        tWindow::hideControl(c);
}

/****************************************************************************\
tDialogBox::showControl

Extends tWindow::showControl to support minimizing.  The function sets the
control's oldVisible flag, but shows it only if the dialog box isn't minimized.
When minimized, no controls are visible (except the window buttons).
\****************************************************************************/
void tDialogBox::showControl(tControl *c)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(c))
        return;
#endif
    c->oldVisible=true;
    if(state!=WINDOW_MINIMIZED)
        tWindow::showControl(c);
}

/****************************************************************************\
tDialogBox::createControl

Extends tWindow::createControl to support minimizing.  When you create a
control in a minimized dialog box, the control automatically becomes hidden.
NOTE: The control's constructor sets oldVisible to true by default.  Therefore,
the control will become visible once the user restores the dialog box.
\****************************************************************************/
void tDialogBox::createControl(tControl *control)
{
    tWindow::createControl(control);
    if(state==WINDOW_MINIMIZED)
        tWindow::hideControl(control);
}

/****************************************************************************\
tDialogBox::onKeyDown

Extends tWindow::onKeyDown to support the ESCAPE key.  Pressing ESCAPE in any
control closes the dialog box.
\****************************************************************************/
void tDialogBox::onKeyDown(tKey key, char ch)
{
    if(closeable && key==GLFW_KEY_ESC)
        close();
    tWindow::onKeyDown(key,ch);
}

/****************************************************************************\
tDialogBox::tDialogBox

Creates and initializes an instance of tDialogBox.  Descendents should provide
values for titleBarHeight and margin that coincide with the control's visual
appearance.  The window begins in the specified state (e.g. minimized,
maximized, etc.)  The window's position and dimensions will not always match those
that you provide.  The constructor will initialize the dialog box with these
values, then call minimize(), maximize(), or restore().  The instructions
in onMinimize(), onMaximize(), and onRestore() will ultimately decide the
window's final position.  In addition, the constructor will adjust the provided
dimensions so that they fit within the resize boundaries.

Inputs:
    controlX, controlY          Coordinates of control's lower-left corner
    width, height               Control's dimensions
    _titleBarHeight             Height of the title bar.  The user drags the
                                    title bar to reposition the window.
    _margin                     Size of the window's margins.  The user drags
                                    the window's margins to resize the window.
    _state                      WINDOW_NORMAL, WINDOW_MAXIMIZED, or WINDOW_MINIMIZED
    modal                       Is this window modal?  A modal window stays on
                                    top of other window and retains focus.
    destroyOnClose              Will the window destroy itself when closed?
                                    If false, the window will hide itself.
    _moveable                   Can the user reposition the window?
    _resizeable                 Can the user resize the window?
    _maximizeable               Is a maximize button visible?
    _minimizeable               Is a minimize button visible?
    _closeable                  Is a close button visible?
    _minWidth, _maxWidth,       Resize boundaries.  Minimum value for minWidth
    _minHeight, _maxHeight          and minHeight is 50.  Maximum value for
                                    maxWidth is getScreenWidth().  Maximum value for
                                    maxHeight is getScreenHeight().  The maximum
                                    width/height must equal or exceed the minimum
                                    width/height.
\****************************************************************************/
tDialogBox::tDialogBox(int controlX, int controlY, int width, int height,
    int _titleBarHeight, int _margin, eWindowState _state, bool modal,
    bool destroyOnClose, bool _moveable, bool _resizeable, bool _maximizeable,
    bool _minimizeable, bool _closeable, int _minWidth, int _maxWidth,
    int _minHeight, int _maxHeight) :
    tWindow(controlX,controlY,width,height,modal,false,destroyOnClose),
    titleBarHeight(_titleBarHeight), margin(_margin), state(WINDOW_NONE),
    moveable(_moveable), resizeable(_resizeable), maximizeable(_maximizeable),
    minimizeable(_minimizeable), closeable(_closeable), titleBarDepressed(false),
    resizing(false), resizeDir(DIR_NONE), minWidth(_minWidth), maxWidth(_maxWidth),
    minHeight(_minHeight), maxHeight(_maxHeight)
{
    type=CTRL_DIALOG_BOX;

    //Check resize boundaries
    if(minWidth<0)  minWidth=0;
    if(minHeight<0) minHeight=0;
    if(maxWidth<0)  maxWidth=INT_MAX;
    if(maxHeight<0) maxHeight=INT_MAX;

    if(resizeable)
    {
        clampRange(minWidth,50,getScreenWidth());
        clampRange(maxWidth,50,getScreenWidth());
        clampRange(minHeight,50,getScreenHeight());
        clampRange(maxHeight,50,getScreenHeight());
        if(minWidth>maxWidth)
            maxWidth=minWidth;
        if(minHeight>maxHeight)
            maxHeight=minHeight;
        clampRange(width,minWidth,maxWidth);
        clampRange(height,minHeight,maxHeight);
    }

    /* Create three window buttons and register them.  At one point I created
     * the one window button in the tStdDialogBox constructor and used a duplicate
     * function to create two copies.  Ultimately, this created much confusion
     * in exchange for a small increase in flexibility.  In the end, I decided
     * to create the buttons here, instead.  I assign the buttons an arbitrary
     * position and action until I update the buttons in minimize(), maximize(),
     * or restore().
     */
    for(int i=0;i<3;i++)
    {
        buttons[i]=mwNew tWindowButton(0,0,WNDACT_NONE,(tEvent) &tDialogBox::windowButtonHandler);
        createControl(buttons[i]);
    }

    //Load oldX, oldY, oldWidth, and oldHeight since restore() uses these values.
    oldX=controlX;
    oldY=controlY;
    oldWidth=width;
    oldHeight=height;

    //Minimize, maximize, or restore the dialog box to initialize the window buttons.
    if(_state==WINDOW_MINIMIZED)
        minimize();
    else if(_state==WINDOW_MAXIMIZED)
        maximize();
    else
        restore();
}

/****************************************************************************\
tDialogBox::resizeActivated

Determines whether the user can resize the dialog box and whether the mouse
is positioned within the margin.  We use the results of this function to
determine whether to display the resize pointer.

Inputs:
    x, y                Absolute coordinates of the mouse
Outputs:
    Returns true if the user can begin resizing by dragging the mouse from the
        current location
\****************************************************************************/
bool tDialogBox::resizeActivated(int x, int y)
{
    return resizeable && state==WINDOW_NORMAL && activated(x,y) &&
        (x-getControlX()<RESIZE_MARGIN || getControlX()+getWidth()-x<=RESIZE_MARGIN ||
        y-getControlY()<RESIZE_MARGIN || getControlY()+getHeight()-y<=RESIZE_MARGIN);
}
const int tDialogBox::RESIZE_MARGIN=5;


/****************************************************************************\
adjustSelection

Selects a new window based on the position of the mouse.  The function aborts
if the selected window is in lock-select mode and you have not specified
overrideLock.  Otherwise, it selects the first visible non-background window.
If it does find a suitable match, it selects the first visible background
window.

Inputs:
    x, y            Absolute coordinates of the mouse
    overrideLock    If true, the function will select a new window even if
                        the current window is in lock select mode.  Set this
                        flag for onMouseDown events.
Outputs:
    none
\****************************************************************************/
static void adjustSelection(int x, int y, bool overrideLock)
{
    tWindow *w=NULL;
    int i;

    //If the selected window is in lock select mode and overrideLock is false,
    //then we don't want to adjust the selection.
    if(selectedWindow&&selectedWindow->isLockSelect()&&!overrideLock)
        return;

    //Find the first visible non-background window which the mouse covers.
    //Stop once we reach a modal window (the user can't select a window behind
    //a modal window.)
    for(i=0;i<windows.size();i++)
        if(windows[i]->isVisible() && !windows[i]->isBackground())
        {
            if(windows[i]->activated(x,y))
            {
                w=windows[i];
                break;
            }
            if(windows[i]->isModal())
                break;
        }

    //If we did not find a suitable window, then check background windows.
    if(i==windows.size())
        for(i=0;i<windows.size();i++)
            if(windows[i]->isVisible() && windows[i]->isBackground() && windows[i]->activated(x,y))
            {
                w=windows[i];
                break;
            }

    //If the user has selected a new window, then generate an onDeselect event
    //for the old window and an onSelect event for the new window.
    if (w != selectedWindow)
    {
        if(selectedWindow)
            selectedWindow->onDeselect();
        selectedWindow=w;
        if(selectedWindow)
            selectedWindow->onSelect(x,y);
    }
}

/****************************************************************************\
onMouseDown

Adjusts the selection and focus when the user presses the mouse in a new
window.  Passes the event to the selected window.  NOTE: This is one of the
three main mouse event handlers.  All onMouseDown events originate from this
function.  The main game loop should call this function when the user
depresses the left or right mouse button.

Inputs:
    x, y            Mouse coordinates measured from the bottom-left corner of
                        the screen (or client area)
    button          MOUSE_BUTTON_LEFT or MOUSE_BUTTON_RIGHT
Outputs:
    none
\****************************************************************************/
void onMouseDown(int x, int y, eMouseButton button)
//this function closely resembles tWindow::onMouseDown
{
    saveMouseX=x;
    saveMouseY=y;

    //Even if the mouse is still over the selected window, another window may
    //cover it.  Therefore, we need to adjust the selection each time.
    adjustSelection(x,y, /* overrideLock = */ true);

    //If the selected window differs from the focused window (and the focused
    //window isn't modal) then change the focus.  NOTE: Unlike
    //tWindow::onMouseDown, you -can- defocus the focused window by clicking
    //in a blank area of the screen.
    if (selectedWindow!=focusedWindow && (!focusedWindow || !focusedWindow->isModal()))
    {
        if (focusedWindow)
            defocusWindow(focusedWindow);
        if (selectedWindow)
            focusWindow(selectedWindow);
    }

    //Always pass the event to the selected window.
    if (selectedWindow)
        selectedWindow->onMouseDown(x,y,button);
}

/****************************************************************************\
onMouseMove

Adjusts the selection as the user moves the mouse (without holding down any
mouse buttons).  Passes the event to the selected window.  NOTE: This is one
of the three main mouse event handlers.  All onMouseMove events originate from
this function.  The main game loop should call this function when the user
moves the mouse (regardless of the button state).

Inputs:
    x, y            Mouse coordinates measured from the bottom-left corner of
                        the screen (or client area)
Outputs:
    none
\****************************************************************************/
void onMouseMove(int x, int y)
{
    saveMouseX=x;
    saveMouseY=y;

    //While the user holds down at least one mouse button, continue to pass the
    //event to the selected window even if the mouse no longer covers it.
    if(isMousePressed())
    {
        if(selectedWindow)
            selectedWindow->onMouseMove(x,y);
    }
    else
    {
        //Even if the mouse is still inside the selected window, another window
        //may cover it.  Therefore, we always adjust the selection.
        adjustSelection(x,y, /* overrideLock = */ false);

        //Dispatch the event to the selected window.
        if (selectedWindow)
            selectedWindow->onMouseMove(x,y);
    }
}

/****************************************************************************\
onMouseUp

Dispatches the event to the selected window and adjusts the selection.  NOTE:
This is one of the three main mouse event handlers.  All onMouseUp events
originate from this function.  The main game loop should call this function
when the user releases the left or right mouse button.

Inputs:
    x, y            Mouse coordinates measured from the bottom-left corner of
                        the screen (or client area)
Outputs:
    none
\****************************************************************************/
void onMouseUp(int x, int y, eMouseButton button)
{
    saveMouseX=x;
    saveMouseY=y;

    //Dispatch the event to the selected window.
    if(selectedWindow)
    {
        selectedWindow->onMouseUp(x,y,button);
        //Even if the mouse is still inside the selected window, another window
        //may cover it.  Therefore, we always adjust the selection.
        adjustSelection(x,y, /* overrideLock = */ false);
    }
}

/****************************************************************************\
onKeyDown

Dispatches the event to the focused window.  NOTE: This is one of the two main
keystroke handlers.  All onKeyDown events originate from this function.  The
main game loop should call this function when the user presses a key.

Inputs:
    key     Virtual key code.  Usually the virtual key code will equal its
                ASCII code, except for special keys (see UIKEY constants in
                header file).
Outputs:
    none
\****************************************************************************/
void onKeyDown(tKey key, char ch)
{
    if(focusedWindow)
        focusedWindow->onKeyDown(key,ch);
}

/****************************************************************************\
onKeyUp

Dispatches the event to the focused window.  NOTE: This is one of the two main
keystroke handlers.  All onKeyUp events originate from this function.  The
main game loop should call this function when the user releases a key.

Inputs:
    key     Virtual key code.  Usually the virtual key code will equal its
                ASCII code, except for special keys (see UIKEY constants in
                header file).
Outputs:
    none
\****************************************************************************/
void onKeyUp(tKey key, char ch)
{
    if(focusedWindow)
        focusedWindow->onKeyUp(key,ch);
}

/****************************************************************************\
onMouseWheel

Dispatches the event to the focused window.  NOTE: This is one of the main mouse
event handlers.  All onMouseWheel events originate from this function.  The
main game loop should call this function when the user rolls the mouse wheel.

Inputs:
    pos     Position of the mouse wheel
Outputs:
    none
\****************************************************************************/
void onMouseWheel(int pos,eMouseWheel direction)
{
    if(focusedWindow)
        focusedWindow->onMouseWheel(pos,direction);
}

/****************************************************************************\
focusWindow

Focuses a window and brings it to the front of the screen.  The function takes
no action if the window is already focused.  If a modal window has focus, you
may only focus other modal windows.

Inputs:
    w           Non-null pointer to a visible window in the windows container
                    (created with createWindow)
Outputs:
    none
\****************************************************************************/
void focusWindow(tWindow *w)
{
    int i;

#ifdef DEBUG
    if(MEMORY_VIOLATION(w))
        return;
    if(!w->isVisible())
    {
        bug("focusWindow: window is hidden");
        return;
    }
#endif
    //No need to continue if the window is already focused.
    if(w==focusedWindow)
        return;

#ifdef DEBUG
    //Make sure the focused window isn't modal.  If so, 'w' must be modal also.
    if(focusedWindow&&focusedWindow->isModal()&&!w->isModal())
    {
        bug("focusWindow: current window is modal");
        return;
    }
#endif

    //Defocus the old window:
    if(focusedWindow)
        defocusWindow(focusedWindow);

    //Find the window's index in the windows container:
    for(i=0;i<windows.size();i++)
        if(windows[i]==w)
            break;

#ifdef DEBUG
    if(i==windows.size())
    {
        bug("focusWindow: window not found");
        return;
    }
#endif

    //Insert the window at the beginning of the windows container (bringing it
    //to the front.)
    windows.erase(windows.begin()+i);
    windows.insert(windows.begin(),w);

    //Focus the new window:
    focusedWindow=w;
    if(focusedWindow)
        focusedWindow->onFocus();

    //The mouse may cover a new window now:
    adjustSelection(saveMouseX,saveMouseY, /* overrideLock = */ false);
}

/****************************************************************************\
defocusWindow

Defocuses the focused window.  Although the parameter may seem redundant, I
leave it here for consistency with focusWindow, showWindow, et al.

Inputs:
    w           Must equal focusedWindow
Outputs:
    none
\****************************************************************************/
void defocusWindow(tWindow *w)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(w))
        return;
    if(focusedWindow!=w)
    {
        bug("defocusWindow: this window isn't focused");
        return;
    }
#endif
    focusedWindow->onDefocus();
    focusedWindow=NULL;
}

/****************************************************************************\
hideWindow

Hides a window and focuses the window on the top of the screen.

Inputs:
    w           Non-null pointer to a window in the windows container
Outputs:
    none
\****************************************************************************/
void hideWindow(tWindow *w)
{
    int i;

#ifdef DEBUG
    if(MEMORY_VIOLATION(w))
        return;

    //Ensure that the pointer exists in the windows container:
    for(i=0;i<windows.size();i++)
        if(windows[i]==w)
            break;
    if(i==windows.size())
    {
        bug("hideWindow: window not found");
        return;
    }
#endif

    //Ensure that selectedWindow and focusedWindow no longer reference the window.
    //onHide will call onDefocus and onDeselect if necessary.
    if(selectedWindow==w)
        selectedWindow=NULL;
    if(focusedWindow==w)
        defocusWindow(focusedWindow);
    if(w->isVisible())
        w->onHide();

    //Focus the window on the top:
    for(i=0;i<windows.size();i++)
        if(windows[i]->isVisible())
        {
            focusWindow(windows[i]);
            return;
        }
}

/****************************************************************************\
showWindow

Shows and focuses a hidden window or focuses a visible window.

Inputs:
    w           Non-null pointer to a window in the windows container
Outputs:
    none
\****************************************************************************/
void showWindow(tWindow *w)
{
    int i;

#ifdef DEBUG
    if(MEMORY_VIOLATION(w))
        return;

    //Ensure that the pointer exists in the windows container:
    for(i=0;i<windows.size();i++)
        if(windows[i]==w)
            break;
    if(i==windows.size())
    {
        bug("showWindow: window not found");
        return;
    }
#endif

    if(!w->isVisible())
        w->onShow();
    focusWindow(w);
}

/****************************************************************************\
drawWindows

Draws windows from back to front.  The function draws all background windows
first.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void drawWindows()
{
    int i;

    if(windows.size()==0) //CAREFUL!  size() is unsigned
        return;
    for(i=windows.size()-1;i>=0;i--)
        if(windows[i]->isBackground())
            windows[i]->draw();
    for(i=windows.size()-1;i>=0;i--)
        if(!windows[i]->isBackground())
            windows[i]->draw();
}

/****************************************************************************\
createWindow

Adds a newly created tWindow object to the window container and focuses it.
You will typically use this function in the following manner:

tWindow *w = new tWindow(...); //or any descendent of tWindow
createWindow(w);

Inputs:
    w           Pointer to a new descendent of tWindow
Outputs:
    none
\****************************************************************************/
void createWindow(tWindow *w)
{
    int x=w->getControlX(),y=w->getControlY();
    if(w->getHorAnchor()==ANCHOR_CENTER || w->getVertAnchor()==ANCHOR_CENTER)
    {
        if(w->getHorAnchor()==ANCHOR_CENTER)
            x=(getScreenWidth()-w->getWidth())/2;
        if(w->getVertAnchor()==ANCHOR_CENTER)
            y=(getScreenHeight()-w->getHeight())/2;
        w->onSetPosition(x,y);
    }
    windows.push_back(w);
    focusWindow(w);
}

/****************************************************************************\
destroyWindow

Destroys a window and focuses the window on the top of the screen.  NOTE:
This module does not actually delete the window until the next call to
pollWindows.

Inputs:
    w           Non-null pointer to a window in the windows container
Outputs:
    none
\****************************************************************************/
void destroyWindow(tWindow *w)
{
    int i;

#ifdef DEBUG
    if(MEMORY_VIOLATION(w))
        return;
    if(selectedWindow==w)
        selectedWindow=NULL;
#endif

    if(focusedWindow==w)
        defocusWindow(w);

#ifdef DEBUG
    //Ensure that window exists in windows container.
    for(i=0;i<windows.size();i++)
        if(windows[i]==w)
            break;
    if(i==windows.size())
    {
        bug("destroyWindow: window not found");
        return;
    }
#endif

    w->onDestroy();

    //Focus the window on the top:
    for(i=0;i<windows.size();i++)
        if(windows[i]->isVisible())
        {
            focusWindow(windows[i]);
            return;
        }
}

/****************************************************************************\
destroyWindows

Destroys all windows and clears focused/selected window pointers.  Call this
function when the application ends to clean up memory.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void destroyWindows()
{
    if(focusedWindow)
        defocusWindow(focusedWindow);
    if(selectedWindow)
        selectedWindow=NULL;

    for(int i=0;i<windows.size();i++)
        if(windows[i])
            mwDelete windows[i];
    windows.clear();
}

/****************************************************************************\
pollWindows

Erases destroyed windows.  Polls the remaining windows.  Call this function
during each pass.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void pollWindows()
{
    int i;
	for(i=0;i<windows.size();i++)
    {
        if(windows[i]->isDestroyed())
        {
            mwDelete windows[i];
            windows.erase(windows.begin()+i);
        }
        else
            i++;
    }

    for(i=0;i<windows.size();i++)
        windows[i]->poll();
}

/****************************************************************************\
refreshWindows

Refreshes all windows.  Call this function after reinitializing OpenGL (e.g.
the user toggles full-screen/windowed mode.)

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void refreshWindows()
{
    for(int i=0;i<windows.size();i++)
        windows[i]->refresh();
}

/****************************************************************************\
setWindowPosition

Genereates an onSetPosition event and adjusts the selection.  Call this function
to reposition a window manually.

Inputs:
    w           Non-null pointer to a window in the windows container
    x, y        New coordinates for the window's bottom-left corner
Outputs:
    none
\****************************************************************************/
void setWindowPosition(tWindow *w, int x, int y)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(w))
        return;
#endif

    w->onSetPosition(x,y);

    //The mouse may cover a new window now:
    adjustSelection(saveMouseX,saveMouseY, /* overrideLock = */ false);
}

/****************************************************************************\
setWindowSize

Generates an onSetSize event and adjusts the selection.  Call this function
to resize a window manually.

Inputs:
    w               Non-null pointer to a window in the windows container
    width, height   New dimensions for the window
Outputs:
    none
\****************************************************************************/
void setWindowSize(tWindow *w, int width, int height)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(w))
        return;
#endif

    w->onSetSize(width,height);

    //The mouse may cover a new window now:
    adjustSelection(saveMouseX,saveMouseY,false);
}

/****************************************************************************\
windowLookup

Searches for the first window with a matching tag.  This allows us to label
important windows with unique identifiers and retrieve them at will.

Inputs:
    tag         Tag for which to search
Outputs:
    Returns a pointer to the first matching window or NULL if no matches were
        found
\****************************************************************************/
tWindow *windowLookup(int tag)
{
    int i;
    for(i=0;i<windows.size();i++)
        if(windows[i]->tag==tag&&!windows[i]->isDestroyed())
            return windows[i];
    return NULL;
}

/****************************************************************************\
closeWindows

Closes all windows.  The function will either hide or destroy each window
depending on the destroyOnClose flag.

Inputs:
    none
Outputs:
    none
\****************************************************************************/
void closeWindows()
{
    int i;
    do
    {
        //IMPORTANT: Order of windows vector may change with each subsequent
        //call to close.  Therefore, we must start the search over each time
        //we find a visible window.
        for(i=0;i<windows.size();i++)
            if(windows[i]->isVisible())
            {
                windows[i]->close();
                break;
            }
    }
    while(i<windows.size());
}

/****************************************************************************\
onSetResolution

The main module should call this function when the window's size or screen's
resolution changes.  The function generates onSetPosition and onSetSize events
for windows with special anchor settings.

Inputs:
    w,h     New values for screen width and screen height
Outputs:
    none
\****************************************************************************/
void onSetResolution(int w, int h)
{
    static int oldWidth=0, oldHeight=0;
    int cx,cy,cw,ch,i;
    bool needMove,needResize;

    int fw=w-oldWidth;
    int fh=h-oldHeight;
    if(fw==0 && fh==0 || w==0 || h==0)
        return;

    oldWidth=w;
    oldHeight=h;

    for(i=0;i<windows.size();i++)
    {
        cx=windows[i]->getControlX();
        cy=windows[i]->getControlY();
        cw=windows[i]->getWidth();
        ch=windows[i]->getHeight();
        needMove=false;
        needResize=false;
        switch(windows[i]->getHorAnchor())
        {
            case ANCHOR_RIGHT:
                cx+=fw;
                needMove=true;
                break;
            case ANCHOR_BOTH:
                cw+=fw;
                needResize=true;
                break;
            case ANCHOR_CENTER:
                cx=(w-windows[i]->getWidth())/2;
                needMove=true;
                break;
        }
        switch(windows[i]->getVertAnchor())
        {
            case ANCHOR_TOP:
                cy+=fh;
                needMove=true;
                break;
            case ANCHOR_BOTH:
                ch+=fh;
                needResize=true;
                break;
            case ANCHOR_CENTER:
                cy=(h-windows[i]->getHeight())/2;
                needMove=true;
                break;
        }
        if(needMove)
            windows[i]->onSetPosition(cx,cy);
        if(needResize)
            windows[i]->onSetSize(cw,ch);
    }
}


//The tag allows pointers one degree of flexibility.  For example, resize and
//scroll pointers can face different directions whereas I-beam pointers can
//adjust their height.  Set tag to zero if the pointer is static.
void refreshMousePointer()
{
    if(mousePointer)
        mousePointer->refresh();
}
void destroyMousePointer()
{
    if(mousePointer)
    {
        mwDelete mousePointer;
        mousePointer=NULL;
    }
}
bool getMousePointer(int x, int y, ePointer& p, int& tag)
{
    if(selectedWindow)
    {
        selectedWindow->getMousePointer(x,y,p,tag);
        return true;
    }
    return false;
}
void drawMousePointer()
{
    ePointer p=POINTER_STD;
    int tag=0;
    getMousePointer(saveMouseX,saveMouseY,p,tag);

    if(mousePointer)
    {
        if(!mousePointer->isType(p, tag))
        {
            mwDelete mousePointer;
            mousePointer=NULL;
        }
    }
    if(mousePointer==NULL)
        switch(p)
        {
            case POINTER_STD:
                mousePointer=mwNew tStdPointer(tag);
                break;
            case POINTER_I_BEAM:
                mousePointer=mwNew tIBeamPointer(tag);
                break;
            case POINTER_RESIZE:
                mousePointer=mwNew tResizePointer(tag);
                break;
            case POINTER_SCROLL:
                mousePointer=mwNew tScrollPointer(tag);
                break;
            case POINTER_TARGET:
                mousePointer=mwNew tTargetPointer(tag);
                break;
            case POINTER_INVISIBLE:
                mousePointer=mwNew tInvisiblePointer(tag);
                break;
        }
    if(mousePointer)
        mousePointer->draw(saveMouseX,saveMouseY);
}


