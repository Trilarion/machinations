/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef oglwrapH
#define oglwrapH

//Includes:
#include <GL/glfw.h>
#include <GL/glext.h>
#include "vec.h"

/***************************************************************************/
/*
 * The following three function attempt to optimize OpenGL states.  'mgl' stands
 * for My GL.
 */
extern void mglBindTexture(GLuint index);            //Calls glBindTexture.
extern void mglSetActiveTexture(int index);

//Other useful OpenGL functions:
extern void calcNormal(float in0[3], float in1[3], float in2[3], float out[3]);
extern void drawButton(int x, int y, int width, int height, int bevel,
    float r1, float g1, float b1, float r2, float g2, float b2, float r3, float g3, float b3);
extern void mglTorus(float r0, float r1, int slices, int loops, float angle);
extern void mglPrism(float x, float y, float z);
extern void mglFuzzyDisk(float inner, float outer, int slices);

extern void findShadowMatrix(const float ground[4], const float light[4], float shadowMat[16]);
extern void multShadowMatrix(const float ground[4], const float light[4]);

extern bool multitextureSupported;
#ifdef LINUX
    #define _glMultiTexCoord1fARB glMultiTexCoord1f
    #define _glMultiTexCoord2fARB glMultiTexCoord2f
    #define _glMultiTexCoord3fARB glMultiTexCoord3f
    #define _glMultiTexCoord4fARB glMultiTexCoord4f
    #define _glActiveTextureARB glActiveTexture
    #define _glClientActiveTextureARB glClientActiveTexture
#else
    extern PFNGLMULTITEXCOORD1FARBPROC _glMultiTexCoord1fARB;
    extern PFNGLMULTITEXCOORD2FARBPROC _glMultiTexCoord2fARB;
    extern PFNGLMULTITEXCOORD3FARBPROC _glMultiTexCoord3fARB;
    extern PFNGLMULTITEXCOORD4FARBPROC _glMultiTexCoord4fARB;
    extern PFNGLACTIVETEXTUREARBPROC _glActiveTextureARB;
    extern PFNGLCLIENTACTIVETEXTUREARBPROC _glClientActiveTextureARB;
#endif
extern int maxTexelUnits;

extern bool texture3DSupported;
#ifdef LINUX
    #define _glTexImage3D glTexImage3D
#else
    extern PFNGLTEXIMAGE3DPROC _glTexImage3D;
#endif

extern void openGLStartup();
extern void openGLRefresh();
extern void openGLShutdown();

extern void ATIRadeon8500Hack();

extern GLenum getLightName(int i);
/****************************************************************************/
#endif
