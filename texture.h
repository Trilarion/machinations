/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef textureH
#define textureH
/****************************************************************************/
//Includes
#include "parse.h"
#include "game.h"

//Defines
#define     TEXTURE_PATH        "data/textures/"
#define     TEXTURE_EXTENSION   ".tpk"

//This enumeration determines how a control reshapes itself when the size of
//the medium changes (window or screen).  Each control (including windows) has a
//horizontal and a vertical anchor.
enum eAnchor {
    ANCHOR_LEFT = 0,    //Left edge is anchored (fixed width) [default]
    ANCHOR_BOTTOM = 0,  //Bottom edge is anchored (fixed height) [default]
    ANCHOR_RIGHT = 1,   //Right edge is anchored (fixed width)
    ANCHOR_TOP = 1,     //Top edge is anchored (fixed height)
    ANCHOR_BOTH = 2,    //Both edges are anchored (variable width/height)
    ANCHOR_CENTER = 3   //Control centers itself within the medium (fixed width/height)
};

class tTexturePack;

struct tTexture
{
    tString         name;
    tTexturePack*   pack;
    bool            alphaBlending;
    bool            alphaTesting;
    int             x;
    int             y;
    int             width;
    int             height;
    int             xOffset;
    int             yOffset;
};

class tTexturePack
{
    tString             name;
    tString             imageFile;
    int                 references;
    iTextureIndex       index;
    int                 width;
    int                 height;
    int                 bpp;
    vector<tTexture>    textures;

    public:
    const tString&      getName() { return name; }
    int                 getReferences() { return references; }
    iTextureIndex       getIndex() { return index; }
    int                 getWidth() { return width; }
    int                 getHeight() { return height; }
    bool                hasAlphaChannel() { return (bpp==4); }
    tTexturePack() : references(0), index(0), width(0), height(0) {}
    ~tTexturePack();
    bool                load(const tString& _name);
    bool                refresh();
    tTexture*           getTexture(const tString& name);
    void                releaseTexture(tTexture *texture);
};

class tSkin
{
    tString             filename;
    int                 references;
    iTextureIndex       index;
    int                 width;
    int                 height;
    int                 bpp;

    public:
    const tString&      getFilename() { return filename; }
    int                 getReferences() { return references; }
    iTextureIndex       getIndex() { return index; }
    int                 getWidth() { return width; }
    int                 getHeight() { return height; }
    bool                hasAlphaChannel() { return (bpp==4); }
    tSkin() : references(0), index(0), width(0), height(0) {}
    ~tSkin();

    bool                load(const tString& _filename);
    bool                refresh();
    void                incrementReferences() { references++; }
    void                decrementReferences() { references--; }
};

/* We developed two classes for rendering animation.  tAnimType contains
 * information about one type of animation, whereas tAnim contains information
 * about one instance of an animation.  You can think of tAnimType as a roll
 * of film and tAnim as a projector.
 */
class tAnimType
{
    public:
    tString             name;               //Unique identifier for referencing animation
    iTextureIndex       texture;            //Texture on which the frames reside
    vector<tTexture *>  frames;             //Dynamic array containing frame data
    float               frameRate;          //Frequency (frames/second)
    bool                repeat;             //Return to the first frame after the last?
    void                destroy();
    tAnimType(tStream& s) throw(tString);
    ~tAnimType() { destroy(); }
};

/* tAnim allows you to play an animation in real-time (e.g. using the system
 * clock) or game time (e.g. using the game clock).  It also allows you to
 * render the animation with any scale or orientation.
 */
class tAnim
{
    private:
    tAnimType*  type;                   //Which "reel of film" is this "projector" playing?
    tTime       startTime1;             //System time when the animation started
    tWorldTime  startTime2;             //World time when the animation started
    int         curFrame;               //Which frame is visible?
    bool        finished;               //Has the animation completed?
                                        //Animation types whose repeat property is set will
                                        //never finish.

    public:
    bool    isFinished()                //Determines if the animation has finished.
        { return finished; }            //A repeating animation will never finish.
    float   getProgress();
    void    load(tAnimType *_type);
    void    start(tTime _startTime);
    void    start(tWorldTime _startTime);
    void    tick(tTime curTime);
    void    tick(tWorldTime curTime);
    void    draw(int x, int y);
    tAnim();
    tAnim(tAnimType* _type);
};

struct tPanelTexture
{
	tTexture *texture;
	eAnchor hAlignment;
	int left;
	int width;
	eAnchor vAlignment;
	int bottom;
	int height;
	bool repeat;
};

class tPanel
{
	private:
	enum eStatus { STATUS_UNSPECIFIED, STATUS_ABSOLUTE, STATUS_RELATIVE };
	typedef list<tPanelTexture>::iterator itPanelTexture;

	tString name;
	list<tPanelTexture> panelTextures;
	void parseValue(int& value, eStatus& status, tStream& s) throw(tString);
	void parseTextureBlock(tStream& s) throw(tString);
	void parseCompositeTextureBlock(tStream& s) throw(tString);
	void destroy();

	public:
    const tString& getName() { return name; }
	tPanel(tStream& s) throw(tString);
	~tPanel() { destroy(); }
	void draw(int x, int y, int width, int height);
};

extern tTexture*        getTexture(const tString& name);
extern void             releaseTexture(tTexture* texture);
extern tSkin*           getSkin(const tString& name);
extern void             releaseSkin(tSkin *skin);

extern void             textureStartup();
extern void             textureShutdown();
extern void             textureRefresh();
extern void             bindTexture(tTexture* texture);
extern void             bindSkin(tSkin* skin);
//If you omit the dimensions, then the function will use the size of the texture
extern void             drawTexture(tTexture* texture, int x, int y, int w=-1, int h=-1,
									bool repeat=false);

extern bool             loadAnimTypes(const char *filename);
extern void             destroyAnimTypes();
extern tAnimType*       animTypeLookup(const tString& name, bool exact);
extern bool             loadPanels(const char *filename);
extern void             destroyPanels();
extern tPanel*          panelLookup(const tString& name, bool exact);
/****************************************************************************/
#endif
