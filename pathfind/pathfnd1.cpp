/****************************************************************************
* Machinations copyright (C) 2001, 2002 by Jon Sargeant and Jindra Kolman   *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include <dir>
#include <io>
#include <math>
#include <stdarg>
#include <stdio>
#include <stdlib>
#include <string>
#include <time>
#include <float>
#include <function>
#include <vector.h>
#include <windows>
#include <winsock2>
#ifdef SGI
    #include <c:\oglsdk\include\gl\gl>		// Header File For The SGI OpenGL Library
    #include <c:\oglsdk\include\gl\glu>		// Header File For The SGI GLu Library
#else
    #include <gl\gl>		    // Header File For The OpenGL32 Library
    #include <gl\glu>		    // Header File For The GLu32 Library
#endif
#pragma hdrstop
//---------------------------------------------------------------------------
#include "types.h"
#include "astar.h"
#include "pathfnd1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMain *Main;
//---------------------------------------------------------------------------
void tpathfinder::onpathfound()
{
    int x,y;
    char buf[40];
    Main->laPathFound->Caption = "Path found";
    sprintf(buf, "%5.3f seconds", highleveltime);
    Main->laHighTime->Caption = buf;
    sprintf(buf, "%5.3f seconds", lowleveltime);
    Main->laLowTime->Caption = buf;
    sprintf(buf, "%d", highlevelnodes);
    Main->laHighNodes->Caption=buf;
    sprintf(buf, "%d", lowlevelnodes);
    Main->laLowNodes->Caption=buf;

    Main->imImage->Canvas->Pen->Color = clRed;
    Main->imImage->Canvas->MoveTo(starttile.x, starttile.y);
    for(int i=0;i<waypoints.size();i++)
    {
        x=waypoints[i].x;
        y=waypoints[i].y;
        if(Main->cbWaypoints->Checked)
            Main->imImage->Canvas->Pixels[x][y] = clRed;
        else
            Main->imImage->Canvas->LineTo(x, y);
        Main->imImage->Canvas->MoveTo(x, y);
    }
}
//---------------------------------------------------------------------------
void tpathfinder::onpathnotfound()
{
    Main->laPathFound->Caption = "Path not found";
}
//---------------------------------------------------------------------------
__fastcall TMain::TMain(TComponent* Owner)
    : TForm(Owner)
{
    bitmapready = false;
    mustreset = false;
    inittimer();
}
//---------------------------------------------------------------------------
void __fastcall TMain::btChooseFileClick(TObject *Sender)
{
    if(odOpen->Execute())
    {
        laFileName->Caption = odOpen->FileName;
        imImage->Picture->LoadFromFile(odOpen->FileName);
        width = MIN(imImage->Picture->Width,MAPSIZE);
        height = MIN(imImage->Picture->Height,MAPSIZE);
        startx=0;
        starty=0;
        endx=width-1;
        endy=height-1;
        init();
    }
}
//---------------------------------------------------------------------------
void TMain::init()
{
    bitmapready = true;
    mustreset = false;
    imImage->Picture->LoadFromFile(odOpen->FileName);
    width = MIN(imImage->Picture->Width,MAPSIZE);
    height = MIN(imImage->Picture->Height,MAPSIZE);
    if(startx>=width)   startx=width-1;
    if(starty>=height)  starty=height-1;
    if(endx>=width)     endx=width-1;
    if(endy>=height)    endy=height-1;
    drawzoom();
    startcolor = imImage->Canvas->Pixels[startx][starty];
    imImage->Canvas->Pixels[startx][starty] = clRed;
    imZoom->Canvas->Brush->Color = clRed;
    imZoom->Canvas->FillRect(TRect(startx*8,starty*8,startx*8+7,starty*8+7));
    endcolor = imImage->Canvas->Pixels[endx][endy];
    imImage->Canvas->Pixels[endx][endy] = clBlue;
    imZoom->Canvas->Brush->Color = clBlue;
    imZoom->Canvas->FillRect(TRect(endx*8,endy*8,endx*8+7,endy*8+7));
}
//---------------------------------------------------------------------------
void TMain::drawzoom()
{
    imZoom->Canvas->Brush->Color = clWhite;
    imZoom->Canvas->FillRect(imZoom->Canvas->ClipRect);
    imZoom->Canvas->Pen->Color = clLtGray;
    for(int y=0; y<32; y++)
    {
        imZoom->Canvas->MoveTo(0,y*8+7);
        imZoom->Canvas->LineTo(imZoom->Canvas->ClipRect.Right,y*8+7);
    }
    for(int x=0; x<32; x++)
    {
        imZoom->Canvas->MoveTo(x*8+7, 0);
        imZoom->Canvas->LineTo(x*8+7, imZoom->Canvas->ClipRect.Bottom);
    }

    for(int y=0; y<MIN(width,32); y++)
        for(int x=0; x<MIN(height,32); x++)
        {
            imZoom->Canvas->Brush->Color = imImage->Canvas->Pixels[x][y];
            imZoom->Canvas->FillRect(TRect(x*8,y*8,x*8+7,y*8+7));
        }
}
//---------------------------------------------------------------------------
void __fastcall TMain::btExitClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------
void __fastcall TMain::imImageMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
    if(!bitmapready || X >= width || Y >= height)
        return;
    if(Button == mbLeft)
    {
        if(X==endx && Y==endy)
            return;
        imImage->Canvas->Pixels[startx][starty] = startcolor;
        imZoom->Canvas->Brush->Color = startcolor;
        imZoom->Canvas->FillRect(TRect(startx*8,starty*8,startx*8+7,starty*8+7));
        startx = X;
        starty = Y;
        startcolor = imImage->Canvas->Pixels[startx][starty];
        imImage->Canvas->Pixels[startx][starty] = clRed;
        imZoom->Canvas->Brush->Color = clRed;
        imZoom->Canvas->FillRect(TRect(startx*8,starty*8,startx*8+7,starty*8+7));
    }
    else if(Button == mbRight)
    {
        if(X==startx && Y==starty)
            return;
        imImage->Canvas->Pixels[endx][endy] = endcolor;
        imZoom->Canvas->Brush->Color = endcolor;
        imZoom->Canvas->FillRect(TRect(endx*8,endy*8,endx*8+7,endy*8+7));
        endx = X;
        endy = Y;
        endcolor = imImage->Canvas->Pixels[endx][endy];
        imImage->Canvas->Pixels[endx][endy] = clBlue;
        imZoom->Canvas->Brush->Color = clBlue;
        imZoom->Canvas->FillRect(TRect(endx*8,endy*8,endx*8+7,endy*8+7));
    }
}
//---------------------------------------------------------------------------
void __fastcall TMain::imZoomMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
    imImageMouseDown(Sender, Button, Shift, X/8, Y/8);
}
//---------------------------------------------------------------------------
void __fastcall TMain::btComputeClick(TObject *Sender)
{
    if(mustreset)
        init();
    for(int x=0;x<width;x++)
        for(int y=0;y<height;y++)
        {
            if(imImage->Canvas->Pixels[x][y]!=clBlack)
                tiles[x][y].weight = 10+(255-(imImage->Canvas->Pixels[x][y] & 255))/10;
            else
                tiles[x][y].weight = -1;
        }
    tiles[startx][starty].weight = 10;
    tiles[endx][endy].weight = 10;

    start = ttime::current();
    calcregions();
    char buf[40];
    sprintf(buf, "%5.3f seconds", ttime::current() - Main->start);
    Main->laRegionSetupTime->Caption = buf;

    start = ttime::current();
    ttilepos s={startx,starty},e={endx,endy};
    tpathfinder pf(s,e,width,height);
    pf.tick(ttime::current(), 10);
    mustreset = true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::btResetClick(TObject *Sender)
{
    init();
}
//---------------------------------------------------------------------------

