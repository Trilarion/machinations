//---------------------------------------------------------------------------
#ifndef astarH
#define astarH
//---------------------------------------------------------------------------
#define MAPSIZE         256
#define AREASIZE        8
#define MAXNEIGHBORS    20
//Max neighbors should equal Region Size DIVIDE UP 2 * 4 + 4

struct ttilenode
{
    unsigned long value;
    ttilepos tile;
};

struct tregionnode
{
    unsigned long value;
    short region;
};

struct ttilegte : public binary_function<ttilenode, ttilenode, bool>
{
    bool operator() (const ttilenode& a, const ttilenode& b) const
    {
        return a.value >= b.value;
    }
};

struct tregiongte : public binary_function<tregionnode, tregionnode, bool>
{
    bool operator() (const tregionnode& a, const tregionnode& b) const
    {
        return a.value >= b.value;
    }
};

struct ttile
{
    short weight; //10 is the minimum weight!
    short region;
    short elevation;
};

//Maximum of 65536 regions!!
struct tregion
{
    tareapos pos;
    short weight; //10 is the minimum weight!
    short elevation;
    unsigned char neighborcount;
    short neighbors[MAXNEIGHBORS];
    direnum neighbordirs[MAXNEIGHBORS];
};

class tpathfinder : public ttask
{
    private:
    //The following variables are used for adjustment in tick, addwaypoint1, and addwaypoint2
    /*
    int curweight;
    ttilepos curtile,oldtile,origtile;
    ttilepos origtile1, oldtile1;
    */

    int heuristic(ttilepos tile1, ttilepos tile2);
    int heuristic(tareapos area1, tareapos area2);
    float weighteddistance(ttilepos tile1, ttilepos tile2);
    int shiftpoint(ttilepos tile1, ttilepos& tile2, int weight, bool counterclockwise);
    bool ispathclear(ttilepos start, ttilepos end, int weight, ttilepos& out);
    void addwaypoint(ttilepos tile);
    bool isvalid(ttilepos tile) { return tile.x>=0 && tile.y>=0 && tile.x<width && tile.y<height; }
    void makevalid(ttilepos tile);

    void lowastar();
    void highastar();

    public:
    ttilepos starttile;
    ttilepos endtile;
    int width;
    int height;
    vector<bool> openregions;
    vector<ttilepos> waypoints;
#ifdef DEBUGASTAR
    private:
    ttime start;
    public:
    long lowlevelnodes;
    long highlevelnodes;
    float lowleveltime;
    float highleveltime;
#endif

    void onpathfound();
    void onpathnotfound();
    void tick(ttime starttime, float duration);
    tpathfinder(ttilepos _starttile, ttilepos _endtile,
        int _width, int _height);
};

extern ttile tiles[MAPSIZE][MAPSIZE];
extern void calcregions();
//---------------------------------------------------------------------------
#endif
