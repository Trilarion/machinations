//---------------------------------------------------------------------------
#ifndef pathfnd1H
#define pathfnd1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <math.h>
#include <stdio.h>
//---------------------------------------------------------------------------
class TMain : public TForm
{
__published:	// IDE-managed Components
    TLabel *Label1;
    TButton *btChooseFile;
    TLabel *Label2;
    TLabel *Label3;
    TLabel *Label4;
    TPanel *Panel1;
    TLabel *Label5;
    TButton *btCompute;
    TButton *btExit;
    TOpenDialog *odOpen;
    TImage *imImage;
    TImage *imZoom;
    TLabel *laFileName;
    TButton *btReset;
    TLabel *Label6;
    TGroupBox *GroupBox1;
    TLabel *Label7;
    TLabel *Label13;
    TLabel *laPathFound;
    TLabel *laHighTime;
    TCheckBox *cbWaypoints;
    TLabel *Label8;
    TLabel *laRegionSetupTime;
    TLabel *laLowTime;
    TLabel *Label10;
    TLabel *Label9;
    TLabel *Label11;
    TLabel *laHighNodes;
    TLabel *laLowNodes;
    void __fastcall btChooseFileClick(TObject *Sender);
    void __fastcall btExitClick(TObject *Sender);
    void __fastcall imImageMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    void __fastcall imZoomMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    void __fastcall btComputeClick(TObject *Sender);
    void __fastcall btResetClick(TObject *Sender);
private:	// User declarations
    void drawzoom();
    TColor startcolor;
    TColor endcolor;
    int startx;
    int starty;
    int endx;
    int endy;
    bool bitmapready;
    void init();
    bool mustreset;
public:		// User declarations
    ttime start;
    int width;
    int height;
    __fastcall TMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMain *Main;
//---------------------------------------------------------------------------
#endif
