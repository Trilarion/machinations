/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef client01H
#define client01H
/****************************************************************************/
//Headers
#include "client.h"

//Defines

//Defines the size of a window button (e.g. close, minimize, maximize, restore)
#define BUTTON_SIZE  16

//Structures

//tStdLabel is a generic descendent of tStatic for drawing a string of text with
//a particular font and color.
class tStdLabel : public tStatic
{
    protected:
    tString caption;        //String that will appear on the screen
    int fontIndex;          //Font with which to draw string
    float r, g, b;          //Color with which to draw string
    int cx,cy;
    bool centered;

    public:
    //Data retrieval functions:
    const tString& getCaption() { return caption; }

    void setCaption(const tString& _caption);
    void setSize(int x, int y);
    void drawControl();

    tStdLabel(int controlX, int controlY, const tString& _caption, int _fontIndex,
        float _r, float _g, float _b);
    tStdLabel(int controlX, int controlY, int width, int height, const tString& _caption, int _fontIndex,
        float _r, float _g, float _b);
};

/* tStdButton provides a standard implementation of tButton.  The class draws
 * cyan text on a black background when the button isn't selected.  The class
 * draws white text on a black background when the button is selected or depressed.
 * The class branches to a custom event handler when the user executes the button.
 */
class tStdButton : public tButton
{
    protected:
    tEvent event;       //Event to call when executed
    int fontIndex;      //Font with which to draw caption
    tString caption;    //Caption
    int captionX;       //Caption's offset from lower-left corner of control
    int captionY;       //"

    protected:
    void onSetSize(int x, int y);
    void drawControl();
    void onExecute();

    public:
    void refresh();
    void setCaption(const tString& _caption);
    void setEvent(tEvent _event);

    tStdButton(int controlX, int controlY, int width, int height, const tString& _caption,
        int _fontIndex, bool canFocus, tEvent _event);
};

/* tStdToggle provides a standard implementation of tToggle.  The class draws
 * a white square beside a caption.  The square contains an 'X' when checked
 * and the square brightens when the control is selected or focused.
 */
class tStdToggle : public tToggle
{
    private:
    int fontIndex;      //Font with which to draw caption
    tString caption;    //Caption beside toggle box
    float r, g, b;      //Color with which to draw caption
    tEvent event;

    protected:
    void drawControl();
    void onChange() { if(event && getParent()->isType(CTRL_WINDOW)) (((tWindow *)getParent())->*event)(this); }
    
    public:
    void setCaption(const tString& _caption);
    void setSize(int x, int y);

    tStdToggle(int controlX, int controlY, const tString& _caption, int _fontIndex,
        float _r, float _g, float _b, bool canFocus, bool isChecked, tEvent _event=NULL);
};

/* tStdRadio provides a standard implementation of tRadio.  The class draws
 * a white ring beside a caption.  The ring contains a filled circle when
 * checked and the ring brightens when the control is selected or focused.
 */
class tStdRadio : public tRadio
{
    private:
    int fontIndex;          //Font with which to draw caption
    tString caption;        //Caption beside radio box
    float r, g, b;          //Color with which to draw caption
    GLUquadricObj *quad;    //Quadric object that OpenGL uses to render ring
    tEvent event;

    protected:
    void drawControl();
    void onChange() { if(event && getParent()->isType(CTRL_WINDOW)) (((tWindow *)getParent())->*event)(this); }

    public:
    void setCaption(const tString& _caption);
    void setSize(int x, int y);

    tStdRadio(int controlX, int controlY, const tString& _caption, int _fontIndex,
        float _r, float _g, float _b, bool canFocus, bool isChecked, int group, tEvent _event=NULL);
    ~tStdRadio();
};

/* tStdComboBox provides a standard implementation of tComboBox.  It highlights
 * the selected choice and draws a triangle near the right edge.  The triangle
 * indicates that the user can expand the list by clicking anywhere inside the box.
 */
class tStdComboBox : public tComboBox
{
    private:
    tEvent event;

    protected:
    void drawContents();
    void onChange() { if(event && getParent()->isType(CTRL_WINDOW)) (((tWindow *)getParent())->*event)(this); }

    public:
    tStdComboBox(int controlX, int controlY, int width, bool canFocus,
        const tString& choices, int choice, int dropDownHeight,
        bool dropDownVertScrollBar, bool dropDownHorScrollBar, int fontIndex,
        tEvent _event=NULL);
};

/* tStdDialogBox provides a standard implementation of tDialogBox.  It draws
 * the window's title on a blue background (if active) or a gray background
 * (if inactive).  It fills the client area with gray and surrounds the whole
 * control with a white border.  It also draws a white divider between the title
 * and client area.
 */
class tStdDialogBox : public tDialogBox
{
    private:
    int fontIndex;          //Font with which to render title
    tString caption;        //Window's title
    tString visibleCaption; //Visible title; usually equals caption except when the window isn't wide
                                //enough to contain full caption.
    int cx, cy;             //Title's offset from the control's lower-left corner

    void adjustCaption();

    public:
    void drawWindow();
    void onSetSize(int w, int h);
    void setCaption(const tString& _caption);

    tStdDialogBox(const tString& _caption, int _fontIndex, int controlX, int controlY, int width, int height,
        eWindowState state, bool modal, bool destroyOnClose, bool moveable, bool resizeable,
        bool maximizeable, bool minimizeable, bool closeable, int minWidth, int maxWidth, int minHeight,
        int maxHeight);
    tStdDialogBox(const tString& _caption, int _fontIndex, int width, int height,
        eWindowState state, bool modal, bool destroyOnClose, bool moveable, bool resizeable,
        bool maximizeable, bool minimizeable, bool closeable, int minWidth, int maxWidth, int minHeight,
        int maxHeight);
};
/****************************************************************************/
#endif
