/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef behaviorH
#define behaviorH

#include "engine.h"

enum eBehaviorName { BEHAVIOR_CONSTRUCTION, BEHAVIOR_DIVISION, BEHAVIOR_EXTRACTION,
    BEHAVIOR_PRODUCTION, BEHAVIOR_GENERIC, BEHAVIOR_GENERIC_CONSUMPTION, BEHAVIOR_STORAGE,
    BEHAVIOR_DETECTION, BEHAVIOR_COLLECTION, BEHAVIOR_WEAPON };

class tBehavior
{
    protected:
    tUnit*              parent;

    public:
    virtual void        intervalTick(tWorldTime t) {}
    virtual void        onBirth(tWorldTime t) {}
    virtual void        onDeath(tWorldTime t) {}
    virtual void        onTransfer(int newOwner, tWorldTime t) {}
    virtual void        onConception(tWorldTime t) {}
    virtual void        onCommand(eCommand command, tWorldTime t) {}
    virtual void        onCommand(eCommand command, const tVector2D& pos, tWorldTime t) {}
    virtual void        onCommand(eCommand command, tUnit *obj, tWorldTime t) {}
    virtual void        onCommand(eCommand command, int quantity, bool repeat, tWorldTime t) {}
    virtual void        onSelect() {}
    virtual void        onDeselect() {}
    virtual void        onDepositDepleted(tDeposit *d, tWorldTime t) {}
    virtual void        onUnitDestroyed(tUnit *u, tWorldTime t) {}
    virtual void        onClearOrders(tWorldTime t) {}
    virtual void        onDamage(tUnit *u, float amt, tWorldTime t) {}
    virtual bool        canExecute(eCommand command) { return false; }
    virtual bool        isFiringAt(tUnit *u) { return false; }
    virtual eCommand    getDefaultCommand() { return COMMAND_NONE; }
    virtual eCommand    getDefaultCommand(tUnit *u) { return COMMAND_NONE; }

    virtual ePriority   getLeader(tUnit*& u, float& distance, bool& overshoot) { return PRIORITY_NONE; }
    virtual ePriority   getDestination(tVector2D& v, float& distance, bool& overshoot) { return PRIORITY_NONE; }
    virtual float       getAttackRange() { return 0; }
    virtual void        dumpCommands() {}

	virtual void        postDraw() {}

    tBehavior(tUnit *_parent) : parent(_parent) {}
    //DO NOT REMOTE THIS LINE otherwise the compiler will not call destructor of descendents.
    virtual ~tBehavior() {}
};

struct tBehaviorType
{
    public:
    virtual void        lookupUnitTypes() throw(tString) {}
    virtual tBehavior*  createBehavior(tUnit *parent)=0;

    //DO NOT REMOTE THIS LINE otherwise the compiler will not call destructor of descendents.
    virtual ~tBehaviorType() {}
};

struct tConstructionType : public tBehaviorType
{
    float               range;
    vector<tModelVectorType*> repairOriginTypes;

    tStringList         names; //Temporary array of unit types that can be constructed by this unit
    bool                constructionOptions[MAX_COMMANDS];

    tArgumentType<float>repairRateArg;
    tArgumentType<float>disassembleRateArg;
    tProcedureType*     onEngageProc;
    tProcedureType*     onDisengageProc;
    tProcedureType*     onBeginRepairingProc;
    tProcedureType*     onBeginDisassemblingProc;
    tProcedureType*     onEndRepairingProc;
    tProcedureType*     onEndDisassemblingProc;

    tModelVectorType*   primaryRepairOriginType;
    tModelVectorType*   primaryRepairDirectionType;
    bool                limitAngle;
    float               cosTheta;
    tAimProcedureType*  aimProcedureType;
    tArgumentType<bool> canAimArg;

    void                lookupUnitTypes() throw(tString);
    tBehavior*          createBehavior(tUnit *parent);
    tConstructionType(tUnitType *parent, tStream& s) throw(tString);
};

class tConstructionBehavior : public tBehavior
{
    private:
    tConstructionType*  type;
    eCommand            curCommand;
    vector<tUnit *>     targets;
    bool                workingOnPrimary;
    bool                repairingSecondary;
    tUnit*              primary;
    tUnit*              secondary;
    bool                autoRepair;
    float               repairRate;
    float               disassembleRate;
    bool                aiming;

    void                setAiming(bool _aiming, tUnit *u, tWorldTime t);
    bool                withinRange(tUnit *u, const tVector3D& origin) //Is this unit close enough to repair/disassemble?
                            { return (u->getCenter()-origin).lengthSquared() <=
								type->range*type->range; }
    bool                withinAngle(tUnit *u, const tVector3D& origin, const tVector3D& direction);
    bool                isUnitFriendly(tUnit *u);
    void                setWorkingOnPrimary(bool _working, tWorldTime t);
    void                setRepairingSecondary(bool _repairing, tWorldTime t);
    void                setAutoRepair(bool _autoRepair);
    void                setPrimary(tUnit *_primary, tWorldTime t);
    void                setSecondary(tUnit *_secondary, tWorldTime t);
    void                setCurCommand(eCommand command, tWorldTime t);

    bool                canWorkOn(tUnit *target);
    bool                canRepair(tUnit *target);
    bool                canDisassemble(tUnit *target);

    void                onBeginRepairing(tUnit *u, tWorldTime t);
    void                onEndRepairing(tUnit *u, tWorldTime t);
    void                onBeginDisassembling(tUnit *u, tWorldTime t);
    void                onEndDisassembling(tUnit *u, tWorldTime t);

    void                setRepairRate(tUnit *u, float _repairRate, tWorldTime t);
    void                setDisassembleRate(tUnit *u, float _disassembleRate, tWorldTime t);

    public:
    eCommand            getDefaultCommand(tUnit *u);
    void                onCommand(eCommand command, tUnit *u, tWorldTime t);
    void                onCommand(eCommand command, tWorldTime t);
    bool                canExecute(eCommand command);
    void                intervalTick(tWorldTime t);
    void                onClearOrders(tWorldTime t);
    void                onUnitDestroyed(tUnit *u, tWorldTime t);
    void                onTransfer(int newOwner, tWorldTime t);
    void                onSelect();
    void                onDeselect();
    ePriority           getLeader(tUnit*& u, float& distance, bool& overshoot);
    void                postDraw();
    tConstructionBehavior(tConstructionType *_type, tUnit *parent) : tBehavior(parent), type(_type),
        curCommand(COMMAND_NONE), workingOnPrimary(false), repairingSecondary(false), primary(NULL),
        secondary(NULL), autoRepair(true), repairRate(0), disassembleRate(0), aiming(false) {}
};

struct tDivisionType : public tBehaviorType
{
    int             resourceConsumed;
    float           consumptionRate;

    tArgumentType<bool> divisionCompleteArg;
    tProcedureType* onInitiateDivisionProc;
    tProcedureType* onTerminateDivisionProc;
    tProcedureType* onBeginDivisionProc;
    tProcedureType* onEndDivisionProc;
    tReferenceFrameType* spawn1BasisType;
    tReferenceFrameType* spawn2BasisType;

    tDivisionType(tUnitType *parent, tStream& s) throw(tString);
    tBehavior*      createBehavior(tUnit *parent);
};

class tDivisionBehavior : public tBehavior
{
    private:
    tDivisionType*  type;
    bool            divisionStarted;    //Has cell division commenced?
    bool            dividing;           //Is the cell dividing at this moment?

    void            setDividing(bool _dividing, tWorldTime t);
    void            initiateDivision(tWorldTime t);
    void            terminateDivision(tWorldTime t);

    public:
    void            intervalTick(tWorldTime t);
    void            onDeath(tWorldTime t);
    void            onTransfer(int newOwner, tWorldTime t);
    bool            canExecute(eCommand command);
    void            onCommand(eCommand command, tWorldTime t);
    tDivisionBehavior(tDivisionType *_type, tUnit *parent);
};

struct tExtractionType : public tBehaviorType
{
    int resourceExtracted;
    tArgumentType<float> extractionRateArg;
    int resourceConsumed;
    tArgumentType<float> consumptionRateArg;
    tProcedureType* onInitiateExtractionProc;
    tProcedureType* onTerminateExtractionProc;
    tProcedureType* onBeginExtractionProc;
    tProcedureType* onEndExtractionProc;

    tExtractionType(tUnitType *parent, tStream& s) throw(tString);
    tBehavior*          createBehavior(tUnit *parent);
};

class tExtractionBehavior : public tBehavior
{
    private:
    tExtractionType*    type;
    bool                turnedOn;           //Has the user turned the unit on?
    bool                running;            //Is the unit currently producing energy?
    tDeposit*           deposit;
    float               consumptionRate;
    float               extractionRate;

    void                setRunning(bool _running, tWorldTime t);
    void                linkDeposit(tWorldTime t);
    void                unlinkDeposit(tWorldTime t);
    bool                canExtract();
    bool                canExtract(tDeposit *d);
    void                setConsumptionRate(float _consumptionRate);
    void                setExtractionRate(float _extractionRate);

    public:
    bool                canExecute(eCommand command);
    void                onCommand(eCommand command, tWorldTime t);
    void                intervalTick(tWorldTime t);
    void                onBirth(tWorldTime t);
    void                onDeath(tWorldTime t);
    void                onTransfer(int newOwner, tWorldTime t);
    void                onDepositDepleted(tDeposit *d, tWorldTime t);
    void                onSelect();
    void                onDeselect();
    tExtractionBehavior(tExtractionType *_type, tUnit *parent) : tBehavior(parent),
        type(_type), turnedOn(true), running(false), deposit(NULL), consumptionRate(0),
        extractionRate(0) {}
};

//Contains information about one production order.  A production order
//may call for multiple unit as long as they belong to the same type.
//If repeat is true, the factory will ignore quantity repeatedly produce the unit.
struct tProduct
{
    eCommand    command;
    int         quantity;
    bool        repeat;
};

struct tProductionType : public tBehaviorType
{
	vector<tModelVectorType*> repairOriginTypes;
    tStringList         names; //Temporary array of unit types that can be produced by this unit
    bool                productionOptions[MAX_COMMANDS];
    tArgumentType<float>productionRateArg;
	tArgumentType<bool>	readyForNextUnitArg;
    tProcedureType*     onInitiateProductionProc;
    tProcedureType*     onCompleteProductionProc;
	tProcedureType*		onCancelProductionProc;
    tProcedureType*     onBeginProductionProc;
    tProcedureType*     onEndProductionProc;
	tReferenceFrameType*spawnBasisType;

    void                lookupUnitTypes() throw(tString);
    tProductionType(tUnitType *parent, tStream& s) throw(tString);
    tBehavior*          createBehavior(tUnit *parent);
};

class tProductionBehavior : public tBehavior
{
    private:
    tProductionType*    type;
    vector<tProduct>    productionQueue;
    eCommand            curProduct;
    bool                producing;
    bool                productionPaused;   //Is production paused?
    tUnit*              product;            //Unit which this unit is producing
    float               productionRate;
    bool                productionComplete;

    void                clearProductionQueue(tWorldTime t);
    void                onProduce(eCommand command, tWorldTime t);
    void                cancelProduction(tWorldTime t);
    void                loadNextProduct(tWorldTime t);
    void                setCurProduct(eCommand command, tWorldTime t);
    void                setProducing(bool _producing, tWorldTime t);
    bool                canProduce(eCommand command);
	void                setProductionRate(float _productionRate, tWorldTime t);

    public:
    bool                canExecute(eCommand command);
    void                onCommand(eCommand command, tWorldTime t);
    void                onCommand(eCommand command, int quantity, bool repeat, tWorldTime t);
    void                onSelect();
    void                onDeselect();
    void                onBeginProduction(eCommand command, tWorldTime t);
    void                onEndProduction(eCommand command, tWorldTime t);
    void                cancelProduction(eCommand command, tWorldTime t);
    void                intervalTick(tWorldTime t);
    void                onDeath(tWorldTime t);
    void                onTransfer(int newOwner, tWorldTime t);
    void                onUnitDestroyed(tUnit *u, tWorldTime t);
    void                dumpCommands();
    void                postDraw();
    tProductionBehavior(tProductionType *_type, tUnit *parent) : tBehavior(parent),
        type(_type), curProduct(COMMAND_NONE), producing(false), productionPaused(false),
        product(NULL), productionRate(0), productionComplete(false) {}
};

struct tGenericType : public tBehaviorType
{
    tProcedureType*     onStartProc;
    tProcedureType*     onStopProc;
    eCommand			startCommand;
	eCommand			stopCommand;

    tGenericType(tUnitType *parent, tStream& s) throw(tString);
    tBehavior*          createBehavior(tUnit *parent);
};

class tGenericBehavior : public tBehavior
{
    private:
    tGenericType*    	type;
    bool                canStart;
    bool                canStop;

    public:
    bool                canExecute(eCommand command);
    void                onCommand(eCommand command, tWorldTime t);
    tGenericBehavior(tGenericType *_type, tUnit *parent)
        : tBehavior(parent), type(_type), canStart(true), canStop(false) {}
};

struct tGenericConsumptionType : public tBehaviorType
{
    int                 resourceConsumed;
    tArgumentType<float>consumptionRateArg;
    tProcedureType*     onStartProc;
    tProcedureType*     onStopProc;
    tProcedureType*     onInitiateProc;
    tProcedureType*     onTerminateProc;
	eCommand			startCommand;
	eCommand			stopCommand;

    tGenericConsumptionType(tUnitType *parent, tStream& s) throw(tString);
    tBehavior*          createBehavior(tUnit *parent);
};

class tGenericConsumptionBehavior : public tBehavior
{
    private:
    tGenericConsumptionType* type;
    bool                turnedOn;       //Does the player want the unit to be cloaked?
    float               consumptionRate;
    bool                running;

    void                setConsumptionRate(float _consumptionRate);
    void                setRunning(bool _running, tWorldTime t);
    void                update(tWorldTime t);

    public:
    bool                canExecute(eCommand command);
    void                onCommand(eCommand command, tWorldTime t);
    void                intervalTick(tWorldTime t);
    void                onTransfer(int newOwner, tWorldTime t);
    void                onDeath(tWorldTime t);
    tGenericConsumptionBehavior(tGenericConsumptionType *_type, tUnit *parent);
};

struct tStorageType : public tBehaviorType
{
    int                 resourceStored;
    float               capacity;
    int                 resourceConsumed;
    float               consumptionRate;
    tProcedureType*     onBeginStorageProc;
    tProcedureType*     onEndStorageProc;

    tStorageType(tUnitType *parent, tStream& s) throw(tString);
    tBehavior*          createBehavior(tUnit *parent);
};

class tStorageBehavior : public tBehavior
{
    private:
    tStorageType*   type;
    bool            running;            //Is the unit currently storing resources?

    void            setRunning(bool _running, tWorldTime t);
    void            update(tWorldTime t);

    public:
    void            onBirth(tWorldTime t);
    void            onDeath(tWorldTime t);
    void            onTransfer(int newOwner, tWorldTime t);
    void            intervalTick(tWorldTime t);
    tStorageBehavior(tStorageType *_type, tUnit *parent) : tBehavior(parent),
        type(_type), running(false) {}
};

struct tDetectionType : public tBehaviorType
{
    int                 resourceConsumed;
    tArgumentType<float>consumptionRateArg;
    tArgumentType<float>fieldOfViewArg;
    tProcedureType*     onInitiateDetectionProc;
    tProcedureType*     onTerminateDetectionProc;
    tProcedureType*     onBeginDetectionProc;
    tProcedureType*     onEndDetectionProc;

    tDetectionType(tUnitType *parent, tStream& s) throw(tString);
    tBehavior*          createBehavior(tUnit *parent);
};

class tDetectionBehavior : public tBehavior
{
    private:
    tDetectionType* type;
    bool            turnedOn;           //Has the user turned the unit on?
    bool            running;            //Is the unit currently detecting?
    float           consumptionRate;
    float           fieldOfView;

    void            setRunning(bool _running, tWorldTime t);
    void            update(tWorldTime t);
    void            setConsumptionRate(float _consumptionRate);

    public:
    bool            canExecute(eCommand command);
    void            onCommand(eCommand command, tWorldTime t);
    void            intervalTick(tWorldTime t);
    void            onBirth(tWorldTime t);
    void            onDeath(tWorldTime t);
    void            onTransfer(int newOwner, tWorldTime t);
    void            onSelect();
    void            onDeselect();
    tDetectionBehavior(tDetectionType *_type, tUnit *parent) : tBehavior(parent),
        type(_type), turnedOn(true), running(false), consumptionRate(0), fieldOfView(0) {}
};

struct tCollectionType : public tBehaviorType
{
    int                 resourceCollected;
    tArgumentType<float>collectionRateArg;
    int                 resourceConsumed;
    tArgumentType<float>consumptionRateArg;

    tProcedureType*     onInitiateCollectionProc;
    tProcedureType*     onTerminateCollectionProc;
    tProcedureType*     onBeginCollectionProc;
    tProcedureType*     onEndCollectionProc;

    tCollectionType(tUnitType *parent, tStream& s) throw(tString);
    tBehavior*          createBehavior(tUnit *parent);
};

class tCollectionBehavior : public tBehavior
{
    private:
    tCollectionType*    type;
    bool                turnedOn;           //Has the user turned the unit on?
    bool                running;            //Is the unit currently producing energy?
    float               collectionRate;
    float               consumptionRate;

    void                setRunning(bool _running, tWorldTime t);
    void                update(tWorldTime t);
    void                setCollectionRate(float _collectionRate);
    void                setConsumptionRate(float _consumptionRate);

    public:
    bool                canExecute(eCommand command);
    void                onCommand(eCommand command, tWorldTime t);
    void                intervalTick(tWorldTime t);
    void                onBirth(tWorldTime t);
    void                onDeath(tWorldTime t);
    void                onTransfer(int newOwner, tWorldTime t);
    void                onSelect();
    void                onDeselect();
    tCollectionBehavior(tCollectionType *_type, tUnit *parent) : tBehavior(parent),
        type(_type), turnedOn(true), running(false), collectionRate(0), consumptionRate(0) {}
};

struct tSalvo
{
    vector<tModelVectorType*> missileOrigins;
    tProcedureType *onFireProc;
    tSalvo() : onFireProc(NULL) {}
};

struct tWeaponType : public tBehaviorType
{
    tMissileType*   missileType;
    float           coolDown;
    int             resourceConsumed;
    float           cost;
    float           range;
    tAimProcedureType* aimProcedureType;
    tArgumentType<bool> canFireArg;
    tArgumentType<bool> canAimArg;
    tProcedureType* onEngageProc;
    tProcedureType* onDisengageProc;
    tProcedureType* onTargetAcquiredProc;
    tProcedureType* onTargetLostProc;
    tProcedureType* onFireProc;
    bool            limitAngle;
    float           cosTheta;
    vector<tSalvo>  salvos;
    tModelVectorType* missileOriginType;
    tModelVectorType* missileDirectionType;

    void parseSalvo(tUnitType *parent, tStream& s) throw(tString);
    tWeaponType(tUnitType *parent, tStream& s) throw(tString);
    tBehavior*      createBehavior(tUnit *parent);
};

class tWeaponBehavior : public tBehavior
{
    private:
    tWeaponType*    type;
    bool            firingAtPrimary;
    bool            firingAtSecondary;
    bool            aiming;

    tUnit*          primary;    //Reason for having primary and secondary:
    tUnit*          secondary;  //You want to move towards a primary target while engaging a secondary target.
    vector<tUnit*>  targets;
    eCommand        combatMode;
    bool            hasTargets;

    tWorldTime      nextRound;
    int             curSalvo;
	tVector3D		missileOrigin;
	tVector3D	    missileDirection;

    bool            withinRange(tUnit *u);
    bool            withinAngle(tUnit *u);
	bool            isUnitFriendly(tUnit *u);
    void            setFiringAtPrimary(bool _firing, tWorldTime t);
    void            setFiringAtSecondary(bool _firing, tWorldTime t);
    void            setCombatMode(eCommand _combatMode);
    void            setHasTargets(bool _hasTargets, tWorldTime t);
    bool            canAttack(tUnit *u) { return parent->isAbilityEnabled(ABILITY_ATTACK) && u->isVulnerable() &&
        (type->resourceConsumed==-1 || isResourceAvailable(parent->getOwner(), type->resourceConsumed,type->cost)); }
    void            setAiming(bool _aiming, tUnit *u, tWorldTime t);
    void            setPrimary(tUnit *_primary, tWorldTime t);
    void            setSecondary(tUnit *_secondary, tWorldTime t);

    protected:
    bool            isFiringAtPrimary() { return firingAtPrimary; }
    bool            isFiringAtSecondary() { return firingAtSecondary; }
    tUnit*          getPrimary() { return primary; }
    tUnit*          getSecondary() { return secondary; }

    void            onBeginFiring(bool primary, tWorldTime t);
    void            onEndFiring(bool primary, tWorldTime t);

    public:
    float           getAttackRange() { return type->range; }
    void            onCommand(eCommand command, tUnit *u, tWorldTime t);
    void            onCommand(eCommand command, tWorldTime t);
    void            onDamage(tUnit *u, float amt, tWorldTime t);
    void            onClearOrders(tWorldTime t);
    void            onUnitDestroyed(tUnit *u, tWorldTime t);
    void            intervalTick(tWorldTime t);
    bool            isFiringAt(tUnit *u);
    eCommand        getDefaultCommand(tUnit *u);
    bool            canExecute(eCommand command);
    void            onSelect();
    void            onDeselect();
    void            onTransfer(int newOwner, tWorldTime t);
    ePriority       getLeader(tUnit*& u, float& distance, bool& overshoot);
    tWeaponBehavior(tWeaponType *_type, tUnit *parent) : tBehavior(parent), type(_type),
        firingAtPrimary(false), firingAtSecondary(false),
        primary(NULL), secondary(NULL), combatMode(COMMAND_FIRE_AT_WILL), hasTargets(false),
        aiming(false), curSalvo(0), nextRound(0), missileOrigin(zeroVector3D),
		missileDirection(zeroVector3D) {}
};

class tMovementBehavior : public tBehavior
{
    private:
    tUnit*          leader;             //Which unit are we following?
    vector<tUnit *> leaders;            //Leaders
    tVector2D       destination;        //Which point are we seeking?
    eCommand        curCommand;         //COMMAND_MOVE, COMMAND_PATROL, or COMMAND_FOLLOW

    void            setCurCommand(eCommand command, tWorldTime t);

    public:
    void            onCommand(eCommand command, tUnit *u, tWorldTime t);
    void            onCommand(eCommand command, const tVector2D& v, tWorldTime t);
    void            onClearOrders(tWorldTime t);
    void            onUnitDestroyed(tUnit *u, tWorldTime t);
    void            intervalTick(tWorldTime t);
    eCommand        getDefaultCommand(tUnit *u);
    eCommand        getDefaultCommand();
    bool            canExecute(eCommand command);
    ePriority       getLeader(tUnit*& u, float& distance, bool& overshoot);
    ePriority       getDestination(tVector2D& v, float& distance, bool& overshoot);
    tMovementBehavior(tUnit *parent) : tBehavior(parent), leader(NULL), destination(zeroVector2D),
        curCommand(COMMAND_NONE) {}
};

extern tBehaviorType *parseBehaviorTypeBlock(tUnitType *parent, tStream& s) throw(tString);
/****************************************************************************/
#endif
