//---------------------------------------------------------------------------
#ifndef texmngr1H
#define texmngr1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include <Dialogs.hpp>
#include <ExtDlgs.hpp>
//---------------------------------------------------------------------------
#define MAX(x,y)                ((x) > (y) ? (x) : (y))
#define MIN(x,y)                ((x) < (y) ? (x) : (y))

#define RESIZE_LEFT             1
#define RESIZE_BOTTOM           2
#define RESIZE_RIGHT            4
#define RESIZE_TOP              8

//iDisplayList holds an index to an OpenGL display list.
typedef GLuint iDisplayList;

//iTextureIndex holds an index to an OpenGL texture.
typedef GLuint iTextureIndex;

//iImageIndex holds an ILUT image name.
typedef ILuint iImageIndex;

//Rounds a number to the nearest whole number
#define ROUND(x)                (int)((x)+0.5)

typedef std::string tString;

template<class A> inline void clampRange(A& a, A x, A y)
{
    if(a<x) a=x;
    if(a>y) a=y;
}

template <class T>
  inline void swap (T& a, T& b)
  {
    T tmp = a;
    a = b;
    b = tmp;
  }

struct tTexture
{
    tString name;
    int left;
    int bottom;
    int width;
    int height;
    int hotspotX;
    int hotspotY;
    bool alphaTesting;
    bool alphaBlending;

    tTexture() : left(0), bottom(0), width(0), height(0), hotspotX(0), hotspotY(0),
        alphaTesting(false), alphaBlending(false) {}
    tTexture(const tString& _name, int _left, int _bottom, int _width, int _height,
        int _hotspotX, int _hotspotY, bool _alphaTesting, bool _alphaBlending) :
        name(_name), left(_left), bottom(_bottom), width(_width), height(_height),
        hotspotX(_hotspotX), hotspotY(_hotspotY), alphaTesting(_alphaTesting),
        alphaBlending(_alphaBlending) {}
    tTexture(const char *_name, int _left, int _bottom, int _width, int _height,
        int _hotspotX, int _hotspotY, bool _alphaTesting, bool _alphaBlending) :
        name(_name), left(_left), bottom(_bottom), width(_width), height(_height),
        hotspotX(_hotspotX), hotspotY(_hotspotY), alphaTesting(_alphaTesting),
        alphaBlending(_alphaBlending) {}
};

class TMain : public TForm
{
__published:	// IDE-managed Components
    TPanel *paView;
    TScrollBar *sbHorizontal;
    TScrollBar *sbVertical;
    TTrackBar *tbZoom;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label3;
    TLabel *Label4;
    TLabel *Label5;
    TLabel *Label6;
    TComboBox *cbBackground;
    TLabel *Label7;
    TPanel *Panel1;
    TLabel *Label8;
    TEdit *edName;
    TLabel *Label9;
    TEdit *edLeft;
    TEdit *edBottom;
    TLabel *Label10;
    TLabel *Label11;
    TEdit *edWidth;
    TLabel *Label12;
    TEdit *edHeight;
    TGroupBox *gbHotspot;
    TLabel *Label13;
    TEdit *edHotspotX;
    TLabel *Label14;
    TEdit *edHotspotY;
    TCheckBox *cbAlphaTesting;
    TCheckBox *cbAlphaBlending;
    TListBox *lbTextures;
    TMainMenu *mmMainMenu;
    TMenuItem *miQuit;
    TButton *btNewTexture;
    TButton *btRemoveTexture;
    TCheckBox *cbTexturesVisible;
    TCheckBox *cbHotspotsVisible;
    TButton *btDrawRectangle;
    TButton *btPositionHotspot;
    TButton *btDuplicate;
    TButton *btDuplicateLeft;
    TButton *btDuplicateUp;
    TButton *btDuplicateRight;
    TButton *btDuplicateDown;
    TLabel *Label15;
    TButton *btSortTextures;
    TPanel *Panel2;
    TMenuItem *miSelectImage;
    TOpenDialog *odSelectImage;
    TStatusBar *sbStatusBar;
    TOpenDialog *odOpenTexturePack;
    TSaveDialog *sdSaveTexturePack;
    TMenuItem *miSetWorkingDirectory;
    TMenuItem *miFile;
    TMenuItem *miNew;
    TMenuItem *miOpen;
    TMenuItem *miSave;
    TMenuItem *miSaveAs;
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall paViewResize(TObject *Sender);
    void __fastcall paViewMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
    void __fastcall sbScroll(TObject *Sender,
          TScrollCode ScrollCode, int &ScrollPos);
    void __fastcall tbZoomChange(TObject *Sender);
    void __fastcall cbBackgroundChange(TObject *Sender);
    void __fastcall btNewTextureClick(TObject *Sender);
    void __fastcall btDuplicateClick(TObject *Sender);
    void __fastcall lbTexturesClick(TObject *Sender);
    void __fastcall btDuplicateUpClick(TObject *Sender);
    void __fastcall btDuplicateLeftClick(TObject *Sender);
    void __fastcall btDuplicateRightClick(TObject *Sender);
    void __fastcall btDuplicateDownClick(TObject *Sender);
    void __fastcall btRemoveTextureClick(TObject *Sender);
    void __fastcall edNameChange(TObject *Sender);
    void __fastcall edLeftChange(TObject *Sender);
    void __fastcall edBottomChange(TObject *Sender);
    void __fastcall edWidthChange(TObject *Sender);
    void __fastcall edHeightChange(TObject *Sender);
    void __fastcall edHotspotXChange(TObject *Sender);
    void __fastcall edHotspotYChange(TObject *Sender);
    void __fastcall cbAlphaTestingClick(TObject *Sender);
    void __fastcall cbAlphaBlendingClick(TObject *Sender);
    void __fastcall cbTexturesVisibleClick(TObject *Sender);
    void __fastcall cbHotspotsVisibleClick(TObject *Sender);
    void __fastcall btSortTexturesClick(TObject *Sender);
    void __fastcall lbTexturesKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
    void __fastcall FormPaint(TObject *Sender);
    void __fastcall btDrawRectangleClick(TObject *Sender);
    void __fastcall paViewMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    void __fastcall paViewMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    void __fastcall btPositionHotspotClick(TObject *Sender);
    void __fastcall miSelectImageClick(TObject *Sender);
    void __fastcall miQuitClick(TObject *Sender);
    void __fastcall miNewClick(TObject *Sender);
    void __fastcall miOpenClick(TObject *Sender);
    void __fastcall miSaveClick(TObject *Sender);
    void __fastcall miSaveAsClick(TObject *Sender);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
    void __fastcall miSetWorkingDirectoryClick(TObject *Sender);
private:	// User declarations
    HDC hdc;
    HGLRC hrc;
    int canvasWidth;
    int canvasHeight;
    int viewWidth;
    int viewHeight;
    int viewX;
    int viewY;
    bool needsRedraw;
    bool imageLoaded;
    tString imageFile;
    int imageWidth;
    int imageHeight;
    iTextureIndex imageTexture;
    int imageScale;
    float backgroundColor[3];
    bool texturesVisible;
    bool hotspotsVisible;
    tTexture *highlightedTexture;
    bool eventMask;
    bool readyToDrawRectangle;
    bool drawingRectangle;
    bool drawingBoundingBox;
    int rectangleX1;
    int rectangleY1;
    int rectangleX2;
    int rectangleY2;
    bool positioningHotspot;
    int resizeMask;
    bool resizing;
    int lastMouseX;
    int lastMouseY;
    bool postSelect;
    bool modified;
    tString currentFile;
    tString workingDirectory;

    void setPixelFormatDescriptor();
    void setupCanvas();
    void drawCanvas();
    void destroyCanvas();
    void updateProjection();
    void clampViewXY();
    void updateCanvasSize();
    bool loadImage();
    void addTexture(tTexture *newTexture);
    tString incrementNumber(const tString& s);
    void imageToCanvasCoords(int ix, int iy, int& cx, int& cy);
    void canvasToImageCoords(int cx, int cy, int& ix, int& iy);
    tTexture *findTextureAt(int x, int y);
    void resetMouseState();
    void clearSelection();
    void updateHighlightedTexture(int canvasX, int canvasY);
    void invertSelection(tTexture *texture);
    bool isSelected(tTexture *texture);
    void __fastcall IdleLoop(TObject*, bool& done);
    void setModified(bool _modified);
    void setCurrentFile(const tString& _currentFile);
    void setImageFile(const tString& _imageFile);
    void setHighlightedTexture(tTexture *_highlightedTexture);

    void writeString(const tString& string, FILE *fp);
    tString readString(FILE *fp);
    void newTexturePack();
    bool saveTexturePack(const char *filename);
    bool loadTexturePack(const char *filename);
    bool saveChangesQuery();
    void loadWorkingDirectory();
    bool saveWorkingDirectory(const tString& workingDirectory);
    void selectAllTextures();
    tString trimPath(const tString& s);
    bool isPowerOf2(int x);
public:		// User declarations
    __fastcall TMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMain *Main;
//---------------------------------------------------------------------------
#endif
