/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef typesH
#define typesH
/****************************************************************************
                         Index of Compiler Switches
 ****************************************************************************
 *
 *   BORLAND:       Compile on Borland C++ Builder
 *   VISUALC:       Compile on Microsoft Visual C++ [default]
 *   WINDOWS:       Compile on Windows [default]
 *   LINUX:         Compile on Linux
 *   LOG:           Output general log messages to output.txt
 *   DEBUG:         Output debug information to output.txt
 *   DEBUGUDP:      Output UDP debug information to output.txt
 *   PACKETLOSS:    Simulates UDP packet loss
 *   MEMWATCH:      Watches for memory leaks and illegal delete operations
 *   LOGCOMMAND:    Log all commands during gameplay
 *   CACHE_HEADERS: This option tells the compiler to use the same block of headers
 *                  for each source file.  The option significantly decreases
 *                  compilation time for compilers that support '#pragma hdrstop'
 *   SGI_AUTO_PTR:  I was having problems with Borland's implementation of auto_ptr,
 *                  so I downloaded another implementation off of the net.
 *
 * glScissor doesn't work on some systems, but a combination of glViewport and glOrtho does.
 * Therefore, I added a compiler option for switching between the two methods
 *   SCISSOR:   Use glScissor to restrict drawing area [default]
 *   VIEWPORT:  Use a combination of glViewport and glOrtho to restrict drawing area
 *   STENCIL_MASK: Use the stencil buffer to restrict drawing area
 ****************************************************************************/

//Includes
#include <GL/glfw.h>
#include <IL/il.h>
#ifndef CACHE_HEADERS
    #include <time.h> //for time_t
    #include <string> //for string
    #include <stdio.h> //for FILE
    #ifdef BORLAND
        #include <vector.h> //for vector
        #include <list.h> //for list
    #else
        #include <vector> //for vector
        #include <list> //for list
    #endif
#endif

//Defines
#ifdef BORLAND
    //Disable the following warning messages:
    #pragma option -w-sig //Conversion may lose significant digits
    #pragma option -w-par //Param never used
    #pragma option -w-csu //Comparing signed and unsigned values
    #pragma option -w-eas //Assigning type to enum
    #pragma option -w-pin //Initialization is only partially bracketed
    #pragma option -w-ncf //Non-const function called for constant object
    #pragma option -w-hid //X hides virtual function Y
    #pragma option -w-thr //Throw expression violates exception specification
        //Borland generates this warning when an internal subroutine generates
        //a different type of exception within a try-catch block.
#endif

/***********************************************************************************
  VERSION INFO - IMPORTANT! CHANGE THIS AFTER EACH MODIFICATION TO NETWORK PROTOCOL
 ***********************************************************************************/
//Display this string in the window's caption.
#define VERSION_STRING   "0.35 (pre-release)"

//Transmit this code to peers to ensure they are using the same version.
#define VERSION_CODE     0x0353
/***********************************************************************************/

//Although values.h defines MAXFLOAT, the constant is not universal.
#define MAX_FLOAT    3.40282347E+38F
//UNIX compilers require the scope resolution operator.
using namespace std;

//The following macros perform rudimentary tasks and warrant no further explanation.
#define MAX(x,y)                ((x) > (y) ? (x) : (y))
#define MIN(x,y)                ((x) < (y) ? (x) : (y))
#define ABS(x)                  ((x) < 0 ? -(x) : (x))
#define DIFF(x,y)               ((x) > (y) ? (x)-(y) : (y)-(x))
#define MAX3(x,y,z)              MAX(x,MAX(y,z))
#define MIN3(x,y,z)              MIN(x,MIN(y,z))
#define DISTANCE(x1,y1,x2,y2)   (sqrt( ((x2)-(x1))*((x2)-(x1)) + ((y2)-(y1))*((y2)-(y1)) ))

//Verify that a value is between a lower bound (a) and an upper bound (b).
//If the value falls outside the range, the macro adjusts it.
template<class A> inline void clampRange(A& a, A x, A y)
{
    if(a<x) a=x;
    if(a>y) a=y;
}

//Finds the smallest multiple of y which meets or exceeds x.  You must provide
//the macros with integers.  Example:
//  ALIGN(9,4) = 12
#define ALIGN(x,y)              ((((x)-1)/(y)+1)*(y))

//Divides x by y and rounds the result up to the next whole number.  You must
//provide the macros with integers.  Example:
//  DIVIDE_UP(7,3) = 3
#define DIVIDE_UP(x,y)          (((x)-1)/(y)+1)

//Rounds a number to the nearest whole number
#define ROUND(x)                (int)((x)+0.5)

//The following set of macros wrap the most common string operations.  This
//allows us to switch to a different string class without wreaking havoc
//throughout the source code.  All of these macros leave the original string
//unchanged and produce copies in some cases.
#define CHARPTR(x)              ((x).c_str())                   //Yields a pointer to the first character
#define STRLEN(x)               ((x).length())                  //Yields the length of the string
#define STRLEFT(x,y)            ((x).substr(0,(y)))             //Copies the leftmost y characters
#define STRRIGHT(x,y)           ((x).substr((x).length()-(y)))  //Copies the rightmost y characters
#define STRMID(x,y,z)           ((x).substr((y),(z)))           //Copies z characters beginning at index y
#define CHRTOSTR(x)             (std::string(1,(x)))            //Converts a single "char" to a string
#define STROFCHR(x,y)           (std::string((y),(x)))          //Creates a string with character x repeated y times
#define STRTOUPPER(x)           stringToUpper(x)                //Converts all characters of a copy to uppercase
#define STRTOLOWER(x)           stringToLower(x)                //Converts all characters of a copy to lowercase
#define STRCMP(x,y)             ((x).compare(y))                //Compares two strings the same way as strcmp
#define STREQUAL(x,y)           ((x).compare(y)==0)             //Determines if two strings are identical
#define STRFIND(x,y)            ((x).find(y))                   //Searches for the substring y in x
                                                                //Returns index of first matching character in x
                                                                //Otherwise, returns length of x
#define STRERASE(x,i,n)         ((x).erase(i,n))                //Erases n characters from x starting at index i
#define STRINSERT(x,i,y)        ((x).insert(i,y))               //Inserts substring y into x at index i
#define STRLTRIM(x,y)           ((x).substr(y))                 //Trims the first y character from a copy of the string
#define STRRTRIM(x,y)           ((x).substr(0,(x).length()-(y)))//Trims the first y character from a copy of the string
#define STRPREFIX(x,y)          (strPrefix(CHARPTR(x),CHARPTR(y)))
                                                                //Determines if x is a prefix to y
                                                                
//Maximum width and height that glTexImage2D will support.  Although some systems
//will support larger textures, all systems will support 256x256 textures.
#define MAX_TEXTURE_SIZE    256

//File in which debug functions record bugs and log messages
#define OUTPUT_FILE          "output.txt"

//Convert a preprocessor definition to an ASCII string:
#define MAKE_ASCII_STRING(x) #x

/****************************************************************************
                                   Debugging
 ****************************************************************************/
/* CHECK_BLOCKING monitors operating system calls (i.e. Winsock) to ensure that
 * they do not block, meaning that the computer freezes for an extended period
 * of time.  If the computer freezes for over a second, an alert is appended to
 * the output file containing the blocking statement and the seconds elapsed.
 */

#ifdef DEBUG
#define CHECK_BLOCKING(command) \
{ \
    tTime now = tTime::current(); \
    command; \
    if(tTime::current() - now > 1) \
        bug("BLOCKAGE DETECTED (%d seconds): %s", (int) (tTime::current() - now), #command); \
}
#else
#define CHECK_BLOCKING(command) command;
#endif

#ifdef PROFILE
#define START_TIMER(x) timers[x].start();
#define STOP_TIMER(x) timers[x].stop();
#define START_GRAPHICS_TIMER(x) /*glFinish();*/ timers[x].start();
#define STOP_GRAPHICS_TIMER(x) /*glFinish();*/ timers[x].stop();
#else
#define START_TIMER(x)
#define STOP_TIMER(x)
#define START_GRAPHICS_TIMER(x)
#define STOP_GRAPHICS_TIMER(x)
#endif


//Enumerations

//Multi-purpose enumeration for referencing discrete directions
enum eDir {DIR_RIGHT,DIR_UP_RIGHT,DIR_UP,DIR_UP_LEFT,DIR_LEFT,DIR_DOWN_LEFT,DIR_DOWN,DIR_DOWN_RIGHT,DIR_RIGHT2,DIR_NONE};
//Multi-purpose enumeration for referencing flags
enum eFlag {FLAG_TRUE, FLAG_FALSE, FLAG_ASK};
//Timer labels for optimizing the game
enum eTimer {TIMER_GAME_LOOP, TIMER_INTERFACE_POLL, TIMER_INTERFACE_DRAW, TIMER_SWAP_BUFFERS,
    TIMER_ENGINE_TICK, TIMER_POLL_WINDOWS, TIMER_INVALIDATE_MOUSE_POSITION, TIMER_DRAW_MAP,
    TIMER_DRAW_WINDOWS, TIMER_DRAW_MOUSE_POINTER, TIMER_NETWORK_TICK, TIMER_COMPILE_VISIBLE_UNITS,
    TIMER_CALC_VISIBLE_AREA, TIMER_DRAW_UNIT_SHADOWS, TIMER_DRAW_DECORATION_SHADOWS,
    TIMER_COPY_TEX_SUB_IMAGE, TIMER_DRAW_TERRAIN, TIMER_POSTDRAW_TERRAIN, TIMER_PREDRAW_UNITS,
    TIMER_DRAW_UNITS, TIMER_DRAW_DECORATIONS, TIMER_DRAW_MISSILES, TIMER_DRAW_EXPLOSIONS,
    TIMER_POSTDRAW_UNITS, TIMER_DRAW_UNIT_OVERLAYS, TIMER_DRAW_WATER, TIMER_DRAW_WAVES,
    TIMER_DRAW_MINIMAP, MAX_TIMERS};

//Typedefs

//tGameTime holds the number of milliseconds elapsed since the game started.
typedef long tGameTime;
//tWorldTime holds the number of seconds elapsed since the game started.  World
//time usually equals gameTime/1000.0, except when the user adjusts the game
//speed.
typedef float tWorldTime;

//iDisplayList holds an index to an OpenGL display list.
typedef GLuint iDisplayList;

//iTextureIndex holds an index to an OpenGL texture.
typedef GLuint iTextureIndex;

//iImageIndex holds an ILUT image name.
typedef ILuint iImageIndex;

//We implement tString as a standard C++ string.
typedef std::string tString;

//The Visual C++ compiler does not define the addition operator for two C++
//strings (e.g. str1 + str2).
#ifdef VISUALC
extern tString operator+(const tString& lhs, const tString& rhs);
#endif

//A string list is a simple but useful container for storing multiple lines
//of text.
typedef vector<tString> tStringList;

//tKey holds a GLFW key (see GLFW_ENTER).  See glfw.h for more information.
typedef int tKey;

//tTime used to be a structure, but I replaced it with a double representing
//the number of seconds that have elapsed since the program started when I
//switched to GLFW.
typedef double tTime;

//Structures

/* Tile position specifies a unique square area in the world.  We will use this
 * structure in the future to store discrete world information such as the
 * visibility of a tile.  By convention, we define a tile (x,y) as the unit
 * square [x,x+1] x [y,y+1].
 */
struct tTilePos
{
    short x;
    short y;
};

//Area position specifies a unique "area" containing AREASIZE*AREASIZE tiles.
//We created this structure for optimizing our A* pathfinding algorithm.
struct tAreaPos
{
    unsigned char x;
    unsigned char y;
};

/* "Tasks" are a rudimentary form of multi-tasking.  The engine calls each task's
 * tick function during each pass and allows it to execute for a fraction of a
 * second.  To implement a new task, derive a new class from tTask and override
 * the class's tick function.  This function has two parameters: the system time
 * when the function was called and the duration it has to execute.  The function
 * should repeatedly poll the current time with a construct such as:
 *  while (currentTime - startTime < duration) {
 *      ...
 *      updateTime(); }
 * The function should set finished to true when the task has completed.
 */
class tTask
{
    public:
    bool            finished;      //Has the task completed?
    virtual void    tick(tTime startTime, float duration) {}
    tTask() : finished(false) {}
};

/* tLargeTexture loads a Windows bitmap file of any size, and divides it into tiles
 * no larger than 256x256.  Typically, you will want to render the texture on
 * the screen as a sprite, but you can also fit the texture to any 3D quad.
 * Just provide draw with the four coordinates of the quad, and the function
 * will make all the necessary calculations.
 */
class tLargeTexture
{
    public:
    int                     width;        //Width of the bitmap in pixels
    int                     height;       //Height of the bitmap in pixels
    int                     horPanels;    //Number of side-by-side textures in the x direction
    int                     vertPanels;   //Number of top-to-bottom textures in the y direction
    vector<iTextureIndex>   textures;     //Dynamic array of texture indices
    tLargeTexture();
    void setSize(int _width, int _height);
    bool loadBitmap(const char *filename);
    void draw(float v[4][3]);
    ~tLargeTexture();
};

//Simple structures for storing OpenGL color data
struct tRGB
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
};
struct tRGBA
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char a;
};

/* tFrameRate provides functions for calculating the frame rate or the average
 * time needed to complete a task.  Since the frame rate often fluctuates, it
 * contains a buffer for smoothing out fluctuations.  To use the structure,
 * bracket one or more lines with start and end.  Then use getFPS() to find
 * the rate at which start() is called, or use getDelayXXX() to find the average
 * delay between start() and end().  Here is an example:
 *  tFrameRate f
 *  f.start();
 *  ...
 *  f.end();
 *  float secs = f.getSeconds();
 */
#define FRAME_BUFFER_SIZE 10
class tFrameRate
{
    private:
    float   delayBuffer[FRAME_BUFFER_SIZE];     //Sample buffer for smoothing out fluctuations
    float   delayTotal;                         //Total time for last 100 samples
    int     delayIndex;                         //Current index within the sample buffer
    float   intervalBuffer[FRAME_BUFFER_SIZE];  //Sample buffer for smoothing out fluctuations
    float   intervalTotal;                      //Total time for last 100 samples
    int     intervalIndex;                      //Current index within the sample buffer

    double  oldTime;                            //Time when user called start()
    tString name;                               //Description of this timer
    bool    enabled;

    public:
    void enable() { enabled=true; }
    void disable() { enabled=false; }
    const tString& getName() { return name; }
    tFrameRate(const tString& _name);
    tFrameRate(const char *_name);

    void    start();
    void    stop();
    float   getFrequency() { return intervalTotal>0?FRAME_BUFFER_SIZE/intervalTotal:0; } //Find the average frequency
    float   getDelaySeconds() { return delayTotal/FRAME_BUFFER_SIZE; }                   //Find the average delay in seconds
    float   getDelayMilliseconds() { return delayTotal/FRAME_BUFFER_SIZE*1000; }         //Find the average delay in milliseconds
    float   getDelayMicroseconds() { return delayTotal/FRAME_BUFFER_SIZE*1000000; }      //Find the average delay in microseconds
    void    setName(const tString& _name) { name=_name; }
};

/* tException is a generic base class for any type of exception.  Typically,
 * you will throw an exception with throw(tException("Error Message")) and
 * catch it with catch(tException& e).
 */
class tException
{
    public:
    tString message;    //Contains error message
    tException(const tString& _message) : message(_message) {}
};

/* tFile serves a specialized purpose.  It contains basic information about one
 * file such as path, name, size, and date.   We use this structure to transmit
 * files across the network and to determine whether two files with the same
 * name match.
 */
struct tFile
{
    tString     path;           //Relative path where file is located (e.g. "download/")
    tString     name;           //Filename plus extension (e.g. "test.map")
    long        size;           //File size in bytes
    time_t      time;           //Raw file date in seconds since 1980

    tString     getFullName()   //Concatenates the path with the name (e.g. "download/test.map")
                    { return path+name; }
    void        setName(const tString& _name);
    FILE*       open(const char* method);
    bool        operator==(const tFile& arg);
    bool        operator!=(const tFile& arg) { return operator==(arg); }
    bool        exists();
    void        remove();
    void        clear();
    tFile() : size(0), time(0) {}
};

struct tBufferIterator
{
    char *start;
    char *current;
    int length;
    tBufferIterator(char *_start, int _length) : start(_start), length(_length), current(_start) {}
};

struct tMapping
{
    float from;
    float to;
    bool isClosed;
    bool isSmooth;
    tMapping() : from(0), to(0), isClosed(true), isSmooth(false) {}
    tMapping(float _from, float _to, bool _isClosed, bool _isSmooth) : from(_from),
        to(_to), isClosed(_isClosed), isSmooth(_isSmooth) {}
};

//Externals

extern bool     isValid(int x, int y, int width, int height);
extern void     makeValid(int& x, int& y, int width, int height);
extern void     longToIP(long IPLong, unsigned char values[4]);
extern long     IPToLong(const unsigned char values[4]);

extern bool     windowXYToModelXY(int windowX, int windowY, float& modelX, float& modelY, float matrix[]);
extern void     createRectShadow(int width, int height, int dispersion, float r, float g,
                    float b, iTextureIndex texture);
extern void     setViewingArea(int x, int y, int w, int h);
extern void     resetViewingArea(int x, int y, int w, int h);
extern void     loadStringList(tStringList& list, const char* string);
extern int      stringListLookup(const tStringList& list, const tString& string);

extern char*    strToLower(char *buffer);
extern char*    strToUpper(char *buffer);
extern bool     strPrefix(const char *prefix, const char *string);
inline bool     strPrefix(const tString& prefix, const char *string) { return strPrefix(CHARPTR(prefix),string); }
inline bool     strPrefix(const char *prefix, const tString& string) { return strPrefix(prefix,CHARPTR(string)); }
inline bool     strPrefix(const tString& prefix, const tString& string)
                    { return strPrefix(CHARPTR(prefix),CHARPTR(string)); }

extern int      strCount(const char *str, char ch);
extern tString  stringToLower(const tString& str);
extern tString  stringToUpper(const tString& str);
extern bool     containsExtension(const char *buffer);
extern void     log(const char *format, ...);
extern void     bug(const char *format, ...);

extern int      mySprintf(tBufferIterator& iter, const char *format, ...) throw(int);
extern int      nearestPowerOf2(int x);
extern void     checkGLError();

extern void     showTimers();
extern void     hideTimers();
extern void     writeString(const tString& string, FILE *fp);
extern tString  readString(FILE *fp);
extern float    transformValue(float val, const vector<tMapping>& mappings);

//Global constants:
extern const char       dirX[10];
extern const char       dirY[10];
extern const tString    EMPTY_STRING;

extern bool timersVisible;

#ifdef PROFILE
extern tFrameRate timers[MAX_TIMERS];
#endif
/****************************************************************************/
#endif
