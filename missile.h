/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef missileH
#define missileH

#include "texture.h"
#include "sound.h"

//eMissileStyle defines two basic types of missiles.  New missile styles will
//need to be hard-coded, as the OpenGL commands are involved.
enum eMissileStyle
{
    MISSILE_LASER,
    MISSILE_CANNON
};

struct tMissileType
{
    tString         name;           //Unique string identifier
    eMissileStyle   style;          //MISSILE_CANNON or MISSILE_LASER
    float           red,green,blue; //Missile's color
    float           firePower;      //Damage the missile will inflict upon impact
    float           velocity;       //Missile's velocity
    tSoundType*     soundType;      //Sound to play when missile is created
    float           length;         //Length of a laser beam (ignored for cannonballs)
    float           width;          //Width of a laser beam or radius of a cannonball
    int             armorClass;     //The armor class to which the missile belongs (e.g. piercing, bashing, etc.)
    tMissileType(tStream& s) throw(tString);
};

struct tUnit;

/* tMissile encapsulates a guided missile travelling from one unit to another.
 * When the missile reaches its target it will explode and inflict damage.  If
 * the target dies or otherwise disappears while the missile is en route, the
 * missile will impact the ground without damaging nearby units.  Currently,
 * the class defines the missile's appearance and behavior, but this is likely
 * to change.
 */
class tMissile
{
    private:
    tMissileType*   type;               //Missile type with which this missile is associated
    tUnit*          source;             //Unit which fired missile
    tUnit*          target;             //Missile's target
    tVector3D       initPos;            //Initial position of the missile when created
    tVector3D       sourcePos;          //Position at the beginning of current trajectory
                                            //(new trajectory calculated each interval)
    tVector3D       targetPos;          //Target position (equals "target"'s initial position)
    tVector3D       headPos;            //The position of the missile's head
    tVector3D       tailPos;            //The position of the missile's tail
    tWorldTime      initTime;           //Time when missile was created
    tWorldTime      startTime;          //Start time of current trajectory (equals time of last intervalTick)
    tWorldTime      endTime;            //Time when missile will reach target (This doesn't change)
    tAnim*          expl;               //Pointer to missile's explosion (NULL if missile still en route)
    bool            finished;           //Is the missile ready to be deleted?
    bool            laserVisible;       //Is the laser visible?
    tVector3D       randOffset;         //Random offset from target's center (need not be synchronized)
    iSoundSource    soundSource;        //Index of the sound source associated with this missile

    void            calcTargetPos();    //Calculate a new trajectory (called in intervalTick)

    //Data retrieval functions: (see descriptions above)
    public:
    bool            isFinished()    { return finished; }
    tUnit*          getSource()     { return source; }
    tUnit*          getTarget()     { return target; }
    const tVector3D& getPosition()  { return headPos; }
    bool            isEnRoute()     { return !expl && !finished; }

    //Externals:
    void            tick(tWorldTime t);
    void            intervalTick(tWorldTime t);
    void            draw();
    tMissile(tUnit *_source, const tVector3D& origin, tUnit *_target,
        tMissileType *_type, tWorldTime _startTime) throw(int);
    ~tMissile();

    //Events:
    void            onUnitDestroyed(tUnit *u, tWorldTime t);     //Unit pointer will become invalid shortly.
};

extern void             parseMissileTypeBlock(tStream& s) throw(tString);
extern void             destroyMissileTypes();

extern tMissileType*    missileTypeLookup(const tString& name, bool exact);
extern tMissileType*    parseMissileType(const char *fieldName, tStream& s) throw(tString);
/****************************************************************************/
#endif
