/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* SOCKWRAP.CPP:  Wrapper for lowest-level socket instructions               *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************
                            Socket Header Files
 ****************************************************************************/
#ifdef WINDOWS
    //The following define is needed so that Visual C++ won't compile error codes,
    //DDE, compression, RPC, some performance stuff and old winsock definitions in windows.h.
    #define WIN32_LEAN_AND_MEAN
    #include <windows.h>
    #include <winsock2.h>
#else
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <sys/ioctl.h>
    #include <sys/filio.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <unistd.h>
    #include <errno.h>
    #include <netdb.h>
#endif

/****************************************************************************/
#include "sockwrap.h"
#include "types.h"
#include "memwatch.h"
#pragma package(smart_init)

/****************************************************************************
                              Static Variables
 ****************************************************************************/
static fd_set in_set;  //Sockets to be checked for readability
static fd_set out_set; //Sockets to be checked for writability
static fd_set exc_set; //Sockets to be checked for errors
static int maxSocket=0; //Contains the largest socket descriptor in the sets above. (Used in 'select'.)



/***************************************************************************\
netGetError

Retrieves the last network error message.

Inputs:
    none
Outputs:
    An error code
\***************************************************************************/
#ifdef WINDOWS
int netGetError()
{
    return WSAGetLastError();
}
#else //LINUX
int netGetError()
{
    return errno;
}
#endif //WINDOWS

/***************************************************************************\
netIsBufferFull

Determines if the last send has caused the internal socket buffer to overflow.

Inputs:
    none
Outputs:
    Returns true if the internal socket buffer has overflowed
\***************************************************************************/
bool netIsBufferFull()
{
	return netGetError()==ENOBUFS || netGetError()==EWOULDBLOCK;
}

/***************************************************************************\
netStartup

Initializes the network.

Inputs:
    none
Outputs:
    Returns true if successful
\***************************************************************************/
#ifdef WINDOWS
bool netStartup()
{
    //The highest version of Windows Sockets support that we can use.
    //The high order byte specifies the minor version (revision) number;
    //the low-order byte specifies the major version number.
    unsigned short wVersionRequested = (unsigned short) MAKEWORD(1, 1);
    WSADATA wsaData;
    int error;

    //Initialize Windows sockets library.
    error = WSAStartup(wVersionRequested, &wsaData);
    if (error != 0)
    {
        bug("netStartup: WSAStartup returned %d", error);
        return false;
    }
    return true;
}
#else //LINUX
bool netStartup()
{
    //The socket library does not need to be initialized on Linux.
    return true;
}
#endif //WINDOWS

/***************************************************************************\
netShutdown

Shuts down the network.

Inputs:
    none
Outputs
    Returns true if successful
\***************************************************************************/
#ifdef WINDOWS
bool netShutdown()
{
    if (WSACleanup() == SOCKET_ERROR)
    {
        bug("netShutdown: WSACleanup failed with WSA error code %d", WSAGetLastError());
        return false;
    }
    return true;
}
#else //LINUX
bool netShutdown()
{
    //The socket library does not need to be shutdown on Linux.
    return true;
}
#endif //WINDOWS

/***************************************************************************\
netNewSocket

Creates a handle to a new TCP/IP or UDP socket.

Inputs:
    none
Outputs:
    s           Handle to the new socket
    Returns true if successful
\***************************************************************************/
bool netNewSocket(bool useTCPIP, tSocket& s)
{
    if(useTCPIP)
        s = socket( PF_INET, SOCK_STREAM, 0 ); //Initialize a socket using TCP/IP protocol
    else
        s = socket( PF_INET, SOCK_DGRAM, 0 ); //Initialize a socket using UDP protocol

	if (s == INVALID_SOCKET)
    {
        bug("netNewSocket: socket failed with error code %d", netGetError());
        return false;
    }

    return true;
}

/***************************************************************************\
netSetReuseAddr

Sets the SO_REUSEADDR socket option.  This allows multiple sockets to have
the same local IP address and port so long as they connect to different remote
addresses.  Closes socket if an error occurs.

Inputs:
    s           Valid socket handle
Outputs:
    s           Sets socket handle to INVALID_SOCKET if an error occurs
    Returns true if successful
\***************************************************************************/
bool netSetReuseAddr(tSocket& s)
{
  	int x = 1;
    int val;

#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netSetReuseAddr: invalid socket");
        return false;
    }
#endif
    val = setsockopt( s, SOL_SOCKET, SO_REUSEADDR, (char *) &x, sizeof(x) );
	if (val < 0)
    {
        bug("netSetReuseAddr: setsockopt failed with error code %d", netGetError());
        netCloseSocket(s);
        return false;
    }
    return true;
}

/***************************************************************************\
netSetNonBlocking

Makes a socket non-blocking.  A non-blocking socket will not freeze the
application when calling certain socket functions.  Closes socket if an
error occurs.

Inputs:
    s           Valid socket handle
Outputs:
    s           Sets socket handle to INVALID_SOCKET if an error occurs
    Returns true if successful
\***************************************************************************/
#ifdef WINDOWS
bool netSetNonBlocking(tSocket& s)
{
    int val;
    unsigned long arg = 1;
#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netSetNonBlocking: invalid socket");
        return false;
    }
#endif
    val = ioctlsocket( s, FIONBIO, &arg );
    if(val == SOCKET_ERROR)
	{
        bug("netSetNonBlocking: ioctlsocket failed with error code %d", netGetError());
        netCloseSocket(s);
    	return false;
    }
    return true;
}
#else //LINUX
bool netSetNonBlocking(tSocket& s)
{
    int val;
    unsigned long arg = 1;
#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netSetNonBlocking: invalid socket");
        return false;
    }
#endif
    val = ioctl( s, FIONBIO, &arg );
    if(val == SOCKET_ERROR)
	{
        bug("netSetNonBlocking: ioctl failed with error code %d", netGetError());
        netCloseSocket(s);
    	return false;
    }
    return true;
}
#endif //WINDOWS

/***************************************************************************\
netBindSocket

Binds a socket to the specified IP address and port.  If you provide 0 for the
IP address, the function will bind the socket to the default IP address.  If
you provide 0 for the port, the function will bind the socket to the next
avialable port.  The assigned address will match the desired address unless
you request the default.  Closes socket if an error occurs.

Inputs:
    s           Valid TCP/IP or UDP socket handle
    IP          Desired IP address (or 0 for default)
    port        Desired port (or 0 for default)
Outputs:
    s           Sets socket handle to INVALID_SOCKET if an error occurs
    IP          Assigned IP address
    port        Assigned port
    Returns true if successful
\***************************************************************************/
bool netBindSocket(tSocket& s, long& IP, int& port)
{
    int val;
	sockaddr_in sa = {0};               //Socket address with all values cleared
    sockaddr_in addr;
    int len=sizeof(addr);
	sa.sin_family = PF_INET;
#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netBindSocket: invalid socket");
        return false;
    }
#endif
    //IMPORTANT: IP or port may be negative!
    if(port != 0)
        sa.sin_port	= htons(port);      //htons converts port from host- to network-byte-order
    else
        sa.sin_port = 0;                //Automatically assign a port to socket
    if(IP != 0)                         //Caller has specified local IP address
        sa.sin_addr.s_addr = htonl(IP);

    //NOTE: If IP=0, then sa.sin_addr is left at 0 which is equivalent to INADDR_ANY.
    //This tells bind to assign any local address to this socket.

    val = bind( s, (sockaddr *) &sa, sizeof(sa) );
    if (val == SOCKET_ERROR)            //Assign the socket an address and a port
    {
        bug("netBindSocket: bind failed with error code %d", netGetError());
        netCloseSocket(s);
        return false;
    }

    getsockname(s, (sockaddr *) &addr, (socklen_t*)&len);
    port = ntohs(addr.sin_port);
    IP = ntohl(addr.sin_addr.s_addr);

#ifdef LOG
    unsigned char vals[4];
    longToIP(IP, vals);
    log("Assigning IP address %d.%d.%d.%d and port %d to socket",
        vals[0],vals[1],vals[2],vals[3],port);
#endif

    return true;
}

/***************************************************************************\
netListen

Instructs a bound TCP/IP socket to begin listening.  Closes socket if an error
occurs.

Inputs:
    s           Bound TCP/IP socket
Outputs:
    s           Sets socket handle to INVALID_SOCKET if an error occurs
    Returns true if successful
\***************************************************************************/
bool netListen(tSocket& s)
{
    int val;
#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netListen: invalid socket");
        return false;
    }
#endif
    //Second parameter specifies the maximum backlog of pending connections; SOMAXCONN is the default.
    val = listen(s, SOMAXCONN);
    if(val==SOCKET_ERROR)
    {
        bug("netListen: listen failed with error code %d", netGetError());
        netCloseSocket(s);
        return false;
    }
    return true;
}

/***************************************************************************\
netConnect

Attempts to connect the specified socket to the specified address.  A successful
return value does not necessarily indicate a successful connection.  The caller
will need to continually poll the socket to determine when data can be
transmitted.  Closes socket if an error occurs.

Inputs:
    s           Bound TCP/IP or UDP socket
    IP, port    Address to which to connect
Outputs:
    Returns true if no fatal error occurred
\***************************************************************************/
bool netConnect(tSocket& s, long IP, int port)
{
    sockaddr_in sa = {0}; //Socket address with all values cleared
    int err;

#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netConnect: invalid socket");
        return false;
    }
#endif

    sa.sin_family = PF_INET;
    sa.sin_port = htons(port);
    sa.sin_addr.s_addr = htonl(IP);

    //Attempt to connect to the specified address:
    err = connect(s, (sockaddr *) &sa, sizeof(sa));

    /* Determine if the connection fails for a reason other than EWOULDBLOCK.
     * EWOULDBLOCK means that the results of the connection are uncertain
     * because it hasn't finished yet.
     */
    if(err == SOCKET_ERROR && netGetError() != EWOULDBLOCK && netGetError() != EINPROGRESS)
    {
        bug("netConnect: connect failed with error code %d", netGetError());
        netCloseSocket(s);
        return false;
    }
    return true;
}

/***************************************************************************\
netSend

Attempts to send a series of bytes through the socket.  You will typically use
this function with TCP/IP sockets after calling netConnect.  The function
replaces the buffer length you provide with the actual number of bytes sent.
Note that a successful return code does not indicate the recipient has received
that data.

Inputs:
    s           Connected TCP/IP or UDP socket
    buf         Buffer containing bytes to send
    len         Number of bytes to send
Outputs:
    len         Number of bytes actually sent; 0 if an error occurs
    Returns true if successful
\***************************************************************************/
bool netSend(tSocket s, const char *buf, int& len)
{
#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netSend: invalid socket");
        return false;
    }
#endif
    len = send(s, buf, len, 0);
    if(len < 0)
    {
        bug("netSend: send failed with error code #%d", netGetError());
        len = 0;
        return false;
    }
    return true;
}

/***************************************************************************\
netSendTo

Attempts to send a series of bytes through the socket to the destination
address.  Use this function with UDP only, as the destination address will be
inherent to TCP/IP sockets.  The function replaces the buffer length you provide
with the actual number of bytes sent.  Note that a successful return code does
not indicate the recipient has received that data.


Inputs:
    s           Bound UDP socket
    IP, port    Destination address
    buf         Buffer containing bytes to send
    len         Number of bytes to send
Outputs:
    len         Number of bytes actually sent; 0 if an error occurs
    Returns true if successful
\***************************************************************************/
#ifdef PACKETLOSS
static bool freq[10]={true, true, false, true, false, true, true, false, false, true};
#endif

bool netSendTo(tSocket s, long IP, int port, const char *buf, int& len)
{
#ifdef PACKETLOSS
    static int freqCounter=0;
    freqCounter++;
    if(freqCounter==10)
        freqCounter=0;
    if(!freq[freqCounter])
        return true;
#endif
    sockaddr_in sa;

#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netSendTo: invalid socket");
        return false;
    }
    if(ARRAY_VIOLATION(buf, len))
        return false;
    if(len <= 0)
    {
        bug("netSendTo: len <= 0");
        return false;
    }
#endif

    //Connect the server to the destination IP address and port:
    sa.sin_family = PF_INET;
    sa.sin_port = htons(port);
    sa.sin_addr.s_addr = htonl(IP);

    len = sendto(s, buf, len, 0, (sockaddr *) &sa, sizeof(sa));
    if(len < 0) //Error
    {
        //Changed 1/1/03: Downgraded this to a log message since it will
        //result normally when the user loses his connection.
        log("netSendTo: sendto failed with error code %d", netGetError());
        len=0;
        return false;
    }
    return true;
}

/***************************************************************************\
netCloseSocket

Flushes a socket's buffer and closes the socket.

Inputs:
    s   The handle of the socket to close (must be valid)
Outputs:
    s   Always sets socket handle to INVALID_SOCKET
    Returns true if successful
\***************************************************************************/
#ifdef WINDOWS
bool netCloseSocket(tSocket& s)
{
    tSocket sock=s;
    s=INVALID_SOCKET;
    if(sock!=INVALID_SOCKET)
    {
        //Before closing the socket, send all buffered data:
        //7/2/03: Changed 's' to 'sock' below.  Obviously an error.
        shutdown(sock, SD_BOTH);

        if(closesocket(sock) == SOCKET_ERROR)
        {
            log("netCloseSocket: closesocket failed with error code %d", netGetError());
            return false;
        }
    }
    return true;
}
#else //LINUX
bool netCloseSocket(tSocket& s)
{
    tSocket sock=s;
    s=INVALID_SOCKET;
    if(sock!=INVALID_SOCKET)
    {
        //Before closing the socket, send all buffered data:
        shutdown(s, SHUT_RDWR);

        if(close(sock) < 0)
        {
            log("netCloseSocket: close failed with error code %d", netGetError());
            return false;
        }
    }
    return true;
}
#endif //WINDOWS

/***************************************************************************\
netClearSets, netIncludeSocket,
netSelect, netCanRead, netCanWrite, netHasError

These six functions provide wrappers for the Winsock 'select' function and
fd_set macros.  Although it is easier to implement these macros directly
in engine.cpp, I prefer to keep the engine as abstract as possible for
compatibility.  Call the functions in this order:

1. netClearSets:  Clears all three socket sets.
2. netIncludeSocket:  Includes the socket in all three socket sets.  Call this
    function for each socket you wish to check for readability, writability,
    and/or exceptions.
3. netSelect:  Calls 'select' with default parameters.  The function will
    retrieve the status of each socket leaving only readable, writable, and
    erroneous sockets in each socket set respectively.  Returns false if
    a fatal error occurred.
4. netCanRead, netCanWrite, netHasError:  Determines if the socket is readable,
    writable, or has an error.  These functions rely on the socket sets produced
    by the last call to netSelect.
\***************************************************************************/
void netClearSets()
{
    FD_ZERO( &in_set  );
    FD_ZERO( &out_set );
    FD_ZERO( &exc_set );
    maxSocket=0;
}

void netIncludeSocket(tSocket s)
{
    FD_SET(s, &in_set);
    FD_SET(s, &out_set);
    FD_SET(s, &exc_set);
    if(s>maxSocket)
        maxSocket=s;
}

bool netSelect()
{
    timeval zerotime = {0};
    /* The first parameter is unused.  The last parameters determines how long
     * the function will wait before it returns.  Zero forces the function
     * the return immediately.
     */
    if ( select( maxSocket, &in_set, &out_set, &exc_set, &zerotime ) == SOCKET_ERROR )
    {
        bug("netSelect: select failed with error code %d", netGetError());
        return false;
    }
    return true;
}

bool netCanRead(tSocket s)
{
    return FD_ISSET(s, &in_set);
}

bool netCanWrite(tSocket s)
{
    return FD_ISSET(s, &out_set);
}

bool netHasError(tSocket s)
{
    return FD_ISSET(s, &exc_set);
}

/***************************************************************************\
netRecvFrom

Receives one packet from the specified UDP socket.  The function stores the
packet in the specified buffer and returns the source address.  The last
parameter allows you to retrieve the next message without removing it from the
Winsock buffer.  If true, the function will produce the same message if called
again.

Inputs:
    s           Bound UDP socket
    buf         Pointer to memory location where function will write packet
                    contents
    len         Size of memory location (maximum number of bytes function will
                    write)
    peek        Leave the message in the Winsock buffer?
Outputs:
    IP, port    Source address
    len         Actual size of packet

    Returns false if no messages are waiting or a fatal error occurred.  In
    both cases, the function sets IP, port, and len to 0.
\***************************************************************************/
bool netRecvFrom(tSocket s, long& IP, int& port, char *buf, int& len, bool peek)
{
    sockaddr_in addr;
    int addrLen = sizeof(addr);

#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netRecvFrom: invalid socket");
        return false;
    }
#endif
    IP=0;
    port=0;
    len = recvfrom(s, buf, len, peek?MSG_PEEK:0, (sockaddr *) &addr, (socklen_t*)&addrLen);
    if(len < 0)
    {
        //It seems that an EWOULDBLOCK error also indicates that there is no data
        //waiting.
        if(netGetError() != EWOULDBLOCK)
            bug("netRecvFrom: recvfrom failed with error code %d", netGetError());
        len=0;
        return false;
    }
    else if(len == 0) //No more messages waiting
        return false;
    IP = ntohl(addr.sin_addr.s_addr);
    port = ntohs(addr.sin_port);
    return true;
}

/***************************************************************************\
netGetMaxMsgSize

Retrieves the maximum packet size of the specified socket.  You may safely
assume that the maximum packet size will be the same for all UDP sockets.
Closes socket if an error occurs.

Inputs:
    s           Valid socket handle
Outputs:
    maxMsgSize  Maximum packet size
    Returns true if successful
\***************************************************************************/
#ifdef WINDOWS
bool netGetMaxMsgSize(tSocket& s, unsigned int& maxMsgSize)
{
    int len=sizeof(maxMsgSize);
#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netGetMaxMsgSize: invalid socket");
        return false;
    }
#endif //DEBUG
    if(getsockopt(s, SOL_SOCKET, SO_MAX_MSG_SIZE, (char *) &maxMsgSize, (socklen_t*)&len) < 0)
    {
        bug("netGetMaxMsgSize: getsockopt failed with error code %d", netGetError());
        netCloseSocket(s);
        return false;
    }
    return true;
}
#else //LINUX
//Linux does not have an SO_MAX_MSG_SIZE counterpart, so we assume a maxmimum
//packet size of 1024.
bool netGetMaxMsgSize(tSocket& s, unsigned int& maxMsgSize)
{
    maxMsgSize=1024;
    return true;
}
#endif //WINDOWS

/***************************************************************************\
netGetSendBufSize

Retrieves the send buffer size of the specified socket.  You may safely
assume that the send buffer size will be the same for all sockets.
Closes socket if an error occurs.

Inputs:
    s           Valid socket handle
Outputs:
    bufSize     Size of send buffer
    Returns true if successful
\***************************************************************************/
bool netGetSendBufSize(tSocket& s, int& bufSize)
{
    int len=sizeof(bufSize);
#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netGetSendBufSize: invalid socket");
        return false;
    }
#endif
    //7/2/03: Changed 'SO_RCVBUF' to 'SO_SNDBUF' below.  Obviously an error.
    if(getsockopt(s, SOL_SOCKET, SO_SNDBUF, (char *) &bufSize, (socklen_t*)&len) < 0)
    {
        bug("netGetSendBufSize: getsockopt failed with error code %d", netGetError());
        netCloseSocket(s);
        return false;
    }
    return true;
}

/***************************************************************************\
netGetRecvBufSize

Retrieves the receive buffer size of the specified socket.  You may safely
assume that the receive buffer size will be the same for all sockets.
Closes socket if an error occurs.

Inputs:
    s           Valid socket handle
Outputs:
    bufSize     Size of receive buffer
    Returns true if successful
\***************************************************************************/
bool netGetRecvBufSize(tSocket& s, int& bufSize)
{
    int len=sizeof(bufSize);
#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netGetRecvBufSize: invalid socket");
        return false;
    }
#endif
    //7/2/03: Changed 'SO_SNDBUF' to 'SO_RCVBUF' below.  Obviously an error.
    if(getsockopt(s, SOL_SOCKET, SO_RCVBUF, (char *) &bufSize, (socklen_t*)&len) < 0)
    {
        bug("netGetRecvBufSize: getsockopt failed with error code %d", netGetError());
        netCloseSocket(s);
        return false;
    }
    return true;
}

/***************************************************************************\
netRecv

Receives buffered data from a connected TCP/IP socket and stores it in a
memory location.  You will typically call this function after calling netSelect
to determine whether there is any data to receive.  The function replaces the
buffer length you supply with the actual number of bytes received.

Inputs:
    s           Connected TCP/IP socket
    buf         Address of memory location
    len         Length of memory location
Outputs:
    len         Number of bytes received (possibly zero if data was not yet available)
    Returns false if the connection was closed (either gracefully or an error
        occurred.
\***************************************************************************/
bool netRecv(tSocket s, char *buf, int& len)
{
    unsigned long bytesWaiting;
#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netRecv: invalid socket");
        return false;
    }
#endif

    //Determine how many bytes are waiting:
#ifdef WINDOWS
    ioctlsocket(s, FIONREAD, &bytesWaiting);
#else
    ioctl(s, FIONREAD, &bytesWaiting);
#endif

    //Receive the data in the specified buffer:
    len = recv(s, buf, len, 0);

    if(len<0) //Error
    {
        len=0;
        //Added 1/1/03: It would seem that netRecv fails with EWOULDBLOCK just
        //after a connection is established even though netCanRead() returns true.
        //Thus, I simply wait until the data becomes available.
        if(netGetError()==EWOULDBLOCK)
            return true;
        bug("netRecv: recv failed with error code %d", netGetError());
        return false;
    }
    if(len==0) //Connection closed gracefully
        return false;
    return true;
}

/***************************************************************************\
netAccept

Accepts a new connection from a listening TCP/IP socket.  You will typically
call this function after calling netSelect to determine if there are any
new connections waiting.  The function produces the address of the new
connection and a handle to a new socket.  The new socket is connected and
ready to go.

Inputs:
    s           The listening TCP/IP socket which detected the new connection
Outputs:
    IP, port    The address of the new connection
    newSocket   A handle to the new socket
    Returns true if successful
\***************************************************************************/
bool netAccept(tSocket s, long& IP, int& port, tSocket& newSocket)
{
    sockaddr_in address;
    int size=sizeof(address);

#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netAccept: invalid socket");
        return false;
    }
#endif
    newSocket = accept(s, (sockaddr *) &address, (socklen_t*)&size);
    if(newSocket == INVALID_SOCKET)
    {
        IP=0;
        port=0;
        bug("netAccept: accept failed with error code %d", netGetError());
        return false;
    }

    IP=ntohl(address.sin_addr.s_addr);
    port=ntohs(address.sin_port);
    return true;
}

/***************************************************************************\
netGetAddress

Retrieves the address to which a TCP/IP socket is connected.  The function also
attempts to retrieve the text alias for this address. (e.g. phnx.tnt30.mindspring.com)
If a text alias is unavailable, the function will return the raw IP address as
a string.

Inputs:
    s           Connected TCP/IP socket
Outputs:
    IP, port    Address to which the socket is connected
    peerName    Pointer to a static string
    Returns true if successful
\***************************************************************************/
bool netGetAddress(tSocket s, long& IP, int& port, const char*& peerName)
{
    sockaddr_in address;
    hostent *host;
    int size=sizeof(address);

#ifdef DEBUG
    if(s==INVALID_SOCKET)
    {
        bug("netGetAddress: invalid socket");
        return false;
    }
#endif
    if ( getpeername( s, (sockaddr *) &address, (socklen_t*)&size ) == INVALID_SOCKET )
    {
        //If getpeername fails, set IP address to 0.0.0.0, port to 0, and
        //peerName to (Unknown).  Record an error message in output file and
        //return an error code.
        IP=0;
        port=0;
        peerName="(Unknown)";
        bug("netGetAddress: getpeername failed with error code %d", netGetError());
        return false;
    }

    IP=ntohl(address.sin_addr.s_addr);
    port=ntohs(address.sin_port);

    /* Attempt to retrieve the text alias for the IP address.
     * (e.g. phnx.tnt30.mindspring.com)  If gethostbyaddr succeeds, it will
     * produce a pointer to a static string (h_name).  If it fails, the raw IP
     * address will suffice.  We use inet_ntoa to convert the IP address to a
     * string (this function also produces a pointer to a static string.)
     */
    host = gethostbyaddr((char *) &address.sin_addr, sizeof(address.sin_addr),AF_INET);
    if(host)
        peerName = host->h_name;
    else
        peerName = inet_ntoa(address.sin_addr);

    return true;
}

/***************************************************************************\
netGetLocalIPs

Retrieves a static array of local IP addresses available on this system.

Inputs:
    none
Outputs:
    Returns a null-terminated ("") double-array of local IP addresses
\***************************************************************************/
const char **netGetLocalIPs()
{
    //Fill in local IP address and name:
    char buffer[80];
    hostent *host;
    gethostname(buffer, 80);
    host = gethostbyname(buffer);
    if(host == NULL)
    {
        bug("netGetLocalIPs: gethostbyname failed with error code %d", netGetError());
        return NULL;
    }
    else
        return (const char **) host->h_addr_list;
}

/***************************************************************************\
netGetLocalName

Retrieves a static string representing the local name.  For example:
phnx.tnt131.mindspring.com

Inputs:
    none
Outputs:
    Returns a pointer to a static null-terminated string
\***************************************************************************/
const char *netGetLocalName()
{
    //Fill in local IP address and name:
    char buffer[80];
    hostent *host;
    gethostname(buffer, 80);
    host = gethostbyname(buffer);
    if(host == NULL)
    {
        bug("netGetLocalName: gethostbyname failed with error code %d", netGetError());
        return NULL;
    }
    else
        return host->h_name;
}
/***************************************************************************\
netDestroySocket

Similar to netCloseSocket, this function closes the socket with closesocket
or close.  The difference is that this function closes the function immediately
via SO_LINGER.  This is important when you want to reconnect right away with
the same address.

Inputs:
    s   The handle of the socket to close (must be valid)
Outputs:
    s   Always sets socket handle to INVALID_SOCKET
    Returns true if successful
\***************************************************************************/
bool netDestroySocket(tSocket& s)
{
    int val;

    //Turn the option on with a zero delay:
    linger l={ /* l_onoff */ 1, /* l_linger */ 0};

    val=setsockopt(s,SOL_SOCKET,SO_LINGER,(char *)&l,sizeof(l));

    if(val<0)
    {
        bug("netDestroySocket: setsockopt failed with error code %d", netGetError());
        netCloseSocket(s);
        return false;
    }

    return netCloseSocket(s);
}
