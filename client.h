/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef clientH
#define clientH
/**************************************************************************
                                Headers
 **************************************************************************/
#include "texture.h"
#include "font.h"

/**************************************************************************
                                Defines
 **************************************************************************/
#define HINT_DELAY          0       //How many seconds after positioning the
                                    //mouse over a control will a hint appear?

/* This macro defines the default behavior for common functions in embedded
 * controls.  For instance, when you write, control->show(), the computer
 * may call control->parent->showControl(control), which will in turn call
 * control->onShow().  This allows the control's parent to update state variables
 * (such as selectedControl and focusedControl) before calling the control's
 * event.  If the control does not have a parent, the macro will simply call
 * the control's event (e.g. control->onShow()).
 *
 * Parameters:
 *  x   Function name (e.g. show or focus)
 *  y   Parent's counterpart (e.g. showControl or focusControl)
 *  z   Control's event (e.g. onShow or onFocus)
 */
#define DEFINE_CTRL_FUNCTION(x,y,z) \
    void tControl::x \
    { \
        if(parent) \
        { \
            if(parent->isType(CTRL_WINDOW)) \
                ((tWindow *)parent)->y; \
            else \
                bug("tControl::" #x ": parent isn't a window"); \
        } \
        else \
            z; \
    }

/**************************************************************************
                              Enumerations
 **************************************************************************/
//We use this enumeration to identify a control, especially when dereferencing
//a tObject pointer:
enum eControl {
    CTRL_NONE,                  CTRL_CONTROL,               CTRL_STATIC,
    CTRL_BUTTON,                CTRL_TOGGLE,                CTRL_RADIO,
    CTRL_SCROLL_BAR,            CTRL_WINDOW_SCROLL_BAR,     CTRL_SCROLL_WINDOW,
    CTRL_TEXT_BOX,              CTRL_EDIT,                  CTRL_LIST_BOX,
    CTRL_DROP_DOWN,             CTRL_COMBO_BOX,             CTRL_WINDOW,
    CTRL_WINDOW_BUTTON,         CTRL_DIALOG_BOX,            CTRL_STD_LABEL,
    CTRL_STD_BUTTON,            CTRL_STD_TOGGLE,            CTRL_STD_RADIO,
    CTRL_STD_SCROLL_BAR,        CTRL_STD_SCROLL_WINDOW,     CTRL_STD_TEXT_BOX,
    CTRL_STD_LIST_BOX,          CTRL_STD_COMBO_BOX,         CTRL_STD_DIALOG_BOX,
    CTRL_MAXIMUM};

//eCursor identifies the typing mode in a text box:
enum eCursor {CURSOR_INSERT, CURSOR_TYPE_OVER};

/* eWindowState identifies the position of a dialog box using the same convention
 * as MS Windows.  A maximized window fills the whole screen.  The contents of
 * a minimized window are not visible.  The user can resize a normal window to
 * suit his fancy.
 */
enum eWindowState {WINDOW_NONE, WINDOW_NORMAL, WINDOW_MAXIMIZED, WINDOW_MINIMIZED};

//eWindowAction defines the action of a window button (appearing in the
//upper-right corner of a dialog box).  Again, we use the same convention
//as MS Windows.
enum eWindowAction {WNDACT_MINIMIZE, WNDACT_RESTORE, WNDACT_MAXIMIZE, WNDACT_CLOSE, WNDACT_NONE};

//We use this enumeration to identify a mouse pointer:
enum ePointer {POINTER_NONE, POINTER_STD, POINTER_I_BEAM, POINTER_RESIZE, POINTER_SCROLL,
    POINTER_TARGET, POINTER_INVISIBLE};

/**************************************************************************
                             Type Definitions
 **************************************************************************/
class tControl;             //Declared here for the tEvent typedef
class tWindow;              //Declared here for the tEvent typedef

//tEvent defines a generic user-defined event:
typedef void (tWindow::*tEvent)(tControl *sender);

/**************************************************************************
                                Structures
 **************************************************************************/
//tControl is the ancestor of all controls.  Although you can instantiate
//a tControl object, you will never want to.
class tControl
{
    //tWindow must be friend so that it can call protected events.
    friend class tWindow;

    //There are some special cases where tDialogBox needs to circumvent tWindow's
    //public control functions (such as setControlPosition) and call protected
    //events directly.
    friend class tDialogBox;

    //Private variables:
    private:
    bool        focusable;      //Can the control be focused by tabbing onto it?
    bool        hintVisible;    //Is the hint currently visible?
    bool        selected;       //Is the mouse over the control?
    bool        focused;        //Does the control have focus, meaning keyboard
                                    //events will be directed to this control.
    bool        disabled;       //Is the control disabled?
    bool        visible;        //Is the control visible?
    bool        destroyed;      //Has the control been destroyed? (lazy delete)
    bool        lockSelect;     //Set this to true if you do not want onDeselect
                                    //called until mouse clicked outside control.
                                    //We use this property exclusively for combo boxes.
    tControl*   parent;         //Control's parent (usually the control on which
                                    //it acts.)
    int         controlX;       //Absolute x-position
    int         controlY;       //Absolute y-position
    int         width;          //Control's width
    int         height;         //Control's height
    tTime       lastMouseMove;  //Used to calculate when to show the hint

    eAnchor     horAnchor;      //ANCHOR_LEFT, ANCHOR_RIGHT, ANCHOR_BOTH, etc
    eAnchor     vertAnchor;     //ANCHOR_TOP, ANCHOR_BOTTOM, ANCHOR_BOTH, etc

    //Private functions:
    void        resetHint();

    //Protected variables:
    protected:
    eControl type;              //CTRL_BUTTON, CTRL_TOGGLE, CTRL_RADIO, etc.
                                //We must protect the type so that descendents
                                //can set it in their constructors.

    int saveMouseX;             //The last position of the mouse recorded by a mouse
    int saveMouseY;             //event handler.  I instantiate these variables for
                                //each control so that none of the controls need
                                //to call getMouseX() or getMouseY().

    /* If you choose to override the following 17 virtual functions, you must
     * call the same function in the ancestor first.  For example, if you are
     * writing an onMouseDown event for a descendent of tButton called
     * tStdButton, your function will look like this:

       void tStdButton::onMouseDown(int x, int y, eMouseButton button)
       {
            tButton::onMouseDown(x,y,button);
            ...
       }
     */

    //Protected events:
    //These functions are protected so that descendents can override them.
    virtual void onMouseDown(int x, int y, eMouseButton button);
    virtual void onMouseMove(int x, int y);
    virtual void onMouseUp(int x, int y, eMouseButton button);
    virtual void onKeyDown(tKey key, char ch);
    virtual void onKeyUp(tKey key, char ch);
    virtual void onMouseWheel(int pos, eMouseWheel direction);
    virtual void onFocus();
    virtual void onDefocus();
    virtual void onSelect(int x, int y);
    virtual void onDeselect();
    virtual void onDisable();
    virtual void onEnable();
    virtual void onShow();
    virtual void onHide();
    virtual void onSetPosition(int _controlX, int _controlY);
    virtual void onSetSize(int _width, int _height);
    virtual void onDestroy();

    public:
    virtual void poll();

    //Public virtuals:
    //Redefine these functions to give your control greater flexibility.

    /* objectActivated determines if the mouse is over a control.  Currently,
     * the function returns true if the mouse is within a rectangle defined by
     * controlX, controlY, width, and height.  Override this function if you
     * wish to design dynamic buttons (buttons that move) or circular buttons.
     */
    virtual bool objectActivated(int x, int y)
        { return x>=controlX && y>=controlY && x<controlX+width && y<controlY+height; }

    /* activated determines if the mouse is over a control -or- any of its
     * children (scroll bars, drop down boxes, etc.).  The function returns
     * objectActivated by default.  Override this function if the control has
     * visible children beyond its boundaries.
     */
    virtual bool activated(int x, int y)
        { return objectActivated(x,y); }

    /* Place drawing instructions in this function.  Most generic controls will
     * call their own user-defined virtual functions, such as drawControl,
     * drawBorder, drawContents, drawWindow, and drawControls.
     */
    virtual void draw() {}

    /* The program calls this function when OpenGL needs to be reinitialized.
     * For instance, the user toggles between windowed and full-screen mode.
     * The function should reload textures and display lists.
     */
    virtual void refresh() {}

    /* A hint is a small box which pops up when you hold the mouse over the
     * control.  Place drawing instructions in this function if you want the
     * control to have a hint.
     */
    virtual void drawHint() {}

    /* Override the destructor to free memory or clean-up when the control is
     * destroyed.
     */
    virtual      ~tControl() {}

    /* Override this function to change the mouse pointer when the control becomes
     * selected.  Your function should set the pointer type and tag and return true.
     * x and y refer to the current mouse coordinates.
     */
    virtual bool getMousePointer(int x, int y, ePointer& p, int& tag) { return false; }

    //Use these functions to control the control externally and adjust state
    //variables in parent.
    virtual void focus();
    virtual void defocus();
    virtual void disable();
    virtual void enable();
    virtual void show();
    virtual void hide();
    virtual void setPosition(int controlX,int controlY);
    virtual void setSize(int width,int height);
    virtual void destroy();

    //Data retrieval functions:
    bool        canFocus()      { return focusable; }
    bool        isHintVisible() { return hintVisible; }
    bool        isSelected()    { return selected; }
    bool        isFocused()     { return focused; }
    bool        isDisabled()    { return disabled; }
    bool        isVisible()     { return visible; }
    bool        isDestroyed()   { return destroyed; }
    bool        isLockSelect()  { return lockSelect; }
    tControl*   getParent()     { return parent; }
    int         getControlX()   { return controlX; }
    int         getControlY()   { return controlY; }
    int         getWidth()      { return width; }
    int         getHeight()     { return height; }
    eAnchor     getHorAnchor()  { return horAnchor; }
    eAnchor     getVertAnchor() { return vertAnchor; }

    //Public functions:
    bool        isType(eControl c);
    void        setLockSelect(bool _lockSelect);
    void        setParent(tControl *p);
    void        setAnchors(eAnchor _horAnchor, eAnchor _vertAnchor)
                    { horAnchor=_horAnchor; vertAnchor=_vertAnchor; }

    //Public variables:
    bool        oldVisible;     //Used by tDialogBox to retain the control's
                                    //visibility once the dialog box is minimized.
    int         tag;            //Multi-purpose tag

    //Constructor:
    tControl(int _controlX, int _controlY, int _width, int _height, bool _focusable) :
        controlX(_controlX), controlY(_controlY), width(_width), height(_height),
        focusable(_focusable), hintVisible(false), selected(false), focused(false),
        disabled(false), visible(true), oldVisible(true), destroyed(false),
        lockSelect(false), parent(NULL), type(CTRL_CONTROL), tag(0), horAnchor(ANCHOR_LEFT),
        vertAnchor(ANCHOR_BOTTOM), saveMouseX(0), saveMouseY(0) {}
};

//A tStatic object has no user-interface.  Its only purpose is to display a
//graphic or text message.  Override drawControl to change the control's
//appearance.
class tStatic : public tControl
{
    //Internals:
    public:
    void draw();

    //Virtuals:
    //Place drawing instructions here:
    virtual void drawControl()=0;

    //Externals:
    tStatic(int controlX, int controlY, int width, int height);
};

/* This class represents an ordinary push-button.  Pressing a mouse button
 * over the control depresses the button.  Releasing the mouse button releases
 * the button.  The control will call onExecute when the button is pressed or
 * released, depending on the executeOnDown flag.  The user can also trigger
 * the button by pressing any key when it is focused.
 */
class tButton : public tControl
{
    //Internals:
    private:
    bool depressed;     //Is the button currently depressed?
    bool executeOnDown;
        //If true, the class calls onExecute when a mouse button is pressed.
        //If false, the class calls onExecute when a mouse button is released.

    //Events:
    protected:
    void onMouseDown(int x, int y, eMouseButton button);
    void onMouseMove(int x, int y);
    void onMouseUp(int x, int y, eMouseButton button);
    void onKeyDown(tKey key, char ch);
    void draw();
    void onDefocus();
    void onDeselect();

    //Virtuals:
    //Place drawing instructions here:
    virtual void drawControl()=0;
    //Place commands here to execute when the button is triggered:
    virtual void onExecute() {}

    //Data retrieval functions:
    public:
    bool isDepressed() { return depressed; }

    //Externals:
    //Call this function to execute the button externally:
    void execute() { onExecute(); }
    tButton(int controlX, int controlY, int width, int height,
        bool canFocus, bool _executeOnDown);
};

//This class represents an ordinary toggle button.  The user toggles the
//control by clicking the mouse over the control or pressing any key.
class tToggle : public tControl
{
    //Internals:
    private:
    bool checked;       //Is the toggle checked?

    //Events:
    protected:
    void onMouseDown(int x, int y, eMouseButton button);
    void onKeyDown(tKey key, char ch);
    void draw();

    //Virtuals:
    //Place drawing instructions here:
    virtual void drawControl()=0;
    //Place commands here to execute when the control is toggled:
    virtual void onChange() {}

    //Data retrieval functions:
    public:
    bool isChecked() { return checked; }

    //Externals
    void set(bool checked);
    tToggle(int controlX, int controlY, int width, int height, bool canFocus,
        bool _checked);
};

/* Radio buttons allow a user to choose between two or more options.  Clicking
 * on one radio button will cause the others in the group to reset.  The tRadio
 * class is similar to the tToggle class except for the additional group variable.
 * When the radio button becomes checked, the class automatically unchecks other
 * radio buttons with a common parent and group.
 */
class tRadio : public tToggle
{
    //Internals:
    private:
    int group;      //Unique value shared by radio buttons in the same group

    //Events:
    protected:
    void onMouseDown(int x, int y, eMouseButton button);
    void onKeyDown(tKey key, char ch);

    //Externals:
    public:
    void set();
    tRadio(int controlX, int controlY, int width, int height, bool canFocus,
        bool isChecked, int _group);
};

/* tScrollBar defines a standard horizontal or vertical scroll bar.  The scroll
 * bar contains an arrow at both ends and a sliding marker.  The user can
 * adjust the marker slowly by clicking on the arrows or rapidly by clicking
 * in the area between the arrows and the marker.  The user can also click
 * and drag the marker to the desired position.  If the scroll bar has focus,
 * the user can further adjust the marker with the arrow keys, page up, page down,
 * end, or home.  Typically, the scroll bar controls a medium such as a window
 * (see tWindowScrollBar), but may also control a value or a setting.  For
 * example, it could be used to control red, green, and blue components in
 * a color mixing window.
 */
class tScrollBar : public tControl
{
    //Internals:
    private:
    bool    vertical;           //Is the scroll bar vertical or horizontal?
                                    //Use this value to draw the scroll bar.
    long    position;           //Position within the medium
                                    //(e.g. the current row in a text box)
    long    range;              //Length of the medium
                                    //(e.g. the total number of rows in a text box)
    long    pageSize;           //Amount of the medium visible at one time
                                    //(e.g. the number of rows that will fit inside a text box)
                                    //The marker will shrink or grow to reflect the pageSize.
                                    //Set this value to zero for scroll bars that do not
                                    //control windows.
    long    scrollRate;         //Amount by which to increase or decrease position
                                    //when the user presses an arrow
    int     arrowLength;        //Length of an arrow parallel to scroll bar's major axis
    int     minMarkerLength;    //The marker size is proportional to the amount of document you
                                    //can see at once.  However, for particularily long documents,
                                    //it's necessarily to establish a minimum length under which
                                    //the marker becomes no smaller.
    int     markerPosition;     //Marker's offset from the control's upper left corner parallel
                                    //to its major axis
    int     markerLength;       //Marker's length parallel to scroll bar's major axis

    bool    fastDecDepressed;   //Has the user clicked in the region above/left of marker?
    bool    decDepressed;       //Has the user pressed the up/left arrow?
    bool    markerDepressed;    //Has the user pressed the marker?
    bool    incDepressed;       //Has the user pressed the down/right arrow?
    bool    fastIncDepressed;   //Has the user clicked in the region below/right of marker?

    int     anchor;             //Used for sliding the marker as the mouse moves
    tTime   mouseDownTime;      //When did the user click on an arrow?
    bool    fastScroll;         //Has the user depressed the arrow for more than
                                    //half a second causing the control to scroll?

    long    calculatePosition(int markerPos);
    void    updateMarker();

    //Events:
    protected:
    void    onMouseDown(int x, int y, eMouseButton button);
    void    onMouseMove(int x, int y);
    void    onMouseUp(int x, int y, eMouseButton button);
    void    draw();
    void    onKeyDown(tKey key, char ch);
    void    poll();
    void    onEnable();
    void    onSetSize(int w, int h);

    //Virtuals:
    //Place drawing instructions here:
    virtual void drawControl()=0;
    //Place instructions here to execute when the position changes:
    virtual void onChange() {}

    //Data retrieval functions:
    public:
    bool    isVertical()            { return vertical; }
    long    getPosition()           { return position; }
    long    getRange()              { return range; }
    long    getPageSize()           { return pageSize; }
    int     getMarkerPosition()     { return markerPosition; }
    int     getMarkerLength()       { return markerLength; }
    bool    isFastDecDepressed()    { return fastDecDepressed; }
    bool    isDecDepressed()        { return decDepressed; }
    bool    isMarkerDepressed()     { return markerDepressed; }
    bool    isIncDepressed()        { return incDepressed; }
    bool    isFastIncDepressed()    { return fastIncDepressed; }

    int     length();
    int     trackLength();
    int     movementRange();
    long    maxPosition();

    //Externals:
    void    setScrollPosition(long _position);
    void    setScrollRange(long _range);
    void    setPageSize(long _pageSize);
    void    setValues(long _position, long _pageSize, long _range);

    bool    up();
    bool    down();
    bool    pageUp();
    bool    pageDown();
    bool    home();
    bool    end();

    //Descendents must set arrowLength and minMarkerLength based on scroll bar's design:
    tScrollBar(int controlX, int controlY, int width, int height, bool canFocus,
        bool _vertical, long _position, long _range, long _pageSize,
        long _scrollRate, int _arrowLength, int _minMarkerLength);
};

/* Since we use scroll bars in subsequent controls, we create a standard descendent
 * from tScrollBar which defines the scroll bar's appearance and dimensions.
 * We use this class in all scroll windows, text boxes, list boxes, and combo boxes.
 */
class tStdScrollBar : public tScrollBar
{
    private:
    tEvent event;

    //Events:
    protected:
    void drawControl();
    void onSetSize(int width, int height);
    void onChange() { if(event && getParent()->isType(CTRL_WINDOW)) (((tWindow *)getParent())->*event)(this); }

    //Externals:
    public:
    static const int BREADTH;   //Scroll bar's minor axis (defined in client.cpp)
    tStdScrollBar(int controlX, int controlY, int length, bool canFocus, bool isVertical,
        long position, long range, long pageSize, long scrollRate, tEvent _event=NULL);
};

//Define tScrollWindow here since tWindowScrollBar references it:
class tScrollWindow;

/* Scroll windows use a special type of scroll bar called tWindowScrollBar.
 * This class notifies its parent (a descendent of tScrollWindow) when the user
 * repositions the scroll bar.  It also notifies its parent when the scroll bar
 * becomes enabled or disabled.
 */
class tWindowScrollBar : public tStdScrollBar
{
    //tScrollWindow must be a friend so it can call protected events:
    friend class tScrollWindow;

    //Internals:
    private:
    void onChange();

    //Events:
    protected:
    void onHide();
    void onShow();

    //Externals:
    public:
    void enable();
    void disable();
    tWindowScrollBar(int controlX, int controlY, int length, bool isVertical,
        long position, long range, long pageSize, long scrollRate);
};

/* A scroll window is a control which lets you scroll through a block of text,
 * an image, or any other media.  A scroll window may have a vertical and/or
 * horizontal scroll bar.  It is the ancestor for text boxes, list boxes,
 * and combo boxes.
 */
class tScrollWindow : public tControl
{
    //Internals:
    private:
    tWindowScrollBar*   horScrollBar;   //Optional pointer to a horizontal scroll bar
    tWindowScrollBar*   vertScrollBar;  //Optional pointer to a vertical scroll bar
    bool windowSelected;                //Is the mouse over the main window? (exluding scroll bars)
    int  windowX;                       //Offset of left edge of scroll window's contents.
                                            //As you scroll right, windowX will increase.
    int  windowY;                       //Offset of top edge of scroll window's contents.
                                            //As you scroll down, windowY will increase.
    int  windowWidth;                   //Width of scroll window's contents.  If windowWidth
                                            //exceeds visWindowWidth(), the contents will not
                                            //fit horizontally within the window.
    int  windowHeight;                  //Height of scroll window's contents.  If windowHeight
                                            //exceeds visWindowHeight(), the contents will not
                                            //fit vertically within the window.

    void checkMousePosition(int x, int y);

    protected:
    bool horSelected;                   //Is the mouse over the horizontal scroll bar?
    bool vertSelected;                  //Is the mouse over the vertical scroll bar?
    void checkXY(int x, int y);

    //Events:
    void onMouseDown(int x, int y, eMouseButton button);
    void onMouseMove(int x, int y);
    void onMouseUp(int x, int y, eMouseButton button);
    void onMouseWheel(int pos, eMouseWheel direction);
    void onSelect(int x, int y);
    void onDeselect();
    void draw();
    void poll();
    void onDisable();
    void onEnable();
    void onSetPosition(int x, int y);
    void onSetSize(int w, int h);

    //Virtuals:
    //Place instructions for drawing the border here:
    virtual void drawBorder()=0;
    //Place instructions for drawing the contents here:
    virtual void drawContents()=0;

    /* Similar to onSelect, except this is called when the mouse is positioned
     * over the main window, ignoring the scroll bars.  This is useful for changing
     * the mouse pointer to an I-Beam in a text box.  NOTE: an onSelectWindow
     * even always accompanies an onSelect event.
     */
    virtual void onSelectWindow() {}

    /* Similar to onDeselect, except this is called when the mouse is moved
     * away from the main window, ignoring the scroll bars.  NOTE: an
     * onDeselectWindow event always accompanies an onDeselect event.
     */
    virtual void onDeselectWindow() {}

    public:
    //This function provides an interface with tWindowScrollBar:
    virtual void onUpdatePosition();

    //Data retrieval functions:
    int  getWindowX()       { return windowX; }
    int  getWindowY()       { return windowY; }
    int  getWindowWidth()   { return windowWidth; }
    int  getWindowHeight()  { return windowHeight; }
    tWindowScrollBar *getHorScrollBar() { return horScrollBar; }
    tWindowScrollBar *getVertScrollBar() { return vertScrollBar; }
    virtual int  visWindowX()=0;
    virtual int  visWindowY()=0;
    virtual int  visWindowWidth()=0;
    virtual int  visWindowHeight()=0;

    //Externals:
    void refresh();
    bool activated(int x, int y);
    void setWindowPosition(int _x, int _y);
    void setWindowSize(int _width, int _height);
    void enableScrollBar(tWindowScrollBar *s);
    void disableScrollBar(tWindowScrollBar *s);
    void createHorScrollBar(tWindowScrollBar *s);
    void createVertScrollBar(tWindowScrollBar *s);

    bool getMousePointer(int x, int y, ePointer& p, int& tag);

    tScrollWindow(int controlX, int controlY, int width, int height,
        bool canFocus, int _windowX, int _windowY, int _windowWidth,
        int _windowHeight);
    ~tScrollWindow();
};

/* Similar to tStdScrollBar, it is necessary to define a standard scroll window
 * here so that descendents can extend its appearance and properties.  In addition
 * to defining the margin, tStdScrollWindow introduces text via fontIndex.
 * fontIndex references one style of font.  See font module for more info.
 */
class tStdScrollWindow : public tScrollWindow
{
    //Internals:
    private:
    int fontIndex;                      //Reference to one style of font
    void drawBorder();

    //Data retrieval functions:
    public:
    int getFontIndex() { return fontIndex; }

    //Externals:
    static const int MARGIN;            //Constant defined in source file

    int visWindowX();
    int visWindowY();
    int visWindowWidth();
    int visWindowHeight();

    tStdScrollWindow(int controlX, int controlY, int width, int height, bool canFocus,
        int windowX, int windowY, int windowWidth, int windowHeight, bool hasVertScrollBar,
        bool hasHorScrollBar, int _fontIndex);
};

/* This structure constitutes the basis for my word wrapping routine.  The
 * structure contains information about one line, such as whether the line
 * ends in a hard-return or a soft-return.  Ultimately, I use the information
 * to calculate when and where to wrap a line.
 */
struct tLineData
{
    vector<int> charOffsets;    //Pixel offsets of each character on the line including
                                    //the offset after the last character.
    bool        modified;       //Do we need to recalculate the character offsets?
    int         length;         //Length of line in pixels (used to optimize word routine)
    bool        extended;       //Does the line end in a letter/space combination such
                                    //that length exceeds window width?
    bool        overflow;       //Does the line length exceed the window width?
    bool        hardReturn;     //Does line end in a hard return (true) or a soft return (false)?
};

/* tTextBox is the basis for all text-editing controls, both multi-line and
 * single line.  It supports most standard text box features, such as word-wrapping
 * insert or typeover mode, and highlighting.  It stores the text in a string
 * list (a vector of string objects).
 */
class tTextBox : public tStdScrollWindow
{
    //Internals:
    private:
    tStringList text;           //Text container (vector<tString>)
    bool        readOnly;       //Can the user modify the text?  If false, the
                                    //user cannot select text nor see the cursor,
                                    //but he can scroll.
    bool        wordWrap;       //Will the text automatically wrap so that the
                                    //length of a line never exceeds the window's width?
    bool        cursorVisible;  //Used in cursor blink cycle
    tTime       cursorTime;     //"
    tTime       scrollTime;     //Used to scroll window when user highlights text
                                    //beyond window's boundaries
    int         scrollX;        //"
    int         scrollY;        //"
    eCursor     cursorMode;     //Is the cursor in insert or typeover mode?
    int         maxLines;       //Maximum number of lines
    int         column;         //Cursor's column
    int         row;            //Cursor's row
    int         attractorX;     //X-coordinate to which cursor aligns itself when
                                    //cursor is moved vertically
    bool        selection;      //Has the user highlighted text?

    //Column and row where the selection starts.  The selection ends at the cursor.
    int         selStartCol;
    int         selStartRow;

    vector<tLineData> lineData; //Formatting data for each line

    void        clearSelection();
    void        removeSelection();
    void        adjustWindowSize();
    void        adjustSelection();
    void        XYToColRow(int x, int y, int &col, int &row, bool pastEndOfLine);
    void        checkCurrentPos();
    void        onUpdatePosition();
    void        insertText(const char *string);
    void        findLineData(int r);
    void        findOffsets(int r);
    bool        wrapUp(int& r);
    bool        wrapDown(int& r);
    void        parseLine(int row);

    protected:
    //Functions for handling keystrokes:
    void        backspace();
    void        charHandler(char ch);
    void        ctrlEnd();
    void        ctrlHome();
    void        del();
    void        down();
    void        end();
    void        enter();
    void        home();
    void        left();
    void        pageDown();
    void        pageUp();
    void        right();
    void        up();

    //This function is protected so that descendents can use it in order to draw highlight:
    void        colRowToXY(int col, int row, int& x, int& y);

    //Events:
    void        onMouseDown(int x, int y, eMouseButton button);
    void        onMouseMove(int x, int y);
    void        onFocus();
    void        onDefocus();
    void        onSetSize(int width, int height);
    void        poll();

    //Virtuals:

    /* onKeyDown is virtual so that descendents can introduce additional
     * behavior.  Since tTextBox defines this function, you must include
     * tTextBox::onKeyDown(key,ch) if you choose to override it.
     * NOTE:  onKeyDown will execute repeatedly if a key is depressed for
     * a certain amount of time.
     */
    virtual void onKeyDown(tKey key, char ch);

    //Data retrieval functions:
    public:
    const tStringList& getText()              { return text; }
    const vector<int>& getCharOffsets(int r)  { return lineData[r].charOffsets; }
    bool        isReadOnly()            { return readOnly; }
    bool        isCursorVisible()       { return cursorVisible; }
    eCursor     getCursorMode()         { return cursorMode; }
    int         getLineCount()          { return text.size(); }
    int         getMaxLines()           { return maxLines; }
    int         getColumn()             { return column; }
    int         getRow()                { return row; }
    bool        isSelection()           { return selection; }
    int         getSelStartCol()        { return selStartCol; }
    int         getSelStartRow()        { return selStartRow; }
    int         wordWrapWidth()         { return visWindowWidth()-2; }

    //Externals:
    void        clear();
    void        addString(const tString& s);
    void        insertString(const tString& s, int index);
    void        deleteString(int index);
    void        replaceString(const tString& s, int index);

    tTextBox(int controlX, int controlY, int width, int height, const tString& value,
        int _maxLines, bool createVertScrollBar, bool createHorScrollBar,
        int fontIndex, bool _wordWrap, bool _readOnly);
};

/* tStdTextBox is the ancestor of tEdit, a one-line text entry box.  This class
 * contains standard functions for drawing the contents of the text box and
 * displaying an I-beam pointer when the user moves the mouse over the control.
 */
class tStdTextBox : public tTextBox
{
    //Internals:
    private:
    void drawContents();

    //Externals:
    public:
    bool getMousePointer(int x, int y, ePointer& p, int& tag);
    tStdTextBox(int controlX, int controlY, int width, int height, const tString& value,
        int maxLines, bool createVertScrollBar, bool createHorScrollBar,
        int fontIndex, bool wordWrap, bool readOnly);
};

/* tEdit offers no additional functionality over tStdTextBox.  Instead, it acts as
 * a transparency for creating standard one-line entry boxes.  'getString' and
 * 'setString' provide fast access to the first and only line of text.
 */
class tEdit : public tStdTextBox
{
    //Events:
    protected:
    void onSetSize(int width, int height);

    //Data retrieval functions:
    public:
    const tString& getString();

    //Externals:
    void setString(const tString& s);

    tEdit(int controlX, int controlY, int width, const tString& value,
        int fontIndex, bool readOnly);
};

/* A list box lets the user choose from a list of choices.  Unlike a combo box,
 * the list is always visible.  The list box responds to keyboard events and may
 * have horizontal and vertical scroll bars.  Only one choice may be highlighted
 * at a time.  We store the choices in a string list container.
 */
class tListBox : public tStdScrollWindow
{
    //Internals:
    //We declare these as protected since descendents such as tDropDownBox need
    //to access them.
    protected:
    int         choice;             //Index of the currently selected choice.
                                    //If no choice is selected, then choice equals -1.
    tStringList choices;            //Choice container (vector<tString>)
    int         choiceCount;        //Number of choices to choose from
    tTime       scrollTime;         //Used to scroll window when user drags mouse
                                        //beyond window's boundaries
    int         scrollX;            //"
    int         scrollY;            //"

    void        checkCurrentPos();
    void        adjustWindowSize();

    //Functions for responding to keyboard events:
    void        up();
    void        down();
    void        pageUp();
    void        pageDown();
    void        home();
    void        end();

    //Events:
    void        poll();
    void        onMouseDown(int x, int y, eMouseButton button);
    void        onMouseMove(int x, int y);

    //Virtuals:
    /* onKeyDown is virtual so that descendents can introduce additional
     * behavior.  Since tListBox defines this function, you must include
     * tListBox::onKeyDown(key,ch) if you choose to override it.
     * NOTE:  onKeyDown will execute repeatedly if a key is depressed for
     * a certain amount of time.
     */
    virtual void    onKeyDown(tKey key, char ch);

    //Place intructions here to execute when the user selects a new choice:
    virtual void    onChange() {}

    //Data retrieval functions:
    public:
    int             getChoice()         { return choice; }
    int             getChoiceCount()    { return choiceCount; }
    const tStringList& getChoices()     { return choices; }
    const tString&  getChoiceString();

    //Externals:
    void        setChoice(int _choice);
    void        clear();
    void        addChoice(const tString& s);
    void        insertChoice(const tString& s, int index);
    void        deleteChoice(int index);
    void        replaceChoice(const tString& s, int index);

    tListBox(int controlX, int controlY, int width, int height, bool canFocus,
        const tString& value, int _choice, bool createVertScrollBar, bool createHorScrollBar,
        int fontIndex);
};

//tStdListBox contains instructions for drawing the contents of the list box.
//We must declare it here since tStdListBox is the ancestor of tDropDown.
class tStdListBox : public tListBox
{
    //Internals:
    private:
    tEvent event;
    void drawContents();

    protected:
    void onChange() { if(event && getParent()->isType(CTRL_WINDOW)) (((tWindow *)getParent())->*event)(this); }

    //Externals:
    public:
    tStdListBox(int controlX, int controlY, int width, int height, bool canFocus,
        const tString& value, int choice, bool createVertScrollBar,
        bool createHorScrollBar, int fontIndex, tEvent _event=NULL);
};

//We must declare tComboBox here since tDropDown references it.
class tComboBox;

/* tDropDown is a specialized list box that cooperates with a combo box.  It
 * redefines several events.  Most importantly, it informs its parent when the
 * user selects a new choice, and hides itself when the user confirms that choice
 * (e.g. the user releases the mouse button or presses enter).
 */
class tDropDown : public tStdListBox
{
    //tComboBox must be a friend so that it can call protected events:
    friend class tComboBox;

    //Internals:
    private:
    void onChange();

    //Events:
    protected:
    void onMouseMove(int x, int y);
    void onMouseUp(int x, int y, eMouseButton button);
    void onKeyDown(tKey key, char ch);
    void onDisable();
    void onEnable();

    //Externals:
    public:
    void hide();
    tDropDown(int controlX, int controlY, int width, int height, bool canFocus,
        const tString& value, int choice, bool createVertScrollBar,
        bool createHorScrollBar, int fontIndex);
};

/* A combo box displays a single line with your selection and a down arrow at
 * the far right.  When you click on the arrow, a drop-down box appears beneath
 * from which you can choose a new selection.
 */
class tComboBox : public tStdScrollWindow
{
    //Internals:
    private:
    int         choice;         //The index of the currently selected choice.
                                //If no choice is selected then choice equals -1.
    tStringList choices;        //Choice container (vector<tString>)
    int         choiceCount;    //Number of choices to choose from
    tDropDown*  dropDownBox;    //Pointer to the control's drop-down box
                                    //(created in the constructor)
    bool        isLatched;      //Used in determining whether to pass onMouseMove
                                    //event to drop-down box

    //Events:
    protected:
    void        poll();
    void        onMouseDown(int x, int y, eMouseButton button);
    void        onMouseMove(int x, int y);
    void        onMouseUp(int x, int y, eMouseButton button);
    void        onMouseWheel(int pos, eMouseWheel direction);
    void        onKeyDown(tKey key, char ch);
    void        onDefocus();
    void        onDeselect();
    void        onDisable();
    void        onSetSize(int width, int height);
    void        onSetPosition(int x, int y);

    //Virtuals:
    //Place instructions here to execute when the user makes a new selection:
    virtual void        onChange() {}

    //Data retrieval functions:
    public:
    int                 getChoice()         { return choice; }
    int                 getChoiceCount()    { return choiceCount; }
    const tStringList&  getChoices()        { return choices; }
    const tString&      getChoiceString();

    //Externals:
    //These two functions provide an interface with tDropDown:
    void        onSetChoice(int _choice);
    void        onConfirmChoice(int _choice);

    void        refresh();
    void        drawDropDownBox();

    void        setChoice(int _choice);
    void        setDropDownHeight(int dropDownHeight);
    bool        activated(int x, int y);
    void        hideDropDownBox(tDropDown *d);
    void        clear();
    void        addChoice(const tString& s);
    void        insertChoice(const tString& s, int index);
    void        deleteChoice(int index);
    void        replaceChoice(const tString& s, int index);

    bool        getMousePointer(int x, int y, ePointer& p, int& tag);

    tComboBox(int controlX, int controlY, int width, bool canFocus,
        const tString& value, int _choice, bool dropDownVertScrollBar,
        bool dropDownHorScrollBar, int dropDownHeight, int fontIndex);
    ~tComboBox();
};

/* A window represents a collection of controls which the user can select or
 * focus using the mouse or keyboard.  The window has no support for repositioning,
 * resizing, minimizing, maximizing, or closing.  tDialogBox introduces these
 * features.
 */
class tWindow : public tControl
{
    //These functions must be friends so they can access the object's protected events:
    friend void hideWindow(tWindow *w);
    friend void showWindow(tWindow *w);
    friend void setWindowPosition(tWindow *w, int x, int y);
    friend void setWindowSize(tWindow *w, int width, int height);
    friend void destroyWindow(tWindow *w);

    //Internals:
    private:
    vector<tControl *>  controls;   //Control container ordered by tab order
    bool        modal;              //Is the window modal?
                                        //Modal means the user cannot focus another window
    bool        background;         //Is this window a background?
                                        //A background always appears beneath foreground
                                        //windows even if focused.
    bool        destroyOnClose;     //If true, window is destroyed when closed.
                                        //If false, window is hidden when closed.

    tControl*   focusedControl;     //Control with focus
    tControl*   selectedControl;    //Control which mouse is over
    tButton*    defaultControl;     //Control to be executed if enter is pressed in certain controls
    tControl*   initialControl;     //Control which initially has focus
    int         current;            //Used for traversing list with getFirstControl and getNextControl
    tControl*   oldFocusedControl;  //Focused control before onDefocus or hideControls

    void        drawControls();

    protected:
    void        onShow();
    void        onDisable();
    void        onEnable();

    void        adjustSelection(int x, int y, bool overrideLock);
    void        deselectControl();

    //Events:
    public:
    void        onFocus();
    void        onDefocus();
    void        onSelect(int x, int y);
    void        onDeselect();
    void        onKeyDown(tKey virtualKey, char ch);
    void        onKeyUp(tKey virtualKey, char ch);
    void        onMouseWheel(int pos, eMouseWheel direction);
    void        onMouseDown(int x, int y, eMouseButton button);
    void        onMouseMove(int x, int y);
    void        onMouseUp(int x, int y, eMouseButton button);
    void        onSetPosition(int x, int y);
    void        onSetSize(int w, int h);
    void        draw();
    void        poll();
    void        refresh();

    //Virtuals:
    //Place instructions here to draw the window's background and edges:
    virtual void drawWindow() {}

    virtual void createControl(tControl *control);
    virtual void destroyControl(tControl *control);
    virtual void setDefault(tButton *d);
    virtual void setInitial(tControl *i);

    //These functions call the control's corresponding event and update state variables:
    virtual void disableControl(tControl *c);
    virtual void enableControl(tControl *c);
    virtual void hideControl(tControl *c);
    virtual void showControl(tControl *c);
    virtual void setControlPosition(tControl *c, int x, int y);
    virtual void setControlSize(tControl *c, int w, int h);
    virtual void focusControl(tControl *c);
    virtual void defocusControl(tControl *c);

    //Data retrieval functions:
    bool        isModal()               { return modal; }
    bool        isBackground()          { return background; }
    tControl*   getFocusedControl()     { return focusedControl; }
    tControl*   getSelectedControl()    { return selectedControl; }
    tButton*    getDefaultControl()     { return defaultControl; }
    tControl*   getInitialControl()     { return initialControl; }
    tControl*   getFirstControl();
    tControl*   getNextControl();

    //Externals:
    void        show();
    void        hide();
    void        focus();
    void        defocus();
    void        setPosition(int x, int y);
    void        setSize(int w, int h);
    void        destroy();
    void        enable();
    void        disable();
    void        close();
    tControl*   controlLookup(int tag);

    bool        getMousePointer(int x, int y, ePointer& p, int& tag);

    tWindow(int controlX, int controlY, int width, int height, bool _modal,
        bool _background, bool _destroyOnClose);
    ~tWindow();
};

//Declare tWindowButton here since tDialogBox references it:
class tWindowButton;

/* tDialogBox introduces most of the features of an MS Windows dialog box.
 * It consists of an embossed rectangle with a title bar across the top
 * and one or more buttons in the top-right corner.  The user can reposition,
 * resize, minimize, maximize, restore, and close the dialog box.
 */
class tDialogBox : public tWindow
{
    //Internals:
    private:
    bool        moveable;      //Can the user move the window? (by dragging title bar)
    bool        resizeable;    //Can the user resize the window? (by dragging the window's edges)
    bool        maximizeable;  //Can the user maximize the window? (by clicking the maximize button)
    bool        minimizeable;  //Can the user minimize the window? (by clicking the minimize button)
    bool        closeable;     //Can the user close the window? (by clicking the close button)

    eWindowState state; //WINDOW_NORMAL, WINDOW_MAXIMIZED, or WINDOW_MINIMIZED

    //Old position/size before window was maximized or minimized:
    int         oldX;
    int         oldY;
    int         oldWidth;
    int         oldHeight;

    bool        titleBarDepressed;  //Has the user depressed the mouse button over the title bar?
    bool        resizing;           //Has the user depressed the mouse button over the window's edges?
    eDir        resizeDir;          //Which edge is the user resizing or about the resize?

    //Reference point for repositioning the dialog box:
    int         anchorX;
    int         anchorY;

    //Reference points for resizing the dialog box:
    int         anchorX1;
    int         anchorY1;
    int         anchorX2;
    int         anchorY2;

    int         titleBarHeight;     //Title bar's height
    int         margin;             //Size of margin surrounding dialog box.
                                        //This does not affect controls' position.

    int         minWidth;           //Minimum width of dialog box
    int         maxWidth;           //Maximum width of dialog box
    int         minHeight;          //Minimum height of dialog box
    int         maxHeight;          //Maximum height of dialog box
    void        checkX1(int& x1, int& x2);
    void        checkX2(int& x1, int& x2);
    void        checkY1(int& y1, int& y2);
    void        checkY2(int& y1, int& y2);
    bool        resizeActivated(int x, int y);
    void        adjustResize(int x, int y);

    static const int   RESIZE_MARGIN;   //This constant defines the width of the margin
                                        //within which users can resize the dialog box.  I should
                                        //probably define this in tStdDialogBox, but it's not worth the time.
    protected:
    tWindowButton*  buttons[3];      //Pointers to up to three window buttons

    //Virtuals:
    virtual bool    titleBarActivated(int x, int y);

    //Descendents should define how to change window position and size:
    virtual void    onMaximize();
    virtual void    onMinimize();
    virtual void    onRestore();

    //Events:
    public:
    void        onMouseDown(int x, int y, eMouseButton button);
    void        onMouseMove(int x, int y);
    void        onMouseUp(int x, int y, eMouseButton button);
    void        onSelect(int x, int y);
    void        onDeselect();
    void        onSetSize(int width, int height);
    void        onKeyDown(tKey key, char ch);

    //Data retrieval functions:
    bool        canMove()           { return moveable; }
    bool        canResize()         { return resizeable; }
    bool        canMaximize()       { return maximizeable; }
    bool        canMinimize()       { return minimizeable; }
    bool        canClose()          { return closeable; }
    eWindowState getState()         { return state; }
    int         getTitleBarHeight() { return titleBarHeight; }
    int         getMargin()         { return margin; }

    //Externals:
    //This function provides an interface with tWindowButton:
    void windowButtonHandler(tControl *sender);

    void maximize();
    void minimize();
    void restore();
    void hideControl(tControl *c);
    void showControl(tControl *c);
    void createControl(tControl *control);

    bool getMousePointer(int x, int y, ePointer& p, int& tag);
    
    tDialogBox(int controlX, int controlY, int width, int height, int _titleBarHeight,
        int _margin, eWindowState _state, bool modal, bool _destroyOnClose, bool _moveable, bool _resizeable,
        bool _maximizeable, bool _minimizeable, bool _closeable, int _minWidth, int _maxWidth,
        int _minHeight, int _maxHeight);
};

/* A window button is a specialized button that controls a dialog box.  The
 * button face illustrates on of four actions (maximize, minimize, restore, and
 * close) based on the 'action' setting.  When the user clicks on the button,
 * it notifies its parent, which takes the appropriate action.
 */
class tWindowButton : public tButton
{
    private:
    //Internals:
    eWindowAction   action;     //WNDACT_MINIMIZE, WNDACT_MAXIMIZE, etc.
    tEvent          event;      //Address of parent's windowButtonHandler
    tTexture*       texture;    //Address of the texture representing the current action

    void            refreshTexture();
    
    //Events:
    protected:
    void            onExecute();
    void            drawControl();

    //Data retrieval functions:
    public:
    eWindowAction   getAction()     { return action; }
    tEvent          getHandler()    { return event; }

    //Externals:
    void            setAction(eWindowAction _action);
    void            setHandler(tEvent _event);

    tWindowButton(int controlX, int controlY, eWindowAction _action, tEvent _event);
    ~tWindowButton();
};

// This the generic base class for all mouse pointers.
// Derive descendents which overload draw.
class tMousePointer
{
    private:
    //Internals:
    ePointer type;          //POINTER_STD, POINTER_I_BEAM, etc.
    int tag;                //Multi-purpose tag for holding information like direction and height.

    //Virtuals:
    public:
    //Place drawing instructions here:
    virtual void draw(int x, int y) {}

    //Place instructions here to execute when OpenGL is reinitialized:
    virtual void refresh() {}

    //Data retrieval functions:
    bool isType(ePointer t) { return type==t; }
    bool isType(ePointer t, int x) { return type==t && tag==x; }

    //Externals:
    tMousePointer(ePointer _type, int _tag) : type(_type), tag(_tag) {}
    //We must declare the destructor as virtual;
    virtual ~tMousePointer() {}
};

/* tStdPointer is the default mouse pointer when none of the pointer below apply.
 * The class draws a repeating animation, "StdPointer", at the mouse coordinates.
 */
class tStdPointer : public tMousePointer
{
    private:
    tAnim anim;     //Animation parameters

    public:
    void draw(int x, int y);

    tStdPointer(int tag);
};

/* tIBeamPointer appears when the user moves the mouse over a text box.  The
 * pointer resembles the standard I-Beam pointer seen in Windows.  The height
 * depends on the font's height.
 */
class tIBeamPointer : public tMousePointer
{
    private:
    int height;     //Height of I-Beam pointer (determined by font height)

    public:
    void draw(int x, int y);

    tIBeamPointer(int _height);
};

/* tResizePointer appears when the user moves the pointer over the edge of a
 * resizeable dialog box.  The same class accommodates all eight directions
 * via the ePointer parameter in the constructor.
 */
class tResizePointer : public tMousePointer
{
    private:
    tAnim anim;         //Animation parameters
    float matrix[16];   //Matrix by which to multiply the modelview matrix before
                            //rendering the animation.  The matrix flips/rotates
                            //the animation to the proper orientation.
    eDir dir;           //The direction that the pointer faces (e.g. DIR_LEFT, DIR_UP, etc.)

    public:
    void draw(int x, int y);

    tResizePointer(int _dir);
};

/* tScrollPointer appears when the user moves the mouse to the edge of the screen
 * during a game.  The pointer indicates that the user is panning the view.
 * The same class accommodates all eight directions via the ePointer parameter in
 * the constructor.
 */
class tScrollPointer : public tMousePointer
{
    private:
    int offsetX;        //Animation's offset from mouse coordinates (initialized in constructor)
    int offsetY;        //"
    tAnim anim;         //Animation parameters
    tTime start;        //Time when first frame of the animation appeared
                            //(used to synchronize animation frames with translation)
    float matrix[16];   //Matrix by which to multiply the modelview matrix before
                            //rendering the animation.
    eDir dir;           //The direction that the pointer faces (e.g. DIR_LEFT, DIR_UP, etc.)

    public:
    void draw(int x, int y);

    tScrollPointer(int tag);
};

/* tTargetPointer appears when the user targets a command during a game.  The
 * pointer indicates that the computer is waiting for the user to select a
 * unit or point for the current command.
 */
class tTargetPointer : public tMousePointer
{
    private:
    tAnim anim;     //Animation parameters

    public:
    void draw(int x, int y);

    tTargetPointer(int tag);
};

/* tInvisible pointer has no physical form.  It is used during building placement
 * so it doesn't cover the apparition.
 */
class tInvisiblePointer : public tMousePointer
{
    public:
    void draw(int x, int y) {}
    tInvisiblePointer(int tag) :  tMousePointer(POINTER_INVISIBLE, tag) {}
};
 /**************************************************************************
                             Global Variables
 **************************************************************************/
extern eControl ancestors[CTRL_MAXIMUM];

extern int saveMouseX;
extern int saveMouseY;

 /**************************************************************************
                             Global Functions
 **************************************************************************/
//Global window functions:
extern void hideWindow(tWindow *w);
extern void showWindow(tWindow *w);
extern void setWindowPosition(tWindow *w, int x, int y);
extern void setWindowSize(tWindow *w, int width, int height);
extern void focusWindow(tWindow *w);
extern void defocusWindow(tWindow *w);
extern void destroyWindow(tWindow *w);
extern void drawWindows();
extern void pollWindows();
extern void refreshWindows();
extern void createWindow(tWindow *w);
extern void destroyWindows();
extern void closeWindows();
extern tWindow *windowLookup(int tag);

//Mouse pointer variables:
extern void refreshMousePointer();
extern void destroyMousePointer();
extern void drawMousePointer();
/****************************************************************************/
#endif
