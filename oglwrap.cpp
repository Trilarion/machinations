/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* OGLWRAP.CPP:  Simple OpenGL wrapper for optimization and debugging.       *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#ifndef CACHE_HEADERS
    #ifdef WINDOWS
        #include <windows.h> //for wglGetProcAddress
    #endif
#endif

#include "oglwrap.h"
#include "types.h"
#include "engine.h"
#pragma package(smart_init)

/****************************************************************************
                             Static Variables
 ****************************************************************************/
//Each texel unit has its own texture.  I currently support up to 4 texel units.
static iTextureIndex currentTexture[4]={~0,~0,~0,~0}; //Changed from -1 since currentTexture is unsigned.
static int activeTexture=0;

static void displayGLExtensions();
static void initMultitexture();
static void initTexture3D();

/****************************************************************************
                             Global Variables
 ****************************************************************************/
//Initialized in initGL via call to glGetString(GL_EXTENSIONS)
bool multitextureSupported=false;
bool texture3DSupported=false;

#ifndef LINUX
    //These function pointers are only valid if multitextureSupported is true:
    PFNGLMULTITEXCOORD1FARBPROC _glMultiTexCoord1fARB = NULL;
    PFNGLMULTITEXCOORD2FARBPROC _glMultiTexCoord2fARB = NULL;
    PFNGLMULTITEXCOORD3FARBPROC _glMultiTexCoord3fARB = NULL;
    PFNGLMULTITEXCOORD4FARBPROC _glMultiTexCoord4fARB = NULL;
    PFNGLACTIVETEXTUREARBPROC _glActiveTextureARB = NULL;
    PFNGLCLIENTACTIVETEXTUREARBPROC	_glClientActiveTextureARB = NULL;
    //These function pointers are only valid if texture3DSupported is true:
    PFNGLTEXIMAGE3DPROC _glTexImage3D = NULL;
#endif

int maxTexelUnits=1; //Every computer supports at least one texture



/****************************************************************************
                              Global Functions
 ****************************************************************************/
//This macro lets us easily determine whether we need to call glEnable
//or glDisable.  We use a macro instead of a function so that we can
//associate a tag with a variable name.
#define ENABLECASE(tag,var)\
case tag:\
    if(var!=val)\
    {\
        var=val;\
        if(var)\
            glEnable(tag);\
        else\
            glDisable(tag);\
    }\
    break;



/***************************************************************************\
mglBindTexture

This function attempts to optimize OpenGL texture binding.  First, it enables
GL_TEXTURE_2D.  Then, it compares the new texture index with the prior texture
index and calls glBindTexture only if the indices differ.
Inputs:
    index   Index of the texture to which to bind
Outputs:
    none
\***************************************************************************/
void mglBindTexture(GLuint index)
{
    if(index!=currentTexture[activeTexture])
    {
        glBindTexture(GL_TEXTURE_2D, index);
        currentTexture[activeTexture]=index;
    }
}

/***************************************************************************\
calcNormal

Calculates a unit vector that is normal to the plane passing through the
three specified points.

Inputs:
    in0, in1, in2   The x-, y- and z-components of three vertices that
                        uniquely define a plane
Outputs:
    out             The x-, y- and z-components of the normal vector
\***************************************************************************/
void calcNormal(float in0[3], float in1[3], float in2[3], float out[3])
{
    float v1[3],v2[3];
    static const int x = 0;
    static const int y = 1;
    static const int z = 2;

    // Calculate two vectors from the three points:
    v1[x] = in0[x] - in1[x];
    v1[y] = in0[y] - in1[y];
    v1[z] = in0[z] - in1[z];
    v2[x] = in1[x] - in2[x];
    v2[y] = in1[y] - in2[y];
    v2[z] = in1[z] - in2[z];

    // Take the cross product of the two vectors to get
    // the normal vector which will be stored in out[]:
    out[x] = v1[y]*v2[z] - v1[z]*v2[y];
    out[y] = v1[z]*v2[x] - v1[x]*v2[z];
    out[z] = v1[x]*v2[y] - v1[y]*v2[x];

    // Normalize the vector:
    float dist = sqrt(out[0] * out[0] + out[1] * out[1] + out[2] * out[2]);
    if(dist>0)
    {
        out[0]/=dist;
        out[1]/=dist;
        out[2]/=dist;
    }
}

/***************************************************************************\
drawButton

Creates the illusion of an embossed or ingraved rectangular region.  The
function achieves this effect by drawing the left and top edges one color,
the button face another color, and the right and bottom edges a third color.
The following diagram illustrates the appearance of the button.

                |<--            width            -->|
              - +-----------------------------------+
              ^ | color1                          / |
                |   +---------------------------+   |
                |   | color2                    |   |
         height |   |                           |   |
                |   |                           |   |
                |   +---------------------------+   |
              , | /   color3                        | } bevel
              - +-----------------------------------+
                |<->| bevel

Inputs:
    x,y             Coordinates of bottom-left corner
    width,height    Dimensions, -including- the bevel
    bevel           Breadth of highlight around edges
    r1,g1,b1        Red, green, and blue components of color1
    r2,g2,b2         "     "         "       "      "  color2
    r3,g3,b3         "     "         "       "      "  color3
Outputs:
    none
\***************************************************************************/
void drawButton(int x, int y, int width, int height, int bevel,
    float r1, float g1, float b1, float r2, float g2, float b2, float r3, float g3, float b3)
{
    glColor3f(r1,g1,b1);
    glBegin(GL_QUADS);
        glVertex2i(x,y+height);
        glVertex2i(x+width,y+height);
        glVertex2i(x+width-bevel, y+height-bevel);
        glVertex2i(x+bevel, y+height-bevel);

        glVertex2i(x,y+height);
        glVertex2i(x,y);
        glVertex2i(x+bevel,y+bevel);
        glVertex2i(x+bevel,y+height-bevel);
    glEnd();

    glColor3f(r2,g2,b2);
    glBegin(GL_QUADS);
        glVertex2i(x+width,y);
        glVertex2i(x+width,y+height);
        glVertex2i(x+width-bevel, y+height-bevel);
        glVertex2i(x+width-bevel, y+bevel);

        glVertex2i(x+width,y);
        glVertex2i(x,y);
        glVertex2i(x+bevel,y+bevel);
        glVertex2i(x+width-bevel,y+bevel);
    glEnd();

    glColor3f(r3,g3,b3);
    glBegin(GL_QUADS);
        glVertex2i(x+bevel,y+bevel);
        glVertex2i(x+width-bevel,y+bevel);
        glVertex2i(x+width-bevel,y+height-bevel);
        glVertex2i(x+bevel,y+height-bevel);
    glEnd();
}

/***************************************************************************\
calcVertex

mglTorus uses this function to calculate one vertex of a torus.

Inputs:
    r0      Primary radius in the x-y plane
    r1      Secondary radius in the xy-z plane
    alpha   Primary angle of rotation about the z-axis (radians)
    beta    Secondary angle of rotation about the xy-axis (radians)
Outputs:
    v[3]    The x-, y- and z-components of the vertex
\***************************************************************************/
static inline void calcVertex(float r0, float r1, float alpha, float beta, float v[3])
{
    float theta = r0+r1*cos(beta);
    v[0]=theta*cos(alpha);
    v[1]=theta*sin(alpha);
    v[2]=r1*sin(beta);
}

/***************************************************************************\
mglTorus

Draws a torus centered at the origin which forms a loop around the z-axis.
Similar to glutCylinder et al, the function draws the torus using the current
OpenGL state variables.  It generates normals for each face, but does not
support textures.  The torus begins at the positive X axis and curls
counter-clockwise about the Z axis by the specified angle.  Set angle to
2pi for a complete torus.  If the angle is less than 2pi, the function draws
a partial torus with a bounding disc on both ends.

Inputs:
    r0          Primary radius in the x-y plane
    r1          Secondary radius in the xy-z plane
    slices      Number of subdivisions around a cross-section
    loops       Number of subdivisions around the whole torus
    angle       The angle the torus makes around the z-axis
Outputs:
    none
\***************************************************************************/
void mglTorus(float r0, float r1, int slices, int loops, float angle)
{
    int ialpha, ibeta;
    float alpha, beta, alpha2, beta2;
    float v0[3], v1[3], v2[3], n[3];
#ifdef DEBUG
    if(slices<=0)
    {
        bug("mglTorus: invalid slices");
        return;
    }
    if(loops<=0)
    {
        bug("mglTorus: invalid loops");
        return;
    }
    if(angle<0 || angle>6.2832)
    {
        bug("mglTorus: invalid angle");
        return;
    }
#endif
    float dalpha=angle/loops;
    float dbeta=M_PI*2/slices;

    glBegin(GL_TRIANGLES);
    for(ialpha=0; ialpha<loops; ialpha++)
    {
        alpha=ialpha*dalpha;
        alpha2=(ialpha+1)*dalpha;
        for(ibeta=0; ibeta<slices; ibeta++)
        {
            beta=ibeta*dbeta;
            beta2=(ibeta+1)*dbeta;
            calcVertex(r0, r1, alpha, beta, v0);
            calcVertex(r0, r1, alpha2, beta, v1);
            calcVertex(r0, r1, alpha2, beta2, v2);
            calcNormal(v0,v1,v2,n);
            glNormal3fv(n);
            glVertex3fv(v0);
            glVertex3fv(v1);
            glVertex3fv(v2);

            calcVertex(r0, r1, alpha, beta, v0);
            calcVertex(r0, r1, alpha2, beta2, v1);
            calcVertex(r0, r1, alpha, beta2, v2);
            calcNormal(v0,v1,v2,n);
            glNormal3fv(n);
            glVertex3fv(v0);
            glVertex3fv(v1);
            glVertex3fv(v2);
        }
    }
    glEnd();
    //draw bounding discs on both ends
    if(angle<M_PI*2)
    {
        glPushMatrix();
        glTranslatef(r0,0,0);
        glRotatef(-90,1,0,0);
        gluDisk(globalQuadricObject,0,r1,slices,1);
        glPopMatrix();
        glPushMatrix();
        glRotatef(angle*180/M_PI,0,0,1);
        glTranslatef(r0,0,0);
        glRotatef(90,1,0,0);
        gluDisk(globalQuadricObject,0,r1,slices,1);
        glPopMatrix();
    }
}

/***************************************************************************\
mglPrism

Draws a prism centered at the origin with texture coordinates and normals.
The prism extends x units along the positive x-axis and x units along the
negative x-axis.  Thus, the width of the prism is 2x.  The same holds for y
and z.

Inputs:
	x	The maximum x-coordinate
	y	The maximum y-coordinate
	z	The maximum z-coordinate
Outputs:
    none
\***************************************************************************/
void mglPrism(float x, float y, float z)
{
    glBegin(GL_QUADS);
        glNormal3f(0,0,1);
        glTexCoord2f(0,0);
        glVertex3f(-x,-y,z);
        glTexCoord2f(1,0);
        glVertex3f(x,-y,z);
        glTexCoord2f(1,1);
        glVertex3f(x,y,z);
        glTexCoord2f(0,1);
        glVertex3f(-x,y,z);

        glNormal3f(0,0,-1);
        glTexCoord2f(0,0);
        glVertex3f(-x,-y,-z);
        glTexCoord2f(1,0);
        glVertex3f(-x,y,-z);
        glTexCoord2f(1,1);
        glVertex3f(x,y,-z);
        glTexCoord2f(0,1);
        glVertex3f(x,-y,-z);

        glNormal3f(0,1,0);
        glTexCoord2f(0,0);
        glVertex3f(-x,y,-z);
        glTexCoord2f(1,0);
        glVertex3f(-x,y,z);
        glTexCoord2f(1,1);
        glVertex3f(x,y,z);
        glTexCoord2f(0,1);
        glVertex3f(x,y,-z);

        glNormal3f(0,-1,0);
        glTexCoord2f(0,0);
        glVertex3f(-x,-y,-z);
        glTexCoord2f(1,0);
        glVertex3f(x,-y,-z);
        glTexCoord2f(1,1);
        glVertex3f(x,-y,z);
        glTexCoord2f(0,1);
        glVertex3f(-x,-y,z);

        glNormal3f(1,0,0);
        glTexCoord2f(0,0);
        glVertex3f(x,-y,-z);
        glTexCoord2f(1,0);
        glVertex3f(x,y,-z);
        glTexCoord2f(1,1);
        glVertex3f(x,y,z);
        glTexCoord2f(0,1);
        glVertex3f(x,-y,z);

        glNormal3f(-1,0,0);
        glTexCoord2f(0,0);
        glVertex3f(-x,-y,-z);
        glTexCoord2f(1,0);
        glVertex3f(-x,-y,z);
        glTexCoord2f(1,1);
        glVertex3f(-x,y,z);
        glTexCoord2f(0,1);
        glVertex3f(-x,y,-z);
    glEnd();
}

void mglFuzzyDisk(float inner, float outer, int slices)
{
    float a=0,da=M_PI*2/slices;
    float x=1,y=0;
    float clr[4];
    int i;
    glGetFloatv(GL_CURRENT_COLOR,clr);
    glBegin(GL_QUADS);
    for(i=0;i<slices;i++)
    {
        glVertex2f(x*inner,y*inner);
        glColor4f(0,0,0,0);
        glVertex2f(x*outer,y*outer);
        a+=da;
        x=lookupCos(a);
        y=lookupSin(a);
        glVertex2f(x*outer,y*outer);
        glColor4fv(clr);
        glVertex2f(x*inner,y*inner);
    }
    glEnd();

    a=0;
    glBegin(GL_TRIANGLE_FAN);
    glVertex2f(0,0);
    glVertex2f(inner,0);
    for(i=0;i<slices;i++)
    {
        a+=da;
        glVertex2f(lookupCos(a)*inner,lookupSin(a)*inner);
    }
    glEnd();
}

static void displayGLExtensions()
{
    char *extensions;

    extensions=(char*)glGetString(GL_EXTENSIONS);
    log("This system supports the following extensions: %s",extensions);
}

static void initMultitexture()
{
    multitextureSupported=
        glfwExtensionSupported("GL_ARB_multitexture") && glfwExtensionSupported("GL_EXT_texture_env_combine");

    if(multitextureSupported)
    {
        glGetIntegerv(GL_MAX_TEXTURE_UNITS_ARB,&maxTexelUnits);
        log("This graphics card supports %d texture units.", maxTexelUnits);

#ifndef LINUX
        _glMultiTexCoord1fARB	= (PFNGLMULTITEXCOORD1FARBPROC)		glfwGetProcAddress("glMultiTexCoord1fARB");
        _glMultiTexCoord2fARB	= (PFNGLMULTITEXCOORD2FARBPROC)		glfwGetProcAddress("glMultiTexCoord2fARB");
        _glMultiTexCoord3fARB	= (PFNGLMULTITEXCOORD3FARBPROC)		glfwGetProcAddress("glMultiTexCoord3fARB");
        _glMultiTexCoord4fARB	= (PFNGLMULTITEXCOORD4FARBPROC)		glfwGetProcAddress("glMultiTexCoord4fARB");
        _glActiveTextureARB		= (PFNGLACTIVETEXTUREARBPROC)		glfwGetProcAddress("glActiveTextureARB");
        _glClientActiveTextureARB=(PFNGLCLIENTACTIVETEXTUREARBPROC) glfwGetProcAddress("glClientActiveTextureARB");

        if(_glMultiTexCoord1fARB==NULL ||
            _glMultiTexCoord2fARB==NULL ||
            _glMultiTexCoord3fARB==NULL ||
            _glMultiTexCoord4fARB==NULL ||
            _glActiveTextureARB==NULL ||
            _glClientActiveTextureARB==NULL)
        {
            bug("Error occurred when retrieving OpenGL extension pointers.");
            multitextureSupported=false;
        }
#endif
    }
    else
        log("This computer does not support multitexturing.");
}

static void initTexture3D()
{
    texture3DSupported=glfwExtensionSupported("GL_EXT_texture3D");

    if(texture3DSupported)
    {
#ifndef LINUX
        _glTexImage3D=(PFNGLTEXIMAGE3DPROC)glfwGetProcAddress("glTexImage3D");

        if(_glTexImage3D==NULL)
        {
            bug("Error occurred when retrieving OpenGL extension pointers.");
            texture3DSupported=false;
        }
#endif
    }
    else
        log("This computer does not support 3D textures.");
}

void findShadowMatrix(const float ground[4], const float light[4], float shadowMat[16])
{
    float  dot;

    dot = ground[0] * light[0] +
          ground[1] * light[1] +
          ground[2] * light[2] +
          ground[3] * light[3];

    shadowMat[0] = dot - light[0] * ground[0];
    shadowMat[4] = 0.0 - light[0] * ground[1];
    shadowMat[8] = 0.0 - light[0] * ground[2];
    shadowMat[12] = 0.0 - light[0] * ground[3];

    shadowMat[1] = 0.0 - light[1] * ground[0];
    shadowMat[5] = dot - light[1] * ground[1];
    shadowMat[9] = 0.0 - light[1] * ground[2];
    shadowMat[13] = 0.0 - light[1] * ground[3];

    shadowMat[2] = 0.0 - light[2] * ground[0];
    shadowMat[6] = 0.0 - light[2] * ground[1];
    shadowMat[10] = dot - light[2] * ground[2];
    shadowMat[14] = 0.0 - light[2] * ground[3];

    shadowMat[3] = 0.0 - light[3] * ground[0];
    shadowMat[7] = 0.0 - light[3] * ground[1];
    shadowMat[11] = 0.0 - light[3] * ground[2];
    shadowMat[15] = dot - light[3] * ground[3];
}

void multShadowMatrix(const float ground[4], const float light[4])
{
    float shadowMat[16];
    findShadowMatrix(ground,light,shadowMat);
    glMultMatrixf((const GLfloat*)shadowMat);
}

void mglSetActiveTexture(int _activeTexture)
{
#ifdef DEBUG
    if(_activeTexture<0 || _activeTexture>=sizeof(currentTexture))
    {
        bug("mglSetActiveTexture: invalid index");
        return;
    }
#endif

    activeTexture=_activeTexture;
    switch(activeTexture)
    {
        case 0:
            _glActiveTextureARB(GL_TEXTURE0_ARB);
            break;
        case 1:
            _glActiveTextureARB(GL_TEXTURE1_ARB);
            break;
        case 2:
            _glActiveTextureARB(GL_TEXTURE2_ARB);
            break;
        case 3:
            _glActiveTextureARB(GL_TEXTURE3_ARB);
            break;
        default:
            bug("mglSetActiveTexture: invalid index");
            break;
    }
}

void openGLStartup()
{
    GLint i;

    displayGLExtensions();
    initMultitexture();
    //initTexture3D();

    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &i);
    log("The maximum texture size on this system is %d pixels",i);
    glGetIntegerv(GL_MAX_PROJECTION_STACK_DEPTH, &i);
    log("The maximum projection stack depth on this system is %d matrices",i);
    glGetIntegerv(GL_MAX_MODELVIEW_STACK_DEPTH, &i);
    log("The maximum model view stack depth on this system is %d matrices",i);

    openGLRefresh();
}
void openGLRefresh()
{
    float ambient[4]={.3,.3,.3,1};
    float diffuse[4]={.5,.5,.5,1};

	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black Background
	glClearDepth(1.0f);									// Depth Buffer Setup
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
    glAlphaFunc(GL_GREATER, 0.1);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glCullFace(GL_BACK);

    mglSetActiveTexture(0);
    for(int i=0; i<4; i++)
        currentTexture[i]=(~0); //Changed from -1 since currentTexture is unsigned.
}
void openGLShutdown()
{
}

/* This lovely piece of code is a temporary fix for a problem I'm having with my
 * ATI Radeon 8500 video card.  When using glCopySubTexImage2D in conjunction
 * with multitexturing, the video card draws strange colorful patterns over the
 * terrain.  I'm nearly certain that the problem is related to the video card;
 * however, I have not found any related information on the net.  The function
 * seems to purge the multitexturing pipeline so that the next multitextured
 * polygon appears correctly.
 */
void ATIRadeon8500Hack()
{
    int i;
    GLboolean colorWriteMask[4],depthWriteMask;

    for(i=0;i<3;i++)
    {
        mglSetActiveTexture(i);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, 1);
    }

    glGetBooleanv(GL_COLOR_WRITEMASK,colorWriteMask);
    glGetBooleanv(GL_DEPTH_WRITEMASK,&depthWriteMask);
    glColorMask(false,false,false,false);
    glDepthMask(false);
    glRecti(0,0,1,1);
    glColorMask(colorWriteMask[0],colorWriteMask[1],colorWriteMask[2],colorWriteMask[3]);
    glDepthMask(depthWriteMask);

    for(i=0;i<3;i++)
    {
        mglSetActiveTexture(i);
        glDisable(GL_TEXTURE_2D);
    }
}
GLenum getLightName(int i)
{
    switch(i)
    {
        case 0: return GL_LIGHT0;
        case 1: return GL_LIGHT1;
        case 2: return GL_LIGHT2;
        case 3: return GL_LIGHT3;
        case 4: return GL_LIGHT4;
        case 5: return GL_LIGHT5;
        case 6: return GL_LIGHT6;
        case 7: return GL_LIGHT7;
    }
    bug("getLightName: invalid index");
    return GL_LIGHT0;
}
