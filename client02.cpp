/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* CLIENT02.CPP: Machinations user-interface: creates controls and handles   *
*               events.                                                     *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as publiFshed by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#include <GL/glfw.h>
#include <IL/ilut.h>
#ifndef CACHE_HEADERS
    #include <ctype.h>
#endif

#include "client02.h"
#include "font.h"
#include "memwatch.h"
#include "oglwrap.h"
#include "game.h"
#include "message.h"
#include "unit.h"
#include "parse.h"
#ifndef BORLAND
    #include "snprintf.h"
#endif
#pragma package(smart_init)

/****************************************************************************
                            Static Variables
 ****************************************************************************/
static tMinimap*        minimap = NULL;
static tGameWindow*     gameWindow = NULL;
static tCommandPanel*   commandPanel = NULL;
static tChatWindow*     chatWindow = NULL;
static long             chosenIP = 0;
static int              chosenPort = 4000;
static bool             hasChosenTCPIP = 0;

static eCommand         curCommand = COMMAND_NONE;
static eParamType       paramType = PARAM_NONE;
static bool             firstTarget = false;

static int              enableCounter[MAX_COMMANDS];
static int              highlightCounter[MAX_COMMANDS];
static int              quantityCounter[MAX_COMMANDS];
static int              repeatCounter[MAX_COMMANDS];

static tFrameRate       FPS("Frame Rate");

static float            menuCubeMatrix[16]={1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
static tTexture*        menuCubeTexture=NULL;

/****************************************************************************
                            Static Functions
 ****************************************************************************/
static void             abortSession(int tag);
static void             cancelConnect(int tag);
static tWindow*         displayWindow(int tag);
static tMessageBox*     messageBox(const tString& message, const tString& title,
                            eChoices choices, void (* event)(int)=NULL);

//By default, executing a production command will add one unit to the production
//queue.  (This applies to keyboard shortcuts only.)
static void             setCurCommand(eCommand command, int quantity = 1, bool repeat = false);
static void             resetFirstTarget();

static void             showMessage(const tString& str, eTextMessage type);
static tString          formatMessage(const tString& str, eTextMessage type);
static void             parseEntry(const tString& s, bool inGame) throw(tString);
static bool             displayHelp(tStream& s, const char *keyword) throw(tString);

static void             restartGame();

/****************************************************************************
                           Global Variables
 ****************************************************************************/
float           panRate = 1;         //Rate of keyboard and mouse pan
float           spinRate = 2;        //Rate at which view spins (rads/sec)
float           zoomRate = .5;       //Exponential constant for zooming view
float           tiltRate = 1;        //Rate at which view tilts (rads/sec)
float           mouseZoomRate = 0.1; //Rate at which rolling the mouse wheel zooms the map
float           mouseSpinRate = 0.03;//Rate that moving the mouse along the x-axis spins the map (pixels/rad)
float           mouseTiltRate = 0.03;//Rate that moving the mouse along the y-axis tilts the map (pixels/rad)

/****************************************************************************/
// PARSERS
/****************************************************************************/
void parseEntry(const tString& string, bool inGame) throw(tString)
{
    tString username,filename;
    tUser *user;
    tFile f;

    tStream s;
    s.load(CHARPTR(string));
    tString tag=parseString("tag", s);
    if(strPrefix(tag,"sendfile"))
    {
        username=parseString("username", s);
        user=userLookup(username,/*exact=*/false);

        if(user==NULL || user->descriptor==NULL)
            throw tString("Invalid user");

        filename=parseString("filename", s);
        f.setName(filename);
        if(!f.exists())
            throw tString("File not found");
        sendFile(user->descriptor,f);
    }
    else if(strPrefix(tag,"drop"))
    {
        username=parseString("username", s);
        user=userLookup(username,/*exact=*/false);
        if(user&&user->descriptor)
            dropUser(user);
        else
            throw tString("Invalid user");
    }
    else if(strPrefix(tag,"simerror"))
    {
        username=parseString("username", s);
        user=userLookup(username,/*exact=*/false);
        if(user&&user->descriptor)
            simulateError(user->descriptor);
        else
            throw tString("Invalid user");
    }
    else if(strPrefix(tag,"help"))
    {
        FILE *helpFile=fopen(HELP_PATH "help.dat", "r");
        tStream helpStream;
        bool matchFound=false;

        if(helpFile==NULL)
            throw tString("Failed to load '")+(HELP_PATH "help.dat")+"'";

        try
        {
            helpStream.load(helpFile);
            if(s.hasOptionalParameter())
            {
                const char* topic=CHARPTR(parseString("parameter", s));
                matchFound=displayHelp(helpStream, topic);
            }
            else
            {
                if(inGame)
                    matchFound=displayHelp(helpStream, "game_help");
                else
                    matchFound=displayHelp(helpStream, "setup_help");
            }
        }
        catch(const tString& message)
        {
            fclose(helpFile);
            throw helpStream.parseErrorMessage(HELP_PATH "help.dat", message);
        }
        fclose(helpFile);

        if(!matchFound)
            throw tString("No help on this topic found.");
    }
    else if(strPrefix(tag,"clear"))
    {
        if(inGame)
        {
            tWindow *window=windowLookup(CHAT_WINDOW);
            if(window)
                ((tChatWindow *) window)->clearMessages();
            if(gameWindow)
                gameWindow->clearMessages();
        }
        else
        {
            tWindow *window=windowLookup(SETUP_GAME_MENU);
            if(window)
                ((tSetupMenu *) window)->clearMessages();
        }
    }
    else if(inGame)
        parseShellCommand(string);
    else
        throw tString("Unrecognized tag '")+tag+"'";
}
bool displayHelp(tStream& s, const char *keyword) throw(tString)
{
    bool matchFound;
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);

    while(!s.atEndOfStream())
    {
        matchFound=false;
        do //at least one keyword on the first line
        {
            tString str=parseString("keyword", s);
            if(strPrefix(keyword,str))
            {
                matchFound=true;
                break;
            }
        }
        while(!s.atEndOfLine());

        tString text=parseString("text",s);
		if(matchFound)
		{
            tStringBuffer buf(text);
            char *ptr1=buf.begin(),*ptr2;
			while(true)
			{
				ptr2=strchr(ptr1,'\n');
				if(ptr2)
					*ptr2='\0';

				if(setupMenu && setupMenu->isVisible())
					setupMenu->onShowMessage(ptr1,TEXT_MESSAGE_STATUS);
				else if(chatWindow && chatWindow->isVisible())
					chatWindow->onShowMessage(ptr1,TEXT_MESSAGE_STATUS);

				if(ptr2)
					ptr1=ptr2+1;
				else
					break;
			}

			return true;
		}
    }
    return false;
}
/****************************************************************************/
// MESSAGE BOX
/****************************************************************************/
tMessageBox::tMessageBox(const tStringList& message, const tString& title,
    eChoices choices, void (* _event)(int)) : tStdDialogBox(title, mediumFont,
    MAX3(textWidth(message,mediumFont)+20,stringWidth(CHARPTR(title),mediumFont)+20,200),
    100+message.size()*fontHeight(mediumFont), WINDOW_NORMAL, true, true, true, false,
    false, false, false, -1,-1,-1,-1), event(_event), def(NULL)
{
    tButton *bt;
    tControl *c;
    int y=getHeight()-70,i;
    for(i=0;i<message.size();i++)
    {
        c=mwNew tStdLabel(0,y,getWidth(),fontHeight(mediumFont),message[i],mediumFont,1,1,1);
        createControl(c);
        y-=fontHeight(mediumFont);
    }
    switch(choices)
    {
        case CHOICES_OK:
            bt = mwNew tStdButton(0,0,getWidth(),50,"OK",mediumFont,false,
                (tEvent) &tMessageBox::onClick);
            createControl(bt);
            bt->tag=OK_BUTTON;
            def=bt;
            break;
        case CHOICES_CANCEL:
            bt = mwNew tStdButton(0,0,getWidth(),50,"Cancel",mediumFont,false,
                (tEvent) &tMessageBox::onClick);
            createControl(bt);
            bt->tag=CANCEL_BUTTON;
            def=bt;
            break;
    }
    setDefault(def);
}
/****************************************************************************/
void tMessageBox::onClick(tControl *sender)
{
    close();
    if(event)
        (*event)(sender->tag);
}
/****************************************************************************/
void tMessageBox::onHide()
{
    tStdDialogBox::onHide();
}
/****************************************************************************/
// STATIC RECTANGLE
/****************************************************************************/
void tStaticRect::drawControl()
{
    int x=getControlX(), y=getControlY();
    int w=getWidth(), h=getHeight();
    drawButton(x,y,w,h,2,.4,.4,.4,.9,.9,.9,0,0,0);
    /* Old Code for drawing solid white border
    glColor3f(1,1,1);
    glBegin(GL_QUADS);
        w=margin;
        glVertex2i(x,y);
        glVertex2i(x+w,y);
        glVertex2i(x+w,y+h);
        glVertex2i(x,y+h);

        x+=getWidth()-margin;
        glVertex2i(x,y);
        glVertex2i(x+w,y);
        glVertex2i(x+w,y+h);
        glVertex2i(x,y+h);

        x=getControlX();
        w=getWidth();
        h=margin;
        glVertex2i(x,y);
        glVertex2i(x+w,y);
        glVertex2i(x+w,y+h);
        glVertex2i(x,y+h);

        y+=getHeight()-margin;
        glVertex2i(x,y);
        glVertex2i(x+w,y);
        glVertex2i(x+w,y+h);
        glVertex2i(x,y+h);
    glEnd();
    */
}
/****************************************************************************/
// MY EDIT
/****************************************************************************/
void tMyEdit::onKeyDown(tKey key, char ch)
{
    tEdit::onKeyDown(key,ch);
    if(key==GLFW_KEY_ENTER && getParent() && getParent()->isType(CTRL_WINDOW) && event && STRLEN(getString())>0)
    {
        history.push_back(getString());
        if(history.size()>MAX_HISTORY)
            history.erase(history.begin());
        historyIndex=history.size();
        (((tWindow *)getParent())->*event)(this);
    }
    else if(key==GLFW_KEY_DOWN && historyIndex+1<history.size())
    {
        historyIndex++;
        setString(history[historyIndex]);
    }
    else if(key==GLFW_KEY_UP && historyIndex>0)
    {
        historyIndex--;
        setString(history[historyIndex]);
    }
}
/****************************************************************************/
// MY COMBO BOX
/****************************************************************************/
void tMyComboBox::onChange()
{
    tUser *user=NULL;
    ePlayer type;
    switch(getChoice())
    {
        case 0:
            type=PLAYER_OPEN;
            break;
        case 1:
            type=PLAYER_CLOSED;
            break;
        case 2:
            type=PLAYER_COMPUTER;
            break;
        default:
            type=PLAYER_HUMAN;
            user=userLookup(getChoiceString(),/*exact=*/true);
            break;
    }
    setPlayer(index, type, user);
}
/****************************************************************************/
// COMMAND BUTTON
/****************************************************************************/
void tCommandButton::drawControl()
{
    int bevel;
    float c1,c2,c3;
    char buf[10];

    if(isDisabled())
        bevel=0;
    else if(active && !isDepressed())
        bevel=1;
    else
        bevel=2;

    if(isDepressed() || active&&!isDisabled())
    {
        c1=.6;
        c2=.8;
        c3=.7;
    }
    else
    {
        c1=.9;
        c2=.5;
        c3=.7;
    }
    if(highlight)
    {
        c1+=.1;
        c2+=.1;
        c3+=.2;
    }
    else if(isSelected())
    {
        c1+=.1;
        c2+=.1;
        c3+=.1;
    }
    if(repeat>0)
        //Draw button in red.
        drawButton(getControlX(),getControlY(),getWidth(),getHeight(),bevel,c1,0,0,c2,0,0,c3,0,0);
    else
        //Draw button in grey.
        drawButton(getControlX(),getControlY(),getWidth(),getHeight(),bevel,c1,c1,c1,c2,c2,c2,c3,c3,c3);

    int x=getControlX()+2;
    int y=getControlY()+2;
    if(isDepressed() || active&&!isDisabled())
    {
        x++;
        y--;
    }

    if(texture)
    {
        glColor3f(1,1,1);
        drawTexture(texture,x,y);
    }

    if(quantity>0 || repeat>0)
    {
        if(quantity>0)
        {
            if(repeat>0)
                snprintf(buf,sizeof(buf),"%d(+%d)",quantity,repeat);
            else
                snprintf(buf,sizeof(buf),"%d",quantity);
        }
        else
            snprintf(buf,sizeof(buf),"(+%d)",repeat);

        glColor3f(.5,.5,.5);
        drawString(buf,x+1,y,smallFont);
        glColor3f(1,1,1);
        drawString(buf,x+0,y+1,smallFont);
    }
}
/****************************************************************************/
void tCommandButton::drawHint()
{
    tStringList s;
    loadStringList(s,CHARPTR(commandTable[tag].caption));

    int w=textWidth(s,smallFont)+2;
    int h=s.size()*fontHeight(smallFont)+2;

    int x=getControlX()+(getWidth()-w)/2;
    int y=getControlY()+getHeight()-fontHeight(smallFont);

    //Ensure that caption is fully visible:
    clampRange(x,0,getScreenWidth()-w);

    glEnable(GL_BLEND);
    glColor4f(0,0,0,.5);
    glBegin(GL_QUADS);
        glVertex2i(x,y);
        glVertex2i(x+w,y);
        glVertex2i(x+w,y+h);
        glVertex2i(x,y+h);
    glEnd();
    glDisable(GL_BLEND);

    x++;    y++;
    glColor3f(1,1,1);
    drawText(s,x,y+h-2,x,y,w,h,smallFont);
}
/****************************************************************************/
void tCommandButton::onMouseDown(int x, int y, eMouseButton button)
{
    tButton::onMouseDown(x,y,button);
    leftPressed=isLeftButtonPressed();
    rightPressed=isRightButtonPressed();
}
/****************************************************************************/
void tCommandButton::onExecute()
{
    //Quantity and repeat are used for production orders only.
    if(leftPressed && rightPressed) //User has pressed both mouse buttons at once
        setCurCommand((eCommand)tag,/*quantity=*/1,/*repeat=*/true);
    else if(leftPressed)
        setCurCommand((eCommand)tag,/*quantity=*/1,/*repeat=*/false);
    else if(rightPressed)
        setCurCommand((eCommand)tag,/*quantity=*/-1,/*repeat=*/false);

    leftPressed=false;
    rightPressed=false;
}
/****************************************************************************/
tCommandButton::tCommandButton(int controlX, int controlY, int _tag) :
    tButton(controlX, controlY, COMMAND_SIZE+4, COMMAND_SIZE+4, false, false),
    active(false), highlight(false), repeat(0), quantity(0), texture(NULL),
    leftPressed(false), rightPressed(false)
{
    tag=_tag;
#ifdef DEBUG
    if(tag<0)
    {
        bug("CommandButton::tCommandButton: tag<0");
        tag=0;
    }
    if(tag>=commandCount)
    {
        bug("CommandButton::tCommandButton: tag>=commandCount");
        tag=commandCount-1;
    }
#endif

    if(!commandTable[tag].interfaceCommand)
        onDisable();
    texture=getTexture(tString("commands.")+commandTable[tag].name);
}
/****************************************************************************/
tCommandButton::~tCommandButton()
{
    if(texture)
        releaseTexture(texture);
}
/****************************************************************************/
void tCommandButton::reset()
{
    active=false;
    highlight=false;
    repeat=0;
    quantity=0;
    if(!commandTable[tag].interfaceCommand)
        disable();
}
/****************************************************************************/
// PROGRESS BAR
/****************************************************************************/
tProgressBar::tProgressBar(const tString& message) : tStdDialogBox(message,
    smallFont, 300, 80, WINDOW_NORMAL, /*modal=*/true, /*destroyOnClose=*/true,
    /*moveable=*/false, /*resizeable=*/false, /*maximizeable=*/false, /*minimizeable=*/false,
    /*closeable=*/false, -1,-1,-1,-1), progress(0)
{
    tag=PROGRESS_BAR_WINDOW;
}
/****************************************************************************/
void tProgressBar::drawWindow()
{
    tStdDialogBox::drawWindow();

    int w=getWidth()-40,h=getHeight()-getTitleBarHeight()-40;
    int x=(int)(w*progress);

    glPushMatrix();
    glTranslatef(getControlX()+20,getControlY()+20,0);
    drawButton(0,0,w,h,2,.5,.5,.5,.9,.9,.9,.65,.65,.65);

    if(x>4)
    {
        glColor3f(0,0,1);
        glRecti(2,2,x-2,h-2);
    }
    glPopMatrix();
}
/****************************************************************************/
// 3D WINDOW
/****************************************************************************/
t3DWindow::t3DWindow(int width, int height, bool modal, bool background, bool destroyOnClose) :
    tWindow(0,0,width,height,modal,background,destroyOnClose)
{
    int i;
    for(i=0;i<16;i++)
    {
        if(i%5==0)
            matrix[i]=1;
        else
            matrix[i]=0;
    }
}
bool t3DWindow::objectActivated(int x, int y)
{
    float fx,fy;
    windowXYToModelXY(x,y,fx,fy,matrix);
    return tWindow::objectActivated((int)fx,(int)fy);
}
void t3DWindow::draw()
{
    glPushMatrix();
    glMultMatrixf(matrix);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(false);
    tWindow::draw();
    glDepthMask(true);
    glDisable(GL_DEPTH_TEST);
    glPopMatrix();
}
void t3DWindow::onMouseDown(int x, int y, eMouseButton button)
{
    float fx,fy;
    windowXYToModelXY(x,y,fx,fy,matrix);
    tWindow::onMouseDown((int)fx,(int)fy,button);
}
void t3DWindow::onMouseMove(int x, int y)
{
    float fx,fy;
    windowXYToModelXY(x,y,fx,fy,matrix);
    tWindow::onMouseMove((int)fx,(int)fy);
}
void t3DWindow::onMouseUp(int x, int y, eMouseButton button)
{
    float fx,fy;
    windowXYToModelXY(x,y,fx,fy,matrix);
    tWindow::onMouseUp((int)fx,(int)fy,button);
}
void t3DWindow::onSelect(int x, int y)
{
    float fx,fy;
    windowXYToModelXY(x,y,fx,fy,matrix);
    tWindow::onSelect((int)fx,(int)fy);
}
void t3DWindow::setMatrix(float _matrix[])
{
#ifdef DEBUG
    if(ARRAY_VIOLATION(_matrix,16))
        return;
#endif
    int i;
    for(i=0;i<16;i++)
        matrix[i]=_matrix[i];
}
bool t3DWindow::getMousePointer(int x, int y, ePointer& p, int& tag)
{
    float fx,fy;
    windowXYToModelXY(x,y,fx,fy,matrix);
    return tWindow::getMousePointer((int)fx,(int)fy,p,tag);
}
/****************************************************************************/
// 3D BUTTON
/****************************************************************************/
void t3DButton::drawControl()
{
    glPushMatrix();

    if(!isDepressed())
    {
        glEnable(GL_BLEND);
        glColor4f(0,0,0,0.5);
        glRecti(getControlX(),getControlY(),getControlX()+getWidth(),getControlY()+getHeight());
        glDisable(GL_BLEND);
        glTranslatef(0,0,9.9);
    }

    glColor3f(1,0,1);
    glDepthMask(true);
    glRecti(getControlX(),getControlY(),getControlX()+getWidth(),getControlY()+getHeight());
    glDepthMask(false);

    glTranslatef(0,0,0.1);
    if(isDisabled())
        glColor3f(.5,.5,.5); //Gray
    else if(isSelected() || isFocused())
        glColor3f(1,1,1); //White
    else
        glColor3f(0,1,1); //Cyan
    //We calculate captionX and CaptionY in refresh every time the caption changes.
    drawString(CHARPTR(caption), getControlX()+captionX, getControlY()+captionY, fontIndex);

    glPopMatrix();
}

/****************************************************************************/
// MAIN MENU
/****************************************************************************/
tMainMenu::tMainMenu() : t3DWindow(400,400,false,true,true),
    startTime(currentTime)
{
    tControl *c;
    cx=200;
    cy=10;
    c=mwNew tStdLabel(cx-160,cy+310,320,50,"Enter your name",mediumFont,0,1,1);
    createControl(c);
    edUserName = mwNew tEdit(cx-120,cy+290,240,globalName,mediumFont,false);
    createControl(edUserName);
    c=mwNew t3DButton(cx-140,cy+220,280,40,"Host Game",mediumFont,false,(tEvent) &tMainMenu::onHostGame);
    createControl(c);
    c=mwNew t3DButton(cx-140,cy+170,280,40,"Connect",mediumFont,false,(tEvent) &tMainMenu::onConnect);
    createControl(c);
    c=mwNew t3DButton(cx-140,cy+120,280,40,"Single Player",mediumFont,false,(tEvent) &tMainMenu::onSinglePlayer);
    createControl(c);
    c=mwNew t3DButton(cx-140,cy+70,280,40,"Options",mediumFont,false,(tEvent) &tMainMenu::onOptions);
    createControl(c);
    c=mwNew t3DButton(cx-140,cy+20,280,40,"Exit",mediumFont,false,(tEvent) &tMainMenu::onExit);
    createControl(c);
    //c=mwNew tStdLabel(0,10,640,0,"Copyright � 2001-2003 Jon Sargeant, Jindrich Kolman, John Carter, and Tom Pittlik",smallFont,1,1,1);
    //createControl(c);
    setInitial(edUserName);
    tag=MAIN_MENU;
    //setAnchors(ANCHOR_CENTER,ANCHOR_CENTER);
}
/****************************************************************************/
void tMainMenu::drawWindow()
{
    tWindow::drawWindow();
    int x=getControlX(),y=getControlY();
    int w=getWidth(),h=getHeight();
    float t=currentTime-startTime;

    /*glEnable(GL_BLEND);
    glBegin(GL_QUADS);
        glColor3f(.5,.5,0);
        glVertex2i(x,y);
        glColor3f(0,0,0);
        glVertex2i(x+w,y);
        glColor3f(.5,.5,0);
        glVertex2i(x+w,y+h);
        glColor3f(1,1,0);
        glVertex2i(x,y+h);
    glEnd();

    if(t<4)
    {
        glColor4f(1,0,0,MIN3(t,4-t,1));
        drawString("The New",getControlX()+cx-120,getControlY()+cy+410,mediumFont);
    }

    if(t>1.5 && t<4)
    {
        glColor4f(0,0,1,MIN3(t-1.5,4-t,1));
        drawString("and Improved",getControlX()+cx-30,getControlY()+cy+410,mediumFont);
    }

    if(t>3)
    {
        glColor4f(1,1,1,MIN(t-3,1));
        drawString("MACHINATIONS",getControlX()+cx-165,getControlY()+cy+400,largeFont);
    }

    if(t>4)
    {
        glColor4f(1,1,1,MIN(t-4,1));
        snprintf(buf,sizeof(buf),"Version %s", VERSION_STRING);
        drawString("Version " MAKE_ASCII_STRING(VERSION_STRING),getControlX()+cx-30,getControlY()+cy+380,smallFont);
    }

    glDisable(GL_BLEND);*/
}
/****************************************************************************/
bool tMainMenu::getUserName()
{
    if(STRLEN(edUserName->getString())==0)
    {
        focusControl(edUserName);
        messageBox("Invalid user name", "Error", CHOICES_OK);
        return false;
    }
    if(!isUsernameValid(CHARPTR(edUserName->getString())))
    {
        focusControl(edUserName);
        messageBox("User name cannot contain spaces.", "Error", CHOICES_OK);
        return false;
    }
    globalName=edUserName->getString();
    return true;
}
/****************************************************************************/
void tMainMenu::onExit(tControl *sender)
{
    endApplication();
}
/****************************************************************************/
void tMainMenu::onHostGame(tControl *sender)
{
    const char **IPs;
    if(!getUserName())
        return;
    if(chosenIP==0)
    {
        IPs=getLocalIPs();
        if(IPs==NULL || IPs[0]==NULL)
        {
            messageBox("Failed to obtain local IP address", "Error", CHOICES_OK);
            return;
        }
        chosenIP=IPToLong((const unsigned char *)&IPs[0][0]);
    }
    if(!hostGame(chosenIP,chosenPort,CHARPTR(globalName),hasChosenTCPIP)) //An unexpected error occurred
    {
        messageBox("Unexpected error occurred.\nView '" OUTPUT_FILE "' for details.", "Error", CHOICES_OK);
    }
    //else await onHostGame event
}
/****************************************************************************/
void tMainMenu::onConnect(tControl *sender)
{
    if(!getUserName())
        return;
    closeWindows();
    displayWindow(CONNECT_MENU);
}
/****************************************************************************/
void tMainMenu::onSinglePlayer(tControl *sender)
{
    if(!getUserName())
        return;
    if(!singlePlayer(CHARPTR(globalName))) //An unexpected error occurred
    {
        messageBox("Unexpected error occurred.\nView '" OUTPUT_FILE "' for details.", "Error", CHOICES_OK);
    }
    //else await on single player event
}
/****************************************************************************/
void tMainMenu::onOptions(tControl *sender)
{
    if(!getUserName())
        return;
    closeWindows();
    displayWindow(OPTIONS_MENU);
}
/****************************************************************************/
// OPTIONS MENU
/****************************************************************************/
void tOptionsMenu::onOKClick(tControl *sender)
{
    int a, b, c, d, port;
    unsigned char values[4];
    port = atoi(CHARPTR(edPort->getString()));
    if(cbIPAddress->getChoice()==-1)
    {
        focusControl(cbIPAddress);
        messageBox("You must choose an IP address.", "Error", CHOICES_OK);
        return;
    }
    if (sscanf(CHARPTR(cbIPAddress->getChoiceString()), "%d.%d.%d.%d", &a, &b, &c, &d) < 4)
    {
        focusControl(cbIPAddress);
        messageBox("Invalid IP address", "Error", CHOICES_OK);
        return;
    }
    if(port < 0)
    {
        focusControl(edPort);
        messageBox("Invalid port", "Error", CHOICES_OK);
        return;
    }
    values[0] = a;
    values[1] = b;
    values[2] = c;
    values[3] = d;
    chosenIP = IPToLong(values);
    chosenPort = port;
    hasChosenTCPIP = rbTCPIP->isChecked();

    closeWindows();
    displayWindow(MAIN_MENU);
}
/****************************************************************************/
void tOptionsMenu::onCancelClick(tControl *sender)
{
    closeWindows();
    displayWindow(MAIN_MENU);
}
/****************************************************************************/
tOptionsMenu::tOptionsMenu() : tWindow(0,0,640,480,false,true,true)
{
    int i;
    long IP;
    int initialChoice=0;
    int cx=320;
    int cy=0;
    tControl *c;
    char buf[1000];
    const char **IPs=getLocalIPs();
    tBufferIterator ptr(buf, sizeof(buf));

    c=mwNew tStdLabel(cx-160,cy+400,320,50,"OPTIONS",largeFont,1,1,1);
    createControl(c);
    c=mwNew tStdLabel(cx-160,cy+330,320,50,"Listen From...",mediumFont,1,1,1);
    createControl(c);
    c=mwNew tStdLabel(cx-160,cy+280,320,50,"IP Address",mediumFont,0,1,1);
    createControl(c);

    try
    {
        if(IPs)
            for(i=0;IPs[i];i++)
                mySprintf(ptr,"%d.%d.%d.%d\n",
                    (unsigned char) IPs[i][0],
                    (unsigned char) IPs[i][1],
                    (unsigned char) IPs[i][2],
                    (unsigned char) IPs[i][3]);
        else
            bug("tOptionsMenu::tOptionsMenu: failed to retrieve a list of local IP addresses");
    }
    catch(int x)
    {
        bug("tOptionsMenu::tOptionsMenu: buffer overflow");
    }

    if(IPs)
        for(i=0;IPs[i];i++)
        {
            IP=IPToLong((const unsigned char *)&IPs[i][0]);
            if(IP==chosenIP)
            {
                initialChoice=i;
                break;
            }
        }

    cbIPAddress = mwNew tStdComboBox(cx-120,cy+250,240,true,buf,initialChoice,100,false,false,mediumFont);
    createControl(cbIPAddress);

    c=mwNew tStdLabel(cx-160,cy+200,320,50,"Port",mediumFont,0,1,1);
    createControl(c);

    if(chosenPort>0)
        snprintf(buf,sizeof(buf),"%d",chosenPort);
    else
        strcpy(buf,"Any");

    edPort = mwNew tEdit(cx-120,cy+170,240,buf,mediumFont,false);
    createControl(edPort);
    c=mwNew tStdLabel(cx-160,cy+100,320,50,"Protocol",mediumFont,0,1,1);
    createControl(c);
    rbTCPIP = mwNew tStdRadio(cx-140,cy+70,"TCP/IP",mediumFont,1,1,1,true,hasChosenTCPIP,1);
    createControl(rbTCPIP);
    rbUDP = mwNew tStdRadio(cx+60,cy+70,"UDP",mediumFont,1,1,1,true,!hasChosenTCPIP,1);
    createControl(rbUDP);

    btOK = mwNew tStdButton(cx-160,cy,160,50,"OK",mediumFont,false, (tEvent) &tOptionsMenu::onOKClick);
    createControl(btOK);
    btCancel = mwNew tStdButton(cx,cy,160,50,"Cancel",mediumFont,false, (tEvent) &tOptionsMenu::onCancelClick);
    createControl(btCancel);

    setInitial(cbIPAddress);
    setDefault(btOK);
    tag=OPTIONS_MENU;
    setAnchors(ANCHOR_CENTER,ANCHOR_CENTER);
}
/****************************************************************************/
void tOptionsMenu::drawWindow()
{
    tWindow::drawWindow();
    int x=getControlX(),y=getControlY();
    int w=getWidth(),h=getHeight();

    glEnable(GL_BLEND);
    glBegin(GL_QUADS);
        glColor3f(0,.5,0);
        glVertex2i(x,y);
        glColor3f(0,0,0);
        glVertex2i(x+w,y);
        glColor3f(0,.5,0);
        glVertex2i(x+w,y+h);
        glColor3f(0,1,0);
        glVertex2i(x,y+h);
    glEnd();
    glDisable(GL_BLEND);
}
/****************************************************************************/
// CONNECT MENU
/****************************************************************************/
void tConnectMenu::onOKClick(tControl *sender)
{
    int port = atoi(CHARPTR(edPort->getString()));
    int a, b, c, d;
    unsigned char values[4];
    long IP;
    char buf[80];
    const char **IPs;

    if (sscanf(CHARPTR(edIPAddress->getString()), "%d.%d.%d.%d", &a, &b, &c, &d) < 4)
    {
        focusControl(edIPAddress);
        messageBox("Invalid IP address", "Error", CHOICES_OK);
        return;
    }
    if(port <= 0)
    {
        focusControl(edPort);
        messageBox("Invalid port", "Error", CHOICES_OK);
        return;
    }
    values[0] = a;
    values[1] = b;
    values[2] = c;
    values[3] = d;
    IP = IPToLong(values);

    if(chosenIP==0)
    {
        IPs=getLocalIPs();
        if(IPs==NULL || IPs[0]==NULL)
        {
            messageBox("Failed to obtain local IP address", "Error", CHOICES_OK);
            return;
        }
        chosenIP=IPToLong((const unsigned char *)&IPs[0][0]);
    }

    if(!connectToHost(IP, port, CHARPTR(globalName), hasChosenTCPIP, chosenIP, chosenPort)) //Unexpected error occurred
    {
        messageBox("Unexpected error occurred.\nView '" OUTPUT_FILE "' for details.", "Error", CHOICES_OK);
    }
    else //Await on connect event
    {
        tWindow *msgBox;
        snprintf(buf,sizeof(buf),"Connecting to %d.%d.%d.%d on port %d...",values[0],values[1],values[2],values[3],port);
        msgBox=messageBox(buf, "Connecting", CHOICES_CANCEL, cancelConnect);
        msgBox->tag=CONNECT_MESSAGE;
    }
}
/****************************************************************************/
void tConnectMenu::onCancelClick(tControl *sender)
{
    closeWindows();
    displayWindow(MAIN_MENU);
    disconnect();
}
/****************************************************************************/
tConnectMenu::tConnectMenu() : tWindow(0,0,640,480,false,true,false)
{
    int cx=320;
    int cy=0;
    tControl *c;
    c=mwNew tStdLabel(cx-160,cy+400,320,50,"CONNECT",largeFont,1,1,1);
    createControl(c);
    c=mwNew tStdLabel(cx-160,cy+300,320,50,"Connect To...",mediumFont,1,1,1);
    createControl(c);
    c=mwNew tStdLabel(cx-160,cy+230,320,50,"IP Address",mediumFont,0,1,1);
    createControl(c);
    edIPAddress = mwNew tEdit(cx-120,cy+200,240,"",mediumFont,false);
    createControl(edIPAddress);
    c=mwNew tStdLabel(cx-160,cy+150,320,50,"Port",mediumFont,0,1,1);
    createControl(c);
    edPort = mwNew tEdit(cx-120,cy+120,240,"4000",mediumFont,false);
    createControl(edPort);

    btOK = mwNew tStdButton(cx-160,cy,160,50,"OK",mediumFont,false, (tEvent) &tConnectMenu::onOKClick);
    createControl(btOK);
    btCancel = mwNew tStdButton(cx,cy,160,50,"Cancel",mediumFont,false, (tEvent) &tConnectMenu::onCancelClick);
    createControl(btCancel);

    setInitial(edIPAddress);
    setDefault(btOK);
    tag=CONNECT_MENU;
    setAnchors(ANCHOR_CENTER,ANCHOR_CENTER);
}
/****************************************************************************/
void tConnectMenu::drawWindow()
{
    tWindow::drawWindow();
    int x=getControlX(),y=getControlY();
    int w=getWidth(),h=getHeight();

    glEnable(GL_BLEND);
    glBegin(GL_QUADS);
        glColor3f(.5,0,0);
        glVertex2i(x,y);
        glColor3f(0,0,0);
        glVertex2i(x+w,y);
        glColor3f(.5,0,0);
        glVertex2i(x+w,y+h);
        glColor3f(1,0,0);
        glVertex2i(x,y+h);
    glEnd();
    glDisable(GL_BLEND);
}
/****************************************************************************/
// SETUP MENU
/****************************************************************************/
tSetupMenu::tSetupMenu() : tWindow(0,0,640,480,false,true,false)
{
    char s[100];
    unsigned char values[4];
    int cx=320;
    int cy=0;
    tControl *c;

    c=mwNew tStdLabel(cx-160,cy+440,320,40,"SETUP GAME",largeFont,1,1,1);
    createControl(c);
    laGameMessage = mwNew tStdLabel(cx-160,cy+420,320,20,"",mediumFont,1,0,0);
    createControl(laGameMessage);

    hideControl(laGameMessage);
    int x1=cx-310;
    int y1=cy+190;

    c=mwNew tStaticRect(x1,y1,400,230);
    createControl(c);
    c=mwNew tStdLabel(x1+5,y1+200,"Players",mediumFont,1,1,1);
    createControl(c);

    for(int i=0;i<MAX_PLAYERS;i++)
    {
        laPlayers[i] = mwNew tStdLabel(x1+50, y1+170-i*20, "", smallFont, 1, 1, 1);
        createControl(laPlayers[i]);
        cbPlayers[i] = mwNew tMyComboBox(i, x1+150, y1+166-i*20, 100, false, "Open\nClosed\nComputer",
            -1, 100, false, false, smallFont);
        createControl(cbPlayers[i]);

        if(isGameClient)
            disableControl(cbPlayers[i]);
        hideControl(laPlayers[i]);
        hideControl(cbPlayers[i]);
    }

    int x2=cx+100;
    int y2=cy+190;
    c=mwNew tStaticRect(x2,y2,210,230);
    createControl(c);
    c=mwNew tStdLabel(x2+5,y2+200,"Settings",mediumFont,1,1,1);
    createControl(c);
    if(networkGame)
    {
        longToIP(networkIP, values);
        snprintf(s,sizeof(s),"IP Address: %d.%d.%d.%d", values[0], values[1], values[2], values[3]);
    }
    else
        strcpy(s,"IP Address: n/a");

    laIPAddress = mwNew tStdLabel(x2+10,y2+172,s,smallFont,1,1,1);
    createControl(laIPAddress);

    if(networkGame)
        snprintf(s,sizeof(s),"Port: %d", networkPort);
    else
        strcpy(s, "Port: n/a");

    laPort = mwNew tStdLabel(x2+10,y2+156,s,smallFont,1,1,1);
    createControl(laPort);
    laMapName = mwNew tStdLabel(x2+10,y2+140,"Map:",smallFont,1,1,1);
    createControl(laMapName);
    laWorldName = mwNew tStdLabel(x2+10,y2+124,"World:",smallFont,1,1,1);
    createControl(laWorldName);

    if(networkGame)
        snprintf(s,sizeof(s),"Latency: %ldms", latency);
    else
        strcpy(s, "Latency: n/a");

    laLatency = mwNew tStdLabel(x2+10,y2+108,s,smallFont,1,1,1);
    createControl(laLatency);

    if(networkGame)
        snprintf(s,sizeof(s),"Network Interval: %ldms", netInterval);
    else
        strcpy(s, "Network Interval: n/a");

    laNetInterval = mwNew tStdLabel(x2+10,y2+92,s,smallFont,1,1,1);
    createControl(laNetInterval);

    snprintf(s,sizeof(s),"Game Interval: %ldms", gameInterval);
    laGameInterval = mwNew tStdLabel(x2+10,y2+76,s,smallFont,1,1,1);
    createControl(laGameInterval);

    snprintf(s,sizeof(s),"Game Speed: %3.1fx", gameSpeed);
    laGameSpeed = mwNew tStdLabel(x2+10,y2+60,s,smallFont,1,1,1);
    createControl(laGameSpeed);

    btChooseMap = mwNew tStdButton(x2,y2+25,210,25,"Choose Map",mediumFont,false,(tEvent) &tSetupMenu::onChooseMapClick);
    createControl(btChooseMap);
    btSetTiming = mwNew tStdButton(x2,y2,210,25,"Set Timing",mediumFont,false,(tEvent) &tSetupMenu::onSetTimingClick);
    createControl(btSetTiming);

    int x3=cx-310;
    int y3=cy+40;

    meOutput = mwNew tStdTextBox(x3,y3+20,500,120,"",/*maxLines=*/-1,/*createVertScrollBar=*/true,
        /*createHorScrollBar=*/false,smallFont,/*wordWrap=*/true,/*readOnly=*/true);
    createControl(meOutput);
    edInput = mwNew tMyEdit(x3,y3,500,"",smallFont,false,(tEvent) &tSetupMenu::onSendText);
    createControl(edInput);

    int x4=cx+200;
    int y4=cy+40;

    lbUsers = mwNew tStdListBox(x4,y4,110,140,true,"",-1,false,false,smallFont);
    createControl(lbUsers);
    btStartGame = mwNew tStdButton(cx-310,0,240,40,"Start Game",mediumFont,false,(tEvent) &tSetupMenu::onStartGameClick);
    createControl(btStartGame);
    btDisconnect = mwNew tStdButton(cx-70,0,240,40,"Disconnect",mediumFont,false,(tEvent) &tSetupMenu::onDisconnectClick);
    createControl(btDisconnect);
    btDrop = mwNew tStdButton(x4,0,110,40,"Drop User",mediumFont,false,(tEvent) &tSetupMenu::onDropClick);
    createControl(btDrop);

    if(isGameClient)
    {
        disableControl(btChooseMap);
        disableControl(btSetTiming);
        disableControl(btStartGame);
        disableControl(btDrop);
    }

    setInitial(edInput);
    tag=SETUP_GAME_MENU;
    setAnchors(ANCHOR_CENTER,ANCHOR_CENTER);
}
/****************************************************************************/
void tSetupMenu::onChooseMapClick(tControl *sender)
{
    displayWindow(MAP_MENU);
}
/****************************************************************************/
void tSetupMenu::onSetTimingClick(tControl *sender)
{
    displayWindow(TIMING_MENU);
}
/****************************************************************************/
void tSetupMenu::onStartGameClick(tControl *sender)
{
    //Obviously, you need to load a world before you can load a map.
    if(!mapLoaded)
    {
        messageBox("You need to choose a map first.", "Error", CHOICES_OK);
        return;
    }

    //The function will return false if the game failed to start but we don't
    //care.  If the function was successful, we await an onGameWaiting and an onStartGame event.
    //The function may generate the following events in response to errors:
    //onParseError, onMapChanged, onWorldChanged, onHostMissing, onAbortGame
    startGame();
}
/****************************************************************************/
void tSetupMenu::onDisconnectClick(tControl *sender)
{
    //We don't care if this function succeeds since we're returning to the main menu anyway
    destroy();
    closeWindows();
    displayWindow(MAIN_MENU);
    disconnect();
}
/****************************************************************************/
void tSetupMenu::onDropClick(tControl *sender)
{
    tUser *u;
    if(lbUsers->getChoice() < 0)
    {
        messageBox("You must select a user.", "Error", CHOICES_OK);
        return;
    }
    tString s=lbUsers->getChoiceString();
    const char *ptr=strchr(CHARPTR(s),' '); //find first space
    if(ptr)
        s=STRLEFT(s,ptr-CHARPTR(s));
    u=userLookup(s,/*exact=*/true);
    if(u && u->descriptor)
        dropUser(u);
    else
        messageBox("Invalid user", "Error", CHOICES_OK);
}
/****************************************************************************/
void tSetupMenu::onSendText(tControl *sender)
{
    char buffer[1000];
    tString string, username, filename;
    tFile f;

    string=CHARPTR(edInput->getString()); //Make a copy of the edit's string.
    if(string[0]=='/')
    {
        string[0]=' '; //Replace the front-slash with a space so that it doesn't interfere with parsing.
        try
        {
            parseEntry(string,/*inGame=*/false);
        }
        catch(const tString& message)
        {
            messageBox(message, "Shell Error", CHOICES_OK);
        }
    }
    else
    {
        snprintf(buffer,sizeof(buffer),"%s: %s", CHARPTR(globalName), CHARPTR(string));
        sendText(buffer, TEXT_MESSAGE_CHAT);
        showMessage(buffer, TEXT_MESSAGE_ECHO);
    }
    edInput->clear();
}
/****************************************************************************/
void tSetupMenu::onShowMessage(const tString& str, eTextMessage type)
{
    meOutput->addString(formatMessage(str, type));
    meOutput->setWindowPosition(0, MAX(0,meOutput->getWindowHeight()-meOutput->visWindowHeight()));
}
/****************************************************************************/
void tSetupMenu::onAddUser(tUser *user, bool initial)
{
    lbUsers->addChoice(user->name);
    for(int i=0; i<MAX_PLAYERS; i++)
        cbPlayers[i]->addChoice(user->name);
}
/****************************************************************************/
void tSetupMenu::onDropUser(tUser *user)
{
    int i,index;
    i=stringListLookup(lbUsers->getChoices(), user->name);
    if(i>=0)
        lbUsers->deleteChoice(i);
    for(index=0; index<MAX_PLAYERS; index++)
    {
        i=stringListLookup(cbPlayers[index]->getChoices(), user->name);
        if(i>=0)
            cbPlayers[index]->deleteChoice(i);
    }
}
/****************************************************************************/
void tSetupMenu::onUpdateLag(tUser *user)
{
    int i=stringListLookup(lbUsers->getChoices(), user->name);
    if(i>=0)
    {
        char buf[80];
        snprintf(buf,sizeof(buf),"%s (%4.2f)", CHARPTR(user->name), user->descriptor->lag);
        lbUsers->replaceChoice(buf,i);
    }
}
/****************************************************************************/
void tSetupMenu::onSetTiming()
{
    char buffer[80];
    if(networkGame)
    {
        snprintf(buffer,sizeof(buffer),"Latency: %ldms", latency);
        laLatency->setCaption(buffer);
        snprintf(buffer,sizeof(buffer),"Network Interval: %ldms", netInterval);
        laNetInterval->setCaption(buffer);
    }
    snprintf(buffer,sizeof(buffer),"Game Interval: %ldms", gameInterval);
    laGameInterval->setCaption(buffer);
    snprintf(buffer,sizeof(buffer),"Game Speed: %3.1fx", gameSpeed);
    laGameSpeed->setCaption(buffer);
}
/****************************************************************************/
void tSetupMenu::onChooseFiles()
{
    laMapName->setCaption("Map: "+mapFile.name);
    laWorldName->setCaption("World: "+worldFile.name);
}
/****************************************************************************/
void tSetupMenu::onLoadMap()
{
    for(int i=0;i<MAX_PLAYERS;i++)
    {
        if(isPlayerVisible(i))
        {
            laPlayers[i]->setCaption(getPlayerCaption(i,/*useUsername=*/false));
            showControl(laPlayers[i]);
        }
        else
            hideControl(laPlayers[i]);
    }
}
/****************************************************************************/
void tSetupMenu::onResetPlayers()
{
    for(int i=0; i<MAX_PLAYERS; i++)
        hideControl(cbPlayers[i]);
}
/****************************************************************************/
void tSetupMenu::onUnloadMap()
{
    laMapName->setCaption("Map:");
    for(int i=0; i<MAX_PLAYERS; i++)
        hideControl(laPlayers[i]);
}
/****************************************************************************/
void tSetupMenu::onUnloadWorld()
{
    laWorldName->setCaption("World:");
}
/****************************************************************************/
void tSetupMenu::onSetPlayer(int index, ePlayer type, tUser *user)
{
    switch(type)
    {
        case PLAYER_NONE:
        case PLAYER_HIDDEN:
            hideControl(cbPlayers[index]);
            break;
        case PLAYER_OPEN:
            cbPlayers[index]->setChoice(0);
            showControl(cbPlayers[index]);
            break;
        case PLAYER_CLOSED:
            cbPlayers[index]->setChoice(1);
            showControl(cbPlayers[index]);
            break;
        case PLAYER_COMPUTER:
            cbPlayers[index]->setChoice(2);
            showControl(cbPlayers[index]);
            break;
        case PLAYER_HUMAN:
            if(user)
            {
                int i=stringListLookup(cbPlayers[index]->getChoices(), user->name);
                if(i>=0)
                    cbPlayers[index]->setChoice(i);
            }
            showControl(cbPlayers[index]);
            break;
    }
}
/****************************************************************************/
void tSetupMenu::onReconnect()
{
    char s[80];
    unsigned char values[4];
    longToIP(networkIP, values);
    snprintf(s,sizeof(s),"IP Address: %d.%d.%d.%d", values[0], values[1], values[2], values[3]);
    laIPAddress->setCaption(s);
    snprintf(s,sizeof(s),"Port: %d", networkPort);
    laPort->setCaption(s);
}
/****************************************************************************/
void tSetupMenu::onStartGame()
{
    laGameMessage->setCaption("Game in progress");
    showControl(laGameMessage);
}
/****************************************************************************/
void tSetupMenu::onEndGame()
{
    hideControl(laGameMessage);
    enableControl(btStartGame);
}
/****************************************************************************/
void tSetupMenu::onGameWaiting()
{
    //Display this message in case the game startup is delayed.
    laGameMessage->setCaption("Game will start momentarily");
    showControl(laGameMessage);
    disableControl(btStartGame);
}
/****************************************************************************/
void tSetupMenu::onAbortGame()
{
    hideControl(laGameMessage);
    enableControl(btStartGame);
}
/****************************************************************************/
void tSetupMenu::drawWindow()
{
    tWindow::drawWindow();
    int x=getControlX(),y=getControlY();
    int w=getWidth(),h=getHeight();

    glEnable(GL_BLEND);
    glBegin(GL_QUADS);
        glColor3f(0,0,.5);
        glVertex2i(x,y);
        glColor3f(0,0,0);
        glVertex2i(x+w,y);
        glColor3f(0,0,.5);
        glVertex2i(x+w,y+h);
        glColor3f(0,0,1);
        glVertex2i(x,y+h);
    glEnd();
    glDisable(GL_BLEND);
}
/****************************************************************************/
// MAP MENU
/****************************************************************************/
void tMapMenu::onOKClick(tControl *sender)
{
    //chooseMap will generate an onParseError event if any errors occur.
    chooseMap(MAP_PATH+edMapFilename->getString(), WORLD_PATH+edWorldFilename->getString(), /*resetPlayers=*/true);
    close();
}
/****************************************************************************/
void tMapMenu::onCancelClick(tControl *sender)
{
    close();
}
/****************************************************************************/
tMapMenu::tMapMenu() : tStdDialogBox("Choose Map",mediumFont,320,220,WINDOW_NORMAL,
    true,true,true,false,false,false,true,-1,-1,-1,-1)
{
    int w=getWidth();
    tControl *c;
    c=mwNew tStdLabel(0,150,w,30,"Map filename",mediumFont,1,1,1);
    createControl(c);
    edMapFilename = mwNew tEdit(10,120,w-20,mapFile.name,mediumFont,false);
    createControl(edMapFilename);

    c=mwNew tStdLabel(0,80,w,30,"World filename",mediumFont,1,1,1);
    createControl(c);
    edWorldFilename = mwNew tEdit(10,50,w-20,worldFile.name,mediumFont,false);
    createControl(edWorldFilename);

    btOK = mwNew tStdButton(0,0,w/2,30,"OK",mediumFont,false, (tEvent) &tMapMenu::onOKClick);
    createControl(btOK);
    btCancel = mwNew tStdButton(w/2,0,w/2,30,"Cancel",mediumFont,false, (tEvent) &tMapMenu::onCancelClick);
    createControl(btCancel);

    setInitial(edMapFilename);
    setDefault(btOK);

    tag=MAP_MENU;
}
/****************************************************************************/
// RECONNECT MENU
/****************************************************************************/
void tReconnectMenu::onOKClick(tControl *sender)
{
    int port = atoi(CHARPTR(edPort->getString()));
    int a, b, c, d;
    unsigned char values[4];
    if (sscanf(CHARPTR(cbIPAddress->getChoiceString()), "%d.%d.%d.%d", &a, &b, &c, &d) < 4)
    {
        focusControl(cbIPAddress);
        messageBox("Invalid IP address", "Error", CHOICES_OK);
        return;
    }
    if(port < 0)
    {
        focusControl(edPort);
        messageBox("Invalid port", "Error", CHOICES_OK);
        return;
    }
    values[0] = a;
    values[1] = b;
    values[2] = c;
    values[3] = d;
    chosenIP = IPToLong(values);
    chosenPort = port;

    reconnect(chosenIP,chosenPort);
    close();
}
/****************************************************************************/
void tReconnectMenu::onCancelClick(tControl *sender)
{
    close();
}
/****************************************************************************/
tReconnectMenu::tReconnectMenu() : tStdDialogBox("Reconnect",mediumFont,370,210,WINDOW_NORMAL,
    true,true,true,false,false,false,true,-1,-1,-1,-1)
{
    int i;
    long IP;
    int initialChoice=0;
    tControl *c;
    char buf[1000];
    const char **IPs=getLocalIPs();
    tBufferIterator ptr(buf, sizeof(buf));

    int w=getWidth();
    int x1=20,x2=150,w2=200;
    int y1=50,y2=90,y3=130;
    int dy=5;

    c=mwNew tStdLabel(x1,y3+dy,"Listen From...",mediumFont,1,1,1);
    createControl(c);
    c=mwNew tStdLabel(x1,y2+dy,"IP Address",mediumFont,1,1,1);
    createControl(c);

    try
    {
        if(IPs)
            for(i=0;IPs[i];i++)
                mySprintf(ptr,"%d.%d.%d.%d\n",
                    (unsigned char) IPs[i][0],
                    (unsigned char) IPs[i][1],
                    (unsigned char) IPs[i][2],
                    (unsigned char) IPs[i][3]);
        else
            bug("tReconnectMenu::tReconnectMenu: failed to retrieve a list of local IP addresses");
    }
    catch(int x)
    {
        bug("tReconnectMenu::tReconnectMenu: buffer overflow");
    }

    if(IPs)
        for(i=0;IPs[i];i++)
        {
            IP=IPToLong((const unsigned char *)&IPs[i][0]);
            if(IP==chosenIP)
            {
                initialChoice=i;
                break;
            }
        }

    cbIPAddress = mwNew tStdComboBox(x2,y2,w2,true,buf,initialChoice,50,false,false,mediumFont);
    createControl(cbIPAddress);

    c=mwNew tStdLabel(x1,y1+dy,"Port",mediumFont,1,1,1);
    createControl(c);

    if(chosenPort>0)
        snprintf(buf,sizeof(buf),"%d",chosenPort);
    else
        strcpy(buf,"Any");

    edPort = mwNew tEdit(x2,y1,w2,buf,mediumFont,false);
    createControl(edPort);

    btOK = mwNew tStdButton(0,0,w/2,30,"OK",mediumFont,false,(tEvent) &tReconnectMenu::onOKClick);
    createControl(btOK);
    btCancel = mwNew tStdButton(w/2,0,w/2,30,"Cancel",mediumFont,false, (tEvent) &tReconnectMenu::onCancelClick);
    createControl(btCancel);

    setInitial(cbIPAddress);
    setDefault(btOK);

    tag=RECONNECT_MENU;
}
/****************************************************************************/
// TIMING MENU
/****************************************************************************/
void tTimingMenu::onOKClick(tControl *sender)
{
    long newLatency;
    long newNetInterval;
    if(networkGame)
    {
        newLatency = atoi(CHARPTR(edLatency->getString()));
        newNetInterval = atoi(CHARPTR(edNetInterval->getString()));
        if(newLatency <= 0 || newLatency > 10000)
        {
            messageBox("Latency must be between 0 and 10000.", "Error", CHOICES_OK);
            return;
        }
        if(newNetInterval < 1 || newNetInterval > 10000)
        {
            messageBox("Network interval must be between 1 and 10000.", "Error", CHOICES_OK);
            return;
        }
    }
    else
    {
        newLatency=latency;
        newNetInterval=netInterval;
    }
    long newGameInterval = atoi(CHARPTR(edGameInterval->getString()));
    if(newGameInterval < 1 || newGameInterval > 10000)
    {
        messageBox("Game interval must be between 1 and 10000.", "Error", CHOICES_OK);
        return;
    }
    float newGameSpeed = atof(CHARPTR(edGameSpeed->getString()));
    if(newGameSpeed < 0.1 || newGameSpeed > 10.0)
    {
        messageBox("Game speed must be between 0.1 and 10.0.", "Error", CHOICES_OK);
        return;
    }
    if(!setTiming(newLatency, newNetInterval, newGameInterval, newGameSpeed))
        messageBox("Unexpected error occurred.\nView '" OUTPUT_FILE "' for details.", "Error", CHOICES_OK);
    close();
}
/****************************************************************************/
void tTimingMenu::onCancelClick(tControl *sender)
{
    close();
}
/****************************************************************************/
tTimingMenu::tTimingMenu() : tStdDialogBox("Set Timing",mediumFont,340,networkGame?250:170,WINDOW_NORMAL,
    true,true,true,false,false,false,true,-1,-1,-1,-1)
{
    int w=getWidth();
    int x1=20, x2=190, w2=130;
    int y1=50, y2=90, y3=130, y4=170;
    int dy=5;

    char s[20];
    tControl *c;
    if(networkGame)
    {
        snprintf(s,sizeof(s),"%ld",latency);

        c=mwNew tStdLabel(x1,y4+dy,"Latency",mediumFont,1,1,1);
        createControl(c);
        edLatency = mwNew tEdit(x2,y4,w2,s,mediumFont,false);
        createControl(edLatency);

        snprintf(s,sizeof(s),"%ld",netInterval);
        c=mwNew tStdLabel(x1,y3+dy,"Network Interval",mediumFont,1,1,1);
        createControl(c);
        edNetInterval = mwNew tEdit(x2,y3,w2,s,mediumFont,false);
        createControl(edNetInterval);
    }
    else
    {
        edLatency=NULL;
        edNetInterval=NULL;
    }

    snprintf(s,sizeof(s),"%ld",gameInterval);
    c=mwNew tStdLabel(x1,y2+dy,"Game Interval",mediumFont,1,1,1);
    createControl(c);
    edGameInterval = mwNew tEdit(x2,y2,w2,s,mediumFont,false);
    createControl(edGameInterval);

    snprintf(s,sizeof(s),"%3.1f",gameSpeed);
    c=mwNew tStdLabel(x1,y1+dy,"Game Speed",mediumFont,1,1,1);
    createControl(c);
    edGameSpeed = mwNew tEdit(x2,y1,w2,s,mediumFont,false);
    createControl(edGameSpeed);

    btOK = mwNew tStdButton(0,0,w/2,30,"OK",mediumFont,false,(tEvent) &tTimingMenu::onOKClick);
    createControl(btOK);
    btCancel = mwNew tStdButton(w/2,0,w/2,30,"Cancel",mediumFont,false, (tEvent) &tTimingMenu::onCancelClick);
    createControl(btCancel);

    if(networkGame)
        setInitial(edLatency);
    else
        setInitial(edGameInterval);
    setDefault(btOK);

    tag=TIMING_MENU;
}
/****************************************************************************/
// GAME MENU
/****************************************************************************/
tGameMenu::tGameMenu() : tStdDialogBox("Game Menu",mediumFont,230,480,WINDOW_NORMAL,
    true,true,true,false,false,false,true,-1,-1,-1,-1)
{
    int w=getWidth()-20, y=410, h=35;

    btPause = mwNew tStdButton(10,y,w,30,isGamePaused()?"�R�esume Game":"�P�ause Game",mediumFont,false,
        (tEvent) &tGameMenu::onPauseClick);
    createControl(btPause);
    y-=h;
    btAlliance = mwNew tStdButton(10,y,w,30,"�A�lliances",mediumFont,false,
        (tEvent) &tGameMenu::onAllianceClick);
    createControl(btAlliance);
    y-=h;
    btMission = mwNew tStdButton(10,y,w,30,"Mission Objectives",mediumFont,false,
        (tEvent) &tGameMenu::onMissionClick);
    createControl(btMission);
    disableControl(btMission);
    y-=h;
    btLoad = mwNew tStdButton(10,y,w,30,"Load Game",mediumFont,false,
        (tEvent) &tGameMenu::onLoadClick);
    createControl(btLoad);
    disableControl(btLoad);
    y-=h;
    btSave = mwNew tStdButton(10,y,w,30,"Save Game",mediumFont,false,
        (tEvent) &tGameMenu::onSaveClick);
    createControl(btSave);
    disableControl(btSave);
    y-=h;
    btOptions = mwNew tStdButton(10,y,w,30,"�O�ptions",mediumFont,false,
        (tEvent) &tGameMenu::onOptionsClick);
    createControl(btOptions);
    y-=h;
    btTiming = mwNew tStdButton(10,y,w,30,isGameClient?"Set Timing":"Set �T�iming",mediumFont,false,
        (tEvent) &tGameMenu::onTimingClick);
    createControl(btTiming);
    y-=h;
    btReconnect = mwNew tStdButton(10,y,w,30,"Reconnect",mediumFont,false,
        (tEvent) &tGameMenu::onReconnectClick);
    createControl(btReconnect);
    y-=h;
    btReturn = mwNew tStdButton(10,y,w,30,"Return to Game",mediumFont,false,
        (tEvent) &tGameMenu::onReturnClick);
    createControl(btReturn);
    y-=h;
    btRestart = mwNew tStdButton(10,y,w,30,isGameClient?"Restart":"Restart �ALT�+�BACK�",mediumFont,false,
        (tEvent) &tGameMenu::onRestartClick);
    createControl(btRestart);
    y-=h;
    btEnd = mwNew tStdButton(10,y,w,30,isGameClient?"End Game":"�E�nd Game",mediumFont,false,
        (tEvent) &tGameMenu::onEndClick);
    createControl(btEnd);
    y-=h;
    btDisconnect = mwNew tStdButton(10,y,w,30,networkGame?"�D�isconnect":"Disconnect",mediumFont,false,
        (tEvent) &tGameMenu::onDisconnectClick);
    createControl(btDisconnect);

    if(isGameClient)
    {
        disableControl(btEnd);
        disableControl(btTiming);
        disableControl(btRestart);
    }
    if(!networkGame)
    {
        disableControl(btDisconnect);
        disableControl(btReconnect);
    }
    setDefault(btReturn);

    tag=GAME_MENU;
}
/****************************************************************************/
void tGameMenu::onPauseClick(tControl *sender)
{
    if(isGamePaused())
    {
        resumeGame();
        btPause->setCaption("�P�ause Game");
    }
    else
    {
        pauseGame();
        btPause->setCaption("�R�esume Game");
    }
}
/****************************************************************************/
void tGameMenu::onEndClick(tControl *sender)
{
    closeWindows();
    displayWindow(SETUP_GAME_MENU);
    endGame();
}
/****************************************************************************/
void tGameMenu::onDisconnectClick(tControl *sender)
{
    abortSession(0);
    disconnect();
}
/****************************************************************************/
void tGameMenu::onAllianceClick(tControl *sender)
{
    displayWindow(ALLIANCE_MENU);
}
/****************************************************************************/
void tGameMenu::onOptionsClick(tControl *sender)
{
    displayWindow(GAME_OPTIONS_MENU);
}
/****************************************************************************/
void tGameMenu::onReconnectClick(tControl *sender)
{
    displayWindow(RECONNECT_MENU);
}
/****************************************************************************/
void tGameMenu::onTimingClick(tControl *sender)
{
    displayWindow(TIMING_MENU);
}
/****************************************************************************/
void tGameMenu::onRestartClick(tControl *sender)
{
    restartGame();
}
/****************************************************************************/
// ALLIANCE MENU
/****************************************************************************/
tAllianceMenu::tAllianceMenu() : tStdDialogBox("Alliances",mediumFont,520,120+getPlayerCount()*40,WINDOW_NORMAL,
    true,true,true,false,false,false,true,-1,-1,-1,-1)
{
    int i,j,y=getHeight()-100;
    tControl *c;

    for(i=0; i<MAX_PLAYERS; i++)
        for(j=0; j<3; j++)
            radios[i][j]=NULL;
    for(i=0; i<MAX_PLAYERS; i++)
        for(j=0; j<3; j++)
            toggles[i][j]=NULL;

    c=mwNew tStdLabel(338, y+45, "Shared      Shared     Shared", smallFont, 1, 1, 1);
    createControl(c);
    c=mwNew tStdLabel(190, y+30, FONT_GREEN "Ally     " FONT_YELLOW "Neutral   " FONT_RED "Enemy   "
        FONT_WHITE "Control   Resources   Vision", smallFont, 1, 1, 1);
    createControl(c);
    c=mwNew tStdLabel(191, y+30, FONT_GREEN "Ally     " FONT_YELLOW "Neutral   " FONT_RED "Enemy",
        smallFont, 1, 1, 1);
    createControl(c);

    for(i=0; i<MAX_PLAYERS; i++)
    {
        if(!isPlayerVisible(i))
            continue;
        c=mwNew tStdLabel(10, y, getPlayerCaption(i,/*useUsername=*/true), mediumFont,
            playerColors[i].r/255.0, playerColors[i].g/255.0, playerColors[i].b/255.0);
        createControl(c);
        if(i!=getLocalPlayerID())
        {
            for(j=0; j<3; j++)
            {
                radios[i][j]=mwNew tStdRadio(190+j*53, y, "", mediumFont, 0, 0, 0, false,
                    getAlliance(i)==(eAlliance)(j+1), i+1);
                createControl(radios[i][j]);
            }
            toggles[i][0]=mwNew tStdToggle(347, y, "", mediumFont, 0, 0, 0, false,
                getSharedControl(i));
            createControl(toggles[i][0]);
            toggles[i][1]=mwNew tStdToggle(410, y, "", mediumFont, 0, 0, 0, false,
                getSharedResources(i));
            createControl(toggles[i][1]);
            toggles[i][2]=mwNew tStdToggle(473, y, "", mediumFont, 0, 0, 0, false,
                getSharedVision(i));
            createControl(toggles[i][2]);
        }
        y-=40;
    }

    btOK=mwNew tStdButton(0,0,250,40,"OK",mediumFont,false,(tEvent) &tAllianceMenu::onOKClick);
    createControl(btOK);
    btCancel=mwNew tStdButton(250,0,250,40,"Cancel",mediumFont,false,(tEvent) &tAllianceMenu::onCancelClick);
    createControl(btCancel);
    tag=ALLIANCE_MENU;
}
/****************************************************************************/
void tAllianceMenu::onOKClick(tControl *sender)
{
    int i,j;
    eAlliance a;
    bool sc, sr, sv;
    for(i=0; i<MAX_PLAYERS; i++)
    {
        if(radios[i][0]==NULL)
            continue;
        for(j=0;j<3;j++)
            if(radios[i][j] && radios[i][j]->isChecked())
                a=(eAlliance)(j+1);
        if(toggles[i][0])
            sc=toggles[i][0]->isChecked();
        if(toggles[i][1])
            sr=toggles[i][1]->isChecked();
        if(toggles[i][2])
            sv=toggles[i][2]->isChecked();
        if(a!=getAlliance(i) || sc!=getSharedControl(i) || sr!=getSharedResources(i)
            || sv!=getSharedVision(i))
            setAlliance(i,a,sc,sr,sv);
    }
    close();
}
/****************************************************************************/
// GAME OPTIONS MENU
/****************************************************************************/
tGameOptionsMenu::tGameOptionsMenu() : tStdDialogBox("Game Options",mediumFont,300,435,WINDOW_NORMAL,
    true,true,true,false,false,false,true,-1,-1,-1,-1)
{
    int choice=0;
    int w=getScreenWidth();
    int y=360;
    int i,j;

    tControl *c;
    c=mwNew tStdLabel(10,y,"Resolution",mediumFont,1,1,1);
    createControl(c);
    //Only compare the width since GLFW occasionally subtracts 15 from the height for some reason...
    if(w==800)
        choice=1;
    else if(w==1024)
        choice=2;
    else if(w==1152)
        choice=3;
    else if(w==1280)
        choice=4;

    y-=45;
    lbResolution = mwNew tStdListBox(160,y,130,70,false,
        "640x480\n800x600\n1024x768\n1152x864\n1280x1024",choice,false,false,smallFont);
    createControl(lbResolution);

    y-=40;
    cbFullScreen = mwNew tStdToggle(10,y,"Full Screen Mode",mediumFont,1,1,1,false,isFullScreen());
    createControl(cbFullScreen);

    y-=55;
    c=mwNew tStdLabel(10,y+15,"Pan Rate",mediumFont,1,1,1);
    createControl(c);
    sbPanRate = mwNew tStdScrollBar(160,y+15,130,false,false,(int)(sqrt(panRate)*40),100,0,5);
    createControl(sbPanRate);
    c=mwNew tStdLabel(160,y,"Slower",smallFont,1,1,1);
    createControl(c);
    c=mwNew tStdLabel(250,y,"Faster",smallFont,1,1,1);
    createControl(c);

    y-=40;
    c=mwNew tStdLabel(10,y+15,"Terrain Quality",mediumFont,1,1,1);
    createControl(c);
    i=0;
    for(j=getTerrainInterval();j>1;j>>=1)
        i++;
    //Invert the slider bar:
    i=6-i;
    sbInterval = mwNew tStdScrollBar(160,y+15,130,false,false,i,6,0,1,(tEvent) &tGameOptionsMenu::onIntervalChange);
    createControl(sbInterval);
    c=mwNew tStdLabel(160,y,"Low",smallFont,1,1,1);
    createControl(c);
    c=mwNew tStdLabel(265,y,"High",smallFont,1,1,1);
    createControl(c);

    y-=20;
    cbAutoInterval = mwNew tStdToggle(10,y,"Vary Terrain Quality With Zoom",smallFont,1,1,1,false,
        !isTerrainIntervalFixed());
    createControl(cbAutoInterval);

    y-=40;
    c=mwNew tStdLabel(10,y+5,"Shade Map",smallFont,1,1,1);
    createControl(c);
    i=0;
    for(j=shadeMapSize;j>128;j>>=1)
        i++;
    sbShadeMap = mwNew tStdScrollBar(110,y,180,false,false,i,4,0,1);
    createControl(sbShadeMap);
    c=mwNew tStdLabel(125,y-15,"None 256  512 1024 2048",smallFont,1,1,1);
    createControl(c);

    y-=35;
    c=mwNew tStdLabel(10,y+5,"Alpha Map",smallFont,1,1,1);
    createControl(c);
    i=0;
    for(j=alphaMapSize;j>128;j>>=1)
        i++;
    sbAlphaMap = mwNew tStdScrollBar(110,y,180,false,false,i,4,0,1);
    createControl(sbAlphaMap);
    c=mwNew tStdLabel(125,y-15,"None 256  512 1024 2048",smallFont,1,1,1);
    createControl(c);

    y-=35;
    c=mwNew tStdLabel(10,y+5,"Water Map",smallFont,1,1,1);
    createControl(c);
    i=0;
    for(j=waterMapSize;j>128;j>>=1)
        i++;
    sbWaterMap = mwNew tStdScrollBar(110,y,180,false,false,i,4,0,1);
    createControl(sbWaterMap);
    c=mwNew tStdLabel(125,y-15,"None 256  512 1024 2048",smallFont,1,1,1);
    createControl(c);

    y-=20;
    btOK=mwNew tStdButton(0,0,150,y,"OK",mediumFont,false,(tEvent) &tGameOptionsMenu::onOKClick);
    createControl(btOK);
    btCancel=mwNew tStdButton(150,0,150,y,"Cancel",mediumFont,false, (tEvent) &tGameOptionsMenu::onCancelClick);
    createControl(btCancel);
    tag=GAME_OPTIONS_MENU;
}
/****************************************************************************/
void tGameOptionsMenu::onOKClick(tControl *sender)
{
    int resx,resy;
    int quality;
    switch(lbResolution->getChoice())
    {
        default:    resx=640;   resy=480;   break;
        case 1:     resx=800;   resy=600;   break;
        case 2:     resx=1024;  resy=768;   break;
        case 3:     resx=1152;  resy=864;   break;
        case 4:     resx=1280;  resy=1024;  break;
    }
    setVideoMode(cbFullScreen->isChecked(), resx, resy);
    panRate=(float)sbPanRate->getPosition()/40*(float)sbPanRate->getPosition()/40;
    quality=1<<(6-sbInterval->getPosition());
    if(cbAutoInterval->isChecked())
        resetTerrainInterval();
    else
        setTerrainInterval(quality);
    if(sbShadeMap->getPosition()==0)
        setShadeMapSize(0);
    else
        setShadeMapSize(128<<sbShadeMap->getPosition());
    if(sbAlphaMap->getPosition()==0)
        setAlphaMapSize(0);
    else
        setAlphaMapSize(128<<sbAlphaMap->getPosition());
    if(sbWaterMap->getPosition()==0)
        setWaterMapSize(0);
    else
        setWaterMapSize(128<<sbWaterMap->getPosition());

    close();
}
/****************************************************************************/
void tGameOptionsMenu::onIntervalChange(tControl *sender)
{
    cbAutoInterval->set(false);
}
/****************************************************************************/
// MINI-MAP
/****************************************************************************/
tMinimap::tMinimap() : tStdDialogBox("Minimap",smallFont,getScreenWidth()-206,329,205,120,WINDOW_NORMAL,
    false,false,true,true,false,true,false,100,-1,100,-1), originX(0),
    originY(0), targetted(false)
{
    update();
    tag=MINIMAP_WINDOW;
    setAnchors(ANCHOR_RIGHT,ANCHOR_BOTTOM);
}
/****************************************************************************/
void tMinimap::drawWindow()
{
    tStdDialogBox::drawWindow();
    if(getState()!=WINDOW_MINIMIZED && objectsLoaded)
    {
        glColor3f(0,0,0);
        glPushMatrix();
        glTranslatef(getControlX()+originX,getControlY()+originY,0);
        glBegin(GL_QUADS);
            glVertex2i(0,0);
            glVertex2i(minimapWidth,0);
            glVertex2i(minimapWidth,minimapHeight);
            glVertex2i(0,minimapHeight);
        glEnd();
        glPopMatrix();
        glViewport(getControlX()+originX,getControlY()+originY,minimapWidth,minimapHeight);
        START_GRAPHICS_TIMER(TIMER_DRAW_MINIMAP)
        drawMinimap();
        STOP_GRAPHICS_TIMER(TIMER_DRAW_MINIMAP)
        glViewport(0,0,getScreenWidth(),getScreenHeight());
    }
}
/****************************************************************************/
void tMinimap::update()
{
    if(getState()!=WINDOW_MINIMIZED && objectsLoaded)
    {
        originX=5;
        originY=5;
        //Let the engine know the dimensions of the minimap:
        setMinimapDimensions(getWidth()-10,getHeight()-getTitleBarHeight()-10);
    }
}
/****************************************************************************/
void tMinimap::onMouseDown(int x, int y, eMouseButton button)
{
    tStdDialogBox::onMouseDown(x,y,button);

    int xr=x-getControlX()-originX;
    int yr=y-getControlY()-originY;

    if(getState()!=WINDOW_MINIMIZED && objectsLoaded && xr>=0 && yr>=0 && xr<minimapWidth && yr<minimapHeight)
    {
        if(button==MOUSE_BUTTON_LEFT)
        {
            if(curCommand!=COMMAND_NONE)
            {
                if(firstTarget || isShiftPressed())
                {
                    resetFirstTarget();
                    targetMinimap(xr,yr,isShiftPressed(),curCommand);
                    targetted=true;
                }
                else
                    panMinimap(xr,yr);
            }
            else
                panMinimap(xr,yr);
        }
        else if(button==MOUSE_BUTTON_RIGHT)
        {
            if(curCommand!=COMMAND_NONE)
            {
                if(!firstTarget)
                    targetMinimap(xr,yr,isShiftPressed(),COMMAND_NONE);
                setCurCommand(COMMAND_NONE);
            }
            else
                targetMinimap(xr,yr,isShiftPressed(),COMMAND_NONE);
        }
    }
}
/****************************************************************************/
void tMinimap::onMouseMove(int x, int y)
{
    tStdDialogBox::onMouseMove(x,y);
    if(getState()!=WINDOW_MINIMIZED && objectsLoaded && isLeftButtonPressed() && !targetted)
    {
        int xr=x-getControlX()-originX;
        int yr=y-getControlY()-originY;
        if(xr>=0 && yr>=0 && xr<minimapWidth && yr<minimapHeight)
            panMinimap(xr,yr);
    }
}
/****************************************************************************/
void tMinimap::onMouseUp(int x, int y, eMouseButton button)
{
    tStdDialogBox::onMouseUp(x,y,button);
    targetted=false;
}
/****************************************************************************/
void tMinimap::onKeyDown(tKey key, char ch)
//route keystrokes from minimap to background
{
    tStdDialogBox::onKeyDown(key,ch);
    if(gameWindow)
    {
        focusWindow(gameWindow);
        gameWindow->onKeyDown(key,ch);
    }
}
/****************************************************************************/
void tMinimap::onKeyUp(tKey virtualKey, char ch)
//route keystrokes from minimap to background
{
    tStdDialogBox::onKeyUp(virtualKey,ch);
    if(gameWindow)
    {
        focusWindow(gameWindow);
        gameWindow->onKeyUp(virtualKey,ch);
    }
}
/****************************************************************************/
void tMinimap::onMouseWheel(int pos, eMouseWheel direction)
//route keystrokes from minimap to background
{
    tStdDialogBox::onMouseWheel(pos,direction);
    if(gameWindow)
    {
        focusWindow(gameWindow);
        gameWindow->onMouseWheel(pos,direction);
    }
}
/****************************************************************************/
void tMinimap::onDestroy()
{
    tStdDialogBox::onDestroy();
    minimap=NULL;
}
/****************************************************************************/
void tMinimap::onMinimize()
//Replace default behavior
{
    setPosition(getScreenWidth()-1-100,1+(getTitleBarHeight()+2*getMargin())*2);
    setSize(100,getTitleBarHeight()+2*getMargin());
}
/****************************************************************************/
bool tMinimap::getMousePointer(int x, int y, ePointer& p, int& tag)
{
    if(tStdDialogBox::getMousePointer(x,y,p,tag))
        return true;

    //The minimap will display a target pointer if the user is currently
    //targetting a command.
    if(curCommand!=COMMAND_NONE && (firstTarget||isShiftPressed()))
    {
        p=POINTER_TARGET;
        tag=0;
        return true;
    }
    return false;
}
/****************************************************************************/
// COMMAND PANEL
/****************************************************************************/
void tCommandPanel::enableCommand(eCommand command)
{
    int i;
    tCommandButton *b=(tCommandButton *)controlLookup((int) command);
    if(b==NULL) //This is a custom button
    {
        for(i=0;i<5;i++)
            if(customButtons[i]==NULL)
                break;
        if(i==5) //Whoops!  Not enough room to show the button
            return;
        b=mwNew tCommandButton(5+i*40,45,command);
        createControl(b);
        customButtons[i]=b;
    }
    b->enable();
}
/****************************************************************************/
void tCommandPanel::enableBuildOption(eCommand command)
{
    tCommandButton *b;
    int i;
    for(i=0;i<5;i++)
        if(buildButtons[i]==NULL)
            break;
    if(i==5) //Whoops!  Not enough room to show the button
        return;
    b=mwNew tCommandButton(5+i*40,5,command);
    createControl(b);
    buildButtons[i]=b;
    b->enable();
}
/****************************************************************************/
void tCommandPanel::disableCommand(eCommand command)
{
    int i;
    tCommandButton *b=(tCommandButton *)controlLookup((int) command);
    if(command<STANDARD_COMMANDS)
    {
        if(b==NULL)
        {
            bug("tCommandPanel::disableCommand: command %d not found",(int)command);
            return;
        }
        b->disable();
    }
    else
    {
        if(b==NULL) //No need to output an error message since there may not be room for button in command panel
            return;
        b->destroy();
        for(i=0;i<5;i++)
            if(customButtons[i]==b)
            {
                customButtons[i]=NULL;
                break;
            }
    }
}
/****************************************************************************/
void tCommandPanel::disableBuildOption(eCommand command)
{
    int i;
    tCommandButton *b=(tCommandButton *)controlLookup((int) command);
    if(b==NULL) //No need to output an error message since there may not be room for button in command panel
        return;
    b->destroy();
    for(i=0;i<5;i++)
        if(buildButtons[i]==b)
        {
            buildButtons[i]=NULL;
            break;
        }
}
/****************************************************************************/
void tCommandPanel::highlightCommand(eCommand command)
{
    tCommandButton *b=(tCommandButton *)controlLookup((int) command);
    if(b==NULL)
    {
        //No need to output error message since there might not be enough room to show it.
        //bug("tCommandPanel::highlightCommand: command %d not found",(int)command);
        return;
    }
    b->setHighlight(true);
}
/****************************************************************************/
void tCommandPanel::resetCommand(eCommand command)
{
    tCommandButton *b=(tCommandButton *)controlLookup((int) command);
    if(b==NULL)
    {
        //No need to output error message since there might not be enough room to show it.
        //bug("tCommandPanel::resetCommand: command %d not found",(int)command);
        return;
    }
    b->setHighlight(false);
}
/****************************************************************************/
void tCommandPanel::activateCommand(eCommand command)
{
    tCommandButton *b=(tCommandButton *)controlLookup((int)command);
    if(b==NULL)
    {
        //No need to output error message since there might not be enough room to show it.
        //bug("tCommandPanel::activateCommand: command %d not found",(int)command);
        return;
    }
    b->setActive(true);
}
/****************************************************************************/
void tCommandPanel::deactivateCommand(eCommand command)
{
    tCommandButton *b=(tCommandButton *)controlLookup((int)command);
    if(b==NULL)
    {
        //No need to output error message since there might not be enough room to show it.
        //bug("tCommandPanel::activateCommand: command %d not found",(int)command);
        return;
    }
    b->setActive(false);
}
/****************************************************************************/
void tCommandPanel::setCommandQuantity(eCommand command, int quantity)
{
    tCommandButton *b=(tCommandButton *)controlLookup((int)command);
    if(b==NULL)
    {
        //No need to output error message since there might not be enough room to show it.
        //bug("tCommandPanel::setCommandQuantity: command %d not found",(int)command);
        return;
    }
    b->setQuantity(quantity);
}
/****************************************************************************/
void tCommandPanel::setCommandRepeat(eCommand command, int repeat)
{
    tCommandButton *b=(tCommandButton *)controlLookup((int)command);
    if(b==NULL)
    {
        //No need to output error message since there might not be enough room to show it.
        //bug("tCommandPanel::setCommandRepeat command %d not found",(int)command);
        return;
    }
    b->setRepeat(repeat);
}
/****************************************************************************/
tCommandPanel::tCommandPanel() : tStdDialogBox("Commands",smallFont,getScreenWidth()-206,
    1,205,225,WINDOW_NORMAL,
    false,false,true,/*true*/false,false,true,false,100,-1,100,-1)
{
    tCommandButton *b;
    int i, x, y;

    //Load the standard command buttons
    for(i=1;i<STANDARD_COMMANDS;i++) //Skip 0 since 0 equals COMMAND_NONE
    {
        x=5+((i-1)%5)*40;
        y=165-((i-1)/5)*40;
        b=mwNew tCommandButton(x,y,i);
        createControl(b);
    }

    //No custom buttons exist yet
    for(i=0;i<5;i++)
        customButtons[i]=NULL;
    for(i=0;i<5;i++)
        buildButtons[i]=NULL;
    tag=COMMAND_PANEL;
    setAnchors(ANCHOR_RIGHT,ANCHOR_BOTTOM);
}
/****************************************************************************/
void tCommandPanel::reset()
{
    int i;
    tCommandButton *b;
    for(i=1;i<STANDARD_COMMANDS;i++)
    {
        b=(tCommandButton *)controlLookup(i);
        if(b)
            b->reset();
    }
    for(;i<commandCount;i++)
    {
        b=(tCommandButton *)controlLookup(i);
        if(b)
            b->destroy();
    }
    //We just cleared all custom buttons
    for(i=0;i<5;i++)
        customButtons[i]=NULL;
    for(i=0;i<5;i++)
        buildButtons[i]=NULL;
}
/****************************************************************************/
void tCommandPanel::onDestroy()
{
    tStdDialogBox::onDestroy();
    commandPanel=NULL;
}
/****************************************************************************/
void tCommandPanel::onKeyDown(tKey key, char ch)
//route keystrokes from command panel to background
{
    tStdDialogBox::onKeyDown(key,ch);
    if(gameWindow)
    {
        focusWindow(gameWindow);
        gameWindow->onKeyDown(key,ch);
    }
}
/****************************************************************************/
void tCommandPanel::onKeyUp(tKey virtualKey, char ch)
//route keystrokes from command panel to background
{
    tStdDialogBox::onKeyUp(virtualKey,ch);
    if(gameWindow)
    {
        focusWindow(gameWindow);
        gameWindow->onKeyUp(virtualKey,ch);
    }
}
/****************************************************************************/
void tCommandPanel::onMouseWheel(int pos, eMouseWheel direction)
//route keystrokes from command panel to background
{
    tStdDialogBox::onMouseWheel(pos,direction);
    if(gameWindow)
    {
        focusWindow(gameWindow);
        gameWindow->onMouseWheel(pos,direction);
    }
}
/****************************************************************************/
void tCommandPanel::onMinimize()
//Replace default behavior
{
    setPosition(getScreenWidth()-1-100,1);
    setSize(100,getTitleBarHeight()+2*getMargin());
}
/****************************************************************************/
bool tCommandPanel::doesCommandExist(eCommand command)
{
    tCommandButton *b=(tCommandButton *)controlLookup((int) command);
    return b!=NULL;
}
/****************************************************************************/
// CHAT WINDOW
/****************************************************************************/
tChatWindow::tChatWindow() : tStdDialogBox("Chat",smallFont,getScreenWidth()-206,226,
    205,103,WINDOW_NORMAL,false,false,true,true,false,true,false,100,-1,
    100,-1)
{
    meOutput = mwNew tStdTextBox(5,5,getWidth()-10,getHeight()-12-getTitleBarHeight(),
        "",-1,true,false,smallFont,true,true);
    meOutput->setAnchors(ANCHOR_BOTH, ANCHOR_BOTH);
    createControl(meOutput);

    tag=CHAT_WINDOW;
    setAnchors(ANCHOR_RIGHT,ANCHOR_BOTTOM);
}
/****************************************************************************/
void tChatWindow::onDestroy()
{
    tStdDialogBox::onDestroy();
    chatWindow=NULL;
}
/****************************************************************************/
void tChatWindow::onMinimize()
//Replace default behavior
{
    setPosition(getScreenWidth()-1-100,1+getTitleBarHeight()+2*getMargin());
    setSize(100,getTitleBarHeight()+2*getMargin());
}
/****************************************************************************/
void tChatWindow::onShowMessage(const tString& str, eTextMessage type)
{
    meOutput->addString(formatMessage(str,type));
    meOutput->setWindowPosition(0, MAX(0,meOutput->getWindowHeight()-meOutput->visWindowHeight()));
}
/****************************************************************************/
void tChatWindow::onKeyDown(tKey key, char ch)
//route keystrokes from chat window to background
{
    tStdDialogBox::onKeyDown(key,ch);
    if(gameWindow)
    {
        focusWindow(gameWindow);
        gameWindow->onKeyDown(key,ch);
    }
}
/****************************************************************************/
void tChatWindow::onKeyUp(tKey virtualKey, char ch)
//route keystrokes from chat window to background
{
    tStdDialogBox::onKeyUp(virtualKey,ch);
    if(gameWindow)
    {
        focusWindow(gameWindow);
        gameWindow->onKeyUp(virtualKey,ch);
    }
}
/****************************************************************************/
void tChatWindow::onMouseWheel(int pos, eMouseWheel direction)
//route keystrokes from chat window to background
{
    tStdDialogBox::onMouseWheel(pos,direction);
    if(gameWindow)
    {
        focusWindow(gameWindow);
        gameWindow->onMouseWheel(pos,direction);
    }
}
/****************************************************************************/
void tChatWindow::onHide()
{
    tStdDialogBox::onHide();
    meOutput->clear();
}
/****************************************************************************/
// GAME WINDOW
/****************************************************************************/
tGameWindow::tGameWindow() : tWindow(0,0,getScreenWidth(),getScreenHeight(),false,true,false),
    anchor(zeroVector2D),panDir(DIR_NONE),panDevice(PAN_NONE),
    selectionX(0), selectionY(0),selectingLeft(false),selectingRight(false),
    recipientType(RECIPIENT_NONE), recipient(NULL), spinDir(SPIN_NONE), zoomDir(ZOOM_NONE),
    tiltDir(TILT_NONE), viewCenter(zeroVector3D), mapRotationAnchor(0), mapScaleAnchor(0),
    mapTiltAnchor(0), firstHotKey('\0'), selectionOrigin(zeroVector3D), orientingMapWithMouse(false),
    mouseWheelAnchorX(0), mouseWheelAnchorY(0)
{
    //Let the engine know the dimensions of the view:
    setViewDimensions(getScreenWidth(),getScreenHeight());
    edInput=mwNew tMyEdit(1,1,getScreenWidth()-2,"",smallFont,false,(tEvent) &tGameWindow::onEnterText);
    edInput->setAnchors(ANCHOR_BOTH, ANCHOR_BOTTOM);
    createControl(edInput);
    laRecipient=mwNew tStdLabel(0,20,"",smallFont,1,1,1);
    createControl(laRecipient);
    hideControl(edInput);
    hideControl(laRecipient);
    tag=GAME_WINDOW;
    setAnchors(ANCHOR_BOTH,ANCHOR_BOTH);
}
/****************************************************************************/
void tGameWindow::onEnterText(tControl *sender)
{
    char buffer[1000];
    tString string=CHARPTR(edInput->getString()); //Make a copy of the edit's string.
    tString error;
    edInput->clear();
    if(recipientType==RECIPIENT_ALL && string[0]=='/' && gameRunning)
    {
        string[0]=' '; //Replace the front-slash with a space so that it doesn't interfere with parsing.
        try
        {
            parseEntry(string,/*inGame=*/true);
        }
        catch(const tString& message)
        {
            messageBox(message, "Shell Error", CHOICES_OK);
            return;
        }
        //First character of the shell command already contains a space.
        snprintf(buffer,sizeof(buffer), "*** %s executed%s ***", CHARPTR(localUser->name), CHARPTR(string));
        sendText(buffer, TEXT_MESSAGE_SHELL);
        showMessage(buffer, TEXT_MESSAGE_SHELL);
        recipientType=RECIPIENT_NONE;
    }
    else
    {
        switch(recipientType)
        {
            case RECIPIENT_ALL:
                snprintf(buffer,sizeof(buffer), "%s: %s", CHARPTR(localUser->name), CHARPTR(string));
                sendText(buffer, TEXT_MESSAGE_CHAT);
                recipientType=RECIPIENT_NONE;
                break;
            case RECIPIENT_ALLIES:
                snprintf(buffer,sizeof(buffer), "%s -> Allies: %s", CHARPTR(localUser->name), CHARPTR(string));
                sendTextToAllies(buffer, TEXT_MESSAGE_CHAT);
                recipientType=RECIPIENT_NONE;
                break;
            case RECIPIENT_ENEMIES:
                snprintf(buffer,sizeof(buffer), "%s -> Enemies: %s", CHARPTR(localUser->name), CHARPTR(string));
                sendTextToEnemies(buffer, TEXT_MESSAGE_CHAT);
                recipientType=RECIPIENT_NONE;
                break;
            case RECIPIENT_PLAYER:
                if(recipient)
                {
                    if(MEMORY_VIOLATION(recipient))
                        bug("tGameWindow::onEnterText: dangling pointer detected");
                    else
                    {
                        snprintf(buffer,sizeof(buffer), "%s -> %s: %s", CHARPTR(localUser->name),
                            CHARPTR(recipient->name), CHARPTR(string));
                        sendTextToUser(recipient, buffer, TEXT_MESSAGE_CHAT);
                        replyTo=recipient->name;
                    }
                    recipientType=RECIPIENT_NONE;
                    recipient=NULL;
                }
                else
                {
                    recipient=userLookup(string,/*exact=*/false);
                    if(recipient==localUser)
                    {
                        messageBox("You can't send a message to yourself, silly!", "Error", CHOICES_OK);
                        recipient=NULL;
                        return;
                    }
                    else if(recipient==NULL || recipient->descriptor==NULL)
                    {
                        messageBox("Unknown User", "Error", CHOICES_OK);
                        recipient=NULL;
                        return;
                    }
                    recipientType=RECIPIENT_PLAYER;
                    snprintf(buffer,sizeof(buffer), "To %s:", CHARPTR(recipient->name));
                    laRecipient->setCaption(buffer);
                    return;
                }
                break;
        }
        showMessage(buffer, TEXT_MESSAGE_ECHO);
    }
}
/****************************************************************************/
void tGameWindow::onKeyDown(tKey key, char ch)
{
    int i;
    eDir newPanDir=DIR_NONE;
    eSpin newSpinDir=SPIN_NONE;
    eZoom newZoomDir=ZOOM_NONE;
    eTilt newTiltDir=TILT_NONE;
    tWindow *w;
    tKey oldHotKey=firstHotKey;
    firstHotKey=0;

    //We don't want CTRL+A to append an 'A' to the end of edInput.
    if(!isAltPressed() && !isCtrlPressed())
        tWindow::onKeyDown(key,ch);

    if(paramType==PARAM_CONSTRUCTION && !firstTarget && isShiftPressed())
        createApparition(curCommand,saveMouseX,saveMouseY);

    if(key==GLFW_KEY_ESC)
    {
        if(edInput->isVisible())
        {
            hideControl(edInput);
            recipientType=RECIPIENT_NONE;
            recipient=NULL;
            hideControl(laRecipient);
        }
        else if(curCommand!=COMMAND_NONE || oldHotKey!='\0')
            setCurCommand(COMMAND_NONE);
        else
        {
            w=mwNew tGameMenu();
            createWindow(w);
        }
    }
    else if(key==GLFW_KEY_ENTER && edInput->isVisible())
    {
        if(recipientType==RECIPIENT_NONE)
        {
            hideControl(edInput);
            hideControl(laRecipient);
        }
    }
    else if((key==GLFW_KEY_ENTER || key=='\'' || key==',' || key=='.') &&
        !edInput->isVisible())
    {
        edInput->clear();
        showControl(edInput);
        focusControl(edInput);
        if(key==',')
        {
            recipientType=RECIPIENT_ENEMIES;
            laRecipient->setCaption("To Enemies:");
        }
        else if(key=='.')
        {
            recipientType=RECIPIENT_ALLIES;
            laRecipient->setCaption("To Allies:");
        }
        else if(key=='\'')
        {
            recipientType=RECIPIENT_PLAYER;
            recipient=NULL;
            laRecipient->setCaption("To Whom?");
            edInput->setString(replyTo);
        }
        else
        {
            recipientType=RECIPIENT_ALL;
            laRecipient->setCaption("To All:");
        }
        showControl(laRecipient);
    }
    else if(key=='/')
    {
        if(!edInput->isVisible())
        {
            edInput->setString("/");
            showControl(edInput);
            focusControl(edInput);
            recipientType=RECIPIENT_ALL;
            laRecipient->setCaption("To All:");
            showControl(laRecipient);
        }
    }
    else if(key=='A' && isCtrlPressed())
    {
        if(curCommand!=COMMAND_NONE)
        {
            if(firstTarget || isShiftPressed())
            {
                resetFirstTarget();
                targetAll(isShiftPressed(), curCommand);
            }
            else
                setCurCommand(COMMAND_NONE);
        }
        if(curCommand==COMMAND_NONE)
            selectAll(isShiftPressed());
    }
    else if(isAltPressed() && key==GLFW_KEY_BACKSPACE)
        restartGame();
    else if(isalpha(ch) && isAltPressed())
    {
        switch(key)
        {
            case 'P':
                if(isGamePaused())
                    resumeGame();
                else
                    pauseGame();
                break;
            case 'R':
                resumeGame();
                break;
            case 'E':
                closeWindows();
                displayWindow(SETUP_GAME_MENU);
                endGame();
                break;
            case 'D':
                abortSession(0);
                disconnect();
                break;
            case 'A':
                displayWindow(ALLIANCE_MENU);
                break;
            case 'O':
                displayWindow(GAME_OPTIONS_MENU);
                break;
            case 'T':
                displayWindow(TIMING_MENU);
                break;
        }
    }
    else if(getFocusedControl()==NULL)
    {
        if(isdigit(ch))
        {
            if(isAltPressed())
                assignToGroup(key-'0',isShiftPressed());
            else
            {
                if(curCommand!=COMMAND_NONE)
                {
                    if(firstTarget || isShiftPressed())
                    {
                        resetFirstTarget();
                        targetGroup(key-'0',isShiftPressed(),curCommand);
                    }
                    else
                        setCurCommand(COMMAND_NONE);
                }
                if(curCommand==COMMAND_NONE)
                    selectGroup(key-'0',isShiftPressed());
            }
            return;
        }
        else if(key=='-')
        {
            setTiming(latency, netInterval, gameInterval, MAX(gameSpeed*0.75,0.1));
            return;
        }
        else if(key=='=')
        {
            setTiming(latency, netInterval, gameInterval, MIN(gameSpeed*1.33,10.0));
            return;
        }

        for(i=0;i<commandCount;i++)
            if(oldHotKey==commandTable[i].hotKey[0] && key==commandTable[i].hotKey[1])
            {
                setCurCommand((eCommand)i);
                return;
            }
        for(i=0;i<commandCount;i++)
            if(key==commandTable[i].hotKey[0] && commandTable[i].hotKey[1]==0)
            {
                setCurCommand((eCommand)i);
                return;
            }

        panView();
        switch(key)
        {
            case GLFW_KEY_LEFT:
                newPanDir=DIR_LEFT;
                break;
            case GLFW_KEY_RIGHT:
                newPanDir=DIR_RIGHT;
                break;
            case GLFW_KEY_UP:
                newPanDir=DIR_UP;
                break;
            case GLFW_KEY_DOWN:
                newPanDir=DIR_DOWN;
                break;
            case GLFW_KEY_HOME:
                newTiltDir=TILT_UP;
                break;
            case GLFW_KEY_END:
                newTiltDir=TILT_DOWN;
                break;
            case GLFW_KEY_PAGEUP:
                newZoomDir=ZOOM_OUT;
                break;
            case GLFW_KEY_PAGEDOWN:
                newZoomDir=ZOOM_IN;
                break;
            case GLFW_KEY_KP_DIVIDE:
                newSpinDir=SPIN_CCW;
                break;
            case GLFW_KEY_KP_MULTIPLY:
                newSpinDir=SPIN_CW;
                break;
            default:
                firstHotKey=key;
                return;
        }

        //We must compare newDir with panDir since this function will repeatedly
        //execute as long as the user holds the key down.
        if(newPanDir!=panDir || panDevice!=PAN_KEYBOARD)
        {
            panDir=newPanDir;
            if(panDir==DIR_NONE)
                panDevice=PAN_NONE;
            else
            {
                startTime=currentTime;
                anchor=mapOffset;
                panDevice=PAN_KEYBOARD;
            }
        }

        if(newSpinDir != spinDir) //Function will repeatedly execute as long as key is depressed
        {
            spinDir = newSpinDir;
            if(spinDir!=SPIN_NONE)
            {
                //Cancel spinning with mouse
                orientingMapWithMouse=false;

                //Initialize state variables:
                viewCenter=getViewCenter();
                mapRotationAnchor=mapRotation;
                startTime=currentTime;
            }
        }

        if(newZoomDir != zoomDir) //Function will repeatedly execute as long as key is depressed
        {
            zoomDir = newZoomDir;
            if(zoomDir!=ZOOM_NONE)
            {
                //Initialize state variables:
                viewCenter=getViewCenter();
                mapScaleAnchor=mapScale;
                startTime=currentTime;
            }
        }

        if(newTiltDir != tiltDir) //Function will repeatedly execute as long as key is depressed
        {
            //Cancel spinning with mouse
            orientingMapWithMouse=false;

            tiltDir = newTiltDir;
            if(tiltDir!=TILT_NONE)
            {
                //Initialize state variables:
                viewCenter=getViewCenter();
                mapTiltAnchor=mapTilt;
                startTime=currentTime;
            }
        }
    }
}
/****************************************************************************/
void tGameWindow::onKeyUp(tKey key, char ch)
{
    tWindow::onKeyUp(key,ch);
    panView();

    if(paramType==PARAM_CONSTRUCTION && !firstTarget && !isShiftPressed())
        destroyApparition();

    if(panDevice==PAN_KEYBOARD)
    {
        switch(panDir)
        {
            case DIR_LEFT:
                if(isKeyPressed(GLFW_KEY_LEFT))
                    return;
                break;
            case DIR_RIGHT:
                if(isKeyPressed(GLFW_KEY_RIGHT))
                    return;
                break;
            case DIR_UP:
                if(isKeyPressed(GLFW_KEY_UP))
                    return;
                break;
            case DIR_DOWN:
                if(isKeyPressed(GLFW_KEY_DOWN))
                    return;
                break;
            default:
                return;
        }
        panDir=DIR_NONE;
        panDevice=PAN_NONE;
    }
    else if(spinDir!=SPIN_NONE)
    {
        switch(spinDir)
        {
            case SPIN_CCW:
                if(isKeyPressed(GLFW_KEY_HOME))
                    return;
                break;
            case SPIN_CW:
                if(isKeyPressed(GLFW_KEY_END))
                    return;
                break;
        }
        spinDir=SPIN_NONE;
    }
    else if(zoomDir!=ZOOM_NONE)
    {
        switch(zoomDir)
        {
            case ZOOM_OUT:
                if(isKeyPressed(GLFW_KEY_PAGEUP))
                    return;
                break;
            case ZOOM_IN:
                if(isKeyPressed(GLFW_KEY_PAGEDOWN))
                    return;
                break;
        }
        zoomDir=ZOOM_NONE;
    }
    else if(tiltDir!=TILT_NONE)
    {
        switch(tiltDir)
        {
            case TILT_UP:
                if(isKeyPressed(GLFW_KEY_HOME))
                    return;
                break;
            case TILT_DOWN:
                if(isKeyPressed(GLFW_KEY_END))
                    return;
                break;
        }
        tiltDir=TILT_NONE;
    }
}
/****************************************************************************/
void tGameWindow::onMouseDown(int x, int y, eMouseButton button)
{
    tWindow::onMouseDown(x,y,button);
    //We don't want to lose our selection when we click inside the text box.
    if(getSelectedControl())
        return;

    int xr=x;
    int yr=y;

    //cancel current selection
    selectingLeft=false;
    selectingRight=false;
    orientingMapWithMouse=false;

    //Cancel two-letter key combination
    firstHotKey='\0';

    //initialize selectionX and selectionY in case this function initiates selection
    selectionX=xr;
    selectionY=yr;
    selectionOrigin=pixelToTerrain3D(selectionX,selectionY);

    if(button==MOUSE_BUTTON_LEFT)
    {
        selectingLeft=true;
        if(curCommand!=COMMAND_NONE && !firstTarget && !isShiftPressed())
            setCurCommand(COMMAND_NONE);
        if(curCommand==COMMAND_NONE)
            selectPoint(xr, yr, isAltPressed(), isCtrlPressed(), isShiftPressed());
    }
    else if(button==MOUSE_BUTTON_RIGHT)
    {
        if(curCommand!=COMMAND_NONE)
        {
            if(!firstTarget)
                selectingRight=true;
            setCurCommand(COMMAND_NONE);
        }
        else
            selectingRight=true;
    }
    else if(button==MOUSE_BUTTON_MIDDLE)
    {
        //Cancel spinning and tilting by keyboard
        spinDir=SPIN_NONE;
        tiltDir=TILT_NONE;

        orientingMapWithMouse=true;
        mouseWheelAnchorX=x;
        mouseWheelAnchorY=y;
        mapRotationAnchor=mapRotation;
        mapTiltAnchor=mapTilt;

    }
    adjustMousePan(x,y);
}
/****************************************************************************/
void tGameWindow::onMouseMove(int x, int y)
{
    tWindow::onMouseMove(x,y);
    adjustMousePan(x,y);
    if(orientingMapWithMouse)
    {
        int dx=x-mouseWheelAnchorX;
        int dy=y-mouseWheelAnchorY;
        float rotation=mapRotationAnchor - dx*mouseSpinRate;
        float tilt=mapTiltAnchor - dy*mouseTiltRate;
        clampRange(tilt,0.0f,(float)(3*M_PI/8));
        spinMap(rotation);
        tiltMap(tilt);
    }

    if(paramType==PARAM_CONSTRUCTION && (firstTarget||isShiftPressed()))
        createApparition(curCommand,x,y);
}
/****************************************************************************/
void tGameWindow::onMouseUp(int x, int y, eMouseButton button)
{
    tWindow::onMouseUp(x,y,button);
    adjustMousePan(x,y);
    int xr=x;
    int yr=y;
    makeValid(xr, yr, viewWidth, viewHeight);
    int x1=selectionX, x2=xr;
    int y1=selectionY, y2=yr;
    bool selectionOutsideView=(x1<0 || y1<0 || x1>=viewWidth || y1>=viewHeight);

    if(selectingLeft)
    {
        selectingLeft=false;
        if(curCommand==COMMAND_NONE)
        {
            if(selectionOutsideView || (DIFF(x1,x2)>=5 && DIFF(y1,y2)>=5))
            {
                if(selectionOutsideView)
                    selectRange(x1,y1,x2,y2,to2D(selectionOrigin),isShiftPressed());
                else
                    selectRange(x1,y1,x2,y2,isShiftPressed());
            }
        }
        else
        {
            resetFirstTarget();
            if( (selectionOutsideView || (DIFF(x1,x2)>=5 && DIFF(y1,y2)>=5)) && paramType==PARAM_UNIT )
            {
                if(selectionOutsideView)
                    targetRange(x1,y1,x2,y2,isShiftPressed(),curCommand);
                else
                    targetRange(x1,y1,x2,y2,to2D(selectionOrigin),isShiftPressed(),curCommand);
            }
            else
                targetPoint(x1,y1,isAltPressed(),isCtrlPressed(), isShiftPressed(),curCommand);
        }
    }
    else if(selectingRight)
    {
        selectingRight=false;
        if(selectionOutsideView || (DIFF(x1,x2)>=5 && DIFF(y1,y2)>=5))
        {
            if(selectionOutsideView)
                targetRange(x1,y1,x2,y2,isShiftPressed(),COMMAND_NONE);
            else
                targetRange(x1,y1,x2,y2,to2D(selectionOrigin),isShiftPressed(),COMMAND_NONE);
        }
        else
            targetPoint(x1,y1,isAltPressed(),isCtrlPressed(),isShiftPressed(),COMMAND_NONE);
    }
    orientingMapWithMouse=false;
}
/****************************************************************************/
void tGameWindow::onSelect(int x, int y)
{
    tWindow::onSelect(x,y);

    if(paramType==PARAM_CONSTRUCTION && (firstTarget||isShiftPressed()))
        createApparition(curCommand,x,y);
}
/****************************************************************************/
void tGameWindow::onDeselect()
{
    tWindow::onDeselect();
    adjustMousePan(-1,-1); //ensure mouse is not on edge
    destroyApparition();
}
/****************************************************************************/
void tGameWindow::onDefocus()
{
    tWindow::onDefocus();
    selectingLeft=false;
    selectingRight=false;
    orientingMapWithMouse=false;
    firstHotKey='\0';
    spinDir=SPIN_NONE;
    zoomDir=ZOOM_NONE;
    tiltDir=TILT_NONE;
    if(panDevice==PAN_KEYBOARD)
    {
        panDevice=PAN_NONE;
        panDir=DIR_NONE;
    }
}
/****************************************************************************/
void tGameWindow::panView()
{
    bool found=false;
    if(spinDir != SPIN_NONE)
    {
        float sign=(spinDir==SPIN_CCW?1:-1);
        spinMap(mapRotationAnchor+sign*(currentTime-startTime)*spinRate);
        centerView(viewCenter);
        vector3DToPixel(selectionOrigin,selectionX,selectionY);
        //We intentionally clamp selectionX and selectionY to a rectangle that
        //is one pixel larger than the view.  This hides the edges that are outside
        //the view.
        clampRange(selectionX,-1,viewWidth);
        clampRange(selectionY,-1,viewHeight);
        found=true;
    }

    if(zoomDir != ZOOM_NONE)
    {
        float sign=(zoomDir==ZOOM_IN?1:-1);
        float zoomFactor=exp(sign*(currentTime-startTime)*zoomRate);
        zoomMap(mapScaleAnchor*zoomFactor);
        centerView(viewCenter);
        vector3DToPixel(selectionOrigin,selectionX,selectionY);
        //We intentionally clamp selectionX and selectionY to a rectangle that
        //is one pixel larger than the view.  This hides the edges that are outside
        //the view.
        clampRange(selectionX,-1,viewWidth);
        clampRange(selectionY,-1,viewHeight);
        found=true;
    }

    if(tiltDir != TILT_NONE)
    {
        float sign=(tiltDir==TILT_DOWN?1:-1);
        float angle=mapTiltAnchor+sign*(currentTime-startTime)*tiltRate;
        clampRange(angle,0.0f,(float)(3*M_PI/8));
        tiltMap(angle);
        centerView(viewCenter);
        vector3DToPixel(selectionOrigin,selectionX,selectionY);
        //We intentionally clamp selectionX and selectionY to a rectangle that
        //is one pixel larger than the view.  This hides the edges that are outside
        //the view.
        clampRange(selectionX,-1,viewWidth);
        clampRange(selectionY,-1,viewHeight);
        found=true;
    }

    if(panDir != DIR_NONE)
    {
        float dx=dirX[panDir]*(currentTime-startTime)*panRate;
        float dy=dirY[panDir]*(currentTime-startTime)*panRate;
        tVector2D _mapOffset={
            anchor.x + panMatrix[0]*dx + panMatrix[2]*dy,
            anchor.y + panMatrix[1]*dx + panMatrix[3]*dy};
        panMap(_mapOffset);
        vector3DToPixel(selectionOrigin,selectionX,selectionY);
        //We intentionally clamp selectionX and selectionY to a rectangle that
        //is one pixel larger than the view.  This hides the edges that are outside
        //the view.
        clampRange(selectionX,-1,viewWidth);
        clampRange(selectionY,-1,viewHeight);
        found=true;
    }

    if(found && paramType==PARAM_CONSTRUCTION && (firstTarget||isShiftPressed()))
        createApparition(curCommand,saveMouseX,saveMouseY);
}
/****************************************************************************/
void tGameWindow::adjustMousePan(int x, int y)
{
    eDir newDir=DIR_NONE;
    if(x==0||y==0||x==getScreenWidth()-1||y==getScreenHeight()-1)
    {
        if(x==0)
        {
            if(y==0)
                newDir=DIR_DOWN_LEFT;
            else if(y==getScreenHeight()-1)
                newDir=DIR_UP_LEFT;
            else
                newDir=DIR_LEFT;
        }
        else if(x==getScreenWidth()-1)
        {
            if(y==0)
                newDir=DIR_DOWN_RIGHT;
            else if(y==getScreenHeight()-1)
                newDir=DIR_UP_RIGHT;
            else
                newDir=DIR_RIGHT;
        }
        else if(y==0)
            newDir=DIR_DOWN;
        else if(y==getScreenHeight()-1)
            newDir=DIR_UP;
    }
    if(newDir!=DIR_NONE)
    {
        if(newDir!=panDir || panDevice!=PAN_MOUSE)
        {
            startTime=currentTime;
            anchor=mapOffset;
            panDir=newDir;
            panDevice=PAN_MOUSE;
            spinDir=SPIN_NONE;
            zoomDir=ZOOM_NONE;
            tiltDir=TILT_NONE;
        }
    }
    else if(panDevice==PAN_MOUSE)
    {
        panDir=DIR_NONE;
        panDevice=PAN_NONE;
    }
}
/****************************************************************************/
void tGameWindow::poll()
{
    tWindow::poll();
    panView();
    if(textMessages.size()>0)
    {
        if(currentTime>nextLine)
        {
            textMessages.erase(textMessages.begin());
            if(textMessages.size()>0)
                nextLine+=textDuration(textMessages[0]);
        }
    }
}
/****************************************************************************/
void tGameWindow::drawWindow()
{
    char buf[20];
    int i,r,l=viewWidth,t=viewHeight,b;
    int x=5,y=getScreenHeight()-14,w=10,h=10;
    bool foreignAid;

    tWindow::drawWindow();

    glViewport(0,0,viewWidth,viewHeight);
    glPushMatrix();
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0,viewWidth,0,viewHeight,1,-1);
    glMatrixMode(GL_MODELVIEW);

    if(selectingLeft||selectingRight)
    {
        int endx=saveMouseX, endy=saveMouseY;
        makeValid(endx, endy, viewWidth, viewHeight);
        glColor3f(0,1,0);
        glBegin(GL_LINES);
            glVertex2i(selectionX,selectionY);
            glVertex2i(endx,selectionY);

            glVertex2i(endx,selectionY);
            glVertex2i(endx,endy);

            glVertex2i(endx,endy);
            glVertex2i(selectionX,endy);

            glVertex2i(selectionX,endy);
            glVertex2i(selectionX,selectionY);
        glEnd();
    }

    glViewport(0,0,getScreenWidth(),getScreenHeight());
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    if(gameFrozen)
        glColor3f(1,0,0);
    else
        glColor3f(0,1,0);
    glRecti(x,y,x+w,y+h);

    glColor3f(1,1,1);
    drawText(textMessages,1,getScreenHeight()-21,1,1,getScreenWidth()-2,getScreenHeight()-2,smallFont);

    foreignAid=canReceiveForeignAid();
    y=viewHeight-15;
    if(foreignAid)
        b=y-15;
    else
        b=y;

    //Draw resources from right to left
    for(i=MAX_RESOURCES-1;i>=0;i--)
        if(doesResourceExist(i))
        {
            r=l;
            l=r-stringWidth(CHARPTR(getResourceCaption(i)),smallFont)-180;

            glEnable(GL_BLEND);
            glColor4f(0,0,0,.5);
            glRecti(l,b,r,t);
            glDisable(GL_BLEND);

            glColor3f(0,0,1);
            snprintf(buf,sizeof(buf),"%+d",(int)getForeignAid(i));
            drawString(buf,r-30,y,smallFont);

            glColor3f(0,1,0);
            snprintf(buf,sizeof(buf),"+%d",(int)getResourceRevenue(i));
            drawString(buf,r-60,y,smallFont);
            if(foreignAid)
            {
                snprintf(buf,sizeof(buf),"+%d",(int)getCombinedRevenue(i));
                drawString(buf,r-60,b,smallFont);
            }

            glColor3f(1,0,0);
            snprintf(buf,sizeof(buf),"-%d",(int)getResourceCost(i));
            drawString(buf,r-90,y,smallFont);
            if(foreignAid)
            {
                snprintf(buf,sizeof(buf),"-%d",(int)getCombinedCost(i));
                drawString(buf,r-90,b,smallFont);
            }

            glColor3f(1,1,0);
            snprintf(buf,sizeof(buf),"%d/%d",(int)getResourceAmount(i),(int)getResourceCapacity(i));
            drawString(buf,r-170,y,smallFont);
            if(foreignAid)
            {
                snprintf(buf,sizeof(buf),"%d/%d",(int)getCombinedAmount(i),(int)getCombinedCapacity(i));
                drawString(buf,r-170,b,smallFont);
            }

            glColor3f(1,1,1);
            drawString(CHARPTR(getResourceCaption(i)),l+2,y,smallFont);
            drawString(CHARPTR(getResourceCaption(i)),l+3,y,smallFont);
        }
}
/****************************************************************************/
void tGameWindow::onHide()
{
    tWindow::onHide();
    firstHotKey='\0';
    if(edInput->isVisible())
    {
        hideControl(edInput);
        recipientType=RECIPIENT_NONE;
        recipient=NULL;
        hideControl(laRecipient);
    }
    if(curCommand!=COMMAND_NONE)
        setCurCommand(COMMAND_NONE);
    textMessages.clear();
}
/****************************************************************************/
void tGameWindow::onDestroy()
{
    tWindow::onDestroy();
    gameWindow=NULL;
}
/****************************************************************************/
void tGameWindow::onShowMessage(const tString& str, eTextMessage type)
{
    if(textMessages.size()==0)
        nextLine=currentTime+textDuration(str);
    textMessages.push_back(formatMessage(str,type));
    if(type==TEXT_MESSAGE_CHAT)
    {
        tStringBuffer sb(str);
        char *ptr=sb.begin();
        //Snarf the user's name from the line.  A semicolon or a space may follow
        //the user's name depending on the type of message.
        ptr = strtok(ptr, ":");
        ptr = strtok(NULL, " ");
        replyTo=ptr;
    }
}
/****************************************************************************/
float tGameWindow::textDuration(const tString& str)
//How long does the average Joe-Blow need to read this string?
{
    return 2+STRLEN(str)/15.0;
}
/****************************************************************************/
void tGameWindow::onDropUser(tUser *user)
{
    if(recipient==user)
    {
        hideControl(edInput);
        recipientType=RECIPIENT_NONE;
        recipient=NULL;
        hideControl(laRecipient);
    }
}
/****************************************************************************/
void tGameWindow::onSetSize(int w, int h)
{
    tWindow::onSetSize(w,h);
    setViewDimensions(w,h);
}
/****************************************************************************/
bool tGameWindow::getMousePointer(int x, int y, ePointer& p, int& tag)
{
    if(tWindow::getMousePointer(x,y,p,tag))
        return true;

    //Display a scroll pointer when the user scrolls the view using the mouse.
    if(panDevice==PAN_MOUSE && panDir!=DIR_NONE)
    {
        p=POINTER_SCROLL;
        tag=(int)panDir;
        return true;
    }

    //Display an invisible pointer if the user is targetting a building or
    //a target pointer if the user is targetting another command.
    if(curCommand!=COMMAND_NONE && (firstTarget||isShiftPressed()))
    {
        if(paramType==PARAM_CONSTRUCTION)
        {
            p=POINTER_INVISIBLE;
            tag=0;
        }
        else
        {
            p=POINTER_TARGET;
            tag=0;
        }
        return true;
    }
    return false;
}
/****************************************************************************/
void tGameWindow::onMouseWheel(int pos,eMouseWheel direction)
{
    if(direction==MOUSE_WHEEL_UP)
        zoomMap(mapScale*(1+mouseZoomRate));
    else if(direction==MOUSE_WHEEL_DOWN)
        zoomMap(mapScale/(1+mouseZoomRate));
}



/****************************************************************************/
// GLOBAL INTERFACE FUNCTIONS
/****************************************************************************/
tMessageBox *messageBox(const tString& message, const tString& title,
    eChoices choices, void (* event)(int))
{
    tStringList s;
    loadStringList(s, CHARPTR(message));
    tMessageBox *m;
    m=mwNew tMessageBox(s, title, choices, event);
    createWindow(m);
    return m;
}
/****************************************************************************/
void abortSession(int tag)
{
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    destroyWindow(setupMenu);
    closeWindows();
    displayWindow(MAIN_MENU);
}
/****************************************************************************/
void cancelConnect(int tag)
{
    disconnect();
}
/****************************************************************************/
void showMessage(const tString& str, eTextMessage type)
{
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu && setupMenu->isVisible())
    {
        setupMenu->onShowMessage(str, type);
        return;
    }
    if(gameWindow && gameWindow->isVisible())
        gameWindow->onShowMessage(str, type);
    if(chatWindow && chatWindow->isVisible())
        chatWindow->onShowMessage(str, type);
}
/****************************************************************************/
tString formatMessage(const tString& str, eTextMessage type)
{
    switch(type)
    {
        case TEXT_MESSAGE_ECHO:
            return FONT_YELLOW+str;
        case TEXT_MESSAGE_CHAT:
            return FONT_YELLOW+str;
        case TEXT_MESSAGE_SHELL:
            return FONT_GREEN+str;
    }
    return FONT_WHITE+str;
}
/****************************************************************************/
tWindow *displayWindow(int tag)
//If window with tag already exists, show and focus it
//If not, create it
{
    tWindow *w=windowLookup(tag);
    if(w)
    {
        showWindow(w);
        return w;
    }
    switch(tag)
    {
        case MAIN_MENU:
            w=mwNew tMainMenu();
            break;
        case OPTIONS_MENU:
            w=mwNew tOptionsMenu();
            break;
        case CONNECT_MENU:
            w=mwNew tConnectMenu();
            break;
        case SETUP_GAME_MENU:
            w=mwNew tSetupMenu();
            break;
        case GAME_WINDOW:
            w=mwNew tGameWindow();
            break;
        case MINIMAP_WINDOW:
            w=mwNew tMinimap();
            break;
        case COMMAND_PANEL:
            w=mwNew tCommandPanel();
            break;
        case ALLIANCE_MENU:
            w=mwNew tAllianceMenu();
            break;
        case GAME_OPTIONS_MENU:
            w=mwNew tGameOptionsMenu();
            break;
        case RECONNECT_MENU:
            w=mwNew tReconnectMenu();
            break;
        case MAP_MENU:
            w=mwNew tMapMenu();
            break;
        case TIMING_MENU:
            w=mwNew tTimingMenu();
            break;
        case CHAT_WINDOW:
            w=mwNew tChatWindow();
            break;
    }
    if(w)
    {
        createWindow(w);
        return w;
    }
    bug("displayWindow: Failed to create window with id %d",tag);
    return NULL;
}
/****************************************************************************/
void enableCommand(eCommand command)
{
#ifdef DEBUG
    if(command<0 || command>=commandCount)
    {
        bug("enableCommand: invalid command");
        return;
    }
#endif
    enableCounter[command]++;
    //Command was disabled before, but it is enabled now
    if(commandPanel && enableCounter[command]==1)
    {
        if(commandTable[command].paramType==PARAM_CONSTRUCTION ||
            commandTable[command].paramType==PARAM_PRODUCTION)
            commandPanel->enableBuildOption(command);
        else
            commandPanel->enableCommand(command);
    }
}
/****************************************************************************/
void disableCommand(eCommand command)
{
#ifdef DEBUG
    if(command<0 || command>=commandCount)
    {
        bug("disableCommand: invalid command");
        return;
    }
#endif
    enableCounter[command]--;
#ifdef DEBUG
    if(enableCounter[command]<0)
    {
        bug("disableCommand: enable counter < 0");
        return;
    }
#endif
    //Command was enabled before, but it is disabled now
    if(commandPanel && enableCounter[command]==0)
    {
        if(commandTable[command].paramType==PARAM_CONSTRUCTION ||
            commandTable[command].paramType==PARAM_PRODUCTION)
            commandPanel->disableBuildOption(command);
        else
            commandPanel->disableCommand(command);
    }
}
/****************************************************************************/
void highlightCommand(eCommand command)
{
#ifdef DEBUG
    if(command<0 || command>=commandCount)
    {
        bug("highlightCommand: invalid command");
        return;
    }
#endif
    highlightCounter[command]++;
    //Command was disabled before, but it is enabled now
    if(commandPanel && highlightCounter[command]==1)
        commandPanel->highlightCommand(command);
}
/****************************************************************************/
void resetCommand(eCommand command)
{
#ifdef DEBUG
    if(command<0 || command>=commandCount)
    {
        bug("resetCommand: invalid command");
        return;
    }
#endif
    highlightCounter[command]--;
#ifdef DEBUG
    if(highlightCounter[command]<0)
    {
        bug("resetCommand: highlight counter < 0");
        return;
    }
#endif
    //Command was enabled before, but it is disabled now
    if(commandPanel && highlightCounter[command]==0)
        commandPanel->resetCommand(command);
}
/****************************************************************************/
void adjustCommandQuantity(eCommand command, int amt)
{
#ifdef DEBUG
    if(command<0 || command>=commandCount)
    {
        bug("adjustCommandQuantity: invalid command");
        return;
    }
#endif
    quantityCounter[command]+=amt;
#ifdef DEBUG
    if(quantityCounter[command]<0)
    {
        bug("adjustCommandQuantity: quantity counter < 0");
        return;
    }
#endif
    if(commandPanel)
        commandPanel->setCommandQuantity(command, quantityCounter[command]);
}
/****************************************************************************/
void adjustCommandRepeat(eCommand command, int amt)
{
#ifdef DEBUG
    if(command<0 || command>=commandCount)
    {
        bug("adjustCommandRepeat: invalid command");
        return;
    }
#endif
    repeatCounter[command]+=amt;
#ifdef DEBUG
    if(repeatCounter[command]<0)
    {
        bug("adjustCommandRepeat: repeat counter < 0");
        return;
    }
#endif
    if(commandPanel)
        commandPanel->setCommandRepeat(command, repeatCounter[command]);
}
/****************************************************************************/
void setCurCommand(eCommand newCommand, int quantity, bool repeat)
{
#ifdef DEBUG
    if(newCommand<0 || newCommand>=commandCount)
    {
        bug("setCurCommand: invalid command");
        return;
    }
#endif
    //First determine if the command is available from the command panel.
    //If the command is unavailable, we assume that the command was generated
    //by a shortcut key.  Thus, we don't report an error.
    if(!commandPanel->doesCommandExist(newCommand))
        return;

    if(commandPanel && curCommand!=COMMAND_NONE)
        commandPanel->deactivateCommand(curCommand);

    //Destroy the old apparition.
    destroyApparition();

    curCommand=newCommand;
    paramType=commandTable[curCommand].paramType;
    firstTarget=true;

    if(curCommand!=COMMAND_NONE)
    {
        if(paramType==PARAM_NONE)
        {
            if(!isShiftPressed() && !commandTable[curCommand].immediate)
                commandSel(COMMAND_STOP);

            commandSel(curCommand);

            curCommand=COMMAND_NONE;
            //No need to set paramType here since it already equals PARAM_NONE.
        }
        else if(paramType==PARAM_PRODUCTION)
        {
            commandSel(curCommand, quantity, repeat);

            curCommand=COMMAND_NONE;
            paramType=PARAM_NONE;
        }
    }

    if(commandPanel && curCommand!=COMMAND_NONE)
        commandPanel->activateCommand(curCommand);

    //Create apparition beneath the mouse pointer showing where the new building
    //will go.
    if(gameWindow && gameWindow->isSelected() && paramType==PARAM_CONSTRUCTION)
        createApparition(curCommand,saveMouseX,saveMouseY);
}
/****************************************************************************/
void resetFirstTarget()
{
    firstTarget=false;

    //Destroy the old apparition.
    if(!isShiftPressed())
        destroyApparition();
}
/****************************************************************************/
void displayProgressBar(const tString& message)
{
    tWindow *w=windowLookup(PROGRESS_BAR_WINDOW);
    if(w)
    {
        destroyWindow(w);
        w=NULL;
    }
    w=mwNew tProgressBar(message);
    createWindow(w);
}
/****************************************************************************/
void updateProgressBar(float progress)
{
    tProgressBar *pb=(tProgressBar *)windowLookup(PROGRESS_BAR_WINDOW);
    if(pb==NULL)
    {
        bug("updateProgressBar: progress bar not found");
        return;
    }
    pb->setProgress(progress);
}
/****************************************************************************/
void closeProgressBar()
{
    tProgressBar *pb=(tProgressBar *)windowLookup(PROGRESS_BAR_WINDOW);
    if(pb==NULL)
    {
        bug("updateProgressBar: progress bar not found");
        return;
    }
    pb->close();
}
/****************************************************************************/
void interfaceStartup() throw(tException)
{
    log("Initializing OpenGL...");
    openGLStartup();

    log("Creating trignometry tables...");
    if(!createTrigTables())
        throw(tException("Failed to create trignometry tables."));

    log("Loading fonts...");
    if(!loadFonts("data/fonts/font.dat"))
        throw(tException("Failed to load 'data/fonts/font.dat'"));

    log("Starting engine...");
    engineStartup();
    log("Starting texture manager...");
    textureStartup();
    log("Loading animation types...");
    if(!loadAnimTypes(TEXTURE_PATH "anim.dat"))
        throw(tException("Failed to load '" TEXTURE_PATH "anim.dat'"));
    log("Loading panels...");
    loadPanels(TEXTURE_PATH "panels.dat");

    log("initializing OpenAL...");
    soundStartup();
    log("Starting sound manager...");
    if(!loadSoundTypes(SOUND_PATH "sound.dat"))
        throw(tException("Failed to load '" SOUND_PATH "sound.dat'"));

    log("Creating main menu...");
    displayWindow(MAIN_MENU);
    //menuCubeTexture=getImage("data/textures/menucube.png");

    FPS.enable();

    log("Interface startup complete.");
}
/****************************************************************************/
void interfaceRefresh()
{
    log("Reinitializing OpenGL...");
    openGLRefresh();
    log("Refreshing fonts...");
    refreshFonts();
    log("Refreshing windows...");
    refreshWindows();                   //refresh windows to reload display lists
    log("Refreshing mouse pointer...");
    refreshMousePointer();              //refresh mouse pointer

    log("Refreshing engine...");
    engineRefresh();
    log("Refreshing texture manager...");
    textureRefresh();
    log("Refreshing models...");
    refreshModelTypes();

    log("Interface refresh complete.");
}
/****************************************************************************/
void interfaceShutdown()
//The order of this function is EXTREMELY important
{
    log("Disconnecting...");
    disconnect();
    log("Shutting down engine...");
    engineShutdown();
    log("Destroying windows...");
    destroyWindows();
    if(menuCubeTexture)
    {
        releaseTexture(menuCubeTexture);
        menuCubeTexture=NULL;
    }

    log("Destroying mouse pointer...");
    destroyMousePointer();

    log("Destroying animation types...");
    destroyAnimTypes();
    log("Destroying panels...");
    destroyPanels();
    log("Shutting down texture manager...");
    textureShutdown();

    log("Destroying sound types...");
    destroySoundTypes();
    log("Shutting down OpenAL...");
    soundShutdown();

    log("Destroying fonts...");
    destroyFonts();

    log("Destroying trignometry tables...");
    destroyTrigTables();

    log("Shutting down OpenGL...");
    openGLShutdown();

    log("Interface shutdown complete.");
}
/****************************************************************************/
static float rotX=0, rotY=0, rotZ=0;
static float rotX0=0, rotY0=0, rotZ0=0;
static float rotXRate=0, rotYRate=0, rotZRate=0;
static float rotXAccel=0, rotYAccel=0, rotZAccel=0;
static float posX=0, posY=0, posZ=0;
static float posX0=0, posY0=0, posZ0=0;
static float posXRate=0, posYRate=0, posZRate=0;
static float posXAccel=0, posYAccel=0, posZAccel=0;
static float lastUpdate=-10;

#define ROT_RANGE 3
#define ROT_MAX_ACCEL 0.3
#define POS_RANGE 0.1
#define POS_MAX_ACCEL 0.03

void interfacePoll()
{
    START_TIMER(TIMER_ENGINE_TICK)
    engineTick();
    STOP_TIMER(TIMER_ENGINE_TICK)
    START_TIMER(TIMER_POLL_WINDOWS)
    pollWindows();
    STOP_TIMER(TIMER_POLL_WINDOWS)

    /*float dt=currentTime-lastUpdate;

    rotX = rotX0 + dt*rotXRate + 0.5*dt*dt*rotXAccel;
    rotY = rotY0 + dt*rotYRate + 0.5*dt*dt*rotYAccel;
    rotZ = rotZ0 + dt*rotZRate + 0.5*dt*dt*rotZAccel;
    posX = posX0 + dt*posXRate + 0.5*dt*dt*posXAccel;
    posY = posY0 + dt*posYRate + 0.5*dt*dt*posYAccel;
    posZ = posZ0 + dt*posZRate + 0.5*dt*dt*posZAccel;

    if(dt>3)
    {
        rotX0=rotX; rotY0=rotY; rotZ0=rotZ;
        rotXRate+=dt*rotXAccel;
        rotYRate+=dt*rotYAccel;
        rotZRate+=dt*rotZAccel;
        posX0=posX; posY0=posY; posZ0=posZ;
        posXRate+=dt*posXAccel;
        posYRate+=dt*posYAccel;
        posZRate+=dt*posZAccel;

        rotXAccel=((rand()%2049)/1024.0-1-rotXRate/ROT_RANGE)*ROT_MAX_ACCEL;
        rotYAccel=((rand()%2049)/1024.0-1-rotYRate/ROT_RANGE)*ROT_MAX_ACCEL;
        rotZAccel=((rand()%2049)/1024.0-1-rotZRate/ROT_RANGE)*ROT_MAX_ACCEL;
        posXAccel=((rand()%2049)/1024.0-1-posXRate/POS_RANGE)*POS_MAX_ACCEL;
        posYAccel=((rand()%2049)/1024.0-1-posYRate/POS_RANGE)*POS_MAX_ACCEL;
        posZAccel=((rand()%2049)/1024.0-1-posZRate/POS_RANGE)*POS_MAX_ACCEL;

        if(posX<-POS_RANGE) posXAccel+=POS_MAX_ACCEL/3;
        if(posX> POS_RANGE) posXAccel-=POS_MAX_ACCEL/3;
        if(posY<-POS_RANGE) posYAccel+=POS_MAX_ACCEL/3;
        if(posY> POS_RANGE) posYAccel-=POS_MAX_ACCEL/3;
        if(posZ<-POS_RANGE) posZAccel+=POS_MAX_ACCEL/3;
        if(posZ> POS_RANGE) posZAccel-=POS_MAX_ACCEL/3;

        lastUpdate=currentTime;
    }
    */
    float sw=getScreenWidth();
    float sh=getScreenHeight();
#ifdef DEBUG
    if(sw==0 || sh==0)
    {
        bug("interfacePoll: invalid screen dimensions");
        return;
    }
#endif

    float ar=sw/sh;
    float inverseOrtho[16]={sw/2,0,0,0, 0,sh/2,0,0, 0,0,1,0, sw/2,sh/2,0,1};
    float m[16];
    glLoadIdentity();

    glMultMatrixf(inverseOrtho);
    gluPerspective(45,ar,1,10);
    glTranslatef(posX,posY,posY-5);
    glScalef(0.005,0.005,0.005);
    glRotatef(rotX*180/M_PI,1,0,0);
    glRotatef(rotY*180/M_PI,0,1,0);
    glRotatef(rotZ*180/M_PI,0,0,1);

    glGetFloatv(GL_MODELVIEW_MATRIX,menuCubeMatrix);

    glTranslatef(-200,-200,200);
    glGetFloatv(GL_MODELVIEW_MATRIX,m);

    t3DWindow *w=(t3DWindow *)windowLookup(MAIN_MENU);
    if(w)
        w->setMatrix(m);

    START_TIMER(TIMER_INVALIDATE_MOUSE_POSITION)
    invalidateMousePosition();
    STOP_TIMER(TIMER_INVALIDATE_MOUSE_POSITION)
}
/****************************************************************************/
void interfaceDraw()
{
    int i;
    glLoadIdentity();
    if(gameRunning)
    {
        START_GRAPHICS_TIMER(TIMER_DRAW_MAP)
        drawMap();
        STOP_GRAPHICS_TIMER(TIMER_DRAW_MAP)
    }
    else
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen

    //We must load the menu cube matrix into the projection matrix so that
    //OpenGL correctly lights the cube.
    /*glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glMultMatrixf(menuCubeMatrix);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_BLEND);
    if(menuCubeTexture)
    {
        bindTexture(menuCubeTexture);
        glEnable(GL_TEXTURE_2D);
    }

    glLoadIdentity();
    glColor3f(1,1,1);
    mglPrism(199.9,199.9,199.9);

    glDisable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    glDisable(GL_COLOR_MATERIAL);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    */

    START_GRAPHICS_TIMER(TIMER_DRAW_WINDOWS)
    drawWindows();
    STOP_GRAPHICS_TIMER(TIMER_DRAW_WINDOWS)

#ifdef PROFILE
    char buf[200];

    glColor3f(1,1,1);

    FPS.start();
    snprintf(buf, sizeof(buf), "%03d FPS  ",(int)FPS.getFrequency());
    drawString(buf,20,getScreenHeight()-15,smallFont);

    if(timersVisible)
    {
        int x1=getScreenWidth()/3;
        int x2=2*getScreenWidth()/3;
        int y;
        int count=MAX_TIMERS/3;
        for(i=0;i<count;i++)
        {
            y=30+fontHeight(smallFont)*(count-i);
            snprintf(buf, sizeof(buf), "%s", CHARPTR(timers[i].getName()));
            drawString(buf,10,y,smallFont);
            snprintf(buf, sizeof(buf), "%6dus", (int)timers[i].getDelayMicroseconds());
            drawString(buf,x1-10-stringWidth(buf,smallFont),y,smallFont);
        }
        for(i=count;i<2*count;i++)
        {
            y=30+fontHeight(smallFont)*(count*2-i);
            snprintf(buf, sizeof(buf), "%s", CHARPTR(timers[i].getName()));
            drawString(buf,x1+10,y,smallFont);
            snprintf(buf, sizeof(buf), "%6dus", (int)timers[i].getDelayMicroseconds());
            drawString(buf,x2-10-stringWidth(buf,smallFont),y,smallFont);
        }
        for(i=2*count;i<MAX_TIMERS;i++)
        {
            y=30+fontHeight(smallFont)*(count*3-i);
            snprintf(buf, sizeof(buf), "%s", CHARPTR(timers[i].getName()));
            drawString(buf,x2+10,y,smallFont);
            snprintf(buf, sizeof(buf), "%6dus", (int)timers[i].getDelayMicroseconds());
            drawString(buf,getScreenWidth()-10-stringWidth(buf,smallFont),y,smallFont);
        }
    }

    glColor3f(0,1,0);
    glBegin(GL_LINES);
        glVertex2i(getScreenWidth()/2, getScreenHeight()/2-5);
        glVertex2i(getScreenWidth()/2, getScreenHeight()/2+5);
        glVertex2i(getScreenWidth()/2-5, getScreenHeight()/2);
        glVertex2i(getScreenWidth()/2+5, getScreenHeight()/2);
    glEnd();
#endif

    START_GRAPHICS_TIMER(TIMER_DRAW_MOUSE_POINTER)
    drawMousePointer();
    STOP_GRAPHICS_TIMER(TIMER_DRAW_MOUSE_POINTER)
}
/****************************************************************************/
void restartGame()
{
    endGame();
    //We assume that all of the game parameters are still valid.
    startGame();
}
/****************************************************************************/
// EVENTS
/****************************************************************************/
void onAddUser(tUser *user, bool initial)
{
    if(!initial && user->descriptor)
        showMessage("*** "+user->name+" ("+user->descriptor->peerName+") joined ***",
            TEXT_MESSAGE_STATUS);
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onAddUser(user,initial);
}
/****************************************************************************/
void onConnect(tDescriptor *desc)
{
    char buffer[80];
    unsigned char values[4];
    longToIP(desc->IPAddress, values);
    snprintf(buffer,sizeof(buffer), "*** Connect to %d.%d.%d.%d on port %d ***",
        values[0], values[1], values[2], values[3], desc->port);
    closeWindows();
    displayWindow(SETUP_GAME_MENU);
    showMessage(buffer, TEXT_MESSAGE_STATUS);
}
/****************************************************************************/
void onReconnect()
{
    char buffer[80];
    unsigned char values[4];
    longToIP(networkIP, values);
    snprintf(buffer,sizeof(buffer), "*** Reconnected from IP address %d.%d.%d.%d on port %d ***", values[0], values[1], values[2], values[3], networkPort);
    showMessage(buffer, TEXT_MESSAGE_STATUS);
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onReconnect();
}
/****************************************************************************/
void onDropUser(tUser *user)
{
    if(user == localUser)
    {
        messageBox("You were dropped from the game.", "Whoops!", CHOICES_OK, abortSession);
    }
    else
    {
        showMessage("*** "+user->name+" left ***", TEXT_MESSAGE_STATUS);
        tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
        if(setupMenu)
            setupMenu->onDropUser(user);
        tGameWindow *gameWindow=(tGameWindow *)windowLookup(GAME_WINDOW);
        if(gameWindow)
            gameWindow->onDropUser(user);
    }
}
/****************************************************************************/
void onHostLeft()
{
    messageBox("This game will end since the host left.", "Host Left", CHOICES_OK, abortSession);
}
/****************************************************************************/
void onHostGame()
{
    char buffer[80];
    unsigned char values[4];
    longToIP(networkIP, values);
    snprintf(buffer,sizeof(buffer), "*** Hosting game at %d.%d.%d.%d on port %d ***",
        values[0], values[1], values[2], values[3], networkPort);
    closeWindows();
    displayWindow(SETUP_GAME_MENU);
    showMessage(buffer, TEXT_MESSAGE_STATUS);
}
/****************************************************************************/
void onNetworkError()
{
    tWindow *msgBox=windowLookup(CONNECT_MESSAGE);
    if(msgBox)
    {
        destroyWindow(msgBox);
        messageBox("Failed to connect", "Network Error", CHOICES_OK);
    }
    else
        messageBox("An unexpected network error has occurred.", "Network Error", CHOICES_OK, abortSession);
}
/****************************************************************************/
void onDisconnect()
{
    //Take no action here since the main menu should already be visible
}
/****************************************************************************/
void onReceiveText(const char *text, eTextMessage type)
{
    showMessage(text, type);
}
/****************************************************************************/
void onConnectionRefused(eRefuse code)
{
    tWindow *msgBox=windowLookup(CONNECT_MESSAGE);
    if(msgBox)
        destroyWindow(msgBox);
    switch(code)
    {
        case REF_HOST_FULL:
            messageBox("The host is full.",
                "Connection Refused", CHOICES_OK);
            break;
        case REF_GAME_STARTED:
            messageBox("The game has already started.",
                "Connection Refused", CHOICES_OK);
            break;
        case REF_NAME_USED:
            messageBox("Someone is already using your name.",
                "Connection Refused", CHOICES_OK);
            break;
        case REF_DROPPED:
            messageBox("You were dropped from the game.",
                "Connection Refused", CHOICES_OK);
            break;
        case REF_OLD_VERSION:
            messageBox("The host is running a different version than you.",
                "Connection Refused", CHOICES_OK);
            break;
        default:
            messageBox("The host refused your connection for an unknown reason.",
                "Connection Refused", CHOICES_OK);
            break;
    }
}
/****************************************************************************/
void onUpdateLag(tUser *user)
{
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onUpdateLag(user);
}
/****************************************************************************/
void onStartGame()
{
    showMessage("*** Game started ***", TEXT_MESSAGE_STATUS);
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onStartGame();
    if(gameRunning)
    {
        curCommand=COMMAND_NONE;
        paramType=PARAM_NONE;

        memset(enableCounter,'\0',sizeof(enableCounter));
        memset(highlightCounter,'\0',sizeof(highlightCounter));
        memset(quantityCounter,'\0',sizeof(quantityCounter));
        memset(repeatCounter,'\0',sizeof(repeatCounter));

        closeWindows();
        gameWindow=(tGameWindow *)displayWindow(GAME_WINDOW);
        minimap=(tMinimap *)displayWindow(MINIMAP_WINDOW);
        commandPanel=(tCommandPanel *)displayWindow(COMMAND_PANEL);
        if(commandPanel)
            commandPanel->reset();
        chatWindow=(tChatWindow *)displayWindow(CHAT_WINDOW);
    }
}
/****************************************************************************/
void onEndGame()
{
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onEndGame();
    if(gameWindow&&gameWindow->isVisible())
    {
        closeWindows();
        displayWindow(SETUP_GAME_MENU);
    }
    showMessage("*** Game ended ***", TEXT_MESSAGE_STATUS);
}
/****************************************************************************/
void onDownloadFile(tDescriptor *desc)
{
    showMessage("*** Downloading file "+desc->fileIn.name+" ***", TEXT_MESSAGE_STATUS);
}
/****************************************************************************/
void onDownloadComplete(tDescriptor *desc)
{
    showMessage("*** Finished downloading file "+desc->fileIn.name+" ***", TEXT_MESSAGE_STATUS);
}
/****************************************************************************/
void onUploadFile(tDescriptor *desc)
{
    showMessage("*** Uploading file "+desc->fileOut.name+" ***", TEXT_MESSAGE_STATUS);
}
/****************************************************************************/
void onUploadComplete(tDescriptor *desc)
{
    showMessage("*** Finished uploading file "+desc->fileOut.name+" ***", TEXT_MESSAGE_STATUS);
}
/****************************************************************************/
void onAbortDownload(tDescriptor *desc)
{
    showMessage("*** Download aborted ***", TEXT_MESSAGE_STATUS);
}
/****************************************************************************/
void onAbortUpload(tDescriptor *desc)
{
    showMessage("*** Upload aborted ***", TEXT_MESSAGE_STATUS);
}
/****************************************************************************/
void onSetTiming()
{
    char buffer[200];
    if(networkGame)
        snprintf(buffer,sizeof(buffer), "*** Latency set to %ldms, Network interval set to %ldms, Game interval set to %ldms, "
            "Game speed set to %3.1fx ***", latency, netInterval, gameInterval, gameSpeed);
    else
        snprintf(buffer,sizeof(buffer), "*** Game interval set to %ldms, Game speed set to %3.1fx ***", gameInterval, gameSpeed);
    showMessage(buffer, TEXT_MESSAGE_STATUS);
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onSetTiming();
}
/****************************************************************************/
void onSetGameSpeed()
{
    char buffer[20];
    snprintf(buffer,sizeof(buffer), "Speed: %3.1fx", gameSpeed);
    showMessage(buffer, TEXT_MESSAGE_STATUS);
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onSetTiming();
}
/****************************************************************************/
void onFreezeGame()
{
}
/****************************************************************************/
void onUnfreezeGame()
{
}
/****************************************************************************/
void onPauseGame(int player)
{
    showMessage("*** "+getPlayerCaption(player,/*useUsername=*/true)+" paused the game ***", TEXT_MESSAGE_STATUS);
}
/****************************************************************************/
void onResumeGame(int player)
{
    showMessage("*** "+getPlayerCaption(player,/*useUsername=*/true)+" resumed the game ***", TEXT_MESSAGE_STATUS);
}
/****************************************************************************/
void onSetAlliance(int player)
{
    eAlliance a=getReverseAlliance(player);
    showMessage("*** "+getPlayerCaption(player,/*useUsername=*/true)+" set alliance with you to "+allianceNames[a]+" ***",
        TEXT_MESSAGE_STATUS);
}
/****************************************************************************/
void onChooseFiles()
{
    char buffer[1000];
    snprintf(buffer,sizeof(buffer), "*** Game host loaded map '%s' and world '%s' ***", CHARPTR(mapFile.name), CHARPTR(worldFile.name));
    showMessage(buffer, TEXT_MESSAGE_STATUS);
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onChooseFiles();
}
/****************************************************************************/
void onResetPlayers()
{
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onResetPlayers();
}
/****************************************************************************/
void onSetPlayer(int index, ePlayer type, tUser *user)
{
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onSetPlayer(index,type,user);
}
/****************************************************************************/
void onSinglePlayer()
{
    closeWindows();
    displayWindow(SETUP_GAME_MENU);
    showMessage("*** Starting a single player game ***", TEXT_MESSAGE_STATUS);
}
/****************************************************************************/
void onDiscrepancyDetected()
{
    showMessage("DISCREPANCY DETECTED", TEXT_MESSAGE_STATUS);
}
/****************************************************************************/
void onParseError(const tString& error)
{
    messageBox(error, "Error loading data", CHOICES_OK);
}
/****************************************************************************/
void onMapChanged()
{
    messageBox(tString("The size or time of '")+mapFile.name+"' has changed\nThe game has loaded the new map file.",
        "Notice", CHOICES_OK);
}
/****************************************************************************/
void onWorldChanged()
{
    messageBox(tString("The size or time of '")+worldFile.name+"' has changed\nThe game has loaded the new world file.",
        "Notice", CHOICES_OK);
}
/****************************************************************************/
void onHostMissing()
{
    messageBox("You need to assign the host to a player!","Error",CHOICES_OK);
}
/****************************************************************************/
void onGameWaiting()
{
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onGameWaiting();
}
/****************************************************************************/
void onAbortGame()
{
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onAbortGame();
}
/****************************************************************************/
void setPanRate(float _panRate)     { panRate=_panRate; }
void setSpinRate(float _spinRate)   { spinRate=_spinRate; }
void setZoomRate(float _zoomRate)   { zoomRate=_zoomRate; }
void setTiltRate(float _tiltRate)   { tiltRate=_tiltRate; }
void setMouseZoomRate(float _mouseZoomRate) { mouseZoomRate=_mouseZoomRate; }
/****************************************************************************/
void onLoadMap()
{
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onLoadMap();
}
/****************************************************************************/
void onLoadWorld()
{
    //Ignore this event since the world name should be set during the onChooseFiles event.
}
/****************************************************************************/
void onUnloadMap()
{
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onUnloadMap();
}
/****************************************************************************/
void onUnloadWorld()
{
    tSetupMenu *setupMenu=(tSetupMenu *)windowLookup(SETUP_GAME_MENU);
    if(setupMenu)
        setupMenu->onUnloadWorld();
}
/****************************************************************************/
// TEMPLATES
/****************************************************************************/
/*
tWindow *w1=new tStdDialogBox("Window 1",0,300,300,WINDOW_NORMAL,false,true,true,true,true,true,true,200,800,100,600);
w1->createControl(new tStdLabel(10,250,"Hello world!",0,0,0,0));
w1->createControl(new tStdButton(10,200,100,30,"Cancel",0,true,NULL));
w1->createControl(new tstdtoggle(150,200,"This is a toggle",0,0,0,0,true,false));
w1->createControl(new tStdRadio(100,100,"Red",0,0,0,0,true,true,0));
w1->createControl(new tStdRadio(150,100,"Blue",0,0,0,0,true,false,0));
w1->createControl(new tStdRadio(200,100,"Green",0,0,0,0,true,false,0));
tWindow *w2=new tStdDialogBox("Window 2",0,300,300,WINDOW_NORMAL,false,true,true,true,true,true,true,200,800,100,600);
w2->createControl(new tEdit(10,250,200,"This is a test",1,false));
w2->createControl(new tStdTextBox(10,30,200,200,"Hello\nWorld",-1,true,true,0,false,false));
tWindow *w3=new tStdDialogBox("Window 3",0,300,300,WINDOW_NORMAL,false,true,true,true,true,true,true,200,800,100,600);
w3->createControl(new tStdListBox(10,150,200,100,true,"Red\nBlue\nGreen\nYellow",-1,true,true,0));
w3->createControl(new tStdComboBox(10,50,200,true,"Red\nBlue\nGreen\nYellow",-1,100,true,true,0));
createWindow(w2);
createWindow(w3);
*/

/*
createControl(new tStdComboBox(200,200,200,true,stdchoices,-1,100,true,false,0));
createControl(new tStdListBox(400,200,200,175,true,stdchoices,0,true,false,0));
createControl(new tStdButton(10,screenHeight-50,125,25,0,"Listen",true));
createControl(new tStdTextBox(25,screenHeight-400,300,175,"Hello World!\nThis is a test",-1,true,true,0,false));
createControl(new tstdtoggle(25,100,20,20,true,true));
createControl(new tStdRadio(100,50,25,25,true,false,1));
createControl(new tStdRadio(130,50,25,25,true,false,1));
createControl(new tStdRadio(160,50,25,25,true,false,1));
createControl(new tstdscrollbar(400,400,200,true,false,0,100,10));
createControl(new tStdListBox(350,200,200,175,true,10,"Red\nBlue\nYellow\nGreen\nOrange\nPurple\nCyan\nBlack\nWhite\nGrey",-1,true,false,0));
createControl(new tEdit(25,425,400,stdstring,0,false));
*/
/****************************************************************************/
// OLD CODE
/****************************************************************************/
/*
glEnable(GL_LIGHTING);
glEnable(GL_DEPTH_TEST);
glEnable(GL_COLOR_MATERIAL);
glMatrixMode(GL_PROJECTION);
glLoadIdentity();
glOrtho(-240,240,-180,180,-100,100);
glScalef(10,10,10);
//gluPerspective(45, (float) screenWidth / screenHeight, 1, 20);
glMatrixMode(GL_MODELVIEW);
glLoadIdentity();
glRotatef(50,1,0,0);
glRotatef(-45,0,1,0);

int x, y;
float v1[3], v2[3], v3[3], v4[3], normal[3];
float dist1, dist2, dist3, dist4;
for(x = 0; x < map_size_x; x++)
    for(y = 0; y < map_size_y; y++)
    {
        v1[0] = x;      v1[2] = y;      v1[1] = (float) alt_map[x][y]/16;
        dist1 = sqrt( (x-64) * (x-64) + (y-64) * (y-64) );
        v2[0] = x+1;    v2[2] = y;      v2[1] = (float) alt_map[x+1][y]/16;
        dist2 = sqrt( (x+1-64) * (x+1-64) + (y-64) * (y-64) );
        v3[0] = x+1;    v3[2] = y+1;    v3[1] = (float) alt_map[x+1][y+1]/16;
        dist3 = sqrt( (x+1-64) * (x+1-64) + (y+1-64) * (y+1-64) );
        v4[0] = x;      v4[2] = y+1;    v4[1] = (float) alt_map[x][y+1]/16;
        dist4 = sqrt( (x-64) * (x-64) + (y+1-64) * (y+1-64) );
        dist1 =
        glBegin(GL_TRIANGLES);
            calcnormal(v1,v3,v2,normal);
            glNormal3fv(normal);
            glColor3f(dist1,dist1,dist1);
            glVertex3fv(v1);
            glColor3f(dist3,dist3,dist3);
            glVertex3fv(v3);
            glColor3f(dist2,dist2,dist2);
            glVertex3fv(v2);

            calcnormal(v1,v4,v3,normal);
            glNormal3fv(normal);
            glColor3f(dist1,dist1,dist1);
            glVertex3fv(v1);
            glColor3f(dist4,dist4,dist4);
            glVertex3fv(v4);
            glColor3f(dist3,dist3,dist3);
            glVertex3fv(v3);
        glEnd();
    }
*/

/*
lists=glGenLists(96);
int height=48,weight=FW_REGULAR;
bool italic=false;
font = CreateFont(height, 0, 0, 0, weight, italic, FALSE, FALSE,
                   ANSI_CHARSET, OUT_TT_PRECIS,
                   CLIP_DEFAULT_PRECIS, DRAFT_QUALITY,
                   DEFAULT_PITCH, "Times New Roman");
SelectObject(hDC,font);
wglUseFontBitmaps(hDC, 32, 96, lists);
glListBase(lists-32);

DeleteObject(font);
glDeleteLists(lists,96);

void drawbitmap()
    {
    glClear(GL_COLOR_BUFFER_BIT);
    AUX_RGBImageRec *image = auxDIBImageLoad("c:\\test.bmp");
    if(image)
        {
        glRasterPos2i(0,0);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glDrawPixels(image->sizeX,image->sizeY,GL_RGB,GL_UNSIGNED_BYTE,image->data);
        }
    }

int DrawGLScene(GLvoid)									// Here's Where We Do All The Drawing
{
    char *message="Hello world!";
    drawbitmap();
    glRasterPos2i(0,100);
    glCallLists(strlen(message),GL_UNSIGNED_BYTE,message);
	//glLoadIdentity();							        // Reset The View

	return TRUE;										// Keep Going
}
*/

