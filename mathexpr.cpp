/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* MATHEXPR.CPP: Very basic C expression parser                              *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop

/****************************************************************************/

#ifndef CACHE_HEADERS
	#include <memory.h>
#endif

#include "mathexpr.h"

static tModel *modelContext=NULL;
static tModelType *modelTypeContext=NULL;
static char *stringPointer=NULL;
static tString currentToken;

static void getToken() throw(tString);
static void matchToken(const char *s) throw(tString);

static pExpression evaluateNegate(pExpression exp);
static pExpression evaluateNot(pExpression exp);
static pExpression evaluateExponent(pExpression exp1, pExpression exp2);
static pExpression evaluateMultiply(pExpression exp1, pExpression exp2);
static pExpression evaluateDivide(pExpression exp1, pExpression exp2);
static pExpression evaluateModulus(pExpression exp1, pExpression exp2);
static pExpression evaluateAdd(pExpression exp1, pExpression exp2);
static pExpression evaluateSubtract(pExpression exp1, pExpression exp2);
static pExpression evaluateLessThan(pExpression exp1, pExpression exp2);
static pExpression evaluateLessThanOrEqual(pExpression exp1, pExpression exp2);
static pExpression evaluateGreaterThan(pExpression exp1, pExpression exp2);
static pExpression evaluateGreaterThanOrEqual(pExpression exp1, pExpression exp2);
static pExpression evaluateEqual(pExpression exp1, pExpression exp2);
static pExpression evaluateNotEqual(pExpression exp1, pExpression exp2);
static pExpression evaluateAnd(pExpression exp1, pExpression exp2);
static pExpression evaluateOr(pExpression exp1, pExpression exp2);
static pExpression evaluateIfThenElse(pExpression exp1, pExpression exp2, pExpression exp3);
static pExpression evaluateSquareRoot(pExpression exp);
static pExpression evaluateSine(pExpression exp);
static pExpression evaluateCosine(pExpression exp);

static pExpression parseLevel1() throw(tString);
static pExpression parseLevel2() throw(tString);
static pExpression parseLevel3() throw(tString);
static pExpression parseLevel4() throw(tString);
static pExpression parseLevel5() throw(tString);
static pExpression parseLevel6() throw(tString);
static pExpression parseLevel7() throw(tString);
static pExpression parseLevel8() throw(tString);
static pExpression parseLevel9() throw(tString);
static pExpression parseLevel10() throw(tString);

pExpression evaluateNegate(pExpression exp)
{
	if(exp->isConstant())
		return pExpression(mwNew tConstant(tNegate::evaluate(exp->evaluate())));
    return pExpression(mwNew tNegate(exp));
}
pExpression evaluateNot(pExpression exp)
{
	if(exp->isConstant())
		return pExpression(mwNew tConstant(tNot::evaluate(exp->evaluate())));
    return pExpression(mwNew tNot(exp));
}
pExpression evaluateExponent(pExpression exp1, pExpression exp2)
{
	if(exp1->isConstant() && exp2->isConstant())
		return pExpression(mwNew tConstant(tExponent::evaluate(exp1->evaluate(),exp2->evaluate())));
	return pExpression(mwNew tExponent(exp1,exp2));
}
pExpression evaluateMultiply(pExpression exp1, pExpression exp2)
{
	if(exp1->isConstant() && exp2->isConstant())
		return pExpression(mwNew tConstant(tMultiply::evaluate(exp1->evaluate(),exp2->evaluate())));
	return pExpression(mwNew tMultiply(exp1,exp2));
}
pExpression evaluateDivide(pExpression exp1, pExpression exp2)
{
	if(exp1->isConstant() && exp2->isConstant())
		return pExpression(mwNew tConstant(tDivide::evaluate(exp1->evaluate(),exp2->evaluate())));
	return pExpression(mwNew tDivide(exp1,exp2));
}
pExpression evaluateModulus(pExpression exp1, pExpression exp2)
{
	if(exp1->isConstant() && exp2->isConstant())
		return pExpression(mwNew tConstant(tModulus::evaluate(exp1->evaluate(),exp2->evaluate())));
	return pExpression(mwNew tModulus(exp1,exp2));
}
pExpression evaluateAdd(pExpression exp1, pExpression exp2)
{
	if(exp1->isConstant() && exp2->isConstant())
		return pExpression(mwNew tConstant(tAdd::evaluate(exp1->evaluate(),exp2->evaluate())));
	return pExpression(mwNew tAdd(exp1,exp2));
}
pExpression evaluateSubtract(pExpression exp1, pExpression exp2)
{
	if(exp1->isConstant() && exp2->isConstant())
		return pExpression(mwNew tConstant(tSubtract::evaluate(exp1->evaluate(),exp2->evaluate())));
	return pExpression(mwNew tSubtract(exp1,exp2));
}
pExpression evaluateLessThan(pExpression exp1, pExpression exp2)
{
	if(exp1->isConstant() && exp2->isConstant())
		return pExpression(mwNew tConstant(tLessThan::evaluate(exp1->evaluate(),exp2->evaluate())));
	return pExpression(mwNew tLessThan(exp1,exp2));
}
pExpression evaluateLessThanOrEqual(pExpression exp1, pExpression exp2)
{
	if(exp1->isConstant() && exp2->isConstant())
		return pExpression(mwNew tConstant(tLessThanOrEqual::evaluate(exp1->evaluate(),exp2->evaluate())));
	return pExpression(mwNew tLessThanOrEqual(exp1,exp2));
}
pExpression evaluateGreaterThan(pExpression exp1, pExpression exp2)
{
	if(exp1->isConstant() && exp2->isConstant())
		return pExpression(mwNew tConstant(tGreaterThan::evaluate(exp1->evaluate(),exp2->evaluate())));
	return pExpression(mwNew tGreaterThan(exp1,exp2));
}
pExpression evaluateGreaterThanOrEqual(pExpression exp1, pExpression exp2)
{
	if(exp1->isConstant() && exp2->isConstant())
		return pExpression(mwNew tConstant(tGreaterThanOrEqual::evaluate(exp1->evaluate(),exp2->evaluate())));
	return pExpression(mwNew tGreaterThanOrEqual(exp1,exp2));
}
pExpression evaluateEqual(pExpression exp1, pExpression exp2)
{
	if(exp1->isConstant() && exp2->isConstant())
		return pExpression(mwNew tConstant(tEqual::evaluate(exp1->evaluate(),exp2->evaluate())));
	return pExpression(mwNew tEqual(exp1,exp2));
}
pExpression evaluateNotEqual(pExpression exp1, pExpression exp2)
{
	if(exp1->isConstant() && exp2->isConstant())
		return pExpression(mwNew tConstant(tNotEqual::evaluate(exp1->evaluate(),exp2->evaluate())));
	return pExpression(mwNew tNotEqual(exp1,exp2));
}
pExpression evaluateAnd(pExpression exp1, pExpression exp2)
{
	if(exp1->isConstant() && exp2->isConstant())
		return pExpression(mwNew tConstant(tAnd::evaluate(exp1->evaluate(),exp2->evaluate())));
	return pExpression(mwNew tAnd(exp1,exp2));
}
pExpression evaluateOr(pExpression exp1, pExpression exp2)
{
	if(exp1->isConstant() && exp2->isConstant())
		return pExpression(mwNew tConstant(tOr::evaluate(exp1->evaluate(),exp2->evaluate())));
	return pExpression(mwNew tOr(exp1,exp2));
}
pExpression evaluateIfThenElse(pExpression exp1, pExpression exp2, pExpression exp3)
{
	if(exp1->isConstant() && exp2->isConstant() && exp3->isConstant())
		return pExpression(mwNew tConstant(
			tIfThenElse::evaluate(exp1->evaluate(),exp2->evaluate(),exp3->evaluate())));
	return pExpression(mwNew tIfThenElse(exp1,exp2,exp3));
}
pExpression evaluateSquareRoot(pExpression exp)
{
	if(exp->isConstant())
		return pExpression(mwNew tConstant(tSquareRoot::evaluate(exp->evaluate())));
	return pExpression(mwNew tSquareRoot(exp));
}
pExpression evaluateSine(pExpression exp)
{
	if(exp->isConstant())
		return pExpression(mwNew tConstant(tSine::evaluate(exp->evaluate())));
	return pExpression(mwNew tSine(exp));
}
pExpression evaluateCosine(pExpression exp)
{
	if(exp->isConstant())
		return pExpression(mwNew tConstant(tCosine::evaluate(exp->evaluate())));
	return pExpression(mwNew tCosine(exp));
}

float tValue::evaluate()
{
    return argumentType.evaluate(modelContext);
}
tValue::tValue(eVariableAttribute attribute, tVariableType *vt) : argumentType(0,BOUNDARY_NONE)
{
	argumentType.setVariableType(attribute,vt);
}
tValue::tValue(eUnitVariable uv) : argumentType(0,BOUNDARY_NONE)
{
	argumentType.setUnitVariable(uv,modelTypeContext);
}
tValue::tValue(tFunctionType *ft) : argumentType(0,BOUNDARY_NONE)
{
	argumentType.setFunctionType(ft);
}

void getToken() throw(tString)
{
    bool invalid=false;

    while(*stringPointer==' '||*stringPointer=='\n'||*stringPointer=='\r'||*stringPointer=='\t')
        stringPointer++;

	char *ptr=stringPointer;

	if(*ptr=='(' || *ptr==')' || *ptr=='-' || *ptr=='+' ||
	   *ptr=='^' || *ptr=='*' || *ptr=='/' || *ptr=='%' ||
	   *ptr=='?' || *ptr==':')
		ptr++;
	else if(*ptr=='<')
	{
		ptr++;
		if(*ptr=='=')
			ptr++;
	}
	else if(*ptr=='>')
	{
		ptr++;
		if(*ptr=='=')
			ptr++;
	}
	else if(*ptr=='=')
	{
		ptr++;
		if(*ptr=='=')
			ptr++;
		else
			invalid=true;
	}
	else if(*ptr=='!')
	{
		ptr++;
		if(*ptr=='=')
			ptr++;
	}
	else if(*ptr=='&')
	{
		ptr++;
		if(*ptr=='&')
			ptr++;
		else
			invalid=true;
	}
	else if(*ptr=='|')
	{
		ptr++;
		if(*ptr=='|')
			ptr++;
		else
			invalid=true;
	}
	else if(isalnum(*ptr) || *ptr=='.' || *ptr=='_')
		while(isalnum(*ptr) || *ptr=='.' || *ptr=='_')
			ptr++;
	else if(*ptr!='\0')
		invalid=true;

	if(invalid)
	{
		if(*ptr!='\0')
			ptr++;
		*ptr='\0';
		throw tString("Invalid token '")+stringPointer+"'";
	}
	char ch=*ptr;
	*ptr='\0';
	currentToken=stringPointer;
	*ptr=ch;
	stringPointer=ptr;
}

void matchToken(const char *s) throw(tString)
{
	if(currentToken!=s)
		throw tString("Expected '")+s+"'";
	getToken();
}
pExpression parseLevel1() throw(tString)
{
	pExpression exp1=parseLevel2();

    if(currentToken=="?")
	{
		getToken();
		pExpression exp2=parseLevel1();
		matchToken(":");
		pExpression exp3=parseLevel1();
        pExpression temp=evaluateIfThenElse(exp1,exp2,exp3);
        exp1=temp;
	}
	return exp1;
}
pExpression parseLevel2() throw(tString)
{
	pExpression exp1=parseLevel3();

	while(currentToken=="||")
	{
		getToken();
		pExpression exp2=parseLevel3();
		pExpression temp=evaluateOr(exp1,exp2);
        exp1=temp;
	}
	return exp1;
}
pExpression parseLevel3() throw(tString)
{
	pExpression exp1=parseLevel4();

	while(currentToken=="&&")
	{
		getToken();
		pExpression exp2=parseLevel4();
		pExpression temp=evaluateAnd(exp1,exp2);
        exp1=temp;
	}
	return exp1;
}
pExpression parseLevel4() throw(tString)
{
	pExpression exp1=parseLevel5();
    tString op;

	while(currentToken=="==" || currentToken=="!=")
	{
        op=currentToken;
		getToken();
		pExpression exp2=parseLevel5();
		if(op=="==")
        {
			pExpression temp=evaluateEqual(exp1,exp2);
            exp1=temp;
        }
		else
        {
			pExpression temp=evaluateNotEqual(exp1,exp2);
            exp1=temp;
        }

	}
	return exp1;
}
pExpression parseLevel5() throw(tString)
{
	pExpression exp1=parseLevel6();
    tString op;

	while(currentToken=="<" || currentToken=="<=" || currentToken==">" || currentToken==">=")
	{
        op=currentToken;
		getToken();
		pExpression exp2=parseLevel6();
		if(op=="<")
        {
			pExpression temp=evaluateLessThan(exp1,exp2);
            exp1=temp;
        }
		else if(op=="<=")
        {
			pExpression temp=evaluateLessThanOrEqual(exp1,exp2);
            exp1=temp;
        }
		else if(op==">")
        {
			pExpression temp=evaluateGreaterThan(exp1,exp2);
            exp1=temp;
        }
		else
        {
			pExpression temp=evaluateGreaterThanOrEqual(exp1,exp2);
            exp1=temp;
        }
	}
	return exp1;
}
pExpression parseLevel6() throw(tString)
{
	pExpression exp1=parseLevel7();

	while(currentToken=="+" || currentToken=="-")
	{
        tString op=currentToken;
		getToken();
		pExpression exp2=parseLevel7();
		if(op=="+")
        {
			pExpression temp=evaluateAdd(exp1,exp2);
            exp1=temp;
        }
		else
        {
			pExpression temp=evaluateSubtract(exp1,exp2);
            exp1=temp;
        }
	}
	return exp1;
}
pExpression parseLevel7() throw(tString)
{
	pExpression exp1=parseLevel8();

	while(currentToken=="*" || currentToken=="/" || currentToken=="%")
	{
        tString op=currentToken;
		getToken();
		pExpression exp2=parseLevel8();
		if(op=="*")
        {
			pExpression temp=evaluateMultiply(exp1,exp2);
            exp1=temp;
        }
		else if(op=="/")
        {
			pExpression temp=evaluateDivide(exp1,exp2);
            exp1=temp;
        }
		else
        {
			pExpression temp=evaluateModulus(exp1,exp2);
            exp1=temp;
        }
	}
	return exp1;
}
pExpression parseLevel8() throw(tString)
{
	pExpression exp1=parseLevel9();

    if(currentToken=="^")
	{
		getToken();
		pExpression exp2=parseLevel8();
		pExpression temp=evaluateExponent(exp1,exp2);
        exp1=temp;
	}
	return exp1;
}
pExpression parseLevel9() throw(tString)
{
	if(currentToken=="-")
	{
		getToken();
		return evaluateNegate(parseLevel9());
	}
	else if(currentToken=="!")
	{
		getToken();
		return evaluateNot(parseLevel9());
	}
	else if(currentToken=="+") //Plus has no effect
	{
		getToken();
		return parseLevel9();
	}
	else if(currentToken=="sqrt")
	{
		getToken();
		return evaluateSquareRoot(parseLevel9());
	}
	else if(currentToken=="sin")
	{
		getToken();
		return evaluateSine(parseLevel9());
	}
	else if(currentToken=="cos")
	{
		getToken();
		return evaluateCosine(parseLevel9());
	}

	pExpression temp=parseLevel10();
    return temp;
}
pExpression parseLevel10() throw(tString)
{
	if(currentToken=="(")
	{
		getToken();
		pExpression exp1=parseLevel1();
		matchToken(")");
		return exp1;
	}

    if(currentToken=="pi")
    {
        getToken();
		pExpression temp=pExpression(mwNew tConstant(M_PI));
        return temp;
    }
    else if(currentToken=="true")
    {
        getToken();
		pExpression temp=pExpression(mwNew tConstant(1));
        return temp;
    }
    else if(currentToken=="false")
    {
        getToken();
		pExpression temp=pExpression(mwNew tConstant(0));
        return temp;
    }
	else if(STRLEN(currentToken)>0 && (isalpha(currentToken[0]) || currentToken[0]=='_'))
	{
		tVariableType *vt=modelTypeContext->variableTypeLookup(currentToken,/*exact=*/true);
        eVariableAttribute attribute=VARIABLE_POSITION;
		if(vt)
        {
            getToken();
            if(currentToken==".")
            {
                getToken();
                attribute=variableAttributeLookup(currentToken,/*exact=*/false);
                getToken();
            }

		    return pExpression(mwNew tValue(attribute, vt));
        }
        tFunctionType *ft=modelTypeContext->functionTypeLookup(currentToken,/*exact=*/true);
        if(ft)
        {
            getToken();
	    	return pExpression(mwNew tValue(ft));
        }
        eUnitVariable uv=unitVariableLookup(currentToken,/*exact=*/false);
        if(uv!=UNITVAR_NONE)
        {
            getToken();
            return pExpression(mwNew tValue(uv));
        }
        throw tString("Unrecognized variable, function, or unit variable '")+currentToken+"'";
    }

    char *ptr;
    float f=strtod(CHARPTR(currentToken),&ptr);
    if(*ptr!='\0') //Error
        throw tString("Invalid token '")+currentToken+"'";
    getToken();
    pExpression temp=pExpression(mwNew tConstant(f));
    return temp;
}

tExpression *parseExpression(const char *s, tModelType *modelType) throw(tString)
{
#ifdef DEBUG
	if(MEMORY_VIOLATION(s))
		return NULL;
	if(MEMORY_VIOLATION(modelType))
		return NULL;
#endif
    char *stringStart=strdup(s);
    strToLower(stringStart);
	stringPointer=stringStart;
	getToken();
	modelTypeContext=modelType;

   	tExpression *exp=parseLevel1().release();

	modelTypeContext=NULL;
    free(stringStart);
    stringPointer=NULL;
    
	return exp;
}

float evaluateExpression(tExpression* exp, tModel *model)
{
#ifdef DEBUG
	if(MEMORY_VIOLATION(exp))
		return 0;
    //NOTE: model can be NULL since we pass this pointer to the argument's evaluate() function.
#endif
	modelContext=model;
	float f=exp->evaluate();
	modelContext=NULL;
	return f;
}
