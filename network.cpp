/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* NETWORK.CPP   Facilitates socket connections; synchronizes gameplay on    *
*               multiple computers.                                         *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#ifndef CACHE_HEADERS
    #include <time.h> //for time_t
    #ifdef LINUX
        #include <sys/stat.h> //for mkdir
        #include <utime.h> //for utime
    #else
        #ifdef BORLAND
            #include <dir.h> //for mkdir
            #include <utime.h> //for _utime
        #else
            #include <direct.h> //for mkdir
            #include <sys/utime.h> //for _utime
        #endif
    #endif
#endif

#include "network.h"
#include "message.h"
#include "game.h"
#ifndef BORLAND
    #include "snprintf.h"
#endif

#pragma package(smart_init)

/****************************************************************************
                             Global Variables
 ****************************************************************************/
bool     useTCPIP = false;       //Communicate with TCP/IP protocol if true; UDP protocol if false
tUser*   firstUser = NULL;       //First user in linked list
tUser*   localUser = NULL;       //Pointer to the local user in the linked list of users (above)
long     networkIP = 0;          //Local IP address
tString  peerName;               //Local name
int      networkPort = 0;        //Local port
bool     networkGame = false;    //Is the user playing a network game? (either as a host or a client)

bool     isGameHost = false;     //Is the engine the game host? (includes single-player session)
bool     isGameClient = false;   //Is the engine a game client?



/****************************************************************************
                              Static Variables
 ****************************************************************************/
static bool     listening = false;      //Is the engine listening?
static bool     networkActive = false;  //Has the network been initialized? (network games only)
static tSocket  server = INVALID_SOCKET;//Handle to the engine's primary socket.  This socket is bound to networkPort.
                                        //In TCP/IP, server *listen*s for new connections; in UDP the server socket
                                        //sends and receives all data packets.
static tTime     lagTimer;              //Used for timing ping messages

static tDescriptor* firstDescriptor = NULL; //Linked list of descriptors
static tDescriptor* gameHost = NULL;        //Which descriptor in the linked list is the game host?

//The following variables are for collecting statistics only
static long packetsSent = 0;            //Number of packets sent (TCP/IP or UDP)
static long packetsResent = 0;          //Number of packets resent (UDP only)
static long packetsRecv = 0;            //Number of packets received (TCP/IP or UDP)
static long packetsRerecv = 0;          //Number of packets re-received (UDP only)



/****************************************************************************
                               Static Functions
 ****************************************************************************/
//See definitions for a description of these functions:
static bool         newSocket(tSocket& socket);
static bool         engineListen(long IP, int port);
static bool         connectToPeer(long IP, int port, tDescriptor **d);
static bool         reconnectToPeer(tDescriptor *d);
static tDescriptor* addDescriptor(long IP, int port);
static void         deleteDescriptor(tDescriptor *desc);
static void         writeToDescriptor(tDescriptor *desc);

// TCPIP only:
static bool         readFromTCPIPDescriptor(tDescriptor *desc);

// UDP only:
static bool         readFromUDPDescriptor(tDescriptor *desc, const char *buffer, int length);
static void         writeToSocket(tDescriptor *desc, const char *buffer, int length, bool immediate);
static tPacket*     newPacket(long ID);
static void         deletePacket(tPacket *packet);
static void         resendPackets(tDescriptor *desc);

static void         readFromBuffer(tDescriptor* desc);
static bool         newConnection();
static void         closeDescriptor(tDescriptor *desc);
static void         connectionLost(tDescriptor *desc);
static int          countDescriptors();
static int          countUsers();
static int          nextID();
static void         connectionEstablished(tDescriptor *desc);
static tUser*       addUser(const tString& name, int ID, tDescriptor *d);
static void         deleteUser(tUser *user);
static tDescriptor* descriptorLookup(int ID);

//Message events:
static void onResendMsg         (tDescriptor *desc, tResendMsg *message) throw(int);
static void onAckPacketsMsg     (tDescriptor *desc, tAckPacketsMsg *message) throw(int);
static void onPacketResentMsg   (tDescriptor *desc, tDataMsg *message) throw(int);
static void onRefuseMsg         (tDescriptor *desc, tRefuseMsg *message) throw(int);
static void onUserDataMsg       (tDescriptor *desc, tUserDataMsg *message) throw(int);
static void onHostAddressMsg    (tDescriptor *desc, tHostAddressMsg *message) throw(int);
static void onAssignMsg         (tDescriptor *desc, tAssignMsg *message) throw(int);
static void onConnectUserMsg    (tDescriptor *desc, tConnectUserMsg *message) throw(int);
static void onIDMsg             (tDescriptor *desc, tIDMsg *message) throw(int);
static void onAddUserMsg        (tDescriptor *desc, tAddUserMsg *message) throw(int);
static void onDropUserMsg       (tDescriptor *desc, tDropUserMsg *message) throw(int);
static void onDisconnectMsg     (tDescriptor *desc, tMessage *message) throw(int);
static void onTextMsg           (tDescriptor *desc, tTextMsg *message) throw(int);
static void onPingMsg           (tDescriptor *desc, tPingMsg *message) throw(int);
static void onPongMsg           (tDescriptor *desc, tPongMsg *message) throw(int);
static void onFileTransferMsg   (tDescriptor *desc, tFileTransferMsg *message) throw(int);
static void onAbortDownloadMsg  (tDescriptor *desc, tMessage *message) throw(int);
static void onAbortUploadMsg    (tDescriptor *desc, tMessage *message) throw(int);
static void onFilePacketMsg     (tDescriptor *desc, tDataMsg *message) throw(int);



/***************************************************************************\
networkStartup

Verify that the network hasn't been initialized yet and initialize the
network with netStartup.  If no errors occur, then retrieve the maximum
message size, the receive buffer size, and the send buffer size.  Verify
that these fit within the acceptable ranges and record them in the output
log.  Initialize the lag timer with the current time.

Inputs:
    none
Outputs:
    Returns true if successful
    Returns false if the function failed
\***************************************************************************/
bool networkStartup()
{
    tSocket s;
    unsigned int maxMsgSize;
    int bufSize;

    //Verify that the network hasn't been initialized yet.  If so, record an
    //error message in the output file and abort.
#ifdef DEBUG
    if(networkActive)
    {
        bug("networkStartup: network already initialized");
        return false;
    }
#endif

    //Attempt to initialize the network with netStartup.  If the function fails,
    //then abort.
    if(netStartup() == false)
        return false;

    //Since we have succeeded in initializing the network, it is now "active".
    networkActive = true;

    //Initialize the lag timer with the current time.  We use the lag timer in
    //the network's tick function to ping the peers at regular intervals.
    lagTimer = currentTime;

    //Initialize a temporary datagram socket or stream socket depending on
    //whether we will connect using TCP/IP or UDP.  We will use this socket
    //to retrieve the message and buffer sizes.
    netNewSocket(useTCPIP, s);

    if(!useTCPIP)
    {
        //Retrieve the maximum message size using the socket we just created.
        //We need to make sure the operating system will support messages up
        //to MAX_BLOCK_SIZE bytes long.
        if(!netGetMaxMsgSize(s, maxMsgSize))
            return false;

        //Record the maximum packet size in the output file for debugging.
        log("Max packet size: %d", maxMsgSize);

        //Now, compare the operating system's largest packet size with our largest
        //packet size.  If the operating system does not support our largest packet,
        //then write an error in the output file and abort.
        if(MAX_BLOCK_SIZE > maxMsgSize)
        {
            bug("networkStartup: MAX_BLOCK_SIZE exceeds winsock SO_MAX_MSG_SIZE");
            netCloseSocket(s);
            return false;
        }
    }

    //Retrieve the receive buffer size
    if(!netGetRecvBufSize(s, bufSize))
        return false;

    //Record the receive buffer size in the output file.
    log("Receive buffer size: %d", bufSize);

    //Retrieve the send buffer size
    if(!netGetSendBufSize(s, bufSize))
        return false;

    //Record the send buffer size in the output file.
    log("Send buffer size: %d", bufSize);

    //Close the temporary socket since we have retrieved the necessary information
    //from it.
    netCloseSocket(s);

    //Ensure that the internal Windows receive buffer will hold our largest block of data.
    if(MAX_BLOCK_SIZE > bufSize)
    {
        bug("networkStartup: MAX_BLOCK_SIZE exceeds winsock SO_SNDBUF");
        networkShutdown();
        return false;
    }

    return true;
}

/***************************************************************************\
networkShutdown

Shuts down the network if active.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
void networkShutdown()
{
    if(networkActive)
    {
        netShutdown();
        networkActive = false;
    }
}

/***************************************************************************\
newSocket

Creates a new TCP/IP or UDP socket.  Sets the reuse address socket option meaning
that multiple sockets can have the same IP address and port so long as the
destination IP address/port differs.  Makes the socket non-blocking (application
doesn't freeze when operating system calls made).

Inputs:
    none
Outputs:
    socket      Handle to new socket
    Returns true if successful
\***************************************************************************/
bool newSocket(tSocket& s)
{
    if(!netNewSocket(useTCPIP, s))
        return false;

    if(!netSetReuseAddr(s))
        return false;

    if(!netSetNonBlocking(s))
        return false;

    return true;
}

/***************************************************************************\
engineListen

Assigns a new server socket to the engine.  Binds the socket to specified
IP address and port.  If either of these parameteres is 0, the function
automatically assigns an IP address and/or port.  With TCP/IP protocol, the
socket begins listening for new connections.  With UDP protocol, the socket
does not listen, but rather receives packets via receivefrom.

Inputs:
    IP      IP address on which to listen (or 0 for default)
    port    Port on which to listen (or 0 for default)
Outputs:
    Returns true if successful
\***************************************************************************/
bool engineListen(long IP, int port)
{
    //Close an existing server socket:
    if(server != INVALID_SOCKET)
    {
        //Refresh the server socket even if already exists
        //bug("engineListen: server socket not deallocated");
        netCloseSocket(server);
    }

    //Record requested address in output file:
#ifdef LOG
    unsigned char vals[4];
    longToIP(IP, vals);
    log("Listening on port %d with IP address %d.%d.%d.%d", port, vals[0], vals[1], vals[2], vals[3]);
#endif

    //Create a new socket:
    if(!newSocket(server))
        return false;

    //Bind the socket to the requested address:
    if(!netBindSocket(server, IP, port))
        return false;

    //Remember the engine's listening address:
    networkPort = port;
    networkIP = IP;

    //TCP/IP socket listens for new connections:
    if(useTCPIP)
        if(!netListen(server))
            return false;

    //Fill in local IP address and name:
    const char *localName = getLocalName();
    if(localName)
        peerName = localName;
    else
        peerName = "(Unknown)";

    listening = true;
    return true;
}

/***************************************************************************\
hostGame

Connects to the network as a host.  The host listens for new connections and
sets up a game.  The function initializes the network for either TCP/IP or UDP
protocol and listens on the specified address.  It also adds the local user's
info to the user list.

Inputs:
    IP, port        Listening address (specify 0 for either parameter to use default)
    username        Name that clients will see when they connect
    TCPIP           Connect with TCP/IP protocol if true
                    Connect with UDP protocol if false
Outputs:
    Returns true if successful
\***************************************************************************/
bool hostGame(long IP, int port, const char *username, bool TCPIP)
{
#ifdef DEBUG
    //You cannot host a game if you are trying to or have already connected to
    //the game host.
    if(gameHost != NULL)
    {
        bug("hostGame: trying to host game when gameHost != NULL");
        return false;
    }
    if(isGameHost)
    {
        bug("hostGame: you are already hosting a game");
        return false;
    }
    if(!isUsernameValid(username))
    {
        bug("hostGame: invalid username");
        return false;
    }
#endif
    //Choose protocol:
    useTCPIP=TCPIP;

    //Load username:
    globalName = username;

    //Initialize the network:
    if(!networkActive && !networkStartup())
        return false;

    //Listen on the specified address:
    if(!engineListen(IP, port))
        return false;

    //Set appropriate engine state variables:
    isGameHost = true;
    networkGame = true;

    //Add one user representing the host.  The host does not have a descriptor.
    localUser = addUser(globalName, 1, NULL);

    //Generate appropriate events:
    onHostGame();
    onAddUser(localUser, true);
    onSessionStartup();

    return true;
}

/***************************************************************************\
connectToHost

Connect to host with specified address via connectToPeer.  Similar to hostGame,
the function initializes the network for either TCP/IP or UDP protocol and listens
on the specified address for other peers.

Inputs:
    IP, port            Host's address
    username            Name that host will see when you connect
    TCPIP               Connect with TCP/IP protocol if true
                        Connect with UDP protocol if false
    localIP, localPort  Listening address (specify 0 for either parameter to use default)
Outputs:
    Returns true if successful
\***************************************************************************/
bool connectToHost(long IP, int port, const char *username, bool TCPIP,
    long localIP, int localPort)
{
#ifdef DEBUG
    if(gameHost != NULL)
    {
        bug("connectToHost: trying to connect to host when gameHost != NULL");
        return false;
    }
    if(isGameHost) //You cannot connect if you are hosting a game
    {
        bug("connectToHost: already hosting game");
        return false;
    }
    if(!isUsernameValid(username))
    {
        bug("connectToHost: invalid username");
        return false;
    }
#endif
    //Choose protocol:
    useTCPIP=TCPIP;

    //Load username:
    globalName = username;

    //Initialize the network:
    if(!networkActive && !networkStartup())
        return false;

    //Listen on the specified address:
    if(!engineListen(localIP,localPort))
        return false;

    //Set appropriate engine state variables:
    isGameClient = true;
    networkGame = true;

    //Complete the connection with connectToPeer:
    return connectToPeer(IP, port, &gameHost);
}

/***************************************************************************\
singlePlayer

Initializes a single-player game.  This function behaves the same way as
hostGame except it doesn't listen for new connections.

Inputs:
    username            The name that will represent you when the game begins
Outputs:
    Returns true if successful
\***************************************************************************/
bool singlePlayer(const char *username)
{
#ifdef DEBUG
    if(gameHost != NULL) //You cannot host a game if you are trying to or have already connected to the game host
    {
        bug("singlePlayer: trying to host game when gameHost != NULL");
        return false;
    }
    if(isGameHost)
    {
        bug("singlePlayer: you are already hosting a game");
        return false;
    }
    if(!isUsernameValid(username))
    {
        bug("singlePlayer: invalid username");
        return false;
    }
#endif

    //Load username:
    globalName = username;

    //Set appropriate engine state variables:
    isGameHost = true;

    //Add one user representing the host.  A single player does not have a descriptor.
    localUser = addUser(globalName, 1, NULL);

    //Generate appropriate events:
    onSinglePlayer();
    onAddUser(localUser, true);
    onSessionStartup();

    return true;
}

/***************************************************************************\
reconnect

Re-establishes connection with peers if your IP address changes (e.g. you lose
your connection to the Internet and your ISP assigns you a new IP).  The function
closes all open sockets and attempts to reconnect to each peer from the new
address.

Inputs:
    IP, port        Address from which to reconnect
Outputs
    Returns true if successful
\***************************************************************************/
bool reconnect(long IP, int port)
{
#ifdef DEBUG
    if(!isGameHost && gameHost==NULL)
    {
        bug("reconnect: you're not connected to a game");
        return false;
    }
#endif

    //Create a new listening socket that is bound to the new address
    if(!engineListen(IP,port))
        return false;

    for(tDescriptor *d=firstDescriptor; d; d=d->next)
    {
        //If using the TCP/IP protocol, close all open sockets:
        if(useTCPIP && d->socket != INVALID_SOCKET)
        {
            netCloseSocket(d->socket);
            d->connected=false;
        }
        //Attempt to reconnect to each peer from the new address:
        if(!reconnectToPeer(d))
        {
            sessionShutdown();
            onNetworkError();
            return false;
        }
    }

    //Generate appropriate events:
    onReconnect();
    return true;
}

/***************************************************************************\
connectToPeer

Attempts to connect to a peer.  With TCP/IP, the function connects to the peer
with a new socket and stores the socket handle in a new descriptor.  With UDP,
the function makes a new descriptor, but uses the engine's server socket to
send preliminary data to the peer.  Since the results of the connection will not
be known for >100 ms, the function starts a "connection time-out" counter.  It
will not block under any circumstances.

Inputs:
    IP, port            Address of peer to which to connect
    d                   If you provide the address of a pointer, the function will
                            assign the address of the new descriptor to the pointer.
                        If you provide a NULL pointer, the function will ignore
                            this parameter.
Outputs:
    Returns true if successful
\***************************************************************************/
bool connectToPeer(long IP, int port, tDescriptor **d)
{
    tSocket socket;
    tDescriptor *desc;
#ifdef LOG
    unsigned char val[4];
    longToIP(IP, val);
    log("Connecting to IP address %d.%d.%d.%d, port %d from port %d", val[0], val[1], val[2], val[3], port, networkPort);
#endif

    //Zero the pointer in case the function fails:
    if(d)
        *d = NULL;

    if(useTCPIP)
    {
        //Initialize a new socket:
        if(!newSocket(socket))
            return false;

        //Bind the socket to the local address:
        if(!netBindSocket(socket, networkIP, networkPort))
            return false;

        //Connect the socket to the remote address:
        if(!netConnect(socket, IP, port))
            return false;
    }

    desc = addDescriptor(IP, port);

    if(useTCPIP)
        desc->socket = socket;          //Store the socket handle in the descriptor.
    else
        connectionEstablished(desc);    //Send preliminary data.

    //Store the descriptor's address in the provided pointer:
    if(d)
        *d = desc;

    return true;
}

/***************************************************************************\
reconnectToPeer

Similar to connectToPeer, except that the same descriptor is reused.  For TCP/IP,
the function creates a new socket and attempts to connect to the remote computer.

Inputs:
    d           Address of an existing descriptor (must contain valid IP address
                    and port)
Outputs:
    Returns true if successful
\***************************************************************************/
bool reconnectToPeer(tDescriptor *d)
{
#ifdef LOG
    unsigned char val[4];
    longToIP(d->IPAddress, val);
    log("Reconnecting to IP address %d.%d.%d.%d and port %d", val[0], val[1], val[2], val[3], d->port);
#endif
#ifdef DEBUG
    if(MEMORY_VIOLATION(d))
        return false;
#endif

    //Restart the "connection time-out" timer:
    //1/2/03: Moved here in case netBindSocket fails
    d->connectTimer = currentTime;

    if(useTCPIP)
    {
#ifdef DEBUG
        if(d->socket != INVALID_SOCKET)
        {
            bug("reconnectToPeer: socket not closed");
            netCloseSocket(d->socket);
        }
#endif
        //Initialize a new socket:
        if(!newSocket(d->socket))
            return false;

        //Bind the socket to the local address:
        if(!netBindSocket(d->socket, networkIP, networkPort))
            return false;

        //Connect the socket to the remote address:
        if(!netConnect(d->socket, d->IPAddress, d->port))
            return false;
    }
    else
        connectionEstablished(d); //Send preliminary data immediately.

    return true;
}

/***************************************************************************\
networkTick

Call this function in the main game loop to manage socket connections.

Inputs:
    none
Outputs:
    Returns true if successful
\***************************************************************************/
bool networkTick()
{
    long IP;
    int bytes, len, port;
    tDescriptor *next;
    tDescriptor *desc;
    tPacket *p, *lastPacket;
    char buf[BUFFER_SIZE], *ptr;
    bool found;

    /**************************************************************************
                          Update socket connections
     **************************************************************************/

    //Pull out expired descriptors:
    for(desc = firstDescriptor; desc != NULL; desc = next)
    {
        next = desc->next;
        if(desc->destroyed)
            deleteDescriptor(desc);
    }

    //Ping peers in order to calculate lag:
    if(networkActive && listening)
    {
        /* At regular intervals, send a ping to each descriptor containing a
         * unique ID.  The remote computer echoes a pong with the same ID.
         * The engine calculates the lag by dividing the time elapsed
         * (ping sent -> pong received) by two.
         */
        if(currentTime - lagTimer > LAG_INTERVAL)
        {
            lagTimer = currentTime;
            for(desc=firstDescriptor; desc!=NULL; desc=desc->next)
                if(desc->connected && !desc->bufferFull)
                {
                    desc->lastPingID++;
                    desc->pingTime = currentTime;
                    tPingMsg message(desc->lastPingID);
                    writeToBuffer(desc, &message);
                }
        }

        /* If a descriptor has lost its connection unintentionally (e.g. network
         * error), wait CONNECTION_DELAY before trying to reconnect.
         */
		for(desc = firstDescriptor; desc != NULL; desc = desc->next)
			if(desc->reconnect && currentTime - desc->connectTimer > CONNECTION_DELAY)
			{
				desc->reconnect = false;
                reconnectToPeer(desc);
                //Changed 1/2/03: If we fail to reconnect because we can no longer
                //bind to the local address, then don't abort.  Just wait until
                //the player manually reconnects from a new address.
                /*if(!reconnectToPeer(desc))
				{
            		sessionShutdown();
                	onNetworkError();
		            return false;
				}*/
			}

        /* If a descriptor has overflowed the winsock outBuffer, wait
         * OVERFLOW_TIMEOUT before sending additional data through descriptor.
         */
		for(desc = firstDescriptor; desc != NULL; desc = desc->next)
			if(desc->bufferFull && currentTime - desc->bufferFullTimer > OVERFLOW_TIMEOUT)
				desc->bufferFull = false;

        if(!useTCPIP)
        {
            /* UDP only: resend one packet every RESEND_INTERVAL if requested
             * Originally, I resent packets immediately when they were requested
             * but this worsened the packet loss by increasing the bandwidth
             * Eventually, the two computers flooded one another with resend
             * requests and resent packets.
             */
            for(desc = firstDescriptor; desc != NULL; desc = desc->next)
                //Changed 1/1/03: Don't try to resent packets unless the descriptor
                //connected.  Otherwise, if netSendTo fails, this routine will
                //flood the peer with resent packets, resent resent packets, etc.
                if(desc->connected && !desc->bufferFull && currentTime - desc->resendTimer > RESEND_INTERVAL)
                {
                    desc->resendTimer = currentTime;
                    lastPacket=NULL;
                    /* Find the *oldest* packet.  Since the packets are stored
                     * from newest to oldest, we must traverse the whole packet
                     * list and select the last packet with a resend flag.
                     */
                    for(p = desc->outPackets; p; p=p->next)
                        if(p->resend)
                            lastPacket=p;
                    if(lastPacket)
                    {
                        //No redundancy on resent packets; only the message that
                        //was requestet is resent.

                        //Assume that the packet arrives successfully and no
                        //longer needs to be resent:
                        lastPacket->resend = false;

                        //Copy the raw message data (ID, length and additional
                        //parameters) to a temporary buffer:
                        ptr = buf;
                        *((long *) ptr) = lastPacket->ID;
						ptr+=sizeof(long);
                        *((long *) ptr) = lastPacket->length;
						ptr+=sizeof(long);
                        *((char *) ptr) = lastPacket->checkSum;
                        ptr+=sizeof(char);
                        *((char *) ptr) = lastPacket->immediate;
                        ptr+=sizeof(char);
                        memcpy(ptr, lastPacket->data, lastPacket->length);
                        ptr += lastPacket->length;

                        //Put the message data in a container for "shipment":
                        tDataMsg message(MSG_PACKET_RESENT, (int) (ptr-buf), buf);
#ifdef DEBUGUDP
                        log("Resending packet #%d of length %d", lastPacket->ID, lastPacket->length);
#endif
                        //Send the message immediately to avoid complications
                        //with outgoing buffer:
                        len=message.packageMessage(buf, sizeof(buf));
                        writeToSocket(desc, buf, len, true);
                    }
                }
        }

        /* File Transfer:
         * Send one packet whose size is FILE_PACKET_SIZE. To avoid flooding,
         * I carefully moderate the packet transmission rate.  With the TCP/IP
         * protocol, I send one packet each time this function executes until
         * the upload finishes.  With the UDP protocol, I send MAX_BACKLOG
         * packets and wait for an acknowledgment before sending additional
         * packets.
         */
		for(desc = firstDescriptor; desc != NULL; desc = desc->next)
			if(desc->fOut)  //Iterate through each descriptor that is uploading a file.
            {
                if(desc->bytesSent < desc->fileOut.size && !desc->bufferFull &&
                    (useTCPIP || desc->outPacketCount < MAX_BACKLOG))
                {
                    int bytes = MIN3(FILE_PACKET_SIZE, desc->fileOut.size - desc->bytesSent, BUFFER_SIZE - desc->outBufferTop);
                    int bytesRead = fread(buf, 1, bytes, desc->fOut);
                    if(bytesRead < bytes)
                        bug("networkTick: read from file failed");
                    if(bytesRead > 0)
                    {
                        tDataMsg message(MSG_FILE_PACKET, bytes, buf);
                        writeToBuffer(desc, &message);
                        desc->bytesSent += bytes;
                    }
                }
                if(desc->bytesSent >= desc->fileOut.size) //Upload complete
                {
                    fclose(desc->fOut);
                    desc->fOut=NULL;

                    //Generate a user-interface event:
                    onUploadComplete(desc);

                    //Generate an engine event:
                    onFileSent(desc);

                    desc->fileOut.clear(); //Clear the tFile structure AFTER we generate the events.
                }
            }

        /* Write buffered data to sockets; read data from sockets to buffers;
         * listen for new connections; check for socket errors.  Due to the
         * differences between TCP/IP and UDP protocol, we must use entirely
         * separate routines for each:
         */
        if(useTCPIP)
        {
            /* TCP/IP uses a single call to netSelect to retrieve the status of
             * the sockets in three sets.  netSelect checks sockets in the
             * first set for readability, sockets in the second for writability,
             * and sockets in the third set for exceptions.
             */

            //Clear the sets since we want to create them anew each time:
            netClearSets();

            //Compile a set of sockets to be checked for readability,
            //writability, and errors:
            for(desc = firstDescriptor; desc != NULL; desc = desc->next)
                if(desc->socket != INVALID_SOCKET)  //Add all valid socket handles
                    netIncludeSocket(desc->socket);

            //If listening, we also want to check for new connections:
            if(listening)
                netIncludeSocket(server);

            //Retrieve the status of the queried sockets.  Bail out if an error
            //occurred.
            if(!netSelect())
                return false;

            //Handle socket errors.  If there is an error, attempt to reconnect
            //or close the connection.
            for(desc = firstDescriptor; desc != NULL; desc = desc->next)
                if(netHasError(desc->socket))
                    connectionLost(desc);

            //Output:
            //Send buffered data if the socket is connected or mark the socket
            //as connected if it is "writable".
            for(desc = firstDescriptor; desc != NULL; desc = desc->next)
            {
                if(desc->connected) //Descriptor is connected.
                {
                    if(netCanWrite(desc->socket) && desc->outBufferTop > 0 && !desc->bufferFull)
                        writeToDescriptor(desc);
                }
                else if(desc->socket != INVALID_SOCKET) //Descriptor is trying to connect.
                {
                    if (netCanWrite(desc->socket)) //Writability indicates that connection has been established.
                    {
                        desc->connected = true;
                        connectionEstablished(desc);
                    }
                    else if (currentTime - desc->connectTimer > CONNECTION_TIMEOUT) //Connection timed out.
                    {
                        log("Connection timed out");
                        connectionLost(desc);
                    }
                }
            }

            //Input:
            //Read from socket if there is buffered data waiting.
            for(desc = firstDescriptor; networkActive && desc != NULL; desc = desc->next)
                if(netCanRead(desc->socket)) //Data waiting
                {
                    if(!readFromTCPIPDescriptor(desc)) //Error reading from socket, or connection closed
                    {
                        connectionLost(desc);
                        continue;
                    }

                    //Interpret data waiting in inBuffer:
                    if(desc->inBufferTop > 0)
                        readFromBuffer(desc);
                }

            //Handle new descriptors:
            if( listening && netCanRead(server) )
                newConnection();
        }
        else if (server != INVALID_SOCKET) //UDP
        {
            /* Since UDP has connectionless sockets, the concept of readability
             * and writability does not apply.  First, we send buffered data
             * using a variation of writeToDescriptor.  Second, we receive
             * all packets through the central server socket.  We determine
             * who sent the packet (and hence which descriptor the data belongs
             * in) from its source address.  If the address is unknown, we treat
             * it as a new connection.
             */

            //Output:
            for(desc = firstDescriptor; desc != NULL; desc = desc->next)
                if(desc->outBufferTop > 0 && !desc->bufferFull)
                    writeToDescriptor(desc); //Send data waiting in outBuffer

            //Input:
            //We use server for incoming packets.
            while(server != INVALID_SOCKET)
            {
                //Peek at the next packet on the queue.  Abort if there
                //aren't any more packets to receive.
                len=sizeof(buf);
                if(!netRecvFrom(server, IP, port, buf, len, true))
                    break;

                /* UDP hack: if packet begins with special signature, then search
                 * for an existing descriptor by ID.  If one exists, then update
                 * the descriptor's IP address and port.
                 */
                if(len == 5 && buf[0]=='\xff' && buf[1]=='\xff' &&
                    buf[2]=='\xff' && buf[3]=='\xff')
                {
                    desc = descriptorLookup(buf[4]);
                    if(desc)
                    {
                        desc->IPAddress=IP;
                        desc->port=port;
#ifdef LOG
                        unsigned char vals[4];
                        longToIP(IP, vals);
                        log("Redirecting packet from IP address %d.%d.%d.%d port %d to descriptor #%d",
                            vals[0],vals[1],vals[2],vals[3],port,desc->ID);
#endif
                    }
                    //Discard the packet even if a matching descriptor wasn't found.
                    netRecvFrom(server, IP, port, buf, len, false);

                    //Proceed to retrieve the next packet.
                    continue;
                }

                found=false;

                //Look through all descriptors to see from where the message came:
                for(desc = firstDescriptor; desc != NULL; desc = desc->next)
                {
                    if(IP==desc->IPAddress && port==desc->port)
                    {
                        found=true;

                        //When we receive at least one message from the descriptor,
                        //mark it as connected.
                        desc->connected = true;

                        //Grab the message again to remove it from the internal
                        //Winsock data packet queue.
                        netRecvFrom(server, IP, port, buf, len, false);

                        //Unpackage the packets received:
                        if (!readFromUDPDescriptor(desc, buf, len))
                            closeDescriptor(desc);
                        else
                        {
                            //Interpret data waiting in inBuffer:
                            if(desc->inBufferTop > 0)
                                readFromBuffer(desc);

                            //Extract data from packets received out of order and
                            //process the data after each packet to prevent
                            //overflowing the in buffer:
                            while(networkActive && desc->connected && desc->inPackets &&
                                desc->inPackets->ID == desc->nextPacketIn)
                            {
                                //Some packets in the linked list serve as place-holders
                                //and contain no data.
                                if(desc->inPackets->data)
                                {
                                    //Determine if there is enough space in the in buffer
                                    //to hold the packet.
                                    bytes = desc->inPackets->length;
                                    if(bytes+desc->inBufferTop>BUFFER_SIZE)
                                    {
                                        bug("networkTick: buffer overflow");
                                        break;
                                    }

                                    //Append the packet data to the end of the in buffer
                                    //and incremenent to the top index accordingly.
                                    memcpy(desc->inBuffer + desc->inBufferTop,
                                        desc->inPackets->data, bytes);
                                    desc->inBufferTop += bytes;

                                    //Interpret data waiting in inBuffer:
                                    if(desc->inBufferTop > 0)
                                        readFromBuffer(desc);
                                }

                                if(desc->connected)
                                {
                                    //Remove the retired packet from the front of the
                                    //linked list.
                                    desc->nextPacketIn++;
                                    p = desc->inPackets;
                                    desc->inPackets = desc->inPackets->next;
                                    deletePacket(p);
                                }
                            }
                        }
                        //We have found a descriptor with a matching address so we
                        //have no need to look throught the remaining descriptors.
                        break;
                    }
                }
                //If the packet arrived from an unknown address, treat it as a new
                //connection:
                if(!found)
                    newConnection();
            }

            //Terminate descriptors that failed to connect:
            for(desc = firstDescriptor; desc != NULL; desc = desc->next)
                if (!desc->connected && currentTime - desc->connectTimer > CONNECTION_TIMEOUT)
                    connectionLost(desc);
        }
    }

    return true;
}

/***************************************************************************\
readFromTCPIPDescriptor

Copies incoming data from the internal Winsock buffer to the end descriptor's
buffer.  Care is taken not to overflow this buffer.  The function will return
false if the connection has been closed (either gracefully or abruptly).

Inputs:
    desc            Descriptor from which to read data
Outputs:
    Returns false if the connection was closed (no distinction made between
        graceful and abrupt disconnects)
\***************************************************************************/
bool readFromTCPIPDescriptor(tDescriptor *desc)
{
    int bytesRead;
#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return false;
    if(!desc->connected)
    {
        bug("readFromTCPIPDescriptor: descriptor not connected");
        return false;
    }
    if(desc->socket == INVALID_SOCKET)
    {
        bug("readFromTCPIPDescriptor: descriptor's socket is invalid");
        return false;
    }
#endif
    bytesRead = sizeof(desc->inBuffer) - desc->inBufferTop;
    if(!netRecv(desc->socket, desc->inBuffer + desc->inBufferTop, bytesRead)) //Connection closed
    {
#ifdef LOG
        log("Connection on descriptor #%d closed", desc->ID);
#endif
        return false;
    }

    //Data may not be available yet if netRecv failed with EWOULDBLOCK.
    if(bytesRead > 0)
        desc->inBufferTop += bytesRead;
    else
        return true;

    //Record the number of packets received for statistical purposes.
    packetsRecv++;
    return true;
}

/***************************************************************************\
writeToDescriptor

Attempts to send all of the data contained in a descriptor's out buffer.  The
function splits the data into blocks no larger than MAX_BLOCK_SIZE.  The treatment
of the blocks depends on the choice of protocol.  With TCP/IP, the function
sends them directly with netSend.  With UDP, it first packages and archives the
packets with writeToSocket.  If an error occurs, it removes the sent data from
the beginning of the out buffer so the remaining data can be sent at a later
time.  If it overflows the internal Winsock buffer, it prevents further
writing to the socket until a reasonable time has passed.  If it encounter any
other error, it will call connectionLost.

Inputs:
    desc            Descriptor to which to write
Outputs
    none
\***************************************************************************/
void writeToDescriptor(tDescriptor *desc)
{
    int i, bytesWritten, blockSize;
    char buffer[BUFFER_SIZE];
    bool failed = false;

#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return;
    if(desc->outBufferTop <= 0)
    {
        bug("writeToDescriptor: no data to send!");
        return;
    }
    if(useTCPIP)
    {
        if(desc->socket == INVALID_SOCKET)
        {
            bug("writeToDescriptor: descriptor's socket is invalid");
            connectionLost(desc);
            return;
        }
        if(!desc->connected)
        {
            bug("writeToDescriptor: descriptor not connected");
            connectionLost(desc);
            return;
        }
    }
#endif

    //Send the data contained in outBuffer in pieces of size MAX_BLOCK_SIZE:
    for(i=0; i<desc->outBufferTop; i+=bytesWritten)
    {
        //Determine how many bytes we wish to send:
        bytesWritten = blockSize = MIN(MAX_BLOCK_SIZE, desc->outBufferTop - i);
        if(useTCPIP)
        {
            //netSend will attempt to send the data and replace bytesWritten with
            //the actual number of bytes written.
            if(!netSend(desc->socket, desc->outBuffer + i, bytesWritten))
            {
                /* Determine whether we have overflowed the internal Winsock
                 * buffer.  If so, prevent further writing to the socket for
                 * a reasonable amount of time.  Otherwise, assume the connection
                 * has been lost.
                 */
                if(netIsBufferFull())
                {
                    desc->bufferFull=true;
                    desc->bufferFullTimer = currentTime;
                }
                else
                    connectionLost(desc);
                failed=true;
                break;
            }
        }
        else
        {
            //In UDP, package and archive the data first.  We pretend that all
            //of the bytes were written, even if the peer did not receive them.
            writeToSocket(desc, desc->outBuffer+i, blockSize, false);
        }
        //Record the number of packets sent for statistical purposes.
        packetsSent++;
    }
    if(failed)
    {
        //Remove the sent data from the outbuffer so we can send the rest of the
        //data at a later time.
        desc->outBufferTop-=i;
        memcpy(buffer, desc->outBuffer+i, desc->outBufferTop);
        memcpy(desc->outBuffer, buffer, desc->outBufferTop);
    }
    else
        //All of the data was successfully sent, so we can clear the out buffer.
        desc->outBufferTop = 0;
}

/***************************************************************************\
readFromUDPDescriptor

Processes a data packets from a buffer according to our UDP algorithm.  You will
usually call this function after you receive a stream of data from a socket.
The function extracts as much data as it can into the descriptor's buffer.  It
archives the remaining out-of-order packets in a linked list.  It will process
these as missing packets begin to arrive.  The function automatically requests
missing packets and acknowledges packets it receives.

Inputs:
    desc        Descriptor through which to read the data
    buffer      Memory address where the data begins
    length      Length of data
Outputs:
    Returns true if successful
\***************************************************************************/
bool readFromUDPDescriptor(tDescriptor *desc, const char *buffer, int length)
//process packet (pointed to by buffer) and store data in inBuffer if no missing packets were detected
//Process incoming packets and extract data to inBuffer
{
    int bytes, len, i;
    const char *ptr = buffer, *end;
    char buf[BUFFER_SIZE];
    long ID;
    tPacket *packet, *p;
    unsigned char expectedSum, actualSum;
    bool immediate;

#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return false;
    if(ARRAY_VIOLATION(buffer, length))
        return false;
    if(length <= 0)
    {
        bug("readFromUDPDescriptor: length <= 0");
        return false;
    }
#endif
#ifdef DEBUGUDP
    log("Incoming data from descriptor #%d", desc->ID);
#endif

    end = buffer+length;
    //Grab packets from the buffer until the end is reached.
    while (ptr < end)
    {
        //Make sure we don't read past the end of the buffer.  Under ideal
        //circumstances, these checks are only needed if the data becomes
        //corrupt.
        if(ptr + sizeof(long) > end)
        {
            bug("readFromUDPDescriptor: data packet missing ID");
            return false;
        }
        ID = *((long *) ptr); //Record the packet ID.
		ptr+=sizeof(long);

        if(ptr + sizeof(long) > end)
        {
            bug("readFromUDPDescriptor: data packet missing length");
            return false;
        }
        len = *((long *) ptr); //Record the packet length.
		ptr+=sizeof(long);

        if(ptr + sizeof(char) > end)
        {
            bug("readFromUDPDescriptor: data packet missing check sum");
            return false;
        }
        expectedSum = *((char *) ptr); //Record the packet's check sum.
        ptr+=sizeof(char);

        if(ptr + sizeof(char) > end)
        {
            bug("readFromUDPDescriptor: data packet missing immediate flag");
            return false;
        }
        immediate = (bool)(*((char *) ptr)); //Record the packet's immediate flag.
        ptr+=sizeof(char);

        //Determine if length is within range:
        if(ptr + len > end)
        {
            bug("readFromUDPDescriptor: data packet continues past end of buffer");
            return false;
        }

        //Determine if the data has been corrupted using the check sum:
        actualSum=0;
        for(i=0;i<len;i++)
            actualSum+=ptr[i];

        if(actualSum != expectedSum)
        {
            bug("readFromUDPDescriptor: corrupted data packet received");
            return false;
        }

#ifdef DEBUGUDP
        char buf2[10000];
        tBufferIterator ptr2(buf2, sizeof(buf2));
        try
        {
            mySprintf(ptr2, "Received packet #%d: (%d bytes)", ID, len);
            for(i=0;i<len;i++)
                mySprintf(ptr2, " %d", (unsigned char)ptr[i]);
        }
        catch(int x)
        {
            bug("readFromUDPDescriptor: buffer overflow");
        }
        log(buf2);
#endif

        /* If the received packet immediately follows the last packet received,
         * extract the data to the inBuffer and add the packet's ID to the
         * acknowledgment array.
         */
        if(ID == desc->nextPacketIn)
        {
            bytes = len;
            //Ensure that the packet data will fit in the inBuffer:
            if(bytes+desc->inBufferTop>BUFFER_SIZE)
            {
                bug("readFromUDPDescriptor: buffer overflow #1");
                return false;
            }

            //Copy the data from the temporary buffer to the inBuffer:
            memcpy(desc->inBuffer + desc->inBufferTop, ptr, bytes);
            desc->inBufferTop += bytes;

            //Prepare to receive the next sequential packet:
            desc->nextPacketIn++;

            //Acknowledge that the engine has received the packet:
            if(desc->packetAckCount >= ACK_INTERVAL)
                bug("readFromUDPDescriptor: packet acknowledgement overflow #1");
            else
                desc->packetAcks[desc->packetAckCount++] = ID;
        }
        /* If the packet was received out of order, store the packet in the
         * proper location in the linked list of incoming packets.  The packet
         * will be processed after the missing packets are received and
         * processed.
         */
        else if(ID > desc->nextPacketIn)
        {
            //Determine whether we have already received this packet.  If so,
            //there is no need to record it again.
            for(p=desc->inPackets; p; p=p->next)
                if(p->ID == ID)
                    break;
            if(p == NULL) //We have not received this packet yet:
            {
                //Prepare to store the packet data in a tPacket structure:
                packet = newPacket(ID);

                /* If the message is a resent packet, a packet resend request, or
                 * a packet acknowledgment, process the message immediately even
                 * though it's out of order.  This is crucial in order to avert
                 * a bottleneck.  We will proceed to insert a tPacket object
                 * in the incoming packets list at the appropriate location, but
                 * it won't have any data associated with it.  Instead, it will
                 * act as a placeholder.
                 */
                if(/**ptr == MSG_PACKET_RESENT || *ptr == MSG_RESEND || *ptr == MSG_ACK_PACKETS*/
                    immediate)
                {
                    bytes = len;
                    //Ensure that the packet data will fit in the inBuffer:
                    if(bytes+desc->inBufferTop>BUFFER_SIZE)
                    {
                        bug("readFromUDPDescriptor: buffer overflow #2");
                        return false;
                    }
                    /* It's crucial that we insert the message at the beginning
                     * of the buffer since the buffer may contain an incomplete
                     * message.
                     */
                    memcpy(buf, desc->inBuffer, desc->inBufferTop);
                    memcpy(desc->inBuffer + bytes, buf, desc->inBufferTop);
                    memcpy(desc->inBuffer, ptr, bytes);
                    desc->inBufferTop += bytes;
                }
                else
                {
                    //Store the packet data in a new dynamic array:
                    packet->length = len;
                    packet->data = mwNew char[len];
                    memcpy(packet->data, ptr, len);
                }
                //Determine if the packet belongs at the beginning of the
                //linked list:
                if(desc->inPackets == NULL || ID<desc->inPackets->ID)
                {
                    /* If the inPackets list is empty and this packet was received
                     * out-of-order, we assume that one or more packets were lost.
                     * In the case where the inPackets list is not empty (one or
                     * more packets proceed this one in the inPackets list), we
                     * assume that we have already requested the lost packets.
                     */
                    if(desc->inPackets == NULL)
                    {
                        tResendMsg message(desc->nextPacketIn, packet->ID-1);
                        bytes=message.packageMessage(buf, sizeof(buf));
                        writeToSocket(desc, buf, bytes, true);
                    }
                    //Insert the packet at the beginning of the linked list:
                    packet->next = desc->inPackets;
                    desc->inPackets = packet;

                    //Acknowledge that the engine has received the packet:
                    if(desc->packetAckCount >= ACK_INTERVAL)
                        bug("readFromUDPDescriptor: packet acknowledgement overflow #2");
                    else
                        desc->packetAcks[desc->packetAckCount++] = ID;
                }
                else    //Packet belongs in the middle or end of the linked list
                {
                    //Determine the exact location where the packet belongs:
                    p = desc->inPackets;
                    while(p->next && ID>p->next->ID)
                        p=p->next;

                    //Insert the packet at the proper location:
                    packet->next = p->next;
                    p->next = packet;

                    //If the packet was added to the end of the list, request that
                    //the missing packets be resent.  If the packet was added
                    //anwhere else, assume that we have already requested the
                    //missing packets.
                    if (packet->next == NULL && p->ID+1<=packet->ID-1)
                    {
                        tResendMsg message(p->ID+1, packet->ID-1);
                        bytes=message.packageMessage(buf, sizeof(buf));
                        writeToSocket(desc, buf, bytes, true);
                    }
                    //Acknowledge that the engine has received the packet:
                    if(desc->packetAckCount >= ACK_INTERVAL)
                        bug("readFromUDPDescriptor: packet acknowledgement overflow #3");
                    else
                        desc->packetAcks[desc->packetAckCount++] = ID;
                }
            }
        }
#ifdef DEBUGUDP
        else
            log("Already have packet #%d", ID);
#endif

        //Move to the address where the next packet begins:
        ptr += len;

        //After ACK_INTERVAL acknowledgements accumulate in packetAcks,
        //send a message containing the message ID's.
        if(desc->packetAckCount>=ACK_INTERVAL)
        {
            tAckPacketsMsg message(desc->packetAcks);
            bytes=message.packageMessage(buf, sizeof(buf));
            writeToSocket(desc, buf, bytes, true);
            desc->packetAckCount=0;
        }
    }
    //Record the number of packets received for statistical purposes.
    packetsRecv++;
    return true;
}

/***************************************************************************\
writeToSocket

Writes data to a socket based on our UDP algorithm.  The function stamps the
data with a header and sends it together with one or more old packets.
Regardless of whether netSendTo succeeds, it archives the data in a new packet.
In this manner, it prevents any data loss.

Inputs:
    desc            Descriptor through which to send data
    buffer          Address in memory where data begins
    length          Length of data
    immediate       Should the recipient execute the message immediately?
                        Set this flag to true for resend, packet resent, and
                        packet acknowledgments, only.
Outputs:
    none
\***************************************************************************/
void writeToSocket(tDescriptor *desc, const char *buffer, int length, bool immediate)
{
    int i, j, len;
    tPacket *packet, *p, *outPackets[1+REDUNDANT_PACKETS];
    char buf[BUFFER_SIZE], *ptr=buf;
    bool failed=false;
    unsigned char checkSum=0;

#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return;
    if(ARRAY_VIOLATION(buffer, length))
        return;
    if(length <= 0)
    {
        bug("writeToSocket: length <= 0");
        return;
    }
#endif

    /* Calculate the check sum from the provided data.  The check sum provides
     * the remote computer with an easy means to determine if the packet is
     * corrupt.
     */
    for(i=0;i<length;i++)
        checkSum+=buffer[i];

    /* The outgoing packets are linked from newest to oldest but we want to
     * package the packets from oldest to newest.  So, we copy the addresses
     * of the first X packets into an array and retrieve the addresses in
     * reverse.  X denotes up to REDUNDANT_PACKETS depending on how many
     * packets are available in the outgoing packets list.
     */
    for(i=0,p=desc->outPackets;i<REDUNDANT_PACKETS && p;i++,p=p->next)
        outPackets[i] = p;

    //Concatenate all of the redundant packets back to back in a buffer:
    for(j=i-1;j>=0;j--)
    {
        //We prefix each packet with its ID, length (exluding this header), and check sum.
        *((long *) ptr) = outPackets[j]->ID;
		ptr+=sizeof(long);
        *((long *) ptr) = outPackets[j]->length;
		ptr+=sizeof(long);
        *((char *) ptr) = outPackets[j]->checkSum;
        ptr+=sizeof(char);
        *((char *) ptr) = outPackets[j]->immediate;
        ptr+=sizeof(char);
        memcpy(ptr, outPackets[j]->data, outPackets[j]->length);
        ptr += outPackets[j]->length;
    }
    //Finally, append the current message to the buffer:
    *((long *) ptr) = desc->nextPacketOut;
	ptr+=sizeof(long);
    *((long *) ptr) = length;
	ptr+=sizeof(long);
    *((char *) ptr) = checkSum;
    ptr+=sizeof(char);
    *((char *) ptr) = (char)immediate;
    ptr+=sizeof(char);
    memcpy(ptr, buffer, length);
    ptr += length;

    //We must use a temporary variable here since netSendTo needs a reference to
    //the buffer length.
    len=(int) (ptr-buf);

    //Write the packet to the socket:
    if(!netSendTo(server, desc->IPAddress, desc->port, buf, len))
    {
        if(netIsBufferFull()) //Winsock outgoing buffer has overflowed
        {
            //Prevent writing to the socket for a reasonable amount of time:
            desc->bufferFull=true;
            desc->bufferFullTimer = currentTime;
        }
        else
            connectionLost(desc);
        //Immediately mark the subsequent packet for resending.
        failed=true;
    }

    //Archive the packet for inclusion in the next outgoing packet.  We may
    //also need to resend the packet if the peer loses it.
    packet = newPacket(desc->nextPacketOut++);

    packet->length = length;
    packet->checkSum = checkSum;
    packet->immediate = (char)immediate;

    //Copy the packet data into a new dynamic array since the provided buffer
    //is volatile.
    packet->data = mwNew char[length];
    memcpy(packet->data, buffer, length);
    packet->resend = failed;

    //Insert the packet at the beginning of the outgoing packets list.
    packet->next = desc->outPackets;
    desc->outPackets = packet;
    desc->outPacketCount++;

#ifdef DEBUGUDP
    char buf2[10000];
    tBufferIterator ptr2(buf2, sizeof(buf2));
    try
    {
        mySprintf(ptr2, "Packet #%d:", packet->ID);
        for(i=0;i<packet->length;i++)
            mySprintf(ptr2, " %d", (unsigned char)packet->data[i]);
    }
    catch(int x)
    {
        bug("writeToSocket: buffer overflow");
    }
    log(buf2);
#endif
}

/***************************************************************************\
newPacket

Allocates memory for a new packet and initializes it with the provided ID.

Inputs:
    ID              ID to assign the new packet
Outputs:
    Returns a pointer to the new packet
\***************************************************************************/
tPacket *newPacket(long ID)
{
    tPacket *p = mwNew tPacket;
    if(p==NULL)
    {
        bug("newPacket: failed to allocate memory for a new packet");
        return NULL;
    }
    p->ID = ID;
    p->length = 0;
    p->checkSum = 0;
    p->immediate = 0;
    p->data = NULL;
    p->resend = false;
    p->next = NULL;
    return p;
}

/***************************************************************************\
deletePacket

Deallocates memory in use by the specified packet.  The user should remove the
packet from any linked lists prior to calling this function.

Inputs:
    packet          Packet to delete
Outputs:
    none
\***************************************************************************/
void deletePacket(tPacket *packet)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(packet))
        return;
#endif
    if(packet->data)
        mwDelete packet->data;
    mwDelete packet;
}

/***************************************************************************\
resendPackets

Marks all outgoing packets in the specified descriptor for resending.  The
engine resends each one by one with each call to networkTick.

Inputs:
    desc            Descriptor containing packets to resend
Outputs:
    none
\***************************************************************************/
void resendPackets(tDescriptor *desc)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return;
#endif
    for(tPacket *p=desc->outPackets; p; p=p->next)
        p->resend=true;
}

/***************************************************************************\
readFromBuffer

Interprets the data contained in the in buffer.  The function reads one message
at a time and calls the appropriate message handler (on...Msg).  It discards
the data after it successfully interpreting it.

Inputs:
    desc        Descriptor from which to interpret data
Outputs:
    none
\***************************************************************************/
//This macro calls the appropriate handler for each type of message.
//It cleans up my code and hides tedious type conversions.
#define READ_MESSAGE_CASE(tag, fcn, type) \
    case tag: \
        fcn(desc,(type *)message); \
        break;

void readFromBuffer(tDescriptor* desc)
{
    int bytes;
    tMessage *message;
#ifdef DEBUG
    //Save a copy of the buffer in this array so that we can dump it if something
    //goes awry.
    char original[BUFFER_SIZE];
    int originalLength;
    char current[BUFFER_SIZE];
    int currentLength;
    memcpy(original,desc->inBuffer,desc->inBufferTop);
    originalLength=desc->inBufferTop;
#endif
    char buffer[BUFFER_SIZE];

#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return;
#endif

    //Parse data for one message into a message structure:
    while ( desc->connected && (message = tMessage::getMessage(desc->inBuffer,desc->inBufferTop,bytes)) )
    {
#ifdef DEBUG
        memcpy(current,desc->inBuffer,desc->inBufferTop);
        currentLength=desc->inBufferTop;
#endif
        //Shift the remaining data in the in buffer to the left overwriting
        //the current message data:
        desc->inBufferTop-=bytes;
        memcpy(buffer, desc->inBuffer+bytes, desc->inBufferTop);
        memcpy(desc->inBuffer, buffer, desc->inBufferTop);

        try
        {
            switch(message->type)
            {
                //Ignore this message: I only use it to confirm a connection on
                //the connecting computer's side with UDP.
                case MSG_NONE:
                    break;

                READ_MESSAGE_CASE(MSG_RESEND,           onResendMsg,          tResendMsg)
                READ_MESSAGE_CASE(MSG_ACK_PACKETS,      onAckPacketsMsg,      tAckPacketsMsg)
                READ_MESSAGE_CASE(MSG_PACKET_RESENT,    onPacketResentMsg,    tDataMsg)
                READ_MESSAGE_CASE(MSG_REFUSE,           onRefuseMsg,          tRefuseMsg)
                READ_MESSAGE_CASE(MSG_USER_DATA,        onUserDataMsg,        tUserDataMsg)
                READ_MESSAGE_CASE(MSG_HOST_ADDRESS,     onHostAddressMsg,     tHostAddressMsg)
                READ_MESSAGE_CASE(MSG_ASSIGN,           onAssignMsg,          tAssignMsg)
                READ_MESSAGE_CASE(MSG_CONNECT_USER,     onConnectUserMsg,     tConnectUserMsg)
                READ_MESSAGE_CASE(MSG_ID,               onIDMsg,              tIDMsg)
                READ_MESSAGE_CASE(MSG_ADD_USER,         onAddUserMsg,         tAddUserMsg)
                READ_MESSAGE_CASE(MSG_DROP_USER,        onDropUserMsg,        tDropUserMsg)
                READ_MESSAGE_CASE(MSG_DISCONNECT,       onDisconnectMsg,      tMessage)
                READ_MESSAGE_CASE(MSG_TEXT,             onTextMsg,            tTextMsg)
                READ_MESSAGE_CASE(MSG_PING,             onPingMsg,            tPingMsg)
                READ_MESSAGE_CASE(MSG_PONG,             onPongMsg,            tPongMsg)
                READ_MESSAGE_CASE(MSG_FILE_TRANSFER,    onFileTransferMsg,    tFileTransferMsg)
                READ_MESSAGE_CASE(MSG_ABORT_DOWNLOAD,   onAbortDownloadMsg,   tMessage)
                READ_MESSAGE_CASE(MSG_ABORT_UPLOAD,     onAbortUploadMsg,     tMessage)
                READ_MESSAGE_CASE(MSG_FILE_PACKET,      onFilePacketMsg,      tDataMsg)

                READ_MESSAGE_CASE(MSG_SET_TIMING,       onSetTimingMsg,       tSetTimingMsg)
                READ_MESSAGE_CASE(MSG_START_GAME,       onStartGameMsg,       tMessage)
                READ_MESSAGE_CASE(MSG_END_GAME,         onEndGameMsg,         tMessage)
                READ_MESSAGE_CASE(MSG_PAUSE_GAME,       onPauseGameMsg,       tMessage)
                READ_MESSAGE_CASE(MSG_RESUME_GAME,      onResumeGameMsg,      tMessage)
                READ_MESSAGE_CASE(MSG_RESET_PLAYERS,    onResetPlayersMsg,    tMessage)
                READ_MESSAGE_CASE(MSG_CHOOSE_FILES,     onChooseFilesMsg,     tChooseFilesMsg)
                READ_MESSAGE_CASE(MSG_ACK_FILES,        onAckFilesMsg,        tAckFilesMsg)
                READ_MESSAGE_CASE(MSG_SET_PLAYER,       onSetPlayerMsg,       tSetPlayerMsg)
                READ_MESSAGE_CASE(MSG_SYNC,             onSyncMsg,            tSyncMsg)

                //If it's none of these messages, it must be a game message:
                default:
                {
                    receiveMessage(tMessage::duplicateMessage(message));
                    break;
                }
            }
            mwDelete message;
        }
        catch(int e) //event handlers will throw -1 if they suspect the message is corrupt
        {
#ifdef DEBUG
            char logBuffer[10000];
            tBufferIterator ptr(logBuffer, sizeof(logBuffer));
            int i;
            try
            {
                mySprintf(ptr,"Original buffer:");
                for (i=0;i<originalLength;i++)
                {
                    mySprintf(ptr," %d",(unsigned char)original[i]);
                    if(ptr.current-ptr.start>ptr.length-10)
                    {
                        log(ptr.start);
                        ptr.current=ptr.start;
                    }
                }
                if(ptr.current>ptr.start)
                {
                    log(ptr.start);
                    ptr.current=ptr.start;
                }

                mySprintf(ptr,"Current buffer:");
                for (i=0;i<currentLength;i++)
                {
                    mySprintf(ptr," %d",(unsigned char)current[i]);
                    if(ptr.current-ptr.start>ptr.length-10)
                    {
                        log(ptr.start);
                        ptr.current=ptr.start;
                    }
                }
                if(ptr.current>ptr.start)
                {
                    log(ptr.start);
                    ptr.current=ptr.start;
                }
            }
            catch(int x)
            {
                bug("readFromBuffer: buffer overflow");
            }
#endif
            mwDelete message;
        }
    }
}

/***************************************************************************\
writeToBuffer

Writes a message to the out buffer.  The engine will actually send the message
when networkTick is called next.

Inputs:
    desc        Descriptor through which to send the message
    message     Message to send
Outputs:
    none
\***************************************************************************/
void writeToBuffer(tDescriptor *desc, tMessage *message)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return;
    if(MEMORY_VIOLATION(message))
        return;
#endif
    //Package the message into the descriptor's out buffer
    int bytes = message->packageMessage(desc->outBuffer+desc->outBufferTop,
        sizeof(desc->outBuffer)-desc->outBufferTop);

    //Determine if there was enough space to hold the new message:
    if(bytes<=0) //Overflow!
    {
        //If the outBuffer overflows, there is a good chance that something is
        //flooding it.  If the descriptor is connected, then flush the buffer
        //first.
        bug("writeToBuffer: possible flooding detected");
        tMessage::dumpMessage(message);

        //Send data waiting in outBuffer and try again:
        if (desc->connected)
            writeToDescriptor(desc);
        bytes = message->packageMessage(desc->outBuffer+desc->outBufferTop,
            sizeof(desc->outBuffer)-desc->outBufferTop);

        //Now, we're really screwed...
        if(bytes<=0)
        {
            bug("writeToBuffer: buffer overflow; data loss imminent");
            return;
        }
    }

    //Shift the out buffer's top:
    desc->outBufferTop += bytes;
}

/***************************************************************************\
addDescriptor

Create a new tDescriptor object with the specified IP address and port.
Insert this descriptor at the beginning of the descriptor list.

Inputs:
    IP          Descriptor's IP address
    port        Descriptor's port
Output:
    Returns pointer to new tDescriptor object
\***************************************************************************/
tDescriptor* addDescriptor(long IP, int port)
{
    tDescriptor *d = mwNew tDescriptor;
    if(d==NULL)
    {
        bug("addDescriptor: failed to allocate memory for a new descriptor");
        return NULL;
    }

    d->ID = 0;

    //TCPIP
    d->socket = INVALID_SOCKET; //No socket yet

    //UDP
    d->nextPacketIn = 0;        //First packet received will have ID=0
    d->nextPacketOut = 0;       //First packet sent will have ID=0
    d->packetAckCount = 0;      //No packet acknowledgements yet
    d->inPackets = NULL;        //Begin with no back log of incoming packets
    d->outPackets = NULL;       //No outgoing packets to store
    d->outPacketCount = 0;      //No outgoing packets in storage
    d->resendTimer = currentTime;

    *d->inBuffer = '\0';
    d->inBufferTop = 0;
    *d->outBuffer = '\0';
    d->outBufferTop = 0;
    d->connected = false;
    d->peerName = "";
    d->IPAddress = IP;
    d->port = port;
    d->connectTimer = currentTime;
    d->user = NULL;
    d->next = NULL;
    d->pingTime = currentTime;
    d->lag = 0;
    d->reconnect = false;
    d->lastPingID = 0;

    d->bufferFull=false;
    d->bufferFullTimer = currentTime;
    d->fOut=NULL;
    d->fIn=NULL;

    //fileIn and fileOut will initialize themselves.

    d->bytesReceived=0;
    d->bytesSent=0;
    d->destroyed=false;

    d->next = firstDescriptor;
    firstDescriptor = d;
    //onCountDescriptors(countDescriptors());

    return d;
}

/***************************************************************************\
deleteDescriptor

Removes a descriptor from the descriptor list and deallocates memory.  Do not
call this function until you have called closeDescriptor.

Inputs:
    desc            Address of the descriptor to delete
Outputs:
    none
\***************************************************************************/
void deleteDescriptor(tDescriptor *desc)
{
    tDescriptor *d;
#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return;
#endif
    if(firstDescriptor==desc)
        firstDescriptor=desc->next;
    else
    {
        for(d = firstDescriptor; d != NULL; d = d->next)
            if(d->next == desc)
            {
                d->next = desc->next;
                break;
            }
        if(d==NULL)
        {
            bug("deleteDescriptor: descriptor not found in linked list");
            return;
        }
    }
    mwDelete desc;
    //onCountDescriptors(countDescriptors());
}

/***************************************************************************\
newConnection

Call this function when the engine detects a new connection from a new address.
The function treats the connection differently depending on the protocol.
With TCP/IP the function accepts the connection through a new socket, retrieves
the socket's address (IP, port, and peer name), and attaches the socket to a new
descriptor.  With UDP, the function receives the first packet, determines
the packet's source address, and copies the data to a new descriptor.  With
both protocols, the function drops the connection if the host is full and
interprets incoming data.

Inputs:
    none
Outputs:
    Returns true if successful
\***************************************************************************/
bool newConnection()
{
    tDescriptor *desc;
    long IP;
    char buffer[BUFFER_SIZE];
    int port, bytes=sizeof(buffer);
    tSocket socket;
    const char *peerName;
    unsigned char vals[4];
    char str[20];

    if(useTCPIP)
    {
        if(!netAccept(server, IP, port, socket))
            return false;

        //Since we succeeded in accepting the connection, we assume we will
        //also succeed in retrieving the source address.
        netGetAddress(socket,IP,port,peerName);

#ifdef LOG
        unsigned char vals[4];
        longToIP(IP, vals);
        log("New connection detected from IP address %d.%d.%d.%d port %d", vals[0], vals[1], vals[2], vals[3], port);
#endif

        desc = addDescriptor(IP, port);
        desc->socket = socket;
        desc->peerName = peerName;
        desc->connected = true;
    }
    else //UDP
    {
        if(!netRecvFrom(server, IP, port, buffer, bytes, false))
            return false;

        desc = addDescriptor(IP, port);
        desc->connected = true;

        longToIP(IP, vals);
        snprintf(str,sizeof(str),"%d.%d.%d.%d",vals[0], vals[1], vals[2], vals[3]);
        desc->peerName = str;

        readFromUDPDescriptor(desc, buffer, bytes);

#ifdef LOG
        log("New connection detected from IP address %s port %d", str, port);
#endif
    }
    if(countDescriptors() > MAX_DESCRIPTORS)
    {
        tRefuseMsg message(REF_HOST_FULL);
        writeToBuffer(desc, &message);
        closeDescriptor(desc);
        return true;
    }
    //This must be placed after host full check since descriptor might close
    //prematurely.
    if(!useTCPIP)
    {
        if(desc->inBufferTop>0)
            readFromBuffer(desc);
    }
    return true;
}

/***************************************************************************\
closeDescriptor

Cleans up a descriptor in preparation for removal.  The function aborts any
file downloads/uploads and sends any unsent data.  For TCP/IP sockets, the
function closes the socket.  For UDP sockets, the function deletes the
incoming and outgoing packet history.  Lastly, it unlinks the descriptor from
its user (if any), sets its destroyed flag, and compares it with the game host
pointer.  The engine will actually delete the descriptor when you next call
networkTick.

Inputs:
    desc            Desriptor to close
Outputs:
    none
\***************************************************************************/
void closeDescriptor(tDescriptor *desc)
{
    tPacket *p, *next;

#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return;
#endif

    //We must mark the descriptor as destroyed first to prevent recursion.
    desc->destroyed=true;

    //Abort any downloads:
    if(desc->fIn)
        abortDownload(desc);
    //Abort any uploads:
    if(desc->fOut)
        abortUpload(desc);
    if(useTCPIP)
    {
        if(desc->socket != INVALID_SOCKET)
        {
            //Send any unsent data and close out the socket:
            if(desc->outBufferTop > 0 && desc->connected)
                writeToDescriptor(desc);
            netCloseSocket(desc->socket);
        }
    }
    else
    {
        //Send any unsent data:
        if(desc->outBufferTop > 0)
            writeToDescriptor(desc);
        //Clean-up the outgoing packet history:
        for(p=desc->outPackets; p; p=next)
        {
            next=p->next;
            deletePacket(p);
        }
        desc->outPackets=NULL;

        //Clean-up the incoming packet history:
        for(p=desc->inPackets; p; p=next)
        {
            next=p->next;
            deletePacket(p);
        }
        desc->inPackets=NULL;
    }
    //Set the descriptor's flags:
    desc->connected=false;

    //Unlink the descriptor from its user:
    if(desc->user)
    {
        desc->user->descriptor = NULL;
        desc->user=NULL;
    }

    //Ensure the the game host pointer does not reference this descriptor:
    if(gameHost == desc)
        gameHost = NULL;
}

/***************************************************************************\
sessionShutdown

Shuts down a host, client, or single-player session.  The function deallocates
memory, closes sockets, resets state variables, and generates events.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
void sessionShutdown()
{
    tDescriptor *d;
    while(firstUser)
        deleteUser(firstUser);
    localUser = NULL;
    isGameHost = false;
    isGameClient = false;
    networkGame = false;

    if(networkActive)
    {
        for(d=firstDescriptor;d;d=d->next)
            closeDescriptor(d);
        if(server != INVALID_SOCKET)
            netCloseSocket(server);
        networkPort = 0;
        gameHost = NULL;
        peerName = "";
        networkIP=0;
        listening = false;
        packetsSent=0;
        packetsRecv=0;
        packetsResent=0;
        packetsRerecv=0;
    }

    //Generate a user-interface event:
    onDisconnect();
    //Generate an engine event:
    onSessionShutdown();
}

/***************************************************************************\
countDescriptors

Counts total descriptors in engine.

Inputs:
    none
Outputs:
    Number of descriptors
\***************************************************************************/
int countDescriptors()
{
    int count = 0;
    tDescriptor *d;
    for(d=firstDescriptor; d!=NULL; d=d->next)
        count++;
    return count;
}

/***************************************************************************\
countUsers

Counts total users in engine.

Inputs:
    none
Outputs:
    Number of users
\***************************************************************************/
int countUsers()
{
    int count = 0;
    tUser *u;
    for(u=firstUser; u!=NULL; u=u->next)
        count++;
    return count;
}

/***************************************************************************\
nextID

Generates a unique ID to avoid conflicts with dropped users who try to reconnect.

Inputs:
    none
Outputs:
    Returns unique ID
\***************************************************************************/
int nextID()
{
    static int _nextID=2;
    return _nextID++;
}

/***************************************************************************\
connectionEstablished

Call this function to send preliminary data once the engine has successfully
connected to a peer.  If a client connects to the host for the first time,
the function sends a user data message.  Otherwise, the function sends an
ID message.

Inputs:
    desc        Descriptor on which a connection has been established
Outputs:
    none
\***************************************************************************/
void connectionEstablished(tDescriptor *desc)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return;
#endif
    if(localUser == NULL) //Connecting to game host
    {
        tUserDataMsg message(VERSION_CODE, globalName);
        writeToBuffer(desc, &message);
    }
    else //Connecting to a peer
    {
        /* Added 12/23/01: This is a hack for UDP.  This leader identifies the
         * peer by its ID.  When the tick function receives a packet from
         * unknown origin, it looks for this special signature.  If found, it
         * searches for an existing descriptor with the same ID and copies the
         * remainder of the packet into this descriptor's inBuffer.  This way,
         * the engine doesn't create a temporary descriptor for the new
         * connection and foul up the packet management system.
         */
        if(!useTCPIP)
        {
            char buf[5] = {255, 255, 255, 255, (char) localUser->ID};
            int len=5;
            netSendTo(server,desc->IPAddress,desc->port,buf,len);
        }

        /* This message introduces the peer.  When the remote computer receives
         * this message, it will search the user list for a matching ID.  If
         * it finds one, it will link the descriptor with the user.  Otherwise,
         * it will await a formal introduction from the game host.
         */
        tIDMsg message(localUser->ID);
        writeToBuffer(desc, &message);

        //Resend all outgoing data.  There should only be outgoing data if peer
        //reconnects.
        if(useTCPIP)
        {
            if(desc->outBufferTop>0)
                writeToDescriptor(desc);
        }
        else
            resendPackets(desc);
    }
}

/***************************************************************************\
connectionLost

This is a general-purpose routine for gracefully handling any kind of network
error.  In most cases, one peer will attempt to reconnect to the other.  The
two exceptions are (1) a client hasn't successfully connect to the host or
(2) a peer hasn't successfully connected to another peer.

Inputs:
    desc        The descriptor which lost its connection
Outputs:
    none
\***************************************************************************/
void connectionLost(tDescriptor *desc)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return;
#endif
    //Determine if the descriptor has already been destroyed, in which case we
    //don't need to take any action.
    if(desc->destroyed)
        return;

    if(localUser == NULL)     //If client hasn't connected to host, then abort
    {
        log("Failed to connect to host (descriptor #%d)",desc->ID);
        sessionShutdown();
		onNetworkError();
    }
    else if(desc->user == NULL) //Drop connections from unknown peers
    {
        log("Connection to unknown peer lost (descriptor #%d)",desc->ID);
    	closeDescriptor(desc);
    }
    else                        //descriptor with greatest ID reconnects
    {
        //Reconnect will time-out after certain duration:
        desc->connectTimer = currentTime;
        desc->connected = false;

        //Close expired socket IMMEDIATELY:
        if(useTCPIP && desc->socket != INVALID_SOCKET)
            netDestroySocket(desc->socket);

        //Descriptor with greatest ID reconnects:
        //Changed 1/1/03: Only reconnect with TCP/IP since we can't possibly
        //hope to reestablish a connection with UDP if a socket error occured.
        //The player will need to click "Reconnect" with UDP.
        if(useTCPIP && localUser->ID > desc->user->ID)
            desc->reconnect = true;
    }
}

/***************************************************************************\
broadcast

Broadcasts a message to all users.  The function calls writeToBuffer for
each user with a descriptor (hence excluding yourself).

Inputs:
    message     Message to broadcast
Outputs:
    none
\***************************************************************************/
void broadcast(tMessage *message)
{
    tUser *u;
#ifdef DEBUG
    if(MEMORY_VIOLATION(message))
        return;
#endif
    for(u=firstUser; u!=NULL; u=u->next)
        if(u->descriptor)
            writeToBuffer(u->descriptor, message);
}

/***************************************************************************\
addUser

Create a new tUser object with the specified name, ID, and descriptor.  Insert
this user into the user list in alphabetical order.

Inputs:
    name        User's name
    ID          User's ID
    d           User's descriptor (or NULL if user does not have one)
Output:
    Returns pointer to new tUser object
\***************************************************************************/
tUser* addUser(const tString& name, int ID, tDescriptor *d)
{
    tUser *user = mwNew tUser;
    if(user==NULL)
    {
        bug("addUser: failed to allocate memory for a new user");
        return NULL;
    }

    user->ID = ID;
    user->name = name;
    user->descriptor = d;
    user->haveMap = false;
    user->haveWorld = false;
    user->ackReceived = false;
    user->next = NULL;

    if(firstUser == NULL || strcmp(CHARPTR(user->name), CHARPTR(firstUser->name)) < 0)
    {
        user->next = firstUser;
        firstUser = user;
    }
    else
    {
        tUser *u;
        for(u = firstUser; u->next && strcmp(CHARPTR(user->name), CHARPTR(u->next->name)) < 0; u=u->next)
        ;
        user->next = u->next;
        u->next = user;
    }
    //onCountUsers(countUsers());

    return user;
}

/***************************************************************************\
deleteUser

Removes a user from the user list and deallocates memory.  If the user has a
descriptor, then the function closes its descriptor also.

Inputs:
    user            User to delete
Outputs:
    none
\***************************************************************************/
void deleteUser(tUser *user)
{
    tUser *u;
#ifdef DEBUG
    if(MEMORY_VIOLATION(user))
        return;
#endif
    //Remove user from user list:
    if(firstUser==user)
    {
        firstUser=user->next;
    }
    else
    {
        for(u = firstUser; u != NULL; u = u->next)
            if(u->next == user)
            {
                u->next = user->next;
                break;
            }
        if(u==NULL)
            bug("deleteUser: user not found in linked list");
    }
    //Close out the user's descriptor if one exists:
    if(user->descriptor)
        closeDescriptor(user->descriptor);

    //Generate engine event before memory deallocated even though event should
    //not dereference the pointer:
    onDeleteUser(user);

    //Deallocate memory:
    mwDelete user;

    //onCountUsers(countUsers());
}

/***************************************************************************\
dropUser

Drops a user from the game.  You must be the game host to call this function.
The function broadcasts the action, then generates an event and deletes the user.

Inputs:
    user        User to drop
Outputs:
    Returns true if successful
\***************************************************************************/
bool dropUser(tUser *user)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(user))
        return false;
#endif
    if(localUser == user || !isGameHost)
        return false;
    else
    {
        //Generate a user-interface event:
        onDropUser(user);

        //Generate an engine event:
        onClientDisconnect(user);

        tDropUserMsg message(user->ID);
        broadcast(&message);

        deleteUser(user);
    }
    return true;
}

/***************************************************************************\
disconnect

Disconnects from a network or single-player session.  If you're hosting a game,
the function informs your clients that the host has left.  If you're connected
to a game as a client, the function sends a disconnect request to the host.  In
any case, the function shuts down the engine with sessionShutdown.
\***************************************************************************/
bool disconnect()
{
    if(isGameHost)
    {
        //Inform clients that the host has left:
        tDropUserMsg message(localUser->ID);
        broadcast(&message);
    }
    else if (gameHost != NULL && gameHost->connected)
    {
        //Send disconnect request to host:
        tMessage message(MSG_DISCONNECT);
        writeToBuffer(gameHost, &message);
    }
    //That's all folks!
    sessionShutdown();
    return true;
}

/***************************************************************************\
sendText

Broadcasts a text message to all peers.

Inputs:
    text        Null-terminated string
Outputs:
    none
\***************************************************************************/
void sendText(const char *text, eTextMessage type)
{
    tTextMsg message(text,type);
    broadcast(&message);
}

/***************************************************************************\
sendTextToUser

Sends a text message to a remote user (having a descriptor).

Inputs:
    text        Null-terminated string
Outputs:
    none
\***************************************************************************/
void sendTextToUser(tUser *user, const char *text, eTextMessage type)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(user))
        return;
    if(MEMORY_VIOLATION(user->descriptor))
        return;
#endif
    tTextMsg message(text,type);
    writeToBuffer(user->descriptor, &message);
}

/***************************************************************************\
userLookup

Finds a user with a matching ID

Inputs:
    ID          Unique identifier
Outputs:
    Returns matching user
\***************************************************************************/
tUser *userLookup(int ID)
{
    for(tUser *u = firstUser; u != NULL; u=u->next)
        if(u->ID == ID)
            return u;
    return NULL;
}

/***************************************************************************\
descriptorLookup

Finds a descriptor with a matching ID

Inputs:
    ID          Unique identifier
Outputs:
    Returns matching descriptor
\***************************************************************************/
tDescriptor *descriptorLookup(int ID)
{
    for(tDescriptor *d = firstDescriptor; d != NULL; d=d->next)
        if(d->ID == ID)
            return d;
    return NULL;
}

/***************************************************************************\
sendFile

Initiates a file transfer with a peer.  The function attempts to open the
specified file, retains file details in the descriptor, notifies the peer
it's about to receive a file, and generates an event.

Inputs:
    desc        Recipient
    f           File details
Outputs:
    Returns true if successful
\***************************************************************************/
bool sendFile(tDescriptor *desc, tFile& f)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return false;
#endif
    //Removed 5/11/03:
    //There's no need to generate an error.  If we're already sending a file,
    //we assume that the new file is more important, so we abort the previous upload.
    //bug("sendFile: already sending file %s", CHARPTR(desc->filenameOut));

    if(desc->fOut)
    {
        //Well, whaddaya know?  We're already sending the file, so no further action is needed.
        if(desc->fileOut==f)
            return true;
        else //Otherwise, abort the previous upload.
            abortUpload(desc);
    }
    
    //Attempt to open the file for reading in binary mode:
    desc->fOut = f.open("rb");
    if(desc->fOut==NULL)
    {
        bug("sendFile: failed to open file %s", CHARPTR(f.getFullName()));
        return false;
    }
    //Store the file details in the descriptor:
    desc->fileOut = f;
    desc->bytesSent = 0;

    //Notify the peer that it's about to receive a file:
    tFileTransferMsg message(f);
    writeToBuffer(desc, &message);

    //Generate an event:
    onUploadFile(desc);
    return true;
}

/***************************************************************************\
abortDownload

Aborts a file transfer with a peer.  The function closes and removes the
incoming file.  It instructs the peer to abort the file transfer and generates
an event.

Inputs:
    desc        Peer with which to abort file transfer
Outputs:
    Returns true if successful
\***************************************************************************/
bool abortDownload(tDescriptor *desc)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return false;
    if(desc->fIn == NULL)
    {
        bug("abortDownload: not downloading file in this descriptor");
        return false;
    }
#endif
    //Close the file which we are receiving:
    fclose(desc->fIn);
    desc->fIn = NULL;

    //Inform the peer that we have aborted the download (hence, the peer
    //should abort its upload).
    tMessage message(MSG_ABORT_UPLOAD);
    writeToBuffer(desc, &message);

    //Generate an event:
    onAbortDownload(desc);

    //Remove the file and clear fileIn AFTER we generate the event:
    desc->fileIn.remove();
    return true;
}

/***************************************************************************\
abortUpload

Aborts a file transfer with a peer.  The function closes the file, instructs
the peer to cancel the download, and generates an event.

Inputs:
    desc        Peer with which to abort file transfer
Outputs
    Returns true if successful
\***************************************************************************/
bool abortUpload(tDescriptor *desc)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return false;
    if(desc->fOut == NULL)
    {
        bug("abortUpload: not uploading file in this descriptor");
        return false;
    }
#endif

    //Close the file which we are sending:
    fclose(desc->fOut);
    desc->fOut = NULL;

    //Inform the peer that we have aborted the upload (hence, the peer should
    //abort its download).
    tMessage message(MSG_ABORT_DOWNLOAD);
    writeToBuffer(desc, &message);

    //Generate an event:
    onAbortUpload(desc);

    desc->fileOut.clear(); //Clear the tFile structure for good measure AFTER we generate the event
    return true;
}

/***************************************************************************\
getLocalIPs

Starts up the network if necessary and uses netGetLocalIPs to retrieve a
static double-array of local IP addresses available on this system.

Inputs:
    none
Outputs:
    Returns a null-terminated ("") double-array of local IP addresses
\***************************************************************************/
const char **getLocalIPs()
{
    if(!networkActive && !networkStartup())
        return NULL;

    return netGetLocalIPs();
}

/***************************************************************************\
getLocalName

Starts up the network is necessary and uses netGetLocalName to retrieve the
local name.  For example:  phnx.tnt131.mindspring.com

Inputs:
    none
Outputs:
    Returns a pointer to a static null-terminated string
\***************************************************************************/
const char *getLocalName()
{
    if(!networkActive && !networkStartup())
        return NULL;

    return netGetLocalName();
}

/***************************************************************************\
userLookup

Finds a user with a matching name.  The search is case-insensitive.
The function always searches for an exact match first, to resolve the unfortunate
situation where the name Delta17 precedes Delta.

Inputs:
    name        Name to search for
    exact       If true, the function searches for an exact match.
                If false, the function searches for a user name with the specified
                    prefix.
Outputs:
    Returns a user pointer if successful
    Returns NULL if a suitable match was not found
\***************************************************************************/
tUser *userLookup(const tString& name, bool exact)
{
    tString s=STRTOLOWER(name);
    for(tUser *u=firstUser; u; u=u->next)
        if(STREQUAL(STRTOLOWER(u->name),s))
            return u;
    if(exact)
        return NULL;
    for(tUser *u=firstUser; u; u=u->next)
        if(STRPREFIX(s, u->name)) //STRPREFIX is case-insensitive
            return u;
    return NULL;
}

/***************************************************************************\
simulateError

Simulates a connection error (testing only).

Inputs:
    desc        Descriptor to experience error
Outputs:
    none
\***************************************************************************/
void simulateError(tDescriptor *desc)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(desc))
        return;
#endif
    log("Simulating error in descriptor #%d", desc->ID);
    connectionLost(desc);
}

/***************************************************************************\
isUsernameValid

Determines if a username is valid.  A valid username has at least one character
and does not have any spaces.

Inputs:
    username        Username to check for validity
Outputs:
    Returns true if the username is valid
\***************************************************************************/
bool isUsernameValid(const char *username)
{
    return username && *username && !strchr(username,' ');
}

/***************************************************************************\
abortAllUploads

Aborts all file transfers with peers.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
void abortAllUploads()
{
    for(tUser *u=firstUser; u; u=u->next)
        if(u->descriptor && u->descriptor->fOut) //you're sending client another file
            abortUpload(u->descriptor);
}
/***************************************************************************\
sendToHost

Sends a message to the game host.

Inputs:
    message         Message to send
Outputs:
    none
\***************************************************************************/
void sendToHost(tMessage *message)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(gameHost))
        return;
    if(MEMORY_VIOLATION(message))
        return;
#endif
    writeToBuffer(gameHost, message);
}



/***************************************************************************\
onResendMsg (message handler)

Called when a peer loses one or more packets.  Searches for the requested packets
in the archived sent packets.  Marks these packets for resending.
\***************************************************************************/
void onResendMsg(tDescriptor *desc, tResendMsg *message) throw(int)
{
    tPacket *p;
    //Locate the first packet we need to resend:
    for(p=desc->outPackets; p; p=p->next)
        if(p->ID == message->lastID)
            break;
    //Mark the subsequent packets for resending until we reach the last one:
    for( ; p; p=p->next)
    {
        p->resend = true;
        packetsResent++;
        if(p->ID == message->firstID)
            break;
    }
    //If we haven't found any packets, record a warning message:
    if(p==NULL)
    {
        bug("onResendMsg: messages #%d - #%d not found in linked list",
            message->firstID, message->lastID);
        tMessage::dumpMessage(message);
        throw -1;
    }
}

/***************************************************************************\
onAckPacketsMsg (message handler)

Called when a peer acknowledges that it has received several packets.  After
this acknowledgement, there is no reason to keep the archived sent packets
any longer, so the function deletes them.
\***************************************************************************/
void onAckPacketsMsg(tDescriptor *desc, tAckPacketsMsg *message) throw(int)
{
    tPacket *p, *next;
    int i;

    //Iterate through the array of packet IDs:
    for(i=0; i<ACK_INTERVAL; i++)
    {
        //If the outgoing packet list is empty, but we still have more packets
        //to acknowledge (remove), we're in trouble.
        if(desc->outPackets == NULL)
        {
            bug("onAckPacketsMsg: desc->outPackets == NULL");
            throw -1;
        }
        //Search specifically for a match with the first packet, since
        //this requires a different linked list operation.
        if(desc->outPackets->ID == message->IDs[i])
        {
            next = desc->outPackets->next;
            deletePacket(desc->outPackets);
            desc->outPackets = next;
            desc->outPacketCount--;
        }
        else
        {
            bool found = false;
            //Iterate through the archived packets looking for a match:
            for(p = desc->outPackets; p->next; p = p->next)
                if(p->next->ID == message->IDs[i])
                {
                    next = p->next->next;
                    deletePacket(p->next);
                    p->next = next;
                    desc->outPacketCount--;
                    found = true;
                    break;
                }
            //If we didn't find a match, we're in trouble.
            if(!found)
            {
                bug("onAckPacketsMsg: couldn't find message #%d in linked list",
                    message->IDs[i]);
                throw -1;
            }
        }
    }
}

/***************************************************************************\
onPacketResentMsg (message handler)

Processes a packet that a peer sent to you a second time.  Resent packets might
be nested, meaning a resent packet might contain another resent packet.
\***************************************************************************/
void onPacketResentMsg(tDescriptor *desc, tDataMsg *message) throw(int)
{
    readFromUDPDescriptor(desc, message->data, message->bytes);
    packetsRerecv++;
}

/***************************************************************************\
onRefuseMsg (message handler)

Called when a peer refuses your connection.  Aborts the connection and
generates an event containing the reason for refusal.
\***************************************************************************/
void onRefuseMsg(tDescriptor *desc, tRefuseMsg *message) throw(int)
{
    sessionShutdown();
    onConnectionRefused(message->code);
}

/***************************************************************************\
onUserDataMsg (message handler)

Called in response to the first message sent by a new peer.  The host
will negotiate with the new peer and add the client to the network.  A
client will redirect the peer to the network host.

NOTE:
If a user connects during the middle of the game, he can chat with other users
but can't participate in the game.  He will be eligible to participate in the
next game, however.
\***************************************************************************/
void onUserDataMsg(tDescriptor *desc, tUserDataMsg *message) throw(int)
{
    tUser *u, *user;
    if(isGameHost)
    {
        //Ensure the client is running the same version as the host:
        if(message->version!=VERSION_CODE)
        {
            //Refuse the connection and drop the person:
            tRefuseMsg message(REF_OLD_VERSION);
            writeToBuffer(desc, &message);
            closeDescriptor(desc);
            return;
        }
        //Ensure that no one else is using the same username:
        u=userLookup(message->name,/*exact=*/true);
        if(u)
        {
            //Refuse the connection and drop the person:
            tRefuseMsg message(REF_NAME_USED);
            writeToBuffer(desc, &message);
            closeDescriptor(desc);
            return;
        }
        //Create a new user object for the user:
        user = addUser(message->name, nextID(), desc);
        //Give the descriptor the same ID as the user:
        desc->ID = user->ID;
        //Link the user to the descriptor:
        desc->user = user;

        //Assign the user a unique ID:
        tAssignMsg message1(user->ID);
        writeToBuffer(desc, &message1);

        //Tell connecting computer what your ID is:
        tIDMsg message2(localUser->ID);
        writeToBuffer(desc, &message2);

        //Transmit your user data to connecting computer:
        tAddUserMsg message3(localUser);
        writeToBuffer(desc, &message3);

        //Instruct connecting computer to connect to each user.  You supply
        //the name and address of each:
        for(u = firstUser; u != NULL; u = u->next)
            if(u != user && u->descriptor)
            {
                tConnectUserMsg message4(u, u->descriptor->IPAddress, u->descriptor->port);
                writeToBuffer(desc, &message4);
            }

        //Instruct each existing user to add the connecting user.  It's the
        //connecting user's responsibility to establish a connection, however.
        tAddUserMsg message5(user);
        for(u = firstUser; u != NULL; u = u->next)
            if(u != user && u->descriptor)
                writeToBuffer(u->descriptor, &message5);

        //Generate a user-interface event:
        onAddUser(user, false); //User-interface adds connecting user's name
        //Generate an engine event:
        onClientConnect(user);
    }
    else if(gameHost && localUser) //I added localUser so that we will not direct peer to non-existant host.
    {
        //If you're not the host, redirect the connecting computer to the host:
        tHostAddressMsg message(gameHost->IPAddress, gameHost->port);
        writeToBuffer(desc, &message);
        closeDescriptor(desc);
    }
    else
        closeDescriptor(desc);  //Safe-guard
}

/***************************************************************************\
onHostAddressMsg (message handler)

We just got redirected.  Shutdown the network and connect to the network host.
\***************************************************************************/
void onHostAddressMsg(tDescriptor *desc, tHostAddressMsg *message) throw(int)
{
    //Save the old listening address since these values will be lost after
    //calling sessionShutdown.
    long oldIP=networkIP;
    int oldPort=networkPort;
    sessionShutdown();
    //Connect to the address in the message:
    connectToHost(message->IPAddress, message->port, CHARPTR(globalName), useTCPIP, oldIP, oldPort);
}

/***************************************************************************\
onAssignMsg (message handler)

Called when the host assigns you an ID.  Creates a local user object with your
username and the specified ID.  Naturally, the user does not have a descriptor.
Generates events.
\***************************************************************************/
void onAssignMsg(tDescriptor *desc, tAssignMsg *message) throw(int)
{
    localUser = addUser(globalName, message->ID, NULL);

    //Connection successful:
    onConnect(desc);
    //Connecting user adds himself:
    onAddUser(localUser, true);
    onSessionStartup();
}

/***************************************************************************\
onConnectUserMsg (message handler)

The host instructs you to connect to a remote user.  If the connection fails
right away, then abort the connection process.  Otherwise, attach a user
object to the new descriptor with the proper name and ID.  Generate an event.
\***************************************************************************/
void onConnectUserMsg(tDescriptor *desc, tConnectUserMsg *message) throw(int)
{
    tDescriptor *d;
    tUser *user;
    connectToPeer(message->IPAddress, message->port, &d);
    if(d==NULL)
    {
        //Connection failed:
        sessionShutdown();
        onNetworkError();
    }
    else
    {
        //Connection successful or still in progress:
        user = addUser(message->user.name, message->user.ID, d);
        d->ID = user->ID;
        d->user = user;

        //Connecting user adds another users's name:
        onAddUser(user, true);
    }
}

/***************************************************************************\
onIDMsg (message handler)

Called when a peer introduces himself to you.  The purpose of the message
is to label this descriptor with the provided ID and attach it to an
existing user with the same ID.  If a user with this ID does not exist,
we wait for the network host to "introduce" the peer.  A special case results
when the user object already has a descriptor attached to it.  In this case,
we assume that the peer is reconnecting from a new address.  We copy the
socket handle to the old descriptor and discard the new descriptor.
\***************************************************************************/
void onIDMsg(tDescriptor *desc, tIDMsg *message) throw(int)
{
    //Determine if a descriptor with the ID already exists:
    tDescriptor *d = descriptorLookup(message->ID);
    if (d)
    {
        //If there is another descriptor assigned to the ID, then extract the
        //pertinent data from the new descriptor (desc) and store it in the old
        //descriptor (d).  Copy the contents of the inBuffer and close the new
        //descriptor.
        if (d != desc)
        {
            if(useTCPIP)
            {
                if(d->socket != INVALID_SOCKET)
                    netCloseSocket(d->socket);
                d->socket = desc->socket;
                desc->socket = INVALID_SOCKET;
            }

            d->port = desc->port;
            d->IPAddress = desc->IPAddress;
#ifdef LOG
            unsigned char vals[4];
            longToIP(desc->IPAddress,vals);
            log("Assigning IP address %d.%d.%d.%d and port %d to descriptor #%d",
                vals[0],vals[1],vals[2],vals[3], desc->port, d->ID);
#endif
            d->connected = desc->connected;
            d->reconnect = false; //Make no further attempts to reconnect.

            //Copy contents of inBuffer before closing the descriptor in case
            //additional data follows this message.
            if(d->inBufferTop+desc->inBufferTop>BUFFER_SIZE)
                bug("onIDMsg: buffer overflow");
            else
            {
                memcpy(d->inBuffer+d->inBufferTop,desc->inBuffer,desc->inBufferTop);
                d->inBufferTop+=desc->inBufferTop;
            }
            closeDescriptor(desc);
        }

        //If a peer reconnects, send any outgoing data immediately.
        //(TCP/IP: I assume that the peer received none of the data in the outBuffer
        //since netSend failed; UDP: A few redundant packets won't hurt anything)
        if(useTCPIP)
        {
            if(d->outBufferTop>0)
                writeToDescriptor(d);
        }
        else
            resendPackets(d);
    }
    //If a peer sends an ID message to the host, it must be trying to reconnect.
    //If the peer no longer has a descriptor on the game host, the game host probably
    //dropped it (in any case, the peer has been removed fromf the game).  So, refuse
    //the connection with an appropriate message.
    else if(isGameHost)
    {
        tRefuseMsg message(REF_DROPPED);
        writeToBuffer(desc, &message);
        closeDescriptor(desc);
    }
    //You're a peer.  This is part of the normal connection process.
    else
    {
        //Label the descriptor and attach to it a user with a matching ID:
        desc->ID = message->ID;
        desc->user = userLookup(desc->ID);
        if(desc->user)
            desc->user->descriptor = desc;

        //Let the remote computer know we established a connection:
        tMessage message1(MSG_NONE);
        writeToBuffer(desc, &message1);
    }
}

/***************************************************************************\
onAddUserMsg (message handler)

The host instructs you to add a new user.  If a descriptor exists with the same
ID, link the new user structure to it.  Generate an event.
\***************************************************************************/
void onAddUserMsg(tDescriptor *desc, tAddUserMsg *message) throw(int)
{
    //Create a new user object for the user:
    tUser *user = addUser(message->user.name, message->user.ID, NULL);

    //Search for a corresponding descriptor:
    user->descriptor = descriptorLookup(user->ID);
    if(user->descriptor)
        user->descriptor->user = user;

    //Existing user adds connecting user's name OR connecting user adds
    //host's name.
    onAddUser(user, user->descriptor == gameHost ? true : false);
}

/***************************************************************************\
onDropUserMsg (message handler)

The host instructs you to drop (delete) a user.  Called in response to one
of three events: (1) The host drops another user from the game; (2) The host drops
you from the game; (3) The host leaves.  The function deletes the user in the
former case or ends the session in the latter cases.  It then generates the
appropriate event.
\***************************************************************************/
void onDropUserMsg(tDescriptor *desc, tDropUserMsg *message) throw(int)
{
    //You were dropped from the game:
    if(localUser && localUser->ID == message->ID)
    {
        sessionShutdown();
        onDropUser(localUser);
    }
    //The host left:
    else if(gameHost && gameHost->user && message->ID == gameHost->user->ID)
    {
        sessionShutdown();
        onHostLeft();
    }
    //The host dropped someone else from the game:
    else
    {
        tUser *user = userLookup(message->ID);
        if(user)
        {
            onDropUser(user);
            deleteUser(user);
        }
    }
}

/***************************************************************************\
onDisconnectMsg (message handler)

Called when client wishes to disconnect.  Delete the user, inform the other
clients, and generate appropriate events.
\***************************************************************************/
void onDisconnectMsg(tDescriptor *desc, tMessage *message) throw(int)
{
    if(desc->user)
    {
        //Generate a user-interface event before the user object is destroyed:
        onDropUser(desc->user);

        //Generate an engine event:
        onClientDisconnect(desc->user);

        //Inform the other clients:
        tDropUserMsg message2(desc->user->ID);
        broadcast(&message2);

        deleteUser(desc->user);
    }
}

/***************************************************************************\
onTextMsg (message handler)

Called when text message received.  Pass the text to the user-interface.
\***************************************************************************/
void onTextMsg(tDescriptor *desc, tTextMsg *message) throw(int)
{
    onReceiveText(CHARPTR(message->text), message->textType);
}

/***************************************************************************\
onPingMsg (message handler)

Called when a peer sends a ping.  Reply with a pong!
\***************************************************************************/
void onPingMsg(tDescriptor *desc, tPingMsg *message) throw(int)
{
    tPongMsg message1(message->ID);
    writeToBuffer(desc, &message1);
}

/***************************************************************************\
onPongMsg (message handler)

Called when a peer send a pong.  Use the time elapsed since the ping was sent
to calculate the peer's lag.  Generate an event.
\***************************************************************************/
void onPongMsg(tDescriptor *desc, tPongMsg *message) throw(int)
{
    //If the pong ID doesn't match that of the last ping sent (e.g. it's overdue)
    //then discard it.  This occurs only if lag > LAG_INTERVAL.
    if(message->ID != desc->lastPingID)
        return;
    desc->lag = (currentTime - desc->pingTime) / 2; //calculate lag
    if(desc->user)
        onUpdateLag(desc->user);
}

/***************************************************************************\
onFileTransferMsg (message handler)

Called when a peer wants to transfer a file.  Open a file in the
download directory (create it if necessary).  Record the file details in the
descriptor.  Generate an event.
\***************************************************************************/
void onFileTransferMsg(tDescriptor *desc, tFileTransferMsg *message) throw(int)
{
    //Ensure that we aren't already receiving a file from this peer:
    if(desc->fIn)
    {
        bug("onFileTransferMsg: already receiving file %s", CHARPTR(desc->fileIn.name));
        tMessage message1(MSG_ABORT_UPLOAD);
        writeToBuffer(desc, &message1);
        return;
    }
    //Verify that there is a download subdirectory:
    my_mkdir(DOWNLOAD_PATH);
    //mkdir will return -1 if the directory has already been created, but I don't care.

    //Record file details in the descriptor:
    desc->fileIn = message->file;
    desc->bytesReceived = 0;

    //We don't use tFile::open since we don't want to update the size and time.
    desc->fIn = fopen(CHARPTR(desc->fileIn.getFullName()),"wb");

    if(desc->fIn==NULL)
    {
        bug("onFileTransferMsg: unable to open file %s", CHARPTR(message->file.name));
        tMessage message1(MSG_ABORT_UPLOAD);
        writeToBuffer(desc, &message1);
        return;
    }

    onDownloadFile(desc);
}

/***************************************************************************\
onAbortDownloadMsg (message handler)

Called when a peer wants to cancel the file transfer.  Close the file,
remove it, and generate an event.
\***************************************************************************/
void onAbortDownloadMsg(tDescriptor *desc, tMessage *message) throw(int)
{
    if(desc->fIn==NULL)
    {
        bug("onAbortDownloadMsg: no file download to abort");
        return;
    }
    fclose(desc->fIn);
    desc->fIn = NULL;
    onAbortDownload(desc);
    //Remove the file and clear the tFile structure AFTER we generate the event:
    desc->fileIn.remove();
}

/***************************************************************************\
onAbortUploadMsg (message handler)

Called when a peer wants to cancel the file transfer.  Close the file
and generate an event.
\***************************************************************************/
void onAbortUploadMsg(tDescriptor *desc, tMessage *message) throw(int)
{
    if(desc->fOut==NULL)
    {
        bug("onAbortUploadMsg: no file upload to abort");
        return;
    }
    fclose(desc->fOut);
    desc->fOut = NULL;
    onAbortUpload(desc);
    //Clear the fFile structure for good measure AFTER we generate the event:
    desc->fileOut.clear();
}

/***************************************************************************\
onFilePacketMsg (message handler)

Called when a peer sends the next chunk of a file.  Appends the chunk to the
end of the file.  If this was the last chunk, the function closes the file,
stamps it with the correct date, and generates the appropriate events.
\***************************************************************************/
void onFilePacketMsg(tDescriptor *desc, tDataMsg *message) throw(int)
{
    if(desc->fIn==NULL)
    {
        bug("onFilePacketMsg: file packet received but not downloading file");
        return;
    }
    //Append the chunk to the end of the file:
    int bytesWritten = fwrite( message->data, 1, message->bytes, desc->fIn );
    if(bytesWritten < message->bytes)
        bug("onFilePacketMsg: error writing packet to file");
    desc->bytesReceived += message->bytes;

    //Determine if we have finished receiving the file:
    if(desc->bytesReceived >= desc->fileIn.size)
    {
        //File's time must be set after writing data; otherwise the time will be
        //replaced with current time
        fclose(desc->fIn);

        tString fullName=desc->fileIn.getFullName();

        //We don't use tFile::open here since we don't want to update the size and time.
        desc->fIn=fopen(CHARPTR(fullName),"r+b");

        if(desc->fIn==NULL)
            bug("onFilePacketMsg: error opening file %s", CHARPTR(fullName));
        else
        {
            //Stamp the file with the correct date:
            utimbuf tb={desc->fileIn.time, desc->fileIn.time};
//Argh!!  Compiler nuances are a pain!
#ifdef VISUALC
            _utime(CHARPTR(fullName),(_utimbuf*)&tb);
#else
            _utime(CHARPTR(fullName),&tb);
#endif
            fclose(desc->fIn);
            desc->fIn = NULL;
        }

        //Generate a user-interface event:
        onDownloadComplete(desc);
        //Generate an engine event:
        onFileReceived(desc);

        desc->fileIn.clear(); //Clear the tFile structure for good measure AFTER we generate events
    }
}


