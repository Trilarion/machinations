/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* COMBMOD.CPP:    Combines two or more Machinations models into a single    *
*                 model.                                                    *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include <condefs.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include <vector.h>
#pragma hdrstop
//---------------------------------------------------------------------------
#pragma argsused

const long machModelCode='M'+('A'<<8)+('C'<<16)+('H'<<24);

struct tVector3D
{
    float x;
    float y;
    float z;
    float radius()
    {
        return sqrt(x*x+y*y);
    }
};

struct tVector2D
{
    float x;
    float y;
};

struct LTriangle
{
    unsigned short a;
    unsigned short b;
    unsigned short c;
};

void addExtension(char *filename, const char *ext)
{
    char *ptr=filename;
    char *start=filename;
    while(*ptr!='\0')
    {
        if(*ptr=='/' || *ptr=='\\')
            start=ptr+1;
        ptr++;
    }

    ptr=strchr(start, '.');
    if(ptr==NULL)
        strcat(filename, ext);
}

void main(int argc, char* argv[])
{
    char filename[300];
    char outputFilename[300]="output.mod";
    int i, j, vertexCount, triangleCount, vertexBase;
    FILE *fp;
    vector<tVector3D> vertices;
    vector<tVector3D> normals;
    vector<tVector2D> texCoords;
    vector<LTriangle> triangles;
    tVector3D v3;
    tVector2D v2;
    LTriangle tri;
    long l;
    float minx=1000000,miny=1000000,minz=1000000;
    float maxx=-1000000,maxy=-1000000,maxz=-1000000;
    float radius=0;

    if(argc==1)
    {
        printf("Usage: combmod <filename> ... [-o <output file>]\n");
        printf("    This utility combines two or more Machinations .mod\n");
        printf("    models into a single model.  Use this utility in conjunction\n");
        printf("    with 'convmod' to break apart .3DS models and group the pieces\n");
        printf("    into one or more components (e.g. chassis, turret, and barrel.)\n");
        printf("    Options:\n");
        printf("      -o - Specifies the name of the output file.\n");
        printf("           'output.mod' is the default.\n");
        return;
    }

    for(i=1; i<argc; i++)
    {
        if(*argv[i]=='-' && strlen(argv[i])==2)
            break;

        strcpy(filename, argv[i]);
        addExtension(filename, ".mod");

        fp=fopen(filename, "rb");
        if(fp==NULL)
        {
            printf("Failed to open '%s'.  Please correct the filename and try again.\n", filename);
            return;
        }

        fread(&l, sizeof(l), 1, fp);
        if(l!=machModelCode)
        {
            printf("'%s' is not a valid Machinations model.  Aborting.\n", filename);
		fclose(fp);
            return;
        }

        fread(&l, sizeof(l), 1, fp);
        vertexCount=(int)l;
        fread(&l, sizeof(l), 1, fp);
        triangleCount=(int)l;

        vertexBase=vertices.size();

        try
        {
            for(j=0;j<vertexCount;j++)
            {
                if(fread(&v3, sizeof(v3), 1, fp)<1)
                    throw -1;

                if(v3.x<minx)            minx=v3.x;
                if(v3.x>maxx)            maxx=v3.x;
                if(v3.y<miny)            miny=v3.y;
                if(v3.y>maxy)            maxy=v3.y;
                if(v3.z<minz)            minz=v3.z;
                if(v3.z>maxz)            maxz=v3.z;
                if(v3.radius()>radius)   radius=v3.radius();

                vertices.push_back(v3);
            }
            for(j=0;j<vertexCount;j++)
            {
                if(fread(&v3, sizeof(v3), 1, fp)<1)
                    throw -1;
                normals.push_back(v3);
            }
            for(j=0;j<vertexCount;j++)
            {
                if(fread(&v2, sizeof(v2), 1, fp)<1)
                    throw -1;
                texCoords.push_back(v2);
            }

            for(j=0;j<triangleCount;j++)
            {
                if(fread(&tri, sizeof(tri), 1, fp)<1)
                    throw -1;
                tri.a+=vertexBase;
                tri.b+=vertexBase;
                tri.c+=vertexBase;
                triangles.push_back(tri);
            }
        }
        catch(int x) //Dummy variable
        {
            printf("'%s' is corrupt.  Aborting.\n", filename);
            fclose(fp);
            return;
        }

        fclose(fp);
    }

    for(;i<argc;i++)
    {
        if(*argv[i]!='-' || strlen(argv[i])!=2)
        {
            printf("Invalid option '%s'\n", argv[i]);
            return;
        }
        switch(tolower(argv[i][1]))
        {
            case 'o':
                i++;
                if(i==argc)
                {
                    printf("Missing output filename.\n");
                    return;
                }
                strcpy(outputFilename, argv[i]);
                addExtension(outputFilename, ".mod");
                break;
        }
    }

    fp=fopen(outputFilename, "wb");
    if(fp==NULL)
    {
        printf("Failed to open '%s'.  Please correct the filename and try again.\n", outputFilename);
        return;
    }

    l=machModelCode;
    fwrite(&l, sizeof(l), 1, fp);
    l=(long)vertices.size();
    fwrite(&l, sizeof(l), 1, fp);
    l=(long)triangles.size();
    fwrite(&l, sizeof(l), 1, fp);

    fwrite(&vertices[0], sizeof(vertices[0]), vertices.size(), fp);
    fwrite(&normals[0], sizeof(normals[0]), normals.size(), fp);
    fwrite(&texCoords[0], sizeof(texCoords[0]), texCoords.size(), fp);

    fwrite(&triangles[0], sizeof(triangles[0]), triangles.size(), fp);
    fclose(fp);

    printf("Successfully created composite model '%s'\n", outputFilename);
    printf("    Vertices: %7d    Triangles: %7d\n", vertices.size(), triangles.size());
    printf("    Center X: %7.3f    Center Y:  %7.3f    Center Z: %7.3f\n",
        (maxx+minx)/2, (maxy+miny)/2, (maxz+minz)/2);
    printf("    Size X:   %7.3f    Size Y:    %7.3f    Size Z:   %7.3f\n", maxx-minx, maxy-miny, maxz-minz);
    printf("    Radius:   %7.3f    Max Z:     %7.3f\n", radius, maxz);
}

