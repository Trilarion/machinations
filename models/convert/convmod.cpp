/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* CONVMOD.CPP:    Converts .3DS models to the native Machinations format.   *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include <condefs.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#pragma hdrstop
/****************************************************************************/
USEUNIT("l3ds.cpp");
//---------------------------------------------------------------------------
#include "l3ds.h"

#define FUDGE_FACTOR 0.0001

const long machModelCode='M'+('A'<<8)+('C'<<16)+('H'<<24);

struct tVector3D
{
    float x;
    float y;
    float z;
    float radius()
    {
        return sqrt(x*x+y*y);
    }
};

struct tVector2D
{
    float x;
    float y;
};

struct TriangleNode
{
    LTriangle tri;
    int group;
};

struct VertexNode
{
    tVector3D   v;
    tVector3D   n;
    tVector2D   t;
    int group;
    int newIndex;
};

void addExtension(char *filename, const char *ext)
{
    char *ptr=filename;
    char *start=filename;
    while(*ptr!='\0')
    {
        if(*ptr=='/' || *ptr=='\\')
            start=ptr+1;
        ptr++;
    }

    ptr=strchr(start, '.');
    if(ptr==NULL)
        strcat(filename, ext);
}

void removeExtension(char *filename)
{
    char *ptr=filename;
    char *start=filename;
    while(*ptr!='\0')
    {
        if(*ptr=='/' || *ptr=='\\')
            start=ptr+1;
        ptr++;
    }

    ptr=strchr(start, '.');
    if(ptr!=NULL)
        *ptr='\0';
}

void main(int argc, char *argv[])
{
    int i,j,k,vcount,tcount,cur,vc,tc;
    char base[100];
    char filename[100];
    L3DS scene;
    FILE *fp;
    long l;
    LVector4 v4;
    LVector3 v3;
    LVector2 v2;
    tVector3D vect3;
    tVector2D vect2;
    float scale=1;
    LTriangle tri;
    TriangleNode *tn=NULL;
    VertexNode *vn=NULL;
    float minx, miny, minz, maxx, maxy, maxz, radius;
    bool reverseNormals=false;
    bool breakObjects=false;
    unsigned short temp;
    bool found;
    float fudgeFactor=0.000001;

    if(argc==1)
    {
        printf("Usage: convmod <filename> [-s scale] [-r] [-b] [-f fudge factor]\n");
        printf("    This utility converts one .3DS model to one or more Machinations\n");
        printf("    .mod objects named object00.mod, object01.mod, etc.\n");
        printf("    Options:\n");
        printf("      -s - Specify a scale larger than 1 if you want to enlarge the model.\n");
        printf("           Specify a scale less than 1 if you want to reduce the model.\n");
        printf("      -r - Reverse all normals. (turn model inside out)\n");
        printf("      -b - Break disjoint meshes apart.\n");
        printf("      -f - [Only applies when used with the -b option]\n");
        printf("           Specify a new value for the fudge factor.  The fudge factor defaults\n");
        printf("           to 0.000001.  The utility \"joins\" nearby vertices if the distance\n");
        printf("           between them is less than the fudge factor.\n");
        return;
    }

    strcpy(filename, argv[1]);
    addExtension(filename, ".3ds");
    strcpy(base, argv[1]);
    removeExtension(base);

    for(i=2;i<argc;i++)
    {
        if(*argv[i]!='-' || strlen(argv[i])!=2)
        {
            printf("Invalid option '%s'\n", argv[i]);
            return;
        }
        switch(tolower(argv[i][1]))
        {
            case 's':
                i++;
                if(i==argc)
                {
                    printf("Missing scale\n");
                    return;
                }
                if(sscanf(argv[i], "%f", &scale)<1)
                {
                    printf("Please specify a decimal value greater than zero for scale.\n");
                    return;
                }
                break;
            case 'f':
                i++;
                if(i==argc)
                {
                    printf("Missing fudge factor\n");
                    return;
                }
                if(sscanf(argv[i], "%f", &fudgeFactor)<1)
                {
                    printf("Please specify a decimal value greater than zero for fudge factor.\n");
                    return;
                }
                break;
            case 'r':
                reverseNormals=true;
                break;
            case 'b':
                breakObjects=true;
                break;
            default:
                printf("Unknown option '%s'\n",argv[i]);
                return;
        }
    }

    if (!scene.LoadFile(filename))
    {
        printf("Failed to load '%s'\n",argv[1]);
        return;
    }

    for(i=0;i<(int)scene.GetMeshCount();i++)
    {
        LMesh &mesh = scene.GetMesh(i);

        vcount=(int)mesh.GetVertexCount();
        tcount=(int)mesh.GetTriangleCount();
        vn=new VertexNode[vcount];
        tn=new TriangleNode[tcount];

        for(j=0;j<vcount;j++)
        {
            v4=mesh.GetVertex(j);
            vect3.x=(float)v4.x*scale;
            vect3.y=(float)v4.y*scale;
            vect3.z=(float)v4.z*scale;
            vn[j].v=vect3;

            v3=mesh.GetNormal(j);
            vect3.x=(float)v3.x;
            vect3.y=(float)v3.y;
            vect3.z=(float)v3.z;
            if(reverseNormals)
            {
                vect3.x*=-1;
                vect3.y*=-1;
                vect3.z*=-1;
            }
            vn[j].n=vect3;

            v2=mesh.GetUV(j);
            vect2.x=(float)v2.x;
            vect2.y=(float)v2.y;
            vn[j].t=vect2;

            if(breakObjects)
                vn[j].group=0;
            else
            {
                vn[j].group=1;
                vn[j].newIndex=j;
            }
        }

        for(j=0;j<tcount;j++)
        {
            tri=mesh.GetTriangle(j);
            if(reverseNormals)
            {
                temp=tri.b;
                tri.b=tri.c;
                tri.c=temp;
            }
            assert(tri.a<vcount);
            assert(tri.b<vcount);
            assert(tri.c<vcount);

            tn[j].tri=tri;
            if(breakObjects)
                tn[j].group=0;
            else
                tn[j].group=1;
        }

        if(breakObjects)
        {
            cur=0;
            while(true)
            {
                for(j=0;j<vcount;j++)
                    if(vn[j].group==0)
                        break;
                if(j==vcount)
                    break;
                cur++;
                vn[j].group=cur;
                do
                {
                    found=false;
                    for(j=0;j<tcount;j++)
                        if(tn[j].group==0)
                            if(vn[tn[j].tri.a].group==cur ||
                                vn[tn[j].tri.b].group==cur ||
                                vn[tn[j].tri.c].group==cur)
                            {
                                tn[j].group=cur;
                                vn[tn[j].tri.a].group=cur;
                                vn[tn[j].tri.b].group=cur;
                                vn[tn[j].tri.c].group=cur;
                                found=true;
                            }

                    //Look for coincidal vertices.
                    //The algorithm is terribly inefficient, but it's fine for small models.
                    for(j=0;j<vcount;j++)
                        for(k=0;k<vcount;k++)
                            if(fabs(vn[j].v.x-vn[k].v.x)<fudgeFactor &&
                                fabs(vn[j].v.y-vn[k].v.y)<fudgeFactor &&
                                fabs(vn[j].v.z-vn[k].v.z)<fudgeFactor)
                                if(vn[j].group==cur && vn[k].group==0)
                                {
                                    vn[k].group=cur;
                                    found=true;
                                }
                }
                while(found);
            }
        }
        else
            cur=1;

        for(j=1;j<=cur;j++)
        {
            sprintf(filename, "%s%02d%02d.mod", base, i, j);
            fp=fopen(filename, "wb");
            if(fp==NULL)
            {
                printf("Failed to write to '%s'\n", filename);
                break;
            }

            vc=0;
            for(k=0;k<vcount;k++)
                if(vn[k].group==j)
                    vn[k].newIndex=vc++;

            tc=0;
            for(k=0;k<tcount;k++)
                if(tn[k].group==j)
                    tc++;

            printf("%s -> %s\n", mesh.GetName().c_str(), filename);
            printf("    Vertices: %7d    Triangles: %7d\n", vc, tc);

            minx=1000000;  miny=1000000;  minz=1000000;
            maxx=-1000000; maxy=-1000000; maxz=-1000000;
            radius=0;

            fwrite(&machModelCode, sizeof(machModelCode), 1, fp);

            l=(long)vc;
            fwrite(&l, sizeof(l), 1, fp);

            l=(long)tc;
            fwrite(&l, sizeof(l), 1, fp);

            for(k=0;k<vcount;k++)
                if(vn[k].group==j)
                {
                    vect3=vn[k].v;
                    if(vect3.x<minx)            minx=vect3.x;
                    if(vect3.x>maxx)            maxx=vect3.x;
                    if(vect3.y<miny)            miny=vect3.y;
                    if(vect3.y>maxy)            maxy=vect3.y;
                    if(vect3.z<minz)            minz=vect3.z;
                    if(vect3.z>maxz)            maxz=vect3.z;
                    if(vect3.radius()>radius)   radius=vect3.radius();
                    fwrite(&vect3, sizeof(vect3), 1, fp);
                }

            printf("    Center X: %7.3f    Center Y:  %7.3f    Center Z: %7.3f\n",
                (maxx+minx)/2, (maxy+miny)/2, (maxz+minz)/2);
            printf("    Size X:   %7.3f    Size Y:    %7.3f    Size Z:   %7.3f\n", maxx-minx, maxy-miny, maxz-minz);
            printf("    Radius:   %7.3f    Max Z:     %7.3f\n", radius, maxz);

            for(k=0;k<vcount;k++)
                if(vn[k].group==j)
                {
                    vect3=vn[k].n;
                    fwrite(&vect3, sizeof(vect3), 1, fp);
                }

            for(k=0;k<vcount;k++)
                if(vn[k].group==j)
                {
                    vect2=vn[k].t;
                    fwrite(&vect2, sizeof(vect2), 1, fp);
                }

            for(k=0;k<tcount;k++)
                if(tn[k].group==j)
                {
                    tri.a=vn[tn[k].tri.a].newIndex;
                    tri.b=vn[tn[k].tri.b].newIndex;
                    tri.c=vn[tn[k].tri.c].newIndex;
                    fwrite(&tri, sizeof(tri), 1, fp);
                }

            fclose(fp);
        }

        delete[] vn;
        vn=NULL;
        delete[] tn;
        tn=NULL;
    }
}

