/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef fontH
#define fontH
/****************************************************************************/
//Headers:
#include "types.h"

//Defines:
/* Insert these character constants into a string to change the color.
 * For instance:
 *  drawString("This is " FONT_BLUE "blue", 0, 0, smallFont);
 */
#define FONT_BLACK  "\x00"
#define FONT_BLUE   "\x01"
#define FONT_GREEN  "\x02"
#define FONT_CYAN   "\x03"
#define FONT_RED    "\x04"
#define FONT_PURPLE "\x05"
#define FONT_YELLOW "\x06"
#define FONT_WHITE  "\x07"

//Structures:
struct tGlyphMetrics
{
    unsigned int    gmBlackBoxX;
    unsigned int    gmBlackBoxY;
    long            gmptGlyphOriginX;
    long            gmptGlyphOriginY;
    short           gmCellIncX;
    short           gmCellIncY;
};

struct tCharacter
{
    short           textureX;
    short           textureY;
    tGlyphMetrics   glyphMetrics;
};

struct tFont
{
    tString         name;
    bool            antialias;
    int             displayLists;
    int             height;
    int             maxCharWidth;
    int             base; //Smallest gmptGlyphOriginY of any character in the font.
                          //You should subtract this value from the desired y-value.
    tCharacter      characters[256];

    tFont() : antialias(false), displayLists(0), height(0), maxCharWidth(0), base(0) {}
};

//Declarations for public variables: (see descriptions in font.cpp)
extern int  smallFont;
extern int  mediumFont;
extern int  largeFont;

//Declarations for public functions: (see descriptions in font.cpp)
extern int          fontLookup(const tString& name, bool exact);
extern int          charWidth(char ch, int fontIndex);
extern int          fontHeight(int fontIndex);
extern bool         loadFonts(const tString& filename);
extern void         refreshFonts();
extern void         destroyFonts();
extern void         drawString(const char *text, int x, int y, int fontIndex);
extern void         drawText(const tStringList& s, int x, int y, int vx, int vy, int vw, int vh, int fontIndex);
extern int          stringWidth(const char *text, int fontIndex);
extern int          textWidth(const tStringList& s, int fontIndex);
extern tString      trimString(const tString& s, int width, int fontIndex);
/****************************************************************************/
#endif
