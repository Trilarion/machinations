/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef cacheH
#define cacheH
/****************************************************************************/

#include "vec.h"

#define CACHE_PATH "cache/"

class tCache
{
    public:
    char *header;
    int headerLength;
    int headerCapacity;

    void addDataToHeader(const char *data, int length);
    void addStringToHeader(const char *s) { addDataToHeader(s,strlen(s)+1); }
    void addCharToHeader(char ch) { addDataToHeader(&ch,1); }
    void addShortToHeader(short s) { addDataToHeader((char*)&s,sizeof(s)); }
    void addLongToHeader(long l) { addDataToHeader((char*)&l,sizeof(l)); }
    void addFloatToHeader(float f) { addDataToHeader((char*)&f,sizeof(f)); }
    void addIntToHeader(int i) { addDataToHeader((char*)&i,sizeof(i)); }
    void addBoolToHeader(bool b) { addDataToHeader((char*)&b,sizeof(b)); }
    void addVector2DToHeader(const tVector2D& v) {  addDataToHeader((char*)&v,sizeof(v)); }
    void addVector3DToHeader(const tVector3D& v) {  addDataToHeader((char*)&v,sizeof(v)); }

    FILE *readFromCache(const char *filename);
    FILE *writeToCache(const char *filename);

    tCache();
    tCache(const tCache& c) : header(NULL), headerLength(0), headerCapacity(0)
        { operator=(c); }
    tCache& operator=(const tCache& c);
    ~tCache();
};
/****************************************************************************/
#endif
