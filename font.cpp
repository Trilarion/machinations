/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* FONT.CPP:     Generates and renders fonts as textured quads.              *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#include "font.h"
#include "oglwrap.h"
#include "memwatch.h"

#pragma package(smart_init)

/****************************************************************************
                            External Variables
 ****************************************************************************/
int smallFont=-1;
int mediumFont=-1;
int largeFont=-1;



/****************************************************************************
                             Static Variables
 ****************************************************************************/
static vector<tFont *>  fonts;
static char             textureData[256][256];
static iTextureIndex    fontTexture = 0;//Index to font's texture
static const char machFontCode[4]={'F','N','T','1'};



/****************************************************************************/
bool loadFonts(const tString& filename)
{
    int i,j,k,base,peak,w,h;
    long l;
    int fontCount;
    char ch;
    char code[4];
    tFont *font;
    
    destroyFonts();

    glGenTextures(1,&fontTexture);

    FILE *fp=fopen(CHARPTR(filename), "rb");
    if(fp==NULL)
    {
        bug("loadFonts: failed to open '%s'",CHARPTR(filename));
        return false;
    }

    fread(code,sizeof(code),1,fp);
    if(memcmp(code,machFontCode,4)!=0)
    {
        bug("loadFonts: '%s' is not a valid Machinations font set",CHARPTR(filename));
        fclose(fp);
        return false;
    }

    fread(&l,sizeof(l),1,fp);
    fontCount=(int)l;

    for(i=0;i<fontCount;i++)
    {
        font = new tFont;
        font->name=readString(fp);
        readString(fp); //truetype font
        readString(fp); //character range
        fread(&l,sizeof(l),1,fp); //font size

        fread(&ch,sizeof(ch),1,fp);
        font->antialias=(bool)(ch&16);

        for(j=0;j<256;j++)
        {
            tCharacter& c=font->characters[j];
            fread(&c.glyphMetrics,sizeof(c.glyphMetrics),1,fp);
            fread(&c.textureX,sizeof(c.textureX),1,fp);
            fread(&c.textureY,sizeof(c.textureY),1,fp);

            w=ALIGN(c.glyphMetrics.gmBlackBoxX, 4);
            h=c.glyphMetrics.gmBlackBoxY;
            for(k=0;k<w*h;k++)
                fgetc(fp);
        }

        font->displayLists=glGenLists(256);
        fonts.push_back(font);
    }
    fread(&l,sizeof(l),1,fp); //character spacing

    fread(textureData,1,256*256,fp);
    fclose(fp);

    //Calculate width, height, and base for each font.
    for(i=0; i<fonts.size(); i++)
    {
        font=fonts[i];

        //The width of a font is the maximum width of any character.
        w=0;
        for (j = 0; j < 256; j++)
            if(font->characters[j].glyphMetrics.gmCellIncX > w)
                w = font->characters[j].glyphMetrics.gmCellIncX;
        font->maxCharWidth=w;

        base = 1000;
        peak = -1000;
        //Find the base (lowest point) and peak (highest point) of the standard
        //ASCII set (characters 32-126)
        for (j = 32; j <= 126; j++)
        {
            tCharacter& c=font->characters[j];
            if(c.glyphMetrics.gmptGlyphOriginY - (signed int) c.glyphMetrics.gmBlackBoxY < base)
                base = c.glyphMetrics.gmptGlyphOriginY - (signed int) c.glyphMetrics.gmBlackBoxY;
            if(c.glyphMetrics.gmptGlyphOriginY > peak)
                peak = c.glyphMetrics.gmptGlyphOriginY;
        }
        //The base of a font is the lowest point of any character.
        font->base=base;
        //The height of a font is the distance between the highest point of any
        //character and the lowest point of any character.
        font->height=peak-base;
    }

    refreshFonts();

    smallFont=fontLookup("small",/*exact=*/true);
    mediumFont=fontLookup("medium",/*exact=*/true);
    largeFont=fontLookup("large",/*exact=*/true);

    return true;
}
/****************************************************************************/
void refreshFonts()
{
    int i,j,ox,oy,mx,my,tx,ty,w,h;
    tFont *font;
    bool specialChar;
    const float black[4]={0,0,0,0};

    if(fontTexture>0)
    {
        glEnable(GL_TEXTURE_2D);
        mglBindTexture(fontTexture);

        //Despite the additional memory requirements, I discovered GL_RGBA was faster
        //than GL_ALPHA since it more closely matches the internal storage format of
        //the VRAM.
        glPixelTransferf(GL_RED_BIAS,1);
        glPixelTransferf(GL_GREEN_BIAS,1);
        glPixelTransferf(GL_BLUE_BIAS,1);
        gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA4, 256, 256, GL_ALPHA, GL_UNSIGNED_BYTE, textureData);
        glPixelTransferf(GL_RED_BIAS,0);
        glPixelTransferf(GL_GREEN_BIAS,0);
        glPixelTransferf(GL_BLUE_BIAS,0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, black);

        glDisable(GL_TEXTURE_2D);
    }

    for(i=0;i<fonts.size();i++)
    {
        font=fonts[i];

        for(j=0;j<256;j++)
        {
            tCharacter& c=font->characters[j];
            glNewList(font->displayLists+j, GL_COMPILE);

                //Special color codes:
                specialChar=true;
                switch(j)
                {
                    case '\x00':    glColor3f(0,0,0);   break;
                    case '\x01':    glColor3f(0,0,1);   break;
                    case '\x02':    glColor3f(0,1,0);   break;
                    case '\x03':    glColor3f(0,1,1);   break;
                    case '\x04':    glColor3f(1,0,0);   break;
                    case '\x05':    glColor3f(1,0,1);   break;
                    case '\x06':    glColor3f(1,1,0);   break;
                    case '\x07':    glColor3f(1,1,1);   break;
                    case (unsigned char)'�':
                        glPushAttrib(GL_CURRENT_BIT);
                        glColor3f(1,1,0);
                        break;
                    case (unsigned char)'�':
                        glPopAttrib();
                        break;
                    default:
                        specialChar=false;

                        //Copy these six values into temporary variables to avoid clutter.
                        w=c.glyphMetrics.gmBlackBoxX;
                        h=c.glyphMetrics.gmBlackBoxY;
                        ox=c.glyphMetrics.gmptGlyphOriginX;
                        oy=c.glyphMetrics.gmptGlyphOriginY;
                        mx=c.glyphMetrics.gmCellIncX;
                        my=c.glyphMetrics.gmCellIncY;
                        tx=c.textureX;
                        ty=c.textureY;
                        //Some character, such as spaces, do not need a textured quad.
                        if(w>0&&h>0&&tx>=0&&ty>=0)
                        {
                            glBegin(GL_QUADS);
                                /*
                                 * Although I would prefer to use glTexCoord2i or glTextCoord2s,
                                 * I could not make these work properly.
                                 *
                                 * Here is a diagram of the character's orientation about the
                                 * origin:                  Y
                                 *                          |
                                 *                          | ox
                                 *                          | <-> +-----+
                                 *                          |     |  |  | } oy
                                 *               -----------+-----|  h  |------X
                                 *                          |     |  |  |
                                 *                          |     |  |  |
                                 *                          |     +-----+
                                 *                          |     <- w ->
                                 */
                                glTexCoord2f((float)tx/256, (float)ty/256);
                                glVertex2i(ox, oy-h);

                                glTexCoord2f((float)tx/256,(float)(ty+h)/256);
                                glVertex2i(ox, oy);

                                glTexCoord2f((float)(tx+w)/256, (float)(ty+h)/256);
                                glVertex2i(ox+w, oy);

                                glTexCoord2f((float)(tx+w)/256, (float)ty/256);
                                glVertex2i(ox+w, oy-h);
                            glEnd();
                        }
                        //The last instruction in each display list is a translation to the
                        //position where the next character should be drawn.
                        glTranslatef(mx, my, 0);
                        break;
                }

                //We don't want the width of special characters to influence the width of a string.
                if(specialChar)
                    c.glyphMetrics.gmCellIncX=0;

            glEndList();
        }
    }
}

/***************************************************************************\
destroyFonts

Deallocates memory used by dynamic arrays.  Deletes display lists and
textures.  Resets fontCount to zero and fontsLoaded to false.  Call this
function when the program finishes or prior to loading a new font.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
void destroyFonts()
{
    int i;
    tFont *font;
    for(i=0;i<fonts.size();i++)
    {
        font=fonts[i];
        if(font->displayLists>0)
            glDeleteLists(font->displayLists, 256);
        delete font;
    }
    fonts.clear();
    if(fontTexture>0)
    {
        glDeleteTextures(1, &fontTexture);
        fontTexture=0;
    }
    smallFont=-1;
    mediumFont=-1;
    largeFont=-1;
}

/***************************************************************************\
drawString

Primary function for rendering a string of text.  The function is essentially
a wrapper for draw.  It verifies the parameters and enables alpha testing.

Inputs:
    len         Length of the string (below)
    string      String to render
    x, y        Lower-left corner of the text block on the screen
    fontIndex   Font with which to render the string
Outputs:
    none
\***************************************************************************/
void drawString(const char *text, int x, int y, int fontIndex)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(text)) //just check the first character
        return;
    if(fontIndex<0||fontIndex>=fonts.size())
    {
        bug("drawString: invalid font index");
        return;
    }
#endif
    tFont *font=fonts[fontIndex];
    if(font->antialias)
        glEnable(GL_BLEND);
    else
        glEnable(GL_ALPHA_TEST);

    glEnable(GL_TEXTURE_2D);
    mglBindTexture(fontTexture);
    //Translate to the position where we will render the first character.
    //We must substract the base since some characters extend beneath the x-axis.
    glPushMatrix();
    glTranslatef(x,y-font->base,0);
    glListBase(font->displayLists);
    glCallLists(strlen(text), GL_UNSIGNED_BYTE, (unsigned char *) text);
    glListBase(0);
    glPopMatrix();
    glDisable(GL_TEXTURE_2D);

    glDisable(GL_BLEND);
    glDisable(GL_ALPHA_TEST);
}

/***************************************************************************\
stringWidth

Calculates the width of a string if it were rendered on the screen.  The
function accomplishes this by summing the width of each character.

Inputs:
    text        Null-terminated string
    fontIndex   Font with which to measure the width
Outputs:
    Returns the width of the string or 0 if an error occurred
\***************************************************************************/
int stringWidth(const char *text, int fontIndex)
{
    int len=0;
#ifdef DEBUG
    if(MEMORY_VIOLATION(text)) //just check the first character
        return 0;
    if(fontIndex<0||fontIndex>=fonts.size())
    {
        bug("textWidth: invalid font index");
        return 0;
    }
#endif
    tFont *font=fonts[fontIndex];
    for(int i=0;i<strlen(text);i++)
        len+=font->characters[(unsigned char)text[i]].glyphMetrics.gmCellIncX;
    return len;
}

/***************************************************************************\
textWidth

Calculates the width of a text block if it were rendered on the screen.  The
function accomplishes this by finding the length of each line and choosing
the longest.

Inputs:
    list        String list containing the text block
    fontIndex   Font with which to measure the width
Outputs:
    Returns the width of the text block or 0 if an error occurred
\***************************************************************************/
int textWidth(const tStringList& list,int fontIndex)
{
    int i,j,best=0;
#ifdef DEBUG
    if(fontIndex<0||fontIndex>=fonts.size())
    {
        bug("textWidth: invalid font index");
        return 0;
    }
#endif
    //Traverse the string list line by line and choose the longest.
    for(i=0;i<list.size();i++)
    {
        j=stringWidth(CHARPTR(list[i]),fontIndex);
        if(j>best)
            best=j;
    }
    return best;
}

/***************************************************************************\
trimString

Trims a string so that it will fit within the specified width if it were
rendered on the screen.  If the string doesn't need to be trimmed (e.g. it
already fits), the function returns it in its original form.  Otherwise,
the function "trims" one or more characters from the end and appends an ellipsis
(...) to the end.  If the length of an ellipsis exceeds the specified width,
the function returns an empty string.

Inputs:
    s           String to trim
    width       Maximum width of the string
    fontIndex   Font with which to guage the width
Outputs:
    Returns the trimmed string or an empty string if an error occurred.
\***************************************************************************/
tString trimString(const tString& s, int width, int fontIndex)
{
    int i;
#ifdef DEBUG
    if(fontIndex<0||fontIndex>=fonts.size())
    {
        bug("trimString: invalid font index");
        return EMPTY_STRING;
    }
#endif
    tFont *font=fonts[fontIndex];

    //Since the provided width will typically far exceed the string's width,
    //this check avoids unnecessary computation.
    if(STRLEN(s)*font->maxCharWidth<=width)
        return s;
    //Ensure there is enough room for the ellipsis
    width-=stringWidth("...",fontIndex);
    if(width<0)
        return EMPTY_STRING;
    //From left to right, determine how many characters will fit within the space.
    for(i=0;i<STRLEN(s);i++)
    {
        width-=font->characters[(unsigned char)s[i]].glyphMetrics.gmCellIncX;
        if(width<0)
            break;
    }
    //Append the ellipsis if necessary
    if(i<STRLEN(s))
        return STRLEFT(s,i)+"...";
    else
        return s;
}

/***************************************************************************\
drawText

Primary function for rendering a block of text.  Since the text block is often
quite large, the function draws only the portion which is visible within
a specified viewport.  NOTE: Some characters may overlap the boundaries of
the viewport, so the user will need to use glScissors or an equivalent for text
boxes.  The text block and/or viewport may extend beyond the edge of the screen.

Inputs:
    list        String list containing the text block
    x, y        Coordinates of the top-left corner of the text block
    vx, vy      Coordinates of the bottom-left corner of the viewport
    vw, vh      Dimension of the viewport
    fontIndex   Font with which to render the text block
Outputs:
    none
\***************************************************************************/
void drawText(const tStringList& list, int x, int y, int vx, int vy, int vw, int vh, int fontIndex)
{
    int i, j, k, position, xpos, ypos;
    //These variables indicate the offset of the top-left corner of the text block
    //from the top-left corner of the viewport.
    int ox=x-vx, oy=vy+vh-y;
#ifdef DEBUG
    if(fontIndex<0||fontIndex>=fonts.size())
    {
        bug("drawText: invalid font index");
        return;
    }
#endif
    tFont *font=fonts[fontIndex];

    if(font->antialias)
        glEnable(GL_BLEND);
    else
        glEnable(GL_ALPHA_TEST);
    glEnable(GL_TEXTURE_2D);
    mglBindTexture(fontTexture);
    glListBase(font->displayLists);

    for(i=0;i<list.size();i++)
    {
        //Skip lines which are not visible within the viewport.
        if (font->height*(i+1)+font->base + oy < 0 ||
            font->height*i+font->base + oy > vh)
            continue;
        //Locate the first visible character on the line.
        position = 0;
        for(j=0;j<STRLEN(list[i]);j++)
        {
            if(position + font->maxCharWidth + ox > 0)
                break;
            position+=font->characters[(unsigned char)list[i][j]].glyphMetrics.gmCellIncX;
        }
        //Calculate where to draw the line.
        xpos=x+position;
        ypos=y-font->height*(i+1);
        //Locate the last visible character on the line.
        for(k=j;k<STRLEN(list[i]);k++)
        {
            if(position + ox > vw)
                break;
            position+=font->characters[(unsigned char)list[i][k]].glyphMetrics.gmCellIncX;
        }

        //Draw only the visible portion of the line.
        glPushMatrix();
        glTranslatef(xpos,ypos-font->base,0);
        glCallLists(k-j, GL_UNSIGNED_BYTE, (unsigned char *) CHARPTR(STRMID(list[i], j, k-j)));
        glPopMatrix();
    }
    glListBase(0);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);
    glDisable(GL_ALPHA_TEST);
}

/***************************************************************************\
charWidth

Finds the width of one character using its glyph metrics.

Inputs:
    ch          Character
    fontIndex   Font with which to measure width
Outputs
    Returns character's width or 0 if an error occurred
\***************************************************************************/
//returns the width of character 'ch'
int charWidth(char ch, int fontIndex)
{
#ifdef DEBUG
    if(fontIndex<0||fontIndex>=fonts.size())
    {
        bug("charWidth: invalid font index");
        return 0;
    }
#endif
    //Select the correct record from the glyph metric array.
    return fonts[fontIndex]->characters[(unsigned char)ch].glyphMetrics.gmCellIncX;
}

/***************************************************************************\
fontHeight

Finds the height of a font (e.g. the distance between the highest point and
the lowest point).

Inputs:
    fontIndex   Font
Outputs:
    Returns the font's height or 0 if an error occurred
\***************************************************************************/
int fontHeight(int fontIndex)
{
#ifdef DEBUG
    if(fontIndex<0||fontIndex>=fonts.size())
    {
        bug("fontHeight: invalid font index");
        return 0;
    }
#endif
    return fonts[fontIndex]->height;
}
/***************************************************************************/
int fontLookup(const tString& name, bool exact)
{
    int i;
    tString n=STRTOLOWER(name);
    for(i=0; i<fonts.size(); i++)
        if(STREQUAL(fonts[i]->name,n))
            return i;
    if(exact)
        return -1;
    for(i=0; i<fonts.size(); i++)
        if(STRPREFIX(n,fonts[i]->name)) //STRPREFIX is case-insensitive
            return i;
    return -1;
}
