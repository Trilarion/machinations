�
 TMAIN 0�  TPF0TMainMainLeft� ToppActiveControl
lbTextures
AutoScrollClientHeight*ClientWidthColor	clBtnFaceConstraints.MinHeight�Constraints.MinWidthFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style Menu
mmMainMenuOldCreateOrderPositionpoDesktopCenterVisible	OnCloseQueryFormCloseQueryOnCreate
FormCreate	OnDestroyFormDestroyOnPaint	FormPaintPixelsPerInch`
TextHeight TLabelLabel1Left� Top�WidthHeightAnchorsakLeftakBottom CaptionZoom:  TLabelLabel2LeftTop
WidthHeightAnchorsakLeftakBottom Caption1x  TLabelLabel3LeftuTop
WidthHeightAnchorsakLeftakBottom Caption16x  TLabelLabel4Left0Top
WidthHeightAnchorsakLeftakBottom Caption2x  TLabelLabel5LeftITop
WidthHeightAnchorsakLeftakBottom Caption4x  TLabelLabel6Left`Top
WidthHeightAnchorsakLeftakBottom Caption8x  TLabelLabel7Left�Top�Width=HeightAnchorsakLeftakBottom CaptionBackground:  TLabelLabel15LeftTopWidth� HeightCaption2This texture pack contains the following textures:  TPanelPanel2Left Top Width� Height
BevelOuter	bvLoweredTabOrder  TPanelpaViewLeft� Top WidthHeight�AnchorsakLeftakTopakRightakBottom 
BevelOuter	bvLoweredTabOrder OnMouseDownpaViewMouseDownOnMouseMovepaViewMouseMove	OnMouseUppaViewMouseUpOnResizepaViewResize  
TScrollBarsbHorizontalLeft� Top�WidthHeightAnchorsakLeftakRightakBottom LargeChange
PageSize TabOrderTabStopOnScrollsbScroll  
TScrollBar
sbVerticalLeftTop WidthHeight�AnchorsakTopakRightakBottom Kind
sbVerticalLargeChange
PageSize TabOrderTabStopOnScrollsbScroll  	TTrackBartbZoomLeftTop�WidthvHeightAnchorsakLeftakBottom LineSize MaxOrientationtrHorizontal	FrequencyPosition SelEnd SelStart TabOrderThumbLength	TickMarkstmBottomRight	TickStyletsAutoOnChangetbZoomChange  	TComboBoxcbBackgroundLeft�Top�WidthaHeightStylecsDropDownListAnchorsakLeftakBottom 
ItemHeightItems.StringsBlackWhiteRedGreenBlue TabOrderOnChangecbBackgroundChange  TPanelPanel1LeftTop� Width� HeightAnchorsakLeftakBottom 
BevelOuter	bvLoweredTabOrder TLabelLabel8LeftTopWidthHeightCaptionName:  TLabelLabel9LeftTop0WidthHeightCaptionLeft:  TLabelLabel10LeftTopHWidth$HeightCaptionBottom:  TLabelLabel11LeftTop`WidthHeightCaptionWidth:  TLabelLabel12LeftTopxWidth"HeightCaptionHeight:  TEditedNameLeft0TopWidth� HeightTabOrder OnChangeedNameChange  TEditedLeftLeft0Top.Width9HeightTabOrderOnChangeedLeftChange  TEditedBottomLeft0TopFWidth9HeightTabOrderOnChangeedBottomChange  TEditedWidthLeft0Top^Width9HeightTabOrderOnChangeedWidthChange  TEditedHeightLeft0TopvWidth9HeightTabOrderOnChangeedHeightChange  	TGroupBox	gbHotspotLeftTop� Width� HeightICaptionHotspotTabOrder TLabelLabel13LeftTopWidth
HeightCaptionX:  TLabelLabel14LeftpTopWidth
HeightCaptionY:  TEdit
edHotspotXLeft(TopWidth9HeightTabOrder OnChangeedHotspotXChange  TEdit
edHotspotYLeft� TopWidth9HeightTabOrderOnChangeedHotspotYChange  TButtonbtPositionHotspotLeftTop(Width� HeightCaptionPosition Hotspot (Right-click)TabOrderOnClickbtPositionHotspotClick   	TCheckBoxcbAlphaTestingLeftTop WidthaHeightCaptionAlpha TestingTabOrderOnClickcbAlphaTestingClick  	TCheckBoxcbAlphaBlendingLeft� Top WidthaHeightCaptionAlpha BlendingTabOrderOnClickcbAlphaBlendingClick  TButtonbtDrawRectangleLeftTop� Width� HeightCaption"Draw Rectangle (CTRL + Drag Mouse)TabOrderOnClickbtDrawRectangleClick  TButtonbtDuplicateLeft� TopPWidth9HeightCaption	DuplicateTabOrder	OnClickbtDuplicateClick  TButtonbtDuplicateLeftLeftwTopPWidthHeightCaption<TabOrder
OnClickbtDuplicateLeftClick  TButtonbtDuplicateUpLeft� Top7WidthHeightCaption^TabOrderOnClickbtDuplicateUpClick  TButtonbtDuplicateRightLeft� TopPWidthHeightCaption>TabOrderOnClickbtDuplicateRightClick  TButtonbtDuplicateDownLeft� TopiWidthHeightCaptionvTabOrderOnClickbtDuplicateDownClick   TListBox
lbTexturesLeftTopWidth� Height� AnchorsakLeftakTopakBottom 
ItemHeightMultiSelect	TabOrderOnClicklbTexturesClick	OnKeyDownlbTexturesKeyDown  TButtonbtNewTextureLeftTop� WidthKHeightAnchorsakLeftakBottom CaptionNew TextureTabOrderOnClickbtNewTextureClick  TButtonbtRemoveTextureLeft� Top� WidthYHeightAnchorsakLeftakBottom CaptionRemove TextureTabOrderOnClickbtRemoveTextureClick  	TCheckBoxcbTexturesVisibleLeft6Top�WidthaHeightAnchorsakLeftakBottom CaptionTextures VisibleChecked	State	cbCheckedTabOrder	OnClickcbTexturesVisibleClick  	TCheckBoxcbHotspotsVisibleLeft�Top�WidthaHeightAnchorsakLeftakBottom CaptionHotspots VisibleChecked	State	cbCheckedTabOrder
OnClickcbHotspotsVisibleClick  TButtonbtSortTexturesLeftMTop� WidthKHeightAnchorsakLeftakBottom CaptionSort TexturesTabOrderOnClickbtSortTexturesClick  
TStatusBarsbStatusBarLeft TopWidthHeightPanelsWidth�  WidthP Widthx Width   SimplePanel  	TMainMenu
mmMainMenuLeft� Top 	TMenuItemmiFileCaption&File 	TMenuItemmiNewCaption&New Texture PackShortCutN@OnClick
miNewClick  	TMenuItemmiOpenCaption&Open Texture Pack...ShortCutO@OnClickmiOpenClick  	TMenuItemmiSaveCaption&Save Texture PackShortCutS@OnClickmiSaveClick  	TMenuItemmiSaveAsCaptionSave Texture Pack &As...OnClickmiSaveAsClick   	TMenuItemmiSelectImageCaptionSelect &Image...OnClickmiSelectImageClick  	TMenuItemmiSetWorkingDirectoryCaptionSet &Working Directory...OnClickmiSetWorkingDirectoryClick  	TMenuItemmiQuitCaption&QuitOnClickmiQuitClick   TOpenDialogodSelectImage
DefaultExt*.pngFilter�PNG Image File (*.png)|*.png|Window Bitmap (*.bmp)|*.bmp|JPEG Image File (*.jpg;*.jpeg)|*.jpg;*.jpeg|GIF Image File (*.gif)|*.gif|All Files (*.*)|*.*TitleSelect ImageLeftTop  TOpenDialogodOpenTexturePack
DefaultExt*.tpkFilter/Texture Packs (*.tpk)|*.tpk|All Files (*.*)|*.*TitleOpen Texture PackLeft>Top  TSaveDialogsdSaveTexturePack
DefaultExt*.tpkFilter/Texture Packs (*.tpk)|*.tpk|All Files (*.*)|*.*TitleSave Texture PackLeft^Top   