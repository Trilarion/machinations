�
 TMAIN 0�
  TPF0TMainMainLeftHTopyActiveControlbtChooseFileBorderIconsbiSystemMenu
biMinimize BorderStylebsSingleCaption
PathfinderClientHeight�ClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoDesktopCenterVisible	PixelsPerInch`
TextHeight TLabelLabel1LeftTopWidthHeightCaption1.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel2LeftTop(WidthHeightCaption2.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel3Left(Top(Width� HeightCaption.Position the origin with the left mouse button  TLabelLabel4Left(Top8Width� HeightCaption4Position the destination with the right mouse button  TLabelLabel5LeftTopJWidthHeightCaption3.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabel
laFileNameLeft� TopWidth� HeightAutoSizeColorclWhiteParentColor  TLabelLabel6LeftTopmWidthHeightCaption4.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel13LeftTop�Width{HeightCaptionXKEY:  Light gray line - result of A* algorithm; Red line - final result after adjustment  TButtonbtChooseFileLeft(TopWidth� HeightCaptionChoose fileTabOrder OnClickbtChooseFileClick  TPanelPanel1LeftTop� WidthHeight
BevelOuter	bvLoweredTabOrder TImageimImageLeftTopWidth Height AlignalLeftOnMouseDownimImageMouseDown  TImageimZoomLeftTopWidth Height AlignalLeftOnMouseDownimZoomMouseDown   TButton	btComputeLeft(TopPWidth� HeightCaptionFind the shortest pathTabOrderOnClickbtComputeClick  TButtonbtExitLeft�Top�WidthKHeightCaptionExitTabOrderOnClickbtExitClick  TButtonbtResetLeft(ToppWidthKHeightCaptionResetTabOrderOnClickbtResetClick  	TGroupBox	GroupBox1Left`TopWidth� Height� CaptionResultsTabOrder TLabelLabel7LeftTop Width