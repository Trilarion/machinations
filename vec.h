/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef vectorH
#define vectorH
/****************************************************************************/

#ifndef CACHE_HEADERS
    #include <math.h> //for sqrt
#endif
#ifndef M_PI
    #define M_PI     3.14159265359
#endif

#define SIN_TABLE_SIZE      16384
#define SIN_SCALE           2607.594588
#define SIN_MASK            0x00003FFF
#define COS_TABLE_SIZE      16384
#define COS_SCALE           2607.594588
#define COS_MASK            0x00003FFF
#define ASIN_TABLE_SIZE     16384
#define ASIN_SCALE          8192
#define ASIN_BIAS           8192
#define ACOS_TABLE_SIZE     16384
#define ACOS_SCALE          8192
#define ACOS_BIAS           8192
#define SPLINE_TABLE_SIZE   1024

//The tVector3D class contains one three-dimensional vector and provides a set
//of basic vector operations.
class tVector3D
{
    public:
    //These are the x, y, and z components of the vector
    float x;
    float y;
    float z;

    //Define an inline function for adding two vectors.
    tVector3D operator+(const tVector3D& arg) const
    {
        tVector3D v = {x+arg.x, y+arg.y, z+arg.z};
        return v;
    }
    //Define an inline function for subtracting two vectors.
    tVector3D operator-(const tVector3D& arg) const
    {
        tVector3D v = {x-arg.x, y-arg.y, z-arg.z};
        return v;
    }
    //Define an inline function for multiplying a vector by a scalar.
    tVector3D operator*(float c) const
    {
        tVector3D v = {c*x, c*y, c*z};
        return v;
    }
    //Define an inline function for dividing a vector by a scalar.
    tVector3D operator/(float c) const
    {
        tVector3D v = {x/c, y/c, z/c};
        return v;
    }
    //Define an inline function for negating a vector.
    tVector3D operator-() const
    {
        tVector3D v = {-x, -y, -z};
        return v;
    }
    //Define an inline function for adding two vectors and storing the result
    //in the first.
    void operator+=(const tVector3D& arg)
    {
        x+=arg.x;
        y+=arg.y;
        z+=arg.z;
    }
    //Define an inline function for subtracting two vectors and storing the
    //result in the first.
    void operator-=(const tVector3D& arg)
    {
        x-=arg.x;
        y-=arg.y;
        z-=arg.z;
    }
    //Define an inline function for multiplying a vector by a scalar and
    //storing the result in the vector.
    void operator*=(float c)
    {
        x*=c;
        y*=c;
        z*=c;
    }
    //Define an inline function for dividing a vector by a scalar and
    //storing the result in the vector.
    void operator/=(float c)
    {
        x/=c;
        y/=c;
        z/=c;
    }
    //Define an inline function for comparing two vectors.
    bool operator==(const tVector3D& arg) const
    {
        return x==arg.x && y==arg.y && z==arg.z;
    }
    //Define an inline function for comparing two vectors.
    bool operator!=(const tVector3D& arg) const
    {
        return x!=arg.x || y!=arg.y || z!=arg.z;
    }
    //Define an inline function for calculating the dot product of two vectors.
    float dot(const tVector3D& arg) const
    {
        return x*arg.x + y*arg.y + z*arg.z;
    }
    //Define an inline function for calculating the dot product of two vectors.
    float dot(float vx, float vy, float vz) const
    {
        tVector3D v = {vx,vy,vz};
        return dot(v);
    }
    //Define an inline function for calculating the cross product of two vectors.
    tVector3D cross(const tVector3D& arg) const
    {
        tVector3D v = {
            y*arg.z-z*arg.y,
            z*arg.x-x*arg.z,
            x*arg.y-y*arg.x};
        return v;
    }
    //Define an inline function for calculating the cross product of two vectors.
    tVector3D cross(float vx, float vy, float vz) const
    {
        tVector3D v = {vx,vy,vz};
        return cross(v);
    }

    //This function returns the length of the vector.
    float length() const
    {
        return sqrt(x*x+y*y+z*z);
    }

    //This function returns a vector's length squared.
    float lengthSquared() const
    {
        return x*x+y*y+z*z;
    }

    //This function returns a copy of the vector which points in the same
    //direction, but has length one.  It does not affect the original vector.
    //If you attempt to normalize the zero vector, this function will return
    //the zero vector.
    tVector3D normalize() const
    {
        tVector3D v = {x,y,z};
        float l=v.length();
        if(l>0)
        {
            v.x/=l;
            v.y/=l;
            v.z/=l;
        }
        return v;
    }

    inline tVector3D parallelComponent(const tVector3D& unitBasis) const
    {
        float projection = dot(unitBasis);
        return unitBasis * projection;
    }
    inline float scalarProjection(const tVector3D& unitBasis) const
    {
        return dot(unitBasis);
    }
    inline tVector3D perpendicularComponent(const tVector3D& unitBasis) const
    {
        return operator-(parallelComponent(unitBasis));
    }
    inline float distanceToLine(const tVector3D& unitBasis) const
    {
        float sp=scalarProjection(unitBasis);
        return sqrt(lengthSquared()-sp*sp);
    }
    inline void splitComponents(const tVector3D& unitBasis, float& x, float& y)
    {
        y=scalarProjection(unitBasis);
        x=sqrt(lengthSquared()-y*y);
    }
    tVector3D truncateLength(float maxLength) const
    {
        float maxLengthSquared = maxLength * maxLength;
        float vecLengthSquared = lengthSquared();
        if (vecLengthSquared <= maxLengthSquared)
            return *this;
        else
            return operator*(maxLength / sqrt(vecLengthSquared));
    }
};

//The tVector2D class contains one two-dimensional vector and provides a set
//of basic vector operations.
class tVector2D
{
    public:
    //These are the x and y components of the vector
    float x;
    float y;

    //Define an inline function for adding two vectors.
    tVector2D operator+(const tVector2D& arg) const
    {
        tVector2D v = {x+arg.x, y+arg.y};
        return v;
    }
    //Define an inline function for subtracting two vectors.
    tVector2D operator-(const tVector2D& arg) const
    {
        tVector2D v = {x-arg.x, y-arg.y};
        return v;
    }
    //Define an inline function for multiplying a vector by a scalar.
    tVector2D operator*(float c) const
    {
        tVector2D v = {c*x, c*y};
        return v;
    }
    //Define an inline function for dividing a vector by a scalar.
    tVector2D operator/(float c) const
    {
        tVector2D v = {x/c, y/c};
        return v;
    }
    //Define an inline function for negating a vector.
    tVector2D operator-() const
    {
        tVector2D v = {-x, -y};
        return v;
    }
    //Define an inline function for adding two vectors and storing the result
    //in the first.
    void operator+=(const tVector2D& arg)
    {
        x+=arg.x;
        y+=arg.y;
    }
    //Define an inline function for subtracting two vectors and storing the
    //result in the first.
    void operator-=(const tVector2D& arg)
    {
        x-=arg.x;
        y-=arg.y;
    }
    //Define an inline function for multiplying a vector by a scalar and
    //storing the result in the vector.
    void operator*=(float c)
    {
        x*=c;
        y*=c;
    }
    //Define an inline function for dividing a vector by a scalar and
    //storing the result in the vector.
    void operator/=(float c)
    {
        x/=c;
        y/=c;
    }
    //Define an inline function for comparing two vectors.
    bool operator==(const tVector2D& arg) const
    {
        return x==arg.x && y==arg.y;
    }
    //Define an inline function for comparing two vectors.
    bool operator!=(const tVector2D& arg) const
    {
        return x!=arg.x || y!=arg.y;
    }
    //Define an inline function for calculating the dot product of two vectors.
    float dot(const tVector2D& arg) const
    {
        return x*arg.x + y*arg.y;
    }
    //Define an inline function for calculating the dot product of two vectors.
    //Equals ||a|| * ||b|| * cos(theta)
    float dot(float vx, float vy) const
    {
        tVector2D v = {vx,vy};
        return dot(v);
    }
    //Define an inline function for calculating the cross product of two vectors.
    //Equals ||a|| * ||b|| * sin(theta)
    float cross(const tVector2D& arg) const
    {
        return x*arg.y-y*arg.x;
    }
    //Define an inline function for calculating the cross product of two vectors.
    float cross(float vx, float vy) const
    {
        tVector2D v = {vx,vy};
        return cross(v);
    }

    //This function returns the length of the vector.
    float length() const
    {
        return sqrt(x*x+y*y);
    }

    //This function returns a vector's length squared.
    float lengthSquared() const
    {
        return x*x+y*y;
    }

    //This function returns the angle of the vector.
    float angle();

    //This function returns a copy of the vector which points in the same
    //direction, but has length one.  It does not affect the original vector.
    //If you attempt to normalize the zero vector, this function will return
    //the zero vector.
    tVector2D normalize() const
    {
        tVector2D v = {x,y};
        float l=v.length();
        if(l!=0)
        {
            v.x/=l;
            v.y/=l;
        }
        return v;
    }

    //Rotates the vector 90 degrees counter-clockwise
    tVector2D perpendicularCCW() const
    {
        tVector2D v={-y,x};
        return v;
    }
    //Rotates the vector 90 degrees clockwise
    tVector2D perpendicularCW() const
    {
        tVector2D v={y,-x};
        return v;
    }
    inline tVector2D parallelComponent(const tVector2D& unitBasis) const
    {
        float projection = dot(unitBasis);
        return unitBasis * projection;
    }
    inline float scalarProjection(const tVector2D& unitBasis) const
    {
        return dot(unitBasis);
    }
    inline tVector2D perpendicularComponent(const tVector2D& unitBasis) const
    {
        return operator-(parallelComponent(unitBasis));
    }
    inline float distanceToLine(const tVector2D& unitBasis) const
    {
        float sp=scalarProjection(unitBasis);
        return sqrt(lengthSquared()-sp*sp);
    }
    inline void splitComponents(const tVector2D& unitBasis, float& x, float& y)
    {
        y=scalarProjection(unitBasis);
        x=sqrt(lengthSquared()-y*y);
    }
    tVector2D truncateLength(float maxLength) const
    {
        float maxLengthSquared = maxLength * maxLength;
        float vecLengthSquared = lengthSquared();
        if (vecLengthSquared <= maxLengthSquared)
            return *this;
        else
            return operator*(maxLength / sqrt(vecLengthSquared));
    }
};

tVector2D inline operator*(float s, const tVector2D& v) { return v*s; }
tVector3D inline operator*(float s, const tVector3D& v) { return v*s; }

//Make these vector constants available to other modules:
extern const tVector3D iHat3D, jHat3D, kHat3D, zeroVector3D;
extern const tVector2D iHat2D, jHat2D, zeroVector2D;

extern tVector2D to2D(const tVector3D& v);
extern tVector3D to3D(const tVector2D& v);

extern bool     angleIsBetween(float angle, float a, float b, bool counterClockwise);
extern int      angleCompare(int x1, int y1, int x2, int y2);

extern bool     createTrigTables();
extern void     destroyTrigTables();
extern float    lookupSin(float angle);
extern float    lookupCos(float angle);
extern float    lookupAsin(float val);
extern float    lookupAcos(float val);
extern float    lookupSpline(float val, int index);
extern float*   lookupSplineArray(float val);
extern float    lookupSplineDeriv(float val, int index);
extern float*   lookupSplineDerivArray(float val);

template<class A>
extern A vecLimitDeviationAngleUtility (const bool insideOrOutside,
                                        const A& source,
                                        const float cosineOfConeAngle,
                                        const A& basis);

// Enforce an upper bound on the angle by which a given arbitrary vector
// deviates from a given reference direction (specified by a unit basis
// vector).  The effect is to clip the "source" vector to be inside a cone
// defined by the basis and an angle. [by Craig Reynolds]
template<class A>
A inline limitMaxDeviationAngle (const A& source,
                                 const float cosineOfConeAngle,
                                 const A& basis)
{
    return vecLimitDeviationAngleUtility (true, // force source INSIDE cone
                                          source,
                                          cosineOfConeAngle,
                                          basis);
}

// Enforce a lower bound on the angle by which a given arbitrary vector
// deviates from a given reference direction (specified by a unit basis
// vector).  The effect is to clip the "source" vector to be outside a cone
// defined by the basis and an angle. [by Craig Reynolds]
template<class A>
A inline limitMinDeviationAngle (const A& source,
                                 const float cosineOfConeAngle,
                                 const A& basis)
{
    return vecLimitDeviationAngleUtility (false, // force source OUTSIDE cone
                                          source,
                                          cosineOfConeAngle,
                                          basis);
}

// Returns the distance between a point and a line.  The line is defined in
// terms of a point on the line ("lineOrigin") and a UNIT vector parallel to
// the line ("lineUnitTangent") [by Craig Reynolds]
template<class A>
float inline distanceFromLine (const A& point,
                               const A& lineOrigin,
                               const A& lineUnitTangent)
{
    const A offset = point - lineOrigin;
    const A perp = offset.perpendicularComponent(lineUnitTangent);
    return perp.length();
}

extern tVector3D multiplyMatrix4x4(const float mat[16], const tVector3D& v);
extern tVector3D multiplyMatrix3x4(const float mat[16], const tVector3D& v);
extern tVector3D multiplyMatrix3x3(const float mat[16], const tVector3D& v);
extern tVector2D multiplyMatrix2x3(const float mat[16], const tVector2D& v);
extern float calcAngleBetween(const tVector3D& a,const tVector3D& b,const tVector3D& axis,
    float& sinAngle); //axis must be normalized
extern bool isIdentityMatrix(const float mat[16]);
/****************************************************************************/
#endif
