/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef parseH
#define parseH
/****************************************************************************/
#ifndef CACHE_HEADERS
    #ifdef BORLAND
        #include <stack.h> //for stack
    #else
        #include <stack> //for stack
    #endif
#endif

#include "types.h"
#include "vec.h"

#define PARSE_NEW_LINE  0x10A
#define KEY_TABLE_SIZE  62

enum eBraceStyle { BRACE_PARENTHESIS, BRACE_SQUARE_BRACKET, BRACE_CURLY_BRACE, BRACE_ANGLE_BRACKET };

struct tKeyTableEntry
{
    const char *name;
    tKey value;
};

struct tStringBuffer
{
    char *pointer;
    tStringBuffer(const tString& s);
    ~tStringBuffer();
    char *begin() { return pointer; }
};

//Structures
class tStream
{
    private:
    char *stringStart;
    char *stringPointer;
    FILE *file;
    char nextChar;
    int nextSymbol;
    tString curToken;
    tString nextToken;
    long curLineNumber;
    long nextLineNumber;
    bool curEndOfLine;
    bool nextEndOfLine;
    bool curEndOfStream;
    bool nextEndOfStream;
    int commentLevel;
    long commentStart;
    stack<eBraceStyle> braces;

    char getNextChar();
    int getNextSymbol();
    const tString& getNextToken() throw(tString);
    bool isWhiteSpace(int sym);
    bool isPunctuation(char ch);

    public:
    tStream();
    void load(FILE *_file) throw(tString);
    void load(const char *string) throw(tString);
    ~tStream();

    long getLineNumber() { return curLineNumber; }
    const tString& parseToken(const char *fieldName) throw(tString);
    void matchToken(const char *keyword) throw(tString);
	bool matchOptionalToken(const char *keyword) throw(tString);
    const tString& peekAhead() { return nextToken; }
    bool atEndOfLine() { return curEndOfLine; }
    bool atEndOfStream() { return curEndOfStream; }
    bool beginBlock();
    bool endBlock() throw(tString);
    void endStatement() throw(tString);
    bool doesBlockHaveName();
	bool hasOptionalParameter();
    tString parseErrorMessage(const char *filename, const tString& exc);
};

//Externals:
const tString&  parseString     (const char *fieldName, tStream& s) throw(tString);
tString         parseIdentifier (const char *fieldName, tStream& s) throw(tString);
int             parseInt        (const char *fieldName, tStream& s) throw(tString);
unsigned int    parseUnsigned   (const char *fieldName, tStream& s) throw(tString);
long            parseLong       (const char *fieldName, tStream& s) throw(tString);
float           parseFloat      (const char *fieldName, tStream& s) throw(tString);
int             parseIntRange   (const char *fieldName, int min, int max, tStream& s) throw(tString);
long            parseLongRange  (const char *fieldName, long min, long max, tStream& s) throw(tString);
float           parseFloatRange (const char *fieldName, float min, float max, tStream& s) throw(tString);
int             parseIntMin     (const char *fieldName, int min, tStream& s) throw(tString);
long            parseLongMin    (const char *fieldName, long min, tStream& s) throw(tString);
float           parseFloatMin   (const char *fieldName, float min, tStream& s) throw(tString);
int             parseIntMax     (const char *fieldName, int max, tStream& s) throw(tString);
long            parseLongMax    (const char *fieldName, long max, tStream& s) throw(tString);
float           parseFloatMax   (const char *fieldName, float max, tStream& s) throw(tString);
bool            parseBool       (const char *fieldName, tStream& s) throw(tString);

tVector2D       parseVector2D   (const char *fieldName, tStream& s) throw(tString);
tVector3D       parseVector3D   (const char *fieldName, tStream& s) throw(tString);
tKey            parseHotKey     (const char *fieldName, tStream& s) throw(tString);
float           parseAngle      (const char *fieldName, tStream& s) throw(tString);
float           parseAngleMin   (const char *fieldName, float min, tStream& s) throw(tString);
float           parseAngleMax   (const char *fieldName, float max, tStream& s) throw(tString);
float           parseAngleRange (const char *fieldName, float min, float max, tStream& s) throw(tString);

template<class T> inline T parse(const char *fieldName, tStream& s) throw(tString)
    { bug("parse<T>: this type is not supported");
    throw tString("Unexpected error occurred"); }
template<class T> inline T parseMin(const char *fieldName, T min, tStream& s) throw(tString)
    { bug("parseMin<T>: this type is not supported");
    throw tString("Unexpected error occurred"); }
template<class T> inline T parseMax(const char *fieldName, T max, tStream& s) throw(tString)
    { bug("parseMax<T>: this type is not supported");
    throw tString("Unexpected error occurred"); }
template<class T> inline T parseRange(const char *fieldName, T min, T max, tStream& s) throw(tString)
    { bug("parseRange<T>: this type is not supported");
    throw tString("Unexpected error occurred"); }

template<> inline int parse<int>(const char *fieldName, tStream& s) throw(tString) { return parseInt(fieldName,s); }
template<> inline long parse<long>(const char *fieldName, tStream& s) throw(tString) { return parseLong(fieldName,s); }
template<> inline float parse<float>(const char *fieldName, tStream& s) throw(tString) { return parseFloat(fieldName,s); }
template<> inline bool parse<bool>(const char *fieldName, tStream& s) throw(tString) { return parseBool(fieldName,s); }

template<> inline int parseMin<int>(const char *fieldName, int min, tStream& s) throw(tString)
    { return parseIntMin(fieldName,min,s); }
template<> inline long parseMin<long>(const char *fieldName, long min, tStream& s) throw(tString)
    { return parseLongMin(fieldName,min,s); }
template<> inline float parseMin<float>(const char *fieldName, float min, tStream& s) throw(tString)
    { return parseFloatMin(fieldName,min,s); }
template<> inline bool parseMin<bool>(const char *fieldName, bool min, tStream& s) throw(tString)
    { bug("parseMin<bool>: this function is not supported");
    throw tString("Unexpected error occurred"); }

template<> inline int parseMax<int>(const char *fieldName, int max, tStream& s) throw(tString)
    { return parseIntMax(fieldName,max,s); }
template<> inline long parseMax<long>(const char *fieldName, long max, tStream& s) throw(tString)
    { return parseLongMax(fieldName,max,s); }
template<> inline float parseMax<float>(const char *fieldName, float max, tStream& s) throw(tString)
    { return parseFloatMax(fieldName,max,s); }
template<> inline bool parseMax<bool>(const char *fieldName, bool max, tStream& s) throw(tString)
    { bug("parseMax<bool>: this function is not supported");
    throw tString("Unexpected error occurred"); }

template<> inline int parseRange<int>(const char *fieldName, int min, int max, tStream& s) throw(tString)
    { return parseIntRange(fieldName,min,max,s); }
template<> inline long parseRange<long>(const char *fieldName, long min, long max, tStream& s) throw(tString)
    { return parseLongRange(fieldName,min,max,s); }
template<> inline float parseRange<float>(const char *fieldName, float min, float max, tStream& s) throw(tString)
    { return parseFloatRange(fieldName,min,max,s); }
template<> inline bool parseRange<bool>(const char *fieldName, bool min, bool max, tStream& s) throw(tString)
    { bug("parseRange<bool>: this function is not supported");
    throw tString("Unexpected error occurred"); }

extern bool lookupVariable(const char *name, const char *filename, tString& value);
extern void endOfStatement(tStream& s) throw(tString);
extern tKeyTableEntry keyTable[KEY_TABLE_SIZE];
/****************************************************************************/
#endif
