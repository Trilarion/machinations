/****************************************************************************
* Machinations copyright (C) 2001, 2002 by Jon Sargeant and Jindra Kolman   *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include <vector.h>
#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------

#include "fontmak1.h"
#include "fontmak2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMain *Main;
//---------------------------------------------------------------------------
const GLYPHMETRICS zeroMetrics={0,0,{0,0},0,0};
static const char machFontCode[4]={'F','N','T','1'};

//I arrange the characters in height ascending order and width ascending order.
int charCompare(const void *a, const void *b)
{
    tCharacter *x=*((tCharacter **)a);
    tCharacter *y=*((tCharacter **)b);

    int x1=x->glyphMetrics.gmBlackBoxX;
    int y1=x->glyphMetrics.gmBlackBoxY;
    int x2=y->glyphMetrics.gmBlackBoxX;
    int y2=y->glyphMetrics.gmBlackBoxY;

    if(y1>y2||y1==y2&&x1>x2)
        return -1;
    else if(y1==y2&&x1==x2)
        return 0;
    else
        return 1;
}
//---------------------------------------------------------------------------
tCharacter::tCharacter() : glyphMetrics(zeroMetrics), data(NULL), textureX(0), textureY(0)
{
}
//---------------------------------------------------------------------------
tCharacter::~tCharacter()
{
    if(data)
        delete[] data;
}
//---------------------------------------------------------------------------
__fastcall TMain::TMain(TComponent* Owner)
    : TForm(Owner), charSpacing(1), modified(false)
{
    NewClick(NULL);
}
//---------------------------------------------------------------------------
void __fastcall TMain::FormDestroy(TObject *Sender)
{
    eraseFonts();
}
//---------------------------------------------------------------------------
void __fastcall TMain::NewClick(TObject *Sender)
{
    if(modified)
    {
        int retval=Application->MessageBox("Save changes to font set?","Confirmation",
            MB_YESNOCANCEL|MB_ICONQUESTION);
        if(retval==IDCANCEL)
            return;
        else if(retval==IDYES)
            SaveClick(NULL);
    }

    currentFile="";
    Caption=AnsiString("Machinations Font Manager");
    eraseFonts();
    arrangeCharacters();
    updateTexture();
    modified=false;
}
//---------------------------------------------------------------------------
void __fastcall TMain::btAddFontClick(TObject *Sender)
{
    tFont *font=new tFont;
    TAddFont *addFont=new TAddFont("Add New Font",font,Application);
    if(addFont->ShowModal()==mrOk)
    {
        lbFonts->Items->AddObject(generateDescription(font),(TObject*)font);
        arrangeCharacters();
        updateTexture();
        modified=true;
    }
    else
        delete font;
    delete addFont;
}
//---------------------------------------------------------------------------
void __fastcall TMain::QuitClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------
void TMain::arrangeCharacters()
{
    int i,j,w,x,y;
    tFont *font;
    vector<tCharacter *> chars;

    memset(textureData,0,256*256);

    for(i=0;i<lbFonts->Items->Count;i++)
    {
        font=(tFont*)lbFonts->Items->Objects[i];
        for(j=0;j<256;j++)
        {
            tCharacter& c=font->characters[j];
            c.textureX=-1;
            c.textureY=-1;
            if(c.glyphMetrics.gmBlackBoxX>0 && c.glyphMetrics.gmBlackBoxY>0 &&
                (!font->caseInsensitive || j<'a' || j>'z'))
                chars.push_back(&c);
        }
    }

    if(chars.size()==0)
        return;

    qsort((void *)&chars[0], chars.size(), sizeof(tCharacter *), charCompare);

    int row=0, col=0, nextRow=chars[0]->glyphMetrics.gmBlackBoxY+charSpacing;
    while(chars.size()>0)
    {
        for(i=0;i<chars.size();i++)
            if(col+chars[i]->glyphMetrics.gmBlackBoxX<=256)
                break;
        if(i<chars.size())
        {
            chars[i]->textureX=col;
            chars[i]->textureY=row;
            if(font->caseInsensitive && i>='A' && i<='Z')
            {
                chars[i|0x20]->textureX=col;
                chars[i|0x20]->textureY=row;
            }

            if(chars[i]->data)
            {
                w = ALIGN(chars[i]->glyphMetrics.gmBlackBoxX, 4);

                for(y=0;y<chars[i]->glyphMetrics.gmBlackBoxY;y++)
                    for(x=0;x<chars[i]->glyphMetrics.gmBlackBoxX;x++)
                        textureData[row+y][col+x]=chars[i]->data[w*y+x];
            }

            col+=chars[i]->glyphMetrics.gmBlackBoxX+charSpacing;
            chars.erase(chars.begin()+i);
        }
        else
        {
            col=0;
            row=nextRow;
            nextRow+=chars[0]->glyphMetrics.gmBlackBoxY+charSpacing;
            if(nextRow>256)
            {
                Application->MessageBox("There isn't enough room on the texture for all of the fonts.",
                    "Error",MB_OK|MB_ICONERROR);
                return;
            }
        }
    }
}
//---------------------------------------------------------------------------
void TMain::updateTexture()
{
    int i,j,val;

    imImage->Canvas->Brush->Color=clBlack;
    imImage->Canvas->FillRect(imImage->Canvas->ClipRect);
    imImage->Hide();
    for(i=0;i<256;i++)
        for(j=0;j<256;j++)
        {
            val=0x010101*(unsigned char)textureData[j][i];
            if(val!=0)
                imImage->Canvas->Pixels[i][255-j]=(TColor)val;
        }
    imImage->Show();
}
//---------------------------------------------------------------------------
void TMain::eraseFonts()
{
    int i;
    for(i=0;i<lbFonts->Items->Count;i++)
        if(lbFonts->Items->Objects[i])
            delete ((tFont*) lbFonts->Items->Objects[i]);
    lbFonts->Items->Clear();
}
//---------------------------------------------------------------------------
AnsiString TMain::generateDescription(tFont *font)
{
    AnsiString s=font->name+":  "+font->trueTypeFont+" "+IntToStr((int)font->fontSize);
    if(font->bold)
        s+=" Bold";
    if(font->italic)
        s+=" Italic";
    if(font->underline)
        s+=" Underline";
    if(font->strikeout)
        s+=" Strikeout";
    if(font->antialias)
        s+=" Antialiased";
    if(font->caseInsensitive)
        s+=" Case-Insensitive";

    return s;
}
//---------------------------------------------------------------------------
void __fastcall TMain::btEditFontClick(TObject *Sender)
{
    int index=lbFonts->ItemIndex;
    if(index<0)
    {
        Application->MessageBox("Please select a font to edit.","Error",MB_OK|MB_ICONERROR);
        return;
    }

    tFont *font=(tFont*)lbFonts->Items->Objects[index];
    TAddFont *addFont=new TAddFont("Edit Exisiting Font",font,Application);
    if(addFont->ShowModal()==mrOk)
    {
        lbFonts->Items->Strings[index]=generateDescription(font);
        arrangeCharacters();
        updateTexture();
        modified=true;
    }
    delete addFont;
}
//---------------------------------------------------------------------------
void __fastcall TMain::btRemoveFontClick(TObject *Sender)
{
    int index=lbFonts->ItemIndex;
    if(index<0)
    {
        Application->MessageBox("Please select a font to delete.","Error",MB_OK|MB_ICONERROR);
        return;
    }

    tFont *font=(tFont*)lbFonts->Items->Objects[index];
    if(font)
        delete font;

    lbFonts->Items->Delete(index);

    arrangeCharacters();
    updateTexture();
    modified=true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::edSpacingChange(TObject *Sender)
{
    int newSpacing;
    if(edSpacing->Text.Length()>0)
    {
        newSpacing=StrToInt(edSpacing->Text);
        if(newSpacing!=charSpacing)
        {
            charSpacing=newSpacing;
            arrangeCharacters();
            updateTexture();
            modified=true;
        }
    }
}
//---------------------------------------------------------------------------
void TMain::saveFontSet(const AnsiString& filename)
{
    int i,j,w,h;
    long l;
    char ch;
    tFont *font;
    FILE *fp=fopen(filename.c_str(),"wb");
    if(fp==NULL)
    {
        AnsiString message=AnsiString("Could not open '")+filename+"' for writing.";
        Application->MessageBox(message.c_str(),"Error",MB_OK|MB_ICONERROR);
        return;
    }

    fwrite(machFontCode,sizeof(machFontCode),1,fp);

    l=(long)lbFonts->Items->Count;
    fwrite(&l,sizeof(l),1,fp);

    for(i=0;i<lbFonts->Items->Count;i++)
    {
        font=(tFont*)lbFonts->Items->Objects[i];
        writeString(font->name,fp);
        writeString(font->trueTypeFont,fp);
        writeString(font->charsToConvert,fp);
        fwrite(&font->fontSize,sizeof(font->fontSize),1,fp);

        ch=0;
        if(font->bold)              ch|=1;
        if(font->italic)            ch|=2;
        if(font->underline)         ch|=4;
        if(font->strikeout)         ch|=8;
        if(font->antialias)         ch|=16;
        if(font->caseInsensitive)   ch|=32;
        fwrite(&ch,sizeof(ch),1,fp);

        for(j=0;j<256;j++)
        {
            tCharacter& c=font->characters[j];
            fwrite(&c.glyphMetrics,sizeof(c.glyphMetrics),1,fp);
            fwrite(&c.textureX,sizeof(c.textureX),1,fp);
            fwrite(&c.textureY,sizeof(c.textureY),1,fp);

            w=ALIGN(c.glyphMetrics.gmBlackBoxX, 4);
            h=c.glyphMetrics.gmBlackBoxY;
            if(w*h>0)
            {
                if(c.data==NULL)
                {
                    Application->MessageBox("Unexpected error occurred while saving.","Error",MB_OK|MB_ICONERROR);
                    fclose(fp);
                    return;
                }

                fwrite(c.data,w*h,1,fp);
            }
        }
    }
    l=(long)charSpacing;
    fwrite(&l,sizeof(l),1,fp);

    fwrite(textureData,1,256*256,fp);

    fclose(fp);
}
//---------------------------------------------------------------------------
bool TMain::loadFontSet(const AnsiString& filename)
{
    int i,j,w,h;
    char code[4];
    int fontCount;
    long l;
    char ch;
    tFont *font;
    FILE *fp=fopen(filename.c_str(),"rb");

    if(fp==NULL)
    {
        AnsiString message=AnsiString("Could not open '")+filename+"' for reading.";
        Application->MessageBox(message.c_str(),"Error",MB_OK|MB_ICONERROR);
        return false;
    }

    fread(code,sizeof(code),1,fp);
    if(memcmp(code,machFontCode,4)!=0)
    {
        AnsiString message=AnsiString("'")+filename+"' is not a valid Machinations font set.";
        Application->MessageBox(message.c_str(),"Error",MB_OK|MB_ICONERROR);
        fclose(fp);
        return false;
    }

    fread(&l,sizeof(l),1,fp);
    fontCount=(int)l;

    for(i=0;i<fontCount;i++)
    {
        font = new tFont;
        font->name=readString(fp);
        font->trueTypeFont=readString(fp);
        font->charsToConvert=readString(fp);
        fread(&font->fontSize,sizeof(font->fontSize),1,fp);

        fread(&ch,sizeof(ch),1,fp);
        font->bold=             (bool)(ch&1);
        font->italic=           (bool)(ch&2);
        font->underline=        (bool)(ch&4);
        font->strikeout=        (bool)(ch&8);
        font->antialias=        (bool)(ch&16);
        font->caseInsensitive=  (bool)(ch&32);

        for(j=0;j<256;j++)
        {
            tCharacter& c=font->characters[j];
            fread(&c.glyphMetrics,sizeof(c.glyphMetrics),1,fp);
            fread(&c.textureX,sizeof(c.textureX),1,fp);
            fread(&c.textureY,sizeof(c.textureY),1,fp);

            w=ALIGN(c.glyphMetrics.gmBlackBoxX, 4);
            h=c.glyphMetrics.gmBlackBoxY;
            if(w*h>0)
            {
                c.data=new char[w*h];
                fread(c.data,w*h,1,fp);
            }
        }

        lbFonts->Items->AddObject(generateDescription(font),(TObject*)font);
    }
    fread(&l,sizeof(l),1,fp);
    charSpacing=(int)l;
    edSpacing->Text=IntToStr(charSpacing);
    ActiveControl=lbFonts;

    fread(textureData,1,256*256,fp);
    fclose(fp);

    updateTexture();
    return true;
}
//---------------------------------------------------------------------------
void TMain::writeString(const AnsiString& string, FILE *fp)
{
    AnsiString s=string+"\n";
    fputs(s.c_str(),fp);
}
//---------------------------------------------------------------------------
AnsiString TMain::readString(FILE *fp)
{
    char buf[1000];
    fgets(buf,1000,fp);
    int len=strlen(buf);
    if(len>0 && buf[len-1]=='\n')
        buf[len-1]='\0';
    return AnsiString(buf);
}
//---------------------------------------------------------------------------
void __fastcall TMain::OpenClick(TObject *Sender)
{
    if(modified)
    {
        int retval=Application->MessageBox("Save changes to font set?","Confirmation",
            MB_YESNOCANCEL|MB_ICONQUESTION);
        if(retval==IDCANCEL)
            return;
        else if(retval==IDYES)
            SaveClick(NULL);
    }

    if(odOpenDialog->Execute())
    {
        NewClick(NULL);
        if(loadFontSet(odOpenDialog->FileName))
        {
            currentFile=odOpenDialog->FileName;
            Caption=AnsiString("Machinations Font Manager - ")+currentFile;
        }
    }
}
//---------------------------------------------------------------------------
void __fastcall TMain::SaveClick(TObject *Sender)
{
    if(currentFile.Length()==0)
        SaveAsClick(NULL);
    else
    {
        saveFontSet(currentFile);
        modified=false;
    }
}
//---------------------------------------------------------------------------
void __fastcall TMain::SaveAsClick(TObject *Sender)
{
    if(sdSaveDialog->Execute())
    {
        currentFile=sdSaveDialog->FileName;
        Caption=AnsiString("Machinations Font Manager - ")+currentFile;
        saveFontSet(currentFile);
        modified=false;
    }
}
//---------------------------------------------------------------------------
void __fastcall TMain::lbFontsKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
    if(Key==VK_RETURN)
        btEditFontClick(NULL);
    else if(Key==VK_DELETE)
        btRemoveFontClick(NULL);
}
//---------------------------------------------------------------------------
void __fastcall TMain::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    if(modified)
    {
        int retval=Application->MessageBox("Save changes to font set?","Confirmation",
            MB_YESNOCANCEL|MB_ICONQUESTION);
        CanClose=true;
        if(retval==IDCANCEL)
            CanClose=false;
        else if(retval==IDYES)
            SaveClick(NULL);
    }
}
//---------------------------------------------------------------------------

