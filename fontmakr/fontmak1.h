/****************************************************************************
* Machinations copyright (C) 2001, 2002 by Jon Sargeant and Jindra Kolman   *
****************************************************************************/
#ifndef fontmak1H
#define fontmak1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <stdlib.h>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
#define ALIGN(x,y)              ((((x)-1)/(y)+1)*(y)) //finds the smallest multiple of y >= x
#define MAX_COLORS              7

struct tCharacter
{
    //Temporary variable for assigning the character a color:
    unsigned char color;

    //I retrieve this data from the true type font:
    GLYPHMETRICS glyphMetrics;
    char *data;
    //I generate this data once I fit the characters on the texture:
    short textureX;
    short textureY;

    tCharacter();
    ~tCharacter();
};

struct tFont
{
    AnsiString name;
    AnsiString trueTypeFont;
    AnsiString charsToConvert;
    long fontSize;
    bool bold;
    bool italic;
    bool underline;
    bool strikeout;
    bool antialias;
    bool caseInsensitive;

    tCharacter characters[256];

    tFont(const AnsiString& _name, const AnsiString& _trueTypeFont, int _fontSize, bool _bold,
        bool _italic, bool _underline, bool _strikeout, bool _antialias,
        bool _caseInsensitive) : name(_name), trueTypeFont(_trueTypeFont),
        fontSize(_fontSize), bold(_bold), italic(_italic), underline(_underline),
        strikeout(_strikeout), antialias(_antialias), caseInsensitive(_caseInsensitive) {}
    tFont() : fontSize(0), bold(false), italic(false), underline(false), strikeout(false),
        charsToConvert("32-126"), antialias(false), caseInsensitive(false) {}
};

class TMain : public TForm
{
__published:	// IDE-managed Components
    TSaveDialog *sdSaveDialog;
    TGroupBox *GroupBox1;
    TPanel *Panel1;
    TImage *imImage;
    TMainMenu *MainMenu;
    TMenuItem *New;
    TMenuItem *Open;
    TMenuItem *Save;
    TMenuItem *Quit;
    TGroupBox *GroupBox2;
    TListBox *lbFonts;
    TButton *btAddFont;
    TButton *btRemoveFont;
    TButton *btEditFont;
    TEdit *edSpacing;
    TLabel *Label1;
    TOpenDialog *odOpenDialog;
    TMenuItem *SaveAs;
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall NewClick(TObject *Sender);
    void __fastcall btAddFontClick(TObject *Sender);
    void __fastcall QuitClick(TObject *Sender);
    void __fastcall btEditFontClick(TObject *Sender);
    void __fastcall btRemoveFontClick(TObject *Sender);
    void __fastcall edSpacingChange(TObject *Sender);
    void __fastcall OpenClick(TObject *Sender);
    void __fastcall SaveClick(TObject *Sender);
    void __fastcall SaveAsClick(TObject *Sender);
    void __fastcall lbFontsKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
private:	// User declarations
    char textureData[256][256];
    int charSpacing;
    AnsiString currentFile;
    bool modified;
    
    void arrangeCharacters();
    void updateTexture();
    void eraseFonts();
    AnsiString generateDescription(tFont *font);
    void saveFontSet(const AnsiString& filename);
    bool loadFontSet(const AnsiString& filename);
    void writeString(const AnsiString& string, FILE *fp);
    AnsiString readString(FILE *fp);
public:		// User declarations
    __fastcall TMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMain *Main;

extern const GLYPHMETRICS zeroMetrics;
//---------------------------------------------------------------------------
#endif
