//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USERES("fontmakr.res");
USEFORM("fontmak1.cpp", Main);
USEFORM("fontmak2.cpp", AddFont);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    try
    {
         Application->Initialize();
         Application->Title = "Machinations Font Manager";
         Application->CreateForm(__classid(TMain), &Main);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    return 0;
}
//---------------------------------------------------------------------------
