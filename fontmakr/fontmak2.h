//---------------------------------------------------------------------------
#ifndef fontmak2H
#define fontmak2H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TAddFont : public TForm
{
__published:	// IDE-managed Components
    TGroupBox *gbSelectFont;
    TLabel *Label1;
    TLabel *Label2;
    TCheckBox *cbBold;
    TCheckBox *cbItalic;
    TCheckBox *cbUnderline;
    TCheckBox *cbStrikeout;
    TEdit *edFontSize;
    TListBox *lbTrueTypeFonts;
    TGroupBox *gbTestFont;
    TMemo *meTest;
    TGroupBox *gbSelectCharacters;
    TLabel *Label3;
    TEdit *edCharsToConvert;
    TGroupBox *gbEnterName;
    TLabel *Label4;
    TEdit *edName;
    TCheckBox *cbAntialias;
    TButton *btOK;
    TButton *btCancel;
    TCheckBox *cbCaseInsensitive;
    void __fastcall cbStyleClick(TObject *Sender);
    void __fastcall edFontSizeChange(TObject *Sender);
    void __fastcall lbTrueTypeFontsClick(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall btOKClick(TObject *Sender);
    void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
    HWND hWnd;
    HDC hDC;
public:		// User declarations
    bool bold;
    bool italic;
    bool underline;
    bool strikeout;
    tFont *font;
    __fastcall TAddFont(const AnsiString& caption, tFont *_font, TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TAddFont *AddFont;
//---------------------------------------------------------------------------
#endif
